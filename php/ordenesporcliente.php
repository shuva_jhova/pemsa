﻿<?php
require_once("usersFunction.php");
$app = new usersFunction();
	if(!$app->islogged()){
		echo "<script>window.top.location.href = 'logout.php';</script>";	
	}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="initial-scale=1.0,user-scalable=yes"/>

<link rel="stylesheet" href="../css/estilo2.css">
<link rel="stylesheet" href="../css/jquery.dataTables.min.css">
 <script language="javascript" type="text/javascript" src="../js/gateway.js"></script>
<script language="javascript" type="text/javascript" src="../js/jquery-1.12.4.js"></script>
<script language="javascript" type="text/javascript" src="../js/jquery.dataTables.min.js"></script>
  <script language="javascript" type="text/javascript" src="../js/ordenesporclientes.js"></script>

	<script language="javascript" src="../js/xlsx.full.min.js"></script>
	
	<script language="javascript" src="../js/FileSaver.js"></script>
	
	<script language="javascript" src="../js/tableexport.js"></script>

<title></title>

</head>
<body onload="getclientes();">
<main>
	<datalist id="clientes"></datalist>
	<section id="titulo">
		<center><h2>Reporte de Ordenes por Cliente.</h2>
		</center>
	</section>
<div>
	<form name="f1" action="#">
		<center>
		<div id="myDiv">
				
				<div class="txt">
					Fecha Inicio<br>
					<input type="date"  id="txtfechainicial" required="required" class="TT" />	
				</div>
				<div class="txt">
					Fecha Fin<br>
					<input type="date"  id="txtfechafinal"  required="required" class="TT" />	
				</div>

				<div class="txt">
					Cliente<br>
					<input type="text"  id="txtcliente" list='clientes' onBlur="getordenesbyclient();" required="required"  class="TT" />	
				</div>

				<div class="txt">
						<input type="button" name="insertar" Value="Actualizar" onClick="getordenesbyclient();" id="ok"/>
				</div>
			<div >
				<img src="../img/load.gif" class="imgload" id="imgload">
			</div>
		</div>
		<div id="sin2" style="display:none;">
			<div id="myDiv">
				<div class="txt">
					Ingresa el Pedido SAE a Buscar<br>
					<input type="text" name="num" placeholder="Pedido SAE" id="txtbuscar" onkeyup="doSearch()" required="required" class="TT" />
				
				</div>
				<!--div class="txt">
						<input type="button" name="insertar" Value="Actualizar" onClick="getusuarios();" id="ok"/>
				</div-->
			
		</div>
		</center>
	</form>
</div>
<center>
</br>
	<!--div class="txt">
			<input type="button" name="insertar" Value="Agregar" onClick="agregar();" id="ok"/>
	</div-->
	</br>
<div id="scro">
 <div id="tabla" style="display:none;">
     <table class="tbl-qa display nowrap" style="width:100%" id="resultado">
		  <thead>
		 <tr>
		<th class="table-header" >OMP</th>
		<th class="table-header" >Pedido SAE</th>
                <th class="table-header" >Código SAE</th>
	        <th class="table-header" >Producto</th>
		<th class="table-header" >Cantidad Solicitada</th>
		<th class="table-header" >Fecha Emisión</th>
		<th class="table-header" >Fecha Requerida</th>
		<th class="table-header" >Acciones</th>
		
              </tr>
 		  </thead>
		  
		<tbody style="height:250px;overflow:scroll">				
             	 </tbody>
              </table>
 </div>
	</br></br>
		<div id="sin" style="display:none;">
			<div class='myDiv'><div class='txt'>SIN DATOS PARA LOS CRITERIOS DE BUSQUEDA SELECCIONADOS</div></div>
		</div>
 </div>
</center>
</div>


</main>
</body>
</html>
