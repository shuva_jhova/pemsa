﻿<?php
require_once("usersFunction.php");
$app = new usersFunction();
	if(!$app->islogged()){
		echo "<script>window.top.location.href = 'logout.php';</script>";	
	}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="initial-scale=1.0,user-scalable=yes"/>

<link rel="stylesheet" href="../css/estilo2.css">
<link rel="stylesheet" href="../css/jquery.dataTables.min.css">
 <script language="javascript" type="text/javascript" src="../js/gateway.js"></script>
<script language="javascript" type="text/javascript" src="../js/jquery-1.12.4.js"></script>
<script language="javascript" type="text/javascript" src="../js/jquery.dataTables.min.js"></script>
  <script language="javascript" type="text/javascript" src="../js/produccionrealejecutada.js"></script>
	<script language="javascript" src="../js/xlsx.full.min.js"></script>
	
	<script language="javascript" src="../js/FileSaver.js"></script>
	
	<script language="javascript" src="../js/tableexport.js"></script>
<title></title>

</head>
<body >
<main>
	
	<section id="titulo">
		<center><h2>Producción Real Ejecutada</h2>
		</center>
	</section>
<div>
	<form name="f1" action="#">
		<center>
			<div id="myDiv">
				<div class="txt">
					Fecha Inicio<br>
					<input type="date"  id="txtfechainicial" required="required" class="TT" />	
				</div>
				<div class="txt">
					Fecha Fin<br>
					<input type="date"  id="txtfechafinal" required="required" class="TT" />		
				</div>

				<div class="txt">
						<input type="button" name="insertar" Value="Actualizar" onClick="getoplaneacionreal();" id="ok"/>
				</div>
			</div>
			<div >
				<img src="../img/load.gif" class="imgload" id="imgload">
			</div>
		<div id="sin2" style="display:none;">
			<div id="myDiv">
				<div class="txt">
					Ingresa el Número de orden a Buscar<br>
					<input type="text" name="num" placeholder="Número de orden" id="txtbuscar" onkeyup="doSearch()" required="required" class="TT" />
				
				</div>
				<!--div class="txt">
						<input type="button" name="insertar" Value="Actualizar" onClick="getusuarios();" id="ok"/>
				</div>
			<div -->
				<!--img src="../img/load.gif" class="imgload" id="imgload"-->
			</div>
		</div>
		</center>
	</form>
</div>
<center>
</br>
	
	</br>
<div id="scro">
 <div id="tabla">
     <table class="tbl-qa display nowrap" style="width:100%" id="resultado">
		  <thead>
		 <tr>
		<th class="table-header" >OMP</th>
		<th class="table-header" >Folio del Ticket</th>
                <th class="table-header" >Fecha Inicio</th>
	        <th class="table-header" >Hora Inicio</th>
		<th class="table-header" >Fecha Fin</th>
		<th class="table-header" >Hora Fin</th>
		<th class="table-header" >Cantidad Solicitada</th>
		<th class="table-header" >Cantidad Útil de Proceso</th>
		<!--th class="table-header" style='display:none;'>Cantidad Excedente de Proceso</th-->
		<th class="table-header" >Merma</th>		
		<th class="table-header" >Máquina</th>
		<th class="table-header" >Descripción del Proceso</th>
		<th class="table-header" >Operador</th>
		
              </tr>
 		  </thead>
		  
		<tbody style="height:250px;overflow:scroll">				
             	 </tbody>
              </table>
 </div>
	</br></br>
		<div id="sin" style="display:none;">
			<div class='myDiv'><div class='txt'>NO HAY REGISTROS EN EL SISTEMA PARA ESTOS CRITERIOS.</div></div>
		</div>
 </div>
</center>
</div>

<div id="myModal" class="modal">

  <!-- Modal content -->
  <div class="modal-content"> 
  <div class="modal-content2">	
    <span class="close">&times;</span>
			<section id="titulo">
        			<center></br><h2>Ingrese los datos del nuevo operador</h2>
					     <h5>Asegurese de que los datos introducidos sean correctos</h5>
				</center>
			</section>
			</br>
	<div class="contenedor">
			
	<div class="myDiv" >
		<div class="txt">
			Nombre(s) del usuario<br>
			<input type="text" name="num" placeholder="Nombre" id="txtnombre" required="required" class="TT" />	
		</div>
		<div class="txt">
			Primer apellido <br>
			<input type="text" name="num" placeholder="Primer apellido" id="txtappat" required="required" class="TT" />	
		</div>
		<div class="txt">
			Segundo apellido<br>
			<input type="text" name="num" placeholder="Segundo apellido" id="txtapmat" required="required" class="TT" />	
		</div>
		<!--div class="txt">
			Usuario<br>
			<input type="text" name="num" placeholder="Usuario" id="txtusuario" required="required" class="TT" />	
		</div>
		<div class="txt">
			Contraseña<br>
			<input type="password" name="num" placeholder="Contraseña" id="txtpassword" required="required" class="TT" />	
		</div>
		<div class="txt">
			Confirma la contraseña<br>
			<input type="password" name="num" placeholder="Confirma la contraseña" id="txtpassword2" required="required" class="TT" />	
		</div>
		<div class="txt">
			El usuario es operador<br>
			<label class="fondotxt">
				<input type='radio' name='operador' value='1' ">Si
				<input type='radio' name='operador' value='0'  checked='checked'">No
			</label>
		</div>
		<div class="txt">
			El usuario es<br>
			<label class="fondotxt">
				<input type='radio' name='vendedor' value='4' ">Vendedor
				<input type='radio' name='vendedor' value='1' ">Cotizador
				<input type='radio' name='vendedor' value='2' ">Servicio a Ventas
				<input type='radio' name='vendedor' value='3' ">Supervisor
				<input type='radio' name='vendedor' value='0'  checked='checked'">N/A
			</label>
		</div-->
						
	</div> 
			<!--section id="titulo">
        			<center></br><h2>Seleccione los módulos de acceso del nuevo usuario</h2>
					 
				</center>
			</section>
			</br>
	<div class="myDiv" id="historia">
				

	</div-->
	<center>
	<div class="txt">
	<input type="button" name="insertar" Value="Guardar" onClick="saveoperador();" id="ok"/>
	</div>
	</center>
	</div>   
  </div>
  </div>
  
</div>
</main>
</body>
</html>
