<?php

//hola jvl hhh
require_once("dbControl.php");
require_once("usersFunction.php");

class appControlcotizador
{ 
	var $app;
	var $userfunctions;
	//var $path="C:/AppServ/www/PlanCtrol/documentos/";
	//var $path2="C:/AppServ/www/PlanCtrol";
	var $path="../documentos/";
	var $path2="..";
	var $frame=1;
	var $ruta="../documentoscotizaciones/";
	var $mermageneral=300;
	public function __construct()
		{
			$this->app = new dbControl();
			$this->userfunctions = new usersFunction();
		}
		
/////////////////////////////////////////////////////FUNCTIONS FOR COTIZADOR///////////////////////////////////////////////////
	////////////////////////////////////////////FUNCTIONS FOR PRODUCTS///////////////////////////////////////////
	public function getgiros(){
		if(!$this->userfunctions->islogged()){
			//$this->userfunctions->logout();
		}
		$datos=array();
		$result=$this->app->getrecord("giros","*","where activo like '1' ");
		for($a=0;$a<sizeof($result);$a++)
    		{
			$vec=$result[$a];	

			$datos[] = array(
					'id'=>$vec['id'],
					'nombre'=>$vec['nombre']				
					);			
                }
		
	return $datos;
	}

	public function getvendedores(){
		if(!$this->userfunctions->islogged()){
			//$this->userfunctions->logout();
		}
	$datos=array();
		$result=$this->app->getrecord("usuarios","usuarios.id,usuarios.operador,usuarios.vendedor,persona.nombre,persona.primerApellido,persona.segundoApellido,usuarios.usuario,usuarios.activo","INNER JOIN persona on persona.id=usuarios.idPersona where usuarios.vendedor like '4'");
		for($a=0;$a<sizeof($result);$a++)
    		{
			$vec=$result[$a];
//trigger_error($vec['id']);
			$datos[] = array(
				'id'=>$vec['id'],
				'nombre'=>$vec['nombre'],
				'appat'=>$vec['primerApellido'],
				'apmat'=>$vec['segundoApellido'],
				'usuario'=>$vec['usuario'],
				'activo'=>$vec['activo'],
				'operador'=>$vec['operador'],
				'vendedor'=>$vec['vendedor']
				);
		
                }
		
		
	return $datos;

	}

	public function saveclient($iduser,$razon,$giro,$domicilio,$nombre,$appat,$apmat,$telefono,$area,$puesto,$etiqueta,$plegadiza,$singleface,$cajaplastico,$display,$tiraje,$variable,$web,$ingenieria,$xmpie,$ventas,$costosevidentes,$costosnoevidentes,$participacion,$satisfaccion,$productos,$consumidor,$canal,$competencia,$vision,$estimacionventa,$nececidades,$estrategia,$proveedores,$espectativas,$exporta){
	if(!$this->userfunctions->islogged()){
			//$this->userfunctions->logout();
		}
	$datos=array();
		$idpersona=$this->app->saverecord('persona',"'$nombre','$appat','$apmat','$telefono','$area','$puesto','0','1'","(nombre,primerApellido,segundoApellido,telefono,area,puesto,enviarNotificacion,activo)");			
		$id=$this->app->saverecord("clientes","now(),$idpersona,$iduser,'$razon','$domicilio','$giro','$exporta','$etiqueta','$plegadiza','$singleface','$cajaplastico','$display','$tiraje','$variable','$web','$ingenieria','$xmpie','$ventas','$costosevidentes','$costosnoevidentes','$satisfaccion','$estimacionventa','$estrategia','$competencia','1'","(fechaRegistro,idPersona,idUsuario,razonSocial,domicilioFiscal,giro,exporta,etiqueta,plegadiza,singleFace,cajaPlastico,display,tiraje,datoVariable,web,disenio,xmpie,ventas,costosEvidentes,costosnoEvidentes,satisfaccionCliente,estimacionVentasanual,estrategia,competencia,activo)");
			if($id>0 ){
				
				$datos[]=array(
					'exito'=>'si',
					'folio'=>$id
				);	
			}else{
				$datos[]=array(
					'exito'=>'no',
					'folio'=>0
				);	
			}		

	return $datos;	
	}

	
	public function getclientes(){
	if(!$this->userfunctions->islogged()){
			//$this->userfunctions->logout();
		}
	$datos=array();
		
		
			$result=$this->app->getrecord("clientes","clientes.id,clientes.razonSocial,clientes.domicilioFiscal,concat(per2.nombre,' ',per2.primerApellido,' ',per2.segundoApellido) as 'vendedor',clientes.activo","INNER JOIN usuarios on usuarios.id=clientes.idUsuario INNER JOIN persona as per2 on per2.id=usuarios.idPersona");
			
			for($a=0;$a<sizeof($result);$a++){
				$vec=$result[$a];
			
			
				$datos[] = array(
					'id'=>$vec['id'],
					'razonsocial'=>$vec['razonSocial'],
					'domicilio'=>$vec['domicilioFiscal'],
					'vendedor'=>$vec['vendedor'],
					'activo'=>$vec['activo']
				);
				
			}
		
	
    
	return $datos;
	}
	public function getclientesenables(){
	if(!$this->userfunctions->islogged()){
			//$this->userfunctions->logout();
		}
	$datos=array();
		
		
			$result=$this->app->getrecord("clientes","clientes.id,per1.telefono,clientes.razonSocial,clientes.domicilioFiscal,concat(per2.nombre,' ',per2.primerApellido,' ',per2.segundoApellido) as 'vendedor',clientes.activo","INNER JOIN persona as per1 on per1.id=clientes.idPersona INNER JOIN usuarios on usuarios.id=clientes.idUsuario INNER JOIN persona as per2 on per2.id=usuarios.idPersona where clientes.activo like '1'");
			
			for($a=0;$a<sizeof($result);$a++){
				$vec=$result[$a];
			
			
				$datos[] = array(
					'id'=>$vec['id'],
					
					'razonsocial'=>$vec['razonSocial'],
					'domicilio'=>$vec['domicilioFiscal'],
					'vendedor'=>$vec['vendedor'],
					'activo'=>$vec['activo']
				);
				
			}
		
	
    
	return $datos;
	}

	public function getclientesbyvendedor($idvendedor){
	if(!$this->userfunctions->islogged()){
			//$this->userfunctions->logout();
		}
	$datos=array();
		
		
			$result=$this->app->getrecord("clientes","clientes.id,per1.telefono,clientes.razonSocial,clientes.domicilioFiscal,concat(per2.nombre,' ',per2.primerApellido,' ',per2.segundoApellido) as 'vendedor',clientes.activo","INNER JOIN persona as per1 on per1.id=clientes.idPersona INNER JOIN usuarios on usuarios.id=clientes.idUsuario INNER JOIN persona as per2 on per2.id=usuarios.idPersona where clientes.idUsuario=$idvendedor");
			
			for($a=0;$a<sizeof($result);$a++){
				$vec=$result[$a];
			
			
				$datos[] = array(
					'id'=>$vec['id'],
					'contacto'=>$vec['contacto'],
					'telefono'=>$vec['telefono'],
					'razonsocial'=>$vec['razonSocial'],
					'domicilio'=>$vec['domicilioFiscal'],
					'vendedor'=>$vec['vendedor'],
					'activo'=>$vec['activo']
				);
				
			}
		
	
    
	return $datos;
	}
	public function getclientesbyvendedorenables($idvendedor){
	if(!$this->userfunctions->islogged()){
			//$this->userfunctions->logout();
		}
	$datos=array();
		
		
			$result=$this->app->getrecord("clientes","per1.telefono,clientes.id,clientes.razonSocial,clientes.domicilioFiscal,concat(per2.nombre,' ',per2.primerApellido,' ',per2.segundoApellido) as 'vendedor',clientes.activo","INNER JOIN persona as per1 on per1.id=clientes.idPersona INNER JOIN usuarios on usuarios.id=clientes.idUsuario INNER JOIN persona as per2 on per2.id=usuarios.idPersona where clientes.idUsuario=$idvendedor and clientes.activo like '1'");
			
			for($a=0;$a<sizeof($result);$a++){
				$vec=$result[$a];
			
			
				$datos[] = array(
					'id'=>$vec['id'],
					
					'razonsocial'=>$vec['razonSocial'],
					'domicilio'=>$vec['domicilioFiscal'],
					'vendedor'=>$vec['vendedor'],
					'activo'=>$vec['activo']
				);
				
			}
		
	
    
	return $datos;
	}

	public function deletecliente($id){
	if(!$this->userfunctions->islogged()){
			//$this->userfunctions->logout();
		}
	//trigger_error($vendedor);
		$datos=array();
		$result=$this->app->getrecord("clientes","clientes.id,clientes.idPersona"," where clientes.id=$id");
		
		if (sizeof($result) > 0) {
			for($a=0;$a<sizeof($result);$a++)
    			{
				$vec=$result[$a];
			
				$idusuario=$vec['id'];
				$idpersona=$vec['idPersona'];
						
			
				if($this->app->deleterecord("clientes","where clientes.id=$id")>0){
					if($this->app->deleterecord("persona"," where persona.id=$idpersona")>0){
						$datos[]=array(
							'exito'=>'si'
						);
							
					}		
				}		
				
                	}	
		} 
		else{
			
			$datos[]=array(
					'exito'=>'sin'

				);
		}


	return $datos;	
	}
	public function getspecificclient($id){
	if(!$this->userfunctions->islogged()){
			//$this->userfunctions->logout();
		}
	$datos=array();
		
		
			$result=$this->app->getrecord("clientes","*"," where clientes.id=$id");
			
			for($a=0;$a<sizeof($result);$a++){
				$vec=$result[$a];
				
				$vec2=$result2[0];
				$datos[] = array(
					'id'=>$vec['id'],
					
					'razonsocial'=>$vec['razonSocial'],
					'domicilio'=>$vec['domicilioFiscal'],
					'giro'=>$vec['giro'],
					'exporta'=>$vec['exporta'],
					'etiqueta'=>$vec['etiqueta'],
					'carton'=>$vec['plegadiza'],
					'corrugado'=>$vec['singleFace'],
					'caja'=>$vec['cajaPlastico'],
					'display'=>$vec['display'],
					'tiraje'=>$vec['tiraje'],
					'datovariable'=>$vec['datoVariable'],
					'metalizado'=>$vec['web'],
					'diseno'=>$vec['disenio'],
					'web'=>$vec['xmpie'],
					'ventas'=>$vec['ventas'],
					'costosdirectos'=>$vec['costosEvidentes'],
					'costosnoevidentes'=>$vec['costosnoEvidentes'],
					'participacion'=>$vec['participacion'],		
					'satisfaccioncliente'=>$vec['satisfaccionCliente'], 
					'productos'=>$vec['productos'],
					'consumidor'=>$vec['consumidor'],
					'canal'=>$vec['canal'],
					'competencia'=>$vec['competencia'],
					'vision'=>$vec['vision'],
					'ventasestimacion'=>$vec['estimacionVentasanual'],
					'necesidades'=>$vec['necesidades'],
					'problematica'=>$vec['estrategia'],
					'proveedores'=>$vec['proveedores'],
					'expectativas'=>$vec['expectativas'],
					'activo'=>$vec['activo']
				);
				
			}
		
	
    
	return $datos;
	}

	public function updateclient ($idclienteedit,$razon,$giro,$domicilio,$nombre,$appat,$apmat,$telefono,$area,$puesto,$etiqueta,$plegadiza,$singleface,$cajaplastico,$display,$tiraje,$variable,$web,$ingenieria,$xmpie,$ventas,$costosevidentes,$costosnoevidentes,$participacion,$satisfaccion,$productos,$consumidor,$canal,$competencia,$vision,$estimacionventa,$nececidades,$estrategia,$proveedores,$espectativas,$exporta,$activo){
		if(!$this->userfunctions->islogged()){
			//$this->userfunctions->logout();
		}
		//trigger_error($idclienteedit);
		$datos=array();
		$result=$this->app->getrecord("clientes","*"," where id= $idclienteedit");
		
		if (sizeof($result) > 0) {
			$vec=$result[0];
			if($this->app->updaterecord("clientes","razonSocial='$razon',domicilioFiscal='$domicilio',giro='$giro',exporta='$exporta',etiqueta='$etiqueta',plegadiza='$plegadiza',singleFace='$singleface',cajaPlastico='$cajaplastico',display='$display',tiraje='$tiraje',datoVariable='$variable',web='$web',disenio='$ingenieria',xmpie='$xmpie',ventas='$ventas',costosEvidentes='$costosevidentes',costosnoEvidentes='$costosnoevidentes',participacion='$participacion',satisfaccionCliente='$satisfaccion',productos='$productos',consumidor='$consumidor',canal='$canal',competencia='$competencia',vision='$vision',estimacionVentasanual='$estimacionventa',necesidades='$nececidades',estrategia='$estrategia',proveedores='$proveedores',expectativas='$espectativas',activo='$activo'"," where id=$idclienteedit")){
				
					$datos[]=array(
						'exito'=>'si',
						'folio'=>$idclienteedit
					);
							
			}	
				
		} 
		else{
			
			$datos[]=array(
					'exito'=>'sin',
					'folio'=>0
				);
		}
	return $datos;	

	}
////////////////////////////////////////////////////////////Funtions for contacto del cliente/////////////////////////////////////////

	public function savecontacto($idcliente,$nombre,$appat,$apmat,$telefono,$extension,$celular,$correo,$area,$puesto){
	if(!$this->userfunctions->islogged()){
			//$this->userfunctions->logout();
		}
	$datos=array();
		$result=$this->app->getrecord("persona","*","where telefono like '$celular'");
				
		if (sizeof($result) > 0) {
			$datos[]=array(
				'exito'=>'ya',
				'folio'=>0
			);
		} 
		else 
		{
			
						
			$id=$this->app->saverecord("persona","'$nombre','$appat','$apmat','$celular','$correo','$area','$puesto','1'","(nombre,primerApellido,segundoApellido,telefono,correo,area,puesto,activo)");			
			if($id>0 ){
				$id=$this->app->saverecord("rel_clientecontacto","$idcliente,$id,'$telefono','$extension','1'","(idCliente,idPersona,telefono,extension,activo)");	
				$datos[]=array(
					'exito'=>'si',
					'folio'=>$id
				);	
			}else{
				$datos[]=array(
					'exito'=>'no',
					'folio'=>0
				);	
			}			
		}


		
	return $datos;
	} 
	public function getallcontactos($idcliente){
	if(!$this->userfunctions->islogged()){
			//$this->userfunctions->logout();
		}
	$datos=array();
		
		
			$result=$this->app->getrecord("rel_clientecontacto","rel_clientecontacto.id,concat(persona.nombre,' ',persona.primerApellido,' ',persona.segundoApellido) as 'nombre',persona.telefono as 'celular',persona.area,persona.puesto,persona.correo,rel_clientecontacto.telefono,rel_clientecontacto.extension "," INNER JOIN persona on persona.id=rel_clientecontacto.idPersona where rel_clientecontacto.idCliente=$idcliente");
			
			for($a=0;$a<sizeof($result);$a++){
				$vec=$result[$a];
				
				$vec2=$result2[0];
				$datos[] = array(
					'id'=>$vec['id'],
					'nombre'=>$vec['nombre'],
					'celular'=>$vec['celular'],
					'area'=>$vec['area'],
					'puesto'=>$vec['puesto'],
					'correo'=>$vec['correo'],
					'telefono'=>$vec['telefono'],
					'extension'=>$vec['extension'],
					
				);
				
			}
		
	
    
	return $datos;	
	}

	public function updatecontacto($id,$nombre,$appat,$apmat,$telefono,$extension,$celular,$correo,$area,$puesto){
		if(!$this->userfunctions->islogged()){
			//$this->userfunctions->logout();
		}
		$datos=array();
		$result=$this->app->getrecord("rel_clientecontacto","*"," where id=$id");
		
		if (sizeof($result) > 0) {
			$vec=$result[0];
			$idpersona=$vec['idPersona'];
			if($this->app->updaterecord("rel_clientecontacto","telefono='$telefono',extension='$extension',activo='$activo'"," where id=$id")){
				$this->app->updaterecord("persona","nombre='$nombre',primerApellido='$appat',segundoApellido='$apmat',telefono='$celular',correo='$correo',area='$area',puesto='$puesto'"," where id=$idpersona");
	
					$datos[]=array(
						'exito'=>'si',
						'folio'=>$idclienteedit
					);
							
			}	
				
		} 
		else{
			
			$datos[]=array(
					'exito'=>'sin',
					'folio'=>0
				);
		}
	return $datos;	

	}
////////////////////////////////////////////////////////FUNCTIONS FOR PRODUCTS/////////////////////////////////////////////////////////

	public function savecotizacionproducto($iduser,$cliente,$tipo,$tipoproducto,$idunico,$ancho,$largo,$alto,$vtaalargo,$vtaaalto,$vtablargo,$vtabalto,$categoria,$sustrato,$sustratoindirecto,$codigo,$tipotinta,$tintafrente,$tintareverso,$barniz,$laminado,$arte,$artefrente,$artevuelta,$pegado,$suaje,$pantones,$especpantone,$empalme,$costo,$especempaque,$notas,$formapago,$formaentrega,$margen,$vigencia,$escalauno,$escalados,$escalatres,$escalacuatro,$escalacinco,$escalaseis,$herramental,$dummy,$justificaciondummy){
		if(!$this->userfunctions->islogged()){
			//$this->userfunctions->logout();
		}
		$datos=array();
		
		$id=$this->app->saverecord("cotizaciones","null,'$cliente','$tipo','$tipoproducto','$idunico',1,$iduser,now(),'','1'");
			if($id>0 ){
				$this->app->saverecord("dimensiones","null,$id,$ancho,$largo,$alto,$vtaalargo,$vtaaalto,$vtablargo,$vtabalto,'1'");
				$this->app->saverecord("sustratos","null,$id,'$categoria','$sustrato','$sustratoindirecto','$codigo','1'");
				$this->app->saverecord("tintas","null,$id,'$tipotinta','$tintafrente','$tintareverso','$barniz','$laminado',$arte,$artefrente,$artevuelta,'$pegado','$suaje',$pantones,'$especpantone','$empalme',$costo,'$especempaque','$notas','1'");
				$this->app->saverecord("escalasvolumen","null,$id,'$escalauno','$escalados','$escalatres','$escalacuatro','$escalacinco','$escalaseis','$herramental','$dummy','$justificaciondummy','1'");
				$this->app->saverecord("terminoscondiciones","null,$id,'$formapago','$formaentrega','$margen','$vigencia','1'");
				$datos[]=array(
					'exito'=>'si',
					'folio'=>$id
				);	
			}else{
				$datos[]=array(
					'exito'=>'no',
					'folio'=>0
				);	
			}		

	return $datos;
	
	}

	public function gettipoproductos(){
	if(!$this->userfunctions->islogged()){
			//$this->userfunctions->logout();
		}
	$datos=array();
		
		
			$result=$this->app->getrecord("tipoproducto","*"," where status=1 order by nombre ASC");
			
			for($a=0;$a<sizeof($result);$a++){
				$vec=$result[$a];
			
			
				$datos[] = array(
					'id'=>$vec['id'],
					'nombre'=>$vec['nombre'],
					);
				
			}
		
	
    
	return $datos;
	}
	public function getcategoriascomplementos(){
	if(!$this->userfunctions->islogged()){
			//$this->userfunctions->logout();
		}
	$datos=array();
		
		
			$result=$this->app->getrecord("sustratobase","DISTINCT(tipo) as 'nombre'"," where activo like '1' order by nombre ASC");
			
			for($a=0;$a<sizeof($result);$a++){
				$vec=$result[$a];
			
			
				$datos[] = array(
					'id'=>$vec['id'],
					'nombre'=>$vec['nombre'],
					);
				
			}
		
	
    
	return $datos;
	}
	public function gettiposustrato($id){
	if(!$this->userfunctions->islogged()){
			//$this->userfunctions->logout();
		}
	$datos=array();
		
		
			$result=$this->app->getrecord("sustratobase","*"," where tipo like '$id' and activo like '1' order by nombre ASC");
			
			for($a=0;$a<sizeof($result);$a++){
				$vec=$result[$a];
			
			
				$datos[] = array(
					'id'=>$vec['id'],
					'nombre'=>$vec['nombre'],
					);
				
			}
		
	
    
	return $datos;
	}

	public function gettipotinta(){
	if(!$this->userfunctions->islogged()){
			//$this->userfunctions->logout();
		}
	$datos=array();
		
		
			$result=$this->app->getrecord("tipotintas","*"," where activo like '1' order by nombre ASC");
			
			for($a=0;$a<sizeof($result);$a++){
				$vec=$result[$a];
			
			
				$datos[] = array(
					'id'=>$vec['id'],
					'nombre'=>$vec['nombre'],
					);
				
			}
		
	
    
	return $datos;
	}

	public function gettipobarniz(){
	if(!$this->userfunctions->islogged()){
			//$this->userfunctions->logout();
		}
	$datos=array();
		
		
			$result=$this->app->getrecord("tipobarniz","*"," where activo like '1' order by nombre ASC");
			
			for($a=0;$a<sizeof($result);$a++){
				$vec=$result[$a];
			
			
				$datos[] = array(
					'id'=>$vec['id'],
					'nombre'=>$vec['nombre'],
					);
				
			}
		
	
    
	return $datos;
	}
	public function gettipolaminado(){
	if(!$this->userfunctions->islogged()){
			//$this->userfunctions->logout();
		}
	$datos=array();
		
		
			$result=$this->app->getrecord("tipolaminado","*"," where activo like '1' order by nombre ASC");
			
			for($a=0;$a<sizeof($result);$a++){
				$vec=$result[$a];
			
			
				$datos[] = array(
					'id'=>$vec['id'],
					'nombre'=>$vec['nombre'],
					);
				
			}
		
	
    
	return $datos;
	}
	public function gettipopegado(){
	if(!$this->userfunctions->islogged()){
			//$this->userfunctions->logout();
		}
	$datos=array();
		
		
			$result=$this->app->getrecord("tipopegado","*"," where activo like '1' order by nombre ASC");
			
			for($a=0;$a<sizeof($result);$a++){
				$vec=$result[$a];
			
			
				$datos[] = array(
					'id'=>$vec['id'],
					'nombre'=>$vec['nombre'],
					);
				
			}
		
	
    
	return $datos;
	}
	public function gettiposuaje(){
	if(!$this->userfunctions->islogged()){
			//$this->userfunctions->logout();
		}
	$datos=array();
		
		
			$result=$this->app->getrecord("tiposuaje","*"," where activo like '1' order by nombre ASC");
			
			for($a=0;$a<sizeof($result);$a++){
				$vec=$result[$a];
			
			
				$datos[] = array(
					'id'=>$vec['id'],
					'nombre'=>$vec['nombre'],
					);
				
			}
		
	
    
	return $datos;
	}
	public function gettipoempalme(){
	if(!$this->userfunctions->islogged()){
			//$this->userfunctions->logout();
		}
	$datos=array();
		
		
			$result=$this->app->getrecord("tipoempalme","*"," where activo like '1' order by nombre ASC");
			
			for($a=0;$a<sizeof($result);$a++){
				$vec=$result[$a];
			
			
				$datos[] = array(
					'id'=>$vec['id'],
					'nombre'=>$vec['nombre'],
					);
				
			}
		
	
    
	return $datos;
	}
	public function gettipopago(){
	if(!$this->userfunctions->islogged()){
			//$this->userfunctions->logout();
		}
	$datos=array();
		
		
			$result=$this->app->getrecord("complementos","*"," where categoria=12 and status=1 order by nombre ASC");
			
			for($a=0;$a<sizeof($result);$a++){
				$vec=$result[$a];
			
			
				$datos[] = array(
					'id'=>$vec['id'],
					'nombre'=>$vec['nombre'],
					);
				
			}
		
	
    
	return $datos;
	}
	public function gettipoentrega(){
	if(!$this->userfunctions->islogged()){
			//$this->userfunctions->logout();
		}
	$datos=array();
		
		
			$result=$this->app->getrecord("complementos","*"," where categoria=18 and status=1 order by nombre ASC");
			
			for($a=0;$a<sizeof($result);$a++){
				$vec=$result[$a];
			
			
				$datos[] = array(
					'id'=>$vec['id'],
					'nombre'=>$vec['nombre'],
					);
				
			}
		
	
    
	return $datos;
	}
	public function getmargen(){
	if(!$this->userfunctions->islogged()){
			//$this->userfunctions->logout();
		}
	$datos=array();
		
		
			$result=$this->app->getrecord("complementos","*"," where categoria=19 and status=1");
			
			for($a=0;$a<sizeof($result);$a++){
				$vec=$result[$a];
			
			
				$datos[] = array(
					'id'=>$vec['id'],
					'nombre'=>$vec['nombre'],
					);
				
			}
		
	
    
	return $datos;
	}
	public function getvigencia(){
	if(!$this->userfunctions->islogged()){
			//$this->userfunctions->logout();
		}
	$datos=array();
		
		
			$result=$this->app->getrecord("complementos","*"," where categoria=13 and status=1 order by nombre ASC");
			
			for($a=0;$a<sizeof($result);$a++){
				$vec=$result[$a];
			
			
				$datos[] = array(
					'id'=>$vec['id'],
					'nombre'=>$vec['nombre'],
					);
				
			}
		
	
    
	return $datos;
	}
	public function getescala(){
	if(!$this->userfunctions->islogged()){
			//$this->userfunctions->logout();
		}
	$datos=array();
		
		
			$result=$this->app->getrecord("complementos","*"," where categoria=8 and status=1 order by nombre ASC");
			
			for($a=0;$a<sizeof($result);$a++){
				$vec=$result[$a];
			
			
				$datos[] = array(
					'id'=>$vec['id'],
					'nombre'=>$vec['nombre'],
					);
				
			}
		
	
    
	return $datos;
	}
	public function getherramental(){
	if(!$this->userfunctions->islogged()){
			//$this->userfunctions->logout();
		}
	$datos=array();
		
		
			$result=$this->app->getrecord("complementos","*"," where categoria=14 and status=1 order by nombre ASC");
			
			for($a=0;$a<sizeof($result);$a++){
				$vec=$result[$a];
			
			
				$datos[] = array(
					'id'=>$vec['id'],
					'nombre'=>$vec['nombre'],
					);
				
			}
		
	
    
	return $datos;
	}
	public function gettipodummy(){
	if(!$this->userfunctions->islogged()){
			//$this->userfunctions->logout();
		}
	$datos=array();
		
		
			$result=$this->app->getrecord("complementos","*"," where categoria=15 and status=1 order by nombre ASC");
			
			for($a=0;$a<sizeof($result);$a++){
				$vec=$result[$a];
			
			
				$datos[] = array(
					'id'=>$vec['id'],
					'nombre'=>$vec['nombre'],
					);
				
			}
		
	
    
	return $datos;
	}
	public function getallcodes($tipo){
		if(!$this->userfunctions->islogged()){
			//$this->userfunctions->logout();
		}
	$datos=array();
		
		
			$result=$this->app->getrecord("codigosecmafefco","*"," where tipo like '$tipo' and activo like '1' order by nombre ASC");
			
			for($a=0;$a<sizeof($result);$a++){
				$vec=$result[$a];
			
			
				$datos[] = array(
					'id'=>$vec['id'],
					'nombre'=>$vec['nombre'],
					);
				
			}
		
	
    
	return $datos;
	}
	public function getcategoriescodes(){
		if(!$this->userfunctions->islogged()){
			//$this->userfunctions->logout();
		}
	$datos=array();
		
		
			$result=$this->app->getrecord("codigosecmafefco","DISTINCT(tipo) as 'nombre'","order by nombre ASC");
			
			for($a=0;$a<sizeof($result);$a++){
				$vec=$result[$a];
			
			
				$datos[] = array(
					'id'=>$vec['id'],
					'nombre'=>$vec['nombre'],
					);
				
			}
		
	
    
	return $datos;
	}

	public function getserigrafia(){
		if(!$this->userfunctions->islogged()){
			//$this->userfunctions->logout();
		}
	$datos=array();
		
		
			$result=$this->app->getrecord("serigrafia","*"," where activo like '1' order by nombre ASC");
			
			for($a=0;$a<sizeof($result);$a++){
				$vec=$result[$a];
			
			
				$datos[] = array(
					'id'=>$vec['id'],
					'nombre'=>$vec['nombre'],
					);
				
			}
		
	
    
	return $datos;
	}
	public function materialventana(){
		if(!$this->userfunctions->islogged()){
			//$this->userfunctions->logout();
		}
	$datos=array();
		
		
			$result=$this->app->getrecord("materialventanas","*"," where activo like '1' order by nombre ASC");
			
			for($a=0;$a<sizeof($result);$a++){
				$vec=$result[$a];
			
			
				$datos[] = array(
					'id'=>$vec['id'],
					'nombre'=>$vec['nombre'],
					);
				
			}
		
	
    
	return $datos;
	}
	public function embalaje($tipo){
		if(!$this->userfunctions->islogged()){
			//$this->userfunctions->logout();
		}
	$datos=array();
		
		if($tipo=="inner"){
			$result=$this->app->getrecord("embalajes","*"," where activo like '1' and tipo like 'inner' order by nombre ASC");
			
			for($a=0;$a<sizeof($result);$a++){
				$vec=$result[$a];
			
			
				$datos[] = array(
					'id'=>$vec['id'],
					'nombre'=>$vec['nombre'],
					);
				
			}
		}
	
    		if($tipo=="master"){
			$result=$this->app->getrecord("embalajes","*"," where activo like '1' and tipo like 'master' order by nombre ASC");
			
			for($a=0;$a<sizeof($result);$a++){
				$vec=$result[$a];
			
			
				$datos[] = array(
					'id'=>$vec['id'],
					'nombre'=>$vec['nombre'],
					);
				
			}
		}
	return $datos;

	} 
	public function numerostintas($tipo){
		if(!$this->userfunctions->islogged()){
			//$this->userfunctions->logout();
		}
	$datos=array();
		
		
			$result=$this->app->getrecord("numerostintas","*"," where activo like '1' and tipo like '$tipo' order by nombre ASC");
			
			for($a=0;$a<sizeof($result);$a++){
				$vec=$result[$a];
			
			
				$datos[] = array(
					'id'=>$vec['id'],
					'nombre'=>$vec['nombre'],
					);
				
			}
		
	
    
	return $datos;

	}
	public function getmanuales(){
	if(!$this->userfunctions->islogged()){
			//$this->userfunctions->logout();
		}
		$datos=array();
		
			$result3=$this->app->getrecord("manuales","*"," where activo like '1' order by descripcion asc");
			for($aa=0;$aa<sizeof($result3);$aa++)
    			{	$vec3=$result3[$aa];
				$datos[]=array(
					'id'=>$vec3['id'],
					'descripcion'=>$vec3['descripcion'],
					'ruta'=>$vec3['ruta']
					);
						
			}	

	return $datos;
	}
	public function getresponsables(){
	if(!$this->userfunctions->islogged()){
			//$this->userfunctions->logout();
		}
		$datos=array();
		
			$result3=$this->app->getrecord("usuarios","CONCAT(persona.nombre,' ',persona.primerApellido,' ',persona.segundoApellido) as 'responsable',usuarios.id ","inner join persona on persona.id=usuarios.idPersona where usuarios.vendedor like '1' and usuarios.activo like '1'");
			for($aa=0;$aa<sizeof($result3);$aa++)
    			{	$vec3=$result3[$aa];
				$datos[]=array(
					'id'=>$vec3['id'],
					'nombre'=>$vec3['responsable']
					);
						
			}	

	return $datos;
	}
	//////////////////////////////////////////////FUNCTIONS FOR  SAVE QUOTATIONS/////////////////////////////////////

	public function savecotizacionsec1($iduser,$cliente,$tipoproducto,$codigounico){
	if(!$this->userfunctions->islogged()){
			//$this->userfunctions->logout();
		}
	$datos=array();
		
		$id=$this->app->saverecord("cotizaciones","'$cliente','$tipoproducto','$codigounico',1,2,$iduser,now(),'1'","(cliente, tipoProducto, idUnico, idStatusCotizacion, idResponsable,idUsuario, fechaElaboracion, activo) ");
			if($id>0 ){
				
				$datos[]=array(
					'exito'=>'si',
					'folio'=>$id
				);	
			}else{
				$datos[]=array(
					'exito'=>'no',
					'folio'=>0
				);	
			}		

	return $datos;	

	}

	public function savecotizacionsec2($idcotizacion,$a,$b,$h,$x,$y,$z,$o,$p,$q,$r,$s,$t,$codigo,$notas){
	if(!$this->userfunctions->islogged()){
			//$this->userfunctions->logout();
		}
	$datos=array();
		
		$id=$this->app->saverecord("dimensiones","$idcotizacion,$a,$b,$h,$x,$y,$z,$o,$p,$q,$r,$s,$t,'$notas','$codigo','1'","(idCotizacion,a,b,h,x,y,z,o,p,q,r,s,t,descripcion,codigo,activo) ");
			if($id>0 ){
				
				$datos[]=array(
					'exito'=>'si',
					'folio'=>$id
				);	
			}else{
				$datos[]=array(
					'exito'=>'no',
					'folio'=>0
				);	
			}		

	return $datos;	

	}
	

	public function savecotizacionsec3($idcotizacion,$nomseccion,$categoria,$sustrato,$empalme,$suaje,$pegado,$tipotintafrente,$tipotintareverso,$tintafrente,$tintareverso,$nompantonesfrente,$nompantonesreverso,$pantonesfrente,$pantonesreverso,$tipobarnizfrente,$tipobarnizreverso,$barnizfrente,$barnizreverso,$tipolaminadofrente,$tipolaminadoreverso,$laminadofrente,$laminadoreverso,$tiposerigrafiafrente,$tiposerigrafiareverso,$serigrafiafrente,$serigrafiareverso,$artefrente,$artereverso,$ventanamaterial,$ventanalargo,$ventanaancho){
	if(!$this->userfunctions->islogged()){
			//$this->userfunctions->logout();
		}
	$datos=array();
		
		$id=$this->app->saverecord("especificacionescotizacion","$idcotizacion,'$categoria','$sustrato','$empalme','$suaje','$pegado','1'","(idCotizacion,categoria,sustratoBase,empalme,suaje,pegado,activo) ");
			if($id>0 ){
				$this->app->saverecord("secciones","$idcotizacion,'$nomseccion','$tipotintafrente','$tipotintareverso',$tintafrente,$tintareverso,'$nompantonesfrente','$nompantonesreverso',$pantonesfrente,$pantonesreverso,'$tipobarnizfrente','$tipobarnizreverso',$barnizfrente,$barnizreverso,'$tipolaminadofrente','$tipolaminadoreverso',$laminadofrente,$laminadoreverso,'$tiposerigrafiafrente','$tiposerigrafiareverso','$serigrafiafrente','$serigrafiareverso',$artefrente,$artereverso,'1'","(idCotizacion,nombreSeccion,tipoTintaFrente,tipoTintaReverso,frenteTinta,reversoTinta,nomPantonesFrente,nomPantonesReverso,frentePantone,reversoPantone,tipoBarnizFrente,tipoBarnizReverso,frenteBarniz,reversoBarniz,tipoLaminadoFrente,tipoLaminadoReverso,frenteLaminado,reversoLaminado,tipoSerigrafiaFrente,tipoSerigrafiaReverso,frenteSerigrafia,reversoSerigrafia,cambiosArteFrente,cambiosArteReverso,activo) ");
				$this->app->saverecord("ventanas","$idcotizacion,'$ventanamaterial',$ventanalargo,$ventanaancho,'1'","(idCotizacion,material,ladoA,ladoB,activo) ");
				$datos[]=array(
					'exito'=>'si',
					'folio'=>$id
				);	
			}else{
				$datos[]=array(
					'exito'=>'no',
					'folio'=>0
				);	
			}		

	return $datos;	
	

	}
	public function savecotizacionsec4($idcotizacion,$herramental,$separarcambiosarte,$web2print,$dummy,$escala1,$escala2,$escala3,$escala4,$escala5,$escala6,$costo,$margen,$vigencia,$formapago,$formaentrega,$inner,$cantidadinner,$master,$cantidadmaster){
	if(!$this->userfunctions->islogged()){
			//$this->userfunctions->logout();
		}
	$datos=array();
		
		$id=$this->app->saverecord("terminosventa","$idcotizacion,'$herramental','$separarcambiosarte','$dummy','$web2print',$escala1,$escala2,$escala3,$escala4,$escala5,$escala6,'$inner',$cantidadinner,'$master',$cantidadmaster,'$vigencia','$formapago','$formaentrega',$costo,'$margen','1'","(idCotizacion,herramental,separarCambiosArte,dummy,webtoPrint,volumen1,volumen2,volumen3,volumen4,volumen5,volumen6,tipoInner,multiploInner,tipoMaster,multiploMaster,vigenciaCotizacion,formaPago,flete,costoObjetivo,margenGanancia,activo) ");
			if($id>0 ){
				$datos[]=array(
					'exito'=>'si',
					'folio'=>$id
				);	
			}else{
				$datos[]=array(
					'exito'=>'no',
					'folio'=>0
				);	
			}		

	return $datos;	
	

	
	}

	
	
///////////////////////////////////////////////////GENERAL FUNCTIONS FOR COTIZATOR////////////////////////////////////////////////////////////




	public function getallquotations($isvendedor,$iduser){
	if(!$this->userfunctions->islogged()){
			//$this->userfunctions->logout();
		}
	$datos=array();
		
		if($isvendedor==4){
			$result=$this->app->getrecord("cotizaciones","cotizaciones.id,cotizaciones.cliente,cotizaciones.tipo,cotizaciones.tipoProducto,cotizaciones.fechaElaboracion,cotizaciones.fechaCotizacion,cotizaciones.fechaPedido,cotizaciones.dias,cotizaciones.pedidoSAE,cotizaciones.rutaCotizacion,status_cotizacion.status,concat(per1.nombre,' ',per1.primerApellido,' ',per1.segundoApellido) as 'usuario',concat(per2.nombre,' ',per2.primerApellido,' ',per2.segundoApellido) as 'responsable'","inner JOIN status_cotizacion on cotizaciones.idStatuscotizacion=status_cotizacion.id INNER JOIN usuarios as user1 on cotizaciones.idUsuario=user1.id INNER JOIN persona as per1 on per1.id=user1.idPersona  INNER JOIN usuarios as user2 on cotizaciones.idResponsable=user2.id INNER JOIN persona as per2 on per2.id=user2.idPersona  where cotizaciones.activo like '1' and cotizaciones.idUsuario=$iduser  order by cotizaciones.fechaElaboracion desc ");
		}else if($isvendedor==1){
			$result=$this->app->getrecord("cotizaciones","cotizaciones.id,cotizaciones.cliente,cotizaciones.tipo,cotizaciones.tipoProducto,cotizaciones.fechaElaboracion,cotizaciones.fechaCotizacion,cotizaciones.fechaPedido,cotizaciones.dias,cotizaciones.pedidoSAE,cotizaciones.rutaCotizacion,status_cotizacion.status,concat(per1.nombre,' ',per1.primerApellido,' ',per1.segundoApellido) as 'usuario',concat(per2.nombre,' ',per2.primerApellido,' ',per2.segundoApellido) as 'responsable'","inner JOIN status_cotizacion on cotizaciones.idStatuscotizacion=status_cotizacion.id INNER JOIN usuarios as user1 on cotizaciones.idUsuario=user1.id INNER JOIN persona as per1 on per1.id=user1.idPersona  INNER JOIN usuarios as user2 on cotizaciones.idResponsable=user2.id INNER JOIN persona as per2 on per2.id=user2.idPersona  where cotizaciones.activo like '1' and cotizaciones.idResponsable=$iduser order by cotizaciones.fechaElaboracion desc ");
		}else if($isvendedor==2){
			$result=$this->app->getrecord("cotizaciones","cotizaciones.id,cotizaciones.cliente,cotizaciones.tipo,cotizaciones.tipoProducto,cotizaciones.fechaElaboracion,cotizaciones.fechaCotizacion,cotizaciones.fechaPedido,cotizaciones.dias,cotizaciones.pedidoSAE,cotizaciones.rutaCotizacion,status_cotizacion.status,concat(per1.nombre,' ',per1.primerApellido,' ',per1.segundoApellido) as 'usuario',concat(per2.nombre,' ',per2.primerApellido,' ',per2.segundoApellido) as 'responsable'","inner JOIN status_cotizacion on cotizaciones.idStatuscotizacion=status_cotizacion.id INNER JOIN usuarios as user1 on cotizaciones.idUsuario=user1.id INNER JOIN persona as per1 on per1.id=user1.idPersona  INNER JOIN usuarios as user2 on cotizaciones.idResponsable=user2.id INNER JOIN persona as per2 on per2.id=user2.idPersona  where cotizaciones.activo like '1' and cotizaciones.pedidoSAE=0  order by cotizaciones.fechaElaboracion desc");
		}else{
			$result=$this->app->getrecord("cotizaciones","cotizaciones.id,cotizaciones.cliente,cotizaciones.tipo,cotizaciones.tipoProducto,cotizaciones.fechaElaboracion,cotizaciones.fechaCotizacion,cotizaciones.fechaPedido,cotizaciones.dias,cotizaciones.pedidoSAE,cotizaciones.rutaCotizacion,status_cotizacion.status,concat(per1.nombre,' ',per1.primerApellido,' ',per1.segundoApellido) as 'usuario',concat(per2.nombre,' ',per2.primerApellido,' ',per2.segundoApellido) as 'responsable'","inner JOIN status_cotizacion on cotizaciones.idStatuscotizacion=status_cotizacion.id INNER JOIN usuarios as user1 on cotizaciones.idUsuario=user1.id INNER JOIN persona as per1 on per1.id=user1.idPersona  INNER JOIN usuarios as user2 on cotizaciones.idResponsable=user2.id INNER JOIN persona as per2 on per2.id=user2.idPersona  where cotizaciones.activo like '1' order by cotizaciones.fechaElaboracion desc ");
		}

		for($a=0;$a<sizeof($result);$a++){
			$vec=$result[$a];
			
			
			$datos[] = array(
				'id'=>$vec['id'],
				'cliente'=>$vec['cliente'],
				'tipo'=>$vec['tipo'],
				'tipoproducto'=>$vec['tipoProducto'],
				'fechaelaboracion'=>$vec['fechaElaboracion'],
				'fechacotizacion'=>$vec['fechaCotizacion'],
				'fechapedido'=>$vec['fechaPedido'],
				'dias'=>$vec['dias'],
				'pedido'=>$vec['pedidoSAE'],
				'ruta'=>$vec['rutaCotizacion'],
				'status'=>$vec['status'],
				'usuario'=>$vec['usuario'],
				'responsable'=>$vec['responsable'],
			);
				
		}
	return $datos;
	}
	public function	updatecotizacionapedido($id,$pedido){
		if(!$this->userfunctions->islogged()){
			
		}
		date_default_timezone_set ("America/Mexico_City");
	//trigger_error($vendedor);
		$datos=array();
		$result=$this->app->getrecord("cotizaciones","*"," where id= $id");
		
			


		if (sizeof($result) > 0) {

			$vec=$result[0];

			
			$dat=explode(" ",$vec['fechaElaboracion']);
			 $datetime1 = date_create($dat[0]);
       			$datetime2 = date_create(date("Y-m-d"));
    
    			$interval = date_diff($datetime1, $datetime2);
    			//trigger_error($dat[0].$interval->d);
			//trigger_error($interval->i);
			//trigger_error($interval->h);
			
			$dias=$interval->d; 	
			if($this->app->updaterecord("cotizaciones","pedidoSAE=$pedido,dias=$dias,fechaPedido=now(),idStatuscotizacion=5"," where id=$id")){
					$datos[]=array(
						'exito'=>'si',
						'folio'=>$id
					);
							
			}	
				
		} 
		else{
			
			$datos[]=array(
					'exito'=>'sin',
					'folio'=>0
				);
		}
	return $datos;
	}
	public function	updatecotizacionresponsable($id,$responsable){
		if(!$this->userfunctions->islogged()){
			
		}
		$datos=array();
		$result=$this->app->getrecord("cotizaciones","*"," where id= $id");
		
			


		if (sizeof($result) > 0) {

			
			if($this->app->updaterecord("cotizaciones","idResponsable=$responsable"," where id=$id")){
					$datos[]=array(
						'exito'=>'si',
						'folio'=>$id
					);
							
			}	
				
		} 
		else{
			
			$datos[]=array(
					'exito'=>'sin',
					'folio'=>0
				);
		}
	return $datos;
	}
	public function getfilesquotation($idcotizacion){
	if(!$this->userfunctions->islogged()){
			//$this->userfunctions->logout();
		}
		$datos=array();
		
			$result3=$this->app->getrecord("rel_cotizacionarchivo","*"," where idCotizacion= $idcotizacion");
			for($aa=0;$aa<sizeof($result3);$aa++)
    			{	$vec3=$result3[$aa];
				$datos[]=array(
					'id'=>$vec3['id'],
					'descripcion'=>$vec3['descripcion'],
					'extra'=>$vec3['extra'],
					'ruta'=>$vec3['ruta']
					);
						
			}	

	return $datos;
	}
	
	public function deletefilequotation($id){
	if(!$this->userfunctions->islogged()){
			//$this->userfunctions->logout();
		}
		$datos=array();
		$result=$this->app->getrecord("rel_cotizacionarchivo","*"," where id=$id");
		
		if (sizeof($result) > 0) 
		{		$vec=$result[0];
			unlink($this->path2.$vec['ruta']);
			if($this->app->deleterecord("rel_cotizacionarchivo"," where id=$id")){
					$datos[]=array(
						'exito'=>'si'
					);					
			}		
				
		} 
		else{
			
			$datos[]=array(
					'exito'=>'sin'

				);
		}


	return $datos;		
	}

	public function deletequotation($id,$tipo){
	if(!$this->userfunctions->islogged()){
			//$this->userfunctions->logout();
		}
	$datos=array();
		$result=$this->app->getrecord("cotizaciones","*"," where id=$id");
		
		if (sizeof($result) > 0) 
		{		
			if($this->app->deleterecord("cotizaciones"," where id=$id")>0){

					switch($tipo){
						case "Producto":
							$this->app->deleterecord("dimensiones"," where idCotizacion=$id");
							$this->app->deleterecord("sustratos"," where idCotizacion=$id");
							$this->app->deleterecord("tintas"," where idCotizacion=$id");
							$this->app->deleterecord("escalasvolumen"," where idCotizacion=$id");
							$this->app->deleterecord("terminoscondiciones"," where idCotizacion=$id");
						break;
						case "Proyecto-Exhibidor":
							$this->app->deleterecord("dimensiones"," where idCotizacion=$id");
							$this->app->deleterecord("exhibidores"," where idCotizacion=$id");						
						break;
						case "Proyecto-Empaque":
							$this->app->deleterecord("dimensiones"," where idCotizacion=$id");
							$this->app->deleterecord("empaques"," where idCotizacion=$id");						
						break;
						case "Maquila":
						
							$this->app->deleterecord("rel_maquilaproceso"," where idCotizacion=$id");	
						break;
					}
					$datos[]=array(
						'exito'=>'si'
					);					
			}		
				
		} 
		else{
			
			$datos[]=array(
					'exito'=>'sin'

				);
		}


	return $datos;


	}

	public function getallquotationsbydate ($fecini,$fecfin){
	if(!$this->userfunctions->islogged()){
			//$this->userfunctions->logout();
		}
	$datos=array();
		
		
		$result=$this->app->getrecord("cotizaciones","cotizaciones.id,cotizaciones.cliente,cotizaciones.tipo,cotizaciones.tipoProducto,cotizaciones.fechaElaboracion,cotizaciones.fechaCotizacion,cotizaciones.fechaPedido,cotizaciones.dias,cotizaciones.pedidoSAE,cotizaciones.rutaCotizacion,status_cotizacion.status,concat(per1.nombre,' ',per1.primerApellido,' ',per1.segundoApellido) as 'usuario',concat(per2.nombre,' ',per2.primerApellido,' ',per2.segundoApellido) as 'responsable'","inner JOIN status_cotizacion on cotizaciones.idStatuscotizacion=status_cotizacion.id INNER JOIN usuarios as user1 on cotizaciones.idUsuario=user1.id INNER JOIN persona as per1 on per1.id=user1.idPersona  INNER JOIN usuarios as user2 on cotizaciones.idResponsable=user2.id INNER JOIN persona as per2 on per2.id=user2.idPersona  where cotizaciones.activo like '1' and cotizaciones.fechaElaboracion  BETWEEN '$fecini' and '$fecfin'");
		$result2=$this->app->getrecord("cotizaciones","COUNT(cotizaciones.id) as 'totalcotizaciones'","where cotizaciones.activo like '1' and cotizaciones.fechaElaboracion  BETWEEN '$fecini' and '$fecfin'");
		$result3=$this->app->getrecord("cotizaciones","COUNT(cotizaciones.id) as 'totalpedidos'","where cotizaciones.activo like '1' and cotizaciones.fechaElaboracion  BETWEEN '$fecini' and '$fecfin' and cotizaciones.pedidoSAE>0");
		$result4=$this->app->getrecord("cotizaciones","COUNT(cotizaciones.id) as 'totalvencidas'","where cotizaciones.activo like '1' and cotizaciones.fechaElaboracion BETWEEN '$fecini' and '$fecfin' and cotizaciones.pedidoSAE=0 and now()>DATE_ADD(cotizaciones.fechaElaboracion, INTERVAL 15 DAY)");
		$result5=$this->app->getrecord("cotizaciones","COUNT(cotizaciones.id) as 'totalactivas'","where cotizaciones.activo like '1' and cotizaciones.fechaElaboracion BETWEEN '$fecini' and '$fecfin' and cotizaciones.pedidoSAE=0 and now()<DATE_ADD(cotizaciones.fechaElaboracion, INTERVAL 15 DAY)");
		for($a=0;$a<sizeof($result);$a++){
			$vec=$result[$a];
			$vec2=$result2[0];
			$vec3=$result3[0];
			$vec4=$result4[0];
			$vec5=$result5[0];


			$dat=explode(" ",$vec['fechaElaboracion']);
			$dat2=explode(" ",$vec['fechaCotizacion']);
			$dat3=explode(" ",$vec['fechaPedido']);

			$datetime1 = date_create($dat[0]);
       			$datetime2 = date_create($dat2[0]);
    			$datetime3 = date_create($dat3[0]);

    			$interval = date_diff($datetime1, $datetime2);

			$interval2 = date_diff($datetime1, $datetime3);

			if($vec['fechaCotizacion']=='0000-00-00 00:00:00'){
				$diascoti=0;
			}else{
				$diascoti=$interval->d;
			}
			if($vec['fechaPedido']=='0000-00-00 00:00:00'){
				$diaspedi=0;
			}else{
				
				$diaspedi=$interval2->d;
			}

			$fecha = new DateTime($dat[0]);
			$fecha->add(new DateInterval('P6D'));
			 $vence1=$fecha->format('Y-m-d');

			$fecha2 = new DateTime($dat[0]);
			$fecha2->add(new DateInterval('P11D'));
			 $vence2=$fecha2->format('Y-m-d');

			$fecha3 = new DateTime($dat[0]);
			$fecha3->add(new DateInterval('P15D'));
			 $vence3=$fecha3->format('Y-m-d');

			$datos[] = array(
				'id'=>$vec['id'],
				'cliente'=>$vec['cliente'],
				'tipo'=>$vec['tipo'],
				'tipoproducto'=>$vec['tipoProducto'],
				'fechaelaboracion'=>$vec['fechaElaboracion'],
				'fechacotizacion'=>$vec['fechaCotizacion'],
				'fechapedido'=>$vec['fechaPedido'],
				'dias'=>$vec['dias'],
				'pedido'=>$vec['pedidoSAE'],
				'ruta'=>$vec['rutaCotizacion'],
				'status'=>$vec['status'],
				'usuario'=>$vec['usuario'],
				'responsable'=>$vec['responsable'],
				'diascotizacion'=>$diascoti,
				'diaspedido'=>$diaspedi,
				'vence1'=>$vence1,
				'vence2'=>$vence2,
				'vence3'=>$vence3,
				'totalcotizaciones'=>$vec2['totalcotizaciones'],
				'totalpedidos'=>$vec3['totalpedidos'],
				'totalactivas'=>$vec5['totalactivas'],
				'totalvencidas'=>$vec4['totalvencidas']
			);
				
		}
	return $datos;
	}

	public function getallquotationsbydateandseller($fecini,$fecfin,$vendedor){

		if(!$this->userfunctions->islogged()){
			//$this->userfunctions->logout();
		}
		$datos=array();
		//trigger_error($vendedor);
	if($vendedor==-1){

		$resultt=$this->app->getrecord("usuarios","id"," where usuarios.vendedor like '4'");
		for($c=0;$c<sizeof($resultt);$c++){
			$clientess=0;
			$vecc=$resultt[$c];
			$vendedor=$vecc['id'];		
			$result0=$this->app->getrecord("usuarios","CONCAT(persona.nombre,' ',persona.primerApellido,' ',persona.segundoApellido) as 'agente' "," inner join persona on persona.id=usuarios.idPersona where usuarios.id=$vendedor");
		
				$vec0=$result0[0];
				$agente=$vec0['agente'];

			$result=$this->app->getrecord("cotizaciones","DISTINCT(cliente)"," where idUsuario=$vendedor order by fechaElaboracion desc ");
		
			for($a=0;$a<sizeof($result);$a++){
				
				$vec=$result[$a];
				$clientecoti=$vec['cliente'];
			
				$result1=$this->app->getrecord("clientes","*"," where razonSocial like '$clientecoti'");
				$vec1=$result1[0];

				$result1=$this->app->getrecord("clientes","*"," where razonSocial like '$clientecoti'");
				$vec1=$result1[0];

				$cliente=$vec1['razonSocial'];
				$estimacion=$vec1['estimacionVentasanual'];
				$fecharegistro=$vec1['fechaRegistro'];
	
				$result2=$this->app->getrecord("cotizaciones","COUNT(cotizaciones.id) as 'totalcotizaciones'","where cotizaciones.activo like '1' and cotizaciones.fechaElaboracion  BETWEEN '$fecini' and '$fecfin' and idUsuario=$vendedor and cotizaciones.cliente like '$cliente'");
				$result3=$this->app->getrecord("cotizaciones","COUNT(cotizaciones.id) as 'totalpedidos'","where cotizaciones.activo like '1' and cotizaciones.fechaElaboracion  BETWEEN '$fecini' and '$fecfin' and cotizaciones.pedidoSAE>0 and cotizaciones.cliente like '$cliente' and idUsuario=$vendedor");
				$result4=$this->app->getrecord("cotizaciones","COUNT(cotizaciones.id) as 'totalvencidas'","where cotizaciones.activo like '1' and cotizaciones.fechaElaboracion BETWEEN '$fecini' and '$fecfin' and cotizaciones.pedidoSAE=0 and now()>DATE_ADD(cotizaciones.fechaElaboracion, INTERVAL 15 DAY) and cotizaciones.cliente like '$cliente' and idUsuario=$vendedor");
				$result5=$this->app->getrecord("cotizaciones","COUNT(cotizaciones.id) as 'totalactivas'","where cotizaciones.activo like '1' and cotizaciones.fechaElaboracion BETWEEN '$fecini' and '$fecfin' and cotizaciones.pedidoSAE=0 and now()<DATE_ADD(cotizaciones.fechaElaboracion, INTERVAL 15 DAY) and cotizaciones.cliente like '$cliente' and idUsuario=$vendedor");
			
				$vec2=$result2[0];
				$vec3=$result3[0];
				$vec4=$result4[0];
				$vec5=$result5[0];

				$datos[] = array(
				
					'cliente'=>$cliente,
					'ventas'=>$estimacion,
					'registro'=>$fecharegistro,
					'agente'=>$agente,
					'totalclientes'=>sizeof($result),
					'totalcotizaciones'=>$vec2['totalcotizaciones'],
					'totalpedidos'=>$vec3['totalpedidos'],
					'totalactivas'=>$vec5['totalactivas'],
					'totalvencidas'=>$vec4['totalvencidas']
				);
			}
		}
	}else{

		$result0=$this->app->getrecord("usuarios","CONCAT(persona.nombre,' ',persona.primerApellido,' ',persona.segundoApellido) as 'agente' "," inner join persona on persona.id=usuarios.idPersona where usuarios.id=$vendedor");
		
			$vec0=$result0[0];
			$agente=$vec0['agente'];

		$result=$this->app->getrecord("cotizaciones","DISTINCT(cliente)"," where idUsuario=$vendedor");
		
		for($a=0;$a<sizeof($result);$a++){

			$vec=$result[$a];
			$clientecoti=$vec['cliente'];
			
			$result1=$this->app->getrecord("clientes","*"," where razonSocial like '$clientecoti'");
			$vec1=$result1[0];

			$result1=$this->app->getrecord("clientes","*"," where razonSocial like '$clientecoti'");
			$vec1=$result1[0];

			$cliente=$vec1['razonSocial'];
			$estimacion=$vec1['estimacionVentasanual'];
			$fecharegistro=$vec1['fechaRegistro'];

			$result2=$this->app->getrecord("cotizaciones","COUNT(cotizaciones.id) as 'totalcotizaciones'","where cotizaciones.activo like '1' and cotizaciones.fechaElaboracion  BETWEEN '$fecini' and '$fecfin' and idUsuario=$vendedor and cotizaciones.cliente like '$cliente'");
			$result3=$this->app->getrecord("cotizaciones","COUNT(cotizaciones.id) as 'totalpedidos'","where cotizaciones.activo like '1' and cotizaciones.fechaElaboracion  BETWEEN '$fecini' and '$fecfin' and cotizaciones.pedidoSAE>0 and cotizaciones.cliente like '$cliente' and idUsuario=$vendedor");
			$result4=$this->app->getrecord("cotizaciones","COUNT(cotizaciones.id) as 'totalvencidas'","where cotizaciones.activo like '1' and cotizaciones.fechaElaboracion BETWEEN '2018-05-01' and '2018-06-15' and cotizaciones.pedidoSAE=0 and now()>DATE_ADD(cotizaciones.fechaElaboracion, INTERVAL 15 DAY) and cotizaciones.cliente like '$cliente' and idUsuario=$vendedor");
			$result5=$this->app->getrecord("cotizaciones","COUNT(cotizaciones.id) as 'totalactivas'","where cotizaciones.activo like '1' and cotizaciones.fechaElaboracion BETWEEN '2018-05-01' and '2018-06-15' and cotizaciones.pedidoSAE=0 and now()<DATE_ADD(cotizaciones.fechaElaboracion, INTERVAL 15 DAY) and cotizaciones.cliente like '$cliente' and idUsuario=$vendedor");
			
			$vec2=$result2[0];
			$vec3=$result3[0];
			$vec4=$result4[0];
			$vec5=$result5[0];

			$datos[] = array(
				
				'cliente'=>$cliente,
				'ventas'=>$estimacion,
				'registro'=>$fecharegistro,
				'agente'=>$agente,
				'totalclientes'=>sizeof($result),
				'totalcotizaciones'=>$vec2['totalcotizaciones'],
				'totalpedidos'=>$vec3['totalpedidos'],
				'totalactivas'=>$vec5['totalactivas'],
				'totalvencidas'=>$vec4['totalvencidas']
			);
		}
			
	}
		
	return $datos;
	}

	public function deletequotationincomplete($id){
		if(!$this->userfunctions->islogged()){
			//$this->userfunctions->logout();
		}
	$datos=array();
		$result=$this->app->getrecord("cotizaciones","*"," where id=$id");
		
		if (sizeof($result) > 0) 
		{		
			if($this->app->deleterecord("cotizaciones"," where id=$id")>0){

							$this->app->deleterecord("dimensiones"," where idCotizacion=$id");
							$this->app->deleterecord("especificacionescotizacion"," where idCotizacion=$id");
							$this->app->deleterecord("secciones"," where idCotizacion=$id");
							$this->app->deleterecord("ventanas"," where idCotizacion=$id");
							$this->app->deleterecord("terminosventa"," where idCotizacion=$id");
					$datos[]=array(
						'exito'=>'si'
					);					
			}		
				
		} 
		else{
			
			$datos[]=array(
					'exito'=>'sin'

				);
		}


	return $datos;

	}
/////////////////////////////////////////FUNCTIONS FORUPDATE QUOTATION//////////////////////////////////////////////////
public function getfirstsection ($id){
	if(!$this->userfunctions->islogged()){
			//$this->userfunctions->logout();
		}
	$datos=array();
		
		
		$result=$this->app->getrecord("cotizaciones","cotizaciones.idUnico,cotizaciones.id,cotizaciones.cliente,cotizaciones.tipo,cotizaciones.tipoProducto,cotizaciones.fechaElaboracion,cotizaciones.fechaCotizacion,cotizaciones.fechaPedido,cotizaciones.dias,cotizaciones.pedidoSAE,cotizaciones.rutaCotizacion,status_cotizacion.status,concat(per1.nombre,' ',per1.primerApellido,' ',per1.segundoApellido) as 'usuario',concat(per2.nombre,' ',per2.primerApellido,' ',per2.segundoApellido) as 'responsable'","inner JOIN status_cotizacion on cotizaciones.idStatuscotizacion=status_cotizacion.id INNER JOIN usuarios as user1 on cotizaciones.idUsuario=user1.id INNER JOIN persona as per1 on per1.id=user1.idPersona  INNER JOIN usuarios as user2 on cotizaciones.idResponsable=user2.id INNER JOIN persona as per2 on per2.id=user2.idPersona  where cotizaciones.activo like '1' and cotizaciones.id=$id ");
		

		for($a=0;$a<sizeof($result);$a++){
			$vec=$result[$a];
			
			
			$datos[] = array(
				'id'=>$vec['id'],
				'cliente'=>$vec['cliente'],
				'tipo'=>$vec['tipo'],
				'idunico'=>$vec['idUnico'],
				'tipoproducto'=>$vec['tipoProducto'],
				'fechaelaboracion'=>$vec['fechaElaboracion'],
				'fechacotizacion'=>$vec['fechaCotizacion'],
				'fechapedido'=>$vec['fechaPedido'],
				'dias'=>$vec['dias'],
				'pedido'=>$vec['pedidoSAE'],
				'ruta'=>$vec['rutaCotizacion'],
				'status'=>$vec['status'],
				'usuario'=>$vec['usuario'],
				'responsable'=>$vec['responsable'],
			);
				
		}
	return $datos;
}

	public function updatefirstsection ($id,$tipoproducto,$idunico){

		if(!$this->userfunctions->islogged()){
			
		}
		$datos=array();
		$result=$this->app->getrecord("cotizaciones","*"," where id= $id");
		
			


		if (sizeof($result) > 0) {

			
			if($this->app->updaterecord("cotizaciones","idUnico='$idunico',tipoProducto='$tipoproducto'"," where id=$id")){
					$datos[]=array(
						'exito'=>'si',
						'folio'=>$id
					);
							
			}	
				
		} 
		else{
			
			$datos[]=array(
					'exito'=>'sin',
					'folio'=>0
				);
		}
	return $datos;

	}
	public function updatesecondsection($id,$a,$b,$h,$x,$y,$z,$o,$p,$q,$r,$s,$t,$codigo,$notas){
		if(!$this->userfunctions->islogged()){
			
		}
		$datos=array();
		$result=$this->app->getrecord("dimensiones","*"," where idCotizacion= $id");
		
			


		if (sizeof($result) > 0) {

			
			if($this->app->updaterecord("dimensiones","a=$a,b=$b,h=$h,x=$x,y=$y,z=$z,o=$o,p=$p,q=$q,r=$r,s=$s,t=$t,codigo='$codigo',descripcion='$notas' "," where idCotizacion=$id")){
					$datos[]=array(
						'exito'=>'si',
						'folio'=>$id
					);
							
			}	
				
		} 
		else{
			
			$datos[]=array(
					'exito'=>'sin',
					'folio'=>0
				);
		}
	return $datos;


	}
	

	public function updatefourthsection($id,$herramental,$separarcambiosarte,$web2print,$dummy,$escala1,$escala2,$escala3,$escala4,$escala5,$escala6,$costo,$margen,$vigencia,$formapago,$formaentrega,$inner,$cantidadinner,$masterr,$cantidadmaster){
		if(!$this->userfunctions->islogged()){
			
		}
		$datos=array();
		$result=$this->app->getrecord("terminosventa","*"," where idCotizacion= $id");
		
			


		if (sizeof($result) > 0) {

			
			if($this->app->updaterecord("terminosventa","herramental='$herramental',separarCambiosArte='$separarcambiosarte',webtoPrint='$web2print',dummy='$dummy',volumen1=$escala1,volumen2=$escala2,volumen3=$escala3,volumen4=$escala4,volumen5=$escala5,volumen6=$escala6,costoObjetivo=$costo,margenGanancia='$margen',vigenciaCotizacion='$vigencia',formaPago='$formapago',flete='$formaentrega',tipoInner='$inner',multiploInner=$cantidadinner,tipoMaster='$masterr',multiploMaster=$cantidadmaster "," where idCotizacion=$id")){
					$datos[]=array(
						'exito'=>'si',
						'folio'=>$id
					);
							
			}	
				
		} 
		else{
			
			$datos[]=array(
					'exito'=>'sin',
					'folio'=>0
				);
		}
	return $datos;
	
	}
	public function updatethirdsection($idsec,$idespe,$idvtana,$id,$categoria,$sustrato,$empalme,$suaje,$pegado,$nombreseccion,$tipotintafrente,$tipotintareverso,$tintafrente,$tintareverso,$nompantonesfrente,$nompantonesreverso,$pantonesfrente,$pantonesreverso,$tipobarnizfrente,$tipobarnizreverso,$barnizfrente,$barnizreverso,
$tipolaminadofrente,$tipolaminadoreverso,$laminadofrente,$laminadoreverso,$tiposerigrafiafrente,$tiposerigrafiareverso,$serigrafiafrente,$serigrafiareverso,$artefrente,$artereverso,$ventanamaterial,$ventanalargo,$ventanaancho){
		if(!$this->userfunctions->islogged()){
			
		}
		$datos=array();
		$result=$this->app->getrecord("especificacionescotizacion","*"," where idCotizacion= $id and id=$idespe");
		
			


		if (sizeof($result) > 0) {

			
			if($this->app->updaterecord("especificacionescotizacion"," categoria='$categoria',sustratoBase='$sustrato',empalme='$empalme',suaje='$suaje',pegado='$pegado'"," where id=$idespe")){
				$this->app->updaterecord("secciones","nombreSeccion='$nombreseccion',tipoTintaFrente='$tipotintafrente',tipoTintaReverso='$tipotintareverso',frenteTinta=$tintafrente,reversoTinta=$tintareverso,nomPantonesFrente='$nompantonesfrente',nomPantonesReverso='$nompantonesreverso',frentePantone=$pantonesfrente,reversoPantone=$pantonesreverso,tipoBarnizFrente='$tipobarnizfrente',tipoBarnizReverso='$tipobarnizreverso',frenteBarniz=$barnizfrente,reversoBarniz=$barnizreverso,
tipoLaminadoFrente='$tipolaminadofrente',tipoLaminadoReverso='$tipolaminadoreverso',frenteLaminado=$laminadofrente,reversoLaminado=$laminadoreverso,tipoSerigrafiaFrente='$tiposerigrafiafrente',tipoSerigrafiaReverso='$tiposerigrafiareverso',cambiosArteFrente=$artefrente,cambiosArteReverso=$artereverso"," where id=$idsec");
			$this->app->updaterecord("ventanas","material='$ventanamaterial',ladoA=$ventanalargo,ladoB=$ventanaancho "," where id=$idvtana");
					$datos[]=array(
						'exito'=>'si',
						'folio'=>$id
					);
							
			}	
				
		} 
		else{
			
			$datos[]=array(
					'exito'=>'sin',
					'folio'=>0
				);
		}
	return $datos;
	
	}
/////////////////////////////////////////FUNCTIONS TO EXPORT INFORMATION//////////////////////////////////////////////////
	public function secc1($id){
		if(!$this->userfunctions->islogged()){
			//$this->userfunctions->logout();
		}
		$datos=array();
		$result=$this->app->getrecord("cotizaciones","*","where id=$id and activo like '1' ");
		for($a=0;$a<sizeof($result);$a++)
    		{
			$vec=$result[$a];	

			$datos[] = array(
					'id'=>$vec['id'],
					'cliente'=>$vec['cliente'],
					'producto'=>$vec['tipoProducto'],
					'idunico'=>$vec['idUnico'],
					'fecharegistro'=>$vec['fechaElaboracion'],
					'secc2'=>$this->secc2($id),
					'secc3'=>$this->secc3($id),
					'secc4'=>$this->secc4($id)		
					);			
                }
		
	return $datos;
	}
	public function secc2($id){
		if(!$this->userfunctions->islogged()){
			//$this->userfunctions->logout();
		}
		$datos=array();
		$result=$this->app->getrecord("dimensiones","*","where idCotizacion=$id and activo like '1' ");
		for($a=0;$a<sizeof($result);$a++)
    		{
			$vec=$result[$a];	

			$datos[] = array(
					'a'=>$vec['a'],
					'b'=>$vec['b'],
					'h'=>$vec['h'],
					'x'=>$vec['x'],
					'y'=>$vec['y'],
					'z'=>$vec['z'],
					'o'=>$vec['o'],
					'p'=>$vec['p'],
					'q'=>$vec['q'],
					'r'=>$vec['r'],
					's'=>$vec['s'],
					't'=>$vec['t'],
					'descripcion'=>$vec['descripcion'],
					'codigo'=>$vec['codigo']
					);			
                }
		
	return $datos;
	}
	public function secc3($id){
		if(!$this->userfunctions->islogged()){
			//$this->userfunctions->logout();
		}
		$datos=array();
		$result=$this->app->getrecord("especificacionescotizacion","*","where idCotizacion=$id and activo like '1' ");

		$result1=$this->app->getrecord("secciones","*","where idCotizacion=$id and activo like '1' ");

		$result2=$this->app->getrecord("ventanas","*","where idCotizacion=$id and activo like '1' ");

		for($a=0;$a<sizeof($result);$a++)
    		{
			$vec=$result[$a];
			$vec1=$result1[$a];
			$vec2=$result2[$a];	

			$datos[] = array(
					'idseccion'=>$vec1['id'],
					'idespecificaciones'=>$vec['id'],
					'idventana'=>$vec2['id'],
					'categoria'=>$vec['categoria'],
					'nombreseccion'=>$vec1['nombreSeccion'],
					'sustrato'=>$vec['sustratoBase'],
					'empalme'=>$vec['empalme'],
					'suaje'=>$vec['suaje'],
					'pegado'=>$vec['pegado'],
					'tipotintafrente'=>$vec1['tipoTintaFrente'],
					'tipotintareverso'=>$vec1['tipoTintaReverso'],
					'frentetinta'=>$vec1['frenteTinta'],
					'reversotinta'=>$vec1['reversoTinta'],
					'nompantonesfrente'=>$vec1['nomPantonesFrente'],
					'nompantonesreverso'=>$vec1['nomPantonesReverso'],
					'frentepantone'=>$vec1['frentePantone'],
					'reversopantone'=>$vec1['reversoPantone'],

					'tipobarnizfrente'=>$vec1['tipoBarnizFrente'],
					'tipobarnizreverso'=>$vec1['tipoBarnizReverso'],
					'frentebarniz'=>$vec1['frenteBarniz'],
					'reversoBarniz'=>$vec1['reversoBarniz'],

					'tipolaminadofrente'=>$vec1['tipoLaminadoFrente'],
					'tipolaminadoreverso'=>$vec1['tipoLaminadoReverso'],
					'frentelaminado'=>$vec1['frenteLaminado'],
					'reversolaminado'=>$vec1['reversoLaminado'],
					'tiposerigrafiafrente'=>$vec1['tipoSerigrafiaFrente'],
					'tiposerigrafiareverso'=>$vec1['tipoSerigrafiaReverso'],
					'frenteserigrafia'=>$vec1['frenteSerigrafia'],
					'reversoserigrafia'=>$vec1['reversoSerigrafia'],
					'cambiosartefrente'=>$vec1['cambiosArteFrente'],
					'cambiosartereverso'=>$vec1['cambiosArteReverso'],
					'materialventana'=>$vec2['material'],
					'ladoa'=>$vec2['ladoA'],
					'ladob'=>$vec2['ladoB']
					);;			
                }
		
	return $datos;
	}
	public function secc3specific($idsec,$idesp,$idvtana){
		if(!$this->userfunctions->islogged()){
			//$this->userfunctions->logout();
		}
		$datos=array();
		$result=$this->app->getrecord("especificacionescotizacion","*","where id=$idesp and activo like '1' ");

		$result1=$this->app->getrecord("secciones","*","where id=$idsec and activo like '1' ");

		$result2=$this->app->getrecord("ventanas","*","where id=$idvtana and activo like '1' ");

		for($a=0;$a<sizeof($result);$a++)
    		{
			$vec=$result[$a];
			$vec1=$result1[$a];
			$vec2=$result2[$a];	

			$datos[] = array(
					'idseccion'=>$vec1['id'],
					'idespecificaciones'=>$vec['id'],
					'categoria'=>$vec['categoria'],
					'idventana'=>$vec2['id'],
					'nombreseccion'=>$vec1['nombreSeccion'],
					'sustrato'=>$vec['sustratoBase'],
					'empalme'=>$vec['empalme'],
					'suaje'=>$vec['suaje'],
					'pegado'=>$vec['pegado'],
					'tipotintafrente'=>$vec1['tipoTintaFrente'],
					'tipotintareverso'=>$vec1['tipoTintaReverso'],
					'frentetinta'=>$vec1['frenteTinta'],
					'reversotinta'=>$vec1['reversoTinta'],
					'nompantonesfrente'=>$vec1['nomPantonesFrente'],
					'nompantonesreverso'=>$vec1['nomPantonesReverso'],
					'frentepantone'=>$vec1['frentePantone'],
					'reversopantone'=>$vec1['reversoPantone'],

					'tipobarnizfrente'=>$vec1['tipoBarnizFrente'],
					'tipobarnizreverso'=>$vec1['tipoBarnizReverso'],
					'frentebarniz'=>$vec1['frenteBarniz'],
					'reversoBarniz'=>$vec1['reversoBarniz'],

					'tipolaminadofrente'=>$vec1['tipoLaminadoFrente'],
					'tipolaminadoreverso'=>$vec1['tipoLaminadoReverso'],
					'frentelaminado'=>$vec1['frenteLaminado'],
					'reversolaminado'=>$vec1['reversoLaminado'],
					'tiposerigrafiafrente'=>$vec1['tipoSerigrafiaFrente'],
					'tiposerigrafiareverso'=>$vec1['tipoSerigrafiaReverso'],
					'frenteserigrafia'=>$vec1['frenteSerigrafia'],
					'reversoserigrafia'=>$vec1['reversoSerigrafia'],
					'cambiosartefrente'=>$vec1['cambiosArteFrente'],
					'cambiosartereverso'=>$vec1['cambiosArteReverso'],
					'materialventana'=>$vec2['material'],
					'ladoa'=>$vec2['ladoA'],
					'ladob'=>$vec2['ladoB']
					);			
                }
		
	return $datos;
	}
	public function secc4($id){
		if(!$this->userfunctions->islogged()){
			//$this->userfunctions->logout();
		}
		$datos=array();
		$result=$this->app->getrecord("terminosventa","*","where idCotizacion=$id and activo like '1' ");
		for($a=0;$a<sizeof($result);$a++)
    		{
			$vec=$result[$a];	

			$datos[] = array(
					'herramental'=>$vec['herramental'],
					'separarcambiosarte'=>$vec['separarCambiosArte'],
					'dummy'=>$vec['dummy'],
					'webtoprint'=>$vec['webtoPrint'],
					'volumen1'=>$vec['volumen1'],
					'volumen2'=>$vec['volumen2'],
					'volumen3'=>$vec['volumen3'],
					'volumen4'=>$vec['volumen4'],
					'volumen5'=>$vec['volumen5'],
					'volumen6'=>$vec['volumen6'],
					'tipoinner'=>$vec['tipoInner'],
					'multiploinner'=>$vec['multiploInner'],
					'tipomaster'=>$vec['tipoMaster'],
					'multiplomaster'=>$vec['multiploMaster'],
					'vigenciacotizacion'=>$vec['vigenciaCotizacion'],
					'formapago'=>$vec['formaPago'],
					'flete'=>$vec['flete'],
					'costoobjetivo'=>$vec['costoObjetivo'],
					'margenganancia'=>$vec['margenGanancia']
					);			
                }
		
	return $datos;
	}
	public function exportfilequotes($id){
		date_default_timezone_set ("America/Mexico_City");

		$hoy = date("YmdHis");

		$data=array();
		$datos=$this->secc1($id);
		$fecha=getdate();
		$nom='Cotizacion-'.$hoy.".xls";		
		$ruta2=$this->ruta.$nom;

		$archivo = fopen($ruta2,'x+');
			
		$contenido="Id Cotizacion	".utf8_decode($datos[0]['id'])."
Cliente	".utf8_decode($datos[0]['cliente'])." 
Producto	".utf8_decode($datos[0]['producto'])."
Id �nico	".utf8_decode($datos[0]['idunico'])."
Fecha de Registro	".utf8_decode($datos[0]['fecharegistro'])." 
Variable A	".utf8_decode($datos[0]['secc2'][0]['a'])." 
Variable B	".utf8_decode($datos[0]['secc2'][0]['b'])."
Variable H	".utf8_decode($datos[0]['secc2'][0]['h'])."
Variable X	".utf8_decode($datos[0]['secc2'][0]['x'])." 
Variable Y	".utf8_decode($datos[0]['secc2'][0]['y'])." 
Variable Z	".utf8_decode($datos[0]['secc2'][0]['z'])."
Variable O	".utf8_decode($datos[0]['secc2'][0]['o'])." 
Variable P	".utf8_decode($datos[0]['secc2'][0]['p'])."
Variable Q	".utf8_decode($datos[0]['secc2'][0]['q'])."
Variable R	".utf8_decode($datos[0]['secc2'][0]['r'])." 
Variable S	".utf8_decode($datos[0]['secc2'][0]['s'])." 
Variable T	".utf8_decode($datos[0]['secc2'][0]['t'])."
Descripcion de Variables	".utf8_decode($datos[0]['secc2'][0]['descripcion'])." 
C�digo de Referencia	".utf8_decode($datos[0]['secc2'][0]['codigo'])."\r\n";

		for($a=0;$a<sizeof($datos[0]['secc3']);$a++){
		$b=$a+1;
		$contenido=$contenido."Nombre Secci�n	".utf8_decode($datos[0]['secc3'][$a]['nombreseccion'])."
Sustrato Base	".utf8_decode($datos[0]['secc3'][$a]['sustrato'])." 
Empalme	".utf8_decode($datos[0]['secc3'][$a]['empalme'])." 
Suaje	".utf8_decode($datos[0]['secc3'][$a]['suaje'])." 
Pegado	".utf8_decode($datos[0]['secc3'][$a]['pegado'])." 
Tipo de Tinta al Frente	".utf8_decode($datos[0]['secc3'][$a]['tipotintafrente'])." 
Tipo de Tinta al Reverso	".utf8_decode($datos[0]['secc3'][$a]['tipotintareverso'])."
Tintas al Frente	".utf8_decode($datos[0]['secc3'][$a]['frentetinta'])." 
Tintas al Reverso	".utf8_decode($datos[0]['secc3'][$a]['reversotinta'])." 
Nombre Pantones al Frente	". utf8_decode($datos[0]['secc3'][$a]['nompantonesfrente'])." 
Nombre Pantones al Reverso	".utf8_decode($datos[0]['secc3'][$a]['nompantonesreverso'])." 
Pantones al Frente	". utf8_decode($datos[0]['secc3'][$a]['frentepantone'])." 
Pantones al Reverso	".utf8_decode($datos[0]['secc3'][$a]['reversopantone'])." 
Tipo de Barniz al Frente	".utf8_decode($datos[0]['secc3'][$a]['tipobarnizfrente'])." 
Tipo de Barniz al Reverso	".utf8_decode($datos[0]['secc3'][$a]['tipobarnizreverso'])." 
Barniz al Frente	".utf8_decode($datos[0]['secc3'][$a]['frentebarniz'])."
Barniz al Reverso	".utf8_decode($datos[0]['secc3'][$a]['reversoBarniz'])." 
Tipo Laminado al Frente	".utf8_decode($datos[0]['secc3'][$a]['tipolaminadofrente'])."
Tipo Laminado al Reverso	".utf8_decode($datos[0]['secc3'][$a]['tipolaminadoreverso'])."
Laminado al Frente	".utf8_decode($datos[0]['secc3'][$a]['frentelaminado'])."
Laminado al Reverso	".utf8_decode($datos[0]['secc3'][$a]['reversolaminado'])." 
Serigraf�a al Frente	". utf8_decode($datos[0]['secc3'][$a]['tiposerigrafiafrente'])."
Serigraf�a al Reverso	".utf8_decode($datos[0]['secc3'][$a]['tiposerigrafiareverso'])."
Cambios de Arte al Frente	". utf8_decode($datos[0]['secc3'][$a]['cambiosartefrente'])."
Cambios de Arte al Reverso	".utf8_decode($datos[0]['secc3'][$a]['cambiosartereverso'])." 
Material de la Ventana	". utf8_decode($datos[0]['secc3'][$a]['materialventana'])." 
Lado A	".utf8_decode($datos[0]['secc3'][$a]['ladoa'])."
Lado B	".utf8_decode($datos[0]['secc3'][$a]['ladob'])."\r\n";
		}

			$contenido=$contenido."Herramental	".utf8_decode($datos[0]['secc4'][0]['herramental'])." 
Separar Cambios de Arte	".utf8_decode($datos[0]['secc4'][0]['separarcambiosarte'])." 
Dummy	".utf8_decode($datos[0]['secc4'][0]['dummy'])."
Escala Volumen 1	". utf8_decode($datos[0]['secc4'][0]['volumen1'])." 
Escala Volumen 2	".utf8_decode($datos[0]['secc4'][0]['volumen2'])." 
Escala Volumen 3	". utf8_decode($datos[0]['secc4'][0]['volumen3'])." 
Escala Volumen 4	".utf8_decode($datos[0]['secc4'][0]['volumen4'])." 
Escala Volumen 5	". utf8_decode($datos[0]['secc4'][0]['volumen5'])."
Escala Volumen 6	".utf8_decode($datos[0]['secc4'][0]['volumen6'])." 
Inner Pack	". utf8_decode($datos[0]['secc4'][0]['tipoinner'])." 
Master Pack	".utf8_decode($datos[0]['secc4'][0]['tipomaster'])."
Forma de Pago	".utf8_decode($datos[0]['secc4'][0]['formapago'])." 
Flete	". utf8_decode($datos[0]['secc4'][0]['flete'])." 
Costo Objetivo	".utf8_decode($datos[0]['secc4'][0]['costoobjetivo'])." 
Margen de Ganancia	".utf8_decode($datos[0]['secc4'][0]['margenganancia']);
		//$contenido=utf8_decode($contenido);
		fputs($archivo,$contenido);
		fclose($archivo);

			$data[]=array(
				'exito'=>'si',
				'ruta'=>$this->ruta.$nom
	
				);	
	return $data;
	}



}
?>