<?php
global $idorden;

$id=$_GET['idp'];
$app;
$datos=array();
$archivos=array();
require("../recursos/FPDFF/fpdf.php");

require_once("appControl.php");
$app=new appControl();
date_default_timezone_set('America/Mexico_City');
$var=date('d/m/y')." ".date('g:i:s a');

$datos=$app->getspecificproduct($id);

//trigger_error(print_r($datos,true));

$clavesae=$datos[0]['codigo'];
while(strlen($clavesae)<8){
$clavesae="0".$clavesae;
}
//$GLOBALS['cve']=$clavesae;
$archivos=$app->getfilesproducto($id,$clavesae);


class PDF extends FPDF
{

var $widths;
var $aligns;

function SetWidths($w)
{
//Set the array of column widths
$this->widths=$w;
}

function SetAligns($a)
{
//Set the array of column alignments
$this->aligns=$a;
}

function Row($data,$border,$fill='D')
{
//Calculate the height of the row
$nb=0;
for($i=0;$i<count($data);$i++)
$nb=max($nb,$this->NbLines($this->widths[$i],$data[$i]));
$h=5*$nb;
//Issue a page break first if needed
$this->CheckPageBreak($h);
//Draw the cells of the row
for($i=0;$i<count($data);$i++)
{
$w=$this->widths[$i];
$a=isset($this->aligns[$i]) ? $this->aligns[$i] : 'L';
//Save the current position
$x=$this->GetX();
$y=$this->GetY();
//Draw the border
if($border==1){
$this->Rect($x,$y,$w,$h,$fill);
}
//Print the text
$this->MultiCell($w,5,$data[$i],0,$a);
//Put the position to the right of the cell
$this->SetXY($x+$w,$y);
}
//Go to the next line
$this->Ln($h);
}

function CheckPageBreak($h)
{
//If the height h would cause an overflow, add a new page immediately
if($this->GetY()+$h>$this->PageBreakTrigger)
$this->AddPage($this->CurOrientation);
}

function NbLines($w,$txt)
{
//Computes the number of lines a MultiCell of width w will take
$cw=&$this->CurrentFont['cw'];
if($w==0)
$w=$this->w-$this->rMargin-$this->x;
$wmax=($w-2*$this->cMargin)*1000/$this->FontSize;
$s=str_replace("\r",'',$txt);
$nb=strlen($s);
if($nb>0 and $s[$nb-1]=="\n")
$nb--;
$sep=-1;
$i=0;
$j=0;
$l=0;
$nl=1;
while($i<$nb)
{
$c=$s[$i];
if($c=="\n")
{
$i++;
$sep=-1;
$j=$i;
$l=0;
$nl++;
continue;
}
if($c==' ')
$sep=$i;
$l+=$cw[$c];
if($l>$wmax)
{
if($sep==-1)
{
if($i==$j)
$i++;
}
else
$i=$sep+1;
$sep=-1;
$j=$i;
$l=0;
$nl++;
}
else
$i++;
}
return $nl;
}
function Footer()
{
$this->SetY(-15);
// Select Arial italic 8
$this->SetFont('Arial','I',12);
// Print current and total page numbers


/*$this->Cell(90,5, "Autoriz�",'',0,'C');
$this->SetX(-114);
$this->Cell(90,5, "Revis�",'',0,'C');

$this->SetY(-30);
$this->Cell(90,5, "Nombre y firma",'T',0,'C');
$this->SetX(-114);
$this->Cell(90,5, "Nombre y firma",'T',0,'C');
$this->Ln(15);*/
$this->SetFont('Arial','I',6);
$this->Cell(0,10,'P�gina '.$this->PageNo().' de {nb}',0,0,'C');



}

function Header()
{
// Logo

$this->SetFont('Arial','',9);
$this->SetXY(49,10);
$this->Cell(56,5,'Nombre del Documento',1,0,'C');
$this->SetXY(105,10);
$this->Cell(90,5,'Ficha T�cnica de Producto',1,0,'C');
$this->Image('encabezado.png',12,10,110);
$this->SetXY(49,15);
$this->Cell(20,5,'Revisi�n',1,0,'C');
$this->SetXY(69,15);
$this->Cell(23,5,'Fecha',1,0,'C');
$this->SetXY(92,15);
$this->Cell(41,5,'Elabor�',1,0,'C');
$this->SetXY(133,15);
$this->Cell(40,5,'Autoriz�',1,0,'C');
$this->SetXY(173,15);
$this->Cell(22,5,'C�digo',1,0,'C');
$this->SetXY(49,20);
$this->Cell(20,5,'01',1,0,'C');
$this->SetXY(69,20);
$this->Cell(23,5,'03.Julio.2018',1,0,'C');
$this->SetXY(92,20);
$this->Cell(41,5,'Alfonso Ochoa Maga�a',1,0,'C');
$this->SetXY(133,20);
$this->Cell(40,5,'Jes�s P�rez Miranda',1,0,'C');
$this->SetXY(173,20);
$this->Cell(22,5,'R-ING-01',1,0,'C');
$this->Line(15, 30, 195, 30);
$this->Ln(13);

}
}



$pdf=new PDF();
//$pdf=new PDF_MC_Table();
$pdf->AliasNbPages();
$pdf->PageNo();
$pdf->SetAuthor('JVL');
$pdf->setMargins(15,0,5);
$pdf->AddPage('P');
$pdf->SetTitle("PEMSA");
$pdf->SetAutoPageBreak(true,15);
$pdf->SetTopMargin(0);


$aux;
$path='';
//trigger_error(print_r($archivos,true));
/*for($a=0;$a<sizeof($archivos);$a++){
	
	$aux=explode('.',$archivos[$a]['ruta']);
	//trigger_error(print_r($aux,true));
	if($aux[1]=='JPG' || $aux[1]=='JPEG' || $aux[1]=='PNG' || $aux[1]=='GIF' || $aux[1]=='jpg' || $aux[1]=='jpeg' || $aux[1]=='png' || $aux[1]=='gif'){
		$path='..'.$archivos[$a]['ruta'];
		break;
	}

}
//trigger_error($path);
//$pdf->Image('..\documentos\doc20180424165146-90023015.png',80,33,50,40); 
if( strlen($path)>0){
	$pdf->Image($path,80,33,50,40); 
	$pdf->Ln(50);
}*/
$pdf->SetFillColor(220,220,220);

$pdf->SetFont('Arial','',10);
$pdf->SetWidths(array(260));

$pdf->SetFillColor(0, 0, 0);

$pdf->SetFont('Arial','B',8);
$pdf->SetWidths(array(180));
$pdf->SetAligns(array('C'));
$pdf->SetTextColor(255,255,255);
$pdf->Row(array('Datos Generales'),1,'FD');
$pdf->SetTextColor(0,0,0);
$pdf->Ln(3);
$pdf->SetFillColor(210, 210, 210);

$pdf->SetFont('Arial','B',8);
$pdf->SetWidths(array(90,90));
$pdf->SetAligns(array('C','C','C'));
$pdf->Row(array('C�digo del producto','Nombre del Producto'),1,'FD');
$pdf->SetFont('Arial','',8);
$pdf->SetAligns(array('J','J','J'));
$pdf->Row(array($clavesae,iconv('UTF-8', 'windows-1252',$datos[0]['nombre'])),1,'D');
$pdf->SetFont('Arial','B',8);
$pdf->SetWidths(array(180));
$pdf->SetAligns(array('C'));
$pdf->Row(array('Descripci�n del Producto'),1,'FD');
$pdf->SetFont('Arial','',8);
$pdf->SetAligns(array('J'));
$pdf->Row(array(iconv('UTF-8', 'windows-1252',$datos[0]['descripcion'])),1,'D');
$pdf->SetFont('Arial','B',8);
$pdf->SetWidths(array(60,60,60));
$pdf->SetAligns(array('C','C','C'));
$pdf->Row(array('Presentaci�n Venta','Marca del Producto','C�digo de Barras'),1,'FD');
$pdf->SetFont('Arial','',8);
$pdf->SetAligns(array('J','J','J'));
$pdf->SetWidths(array(60,60,60));
$pdf->Row(array(iconv('UTF-8', 'windows-1252',$datos[0]['presentacion']),iconv('UTF-8', 'windows-1252',$datos[0]['marca']),iconv('UTF-8', 'windows-1252',$datos[0]['codigobarrasproducto'])),1,'D');

$pdf->SetFont('Arial','B',8);
$pdf->SetWidths(array(60,60,60));
$pdf->SetAligns(array('C','C','C'));
$pdf->Row(array('C�digo de Barras del Empaque','Pa�s de Origen','Fracci�n Arancelaria'),1,'FD');
$pdf->SetFont('Arial','',8);
$pdf->SetAligns(array('J','J','J'));
$pdf->SetWidths(array(60,60,60));
$pdf->Row(array(iconv('UTF-8', 'windows-1252',$datos[0]['codigobarrasempaque']),iconv('UTF-8', 'windows-1252',$datos[0]['pais']),iconv('UTF-8', 'windows-1252',$datos[0]['fraccionarancelaria'])),1,'D');
$pdf->SetFont('Arial','B',8);
$pdf->SetWidths(array(60,120));
$pdf->SetAligns(array('C','C'));
$pdf->Row(array('Exportaci�n','Notas Especiales'),1,'FD');
$expo;
if($datos[0]['exportacion']==0){
	$expo='No';
}else if($datos[0]['exportacion']==1){
	$expo='Si';
} 
$pdf->SetFont('Arial','',8);
$pdf->SetAligns(array('J','J','J'));
$pdf->SetWidths(array(60,120));
$pdf->Row(array(iconv('UTF-8', 'windows-1252',$expo),iconv('UTF-8', 'windows-1252',$datos[0]['notas'])),1,'D');


$pdf->Ln(3);
$pdf->SetFillColor(0, 0, 0);
$pdf->SetTextColor(255,255,255);
$pdf->SetFont('Arial','',8);
$pdf->SetWidths(array(180));
$pdf->SetAligns(array('C'));
$pdf->Row(array('Dimensiones del Producto'),1,'FD');
$pdf->Ln(3);
$pdf->SetTextColor(0,0,0);
$pdf->SetFillColor(210, 210, 210);
$pdf->SetFont('Arial','B',8);
$pdf->SetWidths(array(60,60,60));
$pdf->SetAligns(array('C','C','C'));
$pdf->Row(array('A','B','H'),1,'FD');

$pdf->SetAligns(array('J','J','J'));
$pdf->SetFont('Arial','',8);
$pdf->SetWidths(array(60,60,60));
$pdf->Row(array(iconv('UTF-8', 'windows-1252',$datos[0]['a']),iconv('UTF-8', 'windows-1252',$datos[0]['b']),iconv('UTF-8', 'windows-1252',$datos[0]['h'])),1,'D');


$pdf->SetFont('Arial','B',10);
$pdf->SetWidths(array(260));

$pdf->Ln(3);
$pdf->SetFillColor(0, 0, 0);
$pdf->SetTextColor(255,255,255);
$pdf->SetFont('Arial','',8);
$pdf->SetWidths(array(180));
$pdf->SetAligns(array('C'));
$pdf->Row(array('Especificaciones Generales'),1,'FD');
$pdf->Ln(3);
$pdf->SetTextColor(0,0,0);
$pdf->SetFillColor(210, 210, 210);
$pdf->SetFont('Arial','B',8);
$pdf->SetWidths(array(36,36,36,36,36));
$pdf->SetAligns(array('C','C','C','C','C'));
$pdf->Row(array('Insumo','Dimensiones','Tinta y/o Pantone','Acabado','Barniz'),1,'FD');

$pdf->SetAligns(array('J','J','J'));
$pdf->SetFont('Arial','',8);
$pdf->SetWidths(array(36,36,36,36,36));
$pdf->Row(array(iconv('UTF-8', 'windows-1252',$datos[0]['doc1']),iconv('UTF-8', 'windows-1252',$datos[0]['doc2']),iconv('UTF-8', 'windows-1252',$datos[0]['doc3']),iconv('UTF-8', 'windows-1252',$datos[0]['doc4']),iconv('UTF-8', 'windows-1252',$datos[0]['doc5'])),1,'D');


$pdf->Ln(3);
$pdf->SetFont('Arial','B',10);
$pdf->SetWidths(array(260));

$pdf->SetFillColor(0, 0, 0);
$pdf->SetTextColor(255,255,255);

$pdf->SetFont('Arial','',8);
$pdf->SetWidths(array(180));
$pdf->SetAligns(array('C'));
$pdf->Row(array('Caracteristicas del Empaque'),1,'FD');
$pdf->SetFillColor(170, 170, 170);
$pdf->SetTextColor(0,0,0);
$pdf->Ln(3);
$pdf->SetFont('Arial','B',8);
$pdf->SetWidths(array(180));
$pdf->SetAligns(array('C'));
$pdf->Row(array('Producto'),1,'FD');


$pdf->SetFillColor(210, 210, 210);

$pdf->SetWidths(array(45,45,45,45));
$pdf->SetAligns(array('C','C','C','C'));
$pdf->Row(array('Largo','Ancho','Alto','Peso'),1,'FD');

$pdf->SetAligns(array('J','J','J'));
$pdf->SetFont('Arial','',8);
$pdf->SetWidths(array(45,45,45,45));
$pdf->Row(array(iconv('UTF-8', 'windows-1252',$datos[0]['largoprod']),iconv('UTF-8', 'windows-1252',$datos[0]['anchoprod']),iconv('UTF-8', 'windows-1252',$datos[0]['altoprod']),iconv('UTF-8', 'windows-1252',$datos[0]['pesoprod'])),1,'D');

$pdf->Ln(3);
$pdf->SetFillColor(170, 170, 170);
$pdf->SetFont('Arial','B',8);
$pdf->SetWidths(array(180));
$pdf->SetAligns(array('C'));
$pdf->Row(array('Inner Pack'),1,'FD');


$pdf->SetFillColor(210, 210, 210);

$pdf->SetWidths(array(60,60,60,60));
$pdf->SetAligns(array('C','C','C'));
$pdf->Row(array('Empaque','Multiplo','Largo'),1,'FD');

$pdf->SetAligns(array('J','J','J'));
$pdf->SetFont('Arial','',8);
$pdf->SetWidths(array(60,60,60));
$pdf->Row(array(iconv('UTF-8', 'windows-1252',$datos[0]['empaqueinner']),iconv('UTF-8', 'windows-1252',$datos[0]['multiploinner']),iconv('UTF-8', 'windows-1252',$datos[0]['largoinner'])),1,'D');
$pdf->SetFont('Arial','B',8);
$pdf->SetWidths(array(60,60,60,60));
$pdf->SetAligns(array('C','C','C'));
$pdf->Row(array('Ancho','Alto','Peso (Kg)'),1,'FD');

$pdf->SetAligns(array('J','J','J'));
$pdf->SetFont('Arial','',8);
$pdf->SetWidths(array(60,60,60));
$pdf->Row(array(iconv('UTF-8', 'windows-1252',$datos[0]['anchoinner']),iconv('UTF-8', 'windows-1252',$datos[0]['altoinner']),iconv('UTF-8', 'windows-1252',$datos[0]['pesoinner'])),1,'D');

$pdf->Ln(3);
$pdf->SetFillColor(170, 170, 170);
$pdf->SetFont('Arial','B',8);
$pdf->SetWidths(array(180));
$pdf->SetAligns(array('C'));
$pdf->Row(array('Master Pack'),1,'FD');


$pdf->SetFillColor(210, 210, 210);

$pdf->SetWidths(array(60,60,60,60));
$pdf->SetAligns(array('C','C','C'));
$pdf->Row(array('Empaque','Multiplo','Largo'),1,'FD');

$pdf->SetAligns(array('J','J','J'));
$pdf->SetFont('Arial','',8);
$pdf->SetWidths(array(60,60,60));
$pdf->Row(array(iconv('UTF-8', 'windows-1252',$datos[0]['empaquemaster']),iconv('UTF-8', 'windows-1252',$datos[0]['multiplomaster']),iconv('UTF-8', 'windows-1252',$datos[0]['largomaster'])),1,'D');
$pdf->SetFont('Arial','B',8);
$pdf->SetWidths(array(60,60,60,60));
$pdf->SetAligns(array('C','C','C'));
$pdf->Row(array('Ancho','Alto','Peso (Kg)'),1,'FD');

$pdf->SetAligns(array('J','J','J'));
$pdf->SetFont('Arial','',8);
$pdf->SetWidths(array(60,60,60));
$pdf->Row(array(iconv('UTF-8', 'windows-1252',$datos[0]['anchomaster']),iconv('UTF-8', 'windows-1252',$datos[0]['altomaster']),iconv('UTF-8', 'windows-1252',$datos[0]['pesomaster'])),1,'D');


$pdf->Ln(3);
$pdf->SetFillColor(170, 170, 170);
$pdf->SetFont('Arial','B',8);
$pdf->SetWidths(array(180));
$pdf->SetAligns(array('C'));
$pdf->Row(array('Pallet'),1,'FD');


$pdf->SetFillColor(210, 210, 210);

$pdf->SetWidths(array(60,60,60,60));
$pdf->SetAligns(array('C','C','C'));
$pdf->Row(array('Largo','Ancho','Alto'),1,'FD');

$pdf->SetAligns(array('J','J','J'));
$pdf->SetFont('Arial','',8);
$pdf->SetWidths(array(60,60,60));
$pdf->Row(array(iconv('UTF-8', 'windows-1252',$datos[0]['largopallet']),iconv('UTF-8', 'windows-1252',$datos[0]['anchopallet']),iconv('UTF-8', 'windows-1252',$datos[0]['altopallet'])),1,'D');
$pdf->SetFont('Arial','B',8);
$pdf->SetWidths(array(60,60,60,60));
$pdf->SetAligns(array('C','C','C'));
$pdf->Row(array('Corrugados por Cama','Camas','Corrugados por Pallet'),1,'FD');

$pdf->SetAligns(array('J','J','J'));
$pdf->SetFont('Arial','',8);
$pdf->SetWidths(array(60,60,60));
$pdf->Row(array(iconv('UTF-8', 'windows-1252',$datos[0]['corrugadoscamapallet']),iconv('UTF-8', 'windows-1252',$datos[0]['camaspallet']),iconv('UTF-8', 'windows-1252',$datos[0]['corrugadospallet'])),1,'D');
$pdf->SetFont('Arial','B',8);
$pdf->SetWidths(array(60,60,60,60));
$pdf->SetAligns(array('C','C','C'));
$pdf->Row(array('Total de Piezas','Peso Bruto(kg)','Peso Neto(Kg)'),1,'FD');

$pdf->SetAligns(array('J','J','J'));
$pdf->SetFont('Arial','',8);
$pdf->SetWidths(array(60,60,60));
$pdf->Row(array(iconv('UTF-8', 'windows-1252',$datos[0]['piezaspallet']),iconv('UTF-8', 'windows-1252',$datos[0]['pesobrutopallet']),iconv('UTF-8', 'windows-1252',$datos[0]['pesonetopallet'])),1,'D');



//
//$pdf->Image('encabezado.png',50,240,100); 



$pdf->Output();
?>

