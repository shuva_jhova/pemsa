﻿<?php
require_once("usersFunction.php");
$app = new usersFunction();
	if(!$app->islogged()){
		echo "<script>window.top.location.href = 'logout.php';</script>";	
	}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="initial-scale=1.0,user-scalable=yes"/>

<link rel="stylesheet" href="../css/estilo2.css">
<link rel="stylesheet" href="../css/jquery.dataTables.min.css">
 <script language="javascript" type="text/javascript" src="../js/gateway.js"></script>
<script language="javascript" type="text/javascript" src="../js/jquery-1.12.4.js"></script>
<script language="javascript" type="text/javascript" src="../js/jquery.dataTables.min.js"></script>
<script language="javascript" type="text/javascript" src="../js/numeral.min.js"></script>
  <script language="javascript" type="text/javascript" src="../js/eficiencia.js"></script>
	<script language="javascript" src="../js/xlsx.full.min.js"></script>
	
	<script language="javascript" src="../js/FileSaver.js"></script>
	
	<script language="javascript" src="../js/tableexport.js"></script>
<title></title>

</head>
<body onload="getstatus();">
<main>
	
	<section id="titulo">
		<center><h2>Reporte de Eficiencia, Eficacia y Productividad</h2>
		</center>
	</section>
<div>
	<form name="f1" action="#">
		<center>
		<div ">
		<div id="myDiv">

				<!--div class="txt">
					Fecha Inicial<br>
					<input type="date"  id="txtfechainicial" required="required" class="TT" />	
				</div>
				<div class="txt">
					Ingrese el pedido SAE<br>
					<input type="text"  id="txtpedidosae" required="required" class="TT" />	
				</div-->

				<div class="txt">
					Satus<br>
					<select id="optionstatus" onChange="getOrdenes(this.value);"></select>
				</div>

				<div class="txt">
					Orden<br>
					<select id="optionorder" onChange="getdata();"><option value=0>---Seleccione---</option></select>
				</div>

			

				<div class="txt">
						<input type="button" name="insertar" Value="Actualizar" onClick="getdata();" id="ok"/>
				</div>
			<div >
				<img src="../img/load.gif" class="imgload" id="imgload">
			</div>
		</div>
		</center>
	</form>
</div>
<center>
</br>

	</br>
	<center>
		<div id="sin2" style="display:none;">
			<div id="myDiv">
				<div class="txt">
					Ingresa el pedido SAE a buscar<br>
					<input type="text" name="num" placeholder="Pedido SAE" id="txtbuscar" onkeyup="doSearch()" required="required" class="TT" />
				
				</div>
				
			
			</div>
			
		</div>
	</center>

<div id="scro">
 <div id="tabla" style="display:">
	<table class="tbl-qa">
		  <thead>
			 <tr>
				
				<th class="table-header" >Código SAE.</th>
				<th class="table-header" id='codigosae' ></th>
				<th class="table-header" >Producto.</th>
				<th class="table-header" id='productonombre'></th>
	                </tr>


			 <tr>
				<th class="table-header" >Descripción</th>
				<th class="table-header" colspan="3" id='descripcion'></th>
	                </tr> 		  
		 </thead>
		
	</table>

	<br><br><br>

     <table class="tbl-qa display nowrap" style="width:100%" id="resultado">
		  <thead>
			 <tr>
				
				<th class="table-header" >No. de Ticket</th>
				<th class="table-header">Operador</th>
				<th class="table-header">Eficiencia</th>
				<th class="table-header">Eficacia</th>
				<th class="table-header">Productividad</th>
				<th class="table-header">Máquina</th>
				<th class="table-header">Descripción de Proceso</th>
				
				
	                </tr>

			 
 		  </thead>
		
		  <tbody style="height:250px;overflow:scroll">				
             	 </tbody>
              </table>
 </div>
	</br></br>
		<div id="sin" style="display:none;">
			<div class='myDiv'><div class='txt'>SIN DATOS ENCONTRADOS</div></div>
		</div>
 </div>
</center>
</div>


</main>
</body>
</html>
