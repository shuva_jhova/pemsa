﻿<?php
require_once("usersFunction.php");
$app = new usersFunction();
	if(!$app->islogged()){
		echo "<script>window.top.location.href = 'logout.php';</script>";	
	}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="initial-scale=1.0,user-scalable=yes"/>

<link rel="stylesheet" href="../css/estilo2.css">
<link rel="stylesheet" href="../css/jquery.dataTables.min.css">
 <script language="javascript" type="text/javascript" src="../js/gateway.js"></script>
<script language="javascript" type="text/javascript" src="../js/jquery-1.12.4.js"></script>
<script language="javascript" type="text/javascript" src="../js/jquery.dataTables.min.js"></script>
  <script language="javascript" type="text/javascript" src="../js/maquinas.js"></script>
<title></title>

</head>
<body onload="getmaquinas();">
<main>
	
	<section id="titulo">
		<center><h2>Máquinas Registradas del Sistema</h2>
		</center>
	</section>
<div>
	<form name="f1" action="#">
		<center>
		<div id="sin2" style="display:none;">
			<div id="myDiv">
				<!--div class="txt">
					Ingresa el Nombre de la Máquina a Buscar<br>
					<input type="text" name="num" placeholder="Nombre de la máquina" id="txtbuscar" onkeyup="doSearch()" required="required" class="TT" />
				
				</div-->
				<!--div class="txt">
						<input type="button" name="insertar" Value="Actualizar" onClick="getmaquinas();" id="ok"/>
				</div-->
			<div >
				<img src="../img/load.gif" class="imgload" id="imgload">
			</div>
		</div>
		</center>
	</form>
</div>
<center>
</br>
	<div class="txt">
			<input type="button" name="insertar" Value="Agregar" onClick="agregar();" id="ok"/>
	</div>
	</br>
<div id="scro">
 <div id="tabla">
     <table class="tbl-qa display nowrap" style="width:100%" id="resultado">
		  <thead>
		 <tr>
		<th class="table-header" >Nombre</th>
		<th class="table-header" >Proceso</th>
                <th class="table-header" >Estandar</th>
	        <th class="table-header" >U.M.</th>
	        <th class="table-header" >Tiempo de ajuste(min)</th>
	        <th class="table-header" >Merma de ajuste(Pliegos)</th>
	        <th class="table-header" >Merma del proceso (%)</th>
	        <th class="table-header" >U.M. Entrada</th>
	        <th class="table-header" >U.M. Salida</th>
	        <th class="table-header" >USD</th>
	        <th class="table-header" >MX</th>
		<th class="table-header" >Activo</th>
		<th class="table-header" >Editar</th>
		<th class="table-header" >Eliminar</th>
		
              </tr>
 		  </thead>
		  <tbody style="height:250px;overflow:scroll">				
             	 </tbody>
              </table>
 </div>
	</br></br>
		<div id="sin" style="display:none;">
			<div class='myDiv'><div class='txt'>NO HAY MÁQUINAS REGISTRADOS EN EL SISTEMA </div></div>
		</div>
 </div>
</center>
</div>

<div id="myModal" class="modal">

  <!-- Modal content -->
  <div class="modal-content"> 
  <div class="modal-content2">	
    <span class="close">&times;</span>
			<section id="titulo">
        			<center></br><h2>Ingrese los datos de la nueva máquina</h2>
					     <h5>Asegurese de que los datos introducidos sean correctos</h5>
				</center>
			</section>
			</br>
	<div class="contenedor">
			
	<div class="myDiv" >
		<div class="txt">
			Nombre de la máquina<br>
			<input type="text" name="num" placeholder="Nombre" id="txtnombre" required="required" class="TT" />	
		</div>
		<div class="txt">
			Proceso <br>
			<input type="text" name="num" placeholder="Proceso" id="txtproceso" required="required" class="TT" />	
		</div>
		<div class="txt">
			Estandar<br>
			<input type="number" name="num" placeholder="Estandar" id="txtestandar" required="required" class="TT" />	
		</div>
		<div class="txt">
			U.M.<br>
			<input type="text" name="num" placeholder="U.M." id="txtum" required="required" class="TT" />	
		</div>
		<div class="txt">
			Tiempo de ajuste (min)<br>
			<input type="number" name="num" placeholder="Tiempo ajuste(min)" id="txttiempoajuste" required="required" class="TT" />	
		</div>
		<div class="txt">
			Merma de Ajuste<br>
			<input type="number" name="num" placeholder="Merma de ajuste" id="txtmermaajuste" required="required" class="TT" />	
		</div>
		
		<div class="txt">
			Merma de proceso (%)<br>
			<input type="text" name="num" placeholder="Merma del proceso (%)" id="txtmermaproceso" onkeyup="dopercentaje(this);" required="required" class="TT" />	
		</div>
		<div class="txt">
			U.M. Entrada <br>
			<select name="num" placeholder="U.M. entrada" id="txtumentrada" required="required" class="TT">
			<option value="">-Selecciona</option>
			<option value="Bobina">Bobina</option>
			<option value="Pliego">Pliego</option>
			<option value="Frame">Frame</option>
			<option value="Pieza">Pieza</option>
			</select>	
		</div>
		<div class="txt">
			U.M. Salida<br>
			<select name="num" placeholder="U.M. Salida" id="txtumsalida" required="required" class="TT">	
			<option value="">-Selecciona</option>
			<option value="Bobina">Bobina</option>
			<option value="Pliego">Pliego</option>
			<option value="Frame">Frame</option>
			<option value="Pieza">Pieza</option>
			</select>
		</div>
		<div class="txt">
			USD<br>
			<input type="number" name="num" placeholder="USD" id="txtusd" required="required" class="TT" />	
		</div>
		<div class="txt">
			MX<br>
			<input type="number" name="num" placeholder="MX" id="txtmx" required="required" class="TT" />	
		</div>				
	</div> 
			

	<center>
	<div class="txt">
	<input type="button" name="insertar" Value="Guardar" onClick="saverecord();" id="ok"/>
	</div>
	</center>
	</div>   
  </div>
  </div>
  
</div>
</main>
</body>
</html>
