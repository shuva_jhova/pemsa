<?php
global $idorden;

$idproceso=$_GET['idpro'];
$app;
$datos=array();
require("../recursos/FPDFF/fpdf.php");

require_once("appControl.php");
$app=new appControl();
date_default_timezone_set('America/Mexico_City');
$B=1;
$s=0;
$esp=4;
 $var=date('d/m/y')." ".date('g:i:s a');

$datos=$app->ticketdata($idproceso);
$vec=$datos[0];
//trigger_error(print_r($datos,true));

$clavesae=$vec['sae'];
while(strlen($clavesae)<8){
	$clavesae="0".$clavesae;
}
$GLOBALS['cve']=$clavesae;

class PDF extends FPDF
{

var $widths;
var $aligns;

function SetWidths($w)
{
    //Set the array of column widths
    $this->widths=$w;
}

function SetAligns($a)
{
    //Set the array of column alignments
    $this->aligns=$a;
}

function Row($data,$border,$fill='D',$nb=0)
{
    //Calculate the height of the row
    if($nb==0){
    for($i=0;$i<count($data);$i++)
        $nb=max($nb,$this->NbLines($this->widths[$i],$data[$i]));
    $h=5*$nb;
     }else{
	 $h=5*$nb;
	}
    //Issue a page break first if needed
    $this->CheckPageBreak($h);
    //Draw the cells of the row
    for($i=0;$i<count($data);$i++)
    {
        $w=$this->widths[$i];
        $a=isset($this->aligns[$i]) ? $this->aligns[$i] : 'L';
        //Save the current position
        $x=$this->GetX();
        $y=$this->GetY();
        //Draw the border
	if($border==1){
        	$this->Rect($x,$y,$w,$h,$fill);
	}
        //Print the text
        $this->MultiCell($w,5,$data[$i],0,$a);
        //Put the position to the right of the cell
        $this->SetXY($x+$w,$y);
    }
    //Go to the next line
    $this->Ln($h);
}

function CheckPageBreak($h)
{
    //If the height h would cause an overflow, add a new page immediately
    if($this->GetY()+$h>$this->PageBreakTrigger)
        $this->AddPage($this->CurOrientation);
}

function NbLines($w,$txt)
{
    //Computes the number of lines a MultiCell of width w will take
    $cw=&$this->CurrentFont['cw'];
    if($w==0)
        $w=$this->w-$this->rMargin-$this->x;
    $wmax=($w-2*$this->cMargin)*1000/$this->FontSize;
    $s=str_replace("\r",'',$txt);
    $nb=strlen($s);
    if($nb>0 and $s[$nb-1]=="\n")
        $nb--;
    $sep=-1;
    $i=0;
    $j=0;
    $l=0;
    $nl=1;
    while($i<$nb)
    {
        $c=$s[$i];
        if($c=="\n")
        {
            $i++;
            $sep=-1;
            $j=$i;
            $l=0;
            $nl++;
            continue;
        }
        if($c==' ')
            $sep=$i;
        $l+=$cw[$c];
        if($l>$wmax)
        {
            if($sep==-1)
            {
                if($i==$j)
                    $i++;
            }
            else
                $i=$sep+1;
            $sep=-1;
            $j=$i;
            $l=0;
            $nl++;
        }
        else
            $i++;
    }
    return $nl;
}
	function Footer()
	{
		$this->SetY(-15);
		// Select Arial italic 8
		$this->SetFont('Arial','I',12);
    		// Print current and total page numbers
    

		/*$this->Cell(90,5, "Autoriz�",'',0,'C');
		$this->SetX(-114);
		$this->Cell(90,5, "Revis�",'',0,'C');

		$this->SetY(-30);
		$this->Cell(90,5, "Nombre y firma",'T',0,'C');
		$this->SetX(-114);
		$this->Cell(90,5, "Nombre y firma",'T',0,'C');
		$this->Ln(15);*/
		;
		$this->SetFont('Arial','I',6);
		$this->Cell(0,10,'P�gina '.$this->PageNo().' de {nb}',0,0,'C');



	}

	function Header()
{
// Logo

$this->SetFont('Arial','',9);
$this->SetXY(49,10);
$this->Cell(56,5,'Nombre del Documento',1,0,'C');
$this->SetXY(105,10);
$this->Cell(90,5,'Ticket de Producci�n',1,0,'C');
$this->Image('encabezado.png',12,10,110);
$this->SetXY(49,15);
$this->Cell(20,5,'Revisi�n',1,0,'C');
$this->SetXY(69,15);
$this->Cell(23,5,'Fecha',1,0,'C');
$this->SetXY(92,15);
$this->Cell(41,5,'Elabor�',1,0,'C');
$this->SetXY(133,15);
$this->Cell(40,5,'Autoriz�',1,0,'C');
$this->SetXY(173,15);
$this->Cell(22,5,'C�digo',1,0,'C');
$this->SetXY(49,20);
$this->Cell(20,5,'01',1,0,'C');
$this->SetXY(69,20);
$this->Cell(23,5,'03.Julio.2018',1,0,'C');
$this->SetXY(92,20);
$this->Cell(41,5,'Ram�n Rios Hern�ndez',1,0,'C');
$this->SetXY(133,20);
$this->Cell(40,5,'Jes�s P�rez Miranda',1,0,'C');
$this->SetXY(173,20);
$this->Cell(22,5,'R-PR-01',1,0,'C');
$this->Line(15, 30, 195, 30);

$this->Ln(20);
}
}


$pdf=new PDF();
//$pdf=new PDF_MC_Table();
$pdf->AliasNbPages();
$pdf->PageNo();
$pdf->SetAuthor('JVL');
$pdf->setMargins(15,5);
$pdf->AddPage();
$pdf->SetTitle("PEMSA");
$pdf->SetAutoPageBreak(true,15);
$pdf->SetTopMargin(40);


//$pdf->Ln(20);
$pdf->SetFont('Arial','',9);
$pdf->SetWidths(array(25,15,25,15,25,15,25,40));
$pdf->Row(array('Folio del ticket: ',$vec['folio'],'Pedido SAE: ',$vec['pedidosae'],' Orden Maestra: ',$vec['idorden'],' Fecha Emisi�n: ',$vec['fechaemision']),0);


$pdf->SetFillColor(220,220,220);
$pdf->Ln();
$pdf->SetFont('Arial','B',9);
$pdf->SetWidths(array(180));
$pdf->Row(array('Producto: '),1,'F');


$pdf->SetFont('Arial','B',9);
$pdf->SetWidths(array(132,28,20));
$pdf->Row(array(iconv('UTF-8', 'windows-1252',$vec['producto']),'C�digo SAE: ',$clavesae),0);


$pdf->SetFillColor(220,220,220);
//$pdf->Ln();
$pdf->SetFont('Arial','B',9);
$pdf->SetWidths(array(180));
$pdf->Row(array('Descripci�n: '),1,'F');
$pdf->SetFont('Arial','B',8);
$pdf->SetWidths(array(180));
$pdf->Row(array(iconv('UTF-8', 'windows-1252',$vec['descripcionproducto'])),0);


$pdf->Ln();
$pdf->SetFont('Arial','',8);
$pdf->SetWidths(array(50,130));
$pdf->Row(array('Ticket de producci�n para: ',iconv('UTF-8', 'windows-1252',$vec['proceso'])),0,'D');

$pdf->SetWidths(array(50,130));
$pdf->Row(array('Descripci�n del Proceso: ',iconv('UTF-8', 'windows-1252',$vec['descripcionproceso'])),0,'D');
//$pdf->Row(array(iconv('UTF-8', 'windows-1252',$datos[0]['detalleproducto'][0]['descripcion'])),0);


$pdf->Ln();
$pdf->SetAligns(array('C','C','C','C','C','C','C','C'));
$pdf->SetWidths(array(30,25,30,30,39,26));
$pdf->Row(array('Cantidad de Producto','M�quina','Tiempo Ajuste(min)','Tiempo Proceso(min)','Operador','Formatos/Sustrato'),0);
$pdf->Row(array(iconv('UTF-8', 'windows-1252',number_format($vec['cantidadproducir'])." Pzs"),iconv('UTF-8', 'windows-1252',$vec['maquina']),iconv('UTF-8', 'windows-1252',$vec['tiempoajuste']),ceil(($vec['cantidadticket']/$vec['estandarturno'])*480),iconv('UTF-8', 'windows-1252',$vec['operador']),iconv('UTF-8', 'windows-1252',$vec['piezasformato'])),0);

$pdf->Ln(3);
$pdf->SetWidths(array(30,30,30,30,30,30));
$pdf->Row(array('Cantidad de Proceso','Unidad de Medida','Merma','Fecha Inicio','Hora Inicio','Hora Fin'),0);
$pdf->Row(array((number_format($vec[cantidadticket])),iconv('UTF-8', 'windows-1252',$vec['umproceso']),number_format(iconv('UTF-8', 'windows-1252',$vec['mermadada'])),iconv('UTF-8', 'windows-1252',$vec['fecharequerida']),iconv('UTF-8', 'windows-1252',$vec['horarequerida']),iconv('UTF-8', 'windows-1252',$vec['horafin'])),0);
$pdf->Ln(3);

$pdf->SetFillColor(220,220,220);
$pdf->SetFont('Arial','B',9);
$pdf->SetWidths(array(180));
$pdf->Row(array('Especificaciones Generales: '),1,'F');
$pdf->Ln(3);
$pdf->SetWidths(array(10,150,20));
$pdf->SetAligns(array('C','C','C'));
$pdf->Row(array('No.','Especificaci�n','Revisada'),1);
$pdf->SetAligns(array('C','L','C'));
$pdf->SetFont('Arial','',9);
for($a=0;$a<5;$a++){
	$b=$a+1;
	if($color){
		$pdf->SetFillColor(242, 242, 242);
		
		$pdf->Row(array($b,iconv('UTF-8', 'windows-1252',$vec['espge'.$b]),''),1,'FD');
	}else{
		$pdf->SetFillColor(217, 217, 217);
		$pdf->Row(array($b,iconv('UTF-8', 'windows-1252',$vec['espge'.$b]),''),1,'FD');
	}
	$color=!$color;	
}

/*$pdf->Ln();
$pdf->SetFont('Arial','',9);
$pdf->SetWidths(array(30,8,8,30,104));
$pdf->SetAligns(array('L','L','L','L','L',));
$pdf->Row(array('Conformidad del supervisor: ','SI','NO',' Nombre y Firma',''),1,'D');
$pdf->SetFillColor(220,220,220);


//$pdf->Ln();
$pdf->SetFont('Arial','',9);
$pdf->SetWidths(array(45,135));
$pdf->Row(array('1.- Especificaci�n y/o Insumo:',iconv('UTF-8', 'windows-1252',$vec['esp1'])),0,'D');
$pdf->SetWidths(array(55,125));
$pdf->Row(array('2.- Especificaci�n y/o Dimensiones:',iconv('UTF-8', 'windows-1252',$vec['esp2'])),0,'D');
$pdf->SetWidths(array(60,120));
$pdf->Row(array('3.- Especificaci�n y/o Tinta y/o Pantone: ',iconv('UTF-8', 'windows-1252',$vec['esp3'])),0,'D');
$pdf->SetWidths(array(50,130));
$pdf->Row(array('4.- Especificaci�n y/o Acabado: ',iconv('UTF-8', 'windows-1252',$vec['esp4'])),0,'D');
$pdf->Row(array('5.- Especificaci�n y/o Empaque: ',iconv('UTF-8', 'windows-1252',$vec['esp5'])),0,'D');
*/
$pdf->Ln(5);
$pdf->SetFillColor(220,220,220);
$pdf->SetFont('Arial','B',9);
$pdf->SetWidths(array(180));
$pdf->SetAligns(array('C'));
$pdf->Row(array('Especificaciones Cr�ticas: '),1,'F');
$pdf->Ln(3);
$pdf->SetWidths(array(10,110,10,10,10,10,10,10));
$pdf->SetAligns(array('C','C','C','C','C','C','C','C'));
$pdf->Row(array('No','Especificaci�n','1','2','3','4','5','6'),1);
$pdf->SetAligns(array('C','L','C','C','C','C','C','C'));
$pdf->SetFont('Arial','',9);
for($a=0;$a<3;$a++){
	$b=$a+1;
	if($color){
		$pdf->SetFillColor(242, 242, 242);
		$pdf->Row(array($b,iconv('UTF-8', 'windows-1252',$vec['esp'.$b]),'','','','','',''),1,'FD');
	}else{
		$pdf->SetFillColor(217, 217, 217);
		$pdf->Row(array($b,iconv('UTF-8', 'windows-1252',$vec['esp'.$b]),'','','','','',''),1,'FD');
	}
	$color=!$color;	
}
/*$pdf->SetFont('Arial','',9);
$pdf->SetWidths(array(44,136));
$pdf->SetAligns(array('L','L','L','L','L',));
$pdf->Row(array(' Nombre y Firma del Supervisor',''),1,'D');
$pdf->SetFillColor(220,220,220);
*/
$time1 = strtotime($vec['horainicio']);
$time2 = strtotime($vec['horatermino']);
$difference = round(abs($time2 - $time1) / 60,2);
if($vec['horatermino']=='00:00:00'){
 	$difference=0;
}

$pdf->Ln();
$pdf->SetWidths(array(26,25,26,25,26,26,26));
$pdf->SetAligns(array('C','C','C','C','C','C','C','C'));
$pdf->Row(array('Fecha Inicio','Hora Incio','Fecha Fin','Hora Fin','Total Minutos','Cantidad �til de Proceso','Merma'),0);
//$pdf->Row(array('','','','','','',''),1);
$pdf->Row(array(iconv('UTF-8', 'windows-1252',$vec['fechainicio']),iconv('UTF-8', 'windows-1252',$vec['horainicio']),iconv('UTF-8', 'windows-1252',$vec['fechatermino']),iconv('UTF-8', 'windows-1252',$vec['horatermino']),$difference,iconv('UTF-8', 'windows-1252',$vec['cantidadutil']),iconv('UTF-8', 'windows-1252',$vec['mermareal'])),0);

$pdf->Ln();
$pdf->SetFillColor(220,220,220);
$pdf->SetFont('Arial','B',9);
$pdf->SetWidths(array(180));
$pdf->Row(array('Responsables:'),1,'F');
$pdf->Ln(3);
$pdf->SetFont('Arial','',9);
$pdf->SetWidths(array(60,60,60));
$pdf->SetAligns(array('C','C','C'));
$pdf->Row(array('Jefe de Taller','Calidad','Operador'),1,'D');
$pdf->Row(array('','',''),1,'D',5);
$pdf->SetWidths(array(60,60,60));
$pdf->SetAligns(array('C','C','C'));
$pdf->Row(array('Nombre y Firma','Nombre y Firma','Nombre y Firma'),1,'D');
$pdf->SetFillColor(220,220,220);



$pdf->SetFillColor(220,220,220);
$pdf->Ln();
$pdf->SetFont('Arial','B',10);
$pdf->SetWidths(array(260));



		
	
$pdf->Output();
?>