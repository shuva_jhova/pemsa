<?php
require_once("dbControl.php");
class usersFunction
{ 
	var $app;
	public function __construct()
		{
			$this->app = new dbControl();
		}	
	
	//inician funciones de logueo y control de usuarios//
	public function login($user,$pass)
	{
		date_default_timezone_set ("America/Mexico_City");
		$pass2=md5($pass);		
		session_start();


		$datos=array();
		$result=$this->app->getrecord("usuarios","usuarios.ultimoIngreso,usuarios.enSesion,usuarios.usuario,usuarios.idPerfil,usuarios.operador,usuarios.vendedor,usuarios.id, concat(persona.nombre,' ',persona.primerApellido,' ',persona.segundoApellido) as 'nombre'"," LEFT JOIN persona on persona.id=usuarios.idPersona where usuario like '$user' and password like '$pass2' and usuarios.activo like '1'");
		for($a=0;$a<sizeof($result);$a++)
    		{
			$vec=$result[$a];	
			 $datetime1 = date_create($vec['ultimoIngreso']);
       			$datetime2 = date_create(date("Y-m-d H:i:s"));
    
    			$interval = date_diff($datetime1, $datetime2);
    			//trigger_error($interval->d);
			//trigger_error($interval->i);
			//trigger_error($interval->h);
			//trigger_error($vec['enSesion']);
			if(/*$interval->d>0 || $interval->h>0 || $interval->i>15 || $vec['enSesion']==0*/true){
				$datos[] = array(
					'usuario'=>$vec['usuario'],
					'idperfil'=>$vec['idPerfil'],
					'operador'=>$vec['operador'],
					'vendedor'=>$vec['vendedor'],
					'nombre'=>$vec['nombre'],
					'idusuario'=>$vec['id']				
					);
				$ses=session_id();
				$_SESSION['idusuario']=$vec['id'];
				$_SESSION['quien']=$vec['usuario'];
				$_SESSION['idsession']=$ses;
				$this->app->updaterecord("usuarios","ultimoIngreso=now(),enSesion='1',sesionId='$ses'"," where usuarios.id=$vec[id]");
			}
			else{
				$datos[] = array(
					'usuario'=>'en sesion',
					'idperfil'=>$vec['idPerfil'],
					'operador'=>$vec['operador'],
					'vendedor'=>$vec['vendedor'],
					'idusuario'=>$vec['id']				
					);
			}
			
                }
		
		
	return $datos; 
	}
	public function islogged(){
		date_default_timezone_set ("America/Mexico_City");
		$res=false;
		session_start();
		//trigger_error($_SESSION['idusuario']);	
		if(empty($_SESSION['idusuario'])){
			
			//header("Location: logout.php");
			//session_destroy();
			$res=false;
		}else{

			
			$id=$_SESSION['idusuario'];
			$idsession=$_SESSION['idsession'];
			$result=$this->app->getrecord("usuarios","*","where id=$id and activo like '1'");
			for($a=0;$a<sizeof($result);$a++)
    			{
				 $vec=$result[$a];
				 $datetime1 = date_create($vec['ultimoIngreso']);
       				 $datetime2 = date_create(date("Y-m-d H:i:s"));
    				 $interval = date_diff($datetime1, $datetime2);
				//trigger_error($interval->d);
				//trigger_error($interval->i);
				//trigger_error($interval->h);
				
				if($idsession==$vec['sesionId'] /*&& $interval->d<=0 && $interval->h<=5*/ && $interval->i<=15){
					$res=true;
					$this->app->updaterecord("usuarios","ultimoIngreso=now()"," where usuarios.id=$id");
				}else{
					session_destroy();
				 	$res=false;
				}	
       			}
		}
		//$res=true;//variable for this function always return true
	return $res;
	}
	public function getUserinfo($id){
		$datos=array();
		$result=$this->app->getrecord("usuarios","*","where id=$id");
		for($a=0;$a<sizeof($result);$a++)
    		{
			$vec=$result[$a];
		
			$datos[] = array(
				'usuario'=>$vec['usuario'],
				'idperfil'=>$vec['idPerfil'],
				'idusuario'=>$vec['id']				
				);
		
			$this->app->updaterecord("usuarios","ultimoIngreso=now(),enSesion='1',sesionId='$ses'"," where usuarios.id=$vec[id]");
                }
			
	
	return $datos;
	}
	public function modulosporusuario($idusuario){
		$datos=array();
		$datos2=array();
		$result=$this->app->getrecord("usuarios","modulos.id,modulos.nombre,modulos.ruta","INNER JOIN relperfil_usuario on relperfil_usuario.idUsuario=usuarios.id INNER JOIN modulos on modulos.id=relperfil_usuario.idModulo where usuarios.id=$idusuario and modulos.activo like '1' and usuarios.activo like '1' order by modulos.nombre asc");
		
		//perfiles$result=$this->app->getrecord("usuarios","modulos.id,modulos.nombre,modulos.ruta","INNER JOIN relperfil_usuario on relperfil_usuario.idPerfil=usuarios.idPerfil INNER JOIN modulos on modulos.id=relperfil_usuario.idModulo where usuarios.id=$idusuario and modulos.activo like '1'");
		for($a=0;$a<sizeof($result);$a++)
    		{
			$vec=$result[$a];
			$datos2=array();
		
			$result2=$this->app->getrecord("submodulos","submodulos.id,submodulos.nombre,submodulos.ruta ","INNER JOIN relsubmodulo_usuario on relsubmodulo_usuario.idSubmodulo=submodulos.id where submodulos.idModulo=$vec[id] and relsubmodulo_usuario.idUsuario=$idusuario and submodulos.activo like '1' order by submodulos.nombre asc");
			for($aa=0;$aa<sizeof($result2);$aa++)
    			{
				$vec2=$result2[$aa];
				$datos2[] = array(
					'nombre'=>$vec2['nombre'],
					'ruta'=>$vec2['ruta']			
				);
			
			}
			$datos[]=array (
					'modulo'=>$vec['nombre'],
					'ruta'=>$vec['ruta'],
					'submodulos'=>$datos2		
				);
			unset($datos2);	
                }
		


	return $datos; 
	}

	public function getmodulos(){
		$datos=array();
		$datos2=array();
		
		$result=$this->app->getrecord("modulos","modulos.id,modulos.nombre,modulos.ruta","where modulos.activo like '1'");
		for($a=0;$a<sizeof($result);$a++)
    		{
			$vec=$result[$a];
			$datos2=array();
		
			$result2=$this->app->getrecord("submodulos","*","where idModulo=$vec[id] and activo like '1'");
			for($aa=0;$aa<sizeof($result2);$aa++)
    			{
				$vec2=$result2[$aa];
				$datos2[] = array(
					'id'=>$vec2['id'],
					'nombre'=>$vec2['nombre'],
					'ruta'=>$vec2['ruta']			
				);
			
			}
			$datos[]=array (
					'idmodulo'=>$vec['id'],
					'modulo'=>$vec['nombre'],
					'ruta'=>$vec['ruta'],
					'submodulos'=>$datos2		
				);
			unset($datos2);	
                }
		


	return $datos; 
	}

	public function saveuser($nombre,$appat,$apmat,$usuario,$pass1,$mods,$submods,$operador,$vendedor){
		$datos=array();
		$pass=md5($pass1);
		$result=$this->app->getrecord("usuarios","*","where usuario like '$usuario'");
				
		if (sizeof($result) > 0) {
			$datos[]=array(
				'exito'=>'ya',
				'folio'=>0
			);
		} 
		else 
		{
			$id=$this->app->saverecord('persona',"null,'$nombre','$appat','$apmat','','','','','','','','','','','','','','','','','','','0','1'");			
			$id2=$this->app->saverecord('usuarios',"null,$id,-2,'$usuario','$pass','1','','0','','$operador','$vendedor'");
			for($a=0;$a<sizeof($mods);$a++){
				$this->app->saverecord('relperfil_usuario',"null,$mods[$a],2,$id2,'','','1'");
			}
			for($b=0;$b<sizeof($submods);$b++){
				$this->app->saverecord('relsubmodulo_usuario',"null,$submods[$b],-2,$id2,'','1'");
			}			
			if($id>0 && $id2>0){
				$datos[]=array(
					'exito'=>'si',
					'folio'=>$id2
				);	
			}else{
				$datos[]=array(
					'exito'=>'no',
					'folio'=>0
				);	
			}			
		}


		
	return $datos;

	}

	public function getusuarios(){
		$datos=array();
		$result=$this->app->getrecord("usuarios","usuarios.id,usuarios.operador,usuarios.vendedor,persona.nombre,persona.primerApellido,persona.segundoApellido,usuarios.usuario,usuarios.activo","INNER JOIN persona on persona.id=usuarios.idPersona where usuarios.id>2");
		for($a=0;$a<sizeof($result);$a++)
    		{
			$vec=$result[$a];
//trigger_error($vec['id']);
			$datos[] = array(
				'id'=>$vec['id'],
				'nombre'=>$vec['nombre'],
				'appat'=>$vec['primerApellido'],
				'apmat'=>$vec['segundoApellido'],
				'usuario'=>$vec['usuario'],
				'activo'=>$vec['activo'],
				'operador'=>$vec['operador'],
				'vendedor'=>$vec['vendedor'],
				'modulos'=>$this->modulosporusuario($vec['id'])
				);
		
                }
		
		
	return $datos; 
	}

	public function changepassword($id,$pass){
		$datos=array();
		$pass2=md5($pass);
		$result=$this->app->getrecord("usuarios","*","where usuarios.id=$id ");
		if (sizeof($result) > 0)
    		{
				if($this->app->updaterecord("usuarios","password='$pass2'"," where usuarios.id=$id")){
					$datos[]=array(
						'exito'=>'si',
						'id'=>$id
					);
							
				}
		
                }else{
			$datos[]=array(
					'exito'=>'sin',
					'folio'=>0
				);
		}
		
		
	return $datos; 
	}
	public function updateusuario($id,$nombre,$appat,$apmat,$usuario,$activo,$mods,$submods,$operador,$vendedor){
//trigger_error($vendedor);
		$datos=array();
		$result=$this->app->getrecord("usuarios","usuarios.id,usuarios.idPersona"," where usuarios.id=$id");
		
		if (sizeof($result) > 0) {
			for($a=0;$a<sizeof($result);$a++)
    			{
				$vec=$result[$a];
			
				$idusuario=$vec['id'];
				$idpersona=$vec['idPersona'];
				$this->app->deleterecord("relperfil_usuario"," where idUsuario=$id");
					for($a=0;$a<sizeof($mods);$a++){
						$this->app->saverecord('relperfil_usuario',"null,$mods[$a],2,$id,'','','1'");
					}
		
				$this->app->deleterecord("relsubmodulo_usuario"," where idUsuario=$id");
				
					for($b=0;$b<sizeof($submods);$b++){
						$this->app->saverecord('relsubmodulo_usuario',"null,$submods[$b],2,$id,'','1'");
					}
						
			
				if($this->app->updaterecord("usuarios","usuario='$usuario',activo='$activo',operador='$operador',vendedor='$vendedor'"," where usuarios.id=$id")){
					if($this->app->updaterecord("persona","nombre='$nombre',primerApellido='$appat',segundoApellido='$apmat'"," where persona.id=$idpersona")){
						$datos[]=array(
							'exito'=>'si',
							'folio'=>$idusuario
						);
							
					}		
				}		
				
                	}	
		} 
		else{
			
			$datos[]=array(
					'exito'=>'sin',
					'folio'=>0
				);
		}


	return $datos;	
	}

	public function deleteusuario($id){
//trigger_error($vendedor);
		$datos=array();
		$result=$this->app->getrecord("usuarios","usuarios.id,usuarios.idPersona"," where usuarios.id=$id");
		
		if (sizeof($result) > 0) {
			for($a=0;$a<sizeof($result);$a++)
    			{
				$vec=$result[$a];
			
				$idusuario=$vec['id'];
				$idpersona=$vec['idPersona'];
				$this->app->deleterecord("relperfil_usuario"," where idUsuario=$id");
					
				$this->app->deleterecord("relsubmodulo_usuario"," where idUsuario=$id");		
			
				if($this->app->deleterecord("usuarios","where usuarios.id=$id")>0){
					if($this->app->deleterecord("persona"," where persona.id=$idpersona")>0){
						$datos[]=array(
							'exito'=>'si'
						);
							
					}		
				}		
				
                	}	
		} 
		else{
			
			$datos[]=array(
					'exito'=>'sin'

				);
		}


	return $datos;	
	}

	function logout(){
		header("Location: logout.php");
	}

	//terminan funciones de control de usuarios//
}
?>