<?php
global $idorden;

$idmaquina=$_GET['idm'];
$fechainicio=$_GET['fecini'];
$fechafin=$_GET['fecfi'];
$app;
$datos=array();
require("../recursos/FPDFF/fpdf.php");

require_once("appControl.php");
$app=new appControl();

$B=1;
$s=0;
$esp=4;
$var=date('d/m/y')." ".date('g:i:s a');

$datos=$app->planningbymachineandday($idmaquina,$fechainicio,$fechafin);
$GLOBALS['maq']=$datos[0]['maquina'];
/*
//trigger_error(print_r($datos,true));

$clavesae=$datos[0]['detalleproducto'][0]['codigo'];
while(strlen($clavesae)<8){
$clavesae="0".$clavesae;
}*/
//$GLOBALS['cve']=$clavesae;
$GLOBALS['fechainicio']=$fechainicio;
$GLOBALS['fechafin']=$fechafin;
class PDF extends FPDF
{

var $widths;
var $aligns;

function SetWidths($w)
{
//Set the array of column widths
$this->widths=$w;
}

function SetAligns($a)
{
//Set the array of column alignments
$this->aligns=$a;
}

function Row($data,$border,$fill='D')
{
//Calculate the height of the row
$nb=0;
for($i=0;$i<count($data);$i++)
$nb=max($nb,$this->NbLines($this->widths[$i],$data[$i]));
$h=5*$nb;
//Issue a page break first if needed
$this->CheckPageBreak($h);
//Draw the cells of the row
for($i=0;$i<count($data);$i++)
{
$w=$this->widths[$i];
$a=isset($this->aligns[$i]) ? $this->aligns[$i] : 'L';
//Save the current position
$x=$this->GetX();
$y=$this->GetY();
//Draw the border
if($border==1){
$this->Rect($x,$y,$w,$h,$fill);
}
//Print the text
$this->MultiCell($w,5,$data[$i],0,$a);
//Put the position to the right of the cell
$this->SetXY($x+$w,$y);
}
//Go to the next line
$this->Ln($h);
}

function CheckPageBreak($h)
{
//If the height h would cause an overflow, add a new page immediately
if($this->GetY()+$h>$this->PageBreakTrigger)
$this->AddPage($this->CurOrientation);
}

function NbLines($w,$txt)
{
//Computes the number of lines a MultiCell of width w will take
$cw=&$this->CurrentFont['cw'];
if($w==0)
$w=$this->w-$this->rMargin-$this->x;
$wmax=($w-2*$this->cMargin)*1000/$this->FontSize;
$s=str_replace("\r",'',$txt);
$nb=strlen($s);
if($nb>0 and $s[$nb-1]=="\n")
$nb--;
$sep=-1;
$i=0;
$j=0;
$l=0;
$nl=1;
while($i<$nb)
{
$c=$s[$i];
if($c=="\n")
{
$i++;
$sep=-1;
$j=$i;
$l=0;
$nl++;
continue;
}
if($c==' ')
$sep=$i;
$l+=$cw[$c];
if($l>$wmax)
{
if($sep==-1)
{
if($i==$j)
$i++;
}
else
$i=$sep+1;
$sep=-1;
$j=$i;
$l=0;
$nl++;
}
else
$i++;
}
return $nl;
}
function Footer()
{
$this->SetY(-15);
// Select Arial italic 8
$this->SetFont('Arial','I',12);
// Print current and total page numbers


/*$this->Cell(90,5, "Autoriz�",'',0,'C');
$this->SetX(-114);
$this->Cell(90,5, "Revis�",'',0,'C');

$this->SetY(-30);
$this->Cell(90,5, "Nombre y firma",'T',0,'C');
$this->SetX(-114);
$this->Cell(90,5, "Nombre y firma",'T',0,'C');
$this->Ln(15);*/
$this->SetFont('Arial','I',6);
$this->Cell(0,10,'P�gina '.$this->PageNo().' de {nb}',0,0,'C');



}

function Header()
{

// Logo

$this->SetFont('Arial','',9);
$this->SetXY(49,10);
$this->Cell(115,5,'Nombre del Documento',1,0,'C');
$this->SetXY(164,10);
$this->Cell(115,5,'Carga de trabajo de la M�quina '.$GLOBALS['maq'].' Del '.$GLOBALS['fechainicio'].' al '.$GLOBALS['fechafin'],1,0,'C');
$this->Image('encabezado.png',12,10,110);
$this->SetXY(49,15);
$this->Cell(46,5,'Revisi�n',1,0,'C');
$this->SetXY(95,15);
$this->Cell(46,5,'Fecha',1,0,'C');
$this->SetXY(141,15);
$this->Cell(46,5,'Elabor�',1,0,'C');
$this->SetXY(187,15);
$this->Cell(46,5,'Autoriz�',1,0,'C');
$this->SetXY(233,15);
$this->Cell(46,5,'C�digo',1,0,'C');
$this->SetXY(49,20);
$this->Cell(46,5,'00',1,0,'C');
$this->SetXY(95,20);
$this->Cell(46,5,'03.Julio.2018',1,0,'C');
$this->SetXY(141,20);
$this->Cell(46,5,'Ram�n Rios Hern�ndez',1,0,'C');
$this->SetXY(187,20);
$this->Cell(46,5,'Jes�s P�rez Miranda',1,0,'C');
$this->SetXY(233,20);
$this->Cell(46,5,'R-PR-06',1,0,'C');
$this->Line(15, 30, 280, 30);


/*// Logo
$this->Image('encabezado.png',-1,-1,280);
$this->SetFont('Arial','B',16);
$this->SetX(380);
$this->SetY(20);
$this->Cell(257,10,,0,0,'R');
$this->Ln(7);
$this->Cell(257,10,'Del '.$GLOBALS['fechainicio'].' al '.$GLOBALS['fechafin'],0,0,'R');

$this->Line(10, 35, 285, 35);*/
$this->Ln(15);

}
}



$pdf=new PDF();
//$pdf=new PDF_MC_Table();
$pdf->AliasNbPages();
$pdf->PageNo();
$pdf->SetAuthor('JVL');
$pdf->setMargins(15,0,5);
$pdf->AddPage('L');
$pdf->SetTitle("PEMSA");
$pdf->SetAutoPageBreak(true,15);
$pdf->SetTopMargin(0);
//$pdf->Ln(15);

/*$pdf->SetFont('Arial','',10);
$pdf->SetWidths(array(25,20, 25,25,30,35,35,35,20,30));
$pdf->Row(array('No. Orden: ',$idorden,' Pedido SAE: ',$datos[0]['pedidosae'],' Fecha emisi�n: ',$datos[0]['fechaemision'],' Fecha requerida: ',$datos[0]['fecharequerida'],' Cantidad: ',$datos[0]['cantidad']),0);*/

//$pdf->Ln();

$pdf->SetFillColor(220,220,220);
//$pdf->Ln();
$pdf->SetFont('Arial','B',10);
$pdf->SetWidths(array(260));

$pdf->SetFillColor(166, 166, 166);

$pdf->SetFont('Arial','B',8);
$pdf->SetWidths(array(11,12,13,52,20,15,26,20,14,12,20,18,18,18));
$pdf->Row(array('Orden','Pedido SAE','Codigo SAE','Descripcion SAE','Fecha Elaboraci�n','Fecha Entrega','Cantidad Proceso a Producir','Formatos / Sustrato','Multiplo Proceso','Horas','Fecha Inicio','Hora Inicio','Fecha Fin','Hora Fin'),1,'FD');

$pdf->SetFont('Arial','',7);

// 

$color=true;

//trigger_error(print_r($datos,true));
for($a=0;$a<sizeof($datos);$a++){
//for($a=0;$a<15;$a++){
	if($color){
		$pdf->SetFillColor(242, 242, 242);
		$pdf->Row(array(iconv('UTF-8', 'windows-1252',$datos[$a]['oroden']),iconv('UTF-8', 'windows-1252',$datos[$a]['pedidosae']),iconv('UTF-8', 'windows-1252',$datos[$a]['codigoproducto']),iconv('UTF-8', 'windows-1252',$datos[$a]['nombreproducto']),iconv('UTF-8', 'windows-1252',$datos[$a]['fechaelaboracion']),$datos[$a]['fechaentrega'],number_format($datos[$a]['pliegos']),iconv('UTF-8', 'windows-1252',$datos[$a]['piezasformato']),iconv('UTF-8', 'windows-1252',$datos[$a]['multiploproceso']),iconv('UTF-8', 'windows-1252',$datos[$a]['horas']),$datos[$a]['fechainicio'],$datos[$a]['horainicio'],$datos[$a]['fechafin'],$datos[$a]['horafin']),1,'FD');
		//$pdf->Row(array(iconv('UTF-8', 'windows-1252',$datos[0]['oroden']),iconv('UTF-8', 'windows-1252',$datos[0]['pedidosae']),iconv('UTF-8', 'windows-1252',$datos[0]['codigoproducto']),iconv('UTF-8', 'windows-1252',$datos[0]['nombreproducto']),iconv('UTF-8', 'windows-1252',$datos[0]['fechaelaboracion']),$datos[0]['fechaentrega'],$datos[0]['cantidadordenada'],iconv('UTF-8', 'windows-1252',$datos[0]['piezasformato']),iconv('UTF-8', 'windows-1252',$datos[0]['multiploproceso']),iconv('UTF-8', 'windows-1252',$datos[0]['pliegos']),iconv('UTF-8', 'windows-1252',$datos[0]['horas']),$datos[0]['fechainicio'],$datos[0]['horainicio'],$datos[0]['fechafin'],$datos[0]['horafin']),1,'FD');
	}else{
		$pdf->SetFillColor(217, 217, 217);
		//$pdf->Row(array(iconv('UTF-8', 'windows-1252',$datos[$a]['oroden']),iconv('UTF-8', 'windows-1252',$datos[$a]['pedidosae']),iconv('UTF-8', 'windows-1252',$datos[$a]['codigoproducto']),iconv('UTF-8', 'windows-1252',$datos[$a]['nombreproducto']),iconv('UTF-8', 'windows-1252',$datos[$a]['fechaelaboracion']),$datos[$a]['fechaentrega'],$datos[$a]['cantidadordenada'],iconv('UTF-8', 'windows-1252',$datos[$a]['piezasformato']),iconv('UTF-8', 'windows-1252',$datos[$a]['multiploproceso']),iconv('UTF-8', 'windows-1252',$datos[$a]['horas']),$datos[$a]['fechainicio'],$datos[$a]['horainicio'],$datos[$a]['fechafin'],$datos[$a]['horafin']),1,'FD');
		$pdf->Row(array(iconv('UTF-8', 'windows-1252',$datos[$a]['oroden']),iconv('UTF-8', 'windows-1252',$datos[$a]['pedidosae']),iconv('UTF-8', 'windows-1252',$datos[$a]['codigoproducto']),iconv('UTF-8', 'windows-1252',$datos[$a]['nombreproducto']),iconv('UTF-8', 'windows-1252',$datos[$a]['fechaelaboracion']),$datos[$a]['fechaentrega'],number_format($datos[$a]['pliegos']),iconv('UTF-8', 'windows-1252',$datos[$a]['piezasformato']),iconv('UTF-8', 'windows-1252',$datos[$a]['multiploproceso']),iconv('UTF-8', 'windows-1252',$datos[$a]['horas']),$datos[$a]['fechainicio'],$datos[$a]['horainicio'],$datos[$a]['fechafin'],$datos[$a]['horafin']),1,'FD');
	}
	$color=!$color; 
}


$pdf->Output();
?>

