﻿<?php
require_once("usersFunction.php");
$app = new usersFunction();
	if(!$app->islogged()){
		echo "<script>window.top.location.href = 'logout.php';</script>";	
	}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="initial-scale=1.0,user-scalable=yes"/>

<link rel="stylesheet" href="../css/estilo2.css">
<link rel="stylesheet" href="../css/jquery.dataTables.min.css">
 <script language="javascript" type="text/javascript" src="../js/gateway.js"></script>
<script language="javascript" type="text/javascript" src="../js/jquery-1.12.4.js"></script>
<script language="javascript" type="text/javascript" src="../js/jquery.dataTables.min.js"></script>
  <script language="javascript" type="text/javascript" src="../js/productos.js"></script>
<title></title>

</head>
<body onload="getproductos();">
<datalist id="paises"></datalist>
<datalist id="presentacion"></datalist>
<main>
	
	<section id="titulo">
		<center><h2>Productos Registrados</h2>
		</center>
	</section>
<div>
	<form name="f1" action="#">
		<center>
		<div id="sin2" style="display:none;">
			<div id="myDiv">
				<!--div class="txt">
					Ingresa el Nombre del Producto a Buscar<br>
					<input type="text" name="num" placeholder="Nombre del producto" id="txtbuscar" onkeyup="doSearch()" required="required" class="TT" />
				
				</div>
				<div class="txt">
						<input type="button" name="insertar" Value="Actualizar" onClick="getproductos();" id="ok"/>
				</div-->
			
		</div>
		
		</center>
	</form>
<!--/div-->
<center>
</br>
	<div class="txt">
			<input type="button" name="insertar" Value="Agregar" onClick="agregar();" id="ok"/>
	</div>
		<div >
			<img src="../img/load.gif" class="imgload" id="imgload">
		</div>
	</br>

<!--/////////////////////////////////////////////////////principal products table//////////////////////////////////////////////////////////////////////////////-->


<div id="scro">
	<center>
		<section id="titulo">
				<center><h4>Simbología de Colores</h2>
				</center>
			</section>
				<br>
		<div id="myDiv">
			
			<div class="txt">
				Faltan todas las especificaciones.<br>
				<img  style="background-color: #f7786b; height: 30px; width: 30px;"/>
				
			</div>
			<div class="txt">
				Falta de especificaciones de Calidad.<br>
					
				<img  style="background-color: #96ceb4; height: 30px; width: 30px;" />
			</div>
			<div class="txt">
				Falta de especificacines de Ingeniería.<br>
					
				<img  style="background-color: #f2ae72; height: 30px; width: 30px;" />
			</div>
			<div class="txt">
				Especificaciones Completas.<br>
					
				<img  style="background-color:#88d4f7; height: 30px; width: 30px;" />
			</div>
			
		</div>
	</center>
 <div id="tabla">
     <table class="tbl-qa display nowrap" style="width:100%" id="resultado">
		  <thead>
		 <tr>
		<th class="table-header" >Código</th>
		<th class="table-header" >Nombre</th>
		<th class="table-header" >Acciones</th>
		<th class="table-header" >Procesos</th>
		<th class="table-header" >Componentes</th>
		<th class="table-header" >Especificaciones P.T.</th>
		<th class="table-header" >Activo</th>
		<th class="table-header" >Editar</th>
		<th class="table-header" >Descontinuar</th>
		<th class="table-header" >Duplicar producto</th>
		
              </tr>
 		  </thead>
		 <tbody style="height:250px;overflow:scroll">				
             	 </tbody>
     </table>
 </div>
	</br></br>
		<div id="sin" style="display:none;">
			<div class='myDiv'><div class='txt'>NO HAY PRODUCTOS REGISTRADOS EN EL SISTEMA </div></div>
		</div>
 </div>
</center>
</div>




<!--/////////////////////////////////////////////////////add new product//////////////////////////////////////////////////////////////////////////////-->


<div id="myModal" class="modal">

  <!-- Modal content -->
  <div class="modal-content"> 
  <div class="modal-content2">
  <datalist id="prodsaux"></datalist>	
    <span class="close">&times;</span>
			<section id="titulo">
        			<center></br><h2>Ingrese los datos del nuevo producto</h2>
					     <h5>Asegurese de que los datos introducidos sean correctos</h5>
				</center>
			</section>
			</br>
	<div class="contenedor">
			
	<div class="myDiv" >
		<div class="txt">
			Código del producto<br>
			<input type="text" placeholder="Código del producto" list="prodsaux" id="txtcodigo" onBlur="validateSAE();" required="required" maxlength="8" class="TT" />	
		</div>
		<div class="txt">
			Nombre del Producto<br>
			<input type="text" placeholder="Nombre del producto" id="txtnombre" required="required" maxlength="40" class="TT" />	
		</div>
		<div class="txt">
			Descripción del producto<br>
			<textarea rows="4" cols="50" name="dir"  class="TT" id="txtdescripcion" placeholder="Descripción del producto"></textarea> 		
		</div>
		<!--div class="txt">
			Descontinuado<br>
			<select  class="TT" id="optionlinea">
              		</select> 		
		</div-->
		<div class="txt">
			Presentación Venta<br>
			 <input type="text" placeholder="Presentación Venta" value="Pieza" disabled id="txtpresentacionVenta" list="presentacion" required="required" class="TT" />	
		</div>
		<div class="txt">
			Marca del Producto<br>
			 <input type="text" placeholder="Marca del producto" id="txtmarca" required="required" class="TT" />		
		</div>
		<div class="txt">
			Código de Barras<br>
			 <input type="text" placeholder="Código de Barras" id="txtcodigodebarras" required="required" class="TT" />	
		</div>
								

		<div class="txt">
			Código de Barras Empaque<br>
			 <input type="text" placeholder="Código de Barras Empaque" id="txtcodigodebarrasempaque" required="required" class="TT" />	
		</div>
		<div class="txt">
			País de Origen<br>
			<input type="text"  placeholder="País de Origen" id="txtpais" list="paises" required="required" class="TT" />	
		</div>
		<div class="txt">
			Fracción Aracenlaria<br>
			<input type="text"  placeholder="Fracción Arancelaria" id="txtfraccion" required="required" class="TT" />	
		</div>
		<div class="txt">
			Exportación<br>
			<label class="fondotxt">
				<input type='radio' name='exportacion' value='1' checked='checked'">Si
				<input type='radio' name='exportacion' value='0'  checked='checked'">No
			</label>	
		</div>

		<!--div class="txt">
			Barniz<br>
			<label class="fondotxt">
				<input type='radio' name='barniz' value='Mate' checked='checked'">Mate
				<input type='radio' name='barniz' value='Brillante'  checked='checked'">Brillante
			</label>	
		</div>
		<div class="txt">
			Foto<br>
		    <input type="file" id="foto" class="TT" accept="image/x-png,image/gif,image/jpeg"/>
             	    <input type="text" id="txtfoto"  style="visibility:hidden"/>
                    <output id="list2"><div class="imag"></div></output>
                    <div class="imag" id="fotodiv"><img src="" class="thumb" id="imgfoto"/></div>
				
		</div>
		<div class="txt">
			Foto detalle<br>
		    <input type="file" id="fotodetalle" class="TT" accept="image/x-png,image/gif,image/jpeg"/>
             	    <input type="text" id="txtfotodetalle"  style="visibility:hidden"/>
                    <output id="list2"><div class="imag"></div></output>
                    <div class="imag" id="fotodetallediv"><img src="" class="thumb" id="imgfotodetalle"/></div>
		</div-->
		<div class="txt">
			Notas especiales<br>
			<textarea rows="4" cols="50" name="dir"  class="TT" id="txtnotas" placeholder="Notas especiales"></textarea> 		
		</div>
	</div>
			
	<br\>
	<hr>
		<section id="titulo">
			<center></br><h3>Medidas del Producto</h3>
			</center>
		</section>
		<div class="myDiv">
			
			<div class="txt">
				A<br>
				<input type="number" value="0" onFocus="limpiar(this);" onBlur="validatenumber(this);"  placeholder="Ancho" id="txtaproducto" required="required" class="TT" />	
			</div>
			<div class="txt">
				B<br>
				<input type="number" value="0" onFocus="limpiar(this);" onBlur="validatenumber(this);"  placeholder="Largo" id="txtbproducto" required="required" class="TT" />	
			</div>
			<div class="txt">
				H<br>
				<input type="number" value="0" onFocus="limpiar(this);" onBlur="validatenumber(this);"  placeholder="Alto" id="txthproducto" required="required" class="TT" />	
			</div>			

		</div>
	<br\>
	<hr>
		<section id="titulo">
			<center></br><h3>Especificaciones generales</h3>
			</center>
		</section>
		<div class="myDiv">
			
						<div class="txt">
							Insumo<br>
							<textarea rows="4" cols="50" name="dir"  class="TT" id="txtespec1" placeholder="Insumo"></textarea> 	
						</div>
						<div class="txt">
							Dimensiones del Pliego<br>
							<textarea rows="4" cols="50" name="dir"  class="TT" id="txtespec2" placeholder="Dimensiones"></textarea> 	
						</div>
						<div class="txt">
							Tinta y/o Pantone<br>
							<textarea rows="4" cols="50" name="dir"  class="TT" id="txtespec3" placeholder="Tinta y/o Pantone"></textarea> 	
						</div>
						<div class="txt">
							Acabado<br>
							<textarea rows="4" cols="50" name="dir"  class="TT" id="txtespec4" placeholder="Acabado"></textarea> 		
						</div>
						<div class="txt" >
							Barniz<br>
							<textarea rows="4" cols="50" name="dir"  class="TT" id="txtespec5" placeholder="Barniz"></textarea> 	
						</div>

		</div>
	<br\>
	<hr>		
		<section id="titulo">
        			<center></br><h3>Documentos</h3>
				</center>
		</section>
	<center>
		<div class="txt">

			<img src="../img/folder.png" onClick="addfile();"/>
			<!--input type="button" name="insertar" Value="Agregar" onClick="addfile();" id="ok"/-->
		</div>
		<div class="myDiv" id="docs" style="display:none;">
	
			<div class="txt">
				Seleccione el Archivo<br>
		    		<input type="file" id="doc1" class="TT" onchange="enablebtn();"/>
             	   		
				<center>
					<!--input type="button" name="insertar" Value="Guardar" onClick="" id="ok"/-->
					<textarea rows="6" cols="50" name="dir"  class="TT" id="txtfiledescription" placeholder="Descripción del Archivo"></textarea>
					<br>						
					<img src="../img/save2.png" onclick="uploadfiletoserver();" id="imgsavefile" style="display:none;"/>
				</center>
			</div>		
		</div>
	<!--div class="myDiv" id="docs">
	
		<div class="txt">
			Plano de Pliego<br>
		    <input type="file" id="doc1" class="TT" />
             	    <input type="text" id="txtdoc1"  style="visibility:hidden"/>
                    <output id="list2"><div class="imag"></div></output>
                    <div class="imag" id="doc1div"><img src="" class="thumb" id="imgdoc1"/></div>
				
		</div>
		<div class="txt">
			Plano de pieza y Formación<br>
		    <input type="file" id="doc2" class="TT" />
             	    <input type="text" id="txtdoc2"  style="visibility:hidden"/>
                    <output id="list2"><div class="imag"></div></output>
                    <div class="imag" id="doc2div"><img src="" class="thumb" id="imgdoc2"/></div>
		</div>
		<div class="txt">
			Plano de Empalme<br>
		    <input type="file" id="doc3" class="TT" />
             	    <input type="text" id="txtdoc3"  style="visibility:hidden"/>
                    <output id="list2"><div class="imag"></div></output>
                    <div class="imag" id="doc3div"><img src="" class="thumb" id="imgdoc3"/></div>				
		</div>
		<div class="txt">
			Arte<br>
		    <input type="file" id="doc4" class="TT" />
             	    <input type="text" id="txtdoc4"  style="visibility:hidden"/>
                    <output id="list2"><div class="imag"></div></output>
                    <div class="imag" id="doc4div"><img src="" class="thumb" id="imgdoc4"/></div>
		</div>
		<div class="txt">
			Estudio de Empaque<br>
		    <input type="file" id="doc5" class="TT" />
             	    <input type="text" id="txtdoc5"  style="visibility:hidden"/>
                    <output id="list2"><div class="imag"></div></output>
                    <div class="imag" id="doc5div"><img src="" class="thumb" id="imgdoc5"/></div>
				
		</div>
		<div class="txt">
			Anexo 1<br>
		    <input type="file" id="doc6" class="TT" />
             	    <input type="text" id="txtdoc6"  style="visibility:hidden"/>
                    <output id="list2"><div class="imag"></div></output>
                    <div class="imag" id="doc6div"><img src="" class="thumb" id="imgdoc6"/></div>
		</div>
		<div class="txt">
			Anexo 2<br>
		    <input type="file" id="doc7" class="TT" />
             	    <input type="text" id="txtdoc7"  style="visibility:hidden"/>
                    <output id="list2"><div class="imag"></div></output>
                    <div class="imag" id="doc7div"><img src="" class="thumb" id="imgdoc7"/></div>
				
		</div>
		<div class="txt">
			Anexo 3<br>
		    <input type="file" id="doc8" class="TT" />
             	    <input type="text" id="txtdoc8"  style="visibility:hidden"/>
                    <output id="list2"><div class="imag"></div></output>
                    <div class="imag" id="doc8div"><img src="" class="thumb" id="imgdoc8"/></div>
		</div>

		<div class="txt">
			Notas especiales<br>
			<textarea rows="4" cols="50" name="dir"  class="TT" id="txtnotas" placeholder="Notas especiales"></textarea> 		
		</div>
	
	</div-->
	<br\>
	<hr>		
		<section id="titulo">
        			<center></br><h3>Caracteristicas del Empaque</h3>
					     <h5>Dimensiones en mm</h5>
					    <h5>Peso en Kg.</h5>
				</center>
		</section>


		

	<div  id="mm">

		<section id="titulo">
        			<center></br>
					     <h4>Producto</h4>
				</center>
		</section>
	<div class="myDiv">

		<div class="txt">
			Largo<br>
			<input type="number" value="0" onFocus="limpiar(this);" onBlur="validatenumber(this);"  placeholder="Largo" id="txtlargoproducto" required="required" class="TT" />	
		</div>
		<div class="txt">
			Ancho<br>
			<input type="number" value="0" onFocus="limpiar(this);" onBlur="validatenumber(this);"  placeholder="Ancho" id="txtanchoproducto" required="required" class="TT" />	
		</div>
		<div class="txt">
			Alto<br>
			<input type="number" value="0" onFocus="limpiar(this);" onBlur="validatenumber(this);"  placeholder="Alto" id="txtaltoproducto" required="required" class="TT" />	
		</div>		
		
		
		<div class="txt">
			Peso<br>
			<input type="number" value="0" onFocus="limpiar(this);" onBlur="validatenumber(this);"  placeholder="Peso" id="txtpesopresentacionventa" required="required" class="TT" />	
		</div>
	</div>

		<section id="titulo">
        			<center></br>
					     <h4>Inner Pack</h4>
					
				</center>
		</section>



	<div class="myDiv">
		<datalist id="corrugadosinner">
			<option>Bolsa</option>
			<option>Fajilla</option>
			<option>Paquete Kraft</option>
			<option>Termo Encogible</option>
			<option>No Aplica</option>
		</datalist>
		<div class="txt">
			Empaque<br>
			<input type="text"  placeholder="--Seleccione--" id="txtcorrugadoinner" list="corrugadosinner" onBlur="disbleinner(this.value);" required="required" class="TT" />	
		</div>
		<div class="txt">
			Multiplo<br>
			<input type="number" value="0" onFocus="limpiar(this);" onBlur="validatenumber(this);"  placeholder="Multiplo" id="txtpiezascorrugadoinner" required="required" class="TT" />	
		</div>
		<div class="txt">
			Largo <br>
			<input type="number" value="0" onFocus="limpiar(this);" onBlur="validatenumber(this);"  placeholder="Largo" id="txtlargocorrugadoinner" required="required" class="TT" />	
		</div>
		<div class="txt">
			Ancho <br>
			<input type="number" value="0" onFocus="limpiar(this);" onBlur="validatenumber(this);" placeholder="Ancho" id="txtanchocorrugadoinner" required="required" class="TT" />	
		</div>
		
		<div class="txt">
			Alto <br>
			<input type="number" value="0" onFocus="limpiar(this);" onBlur="validatenumber(this);"  placeholder="Alto" id="txtaltocorrugadoinner" required="required" class="TT" />	
		</div>
		
		<div class="txt">
			Peso<br>
			<input type="number" value="0" onFocus="limpiar(this);" onBlur="validatenumber(this);"  placeholder="Peso" id="txtpesoporcorrugadoinner" required="required" class="TT" />	
		</div>
		

	</div>
		<section id="titulo">
        			<center></br>
					     <h4>Master Pack</h4>
				</center>
		</section>



	<div class="myDiv">
		<datalist id="corrugadosmaster">
			<option>Bolsa</option>
			<option>Corrugado</option>
			<option>Fleje</option>
			<option>Paquete Kraft</option>
			<option>Tarima/Granel</option>
		</datalist>
		<div class="txt">
			Empaque<br>
			<input type="text"  placeholder="--Seleccione--" id="txtcorrugadomaster" list="corrugadosmaster" required="required" class="TT" />	
		</div>
		<div class="txt">
			Multiplo<br>
			<input type="number" value="0" onFocus="limpiar(this);" onBlur="validatenumber(this);"  placeholder="Multiplo" id="txtpiezascorrugadomaster" required="required" class="TT" />	
		</div>
		<div class="txt">
			Largo <br>
			<input type="number" value="0" onFocus="limpiar(this);" onBlur="validatenumber(this);"  placeholder="Largo" id="txtlargocorrugadomaster" required="required" class="TT" />	
		</div>
		<div class="txt">
			Ancho <br>
			<input type="number" value="0" onFocus="limpiar(this);" onBlur="validatenumber(this);" placeholder="Ancho" id="txtanchocorrugadomaster" required="required" class="TT" />	
		</div>
		
		<div class="txt">
			Alto <br>
			<input type="number" value="0" onFocus="limpiar(this);" onBlur="validatenumber(this);"  placeholder="Alto" id="txtaltocorrugadomaster" required="required" class="TT" />	
		</div>
		
		<div class="txt">
			Peso<br>
			<input type="number" value="0" onFocus="limpiar(this);" onBlur="validatenumber(this);"  placeholder="Peso" id="txtpesoporcorrugadomaster" required="required" class="TT" />	
		</div>
		

	</div>


		<section id="titulo">
        		<center></br>
			     <h4>Pallet</h4>
			</center>
		</section>
	<div class="myDiv">
		
		<div class="txt">
			Largo<br>
			<input type="number" value="0" onFocus="limpiar(this);" onBlur="validatenumber(this);"  placeholder="Largo" id="txtlargopallet" required="required" class="TT" />	
		</div>
		<div class="txt">
			Ancho<br>
			<input type="number" value="0" onFocus="limpiar(this);" onBlur="validatenumber(this);" placeholder="Ancho" id="txtanchopallet" required="required" class="TT" />	
		</div>
		<div class="txt">
			Alto<br>
			<input type="number" value="0" onFocus="limpiar(this);" onBlur="validatenumber(this);"  placeholder="Alto" id="txtalturapallet" required="required" class="TT" />	
		</div>
		<div class="txt">
			Corrugados por Cama<br>
			<input type="number" value="0" onFocus="limpiar(this);" onBlur="validatenumber(this);" placeholder="Corrugados por Cama" id="txtcorrugadosporcama" required="required" class="TT" />	
		</div>
		<div class="txt">
			Camas <br>
			<input type="number" value="0" onFocus="limpiar(this);" onBlur="validatenumber(this),calculatepesoneto();"  placeholder="Camas por Pallet" id="txtcamasporpallet" required="required" class="TT" />	
		</div>
		<div class="txt">
			Corrugados por Pallet<br>
			<input type="number" value="0"  onBlur="validatenumber(this);" list='corrugados' placeholder="Corrugados por Pallet" id="txtcorrugadoporpallet" required="required" class="TT" />	
		</div>
		<div class="txt">
			Total de Piezas<br>
			<input type="number" value="0"  onBlur="validatenumber(this);"  placeholder="Total de Piezas por Pallet" id="txtpiezasporpallet" required="required" class="TT" />	
		</div>
		
		<div class="txt">
			Peso Bruto<br>
			<input type="number" value="0" onFocus="limpiar(this);" onBlur="validatenumber(this);"  placeholder="Peso Bruto" id="txtpesoporpallet" required="required" class="TT" />	
		</div>
		<div class="txt">
			Peso Neto<br>
			<input type="number" value="0"  onBlur="validatenumber(this);"  placeholder="Peso Neto" id="txtpesoneto" required="required" class="TT" />	
		</div>

	</div>
	
		
		

		
																		
	</div> 
	<center>
	<div class="txt">
	<input type="button" name="save" disabled Value="Guardar" onClick="saveproducto();" id="ok"/>
	</div>
	</center>
	</div>   
  </div>
  </div>
  
</div>




<!--/////////////////////////////////////////////////////edit products//////////////////////////////////////////////////////////////////////////////-->


<div id="myModaledit" class="modal">

  <!-- Modal content -->
  <div class="modal-content"> 
  <div class="modal-content2">	
    <span class="close" id="closeedit">&times;</span>
			<section id="titulo">
        			<center></br><h2>Datos del producto</h2>
					     <h5>Asegurese de que los datos modificados sean correctos</h5>
				</center>
			</section>
			</br>
	<div class="contenedor">
			
	<div class="myDiv" >
		<div class="txt">
			Código del producto<br>
			<input type="text" placeholder="Código del producto" disabled id="txtcodigo2" onBlur="validateSAE();" required="required" maxlength="8" class="TT" />	
		</div>
		<div class="txt">
			Nombre del Producto<br>
			<input type="text" placeholder="Nombre del producto" id="txtnombre2" required="required" maxlength="40" class="TT" />	
		</div>
		<div class="txt">
			Descripción del Producto<br>
			<textarea rows="4" cols="50" name="dir"  class="TT" id="txtdescripcion2" placeholder="Descripción del producto"></textarea> 		
		</div>
		<!--div class="txt">
			Descontinuado<br>
			<select  class="TT" id="optionlinea">
              		</select> 		
		</div-->
		<div class="txt">
			Presentación Venta<br>
			 <input type="text" placeholder="Presentación Venta" value="Pieza" disabled id="txtpresentacionVenta2" list="presentacion" required="required" class="TT" />	
		</div>
		<div class="txt">
			Marca del Producto<br>
			 <input type="text" placeholder="Marca del producto" id="txtmarca2" required="required" class="TT" />		
		</div>
		<div class="txt">
			Código de Barras<br>
			 <input type="text" placeholder="Codigo de Barras" id="txtcodigodebarras2" required="required" class="TT" />	
		</div>
								

		<div class="txt">
			Código de Barras Empaque<br>
			 <input type="text" placeholder="Codigo de Barras Empaque" id="txtcodigodebarrasempaque2" required="required" class="TT" />	
		</div>
		<div class="txt">
			Pais de Origen<br>
			<input type="text"  placeholder="Pais de Origen" id="txtpais2" list="paises" required="required" class="TT" />	
		</div>
		<div class="txt">
			Fracción Aracenlaria<br>
			<input type="text"  placeholder="Fracción Arancelaria" id="txtfraccion2" required="required" class="TT" />	
		</div>
		<div class="txt" id="exportacion">
			<!--Exportación<br>
			<label class="fondotxt">
				<input type='radio' name='exportacion2' value='1' checked='checked'">Si
				<input type='radio' name='exportacion2' value='0'  checked='checked'">No
			</label-->	
		</div>

		<!--div class="txt" id="barniz">
			<!-Barniz<br>
			<label class="fondotxt">
				<input type='radio' name='barniz2' value='Mate' checked='checked'">Mate
				<input type='radio' name='barniz2' value='Brillante'  checked='checked'">Brillante
			</label->	
		</div-->
		<div class="txt" id="activo">
			<!--Barniz<br>
			<label class="fondotxt">
				<input type='radio' name='activo' value='Mate' checked='checked'">Mate
				<input type='radio' name='activo' value='Brillante'  checked='checked'">Brillante
			</label-->	
		</div>
		<!--div class="txt">
			Foto<br>
		    <input type="file" id="foto2" class="TT" accept="image/x-png,image/gif,image/jpeg"/>
             	    <input type="text" id="txtfoto2"  style="visibility:hidden"/>
                    <output id="list2"><div class="imag"></div></output>
                    <div class="imag" id="fotodiv2"><img src="" class="thumb" id="imgfoto2"/></div>
				
		</div>
		<div class="txt">
			Foto detalle<br>
		    <input type="file" id="fotodetalle2" class="TT" accept="image/x-png,image/gif,image/jpeg"/>
             	    <input type="text" id="txtfotodetalle2"  style="visibility:hidden"/>
                    <output id="list2"><div class="imag"></div></output>
                    <div class="imag" id="fotodetallediv2"><img src="" class="thumb" id="imgfotodetalle2"/></div>
		</div-->
		<div class="txt">
			Notas especiales<br>
			<textarea rows="4" cols="50" name="dir"  class="TT" id="txtnotas2" placeholder="Notas especiales"></textarea> 		
		</div>
	</div>
	<br\>
	<hr>
		<section id="titulo">
			<center></br><h3>Medidas del Producto</h3>
			</center>
		</section>
		<div class="myDiv">
			
			<div class="txt">
				A<br>
				<input type="number" value="0" onFocus="limpiar(this);" onBlur="validatenumber(this);"  placeholder="Ancho" id="txtaproducto2" required="required" class="TT" />	
			</div>
			<div class="txt">
				B<br>
				<input type="number" value="0" onFocus="limpiar(this);" onBlur="validatenumber(this);"  placeholder="Largo" id="txtbproducto2" required="required" class="TT" />	
			</div>
			<div class="txt">
				H<br>
				<input type="number" value="0" onFocus="limpiar(this);" onBlur="validatenumber(this);"  placeholder="Alto" id="txthproducto2" required="required" class="TT" />	
			</div>			

		</div>
	<br\>
	<hr>
		<section id="titulo">
			<center></br><h3>Especificaciones generales</h3>
			</center>

		<div class="myDiv">
			
						<div class="txt">
							Insumo<br>
							<textarea rows="4" cols="50" name="dir"  class="TT" id="txtespec1e" placeholder="Insumo"></textarea> 	
						</div>
						<div class="txt">
							Dimensiones<br>
							<textarea rows="4" cols="50" name="dir"  class="TT" id="txtespec2e" placeholder="Dimensiones"></textarea> 	
						</div>
						<div class="txt">
							Tinta y/o Pantone<br>
							<textarea rows="4" cols="50" name="dir"  class="TT" id="txtespec3e" placeholder="Tinta y/o Pantone"></textarea> 	
						</div>
						<div class="txt">
							Acabado<br>
							<textarea rows="4" cols="50" name="dir"  class="TT" id="txtespec4e" placeholder="Acabado"></textarea> 		
						</div>
						<div class="txt" >
							Barniz<br>
							<textarea rows="4" cols="50" name="dir"  class="TT" id="txtespec5e" placeholder="Barniz"></textarea> 	
						</div>
					
		</div>
			
	<br\>
	<hr>		
		<!--section id="titulo">
        			<center></br><h3>Documentos</h3>
				</center>
		</section>
	<div class="myDiv" id="docs2">
	
		<div class="txt">
			Plano de Pliego<br>
		    <input type="file" id="doc12" class="TT" />
             	    <input type="text" id="txtdoc12"  style="visibility:hidden"/>
                    <output id="list2"><div class="imag"></div></output>
                    <div class="imag" id="doc1div2"><img src="" class="thumb" id="imgdoc12"/></div>
				
		</div>
		<div class="txt">
			Plano de pieza y Formación<br>
		    <input type="file" id="doc22" class="TT" />
             	    <input type="text" id="txtdoc22"  style="visibility:hidden"/>
                    <output id="list2"><div class="imag"></div></output>
                    <div class="imag" id="doc2div2"><img src="" class="thumb" id="imgdoc22"/></div>
		</div>
		<div class="txt">
			Plano de Empalme<br>
		    <input type="file" id="doc32" class="TT" />
             	    <input type="text" id="txtdoc32"  style="visibility:hidden"/>
                    <output id="list2"><div class="imag"></div></output>
                    <div class="imag" id="doc3div2"><img src="" class="thumb" id="imgdoc32"/></div>				
		</div>
		<div class="txt">
			Arte<br>
		    <input type="file" id="doc42" class="TT" />
             	    <input type="text" id="txtdoc42"  style="visibility:hidden"/>
                    <output id="list2"><div class="imag"></div></output>
                    <div class="imag" id="doc4div2"><img src="" class="thumb" id="imgdoc42"/></div>
		</div>
		<div class="txt">
			Estudio de Empaque<br>
		    <input type="file" id="doc52" class="TT" />
             	    <input type="text" id="txtdoc52"  style="visibility:hidden"/>
                    <output id="list2"><div class="imag"></div></output>
                    <div class="imag" id="doc5div2"><img src="" class="thumb" id="imgdoc52"/></div>
				
		</div>
		<div class="txt">
			Anexo 1<br>
		    <input type="file" id="doc62" class="TT" />
             	    <input type="text" id="txtdoc62"  style="visibility:hidden"/>
                    <output id="list2"><div class="imag"></div></output>
                    <div class="imag" id="doc6div2"><img src="" class="thumb" id="imgdoc62"/></div>
		</div>
		<div class="txt">
			Anexo 2<br>
		    <input type="file" id="doc72" class="TT" />
             	    <input type="text" id="txtdoc72"  style="visibility:hidden"/>
                    <output id="list2"><div class="imag"></div></output>
                    <div class="imag" id="doc7div2"><img src="" class="thumb" id="imgdoc72"/></div>
				
		</div>
		<div class="txt">
			Anexo 3<br>
		    <input type="file" id="doc82" class="TT" />
             	    <input type="text" id="txtdoc82"  style="visibility:hidden"/>
                    <output id="list2"><div class="imag"></div></output>
                    <div class="imag" id="doc8div2"><img src="" class="thumb" id="imgdoc82"/></div>
		</div>

		<div class="txt">
			Notas especiales<br>
			<textarea rows="4" cols="50" name="dir"  class="TT" id="txtnotas2" placeholder="Notas especiales"></textarea> 		
		</div>
	
	</div-->
	<br\>
	<hr>		
		<section id="titulo">
        			<center></br><h3>Caracteristicas del Empaque</h3>
					     <h5>Dimensiones en mm</h5>
					    <h5>Peso en Kg.</h5>
				</center>
		</section>
	<div  id="mm2">
	
		
		<section id="titulo">
        			<center></br>
					     <h4>Producto</h4>
				</center>
		</section>
	<div class="myDiv">

		<div class="txt">
			Largo<br>
			<input type="number" value="0" onFocus="limpiar(this);" onBlur="validatenumber(this);"  placeholder="Largo" id="txtlargoproducto2" required="required" class="TT" />	
		</div>
		<div class="txt">
			Ancho<br>
			<input type="number" value="0" onFocus="limpiar(this);" onBlur="validatenumber(this);"  placeholder="Ancho" id="txtanchoproducto2" required="required" class="TT" />	
		</div>
		<div class="txt">
			Alto<br>
			<input type="number" value="0" onFocus="limpiar(this);" onBlur="validatenumber(this);"  placeholder="Alto" id="txtaltoproducto2" required="required" class="TT" />	
		</div>		
		
		
		<div class="txt">
			Peso<br>
			<input type="number" value="0" onFocus="limpiar(this);" onBlur="validatenumber(this);"  placeholder="Peso" id="txtpesopresentacionventa2" required="required" class="TT" />	
		</div>
	</div>

			<section id="titulo">
        			<center></br>
					     <h4>Inner Pack</h4>
					
				</center>
		</section>



	<div class="myDiv">
		
		<div class="txt">
			Empaque<br>
			<input type="text"  placeholder="--Seleccione--" id="txtcorrugadoinner2" list="corrugadosinner" onBlur="disbleinner2(this.value);" required="required" class="TT" />	
		</div>
		<div class="txt">
			Multiplo<br>
			<input type="number" value="0" onFocus="limpiar(this);" onBlur="validatenumber(this);"  placeholder="Multiplo" id="txtpiezascorrugadoinner2" required="required" class="TT" />	
		</div>
		<div class="txt">
			Largo <br>
			<input type="number" value="0" onFocus="limpiar(this);" onBlur="validatenumber(this);"  placeholder="Largo" id="txtlargocorrugadoinner2" required="required" class="TT" />	
		</div>
		<div class="txt">
			Ancho <br>
			<input type="number" value="0" onFocus="limpiar(this);" onBlur="validatenumber(this);" placeholder="Ancho" id="txtanchocorrugadoinner2" required="required" class="TT" />	
		</div>
		
		<div class="txt">
			Alto <br>
			<input type="number" value="0" onFocus="limpiar(this);" onBlur="validatenumber(this);"  placeholder="Alto" id="txtaltocorrugadoinner2" required="required" class="TT" />	
		</div>
		
		<div class="txt">
			Peso<br>
			<input type="number" value="0" onFocus="limpiar(this);" onBlur="validatenumber(this);"  placeholder="Peso" id="txtpesoporcorrugadoinner2" required="required" class="TT" />	
		</div>
		

	</div>
		<section id="titulo">
        			<center></br>
					     <h4>Master Pack</h4>
				</center>
		</section>



	<div class="myDiv">
		
		<div class="txt">
			Empaque<br>
			<input type="text"  placeholder="--Seleccione--" id="txtcorrugadomaster2" list="corrugadosmaster" required="required" class="TT" />	
		</div>
		<div class="txt">
			Multiplo<br>
			<input type="number" value="0" onFocus="limpiar(this);" onBlur="validatenumber(this);"  placeholder="Multiplo" id="txtpiezascorrugadomaster2" required="required" class="TT" />	
		</div>
		<div class="txt">
			Largo <br>
			<input type="number" value="0" onFocus="limpiar(this);" onBlur="validatenumber(this);"  placeholder="Largo" id="txtlargocorrugadomaster2" required="required" class="TT" />	
		</div>
		<div class="txt">
			Ancho <br>
			<input type="number" value="0" onFocus="limpiar(this);" onBlur="validatenumber(this);" placeholder="Ancho" id="txtanchocorrugadomaster2" required="required" class="TT" />	
		</div>
		
		<div class="txt">
			Alto <br>
			<input type="number" value="0" onFocus="limpiar(this);" onBlur="validatenumber(this);"  placeholder="Alto" id="txtaltocorrugadomaster2" required="required" class="TT" />	
		</div>
		
		<div class="txt">
			Peso<br>
			<input type="number" value="0" onFocus="limpiar(this);" onBlur="validatenumber(this);"  placeholder="Peso" id="txtpesoporcorrugadomaster2" required="required" class="TT" />	
		</div>
		

	</div>


		<section id="titulo">
        		<center></br>
			     <h4>Pallet</h4>
			</center>
		</section>
	<div class="myDiv">
		
		<div class="txt">
			Largo<br>
			<input type="number" value="0" onFocus="limpiar(this);" onBlur="validatenumber(this);"  placeholder="Largo" id="txtlargopallet2" required="required" class="TT" />	
		</div>
		<div class="txt">
			Ancho<br>
			<input type="number" value="0" onFocus="limpiar(this);" onBlur="validatenumber(this);" placeholder="Ancho" id="txtanchopallet2" required="required" class="TT" />	
		</div>
		<div class="txt">
			Alto<br>
			<input type="number" value="0" onFocus="limpiar(this);" onBlur="validatenumber(this);"  placeholder="Alto" id="txtalturapallet2" required="required" class="TT" />	
		</div>
		<div class="txt">
			Corrugados por Cama<br>
			<input type="number" value="0" onFocus="limpiar(this);" onBlur="validatenumber(this);" placeholder="Corrugados por Cama" id="txtcorrugadosporcama2" required="required" class="TT" />	
		</div>
		<div class="txt">
			Camas <br>
			<input type="number" value="0" onFocus="limpiar(this);" onBlur="validatenumber(this),calculatepesoneto2();"  placeholder="Camas por Pallet2" id="txtcamasporpallet2" required="required" class="TT" />	
		</div>
		<div class="txt">
			Corrugados por Pallet<br>
			<input type="number" value="0"  onBlur="validatenumber(this);" list='corrugados' placeholder="Corrugados por Pallet" id="txtcorrugadoporpallet2" required="required" class="TT" />	
		</div>
		<div class="txt">
			Total de Piezas<br>
			<input type="number" value="0"  onBlur="validatenumber(this);"  placeholder="Total de Piezas por Pallet" id="txtpiezasporpallet2" required="required" class="TT" />	
		</div>
		
		<div class="txt">
			Peso Bruto<br>
			<input type="number" value="0" onFocus="limpiar(this);" onBlur="validatenumber(this);"  placeholder="Peso Bruto" id="txtpesoporpallet2" required="required" class="TT" />	
		</div>
		<div class="txt">
			Peso Neto<br>
			<input type="number" value="0"  onBlur="validatenumber(this);"  placeholder="Peso Neto" id="txtpesoneto2" required="required" class="TT" />	
		</div>

	</div>	
		
																		
	</div> 
	<center>
	<div class="txt">
	<input type="button" name="save" Value="Actualizar" onClick="saverecord();" id="ok"/>
	</div>
	</center>
	</div>   
  </div>
  </div>
  
</div>






<!--/////////////////////////////////////////////////////duplicate product//////////////////////////////////////////////////////////////////////////////-->


<div id="myModalduplicate" class="modal">

  <!-- Modal content -->
  <div class="modal-content"> 
  <div class="modal-content2">	
    <span class="close" id="closeduplicate">&times;</span>
			<section id="titulo">
        			<center></br><h2>Ingrese los datos del nuevo producto</h2>
					     <h5>Asegurese de que los datos introducidos sean correctos</h5>
				</center>
			</section>
			</br>
	<div class="contenedor">
			
	<div class="myDiv" >
		<div class="txt">
			Código del Producto<br>
			<input type="text" placeholder="Código del Producto" id="txtcodigoduplicate" onBlur="validateSAEduplicate();" required="required" maxlength="8" class="TT" />	
		</div>
		<div class="txt">
			Nombre del Producto<br>
			<input type="text" placeholder="Nombre del Producto" id="txtnombreduplicate" required="required" maxlength="40" class="TT" />	
		</div>
		

	</div>
			
	<center>
	<div class="txt">
	<input type="button" name="saveduplicate" disabled Value="Guardar" onClick="saveduplicate();" id="ok"/>
	</div>
	</center>
	</div>   
  </div>
  </div>
  
</div>



<!--/////////////////////////////////////////////////////view files of product//////////////////////////////////////////////////////////////////////////////-->


<div id="myModalfiles" class="modal">

  <!-- Modal content -->
  <div class="modal-content"> 
  <div class="modal-content2">	
    <span class="close" id="closefiles">&times;</span>
			<section id="titulo">
        			<center></br><h2>Archivos del producto</h2>
					     </br><h3 id="filespodructo"></h3>
				</center>
			</section>
			</br>
	
	<center>
	</br>
	

		<div class="txt">

			<img src="../img/folder.png" onClick="addfile2();"/>
			
		</div>
		<div class="myDiv" id="docs22" style="display:none;">
	
			<div class="txt">
				Seleccione el Archivo<br>
		    		<input type="file" id="file12" class="TT" onchange="enablebtn2();"/>
             	   		
				<center>
					<!--input type="button" name="insertar" Value="Guardar" onClick="" id="ok"/-->
					<textarea rows="6" cols="50" name="dir"  class="TT" id="txtfiledescription2" placeholder="Descripción del Archivo"></textarea>
					<br>						
					<img src="../img/save2.png" onclick="uploadfiletoserver2();" id="imgsavefile2" style="display:none;"/>
				</center>
			</div>		
		</div>
		<div >
			<img src="../img/load.gif" class="imgload" id="imgloadfiles">
		</div>
	<div id="scro">

 		<div id="tablafiles">
   			  <table class="tbl-qa" id="resultadofiles">
		 		 <thead>
					 <tr>
						<th class="table-header" >Descripción</th>
						<th class="table-header" >Ver</th>
						<th class="table-header" >Editar</th>
						<th class="table-header" >Eliminar</th>
						
              				</tr>
 		  		</thead>
		  		<tbody >				
             	 		</tbody>
              		</table>
 		</div>
		</br></br>
		<div id="sinfiles" style="display:none;">
			<div class='myDiv'><div class='txt'>NO HAY ARCHIVOS REGISTRADOS EN EL SISTEMA </div></div>
		</div>
 	</div>
	</center>	



  </div>
  </div>
  
</div>


<!--/////////////////////////////////////////////////////consult process//////////////////////////////////////////////////////////////////////////////-->



<div id="myModalprocesos" class="modal">

  <!-- Modal content -->
  <div class="modal-content"> 
  <div class="modal-content2">	
    <span class="close" id="closeprocesos">&times;</span>
			<section id="titulo">
        			<center></br><h2>Procesos asociados al producto</h2>
					</br><h3 id="propodructo"></h3>
				</center>
			</section>
			</br>


		<div>
				<center>
					<div id="sin2pro" style="display:none;">
						<div id="myDiv">
							<div class="txt">
								Ingresa el Nombre del Proceso a Buscar<br>
								<input type="text" name="num" placeholder="Nombre del Proceso" id="txtbuscarpro" onkeyup="doSearchpro()" required="required" class="TT" />
			
							</div>
							<div class="txt">
								<input type="button" name="insertar" Value="Actualizar" onClick="getprocessbyproduct();" id="ok"/>
							</div>
							<div >
								<img src="../img/load.gif" class="imgload" id="imgloadpro">
							</div>
						</div>
					</div>
				</center>
	
			
			<center>
			</br>
			<div class="txt" id="agregarr">
				<input type="button"  Value="Agregar" onClick="agregarprocesos();" id="ok"/>
			</div>
			</br>
			<div id="scro">

<!--/////////////////////////////////////////////////////add new  process by product//////////////////////////////////////////////////////////////////////////////-->


				<div id="altaproceso" style="display:none;">
					<datalist id="categorias"></datalist>
					<datalist id="subensambles"></datalist>
					<datalist id="horasturno"></datalist>
					<datalist id="bobinas"></datalist>
					</br> </br>
					<div class="contenedor">	
						<center>
							<div class="txt">
								<input type="button"  Value="Cancelar" onClick="cancelaraltaproceso();" id="ok"/>
							</div>
						</center>
					</div>   
					</br></br>
					<div class="myDiv" >
						<div class="txt">
							Máquina<br>
							<select  class="TT" id="optionmaquina" onchange="getmaquinadetail(this.value);" onblur="calcularestandarturno();">
								<option value=0>---Seleccione---</option>		
              						</select> 
						</div>
						<div class="txt">
							Proceso<br>
							<input type="text" placeholder="Nombre del Proceso" id="txtproceso" disabled required="required" class="TT" />	
						</div>
						
						<div class="txt">
							Formatos/Sustrato<br>
							<input type="number" placeholder="Formatos/Sustrato" id="txtpiezasforma" required="required"  class="TT" />	
						</div>
						
						<div class="txt">
							Multiplo Proceso<br>
							<input type="number" value="1" placeholder="Multiplo Proceso" onkeyup="if(this.value>2) this.value=1;"  id="txtmultiploproceso" required="required" class="TT" />	
						</div>
						<div class="txt">
							Descripción del Proceso<br>
							<textarea rows="4" cols="50" name="dir"  class="TT" id="txtdescripcionproceso" placeholder="Descripción" ></textarea> 		
						</div>
						<div class="txt" id='bobinatype' style='display:none;'>
							Bobina<br>
							<input type="text" placeholder="Bobina" list="bobinas" id="txtbobina" required="required"  class="TT" />	
						</div>
						<div class="txt" style="display:none;">
							Categoría<br>
							<input type="text" placeholder="Categoría" list="categorias" id="txtcategoria" required="required"  value="EE" class="TT" />	
						</div>
						<div class="txt" style="display:none;">
							Subensamble<br>
							<input type="text" placeholder="Subensamble" list="subensambles" id="txtsubensamble" required="required" value="Pliego" class="TT" />	
						</div>
						
						<div class="txt">
							Estándar Proceso Hora<br>
							<input type="text" placeholder="Estándar Proceso" id="txtestandarproceso"  disabled required="required"  class="TT" />	
						</div>

						<div class="txt">
							Unidad de Medida<br>
							<input type="text" placeholder="Unidad de Medida" id="txtmedidaentrada" disabled required="required"class="TT" />	
						</div>
						<div class="txt" style="display:none;">
							Merma Ajuste(Pliegos)<br>
							<input type="number" placeholder="Merma Ajuste" id="txtmermaajuste" disabled  required="required" class="TT" />	
						</div>
						
						<div class="txt" style="display:none;">
							Merma del Proceso(%)<br>
							<input type="text" placeholder="Merma del Proceso" id="txtestandarajuste" disabled onchange="dopercentaje(this);"  required="required" class="TT" />	
						</div>
						<!--div class="txt">
							Estándar Calculado<br>
							<input type="text" placeholder="Estándar Calculado" id="txtestandarcalculado" onclick="calculaestandar();" required="required" class="TT" />	
						</div>

						<div class="txt">
							1.Espec y/o Insumo<br>
							<textarea rows="4" cols="50" name="dir"  class="TT"  placeholder="1.Espec y/o Insumo"></textarea> 	
						</div>
						<div class="txt">
							2.Espec y/o Dimensiones<br>
							<textarea rows="4" cols="50" name="dir"  class="TT"  placeholder="2.Espec y/o Dimensiones"></textarea> 	
						</div>
						<div class="txt">
							3.Espec Y/o Tinta y/o Pantone<br>
							<textarea rows="4" cols="50" name="dir"  class="TT"  placeholder="3.Espec y/o Tinta"></textarea> 	
						</div>
						<div class="txt">
							4.Espec y/o Acabado<br>
							<textarea rows="4" cols="50" name="dir"  class="TT"  placeholder="4.Espec y/o Acabado"></textarea> 		
						</div>
						<div class="txt">
							5.Espec y/o Empaque<br>
							<textarea rows="4" cols="50" name="dir"  class="TT" placeholder="5.Espec y/o Empaque"></textarea> 	
						</div-->

						
						<div class="txt" style="display:none;">
							Horas turno<br>
							
							<input type="number" value="8" placeholder="Multiplo Proceso"  id="optionhrsturno" required="required" class="TT" disabled/>
							<!--select class="TT" id="optionhrsturno" onchange="calcularestandarturno();">
								<option value=0>---Seleccione---</option>		
              						</select--> 
						</div>
						<div class="txt" style="display:none;">
							Estándar Turno<br>
							<input type="text" placeholder="Estándar Turno" id="txtestandarturno" onclick="calcularestandarturno();" disabled required="required" class="TT" />	
						</div>
					</div>
						</br>
					<hr>
					<section id="titulo">
        					<center></br><h2>Especificaciones Críticas</h2>
							
						</center>
					</section>
					</br>
					
					<div class="myDiv">
						<div class="txt">
							Especificación Crítica 1<br>
							<textarea rows="4" cols="50" name="dir"  class="TT" id="txtespecc1" placeholder="..."></textarea>  	
						</div>
						<div class="txt">
							Especificación Crítica 2<br>
							<textarea rows="10" cols="50" name="dir"  class="TT" id="txtespecc2" placeholder="..."></textarea> 	
						</div>
						<div class="txt">
							Especificación Crítica 3<br>
							<textarea rows="10" cols="50" name="dir"  class="TT" id="txtespecc3" placeholder="..."></textarea> 	
						</div>
					</div>
					<div class="contenedor">
						<center>
							<div class="txt">
								<input type="button" name="saveprocesos"  Value="Guardar" onClick="saveproceso();" id="ok"/>
							</div>
						</center>
					</div>   

				</div>


<!--/////////////////////////////////////////////////////edit process by product//////////////////////////////////////////////////////////////////////////////-->



				<div id="editaproceso" style="display:none;">
					
					</br> </br>
					<div class="contenedor">	
						<center>
							<div class="txt">
								<input type="button"  Value="Cancelar" onClick="cancelareditaproceso();" id="ok"/>
							</div>
						</center>
					</div>   
					</br></br>
					<div class="myDiv" >
						<div class="txt">
							Máquina<br>
							<select  class="TT" id="optionmaquinaedita" onchange="getmaquinadetailedita(this.value);" onblur="calcularestandarturnoedita();">
								<option value=0>---Seleccione---</option>		
              						</select> 
						</div>
						<div class="txt">
							Proceso<br>
							<input type="text" placeholder="Nombre del Proceso" id="txtprocesoedita" disabled required="required" class="TT" />	
						</div>
						
						<div class="txt">
							Formatos/Sustrato<br>
							<input type="text" placeholder="Sustrato/Formato" id="txtpiezasformaedita" required="required"  class="TT" />	
						</div>
						<div class="txt">
							Multiplo Proceso<br>
							<input type="text" placeholder="Multiplo Proceso" id="txtmultiploprocesoedita" onkeyup="if(this.value>2) this.value=1;" required="required" class="TT" />	
						</div>
						<div class="txt">
							Descripción<br>
							<textarea rows="4" cols="50" name="dir"  class="TT" id="txtdescripcionprocesoedita" placeholder="Descripción"  ></textarea> 		
						</div>
						<div class="txt" id='bobinatype2' style='display:none;'>
							Bobina<br>
							<input type="text" placeholder="Bobina" list="bobinas" id="txtbobinaedita" required="required"  class="TT" />	
						</div>
						<div class="txt" style="display:none;">
							Categoría<br>
							<input type="text" placeholder="Categoría" list="categorias" id="txtcategoriaedita" required="required"  class="TT" />	
						</div>
						<div class="txt" style="display:none;">
							Subensamble<br>
							<input type="text" placeholder="Subensamble" list="subensambles" id="txtsubensambleedita" required="required" class="TT" />	
						</div>
						<div class="txt">
							Estándar Proceso Hora<br>
							<input type="text" placeholder="Estándar Proceso" id="txtestandarprocesoedita" disabled required="required"  class="TT" />	
						</div>
						<div class="txt">
							Unidad de Medida<br>
							<input type="text" placeholder="Unidad de Medida" id="txtmedidaentradaedita" disabled required="required"class="TT" />	
						</div>
						
						<div class="txt" style="display:none;">
							Merma Ajuste(Pliegos)<br>
							<input type="text" placeholder="Merma Ajuste" id="txtmermaajusteedita" disabled required="required" class="TT" />	
						</div>
						
						<div class="txt" style="display:none;">
							Merma del Proceso(%)<br>
							<input type="text" placeholder="Merma del Proceso" id="txtestandarajusteedita" disabled required="required" class="TT" />	
						</div>
						<!--div class="txt">
							Estándar Cálculado<br>
							<input type="text" placeholder="Estándar Cálculado" id="txtestandarcalculadoedita" disabled required="required" class="TT" />	
						</div>


						<div class="txt">
							1.Espec y/o Insumo<br>
							<textarea rows="4" cols="50" name="dir"  class="TT" id="txtespec1edita" placeholder="1.Espec y/o Insumo"></textarea> 	
						</div>
						<div class="txt">
							2.Espec y/o Dimensiones<br>
							<textarea rows="4" cols="50" name="dir"  class="TT" id="txtespec2edita" placeholder="2.Espec y/o Dimensiones"></textarea> 	
						</div>
						<div class="txt">
							3.Espec Y/o Tinta y/o Pantone<br>
							<textarea rows="4" cols="50" name="dir"  class="TT" id="txtespec3edita" placeholder="3.Espec y/o Tinta"></textarea> 	
						</div>
						<div class="txt">
							4.Espec y/o Acabado<br>
							<textarea rows="4" cols="50" name="dir"  class="TT" id="txtespec4edita" placeholder="4.Espec y/o Acabado"></textarea> 		
						</div>
						<div class="txt">
							5.Espec y/o Empaque<br>
							<textarea rows="4" cols="50" name="dir"  class="TT" id="txtespec5edita" placeholder="5.Espec y/o Empaque"></textarea> 	
						</div-->


						
						<div class="txt" style="display:none;">
							Horas Turno<br>
							
							<input type="text" placeholder="Horas Turno" id="optionhrsturnoedita" required="required" disabled value="8" class="TT" />	
							<!--select  class="TT" id="optionhrsturnoedita" onchange="calcularestandarturnoedita();">
								<option value=0>---Seleccione---</option>		
              						</select--> 
						</div>
						<div class="txt" style="display:none;">
							Estándar Turno<br>
							<input type="text" placeholder="Estándar Turno" id="txtestandarturnoedita"  required="required" disabled class="TT" />	
						</div>
						<div class="txt">
							Activo<br>
							
							<select  class="TT" id="optionactivoprocess">
								<option value=1>Si</option>
								<option value=0>No</option>		
              						</select> 
						</div>
					</div>
						<section id="titulo">
        						<center></br><h2>Especificaciones Críticas</h2>
							
							</center>
						</section>
					</br>
					
					<div class="myDiv">
						<div class="txt">
							Especificación Crítica 1<br>
							<textarea rows="10" cols="50" name="dir"  class="TT" id="txtespec1edita" placeholder="..."></textarea> 	
						</div>
						<div class="txt">
							Especificación Crítica 2<br>
							<textarea rows="10" cols="50" name="dir"  class="TT" id="txtespec2edita" placeholder="..."></textarea> 	
						</div>
						<div class="txt">
							Especificación Crítica 3<br>
							<textarea rows="10" cols="50" name="dir"  class="TT" id="txtespec3edita" placeholder="..."></textarea> 	
						</div>
					</div>
					<div class="contenedor">
						<center>
							<div class="txt">
								<input type="button" name="saveprocesos"  Value="Actualizar" onClick="saveprocesoedita();" id="ok"/>
							</div>
						</center>
					</div>   

				</div>
					




 				<div id="tablaproceso">
  					<table class="tbl-qa" id="resultadopro">
		  				<thead>
		 				<tr>
							<th class="table-header" >Codigo</th>
							<th class="table-header" >Máquina</th>
							<th class="table-header" >Proceso</th>
							<th class="table-header" >Formatos/Sustrato</th>
							<th class="table-header" >Multiplo Proceso</th>
							<th class="table-header" >Descripción</th>
							<th class="table-header" >Estándar Proceso Hora</th>
							<th class="table-header" >Unidad de Medida</th>
							
							<th class="table-header" style='display:none;'>Merma Ajuste(Pliegos)</th>
							<th class="table-header" style='display:none;'>Merma del Proceso(%)</th>
							
							<th class="table-header" >Especificación  Crítica 1</th>
							<th class="table-header" >Especificación  Crítica 2</th>
							<th class="table-header" >Especificación  Crítica 3</th>
							
							<th class="table-header" style='display:none;' >Horas Turno</th>
							<th class="table-header" style='display:none;'>Estándar Turno</th>
							<!--th class="table-header" >Categoría</th>
							<th class="table-header" >Subensamble</th>
							<th class="table-header" >Estándar Cálculado</th>
							<th class="table-header" >4.Espec y/o acabado</th>
							<th class="table-header" >5.Espec y/o Empaque</th-->
							
							
							
							<th class="table-header" >Activo</th>
							<th class="table-header" >Editar</th>
							<th class="table-header" >Eliminar</th>
							
		
            					</tr>
 		 				</thead>
		  				<tbody style="height:250px;overflow:scroll">				
             	 				</tbody>
             				</table>
 				</div>
				</br></br>
				<div id="sinpro" style="display:none;">
					<div class='myDiv'><div class='txt'>NO HAY PROCESOS REGISTRADOS EN EL SISTEMA PARA ESTE PRODUCTO </div></div>
				</div>
 			</div>
			</center>
		</div>






  </div>
  </div>
  
</div>



<!--/////////////////////////////////////////////////////////////consult components////////////////////////////////////////////////////////////////////////-->

<div id="myModalcomponentes" class="modal">

  <!-- Modal content -->
  <div class="modal-content"> 
  <div class="modal-content2">	
    <span class="close" id="closecomponentes">&times;</span>
			<section id="titulo">
        			<center></br><h2>Componentes Asociados al Producto</h2>
					</br><h3 id="compodructo"></h3>
				</center>
			</section>
			</br>


		<div>
				<center>
					<div id="sin2com" style="display:none;">
						<div id="myDiv">
							<div class="txt">
								Ingresa el Nombre del Componente a Buscar<br>
								<input type="text" name="num" placeholder="Nombre del Componente" id="txtbuscarcom" onkeyup="doSearchcom()" required="required" class="TT" />
			
							</div>
							<div class="txt">
								<input type="button" name="insertar" Value="Actualizar" onClick="getcomponentsbyproduct();" id="ok"/>
							</div>
							<div >
								<img src="../img/load.gif" class="imgload" id="imgloadcom">
							</div>
						</div>
					</div>
				</center>
	
			
			<center>
			</br>
			<div class="txt" id="agregarcomponente">
				<input type="button"  Value="Agregar" onClick="agregarcomponentes();" id="ok"/>
			</div>
			</br>
			<div id="scro">

<!--/////////////////////////////////////////////////////add new components by product//////////////////////////////////////////////////////////////////////////////-->


				<div id="altacomponente" style="display:none;">
					<datalist id="componentes"></datalist>
					
					</br> </br>
					<div class="contenedor">	
						<center>
							<div class="txt">
								<input type="button"  Value="Cancelar" onClick="cancelaraltacomponente();" id="ok"/>
							</div>
						</center>
					</div>   
					</br></br>
					<div class="myDiv" >
						<div class="txt">
							Código SAE<br>
							<input type="text" placeholder="Código SAE"  list="componentes" onblur="getspecificcomponente(this.value);" id="txtcodigosae" required="required"  class="TT" />	
						</div>
						<div class="txt">
							Descripción<br>
							<input type="text" placeholder="Descripción"  id="txtcomponente" required="required" class="TT" />	
						</div>
						
						<div class="txt">
							Línea<br>
							<input type="text" placeholder="Línea" id="txtlinea" required="required" class="TT" />	
						</div>
						<div class="txt">
							Cantidad<br>
							<input type="number" placeholder="Cantidad" id="txtcantidad"  required="required"  class="TT" />	
						</div>
						<div class="txt">
							Unidad de Medida<br>
							<input type="text" placeholder="U.M." id="txtum" required="required" class="TT" />		
						</div>
						<div class="txt" style="display:none;">
							Último Costo<br>
							<input type="text" placeholder="Último Costo" id="txtultimocosto" value="0" required="required"class="TT" />	
						</div>
						
						<div class="txt" style="display:none;">
							Cantidad<br>
							<input type="number"  id="txtidcomponente" required="required"  class="TT" />	
						</div>
						
					</div>

					<div class="contenedor">
						<center>
							<div class="txt">
								<input type="button" name="saveprocesos"  Value="Guardar" onClick="savecomponente();" id="ok"/>
							</div>
						</center>
					</div>   

				</div>


<!--/////////////////////////////////////////////////////edit components by product//////////////////////////////////////////////////////////////////////////////-->



				<div id="editacomponente" style="display:none;">
					
					</br> </br>
					<div class="contenedor">	
						<center>
							<div class="txt">
								<input type="button"  Value="Cancelar" onClick="cancelareditacomponente();" id="ok"/>
							</div>
						</center>
					</div>   
					</br></br>
					<div class="myDiv" >
						<div class="txt">
							Código SAE<br>
							<input type="text" placeholder="Código SAE"  list="componentes"  onblur="getspecificcomponente2(this.value);" id="txtcodigosae2" required="required"  class="TT" />	
						</div>
						<div class="txt">
							Componente<br>
							<input type="text" placeholder="Componente"   id="txtcomponente2" required="required" class="TT" />	
						</div>
						
						<div class="txt">
							Línea<br>
							<input type="text" placeholder="Línea" id="txtlinea2" required="required" class="TT" />	
						</div>
						<div class="txt">
							Cantidad<br>
							<input type="number" placeholder="Cantidad" id="txtcantidad2" required="required"  class="TT" />	
						</div>
						<div class="txt">
							Unidad de Medida<br>
							<input type="text" placeholder="U.M." id="txtum2" required="required" class="TT" />		
						</div>
						<div class="txt" style="display:none;">
							Último Costo<br>
							<input type="text" placeholder="Último Costo" id="txtultimocosto2" required="required"class="TT" />	
						</div>
						
						<div class="txt" style="display:none;">
							id<br>
							<input type="number" placeholder="Cantidad" id="txtidcomponente2" required="required"  class="TT" />	
						</div>
					</div>

					<div class="contenedor">
						<center>
							<div class="txt">
								<input type="button" name="savecomponentes"   Value="Actualizar" onClick="savecomponenteedita();" id="ok"/>
							</div>
						</center>
					</div>   

				</div>
					




 				<div id="tablacomponente">
  					<table class="tbl-qa" id="resultadocom">
		  				<thead>
		 				<tr>
							<th class="table-header" >Clave SAE</th>
							<th class="table-header" >Componente</th>
							<th class="table-header" >Línea</th>
							<th class="table-header" >Cantidad</th>
							<th class="table-header" >Unidad de Medida</th>
							<!--th class="table-header" >Último costo</th-->
							
							<th class="table-header" >Editar</th>
							<th class="table-header" >Eliminar</th>
							
		
            					</tr>
 		 				</thead>
		  				<tbody style="height:250px;overflow:scroll">				
             	 				</tbody>
             				</table>
 				</div>
				</br></br>
				<div id="sincom" style="display:none;">
					<div class='myDiv'><div class='txt'>NO HAY COMPONENTES REGISTRADOS EN EL SISTEMA PARA ESTE PRODUCTO </div></div>
				</div>
 			</div>
			</center>
		</div>






  </div>
  </div>
  
</div>




<!--/////////////////////////////////////////////////////Especifications by product//////////////////////////////////////////////////////////////////////////////-->



<div id="myModalespecificacionespt" class="modal">

  <!-- Modal content -->
  <div class="modal-content"> 
  <div class="modal-content2">	
    <span class="close" id="closeespecificacionespt">&times;</span>
			<section id="titulo">
        			<center></br><h2>Especificaciones de Producto Terminado Asociadas a: </h2>
					</br><h3 id="compodructopt"></h3>
				</center>
			</section>
			</br>


		<div>
				<center>
					<div id="sin2pt" style="display:none;">
						<div id="myDiv">
							<div class="txt">
								Ingresa el Nombre de la especificación a Buscar<br>
								<input type="text" name="num" placeholder="Nombre de la especificación" id="txtbuscarpt" onkeyup="doSearchpt()" required="required" class="TT" />
			
							</div>
							<div class="txt">
								<input type="button" name="insertar" Value="Actualizar" onClick="getspecificationsbyproduct();" id="ok"/>
							</div>
							<div >
								<img src="../img/load.gif" class="imgload" id="imgloadpt">
							</div>
						</div>
					</div>
				</center>
	
			
			<center>
			</br>
			<div class="txt" id="agregarespecificacionpt">
				<input type="button"  Value="Agregar" onClick="agregarespecificacionpt();" id="ok"/>
			</div>
			</br>
			<div id="scro">

<!--/////////////////////////////////////////////////////add new especification by product//////////////////////////////////////////////////////////////////////////////-->


				<div id="altaespecificacionpt" style="display:none;">
					
					
					</br> </br>
					<div class="contenedor">	
						<center>
							<div class="txt">
								<input type="button"  Value="Cancelar" onClick="cancelaraltaespecificacionpt();" id="ok"/>
							</div>
						</center>
					</div>   
					</br></br>
					<div class="myDiv" >
						<div class="txt">
							Especificación<br>
							<textarea rows="10" cols="29" name="dir"  id="txtespecificacionpt" placeholder="..."></textarea> 	
						</div>
						
					</div>

					<div class="contenedor">
						<center>
							<div class="txt">
								<input type="button" name="savept"  Value="Guardar" onClick="saveespecificacionpt();" id="ok"/>
							</div>
						</center>
					</div>   

				</div>


<!--/////////////////////////////////////////////////////edit especification by product//////////////////////////////////////////////////////////////////////////////-->



				<div id="editaespecificacionpt" style="display:none;">
					
					</br> </br>
					<div class="contenedor">	
						<center>
							<div class="txt">
								<input type="button"  Value="Cancelar" onClick="cancelareditaespecificacionpt();" id="ok"/>
							</div>
						</center>
					</div>   
					</br></br>
					<div class="myDiv" >
						<div class="txt">
							Especificación<br>
							<textarea rows="10" cols="29" name="dir"  id="txtespecificacionptedita" placeholder="..."></textarea> 	
						</div>
						<div class="txt">
							Activo<br>
							 <select  class="TT" id="optionactivoespecificacionpt">
								<option value=1>Si</option>
								<option value=0>No</option>		
              						</select> 	
						</div>
						
					</div>

					<div class="contenedor">
						<center>
							<div class="txt">
								<input type="button" name="savecomponentes"   Value="Actualizar" onClick="saveespecificacionptedita();" id="ok"/>
							</div>
						</center>
					</div>   

				</div>
					




 				<div id="tablaespecificacionpt">
  					<table class="tbl-qa" id="resultadoespecificacionpt">
		  				<thead>
		 				<tr>
							<th class="table-header" >ID</th>
							<th class="table-header" >Especificación</th>
							<th class="table-header" >Activo</th>
							<th class="table-header" >Editar</th>
							<th class="table-header" >Eliminar</th>
							
		
            					</tr>
 		 				</thead>
		  				<tbody style="height:250px;overflow:scroll">				
             	 				</tbody>
             				</table>
 				</div>
				</br></br>
				<div id="sinespecificacionpt" style="display:none;">
					<div class='myDiv'><div class='txt'>NO HAY ESPECIFICACIONES REGISTRADAS EN EL SISTEMA PARA ESTE PRODUCTO </div></div>
				</div>
 			</div>
			</center>
		</div>






  </div>
  </div>
  
</div>







</main>



<!--/////////////////////////////////////////////////////functions for load files and edit it//////////////////////////////////////////////////////////////////////////////-->

<script>


/*var control = document.getElementById("foto");
control.addEventListener("change", function(event) {

    // When the control has changed, there are new files
var files = document.getElementById('foto').files;
  if (files.length > 0) {
	var sizeByte = this.files[0].size / 1024 / 1024;
      
   if(sizeByte >.8){
	alert("El tamaño de la imagen supera el permitido(800Kb)");
	  }else{
		getBase641(files[0]);
	}
    
  }


function getBase641(file) {
   var reader = new FileReader();
   reader.readAsDataURL(file);
   reader.onload = function () {
     //console.log(reader.result);
var cad=reader.result;


document.getElementById("txtfoto").value=cad;
	
//document.getElementById("MyElement").className ="thumb" ;
document.getElementById("imgfoto").src=cad;
document.getElementById("imgfoto").className ="thumb2" ;
document.getElementById("fotodiv").className ="imag2" ;
  };
   reader.onerror = function (error) {
     console.log('Error: ', error);
   };
}

}, false);


function archivo(evt) {
      var files = evt.target.files;      for (var i = 0, f; f = files[i]; i++) {
           if (!f.type.match('image.*')) {
                //continue;
		//alert("El archivo no es de tipo imagen");
		document.getElementById("fotodiv").className ="imag";
		document.getElementById("foto").value="";
		alert("El archivo no es de tipo imagen");
		return;
		//continue;	
           }
           var reader = new FileReader();
           reader.onload = (function(theFile) {
               return function(e) {
                      //document.getElementById("list3").innerHTML = ['<img class="thumb" src="', e.target.result,'" title="', escape(theFile.name), '"/>'].join('');
               };
           })(f);
           reader.readAsDataURL(f);
       }
}
    
  document.getElementById('foto').addEventListener('change', archivo, false);

var control = document.getElementById("fotodetalle");
control.addEventListener("change", function(event) {

    // When the control has changed, there are new files
var files = document.getElementById('fotodetalle').files;
  if (files.length > 0) {
	var sizeByte = this.files[0].size / 1024 / 1024;
      
   if(sizeByte >.8){
	alert("El tamaño de la imagen supera el permitido(800Kb)");
	  }else{
		getBase642(files[0]);
	}
    
  }


function getBase642(file) {
   var reader = new FileReader();
   reader.readAsDataURL(file);
   reader.onload = function () {
     //console.log(reader.result);
var cad=reader.result;


document.getElementById("txtfotodetalle").value=cad;
	
//document.getElementById("MyElement").className ="thumb" ;
document.getElementById("imgfotodetalle").src=cad;
document.getElementById("imgfotodetalle").className ="thumb2" ;
document.getElementById("fotodetallediv").className ="imag2" ;
  };
   reader.onerror = function (error) {
     console.log('Error: ', error);
   };
}

}, false);


function archivo2(evt) {
      var files = evt.target.files;      for (var i = 0, f; f = files[i]; i++) {
           if (!f.type.match('image.*')) {
                //continue;
		
		document.getElementById("fotodetalle").value="";
		document.getElementById("fotodetallediv").className ="imag";
		//setTimeout(function() {
   		alert("El archivo no es de tipo imagen");
		//	}, 300);
		//
		return;	
           }
           var reader = new FileReader();
           reader.onload = (function(theFile) {
               return function(e) {
                      //document.getElementById("list3").innerHTML = ['<img class="thumb" src="', e.target.result,'" title="', escape(theFile.name), '"/>'].join('');
               };
           })(f);
           reader.readAsDataURL(f);
       }
}
 document.getElementById('fotodetalle').addEventListener('change', archivo2, false);
*/


////////////////////////////////////////////funciones para subir los archivos al servidor//////////////////////////////////////////

/*var uploadfiles = document.getElementById('doc1');

uploadfiles.addEventListener('change', function () {
//console.log(codigo);
    var files = this.files;
        uploadFile(this.files[0],'doc1'); // call the function to upload the file
}, false);

var uploadfiles2 = document.getElementById('doc2');
uploadfiles2.addEventListener('change', function () {
//console.log(codigo);
    var files = this.files;
        uploadFile(this.files[0],'doc2'); // call the function to upload the file
}, false);

var uploadfiles3 = document.getElementById('doc3');

uploadfiles3.addEventListener('change', function () {
//console.log(codigo);
    var files = this.files;
        uploadFile(this.files[0],'doc3'); // call the function to upload the file
}, false);

var uploadfiles4 = document.getElementById('doc4');

uploadfiles4.addEventListener('change', function () {
//console.log(codigo);
    var files = this.files;
        uploadFile(this.files[0],'doc4'); // call the function to upload the file
}, false);

var uploadfiles5 = document.getElementById('doc5');

uploadfiles5.addEventListener('change', function () {
//console.log(codigo);
    var files = this.files;
        uploadFile(this.files[0],'doc5'); // call the function to upload the file
}, false);

var uploadfiles6 = document.getElementById('doc6');

uploadfiles6.addEventListener('change', function () {
//console.log(codigo);
    var files = this.files;
        uploadFile(this.files[0],'doc6'); // call the function to upload the file
}, false);

var uploadfiles7 = document.getElementById('doc7');

uploadfiles7.addEventListener('change', function () {
//console.log(codigo);
    var files = this.files;
        uploadFile(this.files[0],'doc7'); // call the function to upload the file
}, false);

var uploadfiles8 = document.getElementById('doc8');

uploadfiles8.addEventListener('change', function () {
//console.log(codigo);
    var files = this.files;
        uploadFile(this.files[0],'doc8'); // call the function to upload the file
}, false);
*/

///////////////////////////////funciones de la edicion de productos /////////////////////////////////////////////////
/*var control = document.getElementById("foto2");
control.addEventListener("change", function(event) {

    // When the control has changed, there are new files
var files = document.getElementById('foto2').files;
  if (files.length > 0) {
	var sizeByte = this.files[0].size / 1024 / 1024;
      
   if(sizeByte >.8){
	alert("El tamaño de la imagen supera el permitido(800Kb)");
	  }else{
		getBase6412(files[0]);
	}
    
  }


function getBase6412(file) {
   var reader = new FileReader();
   reader.readAsDataURL(file);
   reader.onload = function () {
     //console.log(reader.result);
var cad=reader.result;


document.getElementById("txtfoto2").value=cad;
	
//document.getElementById("MyElement").className ="thumb" ;
document.getElementById("imgfoto2").src=cad;
document.getElementById("imgfoto2").className ="thumb2" ;
document.getElementById("fotodiv2").className ="imag2" ;
  };
   reader.onerror = function (error) {
     console.log('Error: ', error);
   };
}

}, false);


function archivo222(evt) {
      var files = evt.target.files;      for (var i = 0, f; f = files[i]; i++) {
           if (!f.type.match('image.*')) {
                //continue;
		//alert("El archivo no es de tipo imagen");
		document.getElementById("fotodiv2").className ="imag";
		document.getElementById("foto2").value="";
		alert("El archivo no es de tipo imagen");
		return;
		//continue;	
           }
           var reader = new FileReader();
           reader.onload = (function(theFile) {
               return function(e) {
                      //document.getElementById("list3").innerHTML = ['<img class="thumb" src="', e.target.result,'" title="', escape(theFile.name), '"/>'].join('');
               };
           })(f);
           reader.readAsDataURL(f);
       }
}
    
  document.getElementById('foto2').addEventListener('change', archivo222, false);

var control = document.getElementById("fotodetalle2");
control.addEventListener("change", function(event) {

    // When the control has changed, there are new files
var files = document.getElementById('fotodetalle2').files;
  if (files.length > 0) {
	var sizeByte = this.files[0].size / 1024 / 1024;
      
   if(sizeByte >.8){
	alert("El tamaño de la imagen supera el permitido(800Kb)");
	  }else{
		getBase6422(files[0]);
	}
    
  }


function getBase6422(file) {
   var reader = new FileReader();
   reader.readAsDataURL(file);
   reader.onload = function () {
     //console.log(reader.result);
var cad=reader.result;


document.getElementById("txtfotodetalle2").value=cad;
	
//document.getElementById("MyElement").className ="thumb" ;
document.getElementById("imgfotodetalle2").src=cad;
document.getElementById("imgfotodetalle2").className ="thumb2" ;
document.getElementById("fotodetallediv2").className ="imag2" ;
  };
   reader.onerror = function (error) {
     console.log('Error: ', error);
   };
}

}, false);


function archivo22(evt) {
      var files = evt.target.files;      for (var i = 0, f; f = files[i]; i++) {
           if (!f.type.match('image.*')) {
                //continue;
		
		document.getElementById("fotodetalle2").value="";
		document.getElementById("fotodetallediv2").className ="imag";
		//setTimeout(function() {
   		alert("El archivo no es de tipo imagen");
		//	}, 300);
		//
		return;	
           }
           var reader = new FileReader();
           reader.onload = (function(theFile) {
               return function(e) {
                      //document.getElementById("list3").innerHTML = ['<img class="thumb" src="', e.target.result,'" title="', escape(theFile.name), '"/>'].join('');
               };
           })(f);
           reader.readAsDataURL(f);
       }
}
 document.getElementById('fotodetalle2').addEventListener('change', archivo22, false);
*/


////////////////////////////////////////////funciones para subir los archivos al servidor edicion//////////////////////////////////////////

/*var uploadfiles = document.getElementById('doc12');

uploadfiles.addEventListener('change', function () {
//console.log(codigo);
    var files = this.files;
	document.getElementById("imgdoc12").className ="thumb" ;
	document.getElementById("doc1div2").className ="imag"
        uploadFile2(this.files[0],'doc1'); // call the function to upload the file
}, false);

var uploadfiles2 = document.getElementById('doc22');

uploadfiles2.addEventListener('change', function () {
//console.log(codigo);
    var files = this.files;
	document.getElementById("imgdoc22").className ="thumb" ;
	document.getElementById("doc2div2").className ="imag"
        uploadFile2(this.files[0],'doc2'); // call the function to upload the file
}, false);

var uploadfiles3 = document.getElementById('doc32');

uploadfiles3.addEventListener('change', function () {
//console.log(codigo);
    var files = this.files;
	document.getElementById("imgdoc32").className ="thumb" ;
	document.getElementById("doc3div2").className ="imag"
        uploadFile2(this.files[0],'doc3'); // call the function to upload the file
}, false);

var uploadfiles4 = document.getElementById('doc42');

uploadfiles4.addEventListener('change', function () {
//console.log(codigo);
    var files = this.files;
	document.getElementById("imgdoc42").className ="thumb" ;
	document.getElementById("doc4div2").className ="imag"
        uploadFile2(this.files[0],'doc4'); // call the function to upload the file
}, false);

var uploadfiles5 = document.getElementById('doc52');

uploadfiles5.addEventListener('change', function () {
//console.log(codigo);
    var files = this.files;
	document.getElementById("imgdoc52").className ="thumb" ;
	document.getElementById("doc5div2").className ="imag"
        uploadFile2(this.files[0],'doc5'); // call the function to upload the file
}, false);

var uploadfiles6 = document.getElementById('doc62');

uploadfiles6.addEventListener('change', function () {
//console.log(codigo);
    var files = this.files;
	document.getElementById("imgdoc62").className ="thumb" ;
	document.getElementById("doc6div2").className ="imag"
        uploadFile2(this.files[0],'doc6'); // call the function to upload the file
}, false);

var uploadfiles7 = document.getElementById('doc72');

uploadfiles7.addEventListener('change', function () {
//console.log(codigo);
    var files = this.files;
	document.getElementById("imgdoc72").className ="thumb" ;
	document.getElementById("doc7div2").className ="imag"
        uploadFile2(this.files[0],'doc7'); // call the function to upload the file
}, false);

var uploadfiles8 = document.getElementById('doc82');

uploadfiles8.addEventListener('change', function () {
//console.log(codigo);
    var files = this.files;
	document.getElementById("imgdoc82").className ="thumb" ;
	document.getElementById("doc8div2").className ="imag"
        uploadFile2(this.files[0],'doc8'); // call the function to upload the file
}, false);

*/

</script>
</body>
</html>
