﻿<?php
require_once("usersFunction.php");
$app = new usersFunction();
	if(!$app->islogged()){
		echo "<script>window.top.location.href = 'logout.php';</script>";	
	}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="initial-scale=1.0,user-scalable=yes"/>

<link rel="stylesheet" href="../css/estilo2.css">
 <link rel="stylesheet" href="../css/jquery.dataTables.min.css">
 <script language="javascript" type="text/javascript" src="../js/gateway.js"></script>
<script language="javascript" type="text/javascript" src="../js/jquery-1.12.4.js"></script>
<script language="javascript" type="text/javascript" src="../js/jquery.dataTables.min.js"></script>
  <script language="javascript" type="text/javascript" src="../js/insumoscontrol.js"></script>
	<script language="javascript" src="../js/xlsx.full.min.js"></script>
	
	<script language="javascript" src="../js/FileSaver.js"></script>
	
	<script language="javascript" src="../js/tableexport.js"></script>
<title></title>

</head>
<body onload="getdata();">
<main>
	
	<section id="titulo">
		<center><h2>Control de Pedidos por Insumos</h2>
		</center>
	</section>
<div>
	<form name="f1" action="#">
		<center>
		<div ">
		<div id="myDiv">

				<!--div class="txt">
					Fecha Inicial<br>
					<input type="date"  id="txtfechainicial" required="required" class="TT" />	
				</div-->
				<!--div class="txt">
					Ingrese el pedido SAE<br>
					<input type="text"  id="txtpedidosae" required="required" class="TT" />	
				</div-->

				<div class="txt" style="display:none;">
					Satus<br>
					<select id="optionstatus" onChange="getOrdenes(this.value);"></select>
				</div>

				<div class="txt" style="display:none;">
					Orden<br>
					<select id="optionorder" onChange="getdata();"><option value=0>---Seleccione---</option></select>
				</div>

			

				<!--div class="txt">
						<input type="button" name="insertar" Value="Actualizar" onClick="getdata();" id="ok"/>
				</div-->
			<div >
				<img src="../img/load.gif" class="imgload" id="imgload">
			</div>
		</div>
		</center>
	</form>
</div>
<center>
</br>

	</br>
	<center>
		<div id="sin2" style="display:none;">
			<div id="myDiv">
				<!--div class="txt">
					Ingresa el pedido SAE a buscar<br>
					<input type="text" name="num" placeholder="Pedido SAE" id="txtbuscar" onkeyup="doSearch()" required="required" class="TT" />
				
				</div-->
				
			
			</div>
			<center>
		
				<div id="myDiv">
					<div class="txt">
						Pedido Retrasado<br>
						<img  style="background-color: #E87975; height: 30px; width: 30px;"/>
				
					</div>
					<div class="txt">
						Pedido A tiempo<br>
					
						<img  style="background-color: #9adbcf; height: 30px; width: 30px;" />
					</div>
				
			
				</div>
		
			</center>

		</div>
	</center>
		<center>
			<div id="msg" style="display:none;height:10%; width:15%; background:#ff6666;">
				
			</div>
		</center>
<div id="scro">
 <div id="tabla" style="display:none;">
     <table class="tbl-qa display nowrap" style="width:100%" id="resultado">
		  <thead>
			 <tr>
				<th class="table-header" rowspan="2" >Pedido SAE</th>
				<th class="table-header" rowspan="2">Producto</th>
				<th class="table-header" rowspan="2">Fecha Elaboración</th>
				<th class="table-header" rowspan="2">Fecha Insumos</th>
				<th class="table-header" rowspan="2">Status</th>
				<th class="table-header" colspan="2">Carton/Papel</th>
				<th class="table-header" colspan="2">Single Face</th>
				<th class="table-header" colspan="2">Pantone</th>
				<th class="table-header" colspan="2">Suaje</th>
				<th class="table-header" colspan="2">Embalaje</th>
				<th class="table-header" colspan="2">Personal Mano Obra</th>
				<!--th class="table-header" colspan="2">Dummy Garantia</th-->
				<th class="table-header" >Especificaciones</th>
				<th class="table-header" >Formación Suaje</th>	
				<th class="table-header" >Filmado placas</th>	
				<th class="table-header" >Entrega de Placas</th>	
				<th class="table-header" colspan="2">Prueba de color</th>	
				<th class="table-header" rowspan="2">Editar</th>
	                </tr>

			 <tr>
				<th class="table-header2" >No. Orden</th>
				<th class="table-header2" >Recepción</th>
				<th class="table-header2" >No. Orden</th>
				<th class="table-header2" >Recepción</th>
				<th class="table-header2" >No. Orden</th>
				<th class="table-header2" >Recepción</th>
				<th class="table-header2" >No. Orden</th>
				<th class="table-header2" >Recepción</th>
				<th class="table-header2" >No. Orden</th>
				<th class="table-header2" >Recepción</th>
				<th class="table-header2" >Junta</th>
				<th class="table-header2" >Contratación</th>
				<!--th class="table-header2" >Junta</th>
				<th class="table-header2" >Aprobación</th>	
				<th class="table-header2" >Junta</th-->	
				<th class="table-header2" >Publicación</th>
				<!--th class="table-header2" >Emisión</th-->
				<th class="table-header2" >Emisión</th>
				<!--th class="table-header2" >Emisión</th-->
				<th class="table-header2" >Emisión</th>	
				<th class="table-header2" >Emisión</th>
				
				
				<th class="table-header2" >Emisión</th>
				<th class="table-header2" >Aprobación</th>
	                </tr>
 		  </thead>
		
		  <tbody style="height:250px;overflow:scroll">					
             	 </tbody>
              </table>
 </div>
	</br></br>
		<div id="sin" style="display:none;">
			<div class='myDiv'><div class='txt'>NÚMERO DE PEDIDO NO ENCONTRADO</div></div>
		</div>
		
 </div>
</center>
</div>
	
</main>
</body>
</html>
