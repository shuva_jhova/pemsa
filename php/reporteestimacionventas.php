﻿<?php
require_once("usersFunction.php");
$app = new usersFunction();
	if(!$app->islogged()){
		echo "<script>window.top.location.href = 'logout.php';</script>";	
	}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="initial-scale=1.0,user-scalable=yes"/>

<link rel="stylesheet" href="../css/estilo2.css">
<link rel="stylesheet" href="../css/jquery.dataTables.min.css">
 <script language="javascript" type="text/javascript" src="../js/gateway.js"></script>
<script language="javascript" type="text/javascript" src="../js/jquery-1.12.4.js"></script>
<script language="javascript" type="text/javascript" src="../js/jquery.dataTables.min.js"></script>
  <script language="javascript" type="text/javascript" src="../js/reporteestimacionventas.js"></script>
<title></title>

</head>
<body onload="getvendedores();" >
<main>
	
	<section id="titulo">
		<center><h2> Estimación de Ventas</h2>
		</center>
	</section>

<div>
	<form name="f1" action="#">
		<center>
		<div id="sin2" style="display:block;">
			<div id="myDiv">
				
				<div class="txt">
					Fecha Inicio<br>
					<input type="date"  id="txtfechainicial" required="required" class="TT" />	
				</div>
				<div class="txt">
					Fecha Fin<br>
					<input type="date"  id="txtfechafinal" required="required" class="TT" />	
				</div>
				<div class="txt">
					Agente<br>
					<select id="optionagente"></select>
				</div>
				<div class="txt">
						<input type="button" name="insertar" Value="Actualizar" onClick="getquotations();" id="ok"/>
				</div>
			<div >
				<img src="../img/load.gif" class="imgload" id="imgload">
			</div>
		</div>
		</center>
	</form>
</div>

<center>
</br>
	<!--div class="txt">
		Ingresa los Datos de la Cotización a Buscar<br>
	<input type="text" name="num" placeholder="Cotización" id="txtbuscar" onkeyup="doSearch()" required="required" class="TT" />
				
	</div-->
				
	<div class="txt" id="print" style="display:none;">
			<input type="button" name="insertar" Value="Imprimir" onClick="printreport();" id="ok"/>
	</div>
	</br>
<div id="scro">
 <div id="tabla">
     <table class="tbl-qa display nowrap" style="width:100%" id="resultado">
		  <thead>
		 <tr>
		<th class="table-header" >Agente</th>
		<th class="table-header" >Cliente</th>
		<th class="table-header" >Fecha Registro</th>
                <th class="table-header" >Estimación Venta Anual</th>
		<th class="table-header" >Total de cotizaciones</th>
		<th class="table-header" >Total de Pedidos</th>
		<th class="table-header" >% de Éxito</th>
              </tr>
 		  </thead>
		  <tbody style="height:250px;overflow:scroll">				
             	 </tbody>
              </table>
 </div>
	</br></br>
		<div id="sin" style="display:none;">
			<div class='myDiv'><div class='txt'>NO HAY COTIZACIONES REGISTRADAS EN EL SISTEMA </div></div>
		</div>
 </div>
</center>
</div>
<div>
 		<table class="tbl-qa display nowrap" style="width:95%" align="center">
		  <thead>
		 <tr>
		<th class="table-header" >Total Clientes</th>
		<th class="table-header" id="totclientes"></th>
		<th class="table-header" >Total Pedidos</th>
		<th class="table-header" id="totpedidos"></th>
                <th class="table-header" >% de Conversión</th>
		<th class="table-header" id="conversion"></th>
		<th class="table-header" >Total Vencidas</th>
		<th class="table-header" id="totvencidas"></th>
		<th class="table-header" >Total Activas</th>
		<th class="table-header" id="totactivas"></th>
		

              </tr>
 		  </thead>
              </table>
</div>

</main>
</body>
</html>
