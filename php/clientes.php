﻿<?php
require_once("usersFunction.php");
$app = new usersFunction();
	if(!$app->islogged()){
		echo "<script>window.top.location.href = 'logout.php';</script>";	
	}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="initial-scale=1.0,user-scalable=yes"/>

<link rel="stylesheet" href="../css/estilo2.css">

<link rel="stylesheet" href="../css/jquery.dataTables.min.css">
 <script language="javascript" type="text/javascript" src="../js/gateway.js"></script>
<script language="javascript" type="text/javascript" src="../js/jquery-1.12.4.js"></script>
<script language="javascript" type="text/javascript" src="../js/jquery.dataTables.min.js"></script>
  <script language="javascript" type="text/javascript" src="../js/clientes.js"></script>
<title></title>

</head>
<body onload="getclientes();">
<main>
	
	<section id="titulo">
		<center><h2>Clientes del sistema</h2>
		</center>
	</section>
<div>
	<form name="f1" action="#">
		<center>
		<div id="sin2" style="display:none;">
			<div id="myDiv">
				<!--div class="txt">
					Ingresa el Nombre del Cliente a buscar<br>
					<input type="text" name="num" placeholder="Nombre del cliente" id="txtbuscar" onkeyup="doSearch()" required="required" class="TT" />
				
				</div-->
				<div class="txt">
						<input type="button" name="insertar" Value="Actualizar" onClick="getclientes();" id="ok"/>
				</div>
			<div >
				<img src="../img/load.gif" class="imgload" id="imgload">
			</div>
		</div>
		</center>
	</form>
</div>
<center>
</br>
	<div class="txt" id="addnew">
			<input type="button" name="insertar" Value="Agregar" onClick="agregar();" id="ok"/>
	</div>
	</br>
<div id="scro">
 <div id="tabla">
    
 <table class="tbl-qa display nowrap" style="width:100%"id="resultado">
		  <thead>
		 <tr>
		<th class="table-header" >ID</th>
		<th class="table-header" >Nombre o Razón Social</th>
		<!--th class="table-header" >Domicilio Fiscal</th-->
                <!--th class="table-header" >Persona de contacto</th>
	        <th class="table-header" >Teléfono de contacto</th-->
		<th class="table-header" >Agente</th>
		
		<th class="table-header" >Activo</th>
		<!--th class="table-header" style="display:none;">Perfil Logístico</th-->
		<th class="table-header" >Editar</th>
		<th class="table-header" >Eliminar</th>
		
              </tr>
 		  </thead>
		  <tbody style="height:250px;overflow:scroll">				
             	 </tbody>
              </table>
 </div>
	</br></br>
		<div id="sin" style="display:none;">
			<div class='myDiv'><div class='txt'>NO HAY CLIENTES REGISTRADOS EN EL SISTEMA </div></div>
		</div>
 </div>
</center>
</div>
<!--///////////////////////////////////////////////CONTACTOS CLIENTE/////////////////////////////////////////////////////////////////////////-->

<div id="myModalcontacto" class="modal">

  <!-- Modal content -->
  <div class="modal-content"> 
  <div class="modal-content2">	
    <span class="close" id="closecontacto">&times;</span>
			<section id="titulo">
        			<center></br><h2>Ingrese los datos del nuevo contacto</h2>
					     <h5>Asegurese de que los datos introducidos sean correctos</h5>
				</center>
			</section>
			</br>
	
	
	
		<center>
			<div class="txt">

				<input type="button" name="insertar" Value="Agregar" onClick="addcontacto();" id="ok"/>
			
			</div>
		</center>
	<div class="contenedor" id="contactos" style="display:none;">
		
		<div class="myDiv">
	
			
			<div class="txt">
				Nombre(s)<br>
				<input type="text" name="num" placeholder="Nombre" id="txtnombre" required="required" class="TT" />	
			</div>
			<div class="txt">
				Primer Apellido<br>
				<input type="text" name="num" placeholder="Primer Apellido" id="txtappat" required="required" class="TT" />	
			</div>
			<div class="txt">
				Segundo Apellido<br>
				<input type="text" name="num" placeholder="Segundo Apellido" id="txtapmat" required="required" class="TT" />	
			</div>
			<div class="txt">
				Telefono<br>
				<input type="number" name="num" maxlength="10" onKeyDown="if(this.value.length==10) return false;" placeholder="Telefono" id="txttelefono" required="required" class="TT" />	
			</div>
			<div class="txt">
				Extensión<br>
				<input type="number" name="num" maxlength="10" onKeyDown="if(this.value.length==10) return false;" placeholder="Extensión" id="txtextension" required="required" class="TT" />	
			</div>
			<div class="txt">
				Celular<br>
				<input type="number" name="num" maxlength="10" onKeyDown="if(this.value.length==10) return false;" placeholder="Telefono" id="txtcelular" required="required" class="TT" />	
			</div>
			<div class="txt">
				Correo<br>
				<input type="email" name="num"  placeholder="Correo electrónico" id="txtcorreo" required="required" class="TT" />	
			</div>
			<div class="txt">
				Área<br>
				<input type="text" name="num" placeholder="Área" id="txtarea" required="required" class="TT" />	
			</div>
			<div class="txt">
				Puesto<br>
				<input type="text" name="num" placeholder="Puesto" id="txtpuesto" required="required" class="TT" />	
			</div>

		</div>
		
			<center>
			<div class="txt">
				<input type="button" name="insertar" Value="Guardar" onClick="savecontacto();" id="ok"/>
			</div>
			<div class="txt">
				<input type="button" name="insertar" Value="Cancelar" onClick="canceladdcontacto();" id="ok"/>
			</div>
			</center>
	</div>
	<div id="tablacontatos">		
		

 		<div id="tablacontactos">
		<div id="scro" >	
   			  <table class="tbl-qa" id="resultadocontactos">
		 		 <thead>
					 <tr>
						<th class="table-header" >Nombre</th>
						<th class="table-header" >Teléfono</th>
						<th class="table-header" >Extensión</th>
						<th class="table-header" >Celular</th>
						<th class="table-header" >Correo Electrónico</th>
						<th class="table-header" >Área</th>
						<th class="table-header" >Puesto</th>
						<th class="table-header" >Editar</th>
						<th class="table-header" >Eliminar</th>
						
              				</tr>
 		  		</thead>
		  		<tbody >				
             	 		</tbody>
              		</table>
		</div>
 		</div>
		</br></br>
		<div id="sincontactos" style="display:none;">
			<div class='myDiv'><div class='txt'>NO HAY CONTACTOS REGISTRADOS EN EL SISTEMA </div></div>
		</div>
 	</div>
		



  </div>
  </div>
  
</div>





<!--////////////////////////////////////////////////CLIENTES/////////////////////////////////////////////////////////////////////////////////-->
<div id="myModal" class="modal">

  <!-- Modal content -->
  <div class="modal-content"> 
  <div class="modal-content2">	
	<datalist id="girosss"></datalist>
    <span class="close" id="closeclientee">&times;</span>
			<section id="titulo">
        			<center></br><h2>Ingrese los Datos del Nuevo Cliente</h2>
					     <h5>Asegurese de que los Datos Introducidos sean Correctos</h5>
				</center>
			</section>
			</br>
	<div class="contenedor">
			
	<div class="myDiv" >
		<div class="txt">
			Razón Social<br>
			<input type="text" name="num" placeholder="Razón Social" id="txtrazon" required="required" class="TT" />	
		</div>
		<div class="txt">
			Giro<br>
			<input type="text" name="num" placeholder="Giro" list="girosss" id="txtgiro" required="required" class="TT" />	
		</div>
		<div class="txt" style="display:none;">
			Domicilio Fiscal<br>
			<textarea rows="5" cols="30" name="dir" id="txtdomicilio" placeholder="..."></textarea> 	
		</div>
		<div class="txt" id="vendedor" style="display:none;">
			Vendedor Asociado<br>
			<select id="optionvendedor"></select>	
		</div>
		<div class="txt">
			Exportador<br>
			<label class="fondotxt">
				<input type='radio' name='exporta' value='1' ">Si
				<input type='radio' name='exporta' value='0'  checked='checked'">No
			</label>
		</div>

	</div>
			
			<section id="titulo">
        			<center>
					     <h5>Posibilidades de Venta</h5>
				</center>
			</section>
	<div class="myDiv" >
		<div class="txt">
			Etiqueta<br>
			<label class="fondotxt">
				<input type='radio' name='etiqueta' value='1' ">Si
				<input type='radio' name='etiqueta' value='0'  checked='checked'">No
			</label>
		</div>

		<div class="txt">
			Caja Plegadiza<br>
			<label class="fondotxt">
				<input type='radio' name='plegadiza' value='1' ">Si
				<input type='radio' name='plegadiza' value='0'  checked='checked'">No
			</label>
		</div>

		<div class="txt">
			Caja Single Face<br>
			<label class="fondotxt">
				<input type='radio' name='singleface' value='1' ">Si
				<input type='radio' name='singleface' value='0'  checked='checked'">No
			</label>
		</div>
		<div class="txt">
			Caja Plástico Corrugado<br>
			<label class="fondotxt">
				<input type='radio' name='cajaplastico' value='1' ">Si
				<input type='radio' name='cajaplastico' value='0'  checked='checked'">No
			</label>
		</div>
		<div class="txt">
			Displays<br>
			<label class="fondotxt">
				<input type='radio' name='display' value='1' ">Si
				<input type='radio' name='display' value='0'  checked='checked'">No
			</label>
		</div>


	</div>
			<section id="titulo">
        			<center>
					     <h5>Oportunidades</h5>
				</center>
			</section>
	<div class="myDiv" >

		<div class="txt">
			Corto Tiraje<br>
			<label class="fondotxt">
				<input type='radio' name='tiraje' value='1' ">Si
				<input type='radio' name='tiraje' value='0'  checked='checked'">No
			</label>
		</div>
		<div class="txt">
			Dato Variable.<br>
			<label class="fondotxt">
				<input type='radio' name='variable' value='1' ">Si
				<input type='radio' name='variable' value='0'  checked='checked'">No
			</label>
		</div>
		<div class="txt">
			Web to Print<br>
			<label class="fondotxt">
				<input type='radio' name='web' value='1' ">Si
				<input type='radio' name='web' value='0'  checked='checked'">No
			</label>
		</div>
		<div class="txt">
			Diseño e Ingeniería<br>
			<label class="fondotxt">
				<input type='radio' name='ingenieria' value='1' ">Si
				<input type='radio' name='ingenieria' value='0'  checked='checked'">No
			</label>
		</div>

		<div class="txt">
			Proyecto XMPIE<br>
			<label class="fondotxt">
				<input type='radio' name='xmpie' value='1' ">Si
				<input type='radio' name='xmpie' value='0'  checked='checked'">No
			</label>
		</div>

	</div>
			<section id="titulo">
        			<center>
					     <h5>Beneficios a Generar</h5>
				</center>
			</section>

	<div class="myDiv" >

		<div class="txt">
			Aumentar Ventas<br>
			<label class="fondotxt">
				<input type='radio' name='ventas' value='1' ">Si
				<input type='radio' name='ventas' value='0'  checked='checked'">No
			</label>
		</div>		

		<div class="txt">
			Mejorar Costos Evidentes<br>
			<label class="fondotxt">
				<input type='radio' name='costosevidentes' value='1' ">Si
				<input type='radio' name='costosevidentes' value='0'  checked='checked'">No
			</label>
		</div>
		<div class="txt">
			Mejorar Costos no Evidentes.<br>
			<label class="fondotxt">
				<input type='radio' name='costosnoevidentes' value='1' ">Si
				<input type='radio' name='costosnoevidentes' value='0'  checked='checked'">No
			</label>
		</div>
		<div class="txt" style="display:none;">
			Mejorar su participación de mercado.<br>
			<label class="fondotxt">
				<input type='radio' name='participacion' value='1' ">Si
				<input type='radio' name='participacion' value='0'  checked='checked'">No
			</label>
		</div>
		<div class="txt">
			Mejorar Satisfacción Clientes<br>
			<label class="fondotxt">
				<input type='radio' name='satisfaccion' value='1' ">Si
				<input type='radio' name='satisfaccion' value='0'  checked='checked'">No
			</label>
		</div>
		<div class="txt">
			Marcar Diferencia Competencia<br>
			<label class="fondotxt">
				<input type='radio' name='competencia' value='1' ">Si
				<input type='radio' name='competencia' value='0'  checked='checked'">No
			</label>
		</div>
		<div class="txt">
			Estimación de Venta Anual con Cliente<br>
			<label class="fondotxt">
				<input type='number' id='txtestimacionventa' value='0' ">
			</label>
		</div>
	</div>
	</br>
	<div class="myDiv" >

		<div class="txt">
			¿Cúal es la estrategia que seguirá con este cliente?<br>
			<label class="fondotxt">
				<textarea rows="10" cols="30" name="dir" id="txtestrategia" placeholder="..."></textarea> 	
			</label>
		</div>		
	</div>
	<div class="myDiv" style="display:none;">
		<div class="txt">
			Productos.<br>
			<textarea rows="10" cols="30" name="dir" id="txtproductos" placeholder="..."></textarea> 	
		</div>
		<div class="txt">
			Consumidor.<br>
			<textarea rows="10" cols="30" name="dir" id="txtconsumidor" placeholder="..."></textarea> 	
		</div>
		<div class="txt">
			Canal de Venta.<br>
			<textarea rows="10" cols="30" name="dir" id="txtcanal" placeholder="..."></textarea> 	
		</div>
		<div class="txt">
			Competencia.<br>
			<textarea rows="10" cols="30" name="dir" id="txtcompetencia" placeholder="..."></textarea> 	
		</div>
		<div class="txt">
			Visión y Estrategia General<br>
			<textarea rows="10" cols="30" name="dir" id="txtvision" placeholder="..."></textarea> 	
		</div>
		<div class="txt">
			Estimación de Venta Anual.<br>
			<textarea rows="10" cols="30" name="dir" id="txtestimacionventa1" placeholder="..."></textarea> 	
		</div>
		<div class="txt">
			Nececidades que el Prospecto Declara.<br>
			<textarea rows="10" cols="30" name="dir" id="txtnececidades" placeholder="..."></textarea> 	
		</div>
		<div class="txt">
			Problematica que el prospecto declara.<br>
			<textarea rows="10" cols="30" name="dir" id="txtproblematica" placeholder="..."></textarea> 	
		</div>
		<div class="txt">
			Proveedores actuales del prospecto.<br>
			<textarea rows="10" cols="30" name="dir" id="txtproveedores" placeholder="..."></textarea> 	
		</div>
		<div class="txt">
			Espectativas del prospecto con nuestra empresa.<br>
			<textarea rows="10" cols="30" name="dir" id="txtespectativas" placeholder="..."></textarea> 	
		</div>
						
	</div> 
			

	<center>
	<div class="txt">
	<input type="button" name="insertar" Value="Guardar" onClick="savecliente();" id="ok"/>
	</div>
	</center>
	</div>   
  </div>
  </div>
  
</div>



<!--////////////////////////////////////////////////////////////////////////////////////EDIT CLIENT//////////////////////////////////////////////////////////////////////-->

<div id="editclient" class="modal">

  <!-- Modal content -->
  <div class="modal-content"> 
  <div class="modal-content2">	
	
    <span class="close" id="closedit">&times;</span>
			<section id="titulo">
        			<center></br><h2>Ingrese los datos a modificar del Cliente</h2>
					     <h5>Asegurese de que los datos introducidos sean correctos</h5>
				</center>
			</section>
			</br>
	<div class="contenedor">
			
	<div class="myDiv" >
		<div class="txt">
			Razon social<br>
			<input type="text" name="num" placeholder="Razon Social" id="txtrazon2" required="required" class="TT" />	
		</div>
		<div class="txt">
			Giro<br>
			<input type="text" name="num" placeholder="Giro" list="girosss" id="txtgiro2" required="required" class="TT" />	
		</div>
		<div class="txt" style="display:none;">
			Domicilio Fiscal<br>
			<textarea rows="10" cols="30" name="dir" id="txtdomicilio2" placeholder="..."></textarea> 	
		</div>
		<div class="txt" id="vendedor2" style="display:none;">
			Vendedor Asociado<br>
			<select id="optionvendedor2"></select>	
		</div>
		<div class="txt">
			Exportador<br>
			<label class="fondotxt">
				<input type='radio' name='exporta2' value='1' ">Si
				<input type='radio' name='exporta2' value='0'  checked='checked'">No
			</label>
		</div>
		<div class="txt">
			Activo<br>
			<select name="num" placeholder="Puesto" id="optionactivo2" required="required" class="TT">
				
			</select>	
		</div>
	</div>
			<!--section id="titulo">
        			<center>
					     <h5>Persona de contacto.</h5>
				</center>
			</section>
	<div class="myDiv" >
		<div class="txt">
			Nombre(s)<br>
			<input type="text" name="num" placeholder="Nombre" id="txtnombre2" required="required" class="TT" />	
		</div>
		<div class="txt">
			Primer Apellido<br>
			<input type="text" name="num" placeholder="Primer Apellido" id="txtappat2" required="required" class="TT" />	
		</div>
		<div class="txt">
			Segundo Apellido<br>
			<input type="text" name="num" placeholder="Segundo Apellido" id="txtapmat2" required="required" class="TT" />	
		</div>
		<div class="txt">
			Telefono<br>
			<input type="text" name="num" maxlength="10" placeholder="Telefono" id="txttelefono2" required="required" class="TT" />	
		</div>
		<div class="txt">
			Área<br>
			<input type="text" name="num" placeholder="Área" id="txtarea2" required="required" class="TT" />	
		</div>
		<div class="txt">
			Puesto<br>
			<input type="text" name="num" placeholder="Puesto" id="txtpuesto2" required="required" class="TT" />	
		</div>
		
	</div-->
			<section id="titulo">
        			<center>
					     <h5>Posibilidades de Venta</h5>
				</center>
			</section>
	<div class="myDiv" >
		<div class="txt">
			Etiqueta<br>
			<label class="fondotxt">
				<input type='radio' name='etiqueta2' value='1' ">Si
				<input type='radio' name='etiqueta2' value='0'  checked='checked'">No
			</label>
		</div>

		<div class="txt">
			Caja Plegadiza<br>
			<label class="fondotxt">
				<input type='radio' name='plegadiza2' value='1' ">Si
				<input type='radio' name='plegadiza2' value='0'  checked='checked'">No
			</label>
		</div>

		<div class="txt">
			Caja Single Face<br>
			<label class="fondotxt">
				<input type='radio' name='singleface2' value='1' ">Si
				<input type='radio' name='singleface2' value='0'  checked='checked'">No
			</label>
		</div>
		<div class="txt">
			Caja Plástico Corrugado<br>
			<label class="fondotxt">
				<input type='radio' name='cajaplastico2' value='1' ">Si
				<input type='radio' name='cajaplastico2' value='0'  checked='checked'">No
			</label>
		</div>
		<div class="txt">
			Displays<br>
			<label class="fondotxt">
				<input type='radio' name='display2' value='1' ">Si
				<input type='radio' name='display2' value='0'  checked='checked'">No
			</label>
		</div>


	</div>
			<section id="titulo">
        			<center>
					     <h5>Oportunidades</h5>
				</center>
			</section>
	<div class="myDiv" >

		<div class="txt">
			Corto Tiraje<br>
			<label class="fondotxt">
				<input type='radio' name='tiraje2' value='1' ">Si
				<input type='radio' name='tiraje2' value='0'  checked='checked'">No
			</label>
		</div>
		<div class="txt">
			Dato Variable.<br>
			<label class="fondotxt">
				<input type='radio' name='variable2' value='1' ">Si
				<input type='radio' name='variable2' value='0'  checked='checked'">No
			</label>
		</div>
		<div class="txt">
			Web to Print<br>
			<label class="fondotxt">
				<input type='radio' name='web2' value='1' ">Si
				<input type='radio' name='web2' value='0'  checked='checked'">No
			</label>
		</div>
		<div class="txt">
			Diseño e Ingeniería<br>
			<label class="fondotxt">
				<input type='radio' name='ingenieria2' value='1' ">Si
				<input type='radio' name='ingenieria2' value='0'  checked='checked'">No
			</label>
		</div>

		<div class="txt">
			Proyecto XMPIE<br>
			<label class="fondotxt">
				<input type='radio' name='xmpie2' value='1' ">Si
				<input type='radio' name='xmpie2' value='0'  checked='checked'">No
			</label>
		</div>

	</div>
			<section id="titulo">
        			<center>
					     <h5>Beneficios a Generar</h5>
				</center>
			</section>

	<div class="myDiv" >

		<div class="txt">
			Aumentar Ventas<br>
			<label class="fondotxt">
				<input type='radio' name='ventas2' value='1' ">Si
				<input type='radio' name='ventas2' value='0'  checked='checked'">No
			</label>
		</div>		

		<div class="txt">
			Mejorar Costos Evidentes<br>
			<label class="fondotxt">
				<input type='radio' name='costosevidentes2' value='1' ">Si
				<input type='radio' name='costosevidentes2' value='0'  checked='checked'">No
			</label>
		</div>
		<div class="txt">
			Mejorar Costos no Evidentes.<br>
			<label class="fondotxt">
				<input type='radio' name='costosnoevidentes2' value='1' ">Si
				<input type='radio' name='costosnoevidentes2' value='0'  checked='checked'">No
			</label>
		</div>
		<div class="txt" style="display:none;">
			Mejorar su participación de mercado.<br>
			<label class="fondotxt">
				<input type='radio' name='participacion2' value='1' ">Si
				<input type='radio' name='participacion2' value='0'  checked='checked'">No
			</label>
		</div>
		<div class="txt">
			Mejorar Satisfacción Clientes<br>
			<label class="fondotxt">
				<input type='radio' name='satisfaccion2' value='1' ">Si
				<input type='radio' name='satisfaccion2' value='0'  checked='checked'">No
			</label>
		</div>
		<div class="txt">
			Marcar Diferencia Competencia<br>
			<label class="fondotxt">
				<input type='radio' name='competencia2' value='1' ">Si
				<input type='radio' name='competencia2' value='0'  checked='checked'">No
			</label>
		</div>
		<div class="txt">
			Estimación de Venta Anual con Cliente<br>
			<label class="fondotxt">
				<input type='number' id='txtestimacionventa2' value='0' ">
			</label>
		</div>
	</div>
	</br>
	<div class="myDiv" >

		<div class="txt">
			¿Cúal es la estrategia que Seguirá con este Cliente?<br>
			<label class="fondotxt">
				<textarea rows="10" cols="30" name="dir" id="txtestrategia2" placeholder="..."></textarea> 	
			</label>
		</div>		
	</div>
	<div class="myDiv" style="display:none;">
		<div class="txt">
			Productos.<br>
			<textarea rows="10" cols="30" name="dir" id="txtproductos2" placeholder="..."></textarea> 	
		</div>
		<div class="txt">
			Consumidor.<br>
			<textarea rows="10" cols="30" name="dir" id="txtconsumidor2" placeholder="..."></textarea> 	
		</div>
		<div class="txt">
			Canal de Venta.<br>
			<textarea rows="10" cols="30" name="dir" id="txtcanal2" placeholder="..."></textarea> 	
		</div>
		<div class="txt">
			Competencia.<br>
			<textarea rows="10" cols="30" name="dir" id="txtcompetencia2" placeholder="..."></textarea> 	
		</div>
		<div class="txt">
			Visión y Estrategia General<br>
			<textarea rows="10" cols="30" name="dir" id="txtvision2" placeholder="..."></textarea> 	
		</div>
		<div class="txt">
			Estimacion de venta anual.<br>
			<textarea rows="10" cols="30" name="dir" id="txtestimacionventa1" placeholder="..."></textarea> 	
		</div>
		<div class="txt">
			Nececidades que el prospecto declara.<br>
			<textarea rows="10" cols="30" name="dir" id="txtnececidades2" placeholder="..."></textarea> 	
		</div>
		<div class="txt">
			Problematica que el prospecto declara.<br>
			<textarea rows="10" cols="30" name="dir" id="txtproblematica2" placeholder="..."></textarea> 	
		</div>
		<div class="txt">
			Proveedores actuales del prospecto.<br>
			<textarea rows="10" cols="30" name="dir" id="txtproveedores2" placeholder="..."></textarea> 	
		</div>
		<div class="txt">
			Espectativas del prospecto con nuestra empresa.<br>
			<textarea rows="10" cols="30" name="dir" id="txtespectativas2" placeholder="..."></textarea> 	
		</div>
						
	</div> 
			

	<center>
	<div class="txt">
	<input type="button" name="insertar" Value="Guardar" onClick="updateCliente();" id="ok"/>
	</div>
	</center>
	</div>   
  </div>
  </div>
  
</div>






</main>
</body>
</html>
