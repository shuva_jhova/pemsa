﻿<?php
require_once("usersFunction.php");
$app = new usersFunction();
	if(!$app->islogged()){
		echo "<script>window.top.location.href = 'logout.php';</script>";	
	}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="initial-scale=1.0,user-scalable=yes"/>

<link rel="stylesheet" href="../css/estilo2.css">
<link rel="stylesheet" href="../css/jquery.dataTables.min.css">
 <script language="javascript" type="text/javascript" src="../js/gateway.js"></script>
<script language="javascript" type="text/javascript" src="../js/jquery-1.12.4.js"></script>
<script language="javascript" type="text/javascript" src="../js/jquery.dataTables.min.js"></script>
<script language="javascript" type="text/javascript" src="../js/numeral.min.js"></script>
  <script language="javascript" type="text/javascript" src="../js/porcentajeutilizacion.js"></script>

	<script language="javascript" src="../js/xlsx.full.min.js"></script>
	
	<script language="javascript" src="../js/FileSaver.js"></script>
	
	<script language="javascript" src="../js/tableexport.js"></script>

<title></title>

</head>
<body onload="getMaquinas();">
<main>
	
	<section id="titulo">
		<center><h2>Porcentaje de la Utilización de la Capacidad Instalada</h2>
		</center>
	</section>
<div>
	<form name="f1" action="#">
		<center>
		<div ">
			<div id="myDiv">
				<!--div class="txt">
					Ingresa el nombre del usuario a buscar<br>
					<input type="text" name="num" placeholder="Nombre del usuario" id="txtbuscar" onkeyup="doSearch()" required="required" class="TT" />
				
				</div-->
				<div class="txt">
					Fecha Inicial<br>
					<input type="date"  id="txtfechainicial" required="required" class="TT" />	
				</div>
				<div class="txt">
					Fecha Final<br>
					<input type="date"  id="txtfechafinal" required="required" class="TT" />	
				</div>

				<div class="txt">
					Máquina<br>
					<select id="optionmaquinas" onChange="getplaneacion();"></select>
				</div>

				<div class="txt">
						<input type="button" name="insertar" Value="Actualizar" onClick="getplaneacion();" id="ok"/>
				</div>
			<div >
				<img src="../img/load.gif" class="imgload" id="imgload">
			</div>
		</div>
		</center>
		<center id='prin' style="display:none;">
		<!--div class="txt">
			<input type="button" name="insertar" Value="Imprimir" onClick="print();" id="ok"/>
		</div-->
	</center>
	</form>
</div>
<center>
</br>

	</br>
<div id="scro">
 <div id="tabla">
	
     
 <table class="tbl-qa display nowrap" style="width:100%" id="resultado">
		  <thead>
			<tr>
				<!--th class="table-header" >No Orden</th>
				<th class="table-header" >Pedido SAE</th>
				<th class="table-header" >Producto SAE</th>
				<th class="table-header" >Descripción SAE</th>
				<th class="table-header" >Fecha Elaboración</th>
				<th class="table-header" >Fecha Entrega</th>
				<th class="table-header" >Piezas Ordenadas</th-->

				<th class="table-header" >Máquina</th>

				<!--th class="table-header" >Formatos/Sustrato</th>
				<th class="table-header" >Multiplos del Proceso</th>
				<th class="table-header" >Cantidad Proceso a Producir</th>
				<th class="table-header" >Horas</th>
				<th class="table-header" >Fecha de Inicio</th>
				<th class="table-header" >Hora de Inicio</th>
				<th class="table-header" >Fecha Fin</th>
				<th class="table-header" >Hora Fin</th-->

				<th class="table-header" >Estandar Por Hora</th>
				<th class="table-header" >Estandar por Día</th>
				<th class="table-header" >Días</th>
				<th class="table-header" >Estandar del Periodo</th>
				<th class="table-header" >Cantidad Total Producida</th>
				<th class="table-header" >% Utilizacion</th>
				
			</tr>
 		  </thead>
		
		  
		<tbody style="height:250px;overflow:scroll">	
				
             	 </tbody>
              </table>
 </div>
	</br></br>
		<div id="sin" style="display:none;">
			<div class='myDiv'><div class='txt'>SIN REGISTROS PARA LOS DATOS SELECCIONADOS</div></div>
		</div>
 </div>
</center>
</div>


</main>

</body>
</html>
