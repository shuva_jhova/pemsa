<?php
global $idorden;

$idorden=$_GET['omp'];
$app;
$datos=array();
require("../recursos/FPDFF/fpdf.php");

require_once("appControl.php");
$app=new appControl();
$var=date('d/m/y')." ".date('g:i:s a');

$datos=$app->getdataforqualitycertificate($idorden);

class PDF extends FPDF
{

var $widths;
var $aligns;

function SetWidths($w)
{
//Set the array of column widths
$this->widths=$w;
}

function SetAligns($a)
{
//Set the array of column alignments
$this->aligns=$a;
}

function Row($data,$border,$fill='D',$nb=0)
{
    //Calculate the height of the row
    if($nb==0){
    for($i=0;$i<count($data);$i++)
        $nb=max($nb,$this->NbLines($this->widths[$i],$data[$i]));
    $h=5*$nb;
     }else{
	 $h=5*$nb;
	}
    //Issue a page break first if needed
    $this->CheckPageBreak($h);
    //Draw the cells of the row
    for($i=0;$i<count($data);$i++)
    {
        $w=$this->widths[$i];
        $a=isset($this->aligns[$i]) ? $this->aligns[$i] : 'L';
        //Save the current position
        $x=$this->GetX();
        $y=$this->GetY();
        //Draw the border
	if($border==1){
        	$this->Rect($x,$y,$w,$h,$fill);
	}
        //Print the text
        $this->MultiCell($w,5,$data[$i],0,$a);
        //Put the position to the right of the cell
        $this->SetXY($x+$w,$y);
    }
    //Go to the next line
    $this->Ln($h);
}

function CheckPageBreak($h)
{
//If the height h would cause an overflow, add a new page immediately
if($this->GetY()+$h>$this->PageBreakTrigger)
$this->AddPage($this->CurOrientation);
}

function NbLines($w,$txt)
{
//Computes the number of lines a MultiCell of width w will take
$cw=&$this->CurrentFont['cw'];
if($w==0)
$w=$this->w-$this->rMargin-$this->x;
$wmax=($w-2*$this->cMargin)*1000/$this->FontSize;
$s=str_replace("\r",'',$txt);
$nb=strlen($s);
if($nb>0 and $s[$nb-1]=="\n")
$nb--;
$sep=-1;
$i=0;
$j=0;
$l=0;
$nl=1;
while($i<$nb)
{
$c=$s[$i];
if($c=="\n")
{
$i++;
$sep=-1;
$j=$i;
$l=0;
$nl++;
continue;
}
if($c==' ')
$sep=$i;
$l+=$cw[$c];
if($l>$wmax)
{
if($sep==-1)
{
if($i==$j)
$i++;
}
else
$i=$sep+1;
$sep=-1;
$j=$i;
$l=0;
$nl++;
}
else
$i++;
}
return $nl;
}
function Footer()
{
$this->SetY(-15);
// Select Arial italic 8

// Print current and total page numbers

/*$this->SetFont('Arial','',9);
$this->SetXY(15,240);
$this->Cell(180,4,'En el presente Certificado de Calidad, se hace constar que el Producto Terminado, del lote antes descrito,cumple las especifica-','',0,'J');
$this->SetXY(15,244);
$this->Cell(180,4,'ciones establecidas por el cliente, para los efectos que convengan al interesado. Despues de 48 horas de recibido el producto','',0,'J');
$this->SetXY(15,248);
$this->Cell(180,4,'terminado, no se aceptan reclamaciones.','',0,'J');
$this->SetFont('Arial','B',9);
$this->SetXY(15,265);
$this->Cell(180,15,'________________________________________','',0,'C');
$this->SetXY(15,270);
$this->Cell(180,15,'Aseguramiento de Calidad','',0,'C');*/
$this->SetFont('Arial','I',6);
$this->SetXY(15,280);
$this->Cell(180,4,'P�gina '.$this->PageNo().' de {nb}',0,0,'R');



}

function Header()
{
// Logo
/*$this->Image('encabezado.png',-1,-1,280);
$this->SetFont('Arial','B',16);
$this->SetX(380);
$this->SetY(20);
$this->Cell(257,10,'Plan de trabajo de la m�quina '.$GLOBALS['maq'],0,0,'R');
$this->Ln(7);
$this->Cell(257,10,'Del '.$GLOBALS['fechainicio'].' al '.$GLOBALS['fechafin'],0,0,'R');
*/
$this->SetFont('Arial','',9);
$this->SetXY(49,10);
$this->Cell(56,5,'Nombre del Documento',1,0,'C');
$this->SetXY(105,10);
$this->Cell(90,5,'Certificado de Calidad de Producto Terminado',1,0,'C');
$this->Image('encabezado.png',12,10,110);
$this->SetXY(49,15);
$this->Cell(20,5,'Revisi�n',1,0,'C');
$this->SetXY(69,15);
$this->Cell(23,5,'Fecha',1,0,'C');
$this->SetXY(92,15);
$this->Cell(41,5,'Elabor�',1,0,'C');
$this->SetXY(133,15);
$this->Cell(40,5,'Autoriz�',1,0,'C');
$this->SetXY(173,15);
$this->Cell(22,5,'C�digo',1,0,'C');
$this->SetXY(49,20);
$this->Cell(20,5,'02',1,0,'C');
$this->SetXY(69,20);
$this->Cell(23,5,'04.Julio.2018',1,0,'C');
$this->SetXY(92,20);
$this->Cell(41,5,'Olivia Castillo Z�rate',1,0,'C');
$this->SetXY(133,20);
$this->Cell(40,5,'Jes�s P�rez Miranda',1,0,'C');
$this->SetXY(173,20);
$this->Cell(22,5,'R-AC-005',1,0,'C');
$this->Line(15, 30, 195, 30);
$this->Ln(20);

}
}



$pdf=new PDF();
//$pdf=new PDF_MC_Table();
$pdf->AliasNbPages();
$pdf->PageNo();
$pdf->SetAuthor('JVL');
$pdf->setMargins(15,0,5);
$pdf->AddPage('P');
$pdf->SetTitle("PEMSA");
$pdf->SetAutoPageBreak(true,15);
$pdf->SetTopMargin(0);


$pdf->SetFont('Arial','',9);

$pdf->SetWidths(array(22,158));
$pdf->Row(array('Cliente:',iconv('UTF-8', 'windows-1252',$datos[0]['cliente'])),1);
$pdf->SetWidths(array(22,23,22,23,22,23,22,23));
$pdf->Row(array('C�digo del Producto:',iconv('UTF-8', 'windows-1252',$datos[0]['codigocliente']),'No Orden de Compra:',iconv('UTF-8', 'windows-1252',$datos[0]['orden']),'C�digo SAE:',iconv('UTF-8', 'windows-1252',$datos[0]['codigosae']),'Pedido SAE:',iconv('UTF-8', 'windows-1252',$datos[0]['pedidosae'])),1);
$pdf->SetWidths(array(45,135));
$pdf->Row(array('Nombre del Producto:',iconv('UTF-8', 'windows-1252',$datos[0]['nombreproducto'])),1);
$pdf->SetWidths(array(45,135));
$pdf->Row(array('Descripci�n del Producto:',iconv('UTF-8', 'windows-1252',$datos[0]['descripcion'])),1);
$pdf->Ln(5);
$pdf->SetFont('Arial','B',9);
$pdf->SetFillColor(0, 0, 0);
$pdf->SetTextColor(255,255,255);
$pdf->SetWidths(array(180));
$pdf->SetAligns(array('C'));
$pdf->Row(array('Procedimiento de Muestreo'),1,'FD');
$pdf->SetTextColor(0,0,0);
$pdf->SetFont('Arial','',9);
$pdf->SetFillColor(116,116,116);
$pdf->SetWidths(array(45,45,45,45));
$pdf->SetAligns(array('C','C','C','C'));
$pdf->Row(array('Tama�o del Lote','Tama�o de la Muestra','Cantidad para Rechazar','No. de Lote'),1,'FD');
$pdf->SetAligns(array('L','L','L','L'));
$pdf->Row(array('','','',iconv('UTF-8', 'windows-1252','                       
            '.$datos[0]['numorden']).' /'),1,'',3);
$pdf->Ln(3);
$pdf->SetWidths(array(180));
$pdf->SetAligns(array('C'));
$pdf->Row(array('Resultados de la Inspecci�n de Calidad'),1,'FD');
$pdf->SetAligns(array('L'));
$pdf->SetWidths(array(60,60,19,19,22));
$pdf->SetFillColor(220,220,220);
$pdf->SetAligns(array('C','C','C','C','C'));
$pdf->Row(array('Especificaci�n','Valor','Cumple','No Cumple','% Cumplimiento'),1,'FD');

$a=0;
$b=0;
$c=0;
if($datos[0]['a']==0){
	$a='';
}else{
	$a=$datos[0]['a'];
}
if($datos[0]['b']==0){
	$b='';
}else{
	$b=$datos[0]['b'];
}
if($datos[0]['h']==0){
	$h='';
}else{
	$h=$datos[0]['h'];
}


$pdf->SetAligns(array('L','L','L','L','L'));
$pdf->Row(array('Material',iconv('UTF-8', 'windows-1252',$datos[0]['material']),'','',''),1);
$pdf->Row(array('Ancho',iconv('UTF-8', 'windows-1252',$a),'','',''),1);
$pdf->Row(array('Largo',iconv('UTF-8', 'windows-1252',$b),'','',''),1);
$pdf->Row(array('Altura',iconv('UTF-8', 'windows-1252',$h),'','',''),1);
//trigger_error(print_r($datos,true));
for($a=0;$a<sizeof($datos[0]['especificaciones']);$a++){
		$pdf->Row(array(iconv('UTF-8', 'windows-1252',$datos[0]['especificaciones'][$a]['especificacion']),'','','',''),1,'');
}
$pdf->Row(array('','','','',''),1,'');
$pdf->Row(array('','','','',''),1,'');
$pdf->Ln(5);
$pdf->SetAligns(array('J'));
$pdf->SetWidths(array(180));
$pdf->Row(array('Este producto es aprobado para su entrega por haberse confirmado que cumple con las especificaciones establecidas por el cliente de acuerdo al procedimiento de liberaci�n de Producto Terminado y a las tablas Military Standard 105D, ANSI/ASQ Standard z1.4 e ISO 2859-1 (Vigente) Grado III Riguroso.'),1);
$pdf->Ln(3);
$pdf->SetFont('Arial','B',9);
$pdf->SetAligns(array('C','C','C'));
$pdf->SetWidths(array(100,40,40));
$pdf->Row(array('


Nombre y Firma  
Aseguramiento de Calidad','Fecha','Hora'),1,'',5);




$pdf->Output();
?>

