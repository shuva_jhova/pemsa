<?php
global $fechainicial;
global $fechafinal;

$fechainicial=$_GET['fecini'];
$fechafinal=$_GET['fecfin'];
$vendedor=$_GET['ven'];

$GLOBALS['fechainicio']=$fechainicial;
$GLOBALS['fechafin']=$fechafinal;

$app;
$datos=array();
require("../recursos/FPDFF/fpdf.php");

require_once("appControlcotizador.php");
$app=new appControlcotizador();


$datos=$app->getallquotationsbydateandseller($fechainicial,$fechafinal,$vendedor);
$GLOBALS['cotizaciones']=$datos[0]['totalcotizaciones'];
$GLOBALS['pedidos']=$datos[0]['totalpedidos'];
$GLOBALS['activas']=$datos[0]['totalactivas'];
$GLOBALS['vencidas']=$datos[0]['totalvencidas'];

$B=1;
$s=0;
$esp=4;
 $var=date('d/m/y')." ".date('g:i:s a');
class PDF extends FPDF
{

var $widths;
var $aligns;

function SetWidths($w)
{
    //Set the array of column widths
    $this->widths=$w;
}

function SetAligns($a)
{
    //Set the array of column alignments
    $this->aligns=$a;
}

function Row($data,$border,$fill='D')
{
    //Calculate the height of the row
    $nb=0;
    for($i=0;$i<count($data);$i++)
        $nb=max($nb,$this->NbLines($this->widths[$i],$data[$i]));
    $h=5*$nb;
    //Issue a page break first if needed
    $this->CheckPageBreak($h);
    //Draw the cells of the row
    for($i=0;$i<count($data);$i++)
    {
        $w=$this->widths[$i];
        $a=isset($this->aligns[$i]) ? $this->aligns[$i] : 'L';
        //Save the current position
        $x=$this->GetX();
        $y=$this->GetY();
        //Draw the border
	if($border==1){
        	$this->Rect($x,$y,$w,$h,$fill);
	}
        //Print the text
        $this->MultiCell($w,5,$data[$i],0,$a);
        //Put the position to the right of the cell
        $this->SetXY($x+$w,$y);
    }
    //Go to the next line
    $this->Ln($h);
}

function CheckPageBreak($h)
{
    //If the height h would cause an overflow, add a new page immediately
    if($this->GetY()+$h>$this->PageBreakTrigger)
        $this->AddPage($this->CurOrientation);
}

function NbLines($w,$txt)
{
    //Computes the number of lines a MultiCell of width w will take
    $cw=&$this->CurrentFont['cw'];
    if($w==0)
        $w=$this->w-$this->rMargin-$this->x;
    $wmax=($w-2*$this->cMargin)*1000/$this->FontSize;
    $s=str_replace("\r",'',$txt);
    $nb=strlen($s);
    if($nb>0 and $s[$nb-1]=="\n")
        $nb--;
    $sep=-1;
    $i=0;
    $j=0;
    $l=0;
    $nl=1;
    while($i<$nb)
    {
        $c=$s[$i];
        if($c=="\n")
        {
            $i++;
            $sep=-1;
            $j=$i;
            $l=0;
            $nl++;
            continue;
        }
        if($c==' ')
            $sep=$i;
        $l+=$cw[$c];
        if($l>$wmax)
        {
            if($sep==-1)
            {
                if($i==$j)
                    $i++;
            }
            else
                $i=$sep+1;
            $sep=-1;
            $j=$i;
            $l=0;
            $nl++;
        }
        else
            $i++;
    }
    return $nl;
}
	function Footer()
	{
		$this->SetY(-20);
		// Select Arial italic 8
		$this->SetFont('Arial','I',6);
    		// Print current and total page numbers
    

		$this->Cell(80,5, "",0,0,'L');
		
		$this->Ln();
		

		$this->Cell(0,10,'P�gina '.$this->PageNo().' de {nb}',0,0,'C');



	}

	function Header()
	{
		// Logo
		$this->Image('encabezado.png',-1,-1,280);
		$this->SetFont('Arial','B',16);
		$this->SetX(380);
		$this->SetY(20);
		$this->Cell(257,10,'Reporte de  Estimaci�n de Ventas',0,0,'R');
		$this->Ln(7);
		$this->Cell(257,10,'Del '.$GLOBALS['fechainicio'].' al '.$GLOBALS['fechafin'],0,0,'R');

		$this->Line(10, 35, 285, 35);
		$this->Ln(5);

	}
}


$pdf=new PDF();

$pdf->AliasNbPages();
$pdf->PageNo();
$pdf->SetAuthor('JVL');
$pdf->setMargins(15,5);
$pdf->AddPage('L');
$pdf->SetTitle("PEMSA");
$pdf->SetTopMargin(15);







//trigger_error(print_r($datos,true));

$pdf->Ln();


$pdf->SetFillColor(166, 166, 166);

$pdf->SetFont('Arial','B',8);
$pdf->SetWidths(array(38,38,38,38,38,38,38));
$pdf->Row(array('Agente','Cliente','Fecha Registro','Estimaci�n de Ventas Anual','Total Cotizaciones','Total Pedidos','% �xito'),1,'FD');

$pdf->SetFont('Arial','',7);


		
	$totpedidos=0;
	$totcotizaciones=0;
	$totactivas=0;
	$totvencidas=0;
	$totclientes=0;
$color=true;
	
	for($a=0;$a<sizeof($datos);$a++){
	
				$dat=explode(" ",$datos[$a]['usuario']);
				$vendedor=strtoupper (substr($dat[0], 0, 1)).strtoupper (substr($dat[1], 0, 1)).strtoupper (substr($dat[2], 0, 1));			

			$porc=0;
			if($datos[$a]["totalcotizaciones"]==0){
				$porc=0;
			}else{
			        $porc=($datos[$a]["totalpedidos"]/$datos[$a]["totalcotizaciones"])*100 ;
			}
				if($color){
					$pdf->SetFillColor(242, 242, 242);
					$pdf->Row(array(iconv('UTF-8', 'windows-1252',$datos[$a]['agente']),iconv('UTF-8', 'windows-1252',$datos[$a]['cliente']),iconv('UTF-8', 'windows-1252',$datos[$a]['registro']),iconv('UTF-8', 'windows-1252',$datos[$a]['ventas']),number_format(iconv('UTF-8', 'windows-1252',$datos[$a]['totalcotizaciones'])),iconv('UTF-8', 'windows-1252',$datos[$a]['totalpedidos']),round($porc,2)),1,'FD');
				
				}else{
						$pdf->SetFillColor(217, 217, 217);
					$pdf->Row(array(iconv('UTF-8', 'windows-1252',$datos[$a]['agente']),iconv('UTF-8', 'windows-1252',$datos[$a]['cliente']),iconv('UTF-8', 'windows-1252',$datos[$a]['registro']),iconv('UTF-8', 'windows-1252',$datos[$a]['ventas']),number_format(iconv('UTF-8', 'windows-1252',$datos[$a]['totalcotizaciones'])),iconv('UTF-8', 'windows-1252',$datos[$a]['totalpedidos']),round($porc,2)),1,'FD');
				}
	
				$color=!$color;
				
	$totpedidos+=$datos[$a]['totalpedidos'];
	$totcotizaciones+=$datos[$a]['totalcotizaciones'];
	$totactivas+=$datos[$a]['totalactivas'];
	$totclientes++;
	$totvencidas+=$datos[$a]['totalvencidas'];	
	}
			$conver=0;

	
			if($totcotizaciones==0){
				$conver=0;
			}else{
			        $conver=($totpedidos/$totcotizaciones)*100;
			}
$pdf->Ln();
$pdf->SetFillColor(166, 166, 166);
$pdf->SetWidths(array(25,20,25,15,25,20,23,23,23,23,23,22));
$pdf->Row(array('Total Clientes',$totclientes,'Total Cotizaciones',$totcotizaciones,'Total Pedidos',$totpedidos ,'% �xito',round($conver,2).'%','Total Activas',$totactivas,'Total Vencidas',$totvencidas),1,'FD');
$pdf->Output();
?>
