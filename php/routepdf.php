<?php
global $idorden;

$idorden=$_GET['ido'];
$app;
$datos=array();
require("../recursos/FPDFF/fpdf.php");

require_once("appControl.php");
$app=new appControl();

$B=1;
$s=0;
$esp=4;
 $var=date('d/m/y')." ".date('g:i:s a');
class PDF extends FPDF
{

var $widths;
var $aligns;

function SetWidths($w)
{
    //Set the array of column widths
    $this->widths=$w;
}

function SetAligns($a)
{
    //Set the array of column alignments
    $this->aligns=$a;
}

function Row($data,$border,$fill='D')
{
    //Calculate the height of the row
    $nb=0;
    for($i=0;$i<count($data);$i++)
        $nb=max($nb,$this->NbLines($this->widths[$i],$data[$i]));
    $h=5*$nb;
    //Issue a page break first if needed
    $this->CheckPageBreak($h);
    //Draw the cells of the row
    for($i=0;$i<count($data);$i++)
    {
        $w=$this->widths[$i];
        $a=isset($this->aligns[$i]) ? $this->aligns[$i] : 'L';
        //Save the current position
        $x=$this->GetX();
        $y=$this->GetY();
        //Draw the border
	if($border==1){
        	$this->Rect($x,$y,$w,$h,$fill);
	}
        //Print the text
        $this->MultiCell($w,5,$data[$i],0,$a);
        //Put the position to the right of the cell
        $this->SetXY($x+$w,$y);
    }
    //Go to the next line
    $this->Ln($h);
}

function CheckPageBreak($h)
{
    //If the height h would cause an overflow, add a new page immediately
    if($this->GetY()+$h>$this->PageBreakTrigger)
        $this->AddPage($this->CurOrientation);
}

function NbLines($w,$txt)
{
    //Computes the number of lines a MultiCell of width w will take
    $cw=&$this->CurrentFont['cw'];
    if($w==0)
        $w=$this->w-$this->rMargin-$this->x;
    $wmax=($w-2*$this->cMargin)*1000/$this->FontSize;
    $s=str_replace("\r",'',$txt);
    $nb=strlen($s);
    if($nb>0 and $s[$nb-1]=="\n")
        $nb--;
    $sep=-1;
    $i=0;
    $j=0;
    $l=0;
    $nl=1;
    while($i<$nb)
    {
        $c=$s[$i];
        if($c=="\n")
        {
            $i++;
            $sep=-1;
            $j=$i;
            $l=0;
            $nl++;
            continue;
        }
        if($c==' ')
            $sep=$i;
        $l+=$cw[$c];
        if($l>$wmax)
        {
            if($sep==-1)
            {
                if($i==$j)
                    $i++;
            }
            else
                $i=$sep+1;
            $sep=-1;
            $j=$i;
            $l=0;
            $nl++;
        }
        else
            $i++;
    }
    return $nl;
}
	function Footer()
	{
		$this->SetY(-20);
		// Select Arial italic 8
		$this->SetFont('Arial','I',6);
    		// Print current and total page numbers
    
		$this->Cell(80,5, "",0,0,'L');
		
		$this->Ln();
		
		$this->Cell(0,10,'P�gina '.$this->PageNo().' de {nb}',0,0,'C');



	}

	function Header()
{
// Logo

$this->SetFont('Arial','',9);
$this->SetXY(49,10);
$this->Cell(115,5,'Nombre del Documento',1,0,'C');
$this->SetXY(164,10);
$this->Cell(115,5,'Ruta de Producci�n',1,0,'C');
$this->Image('encabezado.png',12,10,110);
$this->SetXY(49,15);
$this->Cell(46,5,'Revisi�n',1,0,'C');
$this->SetXY(95,15);
$this->Cell(46,5,'Fecha',1,0,'C');
$this->SetXY(141,15);
$this->Cell(46,5,'Elabor�',1,0,'C');
$this->SetXY(187,15);
$this->Cell(46,5,'Autoriz�',1,0,'C');
$this->SetXY(233,15);
$this->Cell(46,5,'C�digo',1,0,'C');
$this->SetXY(49,20);
$this->Cell(46,5,'00',1,0,'C');
$this->SetXY(95,20);
$this->Cell(46,5,'03.Julio.2018',1,0,'C');
$this->SetXY(141,20);
$this->Cell(46,5,'Por definir',1,0,'C');
$this->SetXY(187,20);
$this->Cell(46,5,'Por definir',1,0,'C');
$this->SetXY(233,20);
$this->Cell(46,5,'Por definir',1,0,'C');
$this->Line(15, 30, 280, 30);
$this->Ln(15);
}
}


$pdf=new PDF();
//$pdf=new PDF_MC_Table();
$pdf->AliasNbPages();
$pdf->PageNo();
$pdf->SetAuthor('JVL');
$pdf->setMargins(15,5);
$pdf->AddPage('L');
$pdf->SetTitle("PEMSA");
$pdf->SetTopMargin(40);





$datos=$app->detailorder($idorden);

//trigger_error(print_r($datos,true));

$clavesae=$datos[0]['detalleproducto'][0]['codigo'];
while(strlen($clavesae)<8){
	$clavesae="0".$clavesae;
}



//$pdf->Ln(15);
$pdf->SetFont('Arial','',10);
$pdf->SetWidths(array(20,15, 25,15,35,35,35,30,40,20));
$pdf->Row(array('No. Orden: ',$idorden,' Pedido SAE: ',$datos[0]['pedidosae'],' Fecha Emisi�n: ',$datos[0]['fechaemision'],' Fecha Requerida: ',$datos[0]['fecharequerida'],' Cantidad de Producto: ',number_format($datos[0]['cantidad'])." Pzs"),0);

$pdf->Ln();
$pdf->SetWidths(array(30,50, 30,100,15,25));
$pdf->Row(array('C�digo SAE: ',$clavesae,' Producto: ',iconv('UTF-8', 'windows-1252',$datos[0]['detalleproducto'][0]['nombre']),'Merma:',$datos[0]['procesos'][0]['mermaestimada']." Pliegos"),0);

$pdf->Ln();
$pdf->SetFont('Arial','B',10);
$pdf->SetWidths(array(260));
$pdf->Row(array('Descripci�n: '),0);


$pdf->SetFont('Arial','B',10);
$pdf->SetWidths(array(260));
$pdf->Row(array(iconv('UTF-8', 'windows-1252',$datos[0]['detalleproducto'][0]['descripcion'])),0);


$pdf->SetFillColor(220,220,220);
$pdf->Ln();
$pdf->SetFont('Arial','B',10);
$pdf->SetWidths(array(260));
$pdf->Row(array('P R O C E S O S '),1,'F');


$pdf->SetFillColor(166, 166, 166);
$pdf->Ln();
$pdf->SetFont('Arial','B',8);
$pdf->SetWidths(array(30,30,28,30,30,20,46,46));
$pdf->Row(array('M�quina','Proceso','Cantidad de Tickets','Cantidad de Proceso Entrada','Cantidad de Proceso Salida','Cantidad �til de Proceso','Fecha Inicio','Fecha Fin'),1,'FD');

$pdf->SetFont('Arial','',7);

//

$color=true;
$vec=$datos[0]['procesos'];
//trigger_error(print_r($vec,true));
	$cantidadpliegos;
	$cantidadtiros;
	$tickets=0;
for($a=0;$a<sizeof($vec);$a++){
	$cantotprocess=$vec[$a]['cantidad'];
	$mermaproceso=$vec[$a]['mermaestimada'];
	$cant=$cantotprocess-$mermaproceso;
	
	if($a>0){
		if($vec[$a-1]['maquina']==$vec[$a]['maquina']){
			$cantidadpliegos+=$cant;
			$cantidadtiros+=$cantotprocess;
			$tickets++;
			//$pdf->Row(array(iconv('UTF-8', 'windows-1252',$vec[$a]['maquina']),iconv('UTF-8', 'windows-1252',$vec[$a]['proceso']),number_format($tickets),number_format($cantidadpliegos),number_format($cantidadtiros),iconv('UTF-8', 'windows-1252',$datos[0]['fecharequerida'])),1,'FD');
			if($a==sizeof($vec)-1){
				$tickets++;
				if($color){
					$pdf->SetFillColor(242, 242, 242);
					$pdf->Row(array(iconv('UTF-8', 'windows-1252',$vec[$a]['maquina']),iconv('UTF-8', 'windows-1252',$vec[$a]['proceso']),number_format($tickets),number_format($cantidadtiros),number_format($cantidadtiros),'','',''),1,'FD');
				
				}else{
					$pdf->SetFillColor(217, 217, 217);
					$pdf->Row(array(iconv('UTF-8', 'windows-1252',$vec[$a]['maquina']),iconv('UTF-8', 'windows-1252',$vec[$a]['proceso']),number_format($tickets),number_format($cantidadtiros),number_format($cantidadtiros),'','',''),1,'FD');
				}
	
				$color=!$color;
				
			}
		}else{
			
			$tickets++;
			if($color){
				$pdf->SetFillColor(242, 242, 242);
				$pdf->Row(array(iconv('UTF-8', 'windows-1252',$vec[$a-1]['maquina']),iconv('UTF-8', 'windows-1252',$vec[$a-1]['proceso']),number_format($tickets),number_format($cantidadtiros),number_format($cantidadtiros),'','',''),1,'FD');
				
			}else{
				$pdf->SetFillColor(217, 217, 217);
				$pdf->Row(array(iconv('UTF-8', 'windows-1252',$vec[$a-1]['maquina']),iconv('UTF-8', 'windows-1252',$vec[$a-1]['proceso']),number_format($tickets),number_format($cantidadtiros),number_format($cantidadtiros),'','',''),1,'FD');
			}


			$color=!$color;
			$cantotprocess=$vec[$a]['cantidad'];
			$mermaproceso=$vec[$a]['mermaestimada'];
			$cant=$cantotprocess-$mermaproceso;
			$cantidadpliegos=$cant;
			$cantidadtiros=$cantotprocess;	
			$tickets=0;
			if($a==sizeof($vec)-1){
				$tickets++;
				if($color){
					$pdf->SetFillColor(242, 242, 242);
					$pdf->Row(array(iconv('UTF-8', 'windows-1252',$vec[$a]['maquina']),iconv('UTF-8', 'windows-1252',$vec[$a]['proceso']),number_format($tickets),number_format($cantidadtiros),number_format($cantidadtiros),'','',''),1,'FD');
				
				}else{
					$pdf->SetFillColor(217, 217, 217);
					$pdf->Row(array(iconv('UTF-8', 'windows-1252',$vec[$a]['maquina']),iconv('UTF-8', 'windows-1252',$vec[$a]['proceso']),number_format($tickets),number_format($cantidadtiros),number_format($cantidadtiros),'','',''),1,'FD');
				}
	
				$color=!$color;
				
			}
		}
	}else{
		$cantidadpliegos+=$cant;
		$cantidadtiros+=$cantotprocess;		
	}
}




		
	
$pdf->Output();
?>