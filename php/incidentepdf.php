<?php
global $idorden;

$id=$_GET['idi'];
//$fechainicio=$_GET['fecini'];
//$fechafin=$_GET['fecfi'];
$app;
$datos=array();
require("../recursos/FPDFF/fpdf.php");

require_once("appControl.php");
$app=new appControl();

$B=1;
$s=0;
$esp=4;
$var=date('d/m/y')." ".date('g:i:s a');

$datos=$app->getspecificincident($id);
//$GLOBALS['maq']=$datos[0]['maquina'];
/*
//trigger_error(print_r($datos,true));

$clavesae=$datos[0]['detalleproducto'][0]['codigo'];
while(strlen($clavesae)<8){
$clavesae="0".$clavesae;
}*/
//$GLOBALS['cve']=$clavesae;
$GLOBALS['fechainicio']=$fechainicio;
$GLOBALS['fechafin']=$fechafin;
class PDF extends FPDF
{

var $widths;
var $aligns;

function SetWidths($w)
{
//Set the array of column widths
$this->widths=$w;
}

function SetAligns($a)
{
//Set the array of column alignments
$this->aligns=$a;
}

function Row($data,$border,$fill='D',$nb=0)
{
    //Calculate the height of the row
    if($nb==0){
    for($i=0;$i<count($data);$i++)
        $nb=max($nb,$this->NbLines($this->widths[$i],$data[$i]));
    $h=5*$nb;
     }else{
	 $h=5*$nb;
	}
    //Issue a page break first if needed
    $this->CheckPageBreak($h);
    //Draw the cells of the row
    for($i=0;$i<count($data);$i++)
    {
        $w=$this->widths[$i];
        $a=isset($this->aligns[$i]) ? $this->aligns[$i] : 'L';
        //Save the current position
        $x=$this->GetX();
        $y=$this->GetY();
        //Draw the border
	if($border==1){
        	$this->Rect($x,$y,$w,$h,$fill);
	}
        //Print the text
        $this->MultiCell($w,5,$data[$i],0,$a);
        //Put the position to the right of the cell
        $this->SetXY($x+$w,$y);
    }
    //Go to the next line
    $this->Ln($h);
}
function CheckPageBreak($h)
{
//If the height h would cause an overflow, add a new page immediately
if($this->GetY()+$h>$this->PageBreakTrigger)
$this->AddPage($this->CurOrientation);
}

function NbLines($w,$txt)
{
//Computes the number of lines a MultiCell of width w will take
$cw=&$this->CurrentFont['cw'];
if($w==0)
$w=$this->w-$this->rMargin-$this->x;
$wmax=($w-2*$this->cMargin)*1000/$this->FontSize;
$s=str_replace("\r",'',$txt);
$nb=strlen($s);
if($nb>0 and $s[$nb-1]=="\n")
$nb--;
$sep=-1;
$i=0;
$j=0;
$l=0;
$nl=1;
while($i<$nb)
{
$c=$s[$i];
if($c=="\n")
{
$i++;
$sep=-1;
$j=$i;
$l=0;
$nl++;
continue;
}
if($c==' ')
$sep=$i;
$l+=$cw[$c];
if($l>$wmax)
{
if($sep==-1)
{
if($i==$j)
$i++;
}
else
$i=$sep+1;
$sep=-1;
$j=$i;
$l=0;
$nl++;
}
else
$i++;
}
return $nl;
}
function Footer()
{
$this->SetY(-15);
// Select Arial italic 8
$this->SetFont('Arial','I',8);
// Print current and total page numbers
$this->Cell(180,2, "Este documento se debe de imprimir en: original (�rea que Reporta) y 1 copia (Recursos Humanos)",'',0,'R');
$this->Ln(2);
$this->SetFont('Arial','I',6);
$this->Cell(0,10,'P�gina '.$this->PageNo().' de {nb}',0,0,'C');



}

function Header()
{
// Logo
/*$this->Image('encabezado.png',10,0,280);
$this->SetFont('Arial','B',16);
$this->SetX(10);
$this->SetY(15);
$this->Cell(180,10,'Reporte de Incidente',0,0,'R');
$this->Ln(7);


$this->Line(15, 35, 195, 35);
$this->Ln(20);*/
// Logo
/*$this->Image('encabezado.png',-1,-1,280);
$this->SetFont('Arial','B',16);
$this->SetX(380);
$this->SetY(20);
$this->Cell(257,10,'Plan de trabajo de la m�quina '.$GLOBALS['maq'],0,0,'R');
$this->Ln(7);
$this->Cell(257,10,'Del '.$GLOBALS['fechainicio'].' al '.$GLOBALS['fechafin'],0,0,'R');
*/
$this->SetFont('Arial','',9);
$this->SetXY(49,10);
$this->Cell(56,5,'Nombre del Documento',1,0,'C');
$this->SetXY(105,10);
$this->Cell(90,5,'Reporte de Incidente',1,0,'C');
$this->Image('encabezado.png',12,10,110);
$this->SetXY(49,15);
$this->Cell(20,5,'Revisi�n',1,0,'C');
$this->SetXY(69,15);
$this->Cell(23,5,'Fecha',1,0,'C');
$this->SetXY(92,15);
$this->Cell(41,5,'Elabor�',1,0,'C');
$this->SetXY(133,15);
$this->Cell(40,5,'Autoriz�',1,0,'C');
$this->SetXY(173,15);
$this->Cell(22,5,'C�digo',1,0,'C');
$this->SetXY(49,20);
$this->Cell(20,5,'02',1,0,'C');
$this->SetXY(69,20);
$this->Cell(23,5,'04.Junio.2018',1,0,'C');
$this->SetXY(92,20);
$this->Cell(41,5,'Ram�n Rios Hern�ndez',1,0,'C');
$this->SetXY(133,20);
$this->Cell(40,5,'Jes�s P�rez Miranda',1,0,'C');
$this->SetXY(173,20);
$this->Cell(22,5,'R-PCP-003',1,0,'C');
$this->Line(15, 30, 195, 30);
$this->Ln(20);
}
}



$pdf=new PDF();
//$pdf=new PDF_MC_Table();
$pdf->AliasNbPages();
$pdf->PageNo();
$pdf->SetAuthor('JVL');
$pdf->setMargins(15,0,5);
$pdf->AddPage('P');
$pdf->SetTitle("PEMSA");
$pdf->SetAutoPageBreak(true,15);
$pdf->SetTopMargin(0);
$pdf->SetFont('Arial','',7);
$pdf->SetAligns(array('J'));
$pdf->SetWidths(array(180));
$pdf->Row(array('Procedimiento para incidentes: El reporte de incidente, una vez elaborado, debe de entregarse al departamento de Recursos humanos quien registrara el incidente en la base de datos de personal. El comit� convocar� una entrevista con los involucrados quienes deben definir la causa ra�z y definir acciones correctivas. Recursos Humanos retroalimentar� a los involucrados. Este reporte puede ir acompa�ado de material adicional como fotograf�as, informes adicionales, firma de testigos sobre el documento o mayor descripci�n de hechos al reverso'),0,'FD');
$pdf->Ln(5);
$pdf->SetFillColor(180,180,180);
$pdf->SetFont('Arial','B',10);
$pdf->SetAligns(array('C','C','C','C','C'));
$pdf->SetWidths(array(42,40,40,46,12));
$pdf->Row(array('Fecha y hora','�rea que reporta','�rea reportada','Responsable de la no conformidad','OMP'),1,'FD');
$pdf->SetFont('Arial','',10);
$pdf->Row(array(iconv('UTF-8', 'windows-1252',$datos[0]['fecha']),iconv('UTF-8', 'windows-1252',$datos[0]['areadenuncia']),iconv('UTF-8', 'windows-1252',$datos[0]['areadenunciada']),iconv('UTF-8', 'windows-1252',ucwords($datos[0]['nombredenunciado']).' '.ucwords($datos[0]['apellidosdenunciado'])),$datos[0]['idorden']),1,'');
$pdf->Ln(5);
//$pdf->SetFillColor(166, 166, 166);
//$pdf->SetFillColor(220,220,220);
//$pdf->SetFont('Arial','B',8);
$pdf->SetWidths(array(180));
$pdf->SetFont('Arial','B',10);
$pdf->Row(array('Incidente (Para ser llenado por el �rea que reporta)'),1,'FD');
$pdf->SetAligns(array('J'));
$pdf->SetFont('Arial','',10);
$pdf->Row(array(iconv('UTF-8', 'windows-1252',$datos[0]['incidente'])),1,'');
$pdf->Ln(5);
$pdf->SetAligns(array('C','C','C','C','C'));
$pdf->SetWidths(array(180));
$pdf->SetFont('Arial','B',10);
$pdf->Row(array('Acci�n de contingencia inmediata (Para ser llenado por el �rea que reporta)'),1,'FD');
$pdf->SetAligns(array('J'));
$pdf->SetFont('Arial','',10);
$pdf->Row(array(iconv('UTF-8', 'windows-1252',$datos[0]['contingencia'])),1,'');
$pdf->Ln(5);
$pdf->SetAligns(array('C','C','C','C','C'));
$pdf->SetWidths(array(180));
$pdf->SetFont('Arial','B',10);
$pdf->Row(array('Descripci�n de la causa ra�z (Para ser llenado por el responsable de la no conformidad)'),1,'FD');
$pdf->SetAligns(array('J'));
$pdf->SetFont('Arial','',10);
$pdf->Row(array(iconv('UTF-8', 'windows-1252',$datos[0]['causa'])),1,'');
$pdf->Ln(5);
$pdf->SetAligns(array('C','C','C','C','C'));
$pdf->SetWidths(array(180));
$pdf->SetFont('Arial','B',10);
$pdf->Row(array('Acci�n/es para erradicar el incidente de forma permanente (Para ser llenado por el responsable de la no conformidad)'),1,'FD');
$pdf->SetAligns(array('J'));
$pdf->SetFont('Arial','',10);
$pdf->Row(array(iconv('UTF-8', 'windows-1252',$datos[0]['erradicar'])),1,'');
$pdf->Ln(5);
$pdf->SetAligns(array('C','C','C','C','C'));
$pdf->SetWidths(array(100,40,40));
$pdf->SetFont('Arial','B',10);
$pdf->Row(array('Reporta','Responsable','Recursos Humanos'),1,'FD');
$pdf->SetFillColor(220, 220, 220);
$pdf->SetWidths(array(34,33,33,40,40));
$pdf->Row(array('Nombre','Firma','Fecha conclusi�n','Firma','Firma'),1,'FD');
$pdf->SetFont('Arial','',10);
$pdf->Row(array(iconv('UTF-8', 'windows-1252',ucwords($datos[0]['nombredenunciante']).' '.ucwords($datos[0]['apellidosdenunciante'])),'',$datos[0]['fechaconclusion'],'',''),1,'D',4);
//trigger_error(print_r($datos,true));

$pdf->Output();
?>

