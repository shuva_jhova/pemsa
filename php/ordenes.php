﻿<?php
require_once("usersFunction.php");
$app = new usersFunction();
	if(!$app->islogged()){
		echo "<script>window.top.location.href = 'logout.php';</script>";	
	}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta charset="utf-8">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="initial-scale=1.0,user-scalable=yes"/>

<link rel="stylesheet" href="../css/estilo2.css">
<link rel="stylesheet" href="../css/jquery.dataTables.min.css">
<script language="javascript" type="text/javascript" src="../recursos/tablefilter/tablefilter.js"></script>
 <script language="javascript" type="text/javascript" src="../js/gateway.js"></script>
<script language="javascript" type="text/javascript" src="../js/jquery-1.12.4.js"></script>
<script language="javascript" type="text/javascript" src="../js/jquery.dataTables.min.js"></script>
  <script language="javascript" type="text/javascript" src="../js/ordenes.js"></script>
 
<title></title>

</head>
<body onload="orders();">
<datalist id="paises"></datalist>
<datalist id="presentacion"></datalist>
<main>
	
	<section id="titulo">
		<center><h2>Ordenes Registradas</h2>
		</center>
	</section>
<div>
	<form name="f1" action="#">
		<center>
		<div id="sin2" style="display:none;">
			<div id="myDiv">
				<!--div class="txt">
					Ingresa el Nombre del Producto a Buscar<br>
					<input type="text" name="num" placeholder="Nombre del producto" id="txtbuscar" onkeyup="doSearch()" required="required" class="TT" />
				
				</div>
				<div class="txt">
						<input type="button" name="insertar" Value="Actualizar" onClick="orders();" id="ok"/>
				</div-->
			<div >
				<img src="../img/load.gif" class="imgload" id="imgload">
			</div>
		</div>
		</center>
	</form>
</div>
<center>
</br>
	<div class="txt" id="addnew">
			<input type="button" name="add" Value="Agregar" onClick="agregar();" id="ok"/>
	</div>
	</br>

<!--/////////////////////////////////////////////////////principal orders table//////////////////////////////////////////////////////////////////////////////-->


<div id="scro">

 <div id="tabla">
   <center>
		<section id="titulo">
				<center><h4>Simbología de Colores</h2>
				</center>
			</section>
				<br>
		<div id="myDiv">
			
			<div class="txt">
				Retrasada<br>
				<img  style="background-color: #f7786b; height: 30px; width: 30px;"/>
				
			</div>
			<div class="txt">
				En Proceso.<br>
					
				<img  style="background-color: #96ceb4; height: 30px; width: 30px;" />
			</div>
			<div class="txt">
				Sin Planear.<br>
					
				<img  style="background-color: #f2ae72; height: 30px; width: 30px;" />
			</div>
			<!--div class="txt">
				Especificaciones Completas.<br>
					
				<img  style="background-color:#88d4f7; height: 30px; width: 30px;" />
			</div-->
			
		</div>
	</center>
 <table class="tbl-qa display nowrap" style="width:100%" id="resultado">
		  <thead>
		 <tr>
		<th class="table-header" >No. de Orden  </th>
		<th class="table-header" >Pedido SAE </th>
		<th class="table-header" >Cliente </th>
		<th class="table-header" >Nombre del Producto</th>
		<th class="table-header" >Cantidad del Producto(Piezas)</th>
		<!--th class="table-header" >Cantidad ya producida</th>
		<th class="table-header" >Cantidad por producir</th-->
		<th class="table-header" >Fecha de Emisión</th>
		<th class="table-header" >Fecha Requerida</th>
		<th class="table-header" >Fecha Confirmada</th>
		<th class="table-header" >Estado</th>
		<th class="table-header" >Acciones</th>
		<th class="table-header" style='display:none;' >Ruta</th>
		<th class="table-header" style='display:none;' >Editar</th>
		<th class="table-header"style='display:none;'  >Eliminar</th>
		
		
              </tr>
 		  </thead>
		 <tbody style="height:250px;overflow:scroll">					
             	 </tbody>
              </table>
 </div>
	</br></br>
		<div id="sin" style="display:none;">
			<div class='myDiv'><div class='txt'>NO HAY ORDENES REGISTRADAS EN EL SISTEMA </div></div>
		</div>
 </div>
</center>
</div>




<!--/////////////////////////////////////////////////////add new order//////////////////////////////////////////////////////////////////////////////-->
<datalist id="clientess"></datalist>
<datalist id="codigoscliente"></datalist>

<div id="myModal" class="modal">

<datalist id="productos"></datalist>

  <!-- Modal content -->
  <div class="modal-content"> 
  <div class="modal-content2">
	
    <span class="close">&times;</span>
			<section id="titulo">
        			<center></br><h2>Ingrese los Datos de  la Nueva Orden</h2>
					     <h5>Asegurese de que los Datos Introducidos sean Correctos</h5>
				</center>
			</section>
			</br>
	<div class="contenedor">
			
	<div class="myDiv" >
		<div class="txt">
			Cliente<br>
			<input type="text"  id="txtcliente" list='clientess' required="required"  class="TT" />	
		</div>
		<div class="txt">
			Código de Producto del Cliente<br>
			<input type="text"  id="txtcodigocliente" list='codigoscliente' required="required"  class="TT" />	
		</div>
		<div class="txt">
			Orden de Compra del Cliente<br>
			<input type="text"  id="txtordencompra" required="required"  class="TT" />	
		</div>
	</div>
	<div class="myDiv" >
		<div class="txt">
			Pedido SAE<br>
			<input type="number"  id="txtpedidosae" required="required"  class="TT" />	
		</div>
		<div class="txt" style="display:none;">
			Fecha de Emisión de la Orden<br>
			<input type="date"  id="txtfechaemision"  required="required"  class="TT" />	
		</div>
		<div class="txt">
			Fecha Requerida<br>
			<input type="date"  id="txtfecharequerida" required="required" class="TT" />	
		</div>

		<div class="txt">
			Cantidad de Producto<br>
			 <input type="number" placeholder="Cantidad de Producto" id="txtcantidad"  required="required" class="TT" />	
		</div>
		<div class="txt">
			Producto<br>
			<input type="text" placeholder="Producto" id="txtproducto" list="productos" onBlur="getproductodetail();" required="required" class="TT" />
					
		</div>
		
		<div class="txt">
			Especificaciones<br>
			<textarea rows="10" cols="30" name="dir" id="txtdescripcion" placeholder="Especificaciones"></textarea> 	
		</div>
		

	</div>
			
	<br\>
		
	<center>
	<div class="txt">
	<input type="button" name="save"  Value="Guardar" onClick="saveorder();" id="ok"/>
	</div>
	</center>
	</div>   
  </div>
  </div>
  
</div>




<!--/////////////////////////////////////////////////////edit orders//////////////////////////////////////////////////////////////////////////////-->


<div id="myModaledit" class="modal">


  <!-- Modal content -->
  <div class="modal-content"> 
  <div class="modal-content2">	
    <span class="close" id="closeedit">&times;</span>
			<section id="titulo">
        			<center></br><h2>Ingrese los nuevos datos de la orden</h2>
					     <h5>Asegurese de que los datos introducidos sean correctos</h5>
				</center>
			</section>
			</br>
	<div class="contenedor">
			
	<div class="myDiv" >
		
		<div class="txt">
			Número de orden<br>
			<input type="number"  id="txtnumeroorden" disabled required="required"  class="TT" />	
		</div>
		<div class="txt">
			Pedido SAE<br>
			<input type="number"  id="txtpedidosae2" disabled required="required"  class="TT" />	
		</div>
		<div class="txt">
			Fecha de emisión de orden<br>
			<input type="date"  id="txtfechaemision2" disabled required="required"  class="TT" />	
		</div>
		<div class="txt">
			Fecha requerida<br>
			<input type="date"  id="txtfecharequerida2" disabled required="required" class="TT" />	
		</div>
		<div class="txt">
			Fecha Confirmada<br>
			<input type="date"  id="txtfechaconfirmada2"  required="required" class="TT" />	
		</div>
		<div class="txt">
			Cantidad a producir<br>
			 <input type="number" placeholder="Cantidad a producir" disabled id="txtcantidad2"  required="required" class="TT" />	
		</div>
		<div class="txt">
			Producto<br>
			<input type="text" placeholder="Producto" disabled id="txtproducto2"  required="required" class="TT" />
					
		</div>
		
		<div class="txt">
			Especificaciones<br>
			<textarea rows="10" cols="30" name="dir" id="txtdescripcion2" disabled placeholder="Descripción del producto"></textarea> 	
		</div>
		<div class="txt">
			Estado<br>
			<select id="optionstatus"></select>
		</div>

	</div>
			
	<br\>
	

	

	
	<center>
	<div class="txt">
	<input type="button" name="save"  Value="Guardar" onClick="saverecord();" id="ok"/>
	</div>
	</center>
	</div>   
  </div>
  </div>
  
</div>






<!--/////////////////////////////////////////////////////detail orders//////////////////////////////////////////////////////////////////////////////-->


<div id="myModaldetail" class="modal">


  <!-- Modal content -->
  <div class="modal-content"> 
  <div class="modal-content2">	
    <span class="close" id="closedetail">&times;</span>
			
			<section id="titulo">
        			<center></br><h2>Detalle de la orden</h2>
					<h2 id='numor'></h2>
					     
				</center>
				 <table class="tbl-qa" id="encabezadosorden">
		  			<thead>
		 			<tr>
						<th class="table-header" id="noOrden">No. Orden: 45</th>
						<th class="table-header" id="pedidosae">Pedido SAE: 45</th>
						<th class="table-header" id="codigosae">Clave SAE: 45</th>
						<th class="table-header" id="fechaemision">Fecha Emisión: 20/02/2018</th>
						<th class="table-header" id="fecharequerida">Fecha Requerida: 05/03/2018</th>
						<th class="table-header" id="status">Estado de la Orden: Proceso</th>
						<th class="table-header" id="cantidad">Cantidad: 50000 piezas</th>
					</tr>
					<tr>
						<th class="table-header" colspan="7" id="producto">Producto</th>
					</tr>
					<tr>
						<th class="table-header" colspan="7" id="descripcion">Descripción: </th>
              				</tr>
 		 			</thead>
		  		
             			</table>
			</section>
			</br>
		<div class="contenedor">
			<hr>
			<section id="titulo">
        			<center></br><h2>Detalle de Procesos por Nombre de Máquina</h2>
					     
				</center>
			</section>
			</br>



		<div class="scro3" >
			<center id='scro3'>
			</center>
 		<!--div id="tabla">
    			 <table class="tbl-qa" id="procesosorden">
		  		<thead>
		 			<tr>
						<th class="table-header" >Proceso Máquina</th>
						no<th class="table-header" >Máquina</th>
						<th class="table-header" >Folio</th>
						<th class="table-header" >Cantidad a producir</th>
						<th class="table-header" >Merma otorgada</th>
						<th class="table-header" >Tiempo de Ajuste(min)</th>
						<th class="table-header" >Unidad de entrada</th>
						<th class="table-header" >Estandar de Turno</th>
						<th class="table-header" >Fecha Requerida</th>
						<th class="table-header" >Hora Requerida</th>
						<th class="table-header" >Fecha y Hora de Termino estimada</th>
						no<th class="table-header" >Hora de Termino estimada</th>
						<th class="table-header" >Cantidad final calculada</th>
						<th class="table-header" >Cantidad Producida</th>
						<th class="table-header" >Merma</th>
						<th class="table-header" >Fecha y Hora inicio</th>
						no<th class="table-header" >Hora de inicio</th>
						<th class="table-header" >Fecha y Hora de termino</th>
						no<th class="table-header" >Hora de termino</th>
						<th class="table-header" >Operador</th>
						<th class="table-header" >Editar</th>
						<th class="table-header" >Imprimir</th>
              				</tr>
 		 		</thead>
		  		<tbody >				
             			 </tbody>
             		</table>
		 </div-->
		</div>  
	
		<!-- hr>
			<section id="titulo">
        			<center></br><h3>Detalle de Componentes</h3>
					     
				</center>
			</section>
			</br> 

		<div class="scro2">
 		<div id="tabla">
    			 <table class="tbl-qa" id="componentesorden">
		  		<thead>
		 			<tr>
						<th class="table-header" >Código SAE</th>
						<th class="table-header" >Componente</th>
						<th class="table-header" >Linea</th>
						<th class="table-header" >Unidad de Medida</th>
						<th class="table-header" >Cantidad unitaria</th>
						<th class="table-header" >Costo unitario</th>
						<th class="table-header" >Cantidad por orden</th>
						<th class="table-header" >Costo Total</th>
						<th class="table-header" >Fecha de Compra</th>
						<th class="table-header" >Editar</th>
              				</tr>
 		 		</thead>
		  		<tbody >				
             			 </tbody>
             		</table>
		 </div>
		</div> --> 
	</div>
  </div>
  </div>
  
</div>










<!--/////////////////////////////////////////////////////Incident Format//////////////////////////////////////////////////////////////////////////////-->
<div id="myModalsaveincidente" class="modal">

  <!-- Modal content -->
  <div class="modal-content"> 
  <div class="modal-content2">	
	<span class="close" id="closeincident">&times;</span>
			<section id="titulo">
        			<center></br><h2>Incidentes Asociados a la Orden </h2><h2 id='ordenid'></h2>
				
				</center>
			</section>
	</br>
	<center>
		<div class="txt">
			<input type="button" name="insertar" Value="Agregar" onClick="formincident();" id="ok"/>
		</div>
		<div class="myDiv" id="incidente" style="display:none;">
			<section id="titulo">
        			<center><h2>Reporte de Incidente</h2>
					     </br><h3 id="filesincident"></h3>
				</center>
			</section>
				<div class="txt">
					Área que Reporta<br>
					<select  class="TT" id="optionareadenuncia" >
						<option value="">---Seleccione---</option>		
              				</select>
				</div>
				<div class="txt">
					Área Reportada<br>
					<select  class="TT" id="optionareadenunciada">
						<option value="">---Seleccione---</option>		
              				</select>
				</div>
			<hr>
				<center><h2>Responsable de la no Conformidad</h2></center>
			<div class="myDiv">
				<div class="txt">
					Nombre(s)<br>
					<input type="text" id="txtdenunciado" placeholder="Nombre(s)"  required="required" class="TT" />
				</div>
				<div class="txt">
					Apellidos<br>
					<input type="text" id="txtapellidosdenunciado" placeholder="Apellidos"  required="required" class="TT" />
				</div>
			</div>
				<hr>
				<div class="txt">
					Incidente (Para ser llenado por el denunciante)<br>
					<textarea rows="7" cols="30" id="txtdescripcionincidente" placeholder="Incidente"  required="required" ></textarea>
				</div>
				<hr>
				<div class="txt">
					Acción de contingencia inmediata (Para ser llenado por el denunciante)<br>
					<textarea rows="7" cols="30"  id="txtcontingencia" placeholder="Acción de contingencia"  required="required"  ></textarea>
				</div>
				<hr>
				<div class="txt">
					Descripción de la causa raíz (Para ser llenado por el denunciado)<br>
					<textarea rows="7" cols="30"  id="txtcausa" placeholder="Causa raíz"  required="required"  ></textarea>
				</div>
				<hr>
				<div class="txt">
					Accion(es) para erradicar el incidente de forma permanente (Para ser llenado por el denunciado)<br>
					<textarea rows="7" cols="30"  id="txterradicar" placeholder="Accion(es) para erradicar el incidente"  required="required" ></textarea>
				</div>
				<div class="txt">
					Fecha conclusión<br>
					<input type="date" id="txtfechaconclusion" placeholder="Fecha"  required="required" class="TT" />
				</div>
				<hr>
				<center><h2>Reporta</h2></center>
			<div class="myDiv">
				<div class="txt">
					Nombre(s)<br>
					<input type="text" id="txtdenunciante" placeholder="Nombre(s)"  required="required" class="TT" />
				</div>
				<div class="txt">
					Apellidos<br>
					<input type="text" id="txtapellidosdenunciante" placeholder="Apellidos"  required="required" class="TT" />
				</div>
			</div>
				<div class="txt">
					<input type="button" name="insertar" Value="Guardar" onClick="saveincident();" id="ok"/>
				</div>
				<div class="txt">
					<input type="button" name="insertar" Value="Cancelar" onClick="canceladdincident()" id="ok"/>
				</div>

			</div>
		

			<div class="myDiv" id="updateincidente" style="display:none;">
			<section id="titulo">
        			<center><h2>Reporte de Incidente</h2>
					     </br><h3 id="filesincident"></h3>
				</center>
			</section>
				<div class="txt">
					Fecha y hora del incidente<br>
					<input type="text" id="txtfecharegistro2" placeholder="Fecha"  required="required" class="TT" disabled/>
				</div>
				<div class="txt">
					Área que reporta<br>
					<select  class="TT" id="optionareadenuncia2">
						<!--option value="">---Seleccione---</option-->		
              				</select>
				</div>
				<div class="txt">
					Área reportada<br>
					<select  class="TT" id="optionareadenunciada2">
						<!--option value="">---Seleccione---</option-->		
              				</select>
				</div>
				<hr>
				<center><h2>Responsable de la no Conformidad</h2></center>
			<div class="myDiv">
				<div class="txt">
					Nombre(s)<br>
					<input type="text" id="txtdenunciado2" placeholder="Nombre(s) del denunciado"  required="required" class="TT" />
				</div>
				<div class="txt">
					Apellidos<br>
					<input type="text" id="txtapellidosdenunciado2" placeholder="Apellidos del denunciado"  required="required" class="TT" />
				</div>
			</div>
				<hr>
				<div class="txt">
					Incidente (Para ser llenado por el denunciante)<br>
					<textarea rows="7" cols="30"  id="txtdescripcionincidente2" placeholder="Incidente"  required="required" ></textarea>
				</div>
				<hr>
				<div class="txt">
					Acción de contingencia inmediata (Para ser llenado por el denunciante)<br>
					<textarea rows="7" cols="30"  id="txtcontingencia2" placeholder="Incidente"  required="required" ></textarea>
				</div>
				<hr>
				<div class="txt">
					Descripción de la causa raíz (Para ser llenado por el denunciado)<br>
					<textarea rows="7" cols="30"  id="txtcausa2" placeholder="Causa raíz"  required="required" ></textarea>
				</div>
				<hr>
				<div class="txt">
					Accion(es) para erradicar el incidente de forma permanente (Para ser llenado por el denunciado)<br>
					<textarea rows="7" cols="30"  id="txterradicar2" placeholder="Causa raíz"  required="required" ></textarea>
				</div>
				<div class="txt">
					Fecha conclusión<br>
					<input type="date" id="txtfechaconclusion2" placeholder="Fecha"  required="required" class="TT" />
				</div>
				<hr>
			<center><h2>Reporta</h2></center>
			<div class="myDiv">
				<div class="txt">
					Nombre(s)<br>
					<input type="text" id="txtdenunciante2" placeholder="Nombre(s) del denunciante"  required="required" class="TT" />
				</div>
				<div class="txt">
					Apellidos<br>
					<input type="text" id="txtapellidosdenunciante2" placeholder="Apellidos del denunciante"  required="required" class="TT" />
				</div>
			</div>
				<div class="txt">
					<input type="button" name="insertar" Value="Actualizar" onClick="updateincident();" id="ok"/>
				</div>
				<div class="txt">
					<input type="button" name="insertar" Value="Cancelar" onClick="cancelupdateincident();" id="ok"/>
				</div>

			</div>
		<div>
			<img src="../img/load.gif" class="imgload" id="imgloadincidents">
		</div>
	<div id="scro" name="incidentcontenedor">

 		<div id="tablaincident">
   			  <table class="tbl-qa" id="resultadoincidents">
		 		 <thead>
					 <tr>
						<th class="table-header" >ID</th>
						<th class="table-header" >Fecha y Hora</th>
						<th class="table-header" >Área que Denuncia</th>
						<th class="table-header" >Área Denunciada</th>
						<th class="table-header" >Nombre del Denunciado</th>
						<th class="table-header" >Imprimir</th>
						<th class="table-header" >Editar</th>
						<th class="table-header" >Eliminar</th>
						
              				</tr>
 		  		</thead>
		  		<tbody >				
             	 		</tbody>
              		</table>
 		</div>
		</br></br>
		<div id="sinincidents" style="display:none;">
			<div class='myDiv'><div class='txt'>NO HAY INCIDENTES REGISTRADOS EN EL SISTEMA </div></div>
		</div>
 	</div>
	</center>	



  </div>
  </div>
  
</div>
<!--/////////////////////////////////////////////////////////////consult components////////////////////////////////////////////////////////////////////////-->






</main>




</body>
</html>
