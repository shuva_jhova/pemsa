<?php

//hola jvl hhh
require_once("dbControl.php");
require_once("usersFunction.php");

class appControl
{ 
	var $app;
	var $userfunctions;
	//var $path="C:/AppServ/www/PlanCtrol/documentos/";
	//var $path2="C:/AppServ/www/PlanCtrol";
	var $path="../documentos/";
	var $path2="";
	var $frame=1;
	var $mermageneral=300;
	public function __construct()
		{
			$this->app = new dbControl();
			$this->userfunctions = new usersFunction();
		}
	//db backup//

	
	//general functions of system//

	public function getpaises(){
		if(!$this->userfunctions->islogged()){
			//$this->userfunctions->logout();
		}
		$datos=array();
		$result=$this->app->getrecord("paises","*","where activo like '1'");
		for($a=0;$a<sizeof($result);$a++)
    		{
			$vec=$result[$a];	

			$datos[] = array(
					'id'=>$vec['id'],
					'codigo'=>$vec['iso'],
					'pais'=>$vec['nombre']				
					);			
                }
		
		
	return $datos; 
	}
	public function respaldo(){
	if(!$this->userfunctions->islogged()){
			
		}
		$datos=array();
		if($this->app->backuptables("hola")=="si"){
			$datos[] = array(
					'exito'=>"si"
					
					);
		}else{
				$datos[] = array(
					'exito'=>"no"
					
					);

		}
	return $datos;
	}
	public function getcategorias(){
		if(!$this->userfunctions->islogged()){
			
		}
		$datos=array();
		$result=$this->app->getrecord("categorias","*","where activo like '1'");
		for($a=0;$a<sizeof($result);$a++)
    		{
			$vec=$result[$a];	

			$datos[] = array(
					'id'=>$vec['id'],
					'nombre'=>$vec['nombre']				
					);			
                }
		
	return $datos;
	}
	
	public function getsubensambles(){
		if(!$this->userfunctions->islogged()){
			
		}

		$datos=array();
		$result=$this->app->getrecord("subensambles","*","where activo like '1'");
		for($a=0;$a<sizeof($result);$a++)
    		{
			$vec=$result[$a];	

			$datos[] = array(
					'id'=>$vec['id'],
					'nombre'=>$vec['nombre']				
					);			
                }	
	return $datos;
	}
	/*public function getbobinas(){
		if(!$this->userfunctions->islogged()){
			
		}

		$datos=array();
		$result=$this->app->getrecord("bobinas","*","where activo like '1'");
		for($a=0;$a<sizeof($result);$a++)
    		{
			$vec=$result[$a];	

			$datos[] = array(
					'id'=>$vec['id'],
					'sae'=>$vec['codigoSAE'],
					'nombre'=>$vec['nombre']				
					);			
                }	
	return $datos;
	}*/
	public function gethorasturno(){
		if(!$this->userfunctions->islogged()){
			
		}

		$datos=array();
		$result=$this->app->getrecord("turno","*","where activo like '1'");
		for($a=0;$a<sizeof($result);$a++)
    		{
			$vec=$result[$a];	

			$datos[] = array(
					'id'=>$vec['id'],
					'nombre'=>$vec['nombre'],	
					'horas'=>$vec['horas']		
					);			
                }
		
		
	return $datos;
	}
	public function getpresentacionventa(){
		if(!$this->userfunctions->islogged()){
			
		}

		$datos=array();
		$result=$this->app->getrecord("presentacionventa","*","where activo like '1'");
		for($a=0;$a<sizeof($result);$a++)
    		{
			$vec=$result[$a];	

			$datos[] = array(
					'id'=>$vec['id'],
					'presentacion'=>$vec['presentacion']
					);			
                }
		
		
	return $datos; 
	}
	
	public function deletepaths ($iduser,$codigo){
		if(!$this->userfunctions->islogged()){
			
		}

		$datos=array();

		$result=$this->app->getrecord("pathsuser".$iduser,"*","where claveSAE like '$codigo'");
		if(sizeof($result)>0)
    		{	
			for($z=0;$z<sizeof($result);$z++){
				$vec=$result[$z];
				$ruta=$vec['ruta'];
				$file =$this->path2.$vec['ruta'];
				unlink($file);
			}
		}

		if($this->app->deletetable("pathsuser".$iduser))
		{
			$datos[]=array(
				'exito'=>'si'
				);			
		}
	return $datos;		
	}
	
///////////////////////////////////////////////////////////////////////////////functions for operators////////////////////////////////////////////////////////////////////

public function saveoperador($nombre,$appat,$apmat){
	if(!$this->userfunctions->islogged()){
			
		}
		$datos=array();
	
			$id=$this->app->saverecord('persona',"'$nombre','$appat','$apmat','1'","(nombre,primerApellido,segundoApellido,activo)");			
			$id2=$this->app->saverecord('operadores',"$id,'1'","(idPersona,activo)");
						
			if( $id2>0){
				$datos[]=array(
					'exito'=>'si',
					'folio'=>$id2
				);	
			}else{
				$datos[]=array(
					'exito'=>'no',
					'folio'=>0
				);	
			}			
		


		
	return $datos;

}
	public function getoperadores(){
		if(!$this->userfunctions->islogged()){
			
		}

		$datos=array();
			
			$result3=$this->app->getrecord("operadores","operadores.id,persona.primerApellido,persona.segundoApellido,persona.nombre,operadores.activo","inner join persona on persona.id=operadores.idPersona");
			for($aa=0;$aa<sizeof($result3);$aa++)
    			{	$vec3=$result3[$aa];
				$datos[]=array(
					'id'=>$vec3['id'],
					'nombre'=>$vec3['nombre'],
					'appat'=>$vec3['primerApellido'],
					'apmat'=>$vec3['segundoApellido'],
					'activo'=>$vec3['activo']
					);
						
			}	

		
	return $datos;

	}
	public function updateoperador($id,$nombre,$appat,$apmat,$activo){
//trigger_error($vendedor);
		$datos=array();
		$result=$this->app->getrecord("operadores","operadores.id,operadores.idPersona"," where operadores.id=$id");
		
		if (sizeof($result) > 0) {
			for($a=0;$a<sizeof($result);$a++)
    			{
				$vec=$result[$a];
			
				$idoperador=$vec['id'];
				$idpersona=$vec['idPersona'];
								
			
				if($this->app->updaterecord("operadores","activo='$activo'"," where operadores.id=$id")){
					if($this->app->updaterecord("persona","nombre='$nombre',primerApellido='$appat',segundoApellido='$apmat'"," where persona.id=$idpersona")){
						$datos[]=array(
							'exito'=>'si',
							'folio'=>$idoperador
						);
							
					}		
				}		
				
                	}	
		} 
		else{
			
			$datos[]=array(
					'exito'=>'sin',
					'folio'=>0
				);
		}


	return $datos;	
	}

	public function deleteoperador($id){
//trigger_error($vendedor);
		$datos=array();
		$result=$this->app->getrecord("operadores","*"," where operadores.id=$id");
		
		if (sizeof($result) > 0) {
			for($a=0;$a<sizeof($result);$a++)
    			{
				$vec=$result[$a];
			
				$idoperador=$vec['id'];
				$idpersona=$vec['idPersona'];
					
				if($this->app->deleterecord("operadores","where id=$id")>0){
					if($this->app->deleterecord("persona"," where persona.id=$idpersona")>0){
						$datos[]=array(
							'exito'=>'si'
						);
							
					}		
				}		
				
                	}	
		} 
		else{
			
			$datos[]=array(
					'exito'=>'sin'

				);
		}


	return $datos;	
	}
///////////////////////////////////////////////////////////////////////////////functions of products////////////////////////////////////////////////////////////////////////////////////////
	
	public function validateSAE($codigo){
		if(!$this->userfunctions->islogged()){
			
		}

		$datos=array();
		/*if(strlen($codigo)==8){*/
			$result=$this->app->getrecord("productos","*","where codigoProducto like '$codigo'");
			if(sizeof($result)>0)
    			{	
				$vec=$result[0];
				$datos[] = array(
					'id'=>$vec['id'],
					'valido'=>"Existe"
					);			
                	}else{
				$datos[] = array(
					'id'=>0,
					'valido'=>"Si"
					);			
			}
		/*}else if (strlen($codigo)==10){
				
			$cod=explode("-",$codigo);
			if($cod[1]=='A' || $cod[1]=='a'){
				$result=$this->app->getrecord("productos","*","where codigoProducto like '$codigo'");
				if(sizeof($result)>0)
    				{	
					$vec=$result[0];
					$datos[] = array(
						'id'=>$vec['id'],
						'valido'=>"Existe"
						);			
                		}else{
					$datos[] = array(
						'id'=>0,
						'valido'=>"Si"
						);			
				}
	

			}else if( $cod[1]=='B' || $cod[1]=='b'){
				$codigo2=$cod[0]."-A";
				$result=$this->app->getrecord("productos","*","where codigoProducto like '$codigo2'");
				if(sizeof($result)>0)
    				{	
					$result=$this->app->getrecord("productos","*","where codigoProducto like '$codigo'");
					if(sizeof($result)>0)
    					{	
						$vec=$result[0];
						$datos[] = array(
							'id'=>$vec['id'],
							'valido'=>"Existe"
							);			
                			}else{
						$datos[] = array(
							'id'=>0,
							'valido'=>"Si"
						);			
					}

			
				}else{
						$datos[] = array(
							'id'=>0,
							'valido'=>"Sin A"
						);			
				}




			}
		}*/
		
		
	return $datos; 
		
	}


	public function saveproducto ($iduser,$codigo,$descripcion,$nombre,$presentacion,$marca,$codigobarras,$codigobarrasempaque,$pais,$fraccionarancelaria,$exportacion,$barniz,$foto,$fotodetalle,$notasespeciales,$corrugado,$corrugadospallet,$piezascorrugado,$camaspallete,$anchocorrugado,$piezaspallet,$largocorrugado,$pesopresentacion,$altrocorrugado,$pesocorrugado,$anchopallet,$pesopallete,$largopallet,$anchoproducto,$alturapallet,$largoproducto,$corrugadosporcama,$altoproducto,$esp1,$esp2,$esp3,$esp4,$esp5,$corrugadoinner,$piezascorrugadoinner,$largocorrugadoinner,$anchocorrugadoinner,$altrocorrugadoinner,$pesocorrugadoinner,$pesoneto,$a,$b,$h){
		if(!$this->userfunctions->islogged()){
			
		}

		$datos=array();
		$doc1=$esp1;$doc2=$esp2;$doc3=$esp3;$doc4=$esp4;$doc5=$esp5;$doc6='';$doc7='';$doc8='';
		$result=$this->app->getrecord("productos","*","where codigoProducto like '$codigo'");
				
		if (sizeof($result) > 0) {
			$datos[]=array(
				'exito'=>'ya',
				'folio'=>0
			);
		} 
		else 
		{
			
			$result2=$this->app->getrecord("presentacionventa","*","where presentacion like '$presentacion'");
			//trigger_error(sizeof($result2));
			if(sizeof($result2)<=0){
				$presentacion=ucfirst ($presentacion);
				$this->app->saverecord("presentacionventa","null,'$presentacion','1'");
			}			

			if(sizeof($codigo)>8){
				$cod=explode("-",$codigo);
				$codigo=$cod[0].'-'.strtoupper($cod[1]);
			}
			
			
			$id=$this->app->saverecord('productos',"null,'$nombre','$codigo','$descripcion','$presentacion','$marca','$codigobarras','$codigobarrasempaque','$foto','$fotodetalle','$doc1','$doc2','$doc3','$doc4','$doc5','$doc6','$doc7','$doc8','','','','','','','','','','','','','','','','','','','$pais','$fraccionarancelaria','$exportacion','$notasespeciales','$barniz','1'");			
			if($id>0 ){
				$this->app->saverecord("innerpack","null,$id,'$corrugadoinner',$piezascorrugadoinner,$largocorrugadoinner,$anchocorrugadoinner,$altrocorrugadoinner,$pesocorrugadoinner,'1'");
				$this->app->saverecord("masterpack","null,$id,'$corrugado',$piezascorrugado,$largocorrugado,$anchocorrugado,$altrocorrugado,$pesocorrugado,'1'");
				$this->app->saverecord("medidas_producto","null,$id,$largoproducto,$anchoproducto,$altoproducto,$pesopresentacion,'1'");
				$this->app->saverecord("pallet","null,$id,$largopallet,$anchopallet,$alturapallet,$corrugadosporcama,$camaspallete,$corrugadospallet,$piezaspallet,$pesopallete,$pesoneto,'1'");
				$this->app->saverecord("medidas_productocertificadocalidad","$id,$b,$a,$h,'1'","(idProducto,largo,ancho,alto,activo)");		
				$result=$this->app->getrecord("pathsuser".$iduser,"*","where claveSAE like '$codigo'");
				if(sizeof($result)>0)
    				{	
					for($z=0;$z<sizeof($result);$z++){
						$vec=$result[$z];
						$clavesae=$vec['claveSAE'];
						$ruta=$vec['ruta'];
						$descripcionfile=$vec['descripcion'];

						$this->app->saverecord("rel_productoarchivo","null,$id,'$clavesae','$ruta','$descripcionfile','','1'");
					}
				}
				$this->app->deletetable("pathsuser".$iduser);

				$datos[]=array(
					'exito'=>'si',
					'folio'=>$id
				);	
			}else{
				$datos[]=array(
					'exito'=>'no',
					'folio'=>0
				);	
			}			
		}


		
	return $datos;
	}
	public function getfilesproducto($idproducto,$sae){
		if(!$this->userfunctions->islogged()){
			
		}

		$datos=array();
		
			$result3=$this->app->getrecord("rel_productoarchivo","*"," where idProducto= $idproducto and claveSAE like '$sae'");
			for($aa=0;$aa<sizeof($result3);$aa++)
    			{	$vec3=$result3[$aa];
				$datos[]=array(
					'id'=>$vec3['id'],
					'descripcion'=>$vec3['descripcion'],
					'ruta'=>$vec3['ruta']
					);
						
			}	

	return $datos;
	}
	public function getallproductsaux(){
		if(!$this->userfunctions->islogged()){
			
		}

		$datos=array();
		
			$result3=$this->app->getrecord("productosauxiliar","*"," where activo like '1'");
			for($aa=0;$aa<sizeof($result3);$aa++)
    			{	$vec3=$result3[$aa];
				$datos[]=array(
					'id'=>$vec3['id'],
					'sae'=>$vec3['clave'],
					'nombre'=>$vec3['nombre'],
					'unidadenrada'=>$vec3['unidadEntrada'],
					'linea'=>$vec3['linea']
					);
						
			}	

	return $datos;
	}

	public function getspecificproductsaux($codigo){
		if(!$this->userfunctions->islogged()){
			
		}

		$datos=array();
		
			$result3=$this->app->getrecord("productosauxiliar","*"," where clave like '$codigo' and activo like '1'");
			for($aa=0;$aa<sizeof($result3);$aa++)
    			{	$vec3=$result3[$aa];
				$datos[]=array(
					'id'=>$vec3['id'],
					'sae'=>$vec3['clave'],
					'nombre'=>$vec3['nombre'],
					'unidadenrada'=>$vec3['unidadEntrada'],
					'linea'=>$vec3['linea']
					);
						
			}	

	return $datos;
	}
	public function getallproducts(){
		if(!$this->userfunctions->islogged()){
			
		}

		$datos=array();
		$color;
		$result=$this->app->getrecord("productos","*"," ORDER BY productos.id ASC");
		for($a=0;$a<sizeof($result);$a++)
    		{
			$vec=$result[$a];
			$id=$vec['id'];
			$result1=$this->app->getrecord("productos","*"," where (length(documento1)<=0 or length(documento2)<=0 or length(documento3)<=0 or length(documento4)<=0 or length(documento5)<=0) and id=$id");
			$result2=$this->app->getrecord("procesos","*"," where (length(especificacion1)<=0 or  length(especificacion2)<=0 or length(especificacion3)<=0) and idProducto=$id");
			$result3=$this->app->getrecord("especificacionespt","*","where idProducto=$id HAVING COUNT(*)<3");
			//trigger_error(print_r($result1,true));

			if(sizeof($result1)>0 and sizeof($result2)>=0 and sizeof($result3)>0 ){
				$color='faltan';
			}else if(sizeof($result1)>0){
				$color='faltaingenieria';
			}else if(sizeof($result2)>0 or sizeof($result3)>0 ){
				$color='faltacalidad';
			}else{
				$color='todasbn';
			}
			$idprod=$vec['id'];

			$result2=$this->app->getrecord("innerpack","*"," where idProducto=$idprod");
			$vecinner=$result2[0];

			$result3=$this->app->getrecord("masterpack","*"," where idProducto=$idprod");
			$vecmaster=$result3[0];

			$result4=$this->app->getrecord("medidas_producto","*"," where idProducto=$idprod");
			$vecmedidas=$result4[0];

			$result6=$this->app->getrecord("medidas_productocertificadocalidad","*"," where idProducto=$idprod");
			$vecmedidascalidad=$result6[0];

			$result5=$this->app->getrecord("pallet","*"," where idProducto=$idprod");
			$vecpallet=$result5[0];

			

			$datos[] = array(
				'id'=>$vec['id'],
				'nombre'=>$vec['nombreProducto'],
				'codigo'=>$vec['codigoProducto'],	
				'descripcion'=>$vec['descripcion'],
				'presentacion'=>$vec['presentacionVenta'],
				'marca'=>$vec['marcaProducto'],
				'codigobarrasproducto'=>$vec['codigoBarrasProducto'],
				'codigobarrasempaque'=>$vec['codigoBarrasEmpaque'],
				'foto'=>$vec['fotos'],
				'fotodetalle'=>$vec['fotosDetalle'],
				'doc1'=>$vec['documento1'],
				'doc2'=>$vec['documento2'],
				'doc3'=>$vec['documento3'],
				'doc4'=>$vec['documento4'],
				'doc5'=>$vec['documento5'],
				'doc6'=>$vec['documento6'],
				'doc7'=>$vec['documento7'],
				'doc8'=>$vec['documento8'],
				'corrugado'=>$vec['corrugado'],
				'piezascorrugado'=>$vec['piezasCorrugado'],
				'anchocorrugado'=>$vec['anchoCorrugado'],	
				'largocorrugado'=>$vec['largoCorrugado'],
				'altocorrugado'=>$vec['altoCorrugado'],
				'anchopallet'=>$vec['anchoPallet'],
				'largopallet'=>$vec['largoPallet'],
				'alturapallet'=>$vec['alturaPallet'],
				'corrugadosporcama'=>$vec['corrugadosPorCama'],	
				'corrugadosporpallet'=>$vec['corrugadosPorPallet'],
				'camasporpallet'=>$vec['camasPorPallet'],
				'piezasPorPallet'=>$vec['piezasPorPallet'],
				'pesopresentacion'=>$vec['pesoPresentacionVenta'],
				'pesoporcorrugado'=>$vec['pesoPorCorrugado'],
				'pesoporpallet'=>$vec['pesoPorPallet'],
				'anchoproducto'=>$vec['anchoProducto'],
				'largoproducto'=>$vec['largoProducto'],
				'altoproducto'=>$vec['altoProducto'],
				'pais'=>$vec['paisOrigen'],
				'fraccionarancelaria'=>$vec['fraccionArancelaria'],
				'exportacion'=>$vec['exportacion'],
				'notas'=>$vec['notasEspeciales'],
				'barniz'=>$vec['barniz'],
				

				'a'=>$vecmedidascalidad['ancho'],
				'b'=>$vecmedidascalidad['largo'],
				'h'=>$vecmedidascalidad['alto'],

				'empaqueinner'=>$vecinner['empaque'],
				'multiploinner'=>$vecinner['multiplo'],
				'largoinner'=>$vecinner['largo'],
				'anchoinner'=>$vecinner['ancho'],
				'altoinner'=>$vecinner['alto'],
				'pesoinner'=>$vecinner['peso'],
				

				'empaquemaster'=>$vecmaster['empaque'],
				'multiplomaster'=>$vecmaster['multiplo'],
				'largomaster'=>$vecmaster['largo'],
				'anchomaster'=>$vecmaster['ancho'],
				'altomaster'=>$vecmaster['alto'],
				'pesomaster'=>$vecmaster['peso'],
				

				'largoprod'=>$vecmedidas['largo'],
				'anchoprod'=>$vecmedidas['ancho'],
				'altoprod'=>$vecmedidas['alto'],
				'pesoprod'=>$vecmedidas['peso'],


				'largopallet'=>$vecpallet['largo'],
				'anchopallet'=>$vecpallet['ancho'],
				'altopallet'=>$vecpallet['alto'],
				'corrugadoscamapallete'=>$vecpallet['corrugadosCama'],
				'camaspallet'=>$vecpallet['camas'],
				'corrugadospallet'=>$vecpallet['corrugadosPallet'],
				'piezaspallet'=>$vecpallet['totalPiezas'],
				'pesobrutopallet'=>$vecpallet['pesoBruto'],
				'pesonetopallet'=>$vecpallet['pesoNeto'],

				'color'=>$color,

				'activo'=>$vec['activo'],
				'ordenes'=>$this->getordersenablesbyproduct($vec['id'])

				);
		
                }
			
	
	return $datos;
	}

	public function getallproductsenable(){
		if(!$this->userfunctions->islogged()){
			
		}
		$color;
		$datos=array();
		$result=$this->app->getrecord("productos","*"," where activo like '1' ORDER BY productos.codigoProducto ASC");
		for($a=0;$a<sizeof($result);$a++)
    		{
			$vec=$result[$a];
			$id=$vec['id'];
			$result1=$this->app->getrecord("productos","*"," where (length(documento1)<=0 or length(documento2)<=0 or length(documento3)<=0 or length(documento4)<=0 or length(documento5)<=0) and id=$id");
			$result2=$this->app->getrecord("procesos","*"," where (length(especificacion1)<=0 or  length(especificacion2)<=0 or length(especificacion3)<=0) and idProducto=$id");
			$result3=$this->app->getrecord("especificacionespt","*","where idProducto=$id HAVING COUNT(*)<3");
			//trigger_error(print_r($result1,true));
			if(sizeof($result1)>0 and sizeof($result2)>=0 and sizeof($result3)>0 ){
				$color='faltan';
			}else if(sizeof($result1)>0){
				$color='faltaingenieria';
			}else if(sizeof($result2)>0 or sizeof($result3)>0 ){
				$color='faltacalidad';
			}else{
				$color='todasbn';
			}
			$idprod=$vec['id'];

			$result2=$this->app->getrecord("innerpack","*"," where idProducto=$idprod");
			$vecinner=$result2[0];

			$result3=$this->app->getrecord("masterpack","*"," where idProducto=$idprod");
			$vecmaster=$result3[0];

			$result4=$this->app->getrecord("medidas_producto","*"," where idProducto=$idprod");
			$vecmedidas=$result4[0];

			$result5=$this->app->getrecord("pallet","*"," where idProducto=$idprod");
			$vecpallet=$result5[0];

			$result6=$this->app->getrecord("medidas_productocertificadocalidad","*"," where idProducto=$idprod");
			$vecmedidascalidad=$result6[0];

			$datos[] = array(
				'id'=>$vec['id'],
				'nombre'=>$vec['nombreProducto'],
				'codigo'=>$vec['codigoProducto'],	
				'descripcion'=>$vec['descripcion'],
				'presentacion'=>$vec['presentacionVenta'],
				'marca'=>$vec['marcaProducto'],
				'codigobarrasproducto'=>$vec['codigoBarrasProducto'],
				'codigobarrasempaque'=>$vec['codigoBarrasEmpaque'],
				'foto'=>$vec['fotos'],
				'fotodetalle'=>$vec['fotosDetalle'],
				'doc1'=>$vec['documento1'],
				'doc2'=>$vec['documento2'],
				'doc3'=>$vec['documento3'],
				'doc4'=>$vec['documento4'],
				'doc5'=>$vec['documento5'],
				'doc6'=>$vec['documento6'],
				'doc7'=>$vec['documento7'],
				'doc8'=>$vec['documento8'],
				'corrugado'=>$vec['corrugado'],
				'piezascorrugado'=>$vec['piezasCorrugado'],
				'anchocorrugado'=>$vec['anchoCorrugado'],	
				'largocorrugado'=>$vec['largoCorrugado'],
				'altocorrugado'=>$vec['altoCorrugado'],
				'anchopallet'=>$vec['anchoPallet'],
				'largopallet'=>$vec['largoPallet'],
				'alturapallet'=>$vec['alturaPallet'],
				'corrugadosporcama'=>$vec['corrugadosPorCama'],	
				'corrugadosporpallet'=>$vec['corrugadosPorPallet'],
				'camasporpallet'=>$vec['camasPorPallet'],
				'piezasPorPallet'=>$vec['piezasPorPallet'],
				'pesopresentacion'=>$vec['pesoPresentacionVenta'],
				'pesoporcorrugado'=>$vec['pesoPorCorrugado'],
				'pesoporpallet'=>$vec['pesoPorPallet'],
				'anchoproducto'=>$vec['anchoProducto'],
				'largoproducto'=>$vec['largoProducto'],
				'altoproducto'=>$vec['altoProducto'],
				'pais'=>$vec['paisOrigen'],
				'fraccionarancelaria'=>$vec['fraccionArancelaria'],
				'exportacion'=>$vec['exportacion'],
				'notas'=>$vec['notasEspeciales'],
				'barniz'=>$vec['barniz'],
				

				'a'=>$vecmedidascalidad['ancho'],
				'b'=>$vecmedidascalidad['largo'],
				'h'=>$vecmedidascalidad['alto'],

				'empaqueinner'=>$vecinner['empaque'],
				'multiploinner'=>$vecinner['multiplo'],
				'largoinner'=>$vecinner['largo'],
				'anchoinner'=>$vecinner['ancho'],
				'altoinner'=>$vecinner['alto'],
				'pesoinner'=>$vecinner['peso'],
				

				'empaquemaster'=>$vecmaster['empaque'],
				'multiplomaster'=>$vecmaster['multiplo'],
				'largomaster'=>$vecmaster['largo'],
				'anchomaster'=>$vecmaster['ancho'],
				'altomaster'=>$vecmaster['alto'],
				'pesomaster'=>$vecmaster['peso'],
				

				'largoprod'=>$vecmedidas['largo'],
				'anchoprod'=>$vecmedidas['ancho'],
				'altoprod'=>$vecmedidas['alto'],
				'pesoprod'=>$vecmedidas['peso'],


				'largopallet'=>$vecpallet['largo'],
				'anchopallet'=>$vecpallet['ancho'],
				'altopallet'=>$vecpallet['alto'],
				'corrugadoscamapallete'=>$vecpallet['corrugadosCama'],
				'camaspallet'=>$vecpallet['camas'],
				'corrugadospallet'=>$vecpallet['corrugadosPallet'],
				'piezaspallet'=>$vecpallet['totalPiezas'],
				'pesobrutopallet'=>$vecpallet['pesoBruto'],
				'pesonetopallet'=>$vecpallet['pesoNeto'],

				'color'=>$color,

				'activo'=>$vec['activo'],
				'ordenes'=>$this->getordersenablesbyproduct($vec['id'])
				);
		
                }
			
	
	return $datos;
	}

	
	public function getspecificproduct($id){
		if(!$this->userfunctions->islogged()){
			
		}

		$datos=array();
		$result=$this->app->getrecord("productos","*"," where id=$id");
		for($a=0;$a<sizeof($result);$a++)
    		{
			$vec=$result[$a];
			$idprod=$vec['id'];

			$result2=$this->app->getrecord("innerpack","*"," where idProducto=$idprod");
			$vecinner=$result2[0];

			$result3=$this->app->getrecord("masterpack","*"," where idProducto=$idprod");
			$vecmaster=$result3[0];

			$result4=$this->app->getrecord("medidas_producto","*"," where idProducto=$idprod");
			$vecmedidas=$result4[0];

			$result5=$this->app->getrecord("pallet","*"," where idProducto=$idprod");
			$vecpallet=$result5[0];

			$result6=$this->app->getrecord("medidas_productocertificadocalidad","*"," where idProducto=$idprod");
			$vecmedidascalidad=$result6[0];

			$datos[] = array(
				'id'=>$vec['id'],
				'nombre'=>$vec['nombreProducto'],
				'codigo'=>$vec['codigoProducto'],	
				'descripcion'=>$vec['descripcion'],
				'presentacion'=>$vec['presentacionVenta'],
				'marca'=>$vec['marcaProducto'],
				'codigobarrasproducto'=>$vec['codigoBarrasProducto'],
				'codigobarrasempaque'=>$vec['codigoBarrasEmpaque'],
				'foto'=>$vec['fotos'],
				'fotodetalle'=>$vec['fotosDetalle'],
				'doc1'=>$vec['documento1'],
				'doc2'=>$vec['documento2'],
				'doc3'=>$vec['documento3'],
				'doc4'=>$vec['documento4'],
				'doc5'=>$vec['documento5'],
				'doc6'=>$vec['documento6'],
				'doc7'=>$vec['documento7'],
				'doc8'=>$vec['documento8'],
				'corrugado'=>$vec['corrugado'],
				'piezascorrugado'=>$vec['piezasCorrugado'],
				'anchocorrugado'=>$vec['anchoCorrugado'],	
				'largocorrugado'=>$vec['largoCorrugado'],
				'altocorrugado'=>$vec['altoCorrugado'],
				'anchopallet'=>$vec['anchoPallet'],
				'largopallet'=>$vec['largoPallet'],
				'alturapallet'=>$vec['alturaPallet'],
				'corrugadosporcama'=>$vec['corrugadosPorCama'],	
				'corrugadosporpallet'=>$vec['corrugadosPorPallet'],
				'camasporpallet'=>$vec['camasPorPallet'],
				'piezasPorPallet'=>$vec['piezasPorPallet'],
				'pesopresentacion'=>$vec['pesoPresentacionVenta'],
				'pesoporcorrugado'=>$vec['pesoPorCorrugado'],
				'pesoporpallet'=>$vec['pesoPorPallet'],
				'anchoproducto'=>$vec['anchoProducto'],
				'largoproducto'=>$vec['largoProducto'],
				'altoproducto'=>$vec['altoProducto'],
				'pais'=>$vec['paisOrigen'],
				'fraccionarancelaria'=>$vec['fraccionArancelaria'],
				'exportacion'=>$vec['exportacion'],
				'notas'=>$vec['notasEspeciales'],
				'barniz'=>$vec['barniz'],


				'a'=>$vecmedidascalidad['ancho'],
				'b'=>$vecmedidascalidad['largo'],
				'h'=>$vecmedidascalidad['alto'],

				'empaqueinner'=>$vecinner['empaque'],
				'multiploinner'=>$vecinner['multiplo'],
				'largoinner'=>$vecinner['largo'],
				'anchoinner'=>$vecinner['ancho'],
				'altoinner'=>$vecinner['alto'],
				'pesoinner'=>$vecinner['peso'],
				

				'empaquemaster'=>$vecmaster['empaque'],
				'multiplomaster'=>$vecmaster['multiplo'],
				'largomaster'=>$vecmaster['largo'],
				'anchomaster'=>$vecmaster['ancho'],
				'altomaster'=>$vecmaster['alto'],
				'pesomaster'=>$vecmaster['peso'],
				

				'largoprod'=>$vecmedidas['largo'],
				'anchoprod'=>$vecmedidas['ancho'],
				'altoprod'=>$vecmedidas['alto'],
				'pesoprod'=>$vecmedidas['peso'],


				'largopallet'=>$vecpallet['largo'],
				'anchopallet'=>$vecpallet['ancho'],
				'altopallet'=>$vecpallet['alto'],
				'corrugadoscamapallet'=>$vecpallet['corrugadosCama'],
				'camaspallet'=>$vecpallet['camas'],
				'corrugadospallet'=>$vecpallet['corrugadosPallet'],
				'piezaspallet'=>$vecpallet['totalPiezas'],
				'pesobrutopallet'=>$vecpallet['pesoBruto'],
				'pesonetopallet'=>$vecpallet['pesoNeto'],

				'activo'=>$vec['activo']
				);
		
                }
			
	
	return $datos;
	}
	public function getspecificproductbysae($cad){
		if(!$this->userfunctions->islogged()){
			
		}

		$sae=explode("-",$cad);
		$datos=array();
		$result=$this->app->getrecord("productos","*"," where codigoProducto like '$sae[0]'");
		for($a=0;$a<sizeof($result);$a++)
    		{
			$vec=$result[$a];
		
			$datos[] = array(
				'id'=>$vec['id'],
				'nombre'=>$vec['nombreProducto'],
				'codigo'=>$vec['codigoProducto'],	
				'descripcion'=>$vec['descripcion'],
				'presentacion'=>$vec['presentacionVenta'],
				'marca'=>$vec['marcaProducto'],
				'codigobarrasproducto'=>$vec['codigoBarrasProducto'],
				'codigobarrasempaque'=>$vec['codigoBarrasEmpaque'],
				'foto'=>$vec['fotos'],
				'fotodetalle'=>$vec['fotosDetalle'],
				'doc1'=>$vec['documento1'],
				'doc2'=>$vec['documento2'],
				'doc3'=>$vec['documento3'],
				'doc4'=>$vec['documento4'],
				'doc5'=>$vec['documento5'],
				'doc6'=>$vec['documento6'],
				'doc7'=>$vec['documento7'],
				'doc8'=>$vec['documento8'],
				'corrugado'=>$vec['corrugado'],
				'piezascorrugado'=>$vec['piezasCorrugado'],
				'anchocorrugado'=>$vec['anchoCorrugado'],	
				'largocorrugado'=>$vec['largoCorrugado'],
				'altocorrugado'=>$vec['altoCorrugado'],
				'anchopallet'=>$vec['anchoPallet'],
				'largopallet'=>$vec['largoPallet'],
				'alturapallet'=>$vec['alturaPallet'],
				'corrugadosporcama'=>$vec['corrugadosPorCama'],	
				'corrugadosporpallet'=>$vec['corrugadosPorPallet'],
				'camasporpallet'=>$vec['camasPorPallet'],
				'piezasPorPallet'=>$vec['piezasPorPallet'],
				'pesopresentacion'=>$vec['pesoPresentacionVenta'],
				'pesoporcorrugado'=>$vec['pesoPorCorrugado'],
				'pesoporpallet'=>$vec['pesoPorPallet'],
				'anchoproducto'=>$vec['anchoProducto'],
				'largoproducto'=>$vec['largoProducto'],
				'altoproducto'=>$vec['altoProducto'],
				'pais'=>$vec['paisOrigen'],
				'fraccionarancelaria'=>$vec['fraccionArancelaria'],
				'exportacion'=>$vec['exportacion'],
				'notas'=>$vec['notasEspeciales'],
				'barniz'=>$vec['barniz'],
				'activo'=>$vec['activo'],
				'procesos'=>$this->getprocesosproducto($vec['id'])
				);
		
                }
			
	
	return $datos;
	}
	public function updateproducto($codigo,$descripcion,$nombre,$presentacion,$marca,$codigobarras,$codigobarrasempaque,$pais,$fraccionarancelaria,$exportacion,$barniz,$foto,$fotodetalle,$notasespeciales,$corrugado,$corrugadospallet,$piezascorrugado,$camaspallete,$anchocorrugado,$piezaspallet,$largocorrugado,$pesopresentacion,$altrocorrugado,$pesocorrugado,$anchopallet,$pesopallete,$largopallet,$anchoproducto,$alturapallet,$largoproducto,$corrugadosporcama,$altoproducto,$esp1,$esp2,$esp3,$esp4,$esp5,$corrugadoinner,$piezascorrugadoinner,$largocorrugadoinner,$anchocorrugadoinner,$altrocorrugadoinner,$pesocorrugadoinner,$pesoneto,$a,$b,$h){
		if(!$this->userfunctions->islogged()){
			
		}

	//trigger_error($vendedor);
		$datos=array();
		$result=$this->app->getrecord("productos","*"," where codigoProducto like '$codigo'");
		
		if (sizeof($result) > 0) {
			$vec=$result[0];
			$idprod=$vec['id'];

			if($this->app->updaterecord("productos","nombreProducto='$nombre',documento1='$esp1',documento2='$esp2',documento3='$esp3',documento4='$esp4',documento5='$esp5',descripcion='$descripcion',presentacionVenta='$presentacion',marcaProducto='$marca',codigoBarrasProducto='$codigobarras',codigoBarrasEmpaque='$codigobarrasempaque',fotos='$foto',fotosDetalle='$fotodetalle',corrugado='$corrugado',piezasCorrugado=$piezascorrugado,anchoCorrugado=$anchocorrugado,largoCorrugado=$largocorrugado,altoCorrugado=$altrocorrugado,anchoPallet=$anchopallet,largoPallet=$largopallet,alturaPallet=$alturapallet,corrugadosPorCama=$corrugadosporcama,corrugadosPorPallet=$corrugadospallet,camasPorPallet=$camaspallete,piezasPorPallet=$piezaspallet,pesoPresentacionVenta=$pesopresentacion,pesoPorCorrugado=$pesocorrugado,pesoPorPallet=$pesopallete,anchoProducto=$anchoproducto,largoProducto=$largoproducto,altoProducto=$altoproducto,paisOrigen='$pais',fraccionArancelaria='$fraccionarancelaria',exportacion='$exportacion',notasEspeciales='$notasespeciales',barniz='$barniz'"," where codigoProducto like '$codigo'")){
					$datos[]=array(
						'exito'=>'si',
						'folio'=>$id
					);
						
					$this->app->updaterecord("innerpack","empaque='$corrugadoinner',multiplo=$piezascorrugadoinner,largo=$largocorrugadoinner,ancho=$anchocorrugadoinner,alto=$altrocorrugadoinner,peso=$pesocorrugadoinner","where idProducto=$idprod");
					$this->app->updaterecord("masterpack","empaque='$corrugado',multiplo=$piezascorrugado,largo=$largocorrugado,ancho=$anchocorrugado,alto=$altrocorrugado,peso=$pesocorrugado","where idProducto=$idprod");
					$this->app->updaterecord("medidas_producto","ancho=$anchoproducto,largo=$largoproducto,alto=$altoproducto,peso=$pesopresentacion","where idProducto=$idprod");
					$this->app->updaterecord("pallet"," ancho=$anchopallet,largo=$largopallet,alto=$alturapallet,corrugadosCama=$corrugadosporcama,corrugadosPallet=$corrugadospallet,camas=$camaspallete,totalPiezas=$piezaspallet,pesoBruto=$pesopallete,pesoNeto=$pesoneto","where idProducto=$idprod");
					$this->app->updaterecord("medidas_productocertificadocalidad","ancho=$a,largo=$b,alto=$h","where idProducto=$idprod");
					$datos[]=array(
						'exito'=>'si',
						'folio'=>$id
					);
							
				$result=$this->app->getrecord("paths","*"," where codigoProducto like '$codigo' and tipo like 'U'");
				if(sizeof($result)>0){
					for($a=0;$a<sizeof($result);$a++){
						$vec=$result[$a];
						
						switch($vec['archivo']){
							case "doc1";
								if($this->app->updaterecord("productos","documento1='$vec[ruta]'"," where codigoProducto like '$codigo'")){
									$ext=explode(".",$vec['ruta']);
									$file_pattern = $this->path ."doc1-".$codigo.".*";
									array_map( "unlink", glob( $file_pattern ) );
									rename ($this->path."doc1-".$codigo."U.".$ext[1],$this->path."doc1-".$codigo.".".$ext[1]);
								}
								
							break;							
							case "doc2";
								if($this->app->updaterecord("productos","documento2='$vec[ruta]'"," where codigoProducto like '$codigo'")){
									$ext=explode(".",$vec['ruta']);
									$file_pattern = $this->path ."doc2-".$codigo.".*";
									array_map( "unlink", glob( $file_pattern ) );
									rename ($this->path."doc2-".$codigo."U.".$ext[1],$this->path."doc2-".$codigo.".".$ext[1]);
								}							break;							
							case "doc3";
								if($this->app->updaterecord("productos","documento3='$vec[ruta]'"," where codigoProducto like '$codigo'")){
									$ext=explode(".",$vec['ruta']);
									$file_pattern =$this->path ."doc3-".$codigo.".*";
									array_map( "unlink", glob( $file_pattern ) );
									rename ($this->path."doc3-".$codigo."U.".$ext[1], $this->path."doc3-".$codigo.".".$ext[1]);
								}
							break;							
							case "doc4";
								if($this->app->updaterecord("productos","documento4='$vec[ruta]'"," where codigoProducto like '$codigo'")){
									$ext=explode(".",$vec['ruta']);
									$file_pattern = $this->path ."doc4-".$codigo.".*";
									array_map( "unlink", glob( $file_pattern ) );
									rename ($this->path."doc4-".$codigo."U.".$ext[1], $this->path."doc4-".$codigo.".".$ext[1]);
								}
							break;							
							case "doc5";
								if($this->app->updaterecord("productos","documento5='$vec[ruta]'"," where codigoProducto like '$codigo'")){
									$ext=explode(".",$vec['ruta']);
									$file_pattern = $this->path ."doc5-".$codigo.".*";
									array_map( "unlink", glob( $file_pattern ) );
									rename ($this->path."doc5-".$codigo."U.".$ext[1], $this->path."doc5-".$codigo.".".$ext[1]);
								}
							break;							
							case "doc6";
								if($this->app->updaterecord("productos","documento6='$vec[ruta]'"," where codigoProducto like '$codigo'")){
									$ext=explode(".",$vec['ruta']);
									$file_pattern = $this->path ."doc6-".$codigo.".*";
									array_map( "unlink", glob( $file_pattern ) );
									rename ($this->path."doc6-".$codigo."U.".$ext[1], $this->path."doc6-".$codigo.".".$ext[1]);
								}
							break;							
							case "doc7";
								if($this->app->updaterecord("productos","documento7='$vec[ruta]'"," where codigoProducto like '$codigo'")){
									$ext=explode(".",$vec['ruta']);
									$file_pattern =$this->path ."doc7-".$codigo.".*";
									array_map( "unlink", glob( $file_pattern ) );
									rename ($this->path."doc7-".$codigo."U.".$ext[1], $this->path."doc7-".$codigo.".".$ext[1]);
								}
							break;							
							case "doc8";
								if($this->app->updaterecord("productos","documento8='$vec[ruta]'"," where codigoProducto like '$codigo'")){
									$ext=explode(".",$vec['ruta']);
									$file_pattern = $this->path ."doc8-".$codigo.".*";
									array_map( "unlink", glob( $file_pattern ) );
									rename ($this->path."doc8-".$codigo."U.".$ext[1], $this->path."doc8-".$codigo.".".$ext[1]);
								}
							break;							


						}
						
					}
				}
							
			}		
				
		} 
		else{
			
			$datos[]=array(
					'exito'=>'sin',
					'folio'=>0
				);
		}

	$this->app->deleterecord('paths',"where codigoProducto like '$codigo'");
	return $datos;	
	}


	public function updateproductoenable($codigo,$descripcion,$nombre,$presentacion,$marca,$codigobarras,$codigobarrasempaque,$pais,$fraccionarancelaria,$exportacion,$barniz,$foto,$fotodetalle,$notasespeciales,$corrugado,$corrugadospallet,$piezascorrugado,$camaspallete,$anchocorrugado,$piezaspallet,$largocorrugado,$pesopresentacion,$altrocorrugado,$pesocorrugado,$anchopallet,$pesopallete,$largopallet,$anchoproducto,$alturapallet,$largoproducto,$corrugadosporcama,$altoproducto,$activo,$esp1,$esp2,$esp3,$esp4,$esp5,$corrugadoinner,$piezascorrugadoinner,$largocorrugadoinner,$anchocorrugadoinner,$altrocorrugadoinner,$pesocorrugadoinner,$pesoneto,$a,$b,$h){
		if(!$this->userfunctions->islogged()){
			
		}

	//trigger_error($codigo);
		$datos=array();
		$result=$this->app->getrecord("productos","*"," where codigoProducto like '$codigo'");
		
		if (sizeof($result) > 0) {
			$vec=$result[0];
			$idprod=$vec['id'];
			if($this->app->updaterecord("productos","nombreProducto='$nombre',documento1='$esp1',documento2='$esp2',documento3='$esp3',documento4='$esp4',documento5='$esp5',descripcion='$descripcion',presentacionVenta='$presentacion',marcaProducto='$marca',codigoBarrasProducto='$codigobarras',codigoBarrasEmpaque='$codigobarrasempaque',fotos='$foto',fotosDetalle='$fotodetalle',corrugado='$corrugado',piezasCorrugado=$piezascorrugado,anchoCorrugado=$anchocorrugado,largoCorrugado=$largocorrugado,altoCorrugado=$altrocorrugado,anchoPallet=$anchopallet,largoPallet=$largopallet,alturaPallet=$alturapallet,corrugadosPorCama=$corrugadosporcama,corrugadosPorPallet=$corrugadospallet,camasPorPallet=$camaspallete,piezasPorPallet=$piezaspallet,pesoPresentacionVenta=$pesopresentacion,pesoPorCorrugado=$pesocorrugado,pesoPorPallet=$pesopallete,anchoProducto=$anchoproducto,largoProducto=$largoproducto,altoProducto=$altoproducto,paisOrigen='$pais',fraccionArancelaria='$fraccionarancelaria',exportacion='$exportacion',notasEspeciales='$notasespeciales',barniz='$barniz',activo='$activo'"," where codigoProducto like '$codigo'")){
					$datos[]=array(
						'exito'=>'si',
						'folio'=>$id
					);
						
					$this->app->updaterecord("innerpack","empaque='$corrugadoinner',multiplo=$piezascorrugadoinner,largo=$largocorrugadoinner,ancho=$anchocorrugadoinner,alto=$altrocorrugadoinner,peso=$pesocorrugadoinner","where idProducto=$idprod");
					$this->app->updaterecord("masterpack","empaque='$corrugado',multiplo=$piezascorrugado,largo=$largocorrugado,ancho=$anchocorrugado,alto=$altrocorrugado,peso=$pesocorrugado","where idProducto=$idprod");
					$this->app->updaterecord("medidas_producto","ancho=$anchoproducto,largo=$largoproducto,alto=$altoproducto,peso=$pesopresentacion","where idProducto=$idprod");
					$this->app->updaterecord("pallet"," ancho=$anchopallet,largo=$largopallet,alto=$alturapallet,corrugadosCama=$corrugadosporcama,corrugadosPallet=$corrugadospallet,camas=$camaspallete,totalPiezas=$piezaspallet,pesoBruto=$pesopallete,pesoNeto=$pesoneto","where idProducto=$idprod");
					$this->app->updaterecord("medidas_productocertificadocalidad","ancho=$a,largo=$b,alto=$h","where idProducto=$idprod");
				/*$result=$this->app->getrecord("paths","*"," where codigoProducto like '$codigo' and tipo like 'U'");
				if(sizeof($result)>0){
					for($a=0;$a<sizeof($result);$a++){
						$vec=$result[$a];
						
						switch($vec['archivo']){
							case "doc1";
								if($this->app->updaterecord("productos","documento1='$vec[ruta]'"," where codigoProducto like '$codigo'")){
									$ext=explode(".",$vec['ruta']);
									$file_pattern = $this->path ."doc1-".$codigo.".*";
									array_map( "unlink", glob( $file_pattern ) );
									rename ($this->path."doc1-".$codigo."U.".$ext[1], $this->path."doc1-".$codigo.".".$ext[1]);
								}
								
							break;							
							case "doc2";
								if($this->app->updaterecord("productos","documento2='$vec[ruta]'"," where codigoProducto like '$codigo'")){
									$ext=explode(".",$vec['ruta']);
									$file_pattern =$this->path ."doc2-".$codigo.".*";
									array_map( "unlink", glob( $file_pattern ) );
									rename ($this->path."doc2-".$codigo."U.".$ext[1], $this->path."doc2-".$codigo.".".$ext[1]);
								}							
							break;							
							case "doc3";
								if($this->app->updaterecord("productos","documento3='$vec[ruta]'"," where codigoProducto like '$codigo'")){
									$ext=explode(".",$vec['ruta']);
									$file_pattern = $this->path."doc3-".$codigo.".*";
									array_map( "unlink", glob( $file_pattern ) );
									rename ($this->path."doc3-".$codigo."U.".$ext[1], $this->path."doc3-".$codigo.".".$ext[1]);
								}
							break;							
							case "doc4";
								if($this->app->updaterecord("productos","documento4='$vec[ruta]'"," where codigoProducto like '$codigo'")){
									$ext=explode(".",$vec['ruta']);
									$file_pattern =$this->path ."doc4-".$codigo.".*";
									array_map( "unlink", glob( $file_pattern ) );
									rename ($this->path."doc4-".$codigo."U.".$ext[1], $this->path."doc4-".$codigo.".".$ext[1]);
								}
							break;							
							case "doc5";
								if($this->app->updaterecord("productos","documento5='$vec[ruta]'"," where codigoProducto like '$codigo'")){
									$ext=explode(".",$vec['ruta']);
									$file_pattern = $this->path ."doc5-".$codigo.".*";
									array_map( "unlink", glob( $file_pattern ) );
									rename ($this->path."doc5-".$codigo."U.".$ext[1], $this->path."doc5-".$codigo.".".$ext[1]);
								}
							break;							
							case "doc6";
								if($this->app->updaterecord("productos","documento6='$vec[ruta]'"," where codigoProducto like '$codigo'")){
									$ext=explode(".",$vec['ruta']);
									$file_pattern = $this->path ."doc6-".$codigo.".*";
									array_map( "unlink", glob( $file_pattern ) );
									rename ($this->path."doc6-".$codigo."U.".$ext[1], $this->path."doc6-".$codigo.".".$ext[1]);
								}
							break;							
							case "doc7";
								if($this->app->updaterecord("productos","documento7='$vec[ruta]'"," where codigoProducto like '$codigo'")){
									$ext=explode(".",$vec['ruta']);
									$file_pattern = $this->path ."doc7-".$codigo.".*";
									array_map( "unlink", glob( $file_pattern ) );
									rename ($this->path."doc7-".$codigo."U.".$ext[1], $this->path."doc7-".$codigo.".".$ext[1]);
								}
							break;							
							case "doc8";
								if($this->app->updaterecord("productos","documento8='$vec[ruta]'"," where codigoProducto like '$codigo'")){
									$ext=explode(".",$vec['ruta']);
									$file_pattern =$this->path ."doc8-".$codigo.".*";
									array_map( "unlink", glob( $file_pattern ) );
									rename ($this->path."doc8-".$codigo."U.".$ext[1], $this->path."doc8-".$codigo.".".$ext[1]);
								}
							break;							


						}
						
					}
				}*/
				
							
			}		
				
		} 
		else{
			
			$datos[]=array(
					'exito'=>'sin',
					'folio'=>0
				);
		}

	$this->app->deleterecord('paths',"where codigoProducto like '$codigo'");
	return $datos;	
	}

	public function deleteproducto($id){
		if(!$this->userfunctions->islogged()){
			
		}

		$datos=array();
		$result=$this->app->getrecord("productos","*"," where id=$id");
		
		if (sizeof($result) > 0) 
		{		
			if($this->app->updaterecord("productos","activo='0'"," where id=$id")){
					$datos[]=array(
						'exito'=>'si'
					);					
			}		
				
		} 
		else{
			
			$datos[]=array(
					'exito'=>'sin'

				);
		}


	return $datos;	
	}

	public function deletefile($id){
		if(!$this->userfunctions->islogged()){
			
		}

		$datos=array();
		$result=$this->app->getrecord("rel_productoarchivo","*"," where id=$id");
		
		if (sizeof($result) > 0) 
		{		$vec=$result[0];
			unlink($this->path2.$vec['ruta']);
			if($this->app->deleterecord("rel_productoarchivo"," where id=$id")){
					$datos[]=array(
						'exito'=>'si'
					);					
			}		
				
		} 
		else{
			
			$datos[]=array(
					'exito'=>'sin'

				);
		}


	return $datos;		
	}
	public function duplicateproducto ($idold,$idnew,$nombrenew){
		if(!$this->userfunctions->islogged()){
			
		}

		$datos=array();
		$result=$this->app->getrecord("productos","*","where codigoProducto like '$idnew'");
				
		if (sizeof($result) > 0) {
			$datos[]=array(
				'exito'=>'ya',
				'folio'=>0
			);
		} 
		else 
		{
			$result=$this->app->getrecord("productos","*"," where codigoProducto like '$idold'");
			for($a=0;$a<sizeof($result);$a++)
    			{
				$vec=$result[$a];
				$doc1=$vec['documento1'];
				$doc2=$vec['documento2'];
				$doc3=$vec['documento3'];
				$doc4=$vec['documento4'];
				$doc5=$vec['documento5'];
				
				/*$aux=explode("-",$vec['documento1']);
				$aux2=explode(".",$aux[1]);
				$doc1=$aux[0]."-".$idnew.".".$aux2[1];
				$file = $this->path2.$vec['documento1'];
				$newfile =$this->path2.$doc1;
				copy($file, $newfile); 

				$aux=explode("-",$vec['documento2']);
				$aux2=explode(".",$aux[1]);
				$doc2=$aux[0]."-".$idnew.".".$aux2[1];
				$file = $this->path2.$vec['documento2'];
				$newfile =$this->path2.$doc2;
				copy($file, $newfile);
 				
				$aux=explode("-",$vec['documento3']);
				$aux2=explode(".",$aux[1]);
				$doc3=$aux[0]."-".$idnew.".".$aux2[1];
				$file = $this->path2.$vec['documento3'];
				$newfile =$this->path2.$doc3;
				copy($file, $newfile);
 				
				$aux=explode("-",$vec['documento4']);
				$aux2=explode(".",$aux[1]);
				$doc4=$aux[0]."-".$idnew.".".$aux2[1];
				$file = $this->path2.$vec['documento4'];
				$newfile =$this->path2.$doc4;
				copy($file, $newfile);
 				
				$aux=explode("-",$vec['documento5']);
				$aux2=explode(".",$aux[1]);
				$doc5=$aux[0]."-".$idnew.".".$aux2[1];
				$file = $this->path2.$vec['documento5'];
				$newfile =$this->path2.$doc5;
				copy($file, $newfile);
 				
				$aux=explode("-",$vec['documento6']);
				$aux2=explode(".",$aux[1]);
				$doc6=$aux[0]."-".$idnew.".".$aux2[1];
				$file = $this->path2.$vec['documento6'];
				$newfile =$this->path2.$doc6;
				copy($file, $newfile);
 				
				$aux=explode("-",$vec['documento7']);
				$aux2=explode(".",$aux[1]);
				$doc7=$aux[0]."-".$idnew.".".$aux2[1];
				$file =$this->path2.$vec['documento7'];
				$newfile =$this->path2.$doc7;
				copy($file, $newfile);
 				
				$aux=explode("-",$vec['documento8']);
				$aux2=explode(".",$aux[1]);
				$doc8=$aux[0]."-".$idnew.".".$aux2[1];
				$file =$this->path2.$vec['documento8'];
				$newfile =$this->path2.$doc8;
				copy($file, $newfile);*/
				
				$id=$this->app->saverecord("productos","'$nombrenew','$idnew','$vec[descripcion]','$vec[presentacionVenta]','$vec[marcaProducto]','$vec[codigoBarrasProducto]','$vec[codigoBarrasEmpaque]','$vec[fotos]','$vec[fotosDetalle]','$doc1','$doc2','$doc3','$doc4','$doc5','$doc6','$doc7','$doc8','$vec[corrugado]',$vec[piezasCorrugado],$vec[anchoCorrugado],$vec[largoCorrugado],$vec[altoCorrugado],$vec[anchoPallet],$vec[largoPallet],$vec[alturaPallet],$vec[corrugadosPorCama],$vec[corrugadosPorPallet],$vec[camasPorPallet],$vec[piezasPorPallet],$vec[pesoPresentacionVenta],$vec[pesoPorCorrugado],$vec[pesoPorPallet],$vec[anchoProducto],$vec[largoProducto],$vec[altoProducto],'$vec[paisOrigen]','$vec[fraccionArancelaria]','$vec[exportacion]','$vec[notasEspeciales]','$vec[barniz]','1'","(nombreProducto,codigoProducto,descripcion,presentacionVenta,marcaProducto,codigoBarrasProducto,codigoBarrasEmpaque,fotos,fotosDetalle,documento1,documento2,documento3,documento4,documento5,documento6,documento7,documento8,corrugado,piezasCorrugado,anchoCorrugado,largoCorrugado,altoCorrugado,anchoPallet,largoPallet,alturaPallet,corrugadosPorCama,corrugadosPorPallet,camasPorPallet,piezasPorPallet,pesoPresentacionVenta,pesoPorCorrugado,pesoPorPallet,anchoProducto,largoProducto,altoProducto,paisOrigen,fraccionArancelaria,exportacion,notasEspeciales,barniz,activo)");			
									
				if($id>0 ){
					$result2=$this->app->getrecord("procesos","*"," where idProducto= $vec[id] order by id ASC");
					for($aa=0;$aa<sizeof($result2);$aa++)
    					{
						$vec2=$result2[$aa];
						$id2=$this->app->saverecord("procesos","$id,'$vec2[categoria]','$vec2[descripcionProceso]','$vec2[subensamble]','$vec2[proceso]',$vec2[maquina],'$vec2[medidaEntrada]','$vec2[piezasFormato]','$vec2[mermaAjuste]','$vec2[estandarProcesoHora]','$vec2[estandarAjuste]','$vec2[estandarCalculado]','$vec2[especificacion1]','$vec2[especificacion2]','$vec2[especificacion3]','$vec2[especificacion4]','$vec2[especificacion5]','$vec2[multiploProceso]',$vec2[horasTurno],'$vec2[estandarTurno]','1'","(idProducto,categoria,descripcionProceso,subensamble,proceso,maquina,medidaEntrada,piezasFormato,mermaAjuste,estandarProcesoHora,estandarAjuste,estandarCalculado,especificacion1,especificacion2,especificacion3,especificacion4,especificacion5,multiploProceso,horasTurno,estandarTurno,activo)");			

					}
			
					$result2=$this->app->getrecord("relproducto_componente","*"," where idProducto= $vec[id]");
					for($aa=0;$aa<sizeof($result2);$aa++)
    					{
						$vec2=$result2[$aa];
						$id3=$this->app->saverecord("relproducto_componente","$id,$vec2[idComponente],$vec2[cantidad],'$vec2[insumo]','1'","(idProducto,idComponente,cantidad,insumo,activo)");			
													
					}

					$result3=$this->app->getrecord("rel_productoarchivo","*"," where idProducto= $vec[id] and claveSAE like '$idold'");
					for($aa=0;$aa<sizeof($result3);$aa++)
    					{	$vec3=$result3[$aa];

						$aux=explode("-",$vec3['ruta']);
						$aux2=explode(".",$aux[1]);
						$doc=$aux[0]."-".$idnew.".".$aux2[1];
						//trigger_error($file. $newfile);
						$file =$this->path2."../".$vec3['ruta'];
						$newfile =$this->path2."../".$doc;
						copy($file, $newfile);
						
						$id3=$this->app->saverecord("rel_productoarchivo","$id,'$idnew','$doc','$vec3[descripcion]','1'","(idProducto,claveSAE,ruta,descripcion,activo)");			
														
					}

					$result4=$this->app->getrecord("innerpack","*"," where idProducto= $vec[id]");
					for($aa=0;$aa<sizeof($result4);$aa++)
    					{	$vec3=$result4[$aa];

				
						$this->app->saverecord("innerpack","$id,'$vec3[empaque]',$vec3[multiplo],$vec3[largo],$vec3[ancho],$vec3[alto],$vec3[peso],'1'","(idProducto,empaque,multiplo,largo,ancho,alto,peso,activo)");
											

		
					}

					$result5=$this->app->getrecord("masterpack","*"," where idProducto= $vec[id]");
					for($aa=0;$aa<sizeof($result5);$aa++)
    					{	$vec3=$result5[$aa];

				
						$this->app->saverecord("masterpack","$id,'$vec3[empaque]',$vec3[multiplo],$vec3[largo],$vec3[ancho],$vec3[alto],$vec3[peso],'1'","(idProducto,empaque,multiplo,largo,ancho,alto,peso,activo)");
											
					}
					
					$result6=$this->app->getrecord("medidas_producto","*"," where idProducto= $vec[id]");
					for($aa=0;$aa<sizeof($result6);$aa++)
    					{	$vec3=$result6[$aa];

				
						$this->app->saverecord("medidas_producto","$id,$vec3[ancho],$vec3[largo],$vec3[alto],$vec3[peso],'1'","(idProducto,ancho,largo,alto,peso,activo)");
												
					}
					$result7=$this->app->getrecord("pallet","*"," where idProducto= $vec[id]");
					for($aa=0;$aa<sizeof($result6);$aa++)
    					{	$vec3=$result7[$aa];

										
						$this->app->saverecord("pallet","$id,$vec3[largo],$vec3[ancho],$vec3[alto],$vec3[corrugadosCama],$vec3[camas],$vec3[corrugadosPallet],$vec3[totalPiezas],$vec3[pesoBruto],$vec3[pesoNeto],'1'","(idProducto,largo,ancho,alto,corrugadosCama,camas,corrugadosPallet,totalPiezas,pesoBruto,pesoNeto,activo)");
										
					}
					
					$result8=$this->app->getrecord("medidas_productocertificadocalidad","*"," where idProducto=$vec[id]");
					for($aa=0;$aa<sizeof($result8);$aa++)
    					{	
						$vec3=$result8[$aa];
						$this->app->saverecord("medidas_productocertificadocalidad","$id,$vec3[largo],$vec3[ancho],$vec3[alto],'1'","(idProducto,largo,ancho,alto,activo)");

					}

					$datos[]=array(
						'exito'=>'si',
						'folio'=>$id
					);	
				}else{
					$datos[]=array(
						'exito'=>'no',
						'folio'=>0
					);	
				}	
		
                	}
		}
			
	

	return $datos;	
	}
	public function getallmaquinas(){
		if(!$this->userfunctions->islogged()){
			
		}

		$datos=array();
		$result=$this->app->getrecord("maquinas","*","where activo like '1' order by nombre asc");
		for($a=0;$a<sizeof($result);$a++)
    		{
			$vec=$result[$a];	

			$datos[] = array(
					'id'=>$vec['id'],
					'nombre'=>$vec['nombre']
					);			
                }
		
		
	return $datos; 
	}
	public function getmaquinadetail ($id,$idproducto=0){
		if(!$this->userfunctions->islogged()){
			
		}

		$datos=array();
		$result=$this->app->getrecord("maquinas","*","where id=$id and activo like '1'");
		for($a=0;$a<sizeof($result);$a++)
    		{
			$vec=$result[$a];	

			$datos[] = array(
					'id'=>$vec['id'],
					'nombre'=>$vec['nombre'],
					'proceso'=>$vec['proceso'],
					'estandar'=>$vec['estandar'],
					'um'=>$vec['unidad_medida'],
					'mermafija'=>$vec['mermaFija'],
					'mermaproceso'=>$vec['mermaProceso'],
					'umentrada'=>$vec['unidadEntrada'],
					'umsalida'=>$vec['unidadSalida'],
					'USD'=>$vec['USD'],
					'mx'=>$vec['MX'],
					'tiempoajuste'=>$vec['tiempoAjuste'],
					'numeroprocesos'=>$this->countprocessbyproduct($idproducto),
					'activo'=>$vec['activo']
				);

                }
		
		
	return $datos; 
	} 
	public function countprocessbyproduct($id){
		if(!$this->userfunctions->islogged()){
			
		}

		$datos=array();
		$result=$this->app->getrecord("procesos","COUNT(idProducto) as 'procesos' ","where idProducto=$id");
		for($a=0;$a<sizeof($result);$a++)
    		{
			$vec=$result[$a];	

			$datos[] = array(
					'procesos'=>$vec['procesos']
				);

                }
		
		
	return $datos; 
	}	
	public function getmaquinas(){
		if(!$this->userfunctions->islogged()){
			
		}

		$datos=array();
		$result=$this->app->getrecord("maquinas","*","order by nombre asc");
		for($a=0;$a<sizeof($result);$a++)
    		{
			$vec=$result[$a];	

			$datos[] = array(
					'id'=>$vec['id'],
					'nombre'=>$vec['nombre'],
					'proceso'=>$vec['proceso'],
					'estandar'=>$vec['estandar'],
					'um'=>$vec['unidad_medida'],
					'mermafija'=>$vec['mermaFija'],
					'mermaproceso'=>$vec['mermaProceso'],
					'umentrada'=>$vec['unidadEntrada'],
					'umsalida'=>$vec['unidadSalida'],
					'USD'=>$vec['USD'],
					'mx'=>$vec['MX'],
					'tiempoajuste'=>$vec['tiempoAjuste'],
					'activo'=>$vec['activo']

					);			
                }
		
		
	return $datos; 
	} 
	public function deletemaquina($id){
		if(!$this->userfunctions->islogged()){
			
		}

		$datos=array();
		$result=$this->app->getrecord("maquinas","*"," where id=$id");
		
		if (sizeof($result) > 0) 
		{		
			if($this->app->deleterecord("maquinas"," where id=$id")>0){
					$datos[]=array(
						'exito'=>'si'
					);					
			}		
				
		} 
		else{
			
			$datos[]=array(
					'exito'=>'sin'

				);
		}


	return $datos;

	}

	public function updatemaquina ($id,$nombre,$proceso,$estandar,$um,$tiempoajuste,$mermaajuste,$mermaproceso,$umentrada,$umsalida,$usd,$mx,$activo){
		if(!$this->userfunctions->islogged()){
			
		}

	//trigger_error($vendedor);
		$datos=array();
		$result=$this->app->getrecord("maquinas","*"," where id= $id");
		
		if (sizeof($result) > 0) {
		
			if($this->app->updaterecord("maquinas","nombre='$nombre',proceso='$proceso',estandar=$estandar,unidad_medida='$um',mermaFija=$mermaajuste,mermaProceso=$mermaproceso,tiempoAjuste=$tiempoajuste,unidadEntrada='$umentrada',unidadSalida='$umsalida',USD=$usd,MX=$mx,activo='$activo'"," where id=$id")){
					$datos[]=array(
						'exito'=>'si',
						'folio'=>$id
					);
							
			}	
				
		} 
		else{
			
			$datos[]=array(
					'exito'=>'sin',
					'folio'=>0
				);
		}
	return $datos;	
	}
	public function savemaquina($nombre,$proceso,$estandar,$um,$tiempoajuste,$mermaajuste,$mermaproceso,$umentrada,$umsalida,$usd,$mx){
		if(!$this->userfunctions->islogged()){
			
		}

	$datos=array();
		$result=$this->app->getrecord("maquinas","*","where nombre like '$nombre'");
				
		if (sizeof($result) > 0) {
			$datos[]=array(
				'exito'=>'ya',
				'folio'=>0
			);
		} 
		else 
		{
			
						
			$id=$this->app->saverecord("maquinas","null,'$nombre','$proceso',$estandar,'$um',$mermaajuste,$mermaproceso,$tiempoajuste,'$umentrada','$umsalida',$usd,$mx,'1'");			
			if($id>0 ){
				
				$datos[]=array(
					'exito'=>'si',
					'folio'=>$id
				);	
			}else{
				$datos[]=array(
					'exito'=>'no',
					'folio'=>0
				);	
			}			
		}


		
	return $datos;
	}
	public function saveproceso ($idproducto,$maquina,$proceso,$categoria,$subensamble,$descripcion,$medidaentrada,$piezasforma,$mermaajuste,$estandarproceso,$estandarajuste,$estandarcalculado,$espec1,$espec2,$espec3,$espec4,$espec5,$multiploproceso,$horas,$estandarturno,$bobina){
		if(!$this->userfunctions->islogged()){
			
		}

	$datos=array();	
	$idbobina;
	$largobobina;
			$categoria=ucfirst ($categoria);
		$result2=$this->app->getrecord("categorias","*","where nombre like '$categoria'");
			//trigger_error(sizeof($result2));
			if(sizeof($result2)<=0){
				
				$this->app->saverecord("categorias","null,'$categoria','','1'");
			}
		$subensamble=ucfirst ($subensamble);
		$result2=$this->app->getrecord("subensambles","*","where nombre like '$subensamble'");
			//trigger_error(sizeof($result2));
			if(sizeof($result2)<=0){
				
				$this->app->saverecord("subensambles","null,'$subensamble','','1'");
			}
		$result3=$this->app->getrecord("bobinas","*","where codigoSAE like '$bobina'");
			//trigger_error("where codigoSAE like '$bobina'");
			if(sizeof($result3)>0){
				$vec1=$result3[0];
				//trigger_error(print_r($vec1,true));
				$idbobina=$vec1['id'];
				$largobobina=$vec1['largo'];	
			}
		$id=$this->app->saverecord("procesos","null,$idproducto,'$categoria','$descripcion','$subensamble','$proceso',$maquina,'$medidaentrada','$piezasforma','$mermaajuste','$estandarproceso','$estandarajuste','$estandarcalculado','$espec1','$espec2','$espec3','$espec4','$espec5','$multiploproceso','','','',$horas,'$estandarturno','','','','1'");			
				if($id>0 ){
						$this->app->saverecord("rel_procesobobina","null,$id,$idbobina,$largobobina,'1'");
					$datos[]=array(
						'exito'=>'si',
						'folio'=>$id
					);	
				}else{
					$datos[]=array(
						'exito'=>'no',
						'folio'=>0
					);	
				}	
	return $datos;
	}
	
	public function getprocesosproducto($idproducto){
		if(!$this->userfunctions->islogged()){
			
		}

	$datos=array();
		$result=$this->app->getrecord("procesos","procesos.id,procesos.idProducto,procesos.categoria,procesos.descripcionProceso,procesos.subensamble,procesos.proceso,procesos.maquina,procesos.medidaEntrada,procesos.piezasFormato,procesos.mermaAjuste,procesos.estandarProcesoHora,procesos.estandarAjuste,procesos.estandarCalculado,procesos.especificacion1,procesos.especificacion2,procesos.especificacion3,procesos.especificacion4,procesos.especificacion5,procesos.multiploProceso,procesos.cantidadOrden,procesos.cantidadProducto,procesos.cantidadProceso,procesos.horasTurno,procesos.estandarTurno,procesos.cantidadTurno,procesos.cantidadTickets,procesos.horasRequeridas,procesos.activo,maquinas.nombre","inner join maquinas on maquinas.id=procesos.maquina where procesos.idProducto=$idproducto order BY procesos.id asc");
		for($a=0;$a<sizeof($result);$a++)
    		{
			$vec=$result[$a];
		
			$datos[] = array(
				'id'=>$vec['id'],
				'idproducto'=>$vec['idProducto'],
				'categoria'=>$vec['categoria'],	
				'maquina'=>$vec['nombre'],
				'descripcion'=>$vec['descripcionProceso'],
				'subensamble'=>$vec['subensamble'],
				'proceso'=>$vec['proceso'],
				'maquinaid'=>$vec['maquina'],
				'medidaentrada'=>$vec['medidaEntrada'],
				'piezasformato'=>$vec['piezasFormato'],
				'mermaajuste'=>$vec['mermaAjuste'],
				'estandarhora'=>$vec['estandarProcesoHora'],
				'estandarajuste'=>$vec['estandarAjuste'],
				'estandarcalculado'=>$vec['estandarCalculado'],
				'esp1'=>$vec['especificacion1'],
				'esp2'=>$vec['especificacion2'],
				'esp3'=>$vec['especificacion3'],
				'esp4'=>$vec['especificacion4'],
				'esp5'=>$vec['especificacion5'],
				'multiploproceso'=>$vec['multiploProceso'],
				'cantidadorden'=>$vec['cantidadOrden'],
				'cantidadproducto'=>$vec['cantidadProducto'],	
				'cantidadproceso'=>$vec['cantidadProceso'],
				'horas'=>$vec['horasTurno'],
				'estandarturno'=>$vec['estandarTurno'],
				'cantidadturno'=>$vec['cantidadTurno'],
				'cantidadtickets'=>$vec['cantidadTickets'],
				'horasrequeridas'=>$vec['horasRequeridas'],	
				'activo'=>$vec['activo'],
				'ordenes'=>$this->getordersenablesbyproduct($idproducto)
				);
		
                }
			
	
	return $datos;
	}

	public function getordersenablesbyproduct($idproducto){
		if(!$this->userfunctions->islogged()){
			
		}
		$result=$this->app->getrecord("ordenmaestra","*","where idProducto=$idproducto and (idStatus=7 or idStatus=3 or idStatus=5)");
		//$result=$this->app->getrecord("ordenmaestra","*","where idProducto=$idproducto and idStatus=0");

		for($a=0;$a<sizeof($result);$a++)
    		{
			$vec=$result[$a];
		
			$datos[] = array(
				'id'=>$vec['id'],
				'pedidosae'=>$vec['pedidoSAE'],
				'fechaemision'=>$vec['fechaEmision'],	
				
				);
		
                }
			
	
	return $datos;
	}
	public function getprocesosproductoenables($idproducto){
		if(!$this->userfunctions->islogged()){
			
		}

	$datos=array();
		$result=$this->app->getrecord("procesos","procesos.id,procesos.idProducto,procesos.categoria,procesos.descripcionProceso,procesos.subensamble,procesos.proceso,procesos.maquina,procesos.medidaEntrada,procesos.piezasFormato,procesos.mermaAjuste,procesos.estandarProcesoHora,procesos.estandarAjuste,procesos.estandarCalculado,procesos.especificacion1,procesos.especificacion2,procesos.especificacion3,procesos.especificacion4,procesos.especificacion5,procesos.multiploProceso,procesos.cantidadOrden,procesos.cantidadProducto,procesos.cantidadProceso,procesos.horasTurno,procesos.estandarTurno,procesos.cantidadTurno,procesos.cantidadTickets,procesos.horasRequeridas,procesos.activo,maquinas.nombre","inner join maquinas on maquinas.id=procesos.maquina where procesos.idProducto=$idproducto and procesos.activo like '1' order BY procesos.id asc");
		for($a=0;$a<sizeof($result);$a++)
    		{
			$vec=$result[$a];
		
			$datos[] = array(
				'id'=>$vec['id'],
				'idproducto'=>$vec['idProducto'],
				'categoria'=>$vec['categoria'],	
				'maquina'=>$vec['nombre'],
				'descripcion'=>$vec['descripcionProceso'],
				'subensamble'=>$vec['subensamble'],
				'proceso'=>$vec['proceso'],
				'maquinaid'=>$vec['maquina'],
				'medidaentrada'=>$vec['medidaEntrada'],
				'piezasformato'=>$vec['piezasFormato'],
				'mermaajuste'=>$vec['mermaAjuste'],
				'estandarhora'=>$vec['estandarProcesoHora'],
				'estandarajuste'=>$vec['estandarAjuste'],
				'estandarcalculado'=>$vec['estandarCalculado'],
				'esp1'=>$vec['especificacion1'],
				'esp2'=>$vec['especificacion2'],
				'esp3'=>$vec['especificacion3'],
				'esp4'=>$vec['especificacion4'],
				'esp5'=>$vec['especificacion5'],
				'multiploproceso'=>$vec['multiploProceso'],
				'cantidadorden'=>$vec['cantidadOrden'],
				'cantidadproducto'=>$vec['cantidadProducto'],	
				'cantidadproceso'=>$vec['cantidadProceso'],
				'horas'=>$vec['horasTurno'],
				'estandarturno'=>$vec['estandarTurno'],
				'cantidadturno'=>$vec['cantidadTurno'],
				'cantidadtickets'=>$vec['cantidadTickets'],
				'horasrequeridas'=>$vec['horasRequeridas'],	
				'activo'=>$vec['activo']
				);
		
                }
			
	
	return $datos;
	}


	public function deleterecordproceso ($id){
		if(!$this->userfunctions->islogged()){
			
		}

		$datos=array();
		$result=$this->app->getrecord("procesos","*"," where id=$id");
		
		if (sizeof($result) > 0) 
		{		
			if($this->app->deleterecord("procesos"," where id=$id")>0){
					$datos[]=array(
						'exito'=>'si'
					);					
			}		
				
		} 
		else{
			
			$datos[]=array(
					'exito'=>'sin'

				);
		}


	return $datos;

	}
	public function saveprocesoedita ($id,$maquina,$proceso,$categoria,$subensamble,$descripcion,$medidaentrada,$piezasforma,$mermaajuste,$estandarproceso,$estandarajuste,$estandarcalculado,$espec1,$espec2,$espec3,$espec4,$espec5,$multiploproceso,$horas,$estandarturno,$activo,$bobina){
		if(!$this->userfunctions->islogged()){
			
		}

		$datos=array();
		$result=$this->app->getrecord("procesos","*"," where id=$id");
		
		if (sizeof($result) > 0) {
			$result3=$this->app->getrecord("bobinas","*","where codigoSAE like '$bobina'");
			//trigger_error("where codigoSAE like '$bobina'");
			if(sizeof($result3)>0){
				$vec1=$result3[0];
				//trigger_error(print_r($vec1,true));
				$idbobina=$vec1['id'];
				$largobobina=$vec1['largo'];	
			}
			if($this->app->updaterecord("procesos","maquina=$maquina,proceso='$proceso',categoria='$categoria',subensamble='$subensamble',descripcionProceso='$descripcion',medidaEntrada='$medidaentrada',piezasFormato='$piezasforma',mermaAjuste='$mermaajuste',estandarProcesoHora='$estandarproceso',estandarAjuste='$estandarajuste',estandarCalculado='$estandarcalculado',especificacion1='$espec1',especificacion2='$espec2',especificacion3='$espec3',especificacion4='$espec4',especificacion5='$espec5',multiploProceso='$multiploproceso',horasTurno=$horas,estandarTurno='$estandarturno',activo='$activo'"," where id=$id")){
					$this->app->deleterecord("rel_procesobobina","where idProceso=$id");
					$this->app->saverecord("rel_procesobobina","null,$id,$idbobina,$largobobina,'1'");
					$datos[]=array(
						'exito'=>'si',
						'folio'=>$id
					);			
			}		
				
		} 
		else{
			
			$datos[]=array(
					'exito'=>'sin',
					'folio'=>0
				);
		}
	return $datos;	
	}
	public function getcomponentes(){
		if(!$this->userfunctions->islogged()){
			
		}

	$datos=array();
		$result=$this->app->getrecord("componentessae","*","where activo like '1'");
		for($a=0;$a<sizeof($result);$a++)
    		{
			$vec=$result[$a];
		
			$datos[] = array(
				'id'=>$vec['id'],
				'clave'=>$vec['clave'],
				'componente'=>$vec['descripcion'],	
				'linea'=>$vec['linea'],
				'unidadEntrada'=>$vec['unidadEntrada'],
				'ultimoCosto'=>$vec['ultimoCosto'],
				'cantidad'=>$vec['cantidad'],
				'paisOrigen'=>$vec['paisOrigen'],
				'merma'=>$vec['merma'],
				'fraccionArancelaria'=>$vec['fraccionArancelaria']
				);
                }		
	
	return $datos;
	}
	public function getallcomponentes(){
		if(!$this->userfunctions->islogged()){
			
		}

	$datos=array();
		$result=$this->app->getrecord("componentessae","*","");
		for($a=0;$a<sizeof($result);$a++)
    		{
			$vec=$result[$a];
		
			$datos[] = array(
				'id'=>$vec['id'],
				'clave'=>$vec['clave'],
				'componente'=>$vec['descripcion'],	
				'linea'=>$vec['linea'],
				'unidadEntrada'=>$vec['unidadEntrada'],
				'ultimoCosto'=>$vec['ultimoCosto'],
				'cantidad'=>$vec['cantidad'],
				'paisOrigen'=>$vec['paisOrigen'],
				'merma'=>$vec['merma'],
				'fraccionArancelaria'=>$vec['fraccionArancelaria'],
				'activo'=>$vec['activo']
				);
                }		
	
	return $datos;
	}
	public function addcomponent($clave,$descripcion,$linea,$unidadentrada,$ultimocosto){
		if(!$this->userfunctions->islogged()){
			
		}

		$datos=array();
		$result=$this->app->getrecord("componentessae","*","where clave like '$clave'");
				
		if (sizeof($result) > 0) {
			$datos[]=array(
				'exito'=>'ya',
				'folio'=>0
			);
		} 
		else 
		{
			$id=$this->app->saverecord('componentessae',"'$clave','$descripcion','$linea','$unidadentrada',$ultimocosto,'1'","(clave,descripcion,linea,unidadEntrada,ultimoCosto,activo)");			
			
						
			if($id>0 ){
				$datos[]=array(
					'exito'=>'si',
					'folio'=>$id
				);	
			}else{
				$datos[]=array(
					'exito'=>'no',
					'folio'=>0
				);	
			}			
		}


		
	return $datos;
	}
	public function deletecomponente($id){
		if(!$this->userfunctions->islogged()){
			
		}

		$datos=array();
		$result=$this->app->getrecord("componentessae","*"," where id=$id");
		
		if (sizeof($result) > 0) 
		{		
			if($this->app->updaterecord("componentessae","activo='0'"," where id=$id")){
					$datos[]=array(
						'exito'=>'si'
					);					
			}		
				
		} 
		else{
			
			$datos[]=array(
					'exito'=>'sin'

				);
		}


	return $datos;

	}
	public function enablecomponente($id){
		if(!$this->userfunctions->islogged()){
			
		}

		$datos=array();
		$result=$this->app->getrecord("componentessae","*"," where id=$id");
		
		if (sizeof($result) > 0) 
		{		
			if($this->app->updaterecord("componentessae","activo='1'"," where id=$id")){
					$datos[]=array(
						'exito'=>'si'
					);					
			}		
				
		} 
		else{
			
			$datos[]=array(
					'exito'=>'sin'

				);
		}


	return $datos;
	}
	public function getspecificcomponente($cad){
		if(!$this->userfunctions->islogged()){
			
		}

	$datos=array();
		//$sae=explode("-",$cad);
		//trigger_error($sae[0]);
		$result=$this->app->getrecord("componentessae","*","where clave like '$cad'");
		for($a=0;$a<sizeof($result);$a++)
    		{
			$vec=$result[$a];
			$datos[] = array(
				'id'=>$vec['id'],
				'clave'=>$vec['clave'],
				'componente'=>$vec['descripcion'],	
				'linea'=>$vec['linea'],
				'unidadEntrada'=>$vec['unidadEntrada'],
				'ultimoCosto'=>$vec['ultimoCosto'],
				'cantidad'=>$vec['cantidad'],
				'paisOrigen'=>$vec['paisOrigen'],
				'merma'=>$vec['merma'],
				'fraccionArancelaria'=>$vec['fraccionArancelaria']
				);
                }
	return $datos;
	}
	public function savecomponente($idproducto,$cantidad,$componente){
		if(!$this->userfunctions->islogged()){
			
		}

		$datos=array();	

		

		$id=$this->app->saverecord("relproducto_componente","null,$idproducto,$componente,$cantidad,'','1'");			
				if($id>0 ){
			
					$datos[]=array(
						'exito'=>'si',
						'folio'=>$id
					);	
				}else{
					$datos[]=array(
						'exito'=>'no',
						'folio'=>0
					);	
				}	
	return $datos;
	
	}
	public function updatecomponent($id,$fechacompra){
		if(!$this->userfunctions->islogged()){
			
		}

	//trigger_error($vendedor);
		$datos=array();
		$result=$this->app->getrecord("rel_componentesorden","*"," where id= $id");
		
		if (sizeof($result) > 0) {
		
			if($this->app->updaterecord("rel_componentesorden","fechaCompra='$fechacompra'"," where id=$id")){
					$datos[]=array(
						'exito'=>'si',
						'folio'=>$id
					);
							
			}	
				
		} 
		else{
			
			$datos[]=array(
					'exito'=>'sin',
					'folio'=>0
				);
		}
	return $datos;	
	}

	public function getcomponentesproducto($idproducto){
		if(!$this->userfunctions->islogged()){
			
		}

	$datos=array();
		
		$result=$this->app->getrecord("relproducto_componente","relproducto_componente.id,relproducto_componente.idComponente,componentessae.clave,componentessae.descripcion,componentessae.linea,componentessae.unidadEntrada,componentessae.ultimoCosto,relproducto_componente.cantidad","INNER JOIN componentessae on relproducto_componente.idComponente=componentessae.id WHERE relproducto_componente.idProducto=$idproducto");
		for($a=0;$a<sizeof($result);$a++)
    		{
			$vec=$result[$a];
			$datos[] = array(
				'id'=>$vec['id'],
				'idcomponente'=>$vec['idComponente'],
				'clave'=>$vec['clave'],
				'componente'=>$vec['descripcion'],	
				'linea'=>$vec['linea'],
				'unidadEntrada'=>$vec['unidadEntrada'],
				'ultimoCosto'=>$vec['ultimoCosto'],
				'cantidad'=>$vec['cantidad']
				);
                }
	return $datos;
	}

	public function deleterecordcomponente ($id){
		if(!$this->userfunctions->islogged()){
			
		}

		$datos=array();
		$result=$this->app->getrecord("relproducto_componente","*"," where id=$id");
		
		if (sizeof($result) > 0) 
		{		
			if($this->app->deleterecord("relproducto_componente"," where id=$id")>0){
					$datos[]=array(
						'exito'=>'si'
					);					
			}		
				
		} 
		else{
			
			$datos[]=array(
					'exito'=>'sin'

				);
		}


	return $datos;

	}

	public function savecomponenteedita($id,$cantidad,$idcomponente){
		if(!$this->userfunctions->islogged()){
			
		}

		$datos=array();
		$result=$this->app->getrecord("relproducto_componente","*"," where id=$id");
		
		if (sizeof($result) > 0) {
		
			if($this->app->updaterecord("relproducto_componente","cantidad=$cantidad,idComponente=$idcomponente"," where id=$id")){
					$datos[]=array(
						'exito'=>'si',
						'folio'=>$id
					);			
			}		
				
		} 
		else{
			
			$datos[]=array(
					'exito'=>'sin',
					'folio'=>0
				);
		}
	return $datos;	
	} 
	

	public function getnumberespecificaionpt($idproducto){
		if(!$this->userfunctions->islogged()){
			
		}

		$datos=array();	

		
		$result=$this->app->getrecord("especificacionespt","COUNT(id) as 'total'","WHERE idProducto=$idproducto ");
		for($a=0;$a<sizeof($result);$a++)
    		{
			$vec=$result[$a];
			$datos[] = array(
	
				'total'=>$vec['total']

				);
                }
	return $datos;
	
	}


	public function saveespecificaionpt($idproducto,$especificacion){
		if(!$this->userfunctions->islogged()){
			
		}

		$datos=array();	

		

		$id=$this->app->saverecord("especificacionespt","$idproducto,'$especificacion','1'","(idProducto,nombre,activo)");			
				if($id>0 ){
			
					$datos[]=array(
						'exito'=>'si',
						'folio'=>$id
					);	
				}else{
					$datos[]=array(
						'exito'=>'no',
						'folio'=>0
					);	
				}	
	return $datos;
	
	}

	public function getespecificacionesptproducto($idproducto){
		if(!$this->userfunctions->islogged()){
			
		}

	$datos=array();
		
		$result=$this->app->getrecord("especificacionespt","*","WHERE idProducto=$idproducto ");
		for($a=0;$a<sizeof($result);$a++)
    		{
			$vec=$result[$a];
			$datos[] = array(
				'id'=>$vec['id'],
				'nombre'=>$vec['nombre'],
				'extra'=>$vec['extra'],	
				'activo'=>$vec['activo']
				);
                }
	return $datos;

	}
	public function saveespecificacionptedita($id,$esppt,$activo){

		if(!$this->userfunctions->islogged()){
			
		}

		$datos=array();
		$result=$this->app->getrecord("especificacionespt","*"," where id=$id");
		
		if (sizeof($result) > 0) {
		
			if($this->app->updaterecord("especificacionespt","nombre='$esppt',activo='$activo'"," where id=$id")){
					$datos[]=array(
						'exito'=>'si',
						'folio'=>$id
					);			
			}		
				
		} 
		else{
			
			$datos[]=array(
					'exito'=>'sin',
					'folio'=>0
				);
		}
	return $datos;	
	}
	public function deleterecordespecificacionpt ($id){
		if(!$this->userfunctions->islogged()){
			
		}

		$datos=array();
		$result=$this->app->getrecord("especificacionespt","*"," where id=$id");
		
		if (sizeof($result) > 0) 
		{		
			if($this->app->deleterecord("especificacionespt"," where id=$id")>0){
					$datos[]=array(
						'exito'=>'si'
					);					
			}		
				
		} 
		else{
			
			$datos[]=array(
					'exito'=>'sin'

				);
		}


	return $datos;

	}
	public function calculatemermause($vec,$cantidad){
		if(!$this->userfunctions->islogged()){
			
		}

		$merma=0;
		$piezas=array();	
		//trigger_error(print_r($vec,true));

		if(sizeof($vec)>0){
			//$id=$vec[0]['id'];
			//$result=$this->app->getrecord("rel_procesobobina","*"," where id=$id");
			
			//if (sizeof($result) > 0) {
				

			//}
			
			for($b=0;$b<sizeof($vec);$b++){
				array_push ( $piezas , $vec[$b]['piezasformato'] );
			}
			$piezasmaximas=max($piezas);

			/*for($a=0;$a<sizeof($vec);$a++){
				for($b=1;$b<=$vec[$a]['multiploproceso'];$b++){
				
					$vecmaquina=$this->getmaquinadetail($vec[$a]['maquinaid']);

					$estandar=$vec[$a]['estandarturno'];

					$cant=ceil($cantidad/$piezasmaximas);

					$mermaproceso=ceil($cant*$vecmaquina[0]['mermaproceso']);
						
	
					//$merma2=$vecmaquina[0]['mermafija']+$mermaproceso;
						
					$merma+=$mermaproceso;
				}
			}*/
			$cant=ceil($cantidad/$piezasmaximas);
			$merma=ceil($cant*.05);

		}

	//trigger_error($this->mermageneral."/".$merma);
	return $this->mermageneral+$merma;		
	}
	

	public function clientesandcodigos(){
		if(!$this->userfunctions->islogged()){
			
		}

		$datos=array();
		$datos1=array();
		$datos2=array();
		$result=$this->app->getrecord("ordenmaestra"," DISTINCT(ordenmaestra.cliente) as 'cliente'","");
		
		
		for($a=0;$a<sizeof($result);$a++)
    		{
			$vec=$result[$a];
			$datos1[] = array(
				
				'cliente'=>$vec['cliente']
				
				);
                }
		$result=$this->app->getrecord("ordenmaestra","  DISTINCT(ordenmaestra.codigoClienteproducto) as 'codigo'","");
		
		
		for($a=0;$a<sizeof($result);$a++)
    		{
			$vec=$result[$a];
			$datos2[] = array(
				
				
				'codigo'=>$vec['codigo']	
				);
                }
		$datos[] = array(
				
				'clientes'=>$datos1,
				'codigos'=>$datos2	
				);
	return $datos;

	}
	public function getdataforqualitycertificate($idorden){
		if(!$this->userfunctions->islogged()){
			
		}

		$datos=array();
		$datos1=array();
		
		$result=$this->app->getrecord("ordenmaestra","ordenmaestra.id as 'numorden', ordenmaestra.cliente,ordenmaestra.codigoClienteproducto as 'codigocliente',ordenmaestra.ordenClientecompra,ordenmaestra.pedidoSAE,ordenmaestra.cantidad,productos.id as 'idproducto',productos.documento1 as 'material' , productos.codigoProducto as 'codigosae',productos.nombreProducto,productos.anchoProducto,productos.largoProducto,productos.altoProducto,productos.descripcion ","  INNER JOIN productos on productos.id=ordenmaestra.idProducto where ordenmaestra.id=$idorden");
		
		if(sizeof($result)>0){

				
			
				

			$vec=$result[0];
			$idproducto=$vec['idproducto'];
			$result4=$this->app->getrecord("medidas_producto","*"," where idProducto=$idproducto");
			$vecmedidas=$result4[0];
			$result2=$this->app->getrecord("especificacionespt"," *"," where idProducto=$idproducto");
			$result6=$this->app->getrecord("medidas_productocertificadocalidad","*"," where idProducto=$idproducto");
			$vecmedidascalidad=$result6[0];

			for($b=0;$b<sizeof($result2);$b++)
    			{
				$vec2=$result2[$b];
				$datos1[] = array(
				
					'especificacion'=>$vec2['nombre']
				
				);
                	}

				
				
			
			for($a=0;$a<sizeof($result);$a++)
    			{
				$vec=$result[$a];
				$datos[] = array(
					'cliente'=>$vec['cliente'],
					'codigocliente'=>$vec['codigocliente'],
					'orden'=>$vec['ordenClientecompra'],
					'pedidosae'=>$vec['pedidoSAE'],
					'cantidad'=>$vec['cantidad'],
					'idproducto'=>$vec['idproducto'],
					'material'=>$vec['material'],
					'codigosae'=>$vec['codigosae'],
					'numorden'=>$vec['numorden'],
					'nombreproducto'=>$vec['nombreProducto'],
					'ancho'=>$vecmedidas['ancho'],
					'largo'=>$vecmedidas['largo'],
					'alto'=>$vecmedidas['alto'],
					'a'=>$vecmedidascalidad['ancho'],
					'b'=>$vecmedidascalidad['largo'],
					'h'=>$vecmedidascalidad['alto'],
					'descripcion'=>$vec['descripcion'],
					'especificaciones'=>$datos1			
				);
                	}
		}
		
		
	return $datos;

	}
	public function addperiodtodate($fecha,$dias){
		$fecha2=$fecha;
		$fechats = strtotime($fecha);
		switch(date('w',$fechats)){
			case 0:
				if($dias==7){
					$fecha2 = strtotime ( '+9 day' , strtotime ( $fecha2 ) ) ;
				}
			break;
			case 1:
				if($dias==7){
					$fecha2 = strtotime ( '+9 day' , strtotime ( $fecha2 ) ) ;
				}
			break;
			case 2:
				if($dias==7){
					$fecha2 = strtotime ( '+9 day' , strtotime ( $fecha2 ) ) ;
				}
			break;
			case 3:
				if($dias==7){
					$fecha2 = strtotime ( '+9 day' , strtotime ( $fecha2 ) ) ;
				}
			break;
			case 4:
				if($dias==7){
					$fecha2 = strtotime ( '+11 day' , strtotime ( $fecha2 ) ) ;
				}
			break;
			case 5:
				if($dias==7){
					$fecha2 = strtotime ( '+11 day' , strtotime ( $fecha2 ) ) ;
				}
			break;
			case 6:
				if($dias==7){
					$fecha2 = strtotime ( '+10 day' , strtotime ( $fecha2 ) ) ;
				}
			break;
		}
	return $fecha2;
	}
	public function saveorden($pedidosae,$fechaemision,$fecharequerida,$producto,$cantidad,$cliente,$codigocliente,$ordencompracliente){
		if(!$this->userfunctions->islogged()){
			
		}
			 date_default_timezone_set ("America/Mexico_City");
			$hoy2 = date("Y-m-d");
			$fechavence=date ( 'Y-m-d' , $this->addperiodtodate($hoy2 ,7));
			//trigger_error($fechavence);
	$datos=array();	
	$piezas=array();		
		$vec=$this->getspecificproductbysae($producto);
		
		$idd=$vec[0]['id'];
		//trigger_error(print_r($vec,true));
		$id=$this->app->saverecord("ordenmaestra","'$pedidosae',$idd,now(),'$fecharequerida',$cantidad,$cantidad,0,5,'0','1','$cliente','$codigocliente','$ordencompracliente'","(pedidoSAE,idProducto,fechaEmision,fechaRequerida,cantidad,faltante,producido,idStatus,msg,activo,cliente,codigoClienteproducto,ordenClientecompra)");			
				if($id>0 ){
					$this->app->saverecord("pedidos_control","$id,now(),'$fechavence','1'","(idOrden,fechaelaboracion,fechainsumos,activo)");			
					$vec2=$this->getprocesosproductoenables($idd);
					
					$cont=0;
					$mermatotal=$this->calculatemermause($vec2,$cantidad);
					//trigger_error("mermatotal:".$mermatotal);
					for($b=0;$b<sizeof($vec2);$b++){
						array_push ( $piezas , $vec2[$b]['piezasformato'] );
					}
					if(sizeof($piezas)>0){
						$piezasmaximas=max($piezas);
					}
					else{
						array_push ( $piezas , 1);
						$piezasmaximas=max($piezas);
					}

					$mermaprocesos=0;


					for($a=0;$a<sizeof($vec2);$a++){
					for($b=1;$b<=$vec2[$a]['multiploproceso'];$b++){

						
						$vecmaquina=$this->getmaquinadetail($vec2[$a]['maquinaid']);

						$idproceso=$vec2[$a]['id'];
						$estandar=$vec2[$a]['estandarturno'];
						$cant=ceil($cantidad/$piezasmaximas);
						$cat2=ceil($cant*$vecmaquina[0]['mermaproceso']);
						

						

						//trigger_error("hhh".$cant);
						//trigger_error($vec2[$a]['maquinaid']);
						//trigger_error(print_r($vecmaquina,true));
						
	
						//$cantconmerma=$cant+$mermatotal;
						$cantconmerma=$cant;
						//$mermaproceso=$vec2[0]['mermafija']+$cat2;
						$mermaproceso=0;
						

						
						//$numberoftickets=$this->numberoftickets($cantconmerma,$estandar,$mermaprocesos,$mermaproceso);	
						
						$tiempoajuste=$vecmaquina[0]['tiempoajuste'];
						

										if($vecmaquina[0]['umsalida']=='Pieza' && $vecmaquina[0]['umentrada']=='Pieza'){
											//$cantevaluar=(($cantconmerma-$mermaprocesos)*$piezasmaximas);
											$cantevaluar=$cant*$piezasmaximas;	
											//trigger_error("pieza if evaluar".$cantevaluar." cantconmerma".$cantconmerma." mermaprocesos".$mermaprocesos);
	
										}else{
											
											$cantevaluar=$cant;
											//$cantevaluar=$cantconmerma-$mermaprocesos;
											//trigger_error("pieza else".$cantevaluar);
	
										}
							$cantproceso=$cantevaluar;
						if($cantevaluar>$estandar){

							while($cantevaluar>0){

											//trigger_error("mermaproceso->".$mermaproceso);
	
								if($mermaproceso>0 ){
									
									$cont++;
									if($cantevaluar>0 && $cantevaluar<=$estandar){
										$proporcionmerma=round(($cantevaluar*$mermaproceso)/$cantproceso);
										//trigger_error(" if estandar proporcionmerma->".$proporcionmerma." canevaluar->".$cantevaluar." mermaproceso->".$mermaproceso." cantproceso->".$cantproceso);
										
											$cantfinalcalculada=($cantevaluar-$proporcionmerma);
										
										//$id12=$this->app->saverecord("rel_procesosorden","null,$id,$idproceso,2,'$id-$cont',$proporcionmerma,$cantevaluar,0,$cantfinalcalculada,0,0,$tiempoajuste,0,0,0,5,'1'");
										$id12=$this->app->saverecord("rel_procesosorden","$id,$idproceso,2,'$id-$cont',$mermatotal,$cantevaluar,0,$cantfinalcalculada,0,0,$tiempoajuste,0,0,0,5,'1'","(idOrden,idProceso,idOperador,folio,mermaEstimada,cantidad,cantidadInicialCalculada,cantidadFinalCalculada,cantidadInicialPlaneador,cantidadFinalPlaneador,tiempoAjuste,cantidadTotal,cantidadUtil,merma,idStatus,activo)");
										$cantevaluar-=$estandar;
									}else{	
										$proporcionmerma=round(($estandar*$mermaproceso)/$cantproceso);

												
										$cantevaluar-=$estandar;
										
											$cantfinalcalculada=($estandar-$proporcionmerma);
										
										//$id12=$this->app->saverecord("rel_procesosorden","null,$id,$idproceso,2,'$id-$cont',$proporcionmerma,$estandar,0,$cantfinalcalculada,0,0,$tiempoajuste,'','','','','',0,0,0,'','','','',5,'1'");
										$id12=$this->app->saverecord("rel_procesosorden","$id,$idproceso,2,'$id-$cont',$mermatotal,$estandar,0,$cantfinalcalculada,0,0,$tiempoajuste,0,0,0,5,'1'","(idOrden,idProceso,idOperador,folio,mermaEstimada,cantidad,cantidadInicialCalculada,cantidadFinalCalculada,cantidadInicialPlaneador,cantidadFinalPlaneador,tiempoAjuste,cantidadTotal,cantidadUtil,merma,idStatus,activo)");
									}
										
										
								}else{
									$cont++;
									if($cantevaluar>0 && $cantevaluar<=$estandar){
										$cantfinalcalculada=$cantevaluar;
										//$id12=$this->app->saverecord("rel_procesosorden","null,$id,$idproceso,2,'$id-$cont',0,$cantevaluar,0,$cantfinalcalculada,0,0,$tiempoajuste,'','','','','',0,0,0,'','','','',5,'1'");
										$id12=$this->app->saverecord("rel_procesosorden","$id,$idproceso,2,'$id-$cont',$mermatotal,$cantevaluar,0,$cantfinalcalculada,0,0,$tiempoajuste,0,0,0,5,'1'","(idOrden,idProceso,idOperador,folio,mermaEstimada,cantidad,cantidadInicialCalculada,cantidadFinalCalculada,cantidadInicialPlaneador,cantidadFinalPlaneador,tiempoAjuste,cantidadTotal,cantidadUtil,merma,idStatus,activo)");
										$cantevaluar-=$estandar;
									}else{	
										$cantevaluar-=$estandar;
										$cantfinalcalculada=$estandar;
										//$id12=$this->app->saverecord("rel_procesosorden","null,$id,$idproceso,2,'$id-$cont',0,$estandar,0,$cantfinalcalculada,0,0,$tiempoajuste,'','','','','',0,0,0,'','','','',5,'1'");
										$id12=$this->app->saverecord("rel_procesosorden","$id,$idproceso,2,'$id-$cont',$mermatotal,$estandar,0,$cantfinalcalculada,0,0,$tiempoajuste,0,0,0,5,'1'","(idOrden,idProceso,idOperador,folio,mermaEstimada,cantidad,cantidadInicialCalculada,cantidadFinalCalculada,cantidadInicialPlaneador,cantidadFinalPlaneador,tiempoAjuste,cantidadTotal,cantidadUtil,merma,idStatus,activo)");
									}


								}
								
									
							}
							
						}else{
						//trigger_error("maquina->".$vecmaquina[0]['nombre']."umsalida->".$vecmaquina[0]['umsalida']." umentrada->".$vecmaquina[0]['umentrada']." piezasformato->".$vec2[$a]['piezasformato']);

										if($vecmaquina[0]['umsalida']=='Pieza' && $vecmaquina[0]['umentrada']=='Pieza'){
											$piezaformatoproceso=$piezasmaximas;
										}else{
											$piezaformatoproceso=1;										
	
										}
						//trigger_error("piezaformatoproceso->".$piezaformatoproceso." piezasmaximas->".$piezasmaximas);


							$cont++;
							if($mermaproceso>0){
								
									//$cantfinalcalculada=($cantevaluar-($mermaproceso*$piezaformatoproceso));
									$cantfinalcalculada=$cantevaluar*$piezaformatoproceso;
								
								//$id12=$this->app->saverecord("rel_procesosorden","null,$id,$idproceso,2,'$id-$cont',$mermaproceso,$cantevaluar,0,$cantfinalcalculada,0,0,$tiempoajuste,'','','','','',0,0,0,'','','','',5,'1'");
								$id12=$this->app->saverecord("rel_procesosorden","$id,$idproceso,2,'$id-$cont',$mermatotal,$cantevaluar,0,$cantfinalcalculada,0,0,$tiempoajuste,0,0,0,5,'1'","(idOrden,idProceso,idOperador,folio,mermaEstimada,cantidad,cantidadInicialCalculada,cantidadFinalCalculada,cantidadInicialPlaneador,cantidadFinalPlaneador,tiempoAjuste,cantidadTotal,cantidadUtil,merma,idStatus,activo)");
							}else{
								$cantfinalcalculada=$cantevaluar;
								//$id12=$this->app->saverecord("rel_procesosorden","null,$id,$idproceso,2,'$id-$cont',0,$cantevaluar,0,$cantfinalcalculada,0,0,$tiempoajuste,'','','','','',0,0,0,'','','','',5,'1'");
								$id12=$this->app->saverecord("rel_procesosorden","$id,$idproceso,2,'$id-$cont',$mermatotal,$cantfinalcalculada,0,$cantfinalcalculada,0,0,$tiempoajuste,0,0,0,5,'1'","(idOrden,idProceso,idOperador,folio,mermaEstimada,cantidad,cantidadInicialCalculada,cantidadFinalCalculada,cantidadInicialPlaneador,cantidadFinalPlaneador,tiempoAjuste,cantidadTotal,cantidadUtil,merma,idStatus,activo)");
							}
						}


						
					$mermaprocesos+=$mermaproceso;
						
					}
					}


					$vec2=$this->getcomponentesproducto($idd);
		
					for($a=0;$a<sizeof($vec2);$a++){
						$idcomponente=$vec2[$a]['id'];
						$cantidadorden=$cantidad*$vec2[$a]['cantidad'];
						$total=$cantidadorden*$vec2[$a]['ultimoCosto'];
						$id12=$this->app->saverecord("rel_componentesorden","$id,$idcomponente,$cantidadorden,$total,'1'","(idOrden,idrelacionComponente,cantidadOrden,costoTotal,activo)");	
					}

					$datos[]=array(
						'exito'=>'si',
						'folio'=>$id
					);	
				}else{
					$datos[]=array(
						'exito'=>'no',
						'folio'=>0
					);	
				}	
	return $datos;
	}

	public function getallorders(){
		if(!$this->userfunctions->islogged()){
			
		}

	$datos=array();

	$result=$this->app->getrecord("ordenmaestra","ordenmaestra.cliente,ordenmaestra.idStatus,ordenmaestra.id,ordenmaestra.pedidoSAE,productos.nombreProducto,ordenmaestra.fechaProduccion,ordenmaestra.fechaEmision,ordenmaestra.fechaRequerida,ordenmaestra.cantidad,ordenmaestra.faltante,ordenmaestra.producido,status_orden.status","INNER JOIN productos on productos.id=ordenmaestra.idProducto INNER JOIN status_orden on status_orden.id=ordenmaestra.idStatus where ordenmaestra.activo like '1' and (ordenmaestra.idStatus=3 or ordenmaestra.idStatus=5 or ordenmaestra.idStatus=7) order by ordenmaestra.fechaEmision desc");
		for($a=0;$a<sizeof($result);$a++)
    		{	
			$vec=$result[$a];
			$color="todasbn";

			if($vec['idStatus']==3){
				$color='faltan';
			}else if($vec['idStatus']==5){
				$color='faltaingenieria';
			}else if($vec['idStatus']==7){
				$color='faltacalidad';
			}else{
				$color='todasbn';
			}
			
			$datos[] = array(
				'id'=>$vec['id'],
				'producto'=>$vec['nombreProducto'],
				'fechaemision'=>$vec['fechaEmision'],	
				'fecharequerida'=>$vec['fechaRequerida'],
				'fechaproduccion'=>$vec['fechaProduccion'],
				'cantidad'=>$vec['cantidad'],
				'cliente'=>$vec['cliente'],
				'faltante'=>$vec['faltante'],
				'producido'=>$vec['producido'],
				'status'=>$vec['status'],
				'pedidosae'=>$vec['pedidoSAE'],
				'activo'=>$vec['activo'],
				'color'=>$color
				);
                }
	
	return $datos;
	}
	public function getspecificorder($id){
		if(!$this->userfunctions->islogged()){
			
		}

	$datos=array();

	$result=$this->app->getrecord("ordenmaestra","ordenmaestra.id,ordenmaestra.pedidoSAE,ordenmaestra.idProducto,ordenmaestra.fechaProduccion,productos.nombreProducto,productos.descripcion,ordenmaestra.fechaEmision,ordenmaestra.fechaRequerida,ordenmaestra.cantidad,ordenmaestra.faltante,ordenmaestra.producido,status_orden.status","INNER JOIN productos on productos.id=ordenmaestra.idProducto INNER JOIN status_orden on status_orden.id=ordenmaestra.idStatus where ordenmaestra.activo like '1' and ordenmaestra.id=$id");
		for($a=0;$a<sizeof($result);$a++)
    		{
			$vec=$result[$a];
			$datos[] = array(
				'id'=>$vec['id'],
				'idproducto'=>$vec['idProducto'],
				'producto'=>$vec['nombreProducto'],
				'fechaemision'=>$vec['fechaEmision'],	
				'fecharequerida'=>$vec['fechaRequerida'],
				'fechaproduccion'=>$vec['fechaProduccion'],
				'descripcion'=>$vec['descripcion'],
				'cantidad'=>$vec['cantidad'],
				'faltante'=>$vec['faltante'],
				'producido'=>$vec['producido'],
				'status'=>$vec['status'],
				'pedidosae'=>$vec['pedidoSAE'],
				'activo'=>$vec['activo']
				);
                }
	
	return $datos;
	}

	public function getordersbyoperator($idoperator){
		if(!$this->userfunctions->islogged()){
			
		}

	
	$datos=array();
	//trigger_error($idoperator);
	$result=$this->app->getrecord("rel_procesosorden","DISTINCT idOrden"," where idOperador=$idoperator and fechaRequerida like CURDATE() and idStatus!=6");
	 //trigger_error(print_r($result,true));
		for($a=0;$a<sizeof($result);$a++)
    		{
			$vec=$result[$a];
			$idorden=$vec['idOrden'];
			
			$datos[] = array(
				$this->getspecificorder($idorden)
				);
                }
	
	return $datos;
	}	

	public function getordersbycliente($fechainicio,$fechafin,$cliente){
		if(!$this->userfunctions->islogged()){
			
		}

	$datos=array();
		$result=$this->app->getrecord("ordenmaestra","ordenmaestra.cliente,ordenmaestra.fechaProduccion,ordenmaestra.id,ordenmaestra.pedidoSAE,productos.codigoProducto,productos.nombreProducto,ordenmaestra.cantidad,ordenmaestra.fechaEmision,ordenmaestra.fechaRequerida","INNER JOIN productos on productos.id=ordenmaestra.idProducto where ordenmaestra.cliente like '$cliente' and ordenmaestra.fechaEmision BETWEEN '$fechainicio 00:00:00' and '$fechafin 23:59:00' order by ordenmaestra.fechaEmision DESC ");
		
		for($a=0;$a<sizeof($result);$a++)
    		{
			$vec=$result[$a];
			
			
			$datos[] = array(
				'id'=>$vec['id'],
				'orden'=>$vec['idOrden'],
				'pedidosae'=>$vec['pedidoSAE'],
				'codigoproducto'=>$vec['codigoProducto'],
				'nombreproducto'=>$vec['nombreProducto'],
				'cantidad'=>$vec['cantidad'],
				'fechaemision'=>$vec['fechaEmision'],
				'fecharequerida'=>$vec['fechaRequerida'],
				'fechaproduccion'=>$vec['fechaProduccion'],
				'cliente'=>$vec['cliente']
				
				);
                }


	return $datos;

	}

	public function getticketsbymachine($idmaquina){
		if(!$this->userfunctions->islogged()){
			
		}

	
	$datos=array();
	//trigger_error($idoperator);
	$result=$this->app->getrecord("rel_procesosorden","maquinas.unidadSalida,rel_procesosorden.idProceso,rel_procesosorden.id,rel_procesosorden.idOperador,rel_procesosorden.fechaInicio,rel_procesosorden.horaInicio,rel_procesosorden.fechaTermino,rel_procesosorden.horaTermino,rel_procesosorden.idOrden,rel_procesosorden.folio,rel_procesosorden.cantidad,rel_procesosorden.cantidadUtil,rel_procesosorden.merma,rel_procesosorden.fechaRequerida,rel_procesosorden.horaRequerida","inner join procesos on procesos.id=rel_procesosorden.idProceso inner Join maquinas on maquinas.id=procesos.maquina where procesos.maquina=$idmaquina and (rel_procesosorden.idStatus=3 or rel_procesosorden.idStatus=7) and rel_procesosorden.fechaTermino='NO_ZERO_DATE'");
	 //trigger_error(print_r($result,true));
		for($a=0;$a<sizeof($result);$a++)
    		{
			$vec=$result[$a];
			$idoperador=$vec['idOperador'];
			$result2=$this->app->getrecord("operadores","CONCAT(persona.nombre,' ',persona.primerApellido,' ',persona.segundoApellido) as 'ope',operadores.id","inner join persona on persona.id=operadores.idPersona where operadores.activo like '1' and operadores.id=$idoperador");
			$vec2=$result2[0];
			//trigger_error("CONCAT(persona.nombre,' ',persona.primerApellido,' ',persona.segundoApellido) as 'ope',operadores.id"."inner join persona on persona.id=operadores.idPersona where operadores.activo like '1' and operadores.id=$idoperador");
			$datos[] = array(
				'id'=>$vec['id'],
				'idorden'=>$vec['idOrden'],
				'operador'=>$vec2['ope'],
				'folio'=>$vec['folio'],
				'cantidad'=>$vec['cantidad'].' '.$vec['unidadSalida'].'(s)',
				'cantidadutil'=>$vec['cantidadUtil'],
				'merma'=>$vec['merma'],
				'fecha'=>$vec['fechaRequerida'],
				'hora'=>$vec['horaRequerida'],
				'fechainicio'=>$vec['fechaInicio'],
				'horainicio'=>$vec['horaInicio'],
				'fechatermino'=>$vec['fechaTermino'],
				'horatermino'=>$vec['horaTermino'],
				'proceso'=>$vec['idProceso']
				);
                }
	
	return $datos;
	}
	public function getspecificticketbymachine($id){
		if(!$this->userfunctions->islogged()){
			
		}

	
	$datos=array();
	
	$result=$this->app->getrecord("rel_procesosorden","*","where id=$id");
	
		for($a=0;$a<sizeof($result);$a++)
    		{
			$vec=$result[$a];
			$idoperador=$vec['idOperador'];
			$result2=$this->app->getrecord("operadores","CONCAT(persona.nombre,' ',persona.primerApellido,' ',persona.segundoApellido) as 'nombre',operadores.id","inner join persona on persona.id=operadores.idPersona where operadores.activo like '1' and operadores.id=$idoperador order by persona.nombre ASC");
			$vec2=$result2[0];
			$datos[] = array(
				'id'=>$vec['id'],
				'idorden'=>$vec['idOrden'],
				'operador'=>$vec2['nombre'],
				'folio'=>$vec['folio'],
				'cantidad'=>$vec['cantidad'],
				'cantidadutil'=>$vec['cantidadUtil'],
				'merma'=>$vec['merma'],
				'fecha'=>$vec['fechaRequerida'],
				'hora'=>$vec['horaRequerida'],
				'fechainicio'=>$vec['fechaInicio'],
				'horainicio'=>$vec['horaInicio'],
				'fechatermino'=>$vec['fechaTermino'],
				'horatermino'=>$vec['horaTermino'],
				);
                }
	
	return $datos;
	}
	public function openticket ($id,$operador,$proceso){
		
		 date_default_timezone_set ("America/Mexico_City");
		if(!$this->userfunctions->islogged()){
			
		}

	$datos=array();
		$result=$this->app->getrecord("rel_procesosorden","*"," where id=$id");
		
		if (sizeof($result) > 0) {
			//$result3=$this->app->getrecord("rel_procesosorden","*"," where (idOperador=$operador and fechaTermino like '0000-00-00') or (fechaInicio like CURDATE() and fechaTermino like '0000-00-00' and idProceso=$proceso)");
			$result3=$this->app->getrecord("rel_procesosorden","*"," where  fechaInicio like CURDATE() and fechaTermino like '0000-00-00' and idProceso=$proceso");
			if (sizeof($result3) > 0) {
				$datos[]=array(
							'exito'=>'abierto',
							'folio'=>$id
						);
			}else{

				$vec=$result[0];
				$folio=$vec['folio'];
				$hoy = date("Y-m-d H:i:s"); 
				$fec=explode(' ',$hoy);
				if($this->app->updaterecord("rel_procesosorden","idStatus=7,fechaInicio='$fec[0]',horaInicio='$fec[1]',idOperador=$operador","where id=$id")){
				
					$this->app->updaterecord("citas","idOperador=$operador","where folio like '$folio'");

						$datos[]=array(
							'exito'=>'si',
							'folio'=>$id
						);		
				}		
			}	
		} 
		else{
			
			$datos[]=array(
					'exito'=>'sin',
					'folio'=>0
				);
		}

	return $datos;	
	}
	public function closeticket ($id,$cantidadutil,$merma){
		
		 date_default_timezone_set ("America/Mexico_City");
		if(!$this->userfunctions->islogged()){
			
		}

	$datos=array();
		$result=$this->app->getrecord("rel_procesosorden","*"," where id=$id");
		
		if (sizeof($result) > 0) {
			
			$hoy = date("Y-m-d H:i:s"); 
			$fec=explode(' ',$hoy);
			if($this->app->updaterecord("rel_procesosorden","idStatus=6, fechaTermino='$fec[0]',horaTermino='$fec[1]',cantidadUtil=$cantidadutil,merma=$merma","where id=$id")){
				
				

					$datos[]=array(
						'exito'=>'si',
						'folio'=>$id
					);		
			}		
				
		} 
		else{
			
			$datos[]=array(
					'exito'=>'sin',
					'folio'=>0
				);
		}

	return $datos;	
	}	
	public function getstatus(){
		if(!$this->userfunctions->islogged()){
			
		}

	$datos=array();

	$result=$this->app->getrecord("status_orden","*","where activo like '1' order by status ASC");
		for($a=0;$a<sizeof($result);$a++)
    		{
			$vec=$result[$a];
			$datos[] = array(
				'id'=>$vec['id'],
				'status'=>$vec['status'],
				'extra'=>$vec['extra'],	
				'activo'=>$vec['activo']
				);
                }
	
	return $datos;	
	}
	public function getstatusbyid($id){
		if(!$this->userfunctions->islogged()){
			
		}

	$datos=array();

	$result=$this->app->getrecord("status_orden","*","where activo like '1' and id=$id order by status asc");
		for($a=0;$a<sizeof($result);$a++)
    		{
			$vec=$result[$a];
			$datos[] = array(
				'id'=>$vec['id'],
				'status'=>$vec['status'],
				'extra'=>$vec['extra'],	
				'activo'=>$vec['activo']
				);
                }
	
	return $datos;	
	}
	public function generateticket($id,$cantidad,$cantidadutil){
		if(!$this->userfunctions->islogged()){
			
		}

	$datos=array();

		$result=$this->app->getrecord("rel_procesosorden","*","where id=$id");
		if(sizeof($result)>0)
    		{
			$vec=$result[0];
			
			
			
			$idorden=$vec['idOrden'];
			$idproceso=$vec['idProceso'];
			$mermaestimada=$vec['mermaEstimada'];
			$can=$cantidad-$cantidadutil;
			$tiempoajuste=$vec['tiempoAjuste'];

			$result2=$this->app->getrecord("rel_procesosorden","COUNT(rel_procesosorden.folio) as 'total' ","where idOrden=$idorden");
			$vec2=$result2[0];
			
			$folio=$idorden."-".($vec2['total']+1);
			//trigger_error($id.$vec2['total']);
			$id=$this->app->saverecord("rel_procesosorden","null,$idorden,$idproceso,2,'$folio',$mermaestimada,$can,0,$can,0,0,$tiempoajuste,now(),'','','','',0,0,0,'','','','',5,'1'");
			if($id>0 ){
				$datos[]=array(
					'exito'=>'si',
					'folio'=>$id
				);	
			}else{
				$datos[]=array(
					'exito'=>'no',
					'folio'=>0
				);	
			}		
		}

	return $datos;
	
	}
	public function updateorder ($id,$fecharequerida,$fechaproduccion,$cantidad,$status){
		if(!$this->userfunctions->islogged()){
			
		}

	$datos=array();
		$result=$this->app->getrecord("ordenmaestra","*"," where id=$id");
		
		if (sizeof($result) > 0) {
			$vec=$result[0];
			$producido=$vec['producido'];

			

			if($this->app->updaterecord("ordenmaestra"," fechaProduccion='$fechaproduccion',fechaRequerida='$fecharequerida',cantidad=$cantidad,faltante=$cantidad-$producido,idStatus=$status","where id=$id")){
				
				if($status==2 || $status==4 || $status==6){
					$this->app->updaterecord("ordenmaestra"," fechaFin=now()","where id=$id");
					$this->app->updaterecord("pedidos_control","activo='0'","where idOrden=$id");
				}
				
				$this->app->updaterecord("citas","idStatuscitas=$status","where idOrden=$id");
				$result3=$this->app->getrecord("rel_componentesorden","rel_componentesorden.id,relproducto_componente.cantidad,componentessae.ultimoCosto","  inner join relproducto_componente on relproducto_componente.id=rel_componentesorden.idrelacionComponente INNER join componentessae on componentessae.id=relproducto_componente.idComponente where rel_componentesorden.idOrden=$id");
				//trigger_error(print_r($result3,true));
				for($r=0;$r<sizeof($result3);$r++){
					$vec4=$result3[$r];
					$idmodificar=$vec4['id'];

					$cantidadunitaria=$vec4['cantidad'];
					$cantidadtotal=$cantidadunitaria*$cantidad;
					$costo=$cantidadtotal*$vec4['ultimoCosto'];
					//trigger_error('unitario->'.$cantidadunitaria.'total->'.$cantidadtotal.'costo total->'.$costo);
					$this->app->updaterecord("rel_componentesorden"," cantidadOrden=$cantidadtotal,costoTotal=$costo","where id=$idmodificar");			

					
				}

					$datos[]=array(
						'exito'=>'si',
						'folio'=>$id
					);		
			}		
				
		} 
		else{
			
			$datos[]=array(
					'exito'=>'sin',
					'folio'=>0
				);
		}

	return $datos;	
	}
	public function deleterecordorder($id){
		if(!$this->userfunctions->islogged()){
			
		}

	$datos=array();
		$result=$this->app->getrecord("ordenmaestra","*"," where id=$id");
		
		if (sizeof($result) > 0) 
		{		
			//if($this->app->updaterecord("ordenmaestra","activo='0'"," where id=$id")){
				//$this->app->updaterecord("rel_componentesorden","activo='0'"," where idOrden=$id");
				//$this->app->updaterecord("rel_procesosorden","activo='0'"," where idOrden=$id");
				//$this->app->updaterecord("citas","activo='0'"," where idOrden=$id");
			if($this->app->deleterecord("ordenmaestra"," where id=$id")>0){
				$this->app->deleterecord("rel_componentesorden"," where idOrden=$id");
				$this->app->deleterecord("rel_procesosorden"," where idOrden=$id");
				$this->app->deleterecord("citas"," where idOrden=$id");
				$this->app->deleterecord("incidentes"," where idOrden=$id");

				$datos[]=array(
					'exito'=>'si'
				);					
			}			
		} 
		else{
			$datos[]=array(
				'exito'=>'sin'
				);
		}
	return $datos;	
	}
	public function detailcomponentsbyorder($id){
		if(!$this->userfunctions->islogged()){
			
		}

	$datos=array();
		$result=$this->app->getrecord("rel_componentesorden","rel_componentesorden.fechaCompra,rel_componentesorden.id,rel_componentesorden.activo,rel_componentesorden.cantidadOrden,rel_componentesorden.costoTotal,componentessae.clave,componentessae.linea,componentessae.descripcion,componentessae.unidadEntrada,componentessae.ultimoCosto,relproducto_componente.cantidad","inner JOIN relproducto_componente on relproducto_componente.id=rel_componentesorden.idrelacionComponente inner JOIN componentessae on componentessae.id=relproducto_componente.idComponente where rel_componentesorden.idOrden=$id");
		for($a=0;$a<sizeof($result);$a++)
    		{
			$vec=$result[$a];
			$datos[] = array(
				'id'=>$vec['id'],
				'cantidadorden'=>$vec['cantidadOrden'],
				'costototal'=>$vec['costoTotal'],	
				'clave'=>$vec['clave'],
				'descripcion'=>$vec['descripcion'],	
				'um'=>$vec['unidadEntrada'],
				'ultimocosto'=>$vec['ultimoCosto'],
				'cantidadunitaria'=>$vec['cantidad'],
				'linea'=>$vec['linea'],
				'fecha'=>$vec['fechaCompra'],
				'activo'=>$vec['activo']
				);
                }


	return $datos;				
	}
	public function detailprocessbyorder($id,$isoperator=0){
		if(!$this->userfunctions->islogged()){
			
		}

		$datos=array();
		$operador;
		if($isoperator>0){
			$result=$this->app->getrecord("rel_procesosorden","rel_procesosorden.idProceso,rel_procesosorden.cantidadFinalCalculada,procesos.piezasFormato,procesos.estandarTurno,rel_procesosorden.fechaEmision,rel_procesosorden.idStatus,rel_procesosorden.cantidadTotal,rel_procesosorden.activo,rel_procesosorden.mermaEstimada,rel_procesosorden.id,rel_procesosorden.idOperador,rel_procesosorden.tiempoAjuste,rel_procesosorden.cantidad,rel_procesosorden.folio,rel_procesosorden.fechafinEstimada,rel_procesosorden.hrfinEstimada,maquinas.id as 'idmaquina',maquinas.estandar,maquinas.unidadEntrada,maquinas.unidadSalida,maquinas.nombre as 'maquina',maquinas.proceso,maquinas.unidad_medida,rel_procesosorden.horaRequerida,rel_procesosorden.fechaRequerida,rel_procesosorden.cantidadUtil,rel_procesosorden.merma,rel_procesosorden.fechaInicio,rel_procesosorden.horaInicio,rel_procesosorden.fechaTermino,rel_procesosorden.horaTermino ","inner JOIN procesos on procesos.id=rel_procesosorden.idProceso INNER JOIN maquinas on maquinas.id=procesos.maquina WHERE rel_procesosorden.idOrden=$id and (rel_procesosorden.fechaRequerida like CURDATE() or rel_procesosorden.fechafinEstimada like CURDATE()) and rel_procesosorden.idStatus!=6 order by rel_procesosorden.id asc");
		}else{
			$result=$this->app->getrecord("rel_procesosorden","rel_procesosorden.idProceso,rel_procesosorden.cantidadFinalCalculada,procesos.piezasFormato,procesos.estandarTurno,rel_procesosorden.fechaEmision,rel_procesosorden.idStatus,rel_procesosorden.cantidadTotal,rel_procesosorden.activo,rel_procesosorden.mermaEstimada,rel_procesosorden.id,rel_procesosorden.idOperador,rel_procesosorden.tiempoAjuste,rel_procesosorden.cantidad,rel_procesosorden.folio,rel_procesosorden.fechafinEstimada,rel_procesosorden.hrfinEstimada,maquinas.id as 'idmaquina',maquinas.estandar,maquinas.unidadEntrada,maquinas.unidadSalida,maquinas.nombre as 'maquina',maquinas.proceso,maquinas.unidad_medida,rel_procesosorden.horaRequerida,rel_procesosorden.fechaRequerida,rel_procesosorden.cantidadUtil,rel_procesosorden.merma,rel_procesosorden.fechaInicio,rel_procesosorden.horaInicio,rel_procesosorden.fechaTermino,rel_procesosorden.horaTermino ","inner JOIN procesos on procesos.id=rel_procesosorden.idProceso INNER JOIN maquinas on maquinas.id=procesos.maquina WHERE rel_procesosorden.idOrden=$id order by rel_procesosorden.id asc");
		}
		for($a=0;$a<sizeof($result);$a++)
    		{
			$vec=$result[$a];
			$order=$vec['id'];
			if($isoperator>0){
				$result2=$this->app->getrecord("rel_procesosorden","concat(persona.nombre,' ',persona.primerApellido,' ',persona.segundoApellido) as 'operador'"," INNER JOIN operadores ON operadores.id=rel_procesosorden.idOperador INNER JOIN persona on persona.id=operadores.idPersona where rel_procesosorden.id=$order and (rel_procesosorden.fechaRequerida like CURDATE() or rel_procesosorden.fechafinEstimada like CURDATE()) and rel_procesosorden.idStatus!=6");
	
			}else{
				$result2=$this->app->getrecord("rel_procesosorden","concat(persona.nombre,' ',persona.primerApellido,' ',persona.segundoApellido) as 'operador'"," INNER JOIN operadores ON operadores.id=rel_procesosorden.idOperador INNER JOIN persona on persona.id=operadores.idPersona where rel_procesosorden.id=$order ");
			}
				$vec2=$result2[0];
				$operador=$vec2['operador'];
			

			$datos[] = array(
				'id'=>$vec['id'],
				'idmaquina'=>$vec['idmaquina'],
				'idproceso'=>$vec['idProceso'],
				'maquina'=>$vec['maquina'],
				'tiempoajuste'=>$vec['tiempoAjuste'],
				'fechafinestimada'=>$vec['fechafinEstimada'],
				'hrfinestimada'=>$vec['hrfinEstimada'],
				'proceso'=>$vec['proceso'],
				'estandarturno'=>$vec['estandarTurno'],	
				'estandarhora'=>$vec['estandar'],
				'um'=>$vec['unidad_medida'],
				'piezasformato'=>$vec['piezasFormato'],
				'horarequerida'=>$vec['horaRequerida'],	
				'fecharequerida'=>$vec['fechaRequerida'],
				'cantidadutil'=>$vec['cantidadUtil'],
				'merma'=>$vec['merma'],
				'umentrada'=>$vec['unidadEntrada'],
				'umsalida'=>$vec['unidadSalida'],
				'cantidadtotal'=>$vec['cantidadTotal'],
				'mermaestimada'=>$vec['mermaEstimada'],
				'fechaemision'=>$vec['fechaEmision'],
				'fechainicio'=>$vec['fechaInicio'],
				'horainicio'=>$vec['horaInicio'],
				'fechafin'=>$vec['fechaTermino'],
				'horafin'=>$vec['horaTermino'],
				'idoperador'=>$vec['idOperador'],
				'cantidad'=>$vec['cantidad'],
				'folio'=>$vec['folio'],
				'operador'=>$operador,
				'status'=>$vec['idStatus'],
				'cantidadfinalcalculada'=>$vec['cantidadFinalCalculada'],
				'activo'=>$vec['activo']
				);
                }


	return $datos;				
	}
	public function getdatabobinabyprocess($id){
		if(!$this->userfunctions->islogged()){
			
		}

	$datos=array();
		$result=$this->app->getrecord("rel_procesobobina","*"," where idProceso=$id");
		for($a=0;$a<sizeof($result);$a++)
    		{
			$vec=$result[$a];
			$datos[] = array(
				'idproceso'=>$vec['idProceso'],
				'idbobina'=>$vec['idBobina'],
				'largo'=>$vec['largo']
				);
                }
	return $datos;

	}
	public function routeorder($idorden){
		if(!$this->userfunctions->islogged()){
			
		}

	$datos=array();
		$result=$this->app->getrecord("rel_procesosorden","DISTINCT(rel_procesosorden.idProceso), maquinas.nombre","  INNER JOIN procesos on procesos.id=rel_procesosorden.idProceso INNER join maquinas on maquinas.id=procesos.maquina where rel_procesosorden.idOrden=$idorden and rel_procesosorden.activo like '1' order by rel_procesosorden.idProceso asc");
		for($a=0;$a<sizeof($result);$a++)
    		{
			$vec=$result[$a];
			$datos[] = array(
				'id1'=>0,
				'id'=>$vec['idProceso'],
				'procesos'=>$vec['nombre'],
				
				);
                }
	return $datos;
	}

	public function detailorder($id,$isoperator=0){
		if(!$this->userfunctions->islogged()){
			
		}

	$datos=array();
		$result=$this->app->getrecord("ordenmaestra","*"," where id=$id and activo like '1'");
		for($a=0;$a<sizeof($result);$a++)
    		{
			$vec=$result[$a];
			$datos[] = array(
				'id'=>$vec['id'],
				'ruta'=>$this->routeorder($id),
				'procesos'=>$this->detailprocessbyorder($id,$isoperator),
				'componentes'=>$this->detailcomponentsbyorder($id),	
				'detalleproducto'=>$this->getspecificproduct($vec['idProducto']),
				'fechaemision'=>$vec['fechaEmision'],	
				'pedidosae'=>$vec['pedidoSAE'],
				'fecharequerida'=>$vec['fechaRequerida'],
				'cantidad'=>$vec['cantidad'],
				'status'=>$this->getstatusbyid($vec['idStatus']),
				'activo'=>$vec['activo']
				);
                }
	return $datos;
	}

	public function getoperators(){
		if(!$this->userfunctions->islogged()){
			
		}

		$datos=array();
		$result=$this->app->getrecord("operadores","operadores.id,concat(persona.nombre,' ',persona.primerApellido,' ',persona.segundoApellido) as 'operador'"," inner join persona on persona.id=operadores.idPersona where operadores.activo like '1' and operadores.extra != '0'");
		for($a=0;$a<sizeof($result);$a++)
    		{
			$vec=$result[$a];
			$datos[] = array(
				'id'=>$vec['id'],
				'operador'=>$vec['operador']
				);
                }
	return $datos;
	}


	public function getdisponibilitybydate($idmaquina,$fecharequerida,$minutos){
		if(!$this->userfunctions->islogged()){
			
		}
		$horaslibres=array("00:00:00","00:30:00","01:00:00","01:30:00","02:00:00","02:30:00","03:00:00","03:30:00","04:00:00","04:30:00","05:00:00","05:30:00","06:00:00","06:30:00",
				"07:00:00","07:30:00","08:00:00","08:30:00","09:00:00","09:30:00","10:00:00","10:30:00","11:00:00","11:30:00","12:00:00","12:30:00","13:00:00","13:30:00","14:00:00","14:30:00",
				"15:00:00","15:30:00","16:00:00","16:30:00","17:00:00","17:30:00","18:00:00","18:30:00","19:00:00","19:30:00","20:00:00","20:30:00","21:00:00","21:30:00","22:00:00","22:30:00",
				"23:00:00","23:30:00");
		$datos=array();
		$datos2=array();
		$horas=array();
			
			$result=$this->app->getrecord("citas","*"," where citas.activo like '1' and (fechaCita like '$fecharequerida' or fechaFin like '$fecharequerida') and idMaquina=$idmaquina");
			//trigger_error(" where citas.activo like '1' and (fechaCita like '$fecharequerida' or fechaFin like '$fecharequerida') and idMaquina=$idmaquina");
		if(sizeof($result)>0){
			for($a=0;$a<sizeof($result);$a++){
				$vec=$result[$a];
				$date1 = new Datetime($vec['fechaCita']." ".$vec['horaInicio']);
				$date2 = new Datetime($vec['fechaFin']." ".$vec['horaFin']);
				$horas=$horaslibres;

				unset($horaslibres);
				$horaslibres=array();
				unset($datos2);
				$datos2=array();
				for($b=0;$b<sizeof($horas);$b++){

					$date3 = new Datetime($fecharequerida." ".$horas[$b]);
					
					$fecha = new DateTime($fecharequerida." ".$horas[$b]);
					$addd="PT".round($minutos).'M';
					//trigger_error($addd);
					$fecha->add(new DateInterval($addd));
					//trigger_error($fecha->format('Y-m-d H:i:s'));
					$date4 = new Datetime($fecha->format('Y-m-d H:i:s'));


					if(($date3<$date1 && $date4<$date1) || ($date3>$date2 && $date4>$date2) ){

						
					array_push($horaslibres,$horas[$b]);
					array_push($datos2,$horas[$b]);
					
					}					
				}
				
			}

			$resultado = array_unique($datos2);
			for($c=0;$c<sizeof($resultado);$c++){
					$datos[]=array(
						'fecha'=>$fecharequerida,
						'hora'=>$resultado[$c]
						);
			}
		}else{
			for($c=0;$c<sizeof($horaslibres);$c++){
					$datos[]=array(
						'fecha'=>$fecharequerida,
						'hora'=>$horaslibres[$c]
						);
			}
		}
	return $datos;
	}
	//public function updateprocessbyorder($id,$folio,$merma,$fecharequerida,$hrrequerida,$operador,$fechaestimada,$horaestimada,$ajuste){
	public function updateprocessbyorder($id,$folio,$merma,$fecharequerida,$hrrequerida,$fechaestimada,$horaestimada,$ajuste){
		if(!$this->userfunctions->islogged()){
			
		}

	$datos=array();
		$result=$this->app->getrecord("rel_procesosorden","*"," where id=$id");
		$idproceso;
		$idorden;
		$idmaquina;

		if (sizeof($result) > 0) {
			$vec2=$result[0];
			$idproceso=$vec2['idProceso'];
			$idorden=$vec2['idOrden'];
			$result2=$this->app->getrecord("procesos","*"," where id=$idproceso");
			$vec3=$result2[0];
			$idmaquina=$vec3['maquina'];

				
				$result4=$this->app->getrecord("citas","*"," where idMaquina=$idmaquina and idOrden=$idorden and idProceso=$idproceso and folio like '$folio'");
		
				if (sizeof($result4) > 0) {
					$vec5=$result4[0];
					$idcita=$vec5['id'];

					/*$resultt=$this->app->getrecord("citas","*"," where ((citas.activo like '1') and ('$fechaestimada $horaestimada' BETWEEN concat(citas.fechaCita,' ',horaInicio) AND concat(citas.fechaFin,' ',citas.horaFin) and idMaquina=$idmaquina and citas.id!=$idcita) or ('$fechaestimada $horaestimada' BETWEEN concat(citas.fechaCita,' ',horaInicio) AND concat(citas.fechaFin,' ',citas.horaFin) and idProceso=$idproceso and citas.id!=$idcita)  or ('$fechaestimada $horaestimada' BETWEEN concat(citas.fechaCita,' ',horaInicio) AND concat(citas.fechaFin,' ',citas.horaFin) and folio like '$folio' and citas.id!=$idcita) or ('$fechaestimada $horaestimada' BETWEEN concat(citas.fechaCita,' ',horaInicio) AND concat(citas.fechaFin,' ',citas.horaFin) and idOperador=$operador and citas.id!=$idcita) or ('$fecharequerida $hrrequerida' BETWEEN concat(citas.fechaCita,' ',horaInicio) AND concat(citas.fechaFin,' ',citas.horaFin) and idMaquina=$idmaquina and citas.id!=$idcita) or ('$fecharequerida $hrrequerida' BETWEEN concat(citas.fechaCita,' ',horaInicio) AND concat(citas.fechaFin,' ',citas.horaFin) and idProceso=$idproceso and citas.id!=$idcita) or ('$fecharequerida $hrrequerida' BETWEEN concat(citas.fechaCita,' ',horaInicio) AND concat(citas.fechaFin,' ',citas.horaFin) and idOperador=$operador and citas.id!=$idcita  )) ");*/
					$resultt=$this->app->getrecord("citas","*"," where ((citas.activo like '1') and ('$fechaestimada $horaestimada' BETWEEN concat(citas.fechaCita,' ',horaInicio) AND concat(citas.fechaFin,' ',citas.horaFin) and idMaquina=$idmaquina and citas.id!=$idcita) or ('$fechaestimada $horaestimada' BETWEEN concat(citas.fechaCita,' ',horaInicio) AND concat(citas.fechaFin,' ',citas.horaFin) and idProceso=$idproceso and citas.id!=$idcita)  or ('$fechaestimada $horaestimada' BETWEEN concat(citas.fechaCita,' ',horaInicio) AND concat(citas.fechaFin,' ',citas.horaFin) and folio like '$folio' and citas.id!=$idcita) or ('$fechaestimada $horaestimada' BETWEEN concat(citas.fechaCita,' ',horaInicio) AND concat(citas.fechaFin,' ',citas.horaFin) and idOperador=$operador and citas.id!=$idcita) or ('$fecharequerida $hrrequerida' BETWEEN concat(citas.fechaCita,' ',horaInicio) AND concat(citas.fechaFin,' ',citas.horaFin) and idMaquina=$idmaquina and citas.id!=$idcita) or ('$fecharequerida $hrrequerida' BETWEEN concat(citas.fechaCita,' ',horaInicio) AND concat(citas.fechaFin,' ',citas.horaFin) and idProceso=$idproceso and citas.id!=$idcita) or ('$fecharequerida $hrrequerida' BETWEEN concat(citas.fechaCita,' ',horaInicio) AND concat(citas.fechaFin,' ',citas.horaFin) and citas.id!=$idcita  )) ");
		
					if (sizeof($resultt) > 0) {
						$vecc=$resultt[0];
						$datos[]=array(
							'exito'=>'ya',
							'folio'=>0,
							'inicio'=>$vecc['fechaCita'].' '.$vecc['horaInicio'],
							'termino'=>$vecc['fechaFin'].' '.$vecc['horaFin']
						);
					//return $datos;
					}else{
						$result1=$this->app->getrecord("ordenmaestra","*"," where id=$idorden");
						$ve=$result1[0];
						$sta=$ve['idStatus'];
						$this->app->updaterecord("citas"," idStatuscitas=$sta,fechaCita='$fecharequerida',horaInicio='$hrrequerida',fechaFin='$fechaestimada',horaFin='$horaestimada'","where id=$idcita");	
						if($this->app->updaterecord("rel_procesosorden","fechaEmision=now(),idStatus=7,mermaEstimada=$merma, tiempoAjuste=$ajuste,fechafinEstimada='$fechaestimada',hrfinEstimada='$horaestimada',fechaRequerida='$fecharequerida',horaRequerida='$hrrequerida',idOperador=2","where id=$id")){
							$datos[]=array(
								'exito'=>'si',
								'folio'=>$id
							);		
						}
					}
				}else{	
					
					/*$resultt=$this->app->getrecord("citas","*"," where ((citas.activo like '1') and ('$fechaestimada $horaestimada' BETWEEN concat(citas.fechaCita,' ',horaInicio) AND concat(citas.fechaFin,' ',citas.horaFin) and idMaquina=$idmaquina) or ('$fechaestimada $horaestimada' BETWEEN concat(citas.fechaCita,' ',horaInicio) AND concat(citas.fechaFin,' ',citas.horaFin) and idProceso=$idproceso) or ('$fechaestimada $horaestimada' BETWEEN concat(citas.fechaCita,' ',horaInicio) AND concat(citas.fechaFin,' ',citas.horaFin) and idOperador=$operador ) or ('$fecharequerida $hrrequerida' BETWEEN concat(citas.fechaCita,' ',horaInicio) AND concat(citas.fechaFin,' ',citas.horaFin) and idMaquina=$idmaquina) or ('$fecharequerida $hrrequerida' BETWEEN concat(citas.fechaCita,' ',horaInicio) AND concat(citas.fechaFin,' ',citas.horaFin) and folio like '$folio') or ('$fecharequerida $hrrequerida' BETWEEN concat(citas.fechaCita,' ',horaInicio) AND concat(citas.fechaFin,' ',citas.horaFin) and idProceso=$idproceso) or ('$fecharequerida $hrrequerida' BETWEEN concat(citas.fechaCita,' ',horaInicio) AND concat(citas.fechaFin,' ',citas.horaFin) and idOperador=$operador )) ");*/

					$resultt=$this->app->getrecord("citas","*"," where ((citas.activo like '1') and ('$fechaestimada $horaestimada' BETWEEN concat(citas.fechaCita,' ',horaInicio) AND concat(citas.fechaFin,' ',citas.horaFin) and idMaquina=$idmaquina) or ('$fechaestimada $horaestimada' BETWEEN concat(citas.fechaCita,' ',horaInicio) AND concat(citas.fechaFin,' ',citas.horaFin) and idProceso=$idproceso) or ('$fechaestimada $horaestimada' BETWEEN concat(citas.fechaCita,' ',horaInicio) AND concat(citas.fechaFin,' ',citas.horaFin) and idOperador=$operador ) or ('$fecharequerida $hrrequerida' BETWEEN concat(citas.fechaCita,' ',horaInicio) AND concat(citas.fechaFin,' ',citas.horaFin) and idMaquina=$idmaquina) or ('$fecharequerida $hrrequerida' BETWEEN concat(citas.fechaCita,' ',horaInicio) AND concat(citas.fechaFin,' ',citas.horaFin) and folio like '$folio') or ('$fecharequerida $hrrequerida' BETWEEN concat(citas.fechaCita,' ',horaInicio) AND concat(citas.fechaFin,' ',citas.horaFin) and idProceso=$idproceso) or ('$fecharequerida $hrrequerida' BETWEEN concat(citas.fechaCita,' ',horaInicio) AND concat(citas.fechaFin,' ',citas.horaFin)  )) ");
		
					if (sizeof($resultt) > 0) {
						$vecc=$resultt[0];
						$datos[]=array(
							'exito'=>'ya',
							'folio'=>0,
							'inicio'=>$vecc['fechaCita'].' '.$vecc['horaInicio'],
							'termino'=>$vecc['fechaFin'].' '.$vecc['horaFin']
						);
					//return $datos;
					}else{
						$result1=$this->app->getrecord("ordenmaestra","*"," where id=$idorden");
						$ve=$result1[0];
						$sta=$ve['idStatus'];
						$this->app->saverecord("citas","null,'$folio',$idmaquina,$idorden,$idproceso,2,$sta,'$fecharequerida','$hrrequerida','$fechaestimada','$horaestimada','','1'");				
						
						if($this->app->updaterecord("rel_procesosorden","fechaEmision=now(), idStatus=7,mermaEstimada=$merma,tiempoAjuste=$ajuste,fechafinEstimada='$fechaestimada',hrfinEstimada='$horaestimada',fechaRequerida='$fecharequerida',horaRequerida='$hrrequerida',idOperador=2","where id=$id")){
							$datos[]=array(
								'exito'=>'si',
								'folio'=>$id
							);		
						}
				
					}
					
				}


	
				
		} 
		else{
			
			$datos[]=array(
					'exito'=>'sin',
					'folio'=>0
				);
		}

	return $datos;	
	}

	public function updateprocessbyorderoperator ($id,$cantidadutil,$cantotal,$merma,$fechainicio,$hrinicio,$fechafin,$hrfin){
		if(!$this->userfunctions->islogged()){
			
		}

		$datos=array();
		$result=$this->app->getrecord("rel_procesosorden","*"," where id=$id");
		//trigger_error(print_r($result,true));
		
		if (sizeof($result) > 0) {
		
			if($this->app->updaterecord("rel_procesosorden"," idStatus=6,cantidadTotal=$cantotal,cantidadUtil=$cantidadutil,merma=$merma,fechaInicio='$fechainicio',horaInicio='$hrinicio',fechaTermino='$fechafin',horaTermino='$hrfin'","where id=$id")){
					$datos[]=array(
						'exito'=>'si',
						'folio'=>$id
					);		
			}		
				
		} 
		else{
			
			$datos[]=array(
					'exito'=>'sin',
					'folio'=>0
				);
		}

	return $datos;	
	}
	public function getallstatusfororders(){
		if(!$this->userfunctions->islogged()){
			
		}

	$datos=array();
		$result=$this->app->getrecord("status_orden","*","where activo like '1' order by status asc");
		for($a=0;$a<sizeof($result);$a++)
    		{
			$vec=$result[$a];
			$vec2=$result2[0];
			$datos[] = array(
				'id'=>$vec['id'],
				'status'=>$vec['status']
				
				);
                }


	return $datos;

	}
	
	public function getplaneacion($fechainicio,$fechafin,$maquina){
		if(!$this->userfunctions->islogged()){
			
		}

	$datos=array();
		$result=$this->app->getrecord("citas","maquinas.nombre as 'maquina',ordenmaestra.pedidoSAE,citas.id,citas.idOrden,citas.fechaCita,citas.horaInicio,citas.fechaFin,citas.horaFin,procesos.proceso","INNER JOIN maquinas on maquinas.id=citas.idMaquina  INNER JOIN ordenmaestra on ordenmaestra.id= citas.idOrden INNER JOIN procesos on procesos.id=citas.idProceso where citas.activo like '1' and fechaCita BETWEEN '$fechainicio' and '$fechafin'  and citas.idMaquina=$maquina order by citas.fechaCita ASC ");
		$result2=$this->app->getrecord("citas"," MIN(citas.fechaCita)AS 'min', MAX(citas.fechaFin) as 'max'","where citas.activo like '1' and fechaCita BETWEEN '$fechainicio' and '$fechafin'  and citas.idMaquina=$maquina");
		for($a=0;$a<sizeof($result);$a++)
    		{
			$vec=$result[$a];
			$vec2=$result2[0];
			$datos[] = array(
				'id'=>$vec['id'],
				'orden'=>$vec['idOrden'],
				'fechacita'=>$vec['fechaCita'],
				'horainicio'=>$vec['horaInicio'],
				'fechaFin'=>$vec['fechaFin'],
				'horaFin'=>$vec['horaFin'],
				'min'=>$vec2['min'],
				'max'=>$vec2['max'],
				'maquina'=>$vec['maquina'],
				'pedidoSAE'=>$vec['pedidoSAE'],
				'proceso'=>$vec['proceso']
				);
                }


	return $datos;

	}
	public function getmaquinasencitas($fechainicio,$fechafin){
		if(!$this->userfunctions->islogged()){
			
		}

		$datos=array();
		
		$result=$this->app->getrecord("citas"," DISTINCT(maquinas.nombre) as 'maquina',maquinas.unidadSalida,citas.idMaquina ","INNER JOIN maquinas on maquinas.id=citas.idMaquina where citas.activo like '1' and fechaCita BETWEEN '$fechainicio' and '$fechafin'");
		for($a=0;$a<sizeof($result);$a++)
    		{
			$vec=$result[$a];
			$datos[] = array(
				'id'=>$vec['idMaquina'],
				'maquina'=>$vec['maquina'].' ('.$vec['unidadSalida'].'(s))'
				);
                }


	return $datos;
		
	}
	public function getmaquinasencitasreal($fechainicio,$fechafin){
		if(!$this->userfunctions->islogged()){
			
		}

		$datos=array();
		
		$result=$this->app->getrecord("rel_procesosorden"," DISTINCT(maquinas.nombre) as 'maquina',maquinas.id,maquinas.unidadSalida ","INNER JOIN procesos on procesos.id=rel_procesosorden.idProceso INNER JOIN maquinas on maquinas.id=procesos.maquina where rel_procesosorden.activo like '1' and ((rel_procesosorden.fechaInicio like CURDATE() and rel_procesosorden.fechaTermino like '0000-00-00') or (rel_procesosorden.fechaInicio !='0000-00-00' and rel_procesosorden.fechaTermino like '0000-00-00') ) ");

		for($a=0;$a<sizeof($result);$a++)
    		{
			$vec=$result[$a];
			$datos[] = array(
				'id'=>$vec['id'],
				'maquina'=>$vec['maquina'].' ('.$vec['unidadSalida'].'(s))'
				);
                }


	return $datos;
		
	}
	public function getallordersbystatus($status){
		if(!$this->userfunctions->islogged()){
			
		}

	$datos=array();
	if(strpos($status,".")>0){
		$st=explode(".",$status);
		$result=$this->app->getrecord("ordenmaestra","ordenmaestra.id,ordenmaestra.pedidoSAE,productos.nombreProducto,ordenmaestra.fechaProduccion,ordenmaestra.fechaEmision,ordenmaestra.fechaFin,ordenmaestra.fechaRequerida,ordenmaestra.cantidad,ordenmaestra.faltante,ordenmaestra.producido,status_orden.status","INNER JOIN productos on productos.id=ordenmaestra.idProducto INNER JOIN status_orden on status_orden.id=ordenmaestra.idStatus where ordenmaestra.activo like '1' and (ordenmaestra.idStatus=$st[0] or ordenmaestra.idStatus=$st[1]) order by ordenmaestra.fechaEmision desc");
	}else if($status==0){
		$result=$this->app->getrecord("ordenmaestra","ordenmaestra.id,ordenmaestra.pedidoSAE,productos.nombreProducto,ordenmaestra.fechaProduccion,ordenmaestra.fechaEmision,ordenmaestra.fechaFin,ordenmaestra.fechaRequerida,ordenmaestra.cantidad,ordenmaestra.faltante,ordenmaestra.producido,status_orden.status","INNER JOIN productos on productos.id=ordenmaestra.idProducto INNER JOIN status_orden on status_orden.id=ordenmaestra.idStatus where ordenmaestra.activo like '1'  order by ordenmaestra.fechaEmision desc");
	}else{
		$result=$this->app->getrecord("ordenmaestra","ordenmaestra.id,ordenmaestra.pedidoSAE,productos.nombreProducto,ordenmaestra.fechaProduccion,ordenmaestra.fechaEmision,ordenmaestra.fechaFin,ordenmaestra.fechaRequerida,ordenmaestra.cantidad,ordenmaestra.faltante,ordenmaestra.producido,status_orden.status","INNER JOIN productos on productos.id=ordenmaestra.idProducto INNER JOIN status_orden on status_orden.id=ordenmaestra.idStatus where ordenmaestra.activo like '1' and ordenmaestra.idStatus=$status order by ordenmaestra.fechaEmision desc");
	}	
		for($a=0;$a<sizeof($result);$a++)
    		{
			$vec=$result[$a];
			$datos[] = array(
				'id'=>$vec['id'],
				'producto'=>$vec['nombreProducto'],
				'fechaemision'=>$vec['fechaEmision'],	
				'fecharequerida'=>$vec['fechaRequerida'],
				'fechaproduccion'=>$vec['fechaProduccion'],
				'fechafin'=>$vec['fechaFin'],
				'cantidad'=>$vec['cantidad'],
				'faltante'=>$vec['faltante'],
				'producido'=>$vec['producido'],
				'status'=>$vec['status'],
				'pedidosae'=>$vec['pedidoSAE'],
				'activo'=>$vec['activo']
				);
                }
	
	return $datos;
	}
	public function getplaneacion2($fechainicio,$fechafin){
		if(!$this->userfunctions->islogged()){
			
		}

	$datos=array();
		$result=$this->app->getrecord("citas","rel_procesosorden.cantidad as 'canproceso',ordenmaestra.cantidad, ordenmaestra.pedidoSAE,citas.id,citas.idMaquina,citas.idOrden,citas.fechaCita,citas.horaInicio,citas.fechaFin,citas.horaFin,procesos.proceso","INNER JOIN ordenmaestra on ordenmaestra.id=citas.idOrden INNER JOIN procesos on procesos.id=citas.idProceso INNER JOIN rel_procesosorden on rel_procesosorden.folio=citas.folio where citas.activo like '1' and fechaCita BETWEEN '$fechainicio' and '$fechafin' order by citas.fechaCita,citas.horaInicio ASC ");
		$result2=$this->app->getrecord("citas"," MIN(citas.fechaCita)AS 'min', MAX(citas.fechaFin) as 'max'","where citas.activo like '1' and fechaCita BETWEEN '$fechainicio' and '$fechafin'");
		
		for($a=0;$a<sizeof($result);$a++)
    		{
			$vec=$result[$a];
			$vec2=$result2[0];
			$color='#848484';

				$date1 = new Datetime($vec['fechaCita']." ".$vec['horaInicio']);
				$date2 = new Datetime($vec['fechaFin']." ".$vec['horaFin']);
				$hoy=new Datetime( date("Y-m-d H:i:s"));
				/*if($hoy>$date1 && $hoy<$date2){
					$color='#04B404';
				}else if($hoy>$date2){
					$color='#848484';
				}else if($hoy<$date1){
					$color='#2E64FE';
				}*/
			
			$datos[] = array(
				'id'=>$vec['id'],
				'orden'=>$vec['idOrden'],
				'fechacita'=>$vec['fechaCita'],
				'horainicio'=>$vec['horaInicio'],
				'fechaFin'=>$vec['fechaFin'],
				'horaFin'=>$vec['horaFin'],
				'min'=>$vec2['min'],
				'max'=>$vec2['max'],
				'cantidad'=>$vec['canproceso'],
				'maquina'=>$vec['idMaquina'],
				'pedidoSAE'=>$vec['pedidoSAE'],
				'maquinas'=>$this->getmaquinasencitas($fechainicio,$fechafin),
				'proceso'=>$vec['proceso'],
				'color'=>$color
				);
                }


	return $datos;

	}
	public function getplaneacion2real($fechainicio,$fechafin){
		 date_default_timezone_set('America/Mexico_City');
		if(!$this->userfunctions->islogged()){
			
		}

	$datos=array();
		$result=$this->app->getrecord("rel_procesosorden","rel_procesosorden.cantidad as 'canproceso',rel_procesosorden.folio,ordenmaestra.cantidad, ordenmaestra.pedidoSAE,rel_procesosorden.id,procesos.maquina,rel_procesosorden.idOrden,rel_procesosorden.fechaInicio,rel_procesosorden.horaInicio,rel_procesosorden.fechaTermino,rel_procesosorden.horaTermino,procesos.proceso","INNER JOIN ordenmaestra on ordenmaestra.id=rel_procesosorden.idOrden INNER JOIN procesos on procesos.id=rel_procesosorden.idProceso where rel_procesosorden.activo like '1' and (rel_procesosorden.fechaInicio like CURDATE() or (rel_procesosorden.fechaInicio!='0000-00-00' and rel_procesosorden.fechaTermino like '0000-00-00')) and (ordenmaestra.idStatus=7 or ordenmaestra.idStatus=3 ) order by rel_procesosorden.fechaInicio,rel_procesosorden.horaInicio ASC ");
		//trigger_error("ordenmaestra.cantidad, ordenmaestra.pedidoSAE,rel_procesosorden.id,procesos.maquina,rel_procesosorden.idOrden,rel_procesosorden.fechaInicio,rel_procesosorden.horaInicio,rel_procesosorden.fechaTermino,rel_procesosorden.horaTermino,procesos.proceso"."INNER JOIN ordenmaestra on ordenmaestra.id=rel_procesosorden.idOrden INNER JOIN productos on productos.id=ordenmaestra.idProducto INNER JOIN procesos on procesos.idProducto=productos.id where rel_procesosorden.activo like '1' and (rel_procesosorden.fechaInicio BETWEEN '$fechainicio' and '$fechafin' or rel_procesosorden.fechaTermino BETWEEN '$fechainicio' and '$fechafin') order by rel_procesosorden.fechaInicio,rel_procesosorden.horaInicio ASC ");
		$result2=$this->app->getrecord("rel_procesosorden"," MIN(rel_procesosorden.fechaInicio)AS 'min', MAX(rel_procesosorden.fechaTermino) as 'max'","where rel_procesosorden.activo like '1' and (rel_procesosorden.fechaInicio like CURDATE() or (rel_procesosorden.fechaInicio!='0000-00-00' and rel_procesosorden.fechaTermino like '0000-00-00'))");
		
		for($a=0;$a<sizeof($result);$a++)
    		{
			$vec=$result[$a];
			$vec2=$result2[0];
			$color='#848484';

				$date1 = new Datetime($vec['fechaInicio']." ".$vec['horaInicio']);
				//$date2 = new Datetime($vec['fechaTermino']." ".$vec['horaTermino']);
				$hoy=new Datetime( date("Y-m-d H:i:s"));

				if($hoy>$date1 && $vec['fechaTermino']=="0000-00-00"){
					$color='#04B404';
					$fechatermino="";
					$max="";
					if($vec['fechaTermino']=="0000-00-00"){
						//$fechatermino=$vec['fechaInicio'];
						$fechaTermino=date("Y-m-d");
					}else{
						$fechatermino=$vec['fechaTermino'];
					}
					if($vec2['max']=="0000-00-00"){
						//$max=$vec2['min'];
						$max=date("Y-m-d");
					}else{
						$max=$vec2['max'];
					}
					$datos[] = array(
						'id'=>$vec['id'],
						'orden'=>$vec['folio'],
						'fechacita'=>$vec['fechaInicio'],
						'horainicio'=>$vec['horaInicio'],
						'fechaFin'=>date("Y-m-d"),
						'horaFin'=>$vec['horaTermino'],
						'min'=>$vec2['min'],
						'max'=>$max,
						'cantidad'=>$vec['canproceso'],
						'maquina'=>$vec['maquina'],
						'pedidoSAE'=>$vec['pedidoSAE'],
						'maquinas'=>$this->getmaquinasencitasreal($fechainicio,$fechafin),
						'proceso'=>$vec['proceso'],
						'color'=>$color
						);
				}else if($hoy>$date2){
					$color='#848484';
				}else if($hoy<$date1){
					$color='#2E64FE';
				}
			
                }


	return $datos;

	}
	public function getproduccionreal($fechainicio,$fechafin){
		if(!$this->userfunctions->islogged()){
			
		}

		$datos=array();
		$result=$this->app->getrecord("rel_procesosorden","rel_procesosorden.cantidad,rel_procesosorden.cantidadFinalPlaneador,rel_procesosorden.idOrden,rel_procesosorden.folio,rel_procesosorden.fechaInicio,rel_procesosorden.horaInicio,rel_procesosorden.fechaTermino,rel_procesosorden.horaTermino,rel_procesosorden.cantidadUtil,rel_procesosorden.merma,maquinas.nombre,procesos.descripcionProceso,CONCAT(persona.nombre,' ',persona.primerApellido,' ',persona.segundoApellido) as 'operador'","INNER JOIN procesos on procesos.id=rel_procesosorden.idProceso INNER JOIN maquinas on maquinas.id=procesos.maquina INNER JOIN operadores on operadores.id=rel_procesosorden.idOperador INNER JOIN persona on persona.id=operadores.idPersona where rel_procesosorden.fechaTermino BETWEEN '$fechainicio' and '$fechafin' order by rel_procesosorden.fechaInicio,rel_procesosorden.horaInicio ASC ");
		
		for($a=0;$a<sizeof($result);$a++)
    		{
			$vec=$result[$a];

			/*$vec2=$result2[0];
			$color='#848484';

				$date1 = new Datetime($vec['fechaInicio']." ".$vec['horaInicio']);
				//$date2 = new Datetime($vec['fechaTermino']." ".$vec['horaTermino']);
				$hoy=new Datetime( date("Y-m-d H:i:s"));

				if($hoy>$date1 && $vec['fechaTermino']=="0000-00-00"){
					$color='#04B404';
					$fechatermino="";
					$max="";
					if($vec['fechaTermino']=="0000-00-00"){
						$fechatermino=$vec['fechaInicio'];
					}else{
						$fechatermino=$vec['fechaTermino'];
					}
					if($vec2['max']=="0000-00-00"){
						$max=$vec2['min'];
					}else{
						$max=$vec2['max'];
					}*/
					$aux=explode('-',$vec['folio']);
					$datos[] = array(
						'idorden'=>$vec['idOrden'],
						'folio'=>$aux[0].'--'.$aux[1],
						'fechainicio'=>$vec['fechaInicio'],
						'horainicio'=>$vec['horaInicio'],
						'fechaFin'=>$vec['fechaTermino'],
						'horaFin'=>$vec['horaTermino'],
						'cantidadutil'=>$vec['cantidadUtil'],
						'merma'=>$vec['merma'],
						'cantidadexcedente'=>$vec['cantidadFinalPlaneador'],
						'maquina'=>$vec['nombre'],
						'proceso'=>$vec['descripcionProceso'],
						'operador'=>$vec['operador'],
						'cantidadsolicitada'=>$vec['cantidad']
						);
				/*}else if($hoy>$date2){
					$color='#848484';
				}else if($hoy<$date1){
					$color='#2E64FE';
				}*/
			
                }

//trigger_error(print_r($datos,true));
	return $datos;	
	}
	public function getplaneacionbyorders($status,$orden){
		if(!$this->userfunctions->islogged()){
			
		}

	$datos=array();

		if($status==1.7){
			
			$result=$this->app->getrecord("citas"," ordenmaestra.pedidoSAE,citas.id,citas.idMaquina,citas.idOrden,citas.fechaCita,citas.horaInicio,citas.fechaFin,citas.horaFin,procesos.proceso","INNER JOIN ordenmaestra on ordenmaestra.id=citas.idOrden INNER JOIN procesos on procesos.id=citas.idProceso where citas.activo like '1' and (ordenmaestra.idStatus=1 || ordenmaestra.idStatus=7) and citas.idOrden= $orden order by citas.fechaCita ASC ");
			$result2=$this->app->getrecord("citas"," MIN(citas.fechaCita)AS 'min', MAX(citas.fechaFin) as 'max'","where citas.activo like '1' and citas.idOrden= $orden ");

		}else {
			$result=$this->app->getrecord("citas"," ordenmaestra.pedidoSAE,citas.id,citas.idMaquina,citas.idOrden,citas.fechaCita,citas.horaInicio,citas.fechaFin,citas.horaFin,procesos.proceso","INNER JOIN ordenmaestra on ordenmaestra.id=citas.idOrden INNER JOIN procesos on procesos.id=citas.idProceso where citas.activo like '1' and ordenmaestra.idStatus=$status and citas.idOrden= $orden order by citas.fechaCita ASC ");
			$result2=$this->app->getrecord("citas"," MIN(citas.fechaCita)AS 'min', MAX(citas.fechaFin) as 'max'","where citas.activo like '1'  and citas.idOrden= $orden");
		}

		
		for($a=0;$a<sizeof($result);$a++)
    		{
			$vec=$result[$a];
			$idorden=$vec['idOrden'];

			$vec2=$result2[0];
			
			$datos[] = array(
				'id'=>$vec['id'],
				'orden'=>$vec['idOrden'],
				'fechacita'=>$vec['fechaCita'],
				'horainicio'=>$vec['horaInicio'],
				'fechaFin'=>$vec['fechaFin'],
				'horaFin'=>$vec['horaFin'],
				'min'=>$vec2['min'],
				'max'=>$vec2['max'],
				'maquina'=>$vec['idMaquina'],
				'pedidoSAE'=>$vec['pedidoSAE'],
				'maquinas'=>$this->getmaquinasencitas($fechainicio,$fechafin),
				'proceso'=>$vec['proceso']
				);
                }


	return $datos;

	}

	public function getplaneacionallordersbystatus($status){
		if(!$this->userfunctions->islogged()){
			
		}

	$datos=array();

		if($status==1.7){		
			
			$result=$this->app->getrecord("ordenmaestra","*","where idStatus=1 or idStatus=7 and activo like '1'");
			$result12=$this->app->getrecord("citas"," MIN(citas.fechaCita)AS 'min', MAX(citas.fechaFin) as 'max'"," where citas.activo like '1' and (citas.idStatuscitas=1 or citas.idStatuscitas=7) ");
			
			for($a=0;$a<sizeof($result);$a++)
    			{
				$vec=$result[$a];
				$idorden=$vec['id'];
				$result2=$this->app->getrecord("citas"," MIN(citas.fechaCita)AS 'inicio', MAX(citas.fechaFin) as 'fin'","where citas.activo like '1' and citas.idOrden=$idorden");
				if(sizeof($result2)>0){
					$vec2=$result2[0];
					$vec3=$result12[0];

					$datos[] = array(
						'id'=>$vec['id'],
						'orden'=>$vec['id'],
						'fecharequerida'=>$vec['fechaRequerida'],
						'fechainicio'=>$vec2['inicio'],
						'fechaFin'=>$vec2['fin'],
						'min'=>$vec3['min'],
						'max'=>$vec3['max'],
						'status'=>'Proceso y Programadas',
						'pedidoSAE'=>$vec['pedidoSAE'],
						'cantidad'=>$vec['cantidad']	
					);
				}
                	}

		}else{
			$result=$this->app->getrecord("ordenmaestra","*","where idStatus=$status  and activo like '1'");
			$result12=$this->app->getrecord("citas","status_orden.status, MIN(citas.fechaCita)AS 'min', MAX(citas.fechaFin) as 'max'"," INNER JOIN 	status_orden on status_orden.id=citas.idStatuscitas where citas.activo like '1' and citas.idStatuscitas=$status ");
			
			for($a=0;$a<sizeof($result);$a++)
    			{
				$vec=$result[$a];
				$idorden=$vec['id'];
				$result2=$this->app->getrecord("citas"," MIN(citas.fechaCita)AS 'inicio', MAX(citas.fechaFin) as 'fin'","where citas.activo like '1' and citas.idOrden=$idorden");
				//trigger_error(sizeof($result2));
				$vec2=$result2[0];
				$vec3=$result12[0];

				if($vec2['inicio']!=NULL && $vec2['fin']!=NULL){

					

					$datos[] = array(
						'id'=>$vec['id'],
						'orden'=>$vec['id'],
						'fecharequerida'=>$vec['fechaRequerida'],
						'fechainicio'=>$vec2['inicio'],
						'fechaFin'=>$vec2['fin'],
						'min'=>$vec3['min'],
						'max'=>$vec3['max'],
						'status'=>$vec3['status'],
						'pedidoSAE'=>$vec['pedidoSAE'],
						'cantidad'=>$vec['cantidad']	
					);
				}
                	}

		}
		
		

	return $datos;

	}

	public function getallinsumos($status=0){
		if(!$this->userfunctions->islogged()){
			
		}

	$datos=array();
		
		if($status==1.7){
			
			$result=$this->app->getrecord("ordenmaestra","*"," where (idStatus=1 or idStatus=7) and activo like '1'");
			for($a=0;$a<sizeof($result);$a++){
				$vec=$result[$a];
				$idpro=$vec['idProducto'];
				$idorden=$vec['id'];

				$result2=$this->app->getrecord("productos","nombreProducto"," where id=$idpro and activo like '1'");
				$vec2=$result2[0];

				$result3=$this->app->getrecord("pedidos_control","*"," where idOrden=$idorden and activo like '1'");
				$vec3=$result3[0];

				$datos[]=array(
					'id'=>$vec3['id'],
					'sae'=>$vec['pedidoSAE'],
					'producto'=>$vec2['nombreProducto'],
					'fechaelaboracion'=>$vec3['fechaelaboracion'],
					'fechainsumos'=>$vec3['fechainsumos'],
					'status'=>$vec3['status'],
					'cartonorden'=>$vec3['cartonOrden'],
					'cartonfecha'=>$vec3['cartonFecha'],
					'singleorden'=>$vec3['singleOrden'],
					'singlefecha'=>$vec3['singleFecha'],
					'pantoneorden'=>$vec3['pantoneOrden'],
					'pantonefecha'=>$vec3['pantoneFecha'],
					'suajeorden'=>$vec3['suajeOrden'],
					'suajefecha'=>$vec3['suajeFecha'],
					'embalajeorden'=>$vec3['embalajeOrden'],
					'embalajefecha'=>$vec3['embalajeFecha'],
					'personaljunta'=>$vec3['personalFechajunta'],
					'personalcontratacion'=>$vec3['personalFechacontratacion'],
					'dummyjunta'=>$vec3['dummyFechajunta'],
					'dummyaprobacion'=>$vec3['dummyFechaaprobacion'],
					'especificacionesjunta'=>$vec3['especificacionesFechajunta'],
					'especificacionespublicacion'=>$vec3['especificacionesFechapublicacion'],
					'formacionsuaje'=>$vec3['formacionSuajeaprobacion'],
					'filmadoplacas'=>$vec3['filmadoPlacasaprobacion'],
					'placasentrega'=>$vec3['placasEntrega'],
					'pruebacoloremision'=>$vec3['pruebacolorFechaemision'],
					'pruebacoloraprobacion'=>$vec3['pruebacolorFechaaprobacion']

				);
				
			}

		}else {
			$result=$this->app->getrecord("ordenmaestra","*"," where  activo like '1' order by fechaEmision desc");
			//trigger_error(print_r($result,true));
			for($a=0;$a<sizeof($result);$a++){
				$vec=$result[$a];
				$idpro=$vec['idProducto'];
				$idorden=$vec['id'];

				$result2=$this->app->getrecord("productos","nombreProducto"," where id=$idpro and activo like '1'");
				$vec2=$result2[0];

				$result3=$this->app->getrecord("pedidos_control","*"," where idOrden=$idorden and activo like '1'");
			for($b=0;$b<sizeof($result3);$b++){
				$vec3=$result3[$b];

				$datos[]=array(
					'id'=>$vec3['id'],
					'sae'=>$vec['pedidoSAE'],
					'producto'=>$vec2['nombreProducto'],
					'fechaelaboracion'=>$vec3['fechaelaboracion'],
					'fechainsumos'=>$vec3['fechainsumos'],
					'status'=>$vec3['status'],
					'cartonorden'=>$vec3['cartonOrden'],
					'cartonfecha'=>$vec3['cartonFecha'],
					'singleorden'=>$vec3['singleOrden'],
					'singlefecha'=>$vec3['singleFecha'],
					'pantoneorden'=>$vec3['pantoneOrden'],
					'pantonefecha'=>$vec3['pantoneFecha'],
					'suajeorden'=>$vec3['suajeOrden'],
					'suajefecha'=>$vec3['suajeFecha'],
					'embalajeorden'=>$vec3['embalajeOrden'],
					'embalajefecha'=>$vec3['embalajeFecha'],
					'personaljunta'=>$vec3['personalFechajunta'],
					'personalcontratacion'=>$vec3['personalFechacontratacion'],
					'dummyjunta'=>$vec3['dummyFechajunta'],
					'dummyaprobacion'=>$vec3['dummyFechaaprobacion'],
					'especificacionesjunta'=>$vec3['especificacionesFechajunta'],
					'especificacionespublicacion'=>$vec3['especificacionesFechapublicacion'],
					'formacionsuaje'=>$vec3['formacionSuajeaprobacion'],
					'filmadoplacas'=>$vec3['filmadoPlacasaprobacion'],
					'placasentrega'=>$vec3['placasEntrega'],
					'pruebacoloremision'=>$vec3['pruebacolorFechaemision'],
					'pruebacoloraprobacion'=>$vec3['pruebacolorFechaaprobacion']

				);
				//trigger_error(print_r($datos,true));
			}
			}
				return $datos;

		}

		
	
    
	return $datos;
	}
	public function getimsumosbyorder($status,$orden){
		if(!$this->userfunctions->islogged()){
			
		}

	$datos=array();
		
		
			
			$result=$this->app->getrecord("ordenmaestra","*"," where  id=$orden and activo like '1'");
			for($a=0;$a<sizeof($result);$a++){
				$vec=$result[$a];
				$idpro=$vec['idProducto'];
				$idorden=$vec['id'];

				$result2=$this->app->getrecord("productos","nombreProducto"," where id=$idpro and activo like '1'");
				$vec2=$result2[0];

				$result3=$this->app->getrecord("pedidos_control","*"," where idOrden=$idorden and activo like '1'");
				$vec3=$result3[0];

				$datos[]=array(
					'id'=>$vec3['id'],
					'sae'=>$vec['pedidoSAE'],
					'producto'=>$vec2['nombreProducto'],
					'fechaelaboracion'=>$vec3['fechaelaboracion'],
					'fechainsumos'=>$vec3['fechainsumos'],
					'status'=>$vec3['status'],
					'cartonorden'=>$vec3['cartonOrden'],
					'cartonfecha'=>$vec3['cartonFecha'],
					'singleorden'=>$vec3['singleOrden'],
					'singlefecha'=>$vec3['singleFecha'],
					'pantoneorden'=>$vec3['pantoneOrden'],
					'pantonefecha'=>$vec3['pantoneFecha'],
					'suajeorden'=>$vec3['suajeOrden'],
					'suajefecha'=>$vec3['suajeFecha'],
					'embalajeorden'=>$vec3['embalajeOrden'],
					'embalajefecha'=>$vec3['embalajeFecha'],
					'personaljunta'=>$vec3['personalFechajunta'],
					'personalcontratacion'=>$vec3['personalFechacontratacion'],
					'dummyjunta'=>$vec3['dummyFechajunta'],
					'dummyaprobacion'=>$vec3['dummyFechaaprobacion'],
					'especificacionesjunta'=>$vec3['especificacionesFechajunta'],
					'especificacionespublicacion'=>$vec3['especificacionesFechapublicacion'],
					'formacionsuaje'=>$vec3['formacionSuajeaprobacion'],
					'filmadoplacas'=>$vec3['filmadoPlacasaprobacion'],
					'placasentrega'=>$vec3['placasEntrega'],
					'pruebacoloremision'=>$vec3['pruebacolorFechaemision'],
					'pruebacoloraprobacion'=>$vec3['pruebacolorFechaaprobacion']

				);
			
			}
		
	
    
	return $datos;
	}


	public function calculatemerma($idorden=1){
		if(!$this->userfunctions->islogged()){
			
		}

	$datos=array();
	$ban=true;
	$idsprocesos=array();
	$totalconmerma=0;
	$totalfinalcalculado=0;
		$result=$this->app->getrecord("rel_procesosorden","DISTINCT(idProceso) as 'idproceso',procesos.mermaAjuste,procesos.estandarAjuste,procesos.piezasFormato "," inner JOIN procesos on procesos.id=rel_procesosorden.idProceso where idOrden=$idorden order by  rel_procesosorden.id ASC");
		
		for($a=0;$a<sizeof($result);$a++)
    		{
			$vec=$result[$a];
			$merma=$merma+$vec['mermaAjuste'];
                }
		
	$merma=$merma*9;
		for($a=0;$a<sizeof($result);$a++)
    		{	$vec=$result[$a];
			$ban2=true;
			$idpro=$vec['idproceso'];
			
			$result2=$this->app->getrecord("rel_procesosorden","sum(cantidad) as 'total' "," where idProceso=$idpro");
			$vec2=$result2[0];
			$total=$vec2['total'];
		

			if($a==0 && $ban==true){
				$totalconmerma=$total+$merma;	
				$ban=false;
			}else{
				$totalconmerma=$totalfinalcalculado;
				$totalfinalcalculado=0;
			}
			
			$result3=$this->app->getrecord("rel_procesosorden","rel_procesosorden.id,rel_procesosorden.cantidad,procesos.medidaEntrada,procesos.piezasFormato,procesos.estandarAjuste "," INNER JOIN procesos on procesos.id=rel_procesosorden.idProceso where rel_procesosorden.idProceso=$idpro");
			for($b=0;$b<sizeof($result3);$b++){
				$vec3=$result3[$b];
				
				

				$cantidad=$vec3['cantidad'];
				$id=$vec3['id'];
				$medidaentrada=$vec3['medidaEntrada'];
				$piezasformato=$vec3['piezasFormato'];
				$estandarAjuste=explode("%",$vec3['estandarAjuste']);
					
				
				
					if(strcasecmp($medidaentrada, "piezas") == 0 && $ban2){
					$vec4=$result[$a-1];
					//trigger_error($totalconmerma);
					//trigger_error(print_r($vec4,true));
					$totalconmerma=$totalconmerma*$vec4['piezasFormato'];
					//trigger_error($totalconmerma);
					$ban2=false;	
				}
				$porcentaje=$estandarAjuste[0]/100;//

				$proporcion=ceil(($cantidad*$totalconmerma)/$total);//
				
				$proporcionfinal=ceil($proporcion-($proporcion*$porcentaje));
				$totalfinalcalculado=$totalfinalcalculado+$proporcionfinal;


				
				$this->app->updaterecord("rel_procesosorden"," cantidadInicialCalculada=$proporcion,cantidadFinalCalculada=$proporcionfinal","where id=$id");
			}

                }	
			

	
	return $datos;

	}

	public function ticketdata($idproceso){
		if(!$this->userfunctions->islogged()){
			
		}

	$datos=array();
		
		$result=$this->app->getrecord("rel_procesosorden","rel_procesosorden.fechafinEstimada,rel_procesosorden.hrfinEstimada,rel_procesosorden.cantidadFinalCalculada,procesos.estandarTurno,rel_procesosorden.folio,rel_procesosorden.idOrden,rel_procesosorden.fechaEmision,productos.nombreProducto,productos.codigoProducto,productos.documento1,productos.documento2,productos.documento3,productos.documento4,productos.documento5,productos.descripcion,procesos.proceso,procesos.descripcionProceso,ordenmaestra.pedidoSAE,ordenmaestra.cantidad as 'cantidadProducir',maquinas.tiempoAjuste,maquinas.unidadEntrada,maquinas.nombre as 'maquina',concat(persona.nombre,' ',persona.primerApellido,' ',persona.segundoApellido) as 'operador',procesos.piezasFormato,procesos.medidaEntrada,rel_procesosorden.mermaEstimada,rel_procesosorden.cantidad as 'cantidadTicket',rel_procesosorden.fechaRequerida,rel_procesosorden.horaRequerida,procesos.especificacion1,procesos.especificacion2,procesos.especificacion3,procesos.especificacion4,procesos.especificacion5,rel_procesosorden.fechaInicio,rel_procesosorden.horaInicio,rel_procesosorden.horaTermino,rel_procesosorden.fechaTermino,rel_procesosorden.merma,rel_procesosorden.cantidadUtil"," left JOIN ordenmaestra on ordenmaestra.id=rel_procesosorden.idOrden left JOIN productos on productos.id=ordenmaestra.idProducto left JOIN procesos on procesos.id=rel_procesosorden.idProceso left JOIN maquinas on maquinas.id=procesos.maquina left JOIN operadores on operadores.id=rel_procesosorden.idOperador left join persona on persona.id=operadores.idPersona where rel_procesosorden.id=$idproceso");
		for($a=0;$a<sizeof($result);$a++)
    		{
			$vec=$result[$a];
			$datos[] = array(
				'folio'=>$vec['folio'],
				'pedidosae'=>$vec['pedidoSAE'],
				'idorden'=>$vec['idOrden'],
				'fechaemision'=>$vec['fechaEmision'],
				'producto'=>$vec['nombreProducto'],
				'sae'=>$vec['codigoProducto'],
				'descripcionproducto'=>$vec['descripcion'],
				'proceso'=>$vec['proceso'],
				'descripcionproceso'=>$vec['descripcionProceso'],
				'cantidadproducir'=>$vec['cantidadProducir'],
				'maquina'=>$vec['maquina'],
				'operador'=>$vec['operador'],
				'cantidadfinal'=>$vec['cantidadFinalCalculada'],
				'piezasformato'=>$vec['piezasFormato'],
				'um'=>$vec['medidaEntrada'],
				'tiempoajuste'=>$vec['tiempoAjuste'],
				'umproceso'=>$vec['unidadEntrada'],
				'mermadada'=>$vec['mermaEstimada'],
				'cantidadticket'=>$vec['cantidadTicket'],
				'fecharequerida'=>$vec['fechaRequerida'],
				'horarequerida'=>$vec['horaRequerida'],
				'esp1'=>$vec['especificacion1'],
				'esp2'=>$vec['especificacion2'],
				'esp3'=>$vec['especificacion3'],
				'esp4'=>$vec['especificacion4'],
				'esp5'=>$vec['especificacion5'],
				'espge1'=>$vec['documento1'],
				'espge2'=>$vec['documento2'],
				'espge3'=>$vec['documento3'],
				'espge4'=>$vec['documento4'],
				'espge5'=>$vec['documento5'],
				'horainicio'=>$vec['horaInicio'],
				'horatermino'=>$vec['horaTermino'],
				'fechatermino'=>$vec['fechaTermino'],
				'fechainicio'=>$vec['fechaInicio'],
				'mermareal'=>$vec['merma'],
				'cantidadutil'=>$vec['cantidadUtil'],
				'estandarturno'=>$vec['estandarTurno'],
				'fechafin'=>$vec['fechafinEstimada'],
				'horafin'=>$vec['hrfinEstimada']

				);
                }


	return $datos;
		
	}
	public function updatecontrolinsumo ($id,$fechainsumo,$fechaelaboracion,$status,$c1,$c2,$p1,$p2,$s1,$s2,$e1,$e2,$si1,$si2,$per1,$per2,$du1,$du2,$esp1,$esp2,$formacionsuaje,$filmadoplacas,$entregaplacas,$col1,$col2){
		if(!$this->userfunctions->islogged()){
			
		}

	$datos=array();
		$result=$this->app->getrecord("pedidos_control","*"," where id=$id");
		//trigger_error(print_r($result,true));
		
		if (sizeof($result) > 0) {
		
			if($this->app->updaterecord("pedidos_control","fechaelaboracion='$fechaelaboracion',fechainsumos='$fechainsumo',status='$status',cartonOrden='$c1',cartonFecha='$c2',singleOrden='$si1',singleFecha='$si2',pantoneOrden='$p1',pantoneFecha='$p2',suajeOrden='$s1',suajeFecha='$s2',embalajeOrden='$e1',embalajeFecha='$e2',personalFechajunta='$per1',personalFechacontratacion='$per2',dummyFechajunta='$du1',dummyFechaaprobacion='$du2',especificacionesFechajunta='$esp1',especificacionesFechapublicacion='$esp2',formacionSuajeaprobacion='$formacionsuaje',filmadoPlacasaprobacion='$filmadoplacas',placasEntrega='$entregaplacas',pruebacolorFechaemision='$col1',pruebacolorFechaaprobacion='$col2' ","where id=$id")){
					$datos[]=array(
						'exito'=>'si',
						'folio'=>$id
					);		
			}		
				
		} 
		else{
			
			$datos[]=array(
					'exito'=>'sin',
					'folio'=>0
				);
		}

	return $datos;	
	}

	public function eficaciaeficienciaall($status){
		if(!$this->userfunctions->islogged()){
			
		}

	$datos=array();
		if($status==1.7){
			$result=$this->app->getrecord("rel_procesosorden"," procesos.estandarTurno,rel_procesosorden.folio,rel_procesosorden.idOrden,rel_procesosorden.fechaEmision,productos.nombreProducto,productos.codigoProducto,productos.descripcion,procesos.proceso,procesos.descripcionProceso,ordenmaestra.cantidad as 'cantidadProducir',maquinas.tiempoAjuste,maquinas.nombre as 'maquina',concat(persona.nombre,' ',persona.primerApellido,' ',persona.segundoApellido) as 'operador',procesos.piezasFormato,procesos.medidaEntrada,rel_procesosorden.mermaEstimada,rel_procesosorden.cantidad as 'cantidadTicket',rel_procesosorden.fechaRequerida,rel_procesosorden.horaRequerida,procesos.especificacion1,procesos.especificacion2,procesos.especificacion3,procesos.especificacion4,procesos.especificacion5,rel_procesosorden.horaInicio,rel_procesosorden.horaTermino,rel_procesosorden.fechaTermino,rel_procesosorden.merma,rel_procesosorden.cantidadUtil"," INNER JOIN ordenmaestra on ordenmaestra.id=rel_procesosorden.idOrden INNER JOIN productos on productos.id=ordenmaestra.idProducto INNER JOIN procesos on procesos.id=rel_procesosorden.idProceso INNER JOIN maquinas on maquinas.id=procesos.maquina INNER JOIN usuarios on usuarios.id=rel_procesosorden.idOperador INNER join persona on persona.id=usuarios.idPersona where (ordenmaestra.idStatus=1 or ordenmaestra.idStatus=7) and ordenmaestra.activo like '1'");
			
			for($a=0;$a<sizeof($result);$a++){
				$vec=$result[$a];
			if($vec['cantidadTicket']>0){
				$eficiencia=$vec['cantidadUtil']/$vec['cantidadTicket'];
			}else{
				$eficiencia=0;
			}

			//if($vec['cantidadTicket']>0){
			if($vec['cantidadUtil']>0){
				//$tiempoproceso=(($vec['cantidadTicket']/$vec['estandarTurno'])*480)+$vec['tiempoAjuste'];
				$tiempoproceso=(($vec['cantidadUtil']/$vec['estandarTurno'])*480)+$vec['tiempoAjuste'];
			}else{
				$tiempoproceso=0;
			}
			$time1 = strtotime($vec['horaInicio']);
			$time2 = strtotime($vec['horaTermino']);
			$difference = abs($time2 - $time1) / 60;
			if($difference>0){
				$eficacia=$tiempoproceso/$difference;
			}else{
				$eficacia=0;
			}
			$productividad=($eficiencia*.5)+($eficacia*.5);
				$datos[] = array(
					'folio'=>$vec['folio'],
					'idorden'=>$vec['idOrden'],
					'fechaemision'=>$vec['fechaEmision'],
					'producto'=>$vec['nombreProducto'],
					'sae'=>$vec['codigoProducto'],
					'descripcionproducto'=>$vec['descripcion'],
						'proceso'=>$vec['proceso'],
					'descripcionproceso'=>$vec['descripcionProceso'],
					'cantidadproducir'=>$vec['cantidadProducir'],
					'maquina'=>$vec['maquina'],
					'operador'=>$vec['operador'],
					'piezasformato'=>$vec['piezasFormato'],
					'um'=>$vec['medidaEntrada'],
					'mermadada'=>$vec['mermaEstimada'],
					'cantidadticket'=>$vec['cantidadTicket'],
					'fecharequerida'=>$vec['fechaRequerida'],
					'horarequerida'=>$vec['horaRequerida'],
					'esp1'=>$vec['especificacion1'],
					'esp2'=>$vec['especificacion2'],
					'esp3'=>$vec['especificacion3'],
					'esp4'=>$vec['especificacion4'],
					'esp5'=>$vec['especificacion5'],
					'horainicio'=>$vec['horaInicio'],
					'horatermino'=>$vec['horaTermino'],
					'fechatermino'=>$vec['fechaTermino'],
					'mermareal'=>$vec['merma'],
					'cantidadutil'=>$vec['cantidadUtil'],
					'estandarturno'=>$vec['estandarTurno'],
					'eficiencia'=>$eficiencia,
					'eficacia'=>$eficacia,
					'productividad'=>$productividad
				);
				
			}

		}else {
			$result=$this->app->getrecord("rel_procesosorden","procesos.estandarTurno,rel_procesosorden.folio,rel_procesosorden.idOrden,rel_procesosorden.fechaEmision,productos.nombreProducto,productos.codigoProducto,productos.descripcion,procesos.proceso,procesos.descripcionProceso,ordenmaestra.cantidad as 'cantidadProducir',maquinas.tiempoAjuste,maquinas.nombre as 'maquina',concat(persona.nombre,' ',persona.primerApellido,' ',persona.segundoApellido) as 'operador',procesos.piezasFormato,procesos.medidaEntrada,rel_procesosorden.mermaEstimada,rel_procesosorden.cantidad as 'cantidadTicket',rel_procesosorden.fechaRequerida,rel_procesosorden.horaRequerida,procesos.especificacion1,procesos.especificacion2,procesos.especificacion3,procesos.especificacion4,procesos.especificacion5,rel_procesosorden.horaInicio,rel_procesosorden.horaTermino,rel_procesosorden.fechaTermino,rel_procesosorden.merma,rel_procesosorden.cantidadUtil"," INNER JOIN ordenmaestra on ordenmaestra.id=rel_procesosorden.idOrden INNER JOIN productos on productos.id=ordenmaestra.idProducto INNER JOIN procesos on procesos.id=rel_procesosorden.idProceso INNER JOIN maquinas on maquinas.id=procesos.maquina INNER JOIN usuarios on usuarios.id=rel_procesosorden.idOperador INNER join persona on persona.id=usuarios.idPersona where  ordenmaestra.idStatus=$status and ordenmaestra.activo like '1'");
			
			for($a=0;$a<sizeof($result);$a++){
				$vec=$result[$a];
			if($vec['cantidadTicket']>0){
				$eficiencia=$vec['cantidadUtil']/$vec['cantidadTicket'];
			}else{
				$eficiencia=0;
			}

			//if($vec['cantidadTicket']>0){
			if($vec['cantidadUtil']>0){
				//$tiempoproceso=(($vec['cantidadTicket']/$vec['estandarTurno'])*480)+$vec['tiempoAjuste'];
				$tiempoproceso=(($vec['cantidadUtil']/$vec['estandarTurno'])*480)+$vec['tiempoAjuste'];
			}else{
				$tiempoproceso=0;
			}
			$time1 = strtotime($vec['horaInicio']);
			$time2 = strtotime($vec['horaTermino']);
			$difference = abs($time2 - $time1) / 60;
			if($difference>0){
				$eficacia=$tiempoproceso/$difference;
			}else{
				$eficacia=0;
			}
			$productividad=($eficiencia*.5)+($eficacia*.5);
			$datos[] = array(
				'folio'=>$vec['folio'],
				'idorden'=>$vec['idOrden'],
				'fechaemision'=>$vec['fechaEmision'],
				'producto'=>$vec['nombreProducto'],
				'sae'=>$vec['codigoProducto'],
				'descripcionproducto'=>$vec['descripcion'],
				'proceso'=>$vec['proceso'],
				'descripcionproceso'=>$vec['descripcionProceso'],
				'cantidadproducir'=>$vec['cantidadProducir'],
				'maquina'=>$vec['maquina'],
				'operador'=>$vec['operador'],
				'piezasformato'=>$vec['piezasFormato'],
				'um'=>$vec['medidaEntrada'],
				'mermadada'=>$vec['mermaEstimada'],
				'cantidadticket'=>$vec['cantidadTicket'],
				'fecharequerida'=>$vec['fechaRequerida'],
				'horarequerida'=>$vec['horaRequerida'],
				'esp1'=>$vec['especificacion1'],
				'esp2'=>$vec['especificacion2'],
				'esp3'=>$vec['especificacion3'],
				'esp4'=>$vec['especificacion4'],
				'esp5'=>$vec['especificacion5'],
				'horainicio'=>$vec['horaInicio'],
				'horatermino'=>$vec['horaTermino'],
				'fechatermino'=>$vec['fechaTermino'],
				'mermareal'=>$vec['merma'],
				'cantidadutil'=>$vec['cantidadUtil'],
				'estandarturno'=>$vec['estandarTurno'],
				'eficiencia'=>$eficiencia,
				'eficacia'=>$eficacia,
				'productividad'=>$productividad
				);
				
			}

		}
		


	return $datos;
		
	}
	public function eficaciaeficienciabyorder($status,$orden){
		if(!$this->userfunctions->islogged()){
			
		}

	$datos=array();
		
		
			$result=$this->app->getrecord("rel_procesosorden"," procesos.estandarTurno,rel_procesosorden.folio,rel_procesosorden.idOrden,rel_procesosorden.fechaEmision,productos.nombreProducto,productos.codigoProducto,productos.descripcion,procesos.proceso,procesos.descripcionProceso,ordenmaestra.cantidad as 'cantidadProducir',maquinas.tiempoAjuste,maquinas.nombre as 'maquina',concat(persona.nombre,' ',persona.primerApellido,' ',persona.segundoApellido) as 'operador',procesos.piezasFormato,procesos.medidaEntrada,rel_procesosorden.mermaEstimada,rel_procesosorden.cantidad as 'cantidadTicket',rel_procesosorden.fechaRequerida,rel_procesosorden.horaRequerida,procesos.especificacion1,procesos.especificacion2,procesos.especificacion3,procesos.especificacion4,procesos.especificacion5,rel_procesosorden.horaInicio,rel_procesosorden.horaTermino,rel_procesosorden.fechaTermino,rel_procesosorden.merma,rel_procesosorden.cantidadUtil"," INNER JOIN ordenmaestra on ordenmaestra.id=rel_procesosorden.idOrden INNER JOIN productos on productos.id=ordenmaestra.idProducto INNER JOIN procesos on procesos.id=rel_procesosorden.idProceso INNER JOIN maquinas on maquinas.id=procesos.maquina INNER JOIN operadores on operadores.id=rel_procesosorden.idOperador INNER join persona on persona.id=operadores.idPersona where  ordenmaestra.activo like '1' and ordenmaestra.id=$orden");
			
			for($a=0;$a<sizeof($result);$a++){
				$vec=$result[$a];
			if($vec['cantidadTicket']>0){
				$eficiencia=$vec['cantidadUtil']/$vec['cantidadTicket'];
			}else{
				$eficiencia=0;
			}

			//if($vec['cantidadTicket']>0){
			if($vec['cantidadUtil']>0){
				//$tiempoproceso=(($vec['cantidadTicket']/$vec['estandarTurno'])*480)+$vec['tiempoAjuste'];
				$tiempoproceso=(($vec['cantidadUtil']/$vec['estandarTurno'])*480)+$vec['tiempoAjuste'];
			}else{
				$tiempoproceso=0;
			}
			$time1 = strtotime($vec['horaInicio']);
			$time2 = strtotime($vec['horaTermino']);
			$difference = abs($time2 - $time1) / 60;
			if($difference>0){
				$eficacia=$tiempoproceso/$difference;
			}else{
				$eficacia=0;
			}
			$productividad=($eficiencia*.5)+($eficacia*.5);
			
			$datos[] = array(
				'folio'=>$vec['folio'],
				'idorden'=>$vec['idOrden'],
				'fechaemision'=>$vec['fechaEmision'],
				'producto'=>$vec['nombreProducto'],
				'sae'=>$vec['codigoProducto'],
				'descripcionproducto'=>$vec['descripcion'],
				'proceso'=>$vec['proceso'],
				'descripcionproceso'=>$vec['descripcionProceso'],
				'cantidadproducir'=>$vec['cantidadProducir'],
				'maquina'=>$vec['maquina'],
				'operador'=>$vec['operador'],
				'piezasformato'=>$vec['piezasFormato'],
				'um'=>$vec['medidaEntrada'],
				'mermadada'=>$vec['mermaEstimada'],
				'cantidadticket'=>$vec['cantidadTicket'],
				'fecharequerida'=>$vec['fechaRequerida'],
				'horarequerida'=>$vec['horaRequerida'],
				'esp1'=>$vec['especificacion1'],
				'esp2'=>$vec['especificacion2'],
				'esp3'=>$vec['especificacion3'],
				'esp4'=>$vec['especificacion4'],
				'esp5'=>$vec['especificacion5'],
				'horainicio'=>$vec['horaInicio'],
				'horatermino'=>$vec['horaTermino'],
				'fechatermino'=>$vec['fechaTermino'],
				'mermareal'=>$vec['merma'],
				'cantidadutil'=>$vec['cantidadUtil'],
				'estandarturno'=>$vec['estandarTurno'],
				'eficiencia'=>$eficiencia,
				'eficacia'=>$eficacia,
				'productividad'=>$productividad
				);
				
			}
		
	
    
	return $datos;
	}

///////////////////////////////////////////////FUNCTIONS FOR REPORT OF GENERAL AVERAGE OF "EFICIENCIA, EFICACIA AND PRODUCTIVIDAD"////////////////////////////////////////////////

	public function averageeficienciabymachine($idmaquina,$fechainicio,$fechafin){
		if(!$this->userfunctions->islogged()){
			
		}

	$datos=array();
	
	$promproductividad=0;
	$promeficacia=0;
	$promeficiencia=0;
	
	$promproductividadtot=0;
	$promeficaciatot=0;
	$promeficienciatot=0;

	$result=$this->app->getrecord("rel_procesosorden","procesos.estandarTurno,rel_procesosorden.folio,rel_procesosorden.idOrden,rel_procesosorden.fechaEmision,productos.nombreProducto,productos.codigoProducto,productos.descripcion,procesos.proceso,procesos.descripcionProceso,ordenmaestra.cantidad as 'cantidadProducir',maquinas.tiempoAjuste,maquinas.nombre as 'maquina',concat(persona.nombre,' ',persona.primerApellido,' ',persona.segundoApellido) as 'operador',procesos.piezasFormato,procesos.medidaEntrada,rel_procesosorden.mermaEstimada,rel_procesosorden.cantidad as 'cantidadTicket',rel_procesosorden.fechaRequerida,rel_procesosorden.horaRequerida,procesos.especificacion1,procesos.especificacion2,procesos.especificacion3,procesos.especificacion4,procesos.especificacion5,rel_procesosorden.horaInicio,rel_procesosorden.horaTermino,rel_procesosorden.fechaTermino,rel_procesosorden.merma,rel_procesosorden.cantidadUtil"," INNER JOIN ordenmaestra on ordenmaestra.id=rel_procesosorden.idOrden INNER JOIN productos on productos.id=ordenmaestra.idProducto INNER JOIN procesos on procesos.id=rel_procesosorden.idProceso INNER JOIN maquinas on maquinas.id=procesos.maquina INNER JOIN usuarios on usuarios.id=rel_procesosorden.idOperador INNER join persona on persona.id=usuarios.idPersona where  maquinas.id=$idmaquina and rel_procesosorden.fechaTermino between '$fechainicio' and '$fechafin'");
			
			for($a=0;$a<sizeof($result);$a++){
				$vec=$result[$a];
				if($vec['cantidadTicket']>0){
					$eficiencia=$vec['cantidadUtil']/$vec['cantidadTicket'];
				}else{
					$eficiencia=0;
				}

				if($vec['cantidadTicket']>0){
					$tiempoproceso=(($vec['cantidadTicket']/$vec['estandarTurno'])*480)+$vec['tiempoAjuste'];
				}else{
					$tiempoproceso=0;
				}
				$time1 = strtotime($vec['horaInicio']);
				$time2 = strtotime($vec['horaTermino']);
				$difference = abs($time2 - $time1) / 60;
				if($difference>0){
					$eficacia=$tiempoproceso/$difference;
				}else{
					$eficacia=0;
				}
				$productividad=($eficiencia*.5)+($eficacia*.5);

				$promproductividad+=$productividad;
				$promeficacia+=$eficacia;
				$promeficiencia+=$eficiencia;
			}					
	
				$promproductividadtot=$promproductividad/sizeof($result);
				$promeficaciatot=$promeficacia/sizeof($result);
				$promeficienciatot=$promeficiencia/sizeof($result);
				$vec=$result[0];
			$datos[] = array(
				'folio'=>$vec['folio'],
				'idorden'=>$vec['idOrden'],
				'fechaemision'=>$vec['fechaEmision'],
				'producto'=>$vec['nombreProducto'],
				'sae'=>$vec['codigoProducto'],
				'descripcionproducto'=>$vec['descripcion'],
				'proceso'=>$vec['proceso'],
				'descripcionproceso'=>$vec['descripcionProceso'],
				'cantidadproducir'=>$vec['cantidadProducir'],
				'maquina'=>$vec['maquina'],
				'operador'=>$vec['operador'],
				'piezasformato'=>$vec['piezasFormato'],
				'um'=>$vec['medidaEntrada'],
				'mermadada'=>$vec['mermaEstimada'],
				'cantidadticket'=>$vec['cantidadTicket'],
				'fecharequerida'=>$vec['fechaRequerida'],
				'horarequerida'=>$vec['horaRequerida'],
				'esp1'=>$vec['especificacion1'],
				'esp2'=>$vec['especificacion2'],
				'esp3'=>$vec['especificacion3'],
				'esp4'=>$vec['especificacion4'],
				'esp5'=>$vec['especificacion5'],
				'horainicio'=>$vec['horaInicio'],
				'horatermino'=>$vec['horaTermino'],
				'fechatermino'=>$vec['fechaTermino'],
				'mermareal'=>$vec['merma'],
				'cantidadutil'=>$vec['cantidadUtil'],
				'estandarturno'=>$vec['estandarTurno'],
				'eficiencia'=>$promeficienciatot,
				'eficacia'=>$promeficaciatot,
				'productividad'=>$promproductividadtot
				);
				
			

	return $datos;
	}

public function averageeficienciaallsmachine($fechainicio,$fechafin){
		if(!$this->userfunctions->islogged()){
			
		}

	$datos=array();
	
	$promproductividad=0;
	$promeficacia=0;
	$promeficiencia=0; 
	
	$promproductividadtot=0;
	$promeficaciatot=0;
	$promeficienciatot=0;

	$result2=$this->app->getrecord("maquinas","*","where activo like '1'");

	for($b=0;$b<sizeof($result2);$b++){
		$vec2=$result2[$b];
		$idmaquina=$vec2['id'];
		$promproductividad=0;
		$promeficacia=0;
		$promeficiencia=0; 
	
		$promproductividadtot=0;
		$promeficaciatot=0;
		$promeficienciatot=0;

		$result=$this->app->getrecord("rel_procesosorden","procesos.estandarTurno,rel_procesosorden.folio,rel_procesosorden.idOrden,rel_procesosorden.fechaEmision,productos.nombreProducto,productos.codigoProducto,productos.descripcion,procesos.proceso,procesos.descripcionProceso,ordenmaestra.cantidad as 'cantidadProducir',maquinas.tiempoAjuste,maquinas.nombre as 'maquina',concat(persona.nombre,' ',persona.primerApellido,' ',persona.segundoApellido) as 'operador',procesos.piezasFormato,procesos.medidaEntrada,rel_procesosorden.mermaEstimada,rel_procesosorden.cantidad as 'cantidadTicket',rel_procesosorden.fechaRequerida,rel_procesosorden.horaRequerida,procesos.especificacion1,procesos.especificacion2,procesos.especificacion3,procesos.especificacion4,procesos.especificacion5,rel_procesosorden.horaInicio,rel_procesosorden.horaTermino,rel_procesosorden.fechaTermino,rel_procesosorden.merma,rel_procesosorden.cantidadUtil"," INNER JOIN ordenmaestra on ordenmaestra.id=rel_procesosorden.idOrden INNER JOIN productos on productos.id=ordenmaestra.idProducto INNER JOIN procesos on procesos.id=rel_procesosorden.idProceso INNER JOIN maquinas on maquinas.id=procesos.maquina INNER JOIN usuarios on usuarios.id=rel_procesosorden.idOperador INNER join persona on persona.id=usuarios.idPersona where  maquinas.id=$idmaquina and rel_procesosorden.fechaTermino between '$fechainicio' and '$fechafin'");
			
			for($a=0;$a<sizeof($result);$a++){
				$vec=$result[$a];
				if($vec['cantidadTicket']>0){
					$eficiencia=$vec['cantidadUtil']/$vec['cantidadTicket'];
				}else{
					$eficiencia=0;
				}

				if($vec['cantidadTicket']>0){
					$tiempoproceso=(($vec['cantidadTicket']/$vec['estandarTurno'])*480)+$vec['tiempoAjuste'];
				}else{
					$tiempoproceso=0;
				}
				$time1 = strtotime($vec['horaInicio']);
				$time2 = strtotime($vec['horaTermino']);
				$difference = abs($time2 - $time1) / 60;
				if($difference>0){
					$eficacia=$tiempoproceso/$difference;
				}else{
					$eficacia=0;
				}
				$productividad=($eficiencia*.5)+($eficacia*.5);

				$promproductividad+=$productividad;
				$promeficacia+=$eficacia;
				$promeficiencia+=$eficiencia;
			}					
				if(sizeof($result)>0){
					$promproductividadtot=$promproductividad/sizeof($result);
					$promeficaciatot=$promeficacia/sizeof($result);
					$promeficienciatot=$promeficiencia/sizeof($result);
				}else{
					$promproductividadtot=$promproductividad/1;
					$promeficaciatot=$promeficacia/1;
					$promeficienciatot=$promeficiencia/1;

				}

				$vec=$result[0];
			$datos[] = array(
				'folio'=>$vec['folio'],
				'idorden'=>$vec['idOrden'],
				'fechaemision'=>$vec['fechaEmision'],
				'producto'=>$vec['nombreProducto'],
				'sae'=>$vec['codigoProducto'],
				'descripcionproducto'=>$vec['descripcion'],
				'proceso'=>$vec['proceso'],
				'descripcionproceso'=>$vec['descripcionProceso'],
				'cantidadproducir'=>$vec['cantidadProducir'],
				'maquina'=>$vec['maquina'],
				'operador'=>$vec['operador'],
				'piezasformato'=>$vec['piezasFormato'],
				'um'=>$vec['medidaEntrada'],
				'mermadada'=>$vec['mermaEstimada'],
				'cantidadticket'=>$vec['cantidadTicket'],
				'fecharequerida'=>$vec['fechaRequerida'],
				'horarequerida'=>$vec['horaRequerida'],
				'esp1'=>$vec['especificacion1'],
				'esp2'=>$vec['especificacion2'],
				'esp3'=>$vec['especificacion3'],
				'esp4'=>$vec['especificacion4'],
				'esp5'=>$vec['especificacion5'],
				'horainicio'=>$vec['horaInicio'],
				'horatermino'=>$vec['horaTermino'],
				'fechatermino'=>$vec['fechaTermino'],
				'mermareal'=>$vec['merma'],
				'cantidadutil'=>$vec['cantidadUtil'],
				'estandarturno'=>$vec['estandarTurno'],
				'eficiencia'=>$promeficienciatot,
				'eficacia'=>$promeficaciatot,
				'productividad'=>$promproductividadtot
				);
				
	}		

	return $datos;
	}
///////////////////////////////////////////////FUNCTIONS FOR REPORT OF STATUS ORDEN////////////////////////////////////////////////


public function statusordenofall($status,$fechainicio,$fechafinal){
		if(!$this->userfunctions->islogged()){
			
		}

	$datos=array();
		
			$result=$this->app->getrecord("ordenmaestra","ordenmaestra.fechaFin,ordenmaestra.id,ordenmaestra.pedidoSAE,ordenmaestra.idProducto,ordenmaestra.fechaEmision,ordenmaestra.fechaRequerida,ordenmaestra.cantidad,status_orden.status","inner join status_orden on status_orden.id=ordenmaestra.idStatus where ordenmaestra.fechaEmision BETWEEN  '$fechainicio' and '$fechafinal' and ordenmaestra.idStatus=$status");
			
			for($a=0;$a<sizeof($result);$a++){
				$vec=$result[$a];
				$idorden=$vec['id'];
				$idproducto=$vec['idProducto'];

				$result2=$this->app->getrecord("rel_procesosorden"," COUNT(rel_procesosorden.id) as 'tickets'","  where idOrden=$idorden");
				$vec2=$result2[0];

				$result3=$this->app->getrecord("rel_procesosorden","COUNT(rel_procesosorden.id) as 'ticketscerrados'","where rel_procesosorden.idOrden=$idorden and (rel_procesosorden.fechaTermino !='0000-00-00' or rel_procesosorden.cantidadUtil>0)");
				$vec3=$result3[0];

				$result4=$this->app->getrecord("productos","*","where id=$idproducto");
				$vec4=$result4[0];

						
				$datos[] = array(
					'idorden'=>$vec['id'],
					'pedidosae'=>$vec['pedidoSAE'],
					'fechaentrega'=>$vec['fechaRequerida'],
					'producto'=>$vec4['nombreProducto'],
					'saeproducto'=>$vec4['codigoProducto'],
					'fechafin'=>$vec['fechaFin'],
					'cantidad'=>$vec['cantidad'],
					'status'=>$vec['status'],
					'totaltickets'=>$vec2['tickets'],
					'ticketscerrados'=>$vec3['ticketscerrados']
				);
				
			}

	return $datos;
		
	}
	public function ordenallstatus($fechainicio,$fechafinal){
		if(!$this->userfunctions->islogged()){
			
		}

	$datos=array();
		
			$result=$this->app->getrecord("ordenmaestra","ordenmaestra.id,ordenmaestra.pedidoSAE,ordenmaestra.idProducto,ordenmaestra.fechaEmision,ordenmaestra.fechaFin,ordenmaestra.fechaRequerida,ordenmaestra.cantidad,status_orden.status","inner join status_orden on status_orden.id=ordenmaestra.idStatus where ordenmaestra.fechaEmision BETWEEN  '$fechainicio' and '$fechafinal'");
			
			for($a=0;$a<sizeof($result);$a++){
				$vec=$result[$a];
				$idorden=$vec['id'];
				$idproducto=$vec['idProducto'];

				$result2=$this->app->getrecord("rel_procesosorden"," COUNT(rel_procesosorden.id) as 'tickets'","  where idOrden=$idorden");
				$vec2=$result2[0];

				$result3=$this->app->getrecord("rel_procesosorden","COUNT(rel_procesosorden.id) as 'ticketscerrados'","where rel_procesosorden.idOrden=$idorden and (rel_procesosorden.fechaTermino !='0000-00-00' or rel_procesosorden.cantidadUtil>0)");
				$vec3=$result3[0];

				$result4=$this->app->getrecord("productos","*","where id=$idproducto");
				$vec4=$result4[0];

						
				$datos[] = array(
					'idorden'=>$vec['id'],
					'pedidosae'=>$vec['pedidoSAE'],
					'fechaentrega'=>$vec['fechaRequerida'],
					'fechafin'=>$vec['fechaFin'],
					'producto'=>$vec4['nombreProducto'],
					'saeproducto'=>$vec4['codigoProducto'],
					'cantidad'=>$vec['cantidad'],
					'status'=>$vec['status'],
					'totaltickets'=>$vec2['tickets'],
					'ticketscerrados'=>$vec3['ticketscerrados']
				);
				
			}

	return $datos;
		
	}
	


////////////////////////////////////////////// NOTIFICATIONS OF PLANNING CONTROL BY SMS ACROSS ANDROID APP///////////////////////// 
	public function notification($ordenes,$cotizaciones){
		//trigger_error(print_r($ordenes,true));
		//trigger_error(print_r($cotizaciones,true));
	$datos=array();	
			
				$datos[] = array(
					'insumos'=>$this->notification2(),
					'usuarios'=>$this->getusuariosnotificationactivos(),
					'cotizaciones'=>$this->getcotizacionesnotification($cotizaciones),
					'ordenes'=>$this->getordersmsg($ordenes)
				
				);
		
	return $datos;
	}
	public function notification2(){
		

	$datos=array();
		
		
		$result=$this->app->getrecord("pedidos_control","ordenmaestra.pedidoSAE,pedidos_control.fechainsumos "," INNER join ordenmaestra on ordenmaestra.id=pedidos_control.idOrden WHERE pedidos_control.fechainsumos like  DATE_ADD(CURDATE(), INTERVAL 1 DAY) and (pedidos_control.cartonFecha like '0000-00-00' or pedidos_control.singleFecha like '0000-00-00' or pedidos_control.pantoneFecha like '0000-00-00' or pedidos_control.suajeFecha like '0000-00-00' or pedidos_control.embalajeFecha like '0000-00-00' or pedidos_control.personalFechajunta like '0000-00-00' or pedidos_control.personalFechacontratacion like '0000-00-00' or  pedidos_control.especificacionesFechapublicacion like '0000-00-00' or pedidos_control.formacionSuajeaprobacion like '0000-00-00' or pedidos_control.filmadoPlacasaprobacion like '0000-00-00' or pedidos_control.placasEntrega like '0000-00-00' or pedidos_control.pruebacolorFechaemision like '0000-00-00' or pedidos_control.pruebacolorFechaaprobacion like '0000-00-00') and pedidos_control.activo like '1'");

			
			for($a=0;$a<sizeof($result);$a++){
				$vec=$result[$a];
			
			
				$datos[] = array(
					'sae'=>$vec['pedidoSAE'],
					'fechainsumos'=>$vec['fechainsumos']
				);
				
			}
		
	
    
	return $datos;
	}
	public function getusuariosnotificationactivos(){
		if(!$this->userfunctions->islogged()){
			
		}

	$datos=array();
		
		
			$result=$this->app->getrecord("usuarios_notificacion","usuarios_notificacion.id, usuarios_notificacion.area,usuarios_notificacion.puesto,usuarios_notificacion.activo, concat(persona.nombre,' ',persona.primerApellido,' ',persona.segundoApellido) as 'nombre',persona.telefono"," INNER join persona on persona.id=usuarios_notificacion.idPersona where usuarios_notificacion.activo != '0'");
			
			for($a=0;$a<sizeof($result);$a++){
				$vec=$result[$a];
			
			
				$datos[] = array(
					'id'=>$vec['id'],
					'nombre'=>$vec['nombre'],
					'telefono'=>$vec['telefono'],
					'area'=>$vec['area'],
					'puesto'=>$vec['puesto'],
					'activo'=>$vec['activo']
				);
				
			}
		
	
    
	return $datos;
	}
	public function getcotizacionesnotification($cotizaciones){
	$datos=array();
		


			for($b=0;$b<sizeof($cotizaciones);$b++){
				$id=$cotizaciones[$b]['id'];;
				$this->app->updaterecord("cotizaciones","msg='1'","where id=$id");
			}
			
			$result=$this->app->getrecord("cotizaciones","cotizaciones.id,cotizaciones.msg,cotizaciones.cliente,cotizaciones.fechaElaboracion,fechaCotizacion,cotizaciones.fechaLiberacion,concat(per1.nombre,' ',per1.primerApellido,' ',per1.segundoApellido) as 'vendedor',concat(per2.nombre,' ',per2.primerApellido,' ',per2.segundoApellido) as 'responsable'","inner JOIN usuarios as user1 on user1.id=cotizaciones.idUsuario INNER JOIN persona as per1 on per1.id=user1.idPersona inner join usuarios as user2 on user2.id=cotizaciones.idResponsable inner join persona as per2 on per2.id=user2.idPersona where cotizaciones.msg like '0'");
			
			for($a=0;$a<sizeof($result);$a++){
				$vec=$result[$a];
			
			
				$datos[] = array(
					'id'=>$vec['id'],
					'cliente'=>$vec['cliente'],
					'fechaliberacion'=>$vec['fechaLiberacion'],
					'fechaelaboracion'=>$vec['fechaElaboracion'],
					'fechacotizacion'=>$vec['fechaCotizacion'],
					'vendedor'=>$vec['vendedor'],
					'responsable'=>$vec['responsable'],
					'msg'=>$vec['msg']
				);
				
			}
		
	
    
	return $datos;	
	}
	public function getusuariosnotification(){
		if(!$this->userfunctions->islogged()){
			
		}

	$datos=array();
		
		
			$result=$this->app->getrecord("usuarios_notificacion","usuarios_notificacion.id, usuarios_notificacion.area,usuarios_notificacion.puesto,usuarios_notificacion.activo, concat(persona.nombre,' ',persona.primerApellido,' ',persona.segundoApellido) as 'nombre',persona.telefono"," INNER join persona on persona.id=usuarios_notificacion.idPersona ");
			
			for($a=0;$a<sizeof($result);$a++){
				$vec=$result[$a];
			
			
				$datos[] = array(
					'id'=>$vec['id'],
					'nombre'=>$vec['nombre'],
					'telefono'=>$vec['telefono'],
					'area'=>$vec['area'],
					'puesto'=>$vec['puesto'],
					'activo'=>$vec['activo']
				);
				
			}
		
	
    
	return $datos;
	}
	public function getusuariosnotificationspecific($telefono){
		if(!$this->userfunctions->islogged()){
			
		}

	$datos=array();
		
		
			$result=$this->app->getrecord("usuarios_notificacion","usuarios_notificacion.id, usuarios_notificacion.area,usuarios_notificacion.puesto,usuarios_notificacion.activo,persona.nombre,persona.primerApellido,persona.segundoApellido,persona.telefono"," INNER join persona on persona.id=usuarios_notificacion.idPersona where  persona.telefono like '$telefono'");
			
			for($a=0;$a<sizeof($result);$a++){
				$vec=$result[$a];
			
			
				$datos[] = array(
					'id'=>$vec['id'],
					'nombre'=>$vec['nombre'],
					'appat'=>$vec['primerApellido'],
					'apmat'=>$vec['segundoApellido'],
					'telefono'=>$vec['telefono'],
					'area'=>$vec['area'],
					'puesto'=>$vec['puesto'],
					'activo'=>$vec['activo']
				);
				
			}
		
	
    
	return $datos;
	}
	public function usuariotelefono($nombre,$appat,$apmat,$telefono,$area,$puesto,$tipo){
		if(!$this->userfunctions->islogged()){
			
		}


	$datos=array();
		
		$result=$this->app->getrecord("persona","*","where telefono like '$telefono'");
				
		if (sizeof($result) > 0) {
			$datos[]=array(
				'exito'=>'ya',
				'folio'=>0
			);
		} 
		else 
		{
				
			$id=$this->app->saverecord('persona',"null,'$nombre','$appat','$apmat','','','','','$telefono','','','','','','','','','','','','','','1','1'");			
			if($id>0 ){
				$id2=$this->app->saverecord('usuarios_notificacion',"null,$id,'$area','$puesto','$tipo'");
				if($id2>0)
    				{	

					$datos[]=array(
						'exito'=>'si',
						'folio'=>$id2
					);
				}	
			}else{
				$datos[]=array(
					'exito'=>'no',
					'folio'=>0
				);	
			}			
		}


		
	return $datos;
	
	}

	public function usuariotelefonoupdate($id,$nombre,$appat,$apmat,$telefono,$area,$puesto,$activo){
		if(!$this->userfunctions->islogged()){
			
		}

	$datos=array();
		$result=$this->app->getrecord("usuarios_notificacion","*"," where id=$id");
		//trigger_error(print_r($result,true));
		
		if (sizeof($result) > 0) {
			$vec=$result[0];
			$idpersona=$vec['idPersona'];	
			if($this->app->updaterecord("usuarios_notificacion","area='$area',puesto='$puesto',activo='$activo'","where id=$id")){
				$this->app->updaterecord("persona","nombre='$nombre',primerApellido='$appat',segundoApellido='$apmat',telefono='$telefono'","where id=$idpersona");
					$datos[]=array(
						'exito'=>'si',
						'folio'=>$id
					);		
			}		
				
		} 
		else{
			
			$datos[]=array(
					'exito'=>'sin',
					'folio'=>0
				);
		}

	return $datos;	

	
	} 

	public function getordersmsg($ordenes){
		$datos=array();
		
			//trigger_error(print_r($ordenes,true));
			for($b=0;$b<sizeof($ordenes);$b++){
				$id=$ordenes[$b]['id'];
				//trigger_error($id);
				$this->app->updaterecord("ordenmaestra","msg='1'","where id=$id");
			}

		$result=$this->app->getrecord("ordenmaestra","*","where msg like '0' AND (idStatus=4 OR idStatus=6)");
				
		for($a=0;$a<sizeof($result);$a++) {
			$vec=$result[$a];
			$datos[]=array(
				'pedidosae'=>$vec['pedidoSAE'],
				'fecharequerida'=>$vec['fechaRequerida'],
				'cantidad'=>$vec['cantidad'],
				'producido'=>$vec['producido'],
				'id'=>$vec['id'],
				'msg'=>$vec['msg'],
				'idstatus'=>$vec['idStatus']
			);
		}
	return $datos; 
	}



////////////////////////////////////////////////////////Plannin by machine//////////////////////////////////////////////////////////
	public function planningbymachineandday($idmaquina,$fechainicio,$fechafin){
		if(!$this->userfunctions->islogged()){
			
		}

	$datos=array();
		/*$result=$this->app->getrecord("rel_procesosorden","productos.nombreProducto,procesos.piezasFormato,procesos.multiploProceso,ordenmaestra.id as 'Noorden',ordenmaestra.pedidoSAE,productos.codigoProducto,maquinas.proceso,ordenmaestra.fechaEmision as 'fechaelaboracion', ordenmaestra.fechaRequerida as 'fechaentrega',rel_procesosorden.cantidadFinalCalculada as 'pliegos',maquinas.nombre as 'maquina',rel_procesosorden.cantidad as 'cantidadordenada', SEC_TO_TIME(TIMESTAMPDIFF(SECOND,concat(rel_procesosorden.fechaRequerida,' ',rel_procesosorden.horaRequerida),concat(rel_procesosorden.fechafinEstimada,' ',rel_procesosorden.hrfinEstimada))) as 'horas',rel_procesosorden.fechaRequerida as 'fechainicio',rel_procesosorden.horaRequerida as 'horainicio',rel_procesosorden.fechafinEstimada as 'fechafin',rel_procesosorden.hrfinEstimada as 'horafin'","INNER JOIN ordenmaestra on ordenmaestra.id=rel_procesosorden.idOrden INNER join productos on productos.id=ordenmaestra.idProducto INNER JOIN procesos on procesos.id=rel_procesosorden.idProceso INNER JOIN maquinas on maquinas.id=procesos.maquina where rel_procesosorden.fechaRequerida like CURDATE() and maquinas.id=$idmaquina");*/

	$result=$this->app->getrecord("rel_procesosorden","maquinas.unidadSalida,productos.nombreProducto,procesos.piezasFormato,procesos.multiploProceso,ordenmaestra.id as 'Noorden',ordenmaestra.pedidoSAE,productos.codigoProducto,maquinas.proceso,ordenmaestra.fechaEmision as 'fechaelaboracion', ordenmaestra.fechaRequerida as 'fechaentrega',rel_procesosorden.cantidadFinalCalculada as 'pliegos',maquinas.nombre as 'maquina',rel_procesosorden.cantidad as 'cantidadordenada', SEC_TO_TIME(TIMESTAMPDIFF(SECOND,concat(rel_procesosorden.fechaRequerida,' ',rel_procesosorden.horaRequerida),concat(rel_procesosorden.fechafinEstimada,' ',rel_procesosorden.hrfinEstimada))) as 'horas',rel_procesosorden.fechaRequerida as 'fechainicio',rel_procesosorden.horaRequerida as 'horainicio',rel_procesosorden.fechafinEstimada as 'fechafin',rel_procesosorden.hrfinEstimada as 'horafin'","INNER JOIN ordenmaestra on ordenmaestra.id=rel_procesosorden.idOrden INNER join productos on productos.id=ordenmaestra.idProducto INNER JOIN procesos on procesos.id=rel_procesosorden.idProceso INNER JOIN maquinas on maquinas.id=procesos.maquina where rel_procesosorden.fechaRequerida BETWEEN '$fechainicio' and  '$fechafin' and maquinas.id=$idmaquina ORDER BY rel_procesosorden.fechaRequerida ASC, rel_procesosorden.horaRequerida ASC");
		//trigger_error(print_r($result,true));
		
		for ($a=0;$a<sizeof($result);$a++) {
			$vec=$result[$a];
				
			$datos[]=array(
				'oroden'=>$vec['Noorden'],
				'pedidosae'=>$vec['pedidoSAE'],
				'codigoproducto'=>$vec['codigoProducto'],
				'proceso'=>$vec['proceso'],
				'fechaelaboracion'=>$vec['fechaelaboracion'],
				'fechaentrega'=>$vec['fechaentrega'],
				'pliegos'=>$vec['pliegos'].' '.$vec['unidadSalida'].'(s)',
				'maquina'=>$vec['maquina'],
				'cantidadordenada'=>$vec['cantidadordenada'].' Pieza(s)',
				'horas'=>$vec['horas'],
				'fechainicio'=>$vec['fechainicio'],
				'horainicio'=>$vec['horainicio'],
				'fechafin'=>$vec['fechafin'],
				'horafin'=>$vec['horafin'],
				'piezasformato'=>$vec['piezasFormato'],
				'multiploproceso'=>$vec['multiploProceso'],
				'nombreproducto'=>$vec['nombreProducto'],
				);			
		} 
		
	return $datos;	

	}

	/////////////////////////////////////////// % UTILIZACION   ////////////////////////////////////////////////////////////	
	public function porcentofutility($idmaquina,$fechainicio,$fechafin){
		if(!$this->userfunctions->islogged()){
			
		}
			$datetime1 = date_create($fechainicio);
       			$datetime2 = date_create($fechafin);
    
    			$interval = date_diff($datetime1, $datetime2);
			$days=$interval->d+1;
			$fecha1=$fechainicio;
			$fecha2=$fechafin;
			$cont=0;
			while(strtotime($fecha1) <=strtotime($fecha2)){
				
				$fechats = strtotime($fecha1);
				$daynumber=date('w',$fechats);
				if($daynumber>0){
					$cont++;
				}
				$fecha1= date('Y-m-d', strtotime($fecha1. ' + 1 days'));
			}
	$datos=array();
		
	$result=$this->app->getrecord("maquinas","maquinas.unidad_medida,maquinas.unidadSalida,maquinas.nombre,maquinas.estandar,SUM(rel_procesosorden.cantidadUtil) as 'suma'","INNER JOIN procesos on procesos.maquina=maquinas.id INNER JOIN rel_procesosorden on rel_procesosorden.idProceso=procesos.id where maquinas.id=$idmaquina and rel_procesosorden.fechaTermino BETWEEN '$fechainicio' and '$fechafin'");
		//trigger_error(print_r($result,true));
		
		for ($a=0;$a<sizeof($result);$a++) {
			$vec=$result[$a];
				
			$datos[]=array(
				'maquina'=>$vec['nombre'],
				'estandar'=>$vec['estandar'],
				'unidadmedida'=>$vec['unidad_medida'],
				'unidadsalida'=>$vec['unidadSalida'],
				'suma'=>$vec['suma'],
				'dias'=>$cont
				);			
		} 
		
	return $datos;	

	}
	public function porcentofutilityallmachine($fechainicio,$fechafin){
		if(!$this->userfunctions->islogged()){
			
		}
		 	$datetime1 = date_create($fechainicio);
       			$datetime2 = date_create($fechafin);
    
    			$interval = date_diff($datetime1, $datetime2);
			$days=$interval->d+1;
			$fecha1=$fechainicio;
			$fecha2=$fechafin;
			$cont=0;
			while(strtotime($fecha1) <=strtotime($fecha2)){
				
				$fechats = strtotime($fecha1);
				$daynumber=date('w',$fechats);
				if($daynumber>0){
					$cont++;
				}
				$fecha1= date('Y-m-d', strtotime($fecha1. ' + 1 days'));
			}

	$datos=array();
	$result2=$this->app->getrecord("maquinas","*","where activo like '1'");
	for ($b=0;$b<sizeof($result2);$b++) {
		$vec2=$result2[$b];
		$idmaquina=$vec2['id'];
		$result=$this->app->getrecord("maquinas","SUM(rel_procesosorden.cantidadUtil) as 'suma'","INNER JOIN procesos on procesos.maquina=maquinas.id INNER JOIN rel_procesosorden on rel_procesosorden.idProceso=procesos.id where maquinas.id=$idmaquina and rel_procesosorden.fechaTermino BETWEEN '$fechainicio' and '$fechafin'");
			//trigger_error(print_r($result,true));
		
			for ($a=0;$a<sizeof($result);$a++) {
				$vec=$result[$a];
				
				$datos[]=array(
					'maquina'=>$vec2['nombre'],
					'unidadmedida'=>$vec2['unidad_medida'],
					'unidadsalida'=>$vec2['unidadSalida'],
					'estandar'=>$vec2['estandar'],
					'suma'=>$vec['suma'],
					'dias'=>$cont
					);			
			} 
	}
		
	return $datos;	

	}
 	public function validateSAEbobina($codigo){
		if(!$this->userfunctions->islogged()){
			
		}

		$datos=array();
		
			$result=$this->app->getrecord("bobinas","*","where codigoSAE like '$codigo'");
			if(sizeof($result)>0)
    			{	
				$vec=$result[0];
				$datos[] = array(
					'id'=>$vec['id'],
					'valido'=>"Existe"
					);			
                	}else{
				$datos[] = array(
					'id'=>0,
					'valido'=>"Si"
					);			
			}
		
		
		
	return $datos; 
		
	}
	public function getbobinas(){
		if(!$this->userfunctions->islogged()){
			
		}
		
		$datos=array();
		$result=$this->app->getrecord("bobinas","*","order by nombre asc");
		for($a=0;$a<sizeof($result);$a++)
    		{
			$vec=$result[$a];	

			$datos[] = array(
					'id'=>$vec['id'],
					'sae'=>$vec['codigoSAE'],
					'nombre'=>$vec['nombre'],
					'largo'=>$vec['largo'],
					'ancho'=>$vec['ancho'],
					'puntos'=>$vec['puntos'],
					'gramos'=>$vec['gramos'],
					'peso'=>$vec['peso'],
					'activo'=>$vec['activo']

					);			
                }
		
		
	return $datos; 
	} 
	public function getbobinasenable(){
		if(!$this->userfunctions->islogged()){
			
		}

		$datos=array();
		$result=$this->app->getrecord("bobinas","*","where activo like '1'");
		for($a=0;$a<sizeof($result);$a++)
    		{
			$vec=$result[$a];	

			$datos[] = array(
					'id'=>$vec['id'],
					'sae'=>$vec['codigoSAE'],
					'nombre'=>$vec['nombre'],
					'largo'=>$vec['largo'],
					'ancho'=>$vec['ancho'],
					'puntos'=>$vec['puntos'],
					'gramos'=>$vec['gramos'],
					'peso'=>$vec['peso'],
					'activo'=>$vec['activo']

					);			
                }
		
		
	return $datos; 
	}
	public function deletebobina($id){
		if(!$this->userfunctions->islogged()){
			
		}

		$datos=array();
		$result=$this->app->getrecord("bobinas","*"," where id=$id");
		
		if (sizeof($result) > 0) 
		{		
			if($this->app->deleterecord("bobinas"," where id=$id")>0){
					$datos[]=array(
						'exito'=>'si'
					);					
			}		
				
		} 
		else{
			
			$datos[]=array(
					'exito'=>'sin'

				);
		}


	return $datos;

	}

	public function updatebobina ($id,$codigo,$nombre,$largo,$ancho,$puntos,$gramos,$peso,$activo){
		if(!$this->userfunctions->islogged()){
			
		}

	//trigger_error($vendedor);
		$datos=array();
		$result=$this->app->getrecord("bobinas","*"," where id= $id");
		
		if (sizeof($result) > 0) {
		
			if($this->app->updaterecord("bobinas","nombre='$nombre',largo=$largo,ancho=$ancho,puntos=$puntos,gramos=$gramos,peso=$peso,activo='$activo'"," where id=$id")){
					$datos[]=array(
						'exito'=>'si',
						'folio'=>$id
					);
							
			}	
				
		} 
		else{
			
			$datos[]=array(
					'exito'=>'sin',
					'folio'=>0
				);
		}
	return $datos;	
	}
	public function savebobina($codigo,$nombre,$largo,$ancho,$puntos,$gramos,$peso){
		if(!$this->userfunctions->islogged()){
			
		}

	$datos=array();
		$result=$this->app->getrecord("bobinas","*","where codigoSAE like '$codigo'");
				
		if (sizeof($result) > 0) {
			$datos[]=array(
				'exito'=>'ya',
				'folio'=>0
			);
		} 
		else 
		{
			
						
			$id=$this->app->saverecord("bobinas","null,'$codigo','$nombre',$largo,$ancho,$puntos,$gramos,$peso,'1'");			
			if($id>0 ){
				
				$datos[]=array(
					'exito'=>'si',
					'folio'=>$id
				);	
			}else{
				$datos[]=array(
					'exito'=>'no',
					'folio'=>0
				);	
			}			
		}


		
	return $datos;
	}
/////////////////////////////////////////////////////////FUNCTION FOR UPDATE STATUS ORDEN AUTOMAICALLY////////////////////////////////////////////////////////////////////

	public function checkstatusorden(/*$idorden=133*/){
		if(!$this->userfunctions->islogged()){
			
		}

	$datos=array();
		date_default_timezone_set('America/Mexico_City');

		$result=$this->app->getrecord("ordenmaestra","*","where  idStatus=5 or idStatus=7 or idStatus=3");//id=$idorden and
				
		for($a=0;$a<sizeof($result);$a++) {
			$vec=$result[$a];
			$idorden=$vec['id'];
			$fecharequerida=$vec['fechaRequerida'];			


			$result2=$this->app->getrecord("rel_procesosorden","count(id) as 'tickets'","where idOrden=$idorden");
			$vec2=$result2[0];
			$totaltickets=$vec2['tickets'];
						
			$result3=$this->app->getrecord("rel_procesosorden","count(id) as 'tickets'","where idOrden=$idorden and fechafinEstimada !='0000-00-00' and fechafinEstimada <=DATE_SUB('$fecharequerida', INTERVAL 1 DAY) ");
			$vec3=$result3[0];
			$ticketsplaneados=$vec3['tickets'];


			$result4=$this->app->getrecord("rel_procesosorden","count(id) as 'tickets'","where idOrden=$idorden and fechafinEstimada !='0000-00-00' and fechafinEstimada >DATE_SUB('$fecharequerida', INTERVAL 1 DAY) ");
			$vec4=$result4[0];
			$ticketsplaneadosfuera=$vec4['tickets'];

			$result5=$this->app->getrecord("rel_procesosorden","count(id) as 'tickets'","where idOrden=$idorden and fechaTermino !='0000-00-00' and cantidadUtil>0 and fechaTermino <=DATE_SUB('$fecharequerida', INTERVAL 1 DAY)  ");
			$vec5=$result5[0];
			$ticketscerradosbn=$vec5['tickets'];

			$result6=$this->app->getrecord("rel_procesosorden","count(id) as 'tickets'","where idOrden=$idorden and fechaTermino !='0000-00-00' and cantidadUtil>0 and fechaTermino >DATE_SUB('$fecharequerida', INTERVAL 1 DAY)  ");
			$vec6=$result6[0];
			$ticketscerradosmal=$vec6['tickets'];

			
			$result7=$this->app->getrecord("rel_procesosorden","count(id) as 'tickets'","where idOrden=$idorden and fechaTermino !='0000-00-00' and fechaTermino >DATE_SUB('$fecharequerida', INTERVAL 1 DAY) ");
			$vec7=$result7[0];
			$ticketscerradosfuera=$vec7['tickets'];

			$fecha = date_create($fecharequerida);
			date_sub($fecha, date_interval_create_from_date_string('1 day'));
			$requerida=date_format($fecha, 'Y-m-d');
			$hoy = date("Y-m-d"); 

			$datetime1 = new DateTime($hoy );
			$datetime2 = new DateTime($requerida);
			$interval = $datetime1->diff($datetime2);
			$dif= $interval->format('%R%a');	
						
			//trigger_error("tottickets:".$totaltickets." tickplaneados".$ticketsplaneados." fechareq".$fecharequerida." fchareq2 ".$requerida." dif".$dif." ticketscerradossbn ".$ticketscerradosbn." mal".$ticketscerradosmal." planeadosfuera".$ticketsplaneadosfuera);

			if($dif<=0){
				//trigger_error("Retrazada");
				$this->app->updaterecord("ordenmaestra"," idStatus=3","where id=$idorden");
			}else{

					
				if(($ticketsplaneadosfuera>0  || $ticketscerradosfuera>0)&& (($ticketscerradosbn+$ticketscerradosmal)<$totaltickets)){
					//trigger_error("retrazada");
					$this->app->updaterecord("ordenmaestra"," idStatus=3","where id=$idorden");
				}else{

					if($ticketsplaneados>0){
						//trigger_error("En proceso");
						$this->app->updaterecord("ordenmaestra"," idStatus=7","where id=$idorden");
						
					
				
						if($totaltickets==$ticketscerradosbn){
							//trigger_error("Finalizada");
							$this->app->updaterecord("ordenmaestra"," idStatus=4,fechaFin=now()","where id=$idorden");
						}else{
							if(($ticketscerradosbn+$ticketscerradosmal)==$totaltickets){
								//trigger_error("Finalizada-retrazo");
								$this->app->updaterecord("ordenmaestra"," idStatus=6 ,fechaFin=now()","where id=$idorden");
								$this->app->updaterecord("pedidos_control","activo='0'","where idOrden=$id");
							}else if(($ticketscerradosbn+$ticketscerradosmal)<$totaltickets && $ticketscerradosmal>0){
								//trigger_errror("retrazada");
								$this->app->updaterecord("ordenmaestra"," idStatus=3","where id=$idorden");
							}else if(($ticketscerradosbn+$ticketscerradosmal)<$totaltickets && $ticketscerradosmal<=0){
								//trigger_error("En proceso");
								$this->app->updaterecord("ordenmaestra"," idStatus=7","where id=$idorden");
							}
						}
					}else{
						//trigger_error("Sin Planear");
						$this->app->updaterecord("ordenmaestra"," idStatus=5","where id=$idorden");
					}
				

				}
			}		
			

		}
				$datos[]=array(
					'exito'=>'si',
					'folio'=>0
				);
	return $datos;
	}

/////////////////////////////////////////////////////FUNCTIONS FOR COTIZADOR///////////////////////////////////////////////////
	////////////////////////////////////////////FUNCTIONS FOR PRODUCTS///////////////////////////////////////////
	public function getgiros(){
		$datos=array();
		$result=$this->app->getrecord("giros","*","where activo like '1' ");
		for($a=0;$a<sizeof($result);$a++)
    		{
			$vec=$result[$a];	

			$datos[] = array(
					'id'=>$vec['id'],
					'nombre'=>$vec['nombre']				
					);			
                }
		
	return $datos;
	}

	public function getvendedores(){
	$datos=array();
		$result=$this->app->getrecord("usuarios","usuarios.id,usuarios.operador,usuarios.vendedor,persona.nombre,persona.primerApellido,persona.segundoApellido,usuarios.usuario,usuarios.activo","INNER JOIN persona on persona.id=usuarios.idPersona where usuarios.vendedor like '1'");
		for($a=0;$a<sizeof($result);$a++)
    		{
			$vec=$result[$a];
//trigger_error($vec['id']);
			$datos[] = array(
				'id'=>$vec['id'],
				'nombre'=>$vec['nombre'],
				'appat'=>$vec['primerApellido'],
				'apmat'=>$vec['segundoApellido'],
				'usuario'=>$vec['usuario'],
				'activo'=>$vec['activo'],
				'operador'=>$vec['operador'],
				'vendedor'=>$vec['vendedor']
				);
		
                }
		
		
	return $datos;

	}

	public function saveclient($iduser,$razon,$giro,$domicilio,$nombre,$appat,$apmat,$telefono,$area,$puesto,$etiqueta,$carton,$corrugado,$caja,$display,$tiraje,$variable,$metalizado,$ingenieria,$web,$ventas,$costosdirectos,$costosnoevidentes,$participacion,$satisfaccion,$productos,$consumidor,$canal,$competencia,$vision,$estimacionventa,$nececidades,$problematica,$proveedores,$espectativas,$exporta){
	$datos=array();
		//$idpersona=$this->app->saverecord('persona',"null,'$nombre','$appat','$apmat','','','','','$telefono','','','$area','$puesto','','','','','','','','','','0','1'");			
		$id=$this->app->saverecord("clientes","null,$iduser,'$razon','$domicilio','$giro','$exporta','$etiqueta','$carton','$corrugado','$caja','$display','$tiraje','$variable','$metalizado','$ingenieria','$web','$ventas','$costosdirectos','$costosnoevidentes','$participacion','$satisfaccion','$productos','$consumidor','$canal','$competencia','$vision','$estimacionventa','$nececidades','$problematica','$proveedores','$espectativas','1'");
			if($id>0 ){
				
				$datos[]=array(
					'exito'=>'si',
					'folio'=>$id
				);	
			}else{
				$datos[]=array(
					'exito'=>'no',
					'folio'=>0
				);	
			}		

	return $datos;	
	}

	
	public function getclientes(){
	$datos=array();
		
		
			$result=$this->app->getrecord("clientes","clientes.id,clientes.razonSocial,clientes.domicilioFiscal,concat(per2.nombre,' ',per2.primerApellido,' ',per2.segundoApellido) as 'vendedor',clientes.activo","INNER JOIN usuarios on usuarios.id=clientes.idUsuario INNER JOIN persona as per2 on per2.id=usuarios.idPersona");
			
			for($a=0;$a<sizeof($result);$a++){
				$vec=$result[$a];
			
			
				$datos[] = array(
					'id'=>$vec['id'],
					
					'razonsocial'=>$vec['razonSocial'],
					'domicilio'=>$vec['domicilioFiscal'],
					'vendedor'=>$vec['vendedor'],
					'activo'=>$vec['activo']
				);
				
			}
		
	
    
	return $datos;
	}
	public function getclientesenables(){
	$datos=array();
		
		
			$result=$this->app->getrecord("clientes","clientes.id,per1.telefono,clientes.razonSocial,clientes.domicilioFiscal,concat(per2.nombre,' ',per2.primerApellido,' ',per2.segundoApellido) as 'vendedor',clientes.activo","INNER JOIN persona as per1 on per1.id=clientes.idPersona INNER JOIN usuarios on usuarios.id=clientes.idUsuario INNER JOIN persona as per2 on per2.id=usuarios.idPersona where clientes.activo like '1'");
			
			for($a=0;$a<sizeof($result);$a++){
				$vec=$result[$a];
			
			
				$datos[] = array(
					'id'=>$vec['id'],
					
					'razonsocial'=>$vec['razonSocial'],
					'domicilio'=>$vec['domicilioFiscal'],
					'vendedor'=>$vec['vendedor'],
					'activo'=>$vec['activo']
				);
				
			}
		
	
    
	return $datos;
	}

	public function getclientesbyvendedor($idvendedor){
	$datos=array();
		
		
			$result=$this->app->getrecord("clientes","clientes.id,,per1.telefono,clientes.razonSocial,clientes.domicilioFiscal,clientes.activo","INNER JOIN persona as per1 on per1.id=clientes.idPersona INNER JOIN usuarios on usuarios.id=clientes.idUsuario INNER JOIN persona as per2 on per2.id=usuarios.idPersona where clientes.idUsuario=$idvendedor");
			
			for($a=0;$a<sizeof($result);$a++){
				$vec=$result[$a];
			
			
				$datos[] = array(
					'id'=>$vec['id'],
					'contacto'=>$vec['contacto'],
					'telefono'=>$vec['telefono'],
					'razonsocial'=>$vec['razonSocial'],
					'domicilio'=>$vec['domicilioFiscal'],
					'vendedor'=>$vec['vendedor'],
					'activo'=>$vec['activo']
				);
				
			}
		
	
    
	return $datos;
	}
	public function getclientesbyvendedorenables($idvendedor){
	$datos=array();
		
		
			$result=$this->app->getrecord("clientes","clientes.id,per1.telefono,clientes.razonSocial,clientes.domicilioFiscal,concat(per2.nombre,' ',per2.primerApellido,' ',per2.segundoApellido) as 'vendedor',clientes.activo","INNER JOIN persona as per1 on per1.id=clientes.idPersona INNER JOIN usuarios on usuarios.id=clientes.idUsuario INNER JOIN persona as per2 on per2.id=usuarios.idPersona where clientes.idUsuario=$idvendedor and clientes.activo like '1'");
			
			for($a=0;$a<sizeof($result);$a++){
				$vec=$result[$a];
			
			
				$datos[] = array(
					'id'=>$vec['id'],
					
					'razonsocial'=>$vec['razonSocial'],
					'domicilio'=>$vec['domicilioFiscal'],
					'vendedor'=>$vec['vendedor'],
					'activo'=>$vec['activo']
				);
				
			}
		
	
    
	return $datos;
	}

	public function deletecliente($id){
//trigger_error($vendedor);
		$datos=array();
		$result=$this->app->getrecord("clientes","clientes.id,clientes.idPersona"," where clientes.id=$id");
		
		if (sizeof($result) > 0) {
			for($a=0;$a<sizeof($result);$a++)
    			{
				$vec=$result[$a];
			
				$idusuario=$vec['id'];
				$idpersona=$vec['idPersona'];
						
			
				if($this->app->deleterecord("clientes","where clientes.id=$id")>0){
					if($this->app->deleterecord("persona"," where persona.id=$idpersona")>0){
						$datos[]=array(
							'exito'=>'si'
						);
							
					}		
				}		
				
                	}	
		} 
		else{
			
			$datos[]=array(
					'exito'=>'sin'

				);
		}


	return $datos;	
	}
	public function getspecificclient($id){
	$datos=array();
		
		
			$result=$this->app->getrecord("clientes","*"," where clientes.id=$id");
			
			for($a=0;$a<sizeof($result);$a++){
				$vec=$result[$a];
				
				$vec2=$result2[0];
				$datos[] = array(
					'id'=>$vec['id'],
					
					'razonsocial'=>$vec['razonSocial'],
					'domicilio'=>$vec['domicilioFiscal'],
					'giro'=>$vec['giro'],
					'exporta'=>$vec['exporta'],
					'etiqueta'=>$vec['etiqueta'],
					'carton'=>$vec['carton'],
					'corrugado'=>$vec['corrugado'],
					'caja'=>$vec['caja'],
					'display'=>$vec['display'],
					'tiraje'=>$vec['tiraje'],
					'datovariable'=>$vec['datoVariable'],
					'metalizado'=>$vec['metalizado'],
					'diseno'=>$vec['disenio'],
					'web'=>$vec['web'],
					'ventas'=>$vec['ventas'],
					'costosdirectos'=>$vec['costosDirectos'],
					'costosnoevidentes'=>$vec['costosnoEvidentes'],
					'participacion'=>$vec['participación'],		
					'satisfaccioncliente'=>$vec['satisfaccionCliente'],
					'productos'=>$vec['productos'],
					'consumidor'=>$vec['consumidor'],
					'canal'=>$vec['canal'],
					'competencia'=>$vec['competencia'],
					'vision'=>$vec['vision'],
					'ventasestimacion'=>$vec['estimacionVentasanual'],
					'necesidades'=>$vec['necesidades'],
					'problematica'=>$vec['problematica'],
					'proveedores'=>$vec['proveedores'],
					'expectativas'=>$vec['expectativas'],
					'activo'=>$vec['activo']
				);
				
			}
		
	
    
	return $datos;
	}

	public function updateclient ($idclienteedit,$razon,$giro,$domicilio,$nombre,$appat,$apmat,$telefono,$area,$puesto,$etiqueta,$carton,$corrugado,$caja,$display,$tiraje,$variable,$metalizado,$ingenieria,$web,$ventas,$costosdirectos,$costosnoevidentes,$participacion,$satisfaccion,$productos,$consumidor,$canal,$competencia,$vision,$estimacionventa,$nececidades,$problematica,$proveedores,$espectativas,$exporta,$activo){
		//trigger_error($idclienteedit);
		$datos=array();
		$result=$this->app->getrecord("clientes","*"," where id= $idclienteedit");
		
		if (sizeof($result) > 0) {
			$vec=$result[0];
			if($this->app->updaterecord("clientes","razonSocial='$razon',domicilioFiscal='$domicilio',giro='$giro',exporta='$exporta',etiqueta='$etiqueta',carton='$carton',corrugado='$corrugado',caja='$caja',display='$display',tiraje='$tiraje',datoVariable='$variable',metalizado='$metalizado',disenio='$ingenieria',web='$web',ventas='$ventas',costosDirectos='$costosdirectos',costosnoEvidentes='$costosnoevidentes',participacion='$participacion',satisfaccionCliente='$satisfaccion',productos='$productos',consumidor='$consumidor',canal='$canal',competencia='$competencia',vision='$vision',estimacionVentasanual='$estimacionventa',necesidades='$nececidades',problematica='$problematica',proveedores='$proveedores',expectativas='$espectativas',activo='$activo'"," where id=$idclienteedit")){
				
					$datos[]=array(
						'exito'=>'si',
						'folio'=>$idclienteedit
					);
							
			}	
				
		} 
		else{
			
			$datos[]=array(
					'exito'=>'sin',
					'folio'=>0
				);
		}
	return $datos;	

	}
////////////////////////////////////////////////////////////Funtions for contacto del cliente/////////////////////////////////////////

	public function savecontacto($idcliente,$nombre,$appat,$apmat,$telefono,$extension,$celular,$correo,$area,$puesto){
	$datos=array();
		$result=$this->app->getrecord("persona","*","where telefono like '$celular'");
				
		if (sizeof($result) > 0) {
			$datos[]=array(
				'exito'=>'ya',
				'folio'=>0
			);
		} 
		else 
		{
			
						
			$id=$this->app->saverecord("persona","null,'$nombre','$appat','apmat','','','','','$celular','','$correo','$area','$puesto','','','','','','','','','','','1'");			
			if($id>0 ){
				$id=$this->app->saverecord("rel_clientecontacto","null,$idcliente,$id,'$telefono','$extension','1'");	
				$datos[]=array(
					'exito'=>'si',
					'folio'=>$id
				);	
			}else{
				$datos[]=array(
					'exito'=>'no',
					'folio'=>0
				);	
			}			
		}


		
	return $datos;
	} 
	public function getallcontactos($idcliente){
	$datos=array();
		
		
			$result=$this->app->getrecord("rel_clientecontacto","rel_clientecontacto.id,concat(persona.nombre,' ',persona.primerApellido,' ',persona.segundoApellido) as 'nombre',persona.telefono as 'celular',persona.area,persona.puesto,persona.correo,rel_clientecontacto.telefono,rel_clientecontacto.extension "," INNER JOIN persona on persona.id=rel_clientecontacto.idPersona where rel_clientecontacto.idCliente=$idcliente");
			
			for($a=0;$a<sizeof($result);$a++){
				$vec=$result[$a];
				
				$vec2=$result2[0];
				$datos[] = array(
					'id'=>$vec['id'],
					'nombre'=>$vec['nombre'],
					'celular'=>$vec['celular'],
					'area'=>$vec['area'],
					'puesto'=>$vec['puesto'],
					'correo'=>$vec['correo'],
					'telefono'=>$vec['telefono'],
					'extension'=>$vec['extension'],
					
				);
				
			}
		
	
    
	return $datos;	
	}

	public function updatecontacto($id,$nombre,$appat,$apmat,$telefono,$extension,$celular,$correo,$area,$puesto){
		$datos=array();
		$result=$this->app->getrecord("clientes","*"," where id= $idclienteedit");
		
		if (sizeof($result) > 0) {
			$vec=$result[0];
			if($this->app->updaterecord("clientes","razonSocial='$razon',domicilioFiscal='$domicilio',giro='$giro',exporta='$exporta',etiqueta='$etiqueta',carton='$carton',corrugado='$corrugado',caja='$caja',display='$display',tiraje='$tiraje',datoVariable='$variable',metalizado='$metalizado',disenio='$ingenieria',web='$web',ventas='$ventas',costosDirectos='$costosdirectos',costosnoEvidentes='$costosnoevidentes',participacion='$participacion',satisfaccionCliente='$satisfaccion',productos='$productos',consumidor='$consumidor',canal='$canal',competencia='$competencia',vision='$vision',estimacionVentasanual='$estimacionventa',necesidades='$nececidades',problematica='$problematica',proveedores='$proveedores',expectativas='$espectativas',activo='$activo'"," where id=$idclienteedit")){
				
					$datos[]=array(
						'exito'=>'si',
						'folio'=>$idclienteedit
					);
							
			}	
				
		} 
		else{
			
			$datos[]=array(
					'exito'=>'sin',
					'folio'=>0
				);
		}
	return $datos;	

	}
////////////////////////////////////////////////////////FUNCTIONS FOR PRODUCTS/////////////////////////////////////////////////////////

	public function savecotizacionproducto($iduser,$cliente,$tipo,$tipoproducto,$idunico,$ancho,$largo,$alto,$vtaalargo,$vtaaalto,$vtablargo,$vtabalto,$categoria,$sustrato,$sustratoindirecto,$codigo,$tipotinta,$tintafrente,$tintareverso,$barniz,$laminado,$arte,$artefrente,$artevuelta,$pegado,$suaje,$pantones,$especpantone,$empalme,$costo,$especempaque,$notas,$formapago,$formaentrega,$margen,$vigencia,$escalauno,$escalados,$escalatres,$escalacuatro,$escalacinco,$escalaseis,$herramental,$dummy,$justificaciondummy){
	$datos=array();
		
		$id=$this->app->saverecord("cotizaciones","null,'$cliente','$tipo','$tipoproducto','$idunico',1,$iduser,now(),'','1'");
			if($id>0 ){
				$this->app->saverecord("dimensiones","null,$id,$ancho,$largo,$alto,$vtaalargo,$vtaaalto,$vtablargo,$vtabalto,'1'");
				$this->app->saverecord("sustratos","null,$id,'$categoria','$sustrato','$sustratoindirecto','$codigo','1'");
				$this->app->saverecord("tintas","null,$id,'$tipotinta','$tintafrente','$tintareverso','$barniz','$laminado',$arte,$artefrente,$artevuelta,'$pegado','$suaje',$pantones,'$especpantone','$empalme',$costo,'$especempaque','$notas','1'");
				$this->app->saverecord("escalasvolumen","null,$id,'$escalauno','$escalados','$escalatres','$escalacuatro','$escalacinco','$escalaseis','$herramental','$dummy','$justificaciondummy','1'");
				$this->app->saverecord("terminoscondiciones","null,$id,'$formapago','$formaentrega','$margen','$vigencia','1'");
				$datos[]=array(
					'exito'=>'si',
					'folio'=>$id
				);	
			}else{
				$datos[]=array(
					'exito'=>'no',
					'folio'=>0
				);	
			}		

	return $datos;
	
	}

	public function gettipoproductos(){
	$datos=array();
		
		
			$result=$this->app->getrecord("complementos","*"," where categoria=20 and status=1");
			
			for($a=0;$a<sizeof($result);$a++){
				$vec=$result[$a];
			
			
				$datos[] = array(
					'id'=>$vec['id'],
					'nombre'=>$vec['nombre'],
					);
				
			}
		
	
    
	return $datos;
	}
	public function getcategoriascomplementos(){
	$datos=array();
		
		
			$result=$this->app->getrecord("complementos","*"," where categoria=17 and status=1");
			
			for($a=0;$a<sizeof($result);$a++){
				$vec=$result[$a];
			
			
				$datos[] = array(
					'id'=>$vec['id'],
					'nombre'=>$vec['nombre'],
					);
				
			}
		
	
    
	return $datos;
	}
	public function gettiposustrato($id){
	$datos=array();
		
		
			$result=$this->app->getrecord("complementos","*"," where descripcion like '$id' and status=1");
			
			for($a=0;$a<sizeof($result);$a++){
				$vec=$result[$a];
			
			
				$datos[] = array(
					'id'=>$vec['id'],
					'nombre'=>$vec['nombre'],
					);
				
			}
		
	
    
	return $datos;
	}

	public function gettipotinta(){
	$datos=array();
		
		
			$result=$this->app->getrecord("complementos","*"," where categoria=2 and status=1");
			
			for($a=0;$a<sizeof($result);$a++){
				$vec=$result[$a];
			
			
				$datos[] = array(
					'id'=>$vec['id'],
					'nombre'=>$vec['nombre'],
					);
				
			}
		
	
    
	return $datos;
	}

	public function gettipobarniz(){
	$datos=array();
		
		
			$result=$this->app->getrecord("complementos","*"," where categoria=3 and status=1");
			
			for($a=0;$a<sizeof($result);$a++){
				$vec=$result[$a];
			
			
				$datos[] = array(
					'id'=>$vec['id'],
					'nombre'=>$vec['nombre'],
					);
				
			}
		
	
    
	return $datos;
	}
	public function gettipolaminado(){
	$datos=array();
		
		
			$result=$this->app->getrecord("complementos","*"," where categoria=4 and status=1");
			
			for($a=0;$a<sizeof($result);$a++){
				$vec=$result[$a];
			
			
				$datos[] = array(
					'id'=>$vec['id'],
					'nombre'=>$vec['nombre'],
					);
				
			}
		
	
    
	return $datos;
	}
	public function gettipopegado(){
	$datos=array();
		
		
			$result=$this->app->getrecord("complementos","*"," where categoria=5 and status=1");
			
			for($a=0;$a<sizeof($result);$a++){
				$vec=$result[$a];
			
			
				$datos[] = array(
					'id'=>$vec['id'],
					'nombre'=>$vec['nombre'],
					);
				
			}
		
	
    
	return $datos;
	}
	public function gettiposuaje(){
	$datos=array();
		
		
			$result=$this->app->getrecord("complementos","*"," where categoria=6 and status=1");
			
			for($a=0;$a<sizeof($result);$a++){
				$vec=$result[$a];
			
			
				$datos[] = array(
					'id'=>$vec['id'],
					'nombre'=>$vec['nombre'],
					);
				
			}
		
	
    
	return $datos;
	}
	public function gettipoempalme(){
	$datos=array();
		
		
			$result=$this->app->getrecord("complementos","*"," where categoria=7 and status=1");
			
			for($a=0;$a<sizeof($result);$a++){
				$vec=$result[$a];
			
			
				$datos[] = array(
					'id'=>$vec['id'],
					'nombre'=>$vec['nombre'],
					);
				
			}
		
	
    
	return $datos;
	}
	public function gettipopago(){
	$datos=array();
		
		
			$result=$this->app->getrecord("complementos","*"," where categoria=12 and status=1");
			
			for($a=0;$a<sizeof($result);$a++){
				$vec=$result[$a];
			
			
				$datos[] = array(
					'id'=>$vec['id'],
					'nombre'=>$vec['nombre'],
					);
				
			}
		
	
    
	return $datos;
	}
	public function gettipoentrega(){
	$datos=array();
		
		
			$result=$this->app->getrecord("complementos","*"," where categoria=18 and status=1");
			
			for($a=0;$a<sizeof($result);$a++){
				$vec=$result[$a];
			
			
				$datos[] = array(
					'id'=>$vec['id'],
					'nombre'=>$vec['nombre'],
					);
				
			}
		
	
    
	return $datos;
	}
	public function getmargen(){
	$datos=array();
		
		
			$result=$this->app->getrecord("complementos","*"," where categoria=19 and status=1");
			
			for($a=0;$a<sizeof($result);$a++){
				$vec=$result[$a];
			
			
				$datos[] = array(
					'id'=>$vec['id'],
					'nombre'=>$vec['nombre'],
					);
				
			}
		
	
    
	return $datos;
	}
	public function getvigencia(){
	$datos=array();
		
		
			$result=$this->app->getrecord("complementos","*"," where categoria=13 and status=1");
			
			for($a=0;$a<sizeof($result);$a++){
				$vec=$result[$a];
			
			
				$datos[] = array(
					'id'=>$vec['id'],
					'nombre'=>$vec['nombre'],
					);
				
			}
		
	
    
	return $datos;
	}
	public function getescala(){
	$datos=array();
		
		
			$result=$this->app->getrecord("complementos","*"," where categoria=8 and status=1");
			
			for($a=0;$a<sizeof($result);$a++){
				$vec=$result[$a];
			
			
				$datos[] = array(
					'id'=>$vec['id'],
					'nombre'=>$vec['nombre'],
					);
				
			}
		
	
    
	return $datos;
	}
	public function getherramental(){
	$datos=array();
		
		
			$result=$this->app->getrecord("complementos","*"," where categoria=14 and status=1");
			
			for($a=0;$a<sizeof($result);$a++){
				$vec=$result[$a];
			
			
				$datos[] = array(
					'id'=>$vec['id'],
					'nombre'=>$vec['nombre'],
					);
				
			}
		
	
    
	return $datos;
	}
	public function gettipodummy(){
	$datos=array();
		
		
			$result=$this->app->getrecord("complementos","*"," where categoria=15 and status=1");
			
			for($a=0;$a<sizeof($result);$a++){
				$vec=$result[$a];
			
			
				$datos[] = array(
					'id'=>$vec['id'],
					'nombre'=>$vec['nombre'],
					);
				
			}
		
	
    
	return $datos;
	}
	//////////////////////////////////////////////FUNCTIONS FOR PROYECTS/////////////////////////////////////


	public function savecotizacionproyectoexhibidor($iduser,$cliente,$alto,$ancho,$largo,$vtaalargo,$vtaaalto,$vtablargo,$vtabalto,$tipoexhibidor,$tienda,$peso,$cantidad,$tiempo,$presupuesto,$muestras,$infoadicional,$muestra,$muestraimpresa,$arte,$brief){

	$datos=array();
		
		$id=$this->app->saverecord("cotizaciones","null,'$cliente','Proyecto-Exhibidor','','',1,$iduser,now(),'','1'");
			if($id>0 ){
				$this->app->saverecord("dimensiones","null,$id,$ancho,$largo,$alto,$vtaalargo,$vtaaalto,$vtablargo,$vtabalto,'1'");
				$this->app->saverecord("exhibidores","null,$id,'$tipoexhibidor','$tienda',$peso,$cantidad,'$tiempo',$presupuesto,'$muestras','$infoadicional','$muestra','$muestraimpresa','$arte','$brief','1'");

				$datos[]=array(
					'exito'=>'si',
					'folio'=>$id
				);	
			}else{
				$datos[]=array(
					'exito'=>'no',
					'folio'=>0
				);	
			}		

	return $datos;

		
	}

	public function savecotizacionproyectoempaque ($iduser,$cliente,$alto,$ancho,$largo,$vtaalargo,$vtaaalto,$vtablargo,$vtabalto,$codigoempaque,$materialempaque,$peso,$tipoempaque,$muestras, $infoadicional,$muestra,$muestraimpresa,$arte,$brief){
	$datos=array();
		
		$id=$this->app->saverecord("cotizaciones","null,'$cliente','Proyecto-Empaque','','',1,$iduser,now(),'','1'");
			if($id>0 ){
				$this->app->saverecord("dimensiones","null,$id,$ancho,$largo,$alto,$vtaalargo,$vtaaalto,$vtablargo,$vtabalto,'1'");
				$this->app->saverecord("empaques","null,$id,'$codigoempaque','$materialempaque',$peso,'$tipoempaque','$muestras','$infoadicional','$muestra','$muestraimpresa','$arte','$brief','1'");

				$datos[]=array(
					'exito'=>'si',
					'folio'=>$id
				);	
			}else{
				$datos[]=array(
					'exito'=>'no',
					'folio'=>0
				);	
			}		

	return $datos;
	}

	public function getsustratoS(){
	$datos=array();
		
		
			$result=$this->app->getrecord("complementos","*"," where categoria=1 and status=1");
			
			for($a=0;$a<sizeof($result);$a++){
				$vec=$result[$a];
			
			
				$datos[] = array(
					'id'=>$vec['id'],
					'nombre'=>$vec['nombre'],
					);
				
			}
		
	
    
	return $datos;
	}
	public function gettipoexhibidor(){
	$datos=array();
		
		
			$result=$this->app->getrecord("complementos","*"," where categoria=10 and status=1");
			
			for($a=0;$a<sizeof($result);$a++){
				$vec=$result[$a];
			
			
				$datos[] = array(
					'id'=>$vec['id'],
					'nombre'=>$vec['nombre'],
					);
				
			}
		
	
    
	return $datos;
	}
	public function gettipotienda(){
	$datos=array();
		
		
			$result=$this->app->getrecord("complementos","*"," where categoria=11 and status=1");
			
			for($a=0;$a<sizeof($result);$a++){
				$vec=$result[$a];
			
			
				$datos[] = array(
					'id'=>$vec['id'],
					'nombre'=>$vec['nombre'],
					);
				
			}
		
	
    
	return $datos;
	}
////////////////////////////////////////////FUNCTIONS FOR MAQUILA//////////////////////////////////////
	public function savecotizacionmaquila($iduser,$data,$cliente){
	$datos=array();
		//trigger_error(print_r($data,true));
		$id=$this->app->saverecord("cotizaciones","null,'$cliente','Maquila','','',1,$iduser,now(),'','1'");
			if($id>0 ){
				for($a=0;$a<sizeof($data);$a++){
					$sae=$data[$a]['sae'];
					$desc=$data[$a]['descripcion'];
					$cant=$data[$a]['cantidad'];
					$unidad=$data[$a]['unidad'];
					
					$this->app->saverecord("rel_maquilaproceso","null,'$sae','$desc',$cant,'$unidad',$id,$iduser");
				}
				

				$datos[]=array(
					'exito'=>'si',
					'folio'=>$id
				);	
			}else{
				$datos[]=array(
					'exito'=>'no',
					'folio'=>0
				);	
			}		

	return $datos;
	}
	public function getmaquilasenable(){
	$datos=array();
		
		
			$result=$this->app->getrecord("maquilaproceso","*"," where activo like '1'");
			
			for($a=0;$a<sizeof($result);$a++){
				$vec=$result[$a];
			
			
				$datos[] = array(
					'id'=>$vec['id'],
					'categoria'=>$vec['categoria'],
					'tipo'=>$vec['tipo'],
					'clave'=>$vec['clave'],
					'descripcion'=>$vec['descripcion'],
					'um'=>$vec['unidadMedida'],
					);
				
			}
		
	
    
	return $datos;
	}

///////////////////////////////////////////////////GENERAL FUNCTIONS FOR COTIZATOR////////////////////////////////////////////////////////////

	public function getallquotations($isvendedor,$iduser){
	$datos=array();
		
		if($isvendedor>0){
			$result=$this->app->getrecord("cotizaciones","cotizaciones.id,cotizaciones.cliente,cotizaciones.tipo,cotizaciones.tipoProducto,cotizaciones.fechaElaboracion,status_cotizacion.status,concat(persona.nombre,' ',persona.primerApellido,' ',persona.segundoApellido) as 'usuario' ","inner JOIN status_cotizacion on cotizaciones.idStatuscotizacion=status_cotizacion.id INNER JOIN usuarios on cotizaciones.idUsuario=usuarios.id INNER JOIN persona on persona.id=usuarios.idPersona where cotizaciones.activo like '1' and cotizaciones.idUsuario=$iduser");
		}else{
			$result=$this->app->getrecord("cotizaciones","cotizaciones.id,cotizaciones.cliente,cotizaciones.tipo,cotizaciones.tipoProducto,cotizaciones.fechaElaboracion,status_cotizacion.status,concat(persona.nombre,' ',persona.primerApellido,' ',persona.segundoApellido) as 'usuario' ","inner JOIN status_cotizacion on cotizaciones.idStatuscotizacion=status_cotizacion.id INNER JOIN usuarios on cotizaciones.idUsuario=usuarios.id INNER JOIN persona on persona.id=usuarios.idPersona where cotizaciones.activo like '1'");
		}	

		for($a=0;$a<sizeof($result);$a++){
			$vec=$result[$a];
			
			
			$datos[] = array(
				'id'=>$vec['id'],
				'cliente'=>$vec['cliente'],
				'tipo'=>$vec['tipo'],
				'tipoproducto'=>$vec['tipoProducto'],
				'fechaelaboracion'=>$vec['fechaElaboracion'],
				'status'=>$vec['status'],
				'usuario'=>$vec['usuario']
			);
				
		}
	return $datos;
	}
	public function getfilesquotation($idcotizacion){
		$datos=array();
		
			$result3=$this->app->getrecord("rel_cotizacionarchivo","*"," where idCotizacion= $idcotizacion");
			for($aa=0;$aa<sizeof($result3);$aa++)
    			{	$vec3=$result3[$aa];
				$datos[]=array(
					'id'=>$vec3['id'],
					'descripcion'=>$vec3['descripcion'],
					'ruta'=>$vec3['ruta']
					);
						
			}	

	return $datos;
	}
	
	public function deletefilequotation($id){
		$datos=array();
		$result=$this->app->getrecord("rel_cotizacionarchivo","*"," where id=$id");
		
		if (sizeof($result) > 0) 
		{		$vec=$result[0];
			unlink($this->path2.$vec['ruta']);
			if($this->app->deleterecord("rel_cotizacionarchivo"," where id=$id")){
					$datos[]=array(
						'exito'=>'si'
					);					
			}		
				
		} 
		else{
			
			$datos[]=array(
					'exito'=>'sin'

				);
		}


	return $datos;		
	}

	public function deletequotation($id,$tipo){
	$datos=array();
		$result=$this->app->getrecord("cotizaciones","*"," where id=$id");
		
		if (sizeof($result) > 0) 
		{		
			if($this->app->deleterecord("cotizaciones"," where id=$id")>0){

					switch($tipo){
						case "Producto":
							$this->app->deleterecord("dimensiones"," where idCotizacion=$id");
							$this->app->deleterecord("sustratos"," where idCotizacion=$id");
							$this->app->deleterecord("tintas"," where idCotizacion=$id");
							$this->app->deleterecord("escalasvolumen"," where idCotizacion=$id");
							$this->app->deleterecord("terminoscondiciones"," where idCotizacion=$id");
						break;
						case "Proyecto-Exhibidor":
							$this->app->deleterecord("dimensiones"," where idCotizacion=$id");
							$this->app->deleterecord("exhibidores"," where idCotizacion=$id");						
						break;
						case "Proyecto-Empaque":
							$this->app->deleterecord("dimensiones"," where idCotizacion=$id");
							$this->app->deleterecord("empaques"," where idCotizacion=$id");						
						break;
						case "Maquila":
						
							$this->app->deleterecord("rel_maquilaproceso"," where idCotizacion=$id");	
						break;
					}
					$datos[]=array(
						'exito'=>'si'
					);					
			}		
				
		} 
		else{
			
			$datos[]=array(
					'exito'=>'sin'

				);
		}


	return $datos;


	}
/////////////////////////////////////////////////////////////////////////////////FUNCTIONS FOR INCIDENTS////////////////////////////////////////////////////

	public function saveincident($idorden,$areadenuncia,$areadenunciada,$denunciado,$apelliodosdenunciado,$incidente,$contingencia,$causa,$erradicar,$fechaconclusion,$denunciante,$apellidosdenunciante){
		$datos=array();
		$result=$this->app->saverecord("incidentes","$idorden,now(),'$areadenuncia','$areadenunciada','$denunciado','$apelliodosdenunciado','$incidente','$contingencia','$causa','$erradicar','$fechaconclusion','$denunciante','$apellidosdenunciante'","(idOrden,fecha,areaDenuncia,areaDenunciada,nombreDenunciado,apellidosDenunciado,incidente,contingencia,causa,erradicar,fechaconclusion,nombreDenunciante,apellidosDenunciante)");
		if(sizeof($result)){
			$datos[]=array(
					'exito'=>'si'
			);
		}else{
			$datos[]=array(
					'exito'=>'no'
			);
		}
	return $datos;
	}
	public function getdepartaments(){
		$datos=array();
		
		
			$result=$this->app->getrecord("departamentos","*"," where activo like '1'");
			
			for($a=0;$a<sizeof($result);$a++){
				$vec=$result[$a];
			
			
				$datos[] = array(
					'id'=>$vec['id'],
					'nombre'=>$vec['nombre'],
					);
				
			}
		
	
    
	return $datos;
	}

	public function getallincidentsbyorder($idorden){
		$datos=array();
		
		
			$result=$this->app->getrecord("incidentes","*"," where activo like '1' and idOrden=$idorden");
			
			for($a=0;$a<sizeof($result);$a++){
				$vec=$result[$a];
			
			
				$datos[] = array(
					'id'=>$vec['id'],
					'idorden'=>$vec['idOrden'],
					'fecha'=>$vec['fecha'],
					'areadenuncia'=>$vec['areaDenuncia'],
					'areadenunciada'=>$vec['areaDenunciada'],
					'nombredenunciado'=>$vec['nombreDenunciado'],
					'apellidosdenunciado'=>$vec['apellidosDenunciado'],
					'incidente'=>$vec['incidente'],
					'contingencia'=>$vec['contingencia'],
					'causa'=>$vec['causa'],
					'erradicar'=>$vec['erradicar'],
					'fechaconclusion'=>$vec['fechaconclusion'],
					'nombredenunciante'=>$vec['nombreDenunciante'],
					'apellidosdenunciante'=>$vec['apellidosDenunciante'],
					'activo'=>$vec['activo'],

					);
				
			}
		
	
    
	return $datos;
	}
	public function getspecificincident($id){
		$datos=array();
		
		
			$result=$this->app->getrecord("incidentes","*"," where activo like '1' and id=$id");
			
			for($a=0;$a<sizeof($result);$a++){
				$vec=$result[$a];
			
			
				$datos[] = array(
					'id'=>$vec['id'],
					'idorden'=>$vec['idOrden'],
					'fecha'=>$vec['fecha'],
					'areadenuncia'=>$vec['areaDenuncia'],
					'areadenunciada'=>$vec['areaDenunciada'],
					'nombredenunciado'=>$vec['nombreDenunciado'],
					'apellidosdenunciado'=>$vec['apellidosDenunciado'],
					'incidente'=>$vec['incidente'],
					'contingencia'=>$vec['contingencia'],
					'causa'=>$vec['causa'],
					'erradicar'=>$vec['erradicar'],
					'fechaconclusion'=>$vec['fechaconclusion'],
					'nombredenunciante'=>$vec['nombreDenunciante'],
					'apellidosdenunciante'=>$vec['apellidosDenunciante'],
					'activo'=>$vec['activo'],

					);
				
			}
		
	
    
	return $datos;
	}

	public function updateincident($id,$areadenuncia,$areadenunciada,$denunciado,$apelliodosdenunciado,$incidente,$contingencia,$causa,$erradicar,$fechaconclusion,$denunciante,$apellidosdenunciante){
	$datos=array();
		$result=$this->app->getrecord("incidentes","*"," where activo like '1' and id=$id");
		
		if (sizeof($result) > 0) {
			$vec=$result[0];
			if($this->app->updaterecord("incidentes","areaDenuncia='$areadenuncia',areaDenunciada='$areadenunciada',nombreDenunciado='$denunciado',apellidosDenunciado='$apelliodosdenunciado',incidente='$incidente',contingencia='$contingencia',causa='$causa',erradicar='$erradicar',fechaconclusion='$fechaconclusion',nombreDenunciante='$denunciante',apellidosDenunciante='$apellidosdenunciante'"," where id=$id")){
				
					$datos[]=array(
						'exito'=>'si',
						'folio'=>$id
					);
							
			}	
				
		} 
		else{
			
			$datos[]=array(
					'exito'=>'sin',
					'folio'=>0
				);
		}
	return $datos;	

	}
	public function deleteincident($id){
		$datos=array();
		$result=$this->app->getrecord("incidentes","*"," where id=$id");
		
		if (sizeof($result) > 0) 
		{		
			if($this->app->deleterecord("incidentes"," where id=$id")>0){

					
					$datos[]=array(
						'exito'=>'si'
					);					
			}		
				
		} 
		else{
			
			$datos[]=array(
					'exito'=>'sin'

				);
		}


	return $datos;

	}
	public function getincidentsbydate($fechainicio,$fechafin,$area){
		$datos=array();
		if($area=='todas'){
			$result=$this->app->getrecord("incidentes","*"," where incidentes.fecha BETWEEN '$fechainicio 00:00:00' and '$fechafin 23:59:00' and activo like '1'");
			
			for($a=0;$a<sizeof($result);$a++){
				$vec=$result[$a];
			
			
				$datos[] = array(
					'id'=>$vec['id'],
					'idorden'=>$vec['idOrden'],
					'fecha'=>$vec['fecha'],
					'areadenuncia'=>$vec['areaDenuncia'],
					'areadenunciada'=>$vec['areaDenunciada'],
					'nombredenunciado'=>$vec['nombreDenunciado'],
					'apellidosdenunciado'=>$vec['apellidosDenunciado'],
					'incidente'=>$vec['incidente'],
					'contingencia'=>$vec['contingencia'],
					'causa'=>$vec['causa'],
					'erradicar'=>$vec['erradicar'],
					'fechaconclusion'=>$vec['fechaconclusion'],
					'nombredenunciante'=>$vec['nombreDenunciante'],
					'apellidosdenunciante'=>$vec['apellidosDenunciante'],
					'activo'=>$vec['activo']

					);
				
			}
		}else{
		
			$result=$this->app->getrecord("incidentes","*"," where incidentes.fecha BETWEEN '$fechainicio 00:00:00' and '$fechafin 23:59:00' and areaDenuncia like '$area' and activo like '1'");
			
			for($a=0;$a<sizeof($result);$a++){
				$vec=$result[$a];
			
			
				$datos[] = array(
					'id'=>$vec['id'],
					'idorden'=>$vec['idOrden'],
					'fecha'=>$vec['fecha'],
					'areadenuncia'=>$vec['areaDenuncia'],
					'areadenunciada'=>$vec['areaDenunciada'],
					'nombredenunciado'=>$vec['nombreDenunciado'],
					'apellidosdenunciado'=>$vec['apellidosDenunciado'],
					'incidente'=>$vec['incidente'],
					'contingencia'=>$vec['contingencia'],
					'causa'=>$vec['causa'],
					'erradicar'=>$vec['erradicar'],
					'fechaconclusion'=>$vec['fechaconclusion'],
					'nombredenunciante'=>$vec['nombreDenunciante'],
					'apellidosdenunciante'=>$vec['apellidosDenunciante'],
					'activo'=>$vec['activo']

					);
				
			}
		
		}	
    
	return $datos;
	}
	
}
?>