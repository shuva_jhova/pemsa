﻿<?php
require_once("usersFunction.php");
$app = new usersFunction();
	if(!$app->islogged()){
		echo "<script>window.top.location.href = 'logout.php';</script>";	
	}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="initial-scale=1.0,user-scalable=yes"/>

<link rel="stylesheet" href="../css/estilo2.css">
<link rel="stylesheet" href="../css/jquery.dataTables.min.css">
 <script language="javascript" type="text/javascript" src="../js/gateway.js"></script>
<script language="javascript" type="text/javascript" src="../js/jquery-1.12.4.js"></script>
<script language="javascript" type="text/javascript" src="../js/jquery.dataTables.min.js"></script>
  <script language="javascript" type="text/javascript" src="../js/solicitudes.js"></script>
<title></title>

</head>
<body onload="getquotations();">
<main>
	
	<section id="titulo">
		<center><h2> Lista de Cotizaciones</h2>
		</center>
	</section>

<div>
	<form name="f1" action="#">
		<center>
		<div id="sin2" style="display:none;">
			<div id="myDiv">
				<!--div class="txt">
					Ingresa los Datos de la Cotización a Buscar<br>
					<input type="text" name="num" placeholder="Cotización" id="txtbuscar" onkeyup="doSearch()" required="required" class="TT" />
				
				</div-->
				<div class="txt">
						<input type="button" name="insertar" Value="Actualizar" onClick="getquotationsupdate();" id="ok"/>
				</div>
			<div >
				<img src="../img/load.gif" class="imgload" id="imgload">
			</div>
		</div>
		</center>
	</form>
</div>

<center>
</br>
	<div class="txt" id='addnew'>
			<input type="button" name="addnew" Value="Agregar" onClick="agregar();" id="ok"/>
	</div>
	</br>
<div id="scro">
 <div id="tabla">
     <table class="tbl-qa display nowrap" style="width:100%" id="resultado">
		  <thead>
		 <tr>
		<th class="table-header" >ID</th>
		<th class="table-header" >Cliente</th>
                <th class="table-header" >Producto</th>
		<!--th class="table-header" >Tipo</th-->
	        <th class="table-header" >Fecha Registro</th>
		<th class="table-header" >Fecha Cotizacion</th>
		<th class="table-header" >Pedido SAE</th>
		<th class="table-header" >Fecha Pedido</th>
	        <th class="table-header" >Usuario</th>
		<th class="table-header" >Responsable</th>
		<th class="table-header" >Estado</th>
	        <th class="table-header" >Archivos</th>
	        <th class="table-header" >Exportar</th>
		<th class="table-header" >Editar</th>
		<th class="table-header" >Eliminar</th>
              </tr>
 		  </thead>
		  <tbody style="height:250px;overflow:scroll">				
             	 </tbody>
              </table>
 </div>
	</br></br>
		<div id="sin" style="display:none;">
			<div class='myDiv'><div class='txt'>NO HAY COTIZACIONES REGISTRADAS EN EL SISTEMA </div></div>
		</div>
 </div>
</center>
</div>


<div id="myModal" class="modal">

  <!-- Modal content -->
  <div class="modal-content"> 
  <div class="modal-content2">	
    <span class="close">&times;</span>


			<section id="titulo">
        			<center></br><h2>Ingrese los Datos de la Nueva Cotización</h2>
					     <h5>Asegurese de que los Datos Introducidos sean Correctos</h5>
				</center>
			</section>
<div class="contenedor">
			
	<div class="myDiv" >
		
		<div class="txt" >
			Cliente<br>
			<select id="optioncliente"></select>	
		</div>
		<div class="txt" >
				Tipo de Producto<br>
				<select id="optionproducto"></select>	
			</div>
		
	</div>
</div>

<div id="productos" >

<div id="sec1" style="display:block;">
		<!--section id="titulo">
			<center><h3>Producto</h3>
			</center>
		</section-->	
		
	<div class="contenedor">
		<div class="myDiv" >
			
			<div class="txt" >
				Código de Identificación Único<br>
				<input type="text" placeholder="Id Único" id="txtidunico" class="TT"/>		
			</div>
		</div>
	</div>
	<center>
		<div class="txt">
			<input type="button" name="insertar" Value="Siguiente" onClick="savecotizacionsec1();" id="ok"/>
		</div>
	</center>
</div>

<div id="sec2" style="display:none;">
	<hr>
		<section id="titulo">
			<center><h2>Descriptivo Conceptual y Dimensional</h2><h3>Dimensiones (mm)</h3>
			</center>
		</section>
	<section id="titulo">
			<center><h2>Manuales</h2>
			</center>
		</section>
	
		<div id="tablafiles">
			
   			  <table class="tbl-qa" id="resultadofiles">
		 		 <thead>
					 <tr>
						<th class="table-header" >Descripción</th>
						<th class="table-header" >Ver</th>
              				</tr>
 		  		</thead>
		  		<tbody >				
             	 		</tbody>
              		</table>
 		</div>
		</br></br>
		<div id="sinfiles" style="display:none;">
			<div class='myDiv'><div class='txt'>NO HAY MANUALES REGISTRADOS EN EL SISTEMA </div></div>
		</div>	
	<div class="contenedor">
		<div class="myDiv" >
			<div class="txt" >
				A<br>
				<input type="number" placeholder="A" id="txta" value=0 class="TT"/>		
			</div>

			<div class="txt" >
				B<br>
				<input type="number" placeholder="B" id="txtb" value=0 class="TT"/>		
			</div>
			<div class="txt" >
				H<br>
				<input type="number" placeholder="H" id="txth" value=0 class="TT"/>		
			</div>
			<div class="txt" >
				X<br>
				<input type="number" placeholder="X" id="txtx" value=0 class="TT"/>		
			</div>

			<div class="txt" >
				Y<br>
				<input type="number" placeholder="Y" id="txty" value=0 class="TT"/>		
			</div>
			<div class="txt" >
				Z<br>
				<input type="number" placeholder="Z" id="txtz" value=0 class="TT"/>		
			</div>
			<div class="txt" >
				O<br>
				<input type="number" placeholder="O" id="txto" value=0 class="TT"/>		
			</div>

			<div class="txt" >
				P<br>
				<input type="number" placeholder="P" id="txtp" value=0 class="TT"/>		
			</div>
			<div class="txt" >
				Q<br>
				<input type="number" placeholder="Q" id="txtq" value=0 class="TT"/>		
			</div>
			<div class="txt" >
				R<br>
				<input type="number" placeholder="R" id="txtr" value=0 class="TT"/>		
			</div>

			<div class="txt" >
				S<br>
				<input type="number" placeholder="S" id="txts" value=0 class="TT"/>		
			</div>
			<div class="txt" >
				T<br>
				<input type="number" placeholder="T" id="txtt" value=0 class="TT"/>		
			</div>
			<div class="txt" >
				Tipo Código<br>
				<select id="optionfamiliacodigo" onChange="getcodesspecific();"></select><br>
				Código<br>
				<select id="optioncodigo"></select>	
			</div>
			<div class="txt" >
				Descriptivo de Variables a Considerar<br>
				<textarea rows="3" cols="30" name="dir" id="txtnotas" placeholder="Describe aquí las Dimensiones del Producto"></textarea>		
			</div>
			
		</div>
	</div>
	<center>
		<div class="txt">
			<input type="button" name="insertar" Value="Siguiente" onClick="savecotizacionsec2();" id="ok"/>
		</div>		
	</center>
</div>

<div id="secfile" style="display:none;">
	<center>
		<div class="txt">
			Subir Archivos<br>
			<img src="../img/folder.png" onClick="addfile();"/>
			<!--input type="button" name="insertar" Value="Agregar" onClick="addfile();" id="ok"/-->
		</div>
		
	</center>

		<center>
		
		
		<div class="myDiv" id="docs" style="display:none;">
	
			<div class="txt">
				Seleccione el Archivo<br>
		    		<input type="file" id="doc1" class="TT" onchange="enablebtn();"/>
             	   		
				<center>
					<!--input type="button" name="insertar" Value="Guardar" onClick="" id="ok"/-->
					<textarea rows="6" cols="50" name="dir"  class="TT" id="txtfiledescription" placeholder="Descripción del Archivo"></textarea>
					<br>						
					<img src="../img/save2.png" onclick="uploadfiletoserver();" id="imgsavefile" style="display:none;"/>
				</center>
			</div>		
		</div>


		<div class="txt">
			<input type="button" name="insertar" Value="Siguiente" onClick="savecotizacionfile();" id="ok"/>
		</div>
	</center>

	<div id="tablafilesadd">
   			  <table class="tbl-qa" id="resultadofilesadd">
		 		 <thead>
					 <tr>
						<th class="table-header" >Descripcion</th>
						<th class="table-header" >Ver</th>
						<!--th class="table-header" >Editar</th-->
						<th class="table-header" >Eliminar</th>
						
              				</tr>
 		  		</thead>
		  		<tbody >				
             	 		</tbody>
              		</table>
 		</div>
		</br></br>
		<div id="sinfilesadd" style="display:none;">
			<div class='myDiv'><div class='txt'>NO HAY ARCHIVOS REGISTRADOS EN EL SISTEMA </div></div>
		</div>	
		
</div>

<div id="sec3" style="display:none;">
	<hr>
	<section id="titulo">
			<center><h2>Descriptivo de Materiales y Acabados</h2>
			</center>
		</section>
		
	<div class="contenedor">
		<div class="myDiv" >
			<div class="txt" >
				Nombre Sección<br>
				<input type="text" placeholder="Nombre Sección" id="txtnomsec" class="TT"/>
			</div>
			<div class="txt" >
				Categoria del Sustrato<br>
				<select id="optioncategorias" onChange="getsustratosss(this.value);"></select>	
			</div>
			<div class="txt" >
				Sustrato Base<br>
				<select id="optiontiposustrato"></select>	
			</div>
			<div class="txt" >
				Empalme<br>
				<select id="optionempalme"></select>	
			</div>
			<div class="txt" >
				Suaje<br>
				<select id="optionsuaje"></select>	
			</div>
			<div class="txt" >
				Pegado<br>
				<select id="optionpegado"></select>	
			</div>
		</div>
	</div>

		<section id="titulo">
			<center><h2>Impresión y Acabados</h2>
			</center>
		</section>
		
	<div class="contenedor">
		<div class="myDiv" >
			<div class="txt" >
				CMYK Frente<br>
				<select id="optiontintafrente" onChange="changevalue(6,this);"></select>
				<br>
				<select placeholder="Frente" id="txttintafrente" class="TT"></select>
			</div>
			<div class="txt" >
				CMYK Reverso<br>
				<select id="optiontintareverso" onChange="changevalue(7,this);"></select><br>
				<select placeholder="Reverso" id="txttintareverso" class="TT"></select>
			</div>
			<div class="txt" >
				Pantones Frente<br>
				<input type="text" placeholder="Pantones" id="txtpantonesfrente" class="TT"/>
				<select placeholder="Frente" id="txtnumeropantonesfrente"  onChange="changevalue(18,this);"  class="TT"></select>
			</div>
			<div class="txt" >
				Pantones Reverso<br>
				<input type="text" placeholder="Pantones"  id="txtpantonesreverso" class="TT"/>
				<select placeholder="Reverso" id="txtnumeropantonesreverso" onChange="changevalue(17,this);" class="TT"></select>
			</div>
			<div class="txt" >
				Barniz Frente<br>
				<select id="optionbarnizfrente" onChange="changevalue(1,this);"></select><br>
				<label class="fondotxt" style="display:none;">
				<input type='radio' name='barnizfrente' value='1' ">Si
				<input type='radio' name='barnizfrente' value='0'  checked='checked'">No
				</label>	
			</div>
			<div class="txt" >
				Barniz Reverso<br>
				<select id="optionbarnizreverso" onChange="changevalue(2,this);"></select><br>
				<label class="fondotxt" style="display:none;">
				<input type='radio' name='barnizreverso' value='1' ">Si
				<input type='radio' name='barnizreverso' value='0'  checked='checked'">No
				</label>	
			</div>
			<div class="txt" >
				Laminado Frente<br>
				<select id="optionlaminadofrente" onChange="changevalue(3,this);"></select><br>
				<label class="fondotxt" style="display:none;">
				<input type='radio' name='laminadofrente' value='1' ">Si
				<input type='radio' name='laminadofrente' value='0'  checked='checked'">No
				</label>	
			</div>
			<div class="txt" >
				Laminado Reverso<br>
				<select id="optionlaminadoreverso" onChange="changevalue(4,this);"> </select><br>
				<label class="fondotxt" style="display:none;">
				<input type='radio' name='laminadoreverso' value='1' ">Si
				<input type='radio' name='laminadoreverso' value='0'  checked='checked'">No
				</label>	
			</div>
			<div class="txt" >
				Serigrafía Frente<br>
				<textarea rows="3" cols="30" name="serigrafiafrente" id="serigrafiafrente" placeholder="Serigrafía"></textarea>
				<select id="optionserigrafiafrente" style='display:none;'></select>
				<!--label class="fondotxt" style="display:none;">
				<input type='radio' name='serigrafiafrente2' value='1' ">Si
				<input type='radio' name='serigrafiafrente2' value='0'  checked='checked'">No
				</label-->	
			</div>
			<div class="txt" >
				Serigrafía Reverso<br>
				<textarea rows="3" cols="30" name="serigrafiareverso" id="serigrafiareverso" placeholder="Serigrafía"></textarea>
				<select id="optionserigrafiareverso" style='display:none;'></select>
				<!--label class="fondotxt" style="display:none;">
				<input type='radio' name='serigrafiareverso2' value='1' ">Si
				<input type='radio' name='serigrafiareverso2' value='0'  checked='checked'">No
				</label-->	
			</div>
			<div class="txt" >
				Cambios de Arte Frente<br>
				<select id="txtcambiosartefrente"></select>
				<!--input type="number" placeholder="Frente" id="txtcambiosartefrente" class="TT"/-->		
			</div>
			<div class="txt" >
				Cambios de Arte Reverso<br>
				<select id="txtcambiosartereverso"></select>
				<!--input type="number" placeholder="Reverso" id="txtcambiosartereverso" class="TT"/-->		
			</div>
		</div>
	</div>
	
	<section id="titulo">
			<center><h2>Ventana</h2>
			</center>
		</section>
		
	<div class="contenedor">
		<div class="myDiv" >
			<div class="txt" >
				Material<br>
				<select id="optionmaterialventana" onChange="changevalue(5,this);"></select>	
			</div>
			<div class="txt" >
				A(mm)<br>
				<input type="number" placeholder="A(mm)" id="txtventanalargo" class="TT" disabled/>		
			</div>
			<div class="txt" >
				B(mm)<br>
				<input type="number" placeholder="B(mm)" id="txtventanaalto" class="TT" disabled/>		
			</div>
		</div>
	</div>
	<center>
		<div class="txt">
			<input type="button" name="insertar" Value="Sección +" onClick="savecotizacionsec32();" id="ok"/>
		</div>
		<div class="txt">
			<input type="button" name="insertar" Value="Siguiente" onClick="savecotizacionsec3();" id="ok"/>
		</div>
	</center>
</div>

<div id="sec4" style="display:none;">
	<hr>
			<section id="titulo">
			<center><h2>Términos y Condiciones Comerciales</h2>
			</center>
		</section>	
		</br>
	<div class="contenedor">
		<div class="myDiv">
			<div class="txt" >
				Herramental<br>
				<select id="optionherramental"></select>	
			</div>
			<div class="txt" >
				Separar Cambios de Arte<br>
				<label class="fondotxt">
				<input type='radio' name='cambiosarte' value='1' ">Si
				<input type='radio' name='cambiosarte' value='0'  checked='checked'">No
				</label>	
			</div>
			<div class="txt" >
				Dummy<br>
				<select id="optiondummy"></select>	
			</div>
			<div class="txt" >
				Agregar a Web to Print<br>
				<label class="fondotxt">
				<input type='radio' name='web2print' value='1' ">Si
				<input type='radio' name='web2print' value='0'  checked='checked'">No
				</label>
			</div>
		</div>


	</div>

		<section id="titulo">
			<center><h2>Escala de Volumen Solicitada</h2>
			</center>
		</section>
	<div class="contenedor">
		<datalist id="escalas"></datalist>
		<div class="myDiv">
			<div class="txt" >
				Primera escala<br>
				<input type="number" placeholder="Escala uno" value=0 list="escalas" id="txtprimerescala" class="TT"/>	
					
			</div>
			<div class="txt" >
				Segunda escala<br>
				<input type="number" placeholder="Escala uno" value=0 list="escalas" id="txtsegundaescala" class="TT"/>		
			</div>
			<div class="txt" >
				Tercera escala<br>
				<input type="number" placeholder="Escala uno" value=0 list="escalas" id="txttercerescala" class="TT"/>		
			</div>
			<div class="txt" >
				Cuarta escala<br>
				<input type="number" placeholder="Escala uno" value=0 list="escalas" id="txtcuartaescala" class="TT"/>		
			</div>
			<div class="txt" >
				Quinta escala<br>
				<input type="number" placeholder="Escala uno" value=0 list="escalas" id="txtquintaescala" class="TT"/>		
			</div>
			<div class="txt" >
				Sexta escala<br>
				<input type="number" placeholder="Escala uno" value=0 list="escalas" id="txtsextaescala" class="TT"/>		
			</div>
			<div class="txt" >
				Costo Objetivo<br>
				<input type="number" placeholder="Costo Objetivo" value=0 id="txtcostoobjetivo" class="TT"/>		
			</div>
			<div class="txt" >
				Margen de Ganancia<br>
				<select id="optionmargenganacia"></select>	
			</div>
		</div>


	</div>

	<section id="titulo">
			<center><h2>Terminos Generales</h2>
		</center>
		</section>	
		</br>
	<div class="contenedor">
		<div class="myDiv">
			<div class="txt" >
				Vigencia de la cotización<br>
				<select id="optionvigenciacotizacion"></select>	
			</div>
			<div class="txt" >
				Forma de pago<br>
				<select id="optionformapago"></select>	
			</div>
			<div class="txt" >
				Flete<br>
				<select id="optionformaentrega"></select>	
			</div>	
			
		</div>


	</div>
	<section id="titulo">
			<center><h2>Especificaciones del Embalaje</h2>
		</center>
		</section>	
		</br>
	<div class="contenedor">
		<div class="myDiv">
			<div class="txt" >
				Inner Pack<br>
				<select id="optioninner" onChange="changevalue(8,this);"></select><br>
				Multiplo	
				<input type="number" placeholder="Inner Pack" value=0 id="txtmultiploinner" class="TT" disabled/>
			</div>
			<div class="txt" >
				Master Pack<br>
				<select id="optionmaster"></select><br>
				Multiplo	
				<input type="number" placeholder="Master Pack" value=0 id="txtmultiplomaster" class="TT"/>
			</div>	
			
		</div>


	</div>
	<center>
		<div class="txt">
			<input type="button" name="insertar" Value="Guardar" onClick="savecotizacionsec4();" id="ok"/>
		</div>
	</center>
</div>



</div>

</div>
</div>
  
</div>




<!--///////////////////////////////////////////////////////edit quotes/////////////////////////////////////////////////////////////////////////////////////-->	

<div id="myModaledit" class="modal" style="display:none;">

  <!-- Modal content -->
  <div class="modal-content"> 
  <div class="modal-content2">	
    <span class="close" id="closedit">&times;</span>


			<section id="titulo">
        			<center></br><h2>Ingrese los datos a modificar de la cotización</h2>
					     <h5>Asegurese de que los datos introducidos sean correctos</h5>
				</center>
			</section>
<div class="contenedor">
			
	<div class="myDiv" >
		<div class="txt" >
				Cliente<br>
				<input type="text" placeholder="Id Único" id="txtcliente2" disabled class="TT"/>
			</div>
		<div class="txt" >
				Tipo de producto<br>
				<select id="optionproducto2"></select>	
			</div>
		
	</div>
</div>

<div id="productos" >

<div id="sec1edit" style="display:block;">
		<!--section id="titulo">
			<center><h3>Producto</h3>
			</center>
		</section-->	
		
	<div class="contenedor">
		<div class="myDiv" >
			
			<div class="txt" >
				Código de Identificación Único<br>
				<input type="text" placeholder="Id Único" id="txtidunico2" class="TT"/>		
			</div>
		</div>
	</div>
	<center>
		<div class="txt">
			<input type="button" name="insertar" Value="Actualizar" onClick="savefirstsectionupdate();" id="ok"/>
		</div>
	</center>
</div>

		<section id="titulo">
			<center><h2 class="secciones" onclick="descriptivoconceptual();">Descriptivo Conceptual y Dimensional</h2>
			</center>
		</section>
<div id="sec2edit" style="display:none;">
	<hr>
		
	<section id="titulo">
			<center><h3>Dimensiones del producto en mm</h3> </br> <h2>Manuales</h2>
			</center>
		</section>
	
		<div id="tablafilesedit">
			
   			  <table class="tbl-qa" id="resultadofilesedit">
		 		 <thead>
					 <tr>
						<th class="table-header" >Descripción</th>
						<th class="table-header" >Ver</th>
              				</tr>
 		  		</thead>
		  		<tbody >				
             	 		</tbody>
              		</table>
 		</div>
		</br></br>
		<div id="sinfilesedit" style="display:none;">
			<div class='myDiv'><div class='txt'>NO HAY MANUALES REGISTRADOS EN EL SISTEMA </div></div>
		</div>	
	<div class="contenedor">
		<div class="myDiv" >
			<div class="txt" >
				A<br>
				<input type="number" placeholder="A" id="txta2" value=0 class="TT"/>		
			</div>

			<div class="txt" >
				B<br>
				<input type="number" placeholder="B" id="txtb2" value=0 class="TT"/>		
			</div>
			<div class="txt" >
				H<br>
				<input type="number" placeholder="H" id="txth2" value=0 class="TT"/>		
			</div>
			<div class="txt" >
				X<br>
				<input type="number" placeholder="X" id="txtx2" value=0 class="TT"/>		
			</div>

			<div class="txt" >
				Y<br>
				<input type="number" placeholder="Y" id="txty2" value=0 class="TT"/>		
			</div>
			<div class="txt" >
				Z<br>
				<input type="number" placeholder="Z" id="txtz2" value=0 class="TT"/>		
			</div>
			<div class="txt" >
				O<br>
				<input type="number" placeholder="O" id="txto2" value=0 class="TT"/>		
			</div>

			<div class="txt" >
				P<br>
				<input type="number" placeholder="P" id="txtp2" value=0 class="TT"/>		
			</div>
			<div class="txt" >
				Q<br>
				<input type="number" placeholder="Q" id="txtq2" value=0 class="TT"/>		
			</div>
			<div class="txt" >
				R<br>
				<input type="number" placeholder="R" id="txtr2" value=0 class="TT"/>		
			</div>

			<div class="txt" >
				S<br>
				<input type="number" placeholder="S" id="txts2" value=0 class="TT"/>		
			</div>
			<div class="txt" >
				T<br>
				<input type="number" placeholder="T" id="txtt2" value=0 class="TT"/>		
			</div>
			<div class="txt" >
				Tipo Código<br>
				<select id="optionfamiliacodigo2" onChange="getcodesspecific2();"></select><br>
				Código<br>
				<select id="optioncodigo2"></select>	
			</div>
			<div class="txt" >
				Descriptivo de Variables a Considerar<br>
				<textarea rows="3" cols="30" name="dir" id="txtnotas2" placeholder="Describe aquí las Dimensiones del Producto"></textarea>		
			</div>
			
		</div>
	</div>
	<center>
		<div class="txt">
			<input type="button" name="insertar" Value="Actualizar" onClick="savesecondsectionupdate();" id="ok"/>
		</div>		
	</center>
</div>


		<section id="titulo">
			<center><h2 class="secciones" onclick="seccionesss();">Secciones</h2>
			</center>
		</section>

	<div id="allsectionsgral" style="display:none;">
		<section id="titulo" >
			<center id="allsections">
			</center>
		</section>
	</div>
<div id="sec3edit" style="display:none;">
	<hr>
		<section id="titulo">
			<center><h2 class="secciones">Descriptivo de Materiales y Acabados</h2>
			</center>
		</section>

		
	<div class="contenedor">
		<div class="myDiv" >
			<div class="txt" >
				Nombre de la Sección<br>
				<input type="text" placeholder="Nombre" id="txtnombreseccion2" class="TT"/>
			</div>
			<div class="txt" >
				Categorias<br>
				<select id="optioncategorias2" onChange="getsustratosss2(this.value);"></select>	
			</div>
			<div class="txt" >
				Sustrato Base<br>
				<select id="optiontiposustrato2"></select>	
			</div>
			<div class="txt" >
				Empalme<br>
				<select id="optionempalme2"></select>	
			</div>
			<div class="txt" >
				Suaje<br>
				<select id="optionsuaje2"></select>	
			</div>
			<div class="txt" >
				Pegado<br>
				<select id="optionpegado2"></select>	
			</div>
		</div>
	</div>

		<section id="titulo">
			<center><h2>Impresión y Acabados</h2>
			</center>
		</section>
		
	<div class="contenedor">
		<div class="myDiv" >
			<div class="txt" >
				CMYK Frente<br>
				<select id="optiontintafrente2" onChange="changevalue(14,this);"></select>
				<br>
				<select placeholder="Frente" id="txttintafrente2" class="TT"></select>
			</div>
			<div class="txt" >
				CMYK Reverso<br>
				<select id="optiontintareverso2" onChange="changevalue(15,this);"></select><br>
				<select placeholder="Reverso" id="txttintareverso2" class="TT"></select>
			</div>
			<div class="txt" >
				Pantones Frente<br>
				<input type="text" placeholder="Pantones" id="txtpantonesfrente2" class="TT"/>
				<select placeholder="Frente" id="txtnumeropantonesfrente2" onChange="changevalue(19,this);"  class="TT"></select>
			</div>
			<div class="txt" >
				Pantones Reverso<br>
				<input type="text" placeholder="Pantones" id="txtpantonesreverso2" class="TT"/>
				<select placeholder="Reverso" id="txtnumeropantonesreverso2" class="TT" onChange="changevalue(20,this);" ></select>
			</div>
			<div class="txt" >
				Barniz Frente<br>
				<select id="optionbarnizfrente2" onChange="changevalue(9,this);"></select>
				<label class="fondotxt" style="display:none;">
				<input type='radio' name='barnizfrente2' value='1' ">Si
				<input type='radio' name='barnizfrente2' value='0'  ">No
				</label>	
			</div>
			<div class="txt" >
				Barniz Reverso<br>
				<select id="optionbarnizreverso2" onChange="changevalue(10,this);"></select>
				<label class="fondotxt" style="display:none;">
				<input type='radio' name='barnizreverso2' value='1' ">Si
				<input type='radio' name='barnizreverso2' value='0' ">No
				</label>	
			</div>
			<div class="txt" >
				Laminado Frente<br>
				<select id="optionlaminadofrente2" onChange="changevalue(11,this);"></select>
				<label class="fondotxt" style="display:none;">
				<input type='radio' name='laminadofrente2' value='1' ">Si
				<input type='radio' name='laminadofrente2' value='0' ">No
				</label>	
			</div>
			<div class="txt" >
				Laminado Reverso<br>
				<select id="optionlaminadoreverso2" onChange="changevalue(12,this);"></select>
				<label class="fondotxt" style="display:none;">
				<input type='radio' name='laminadoreverso2' value='1' ">Si
				<input type='radio' name='laminadoreverso2' value='0' ">No
				</label>	
			</div>
			<div class="txt" >
				Serigrafía Frente<br>
				<textarea rows="3" cols="30" name="serigrafiafrente" id="serigrafiafrente2" placeholder="Serigrafía"></textarea>
				<select id="optionserigrafiafrente" style='display:none;'></select>
				<!--label class="fondotxt" style="display:none;">
				<input type='radio' name='serigrafiafrente2' value='1' ">Si
				<input type='radio' name='serigrafiafrente2' value='0'  ">No
				</label-->	
			</div>
			<div class="txt" >
				Serigrafía Reverso<br>
				<textarea rows="3" cols="30" name="serigrafiareverso" id="serigrafiareverso2" placeholder="Serigrafía"></textarea>
				<select id="optionserigrafiareverso" style='display:none;'></select>
				<!--label class="fondotxt" style="display:none;">
				<input type='radio' name='serigrafiareverso2' value='1' ">Si
				<input type='radio' name='serigrafiareverso2' value='0'  checked='checked'">No
				</label-->	
			</div>
			<div class="txt" >
				Cambios de Arte Frente<br>
				<select id="txtcambiosartefrente2"></select>
				<!--input type="number" placeholder="Frente" id="txtcambiosartefrente2" class="TT"/-->		
			</div>
			<div class="txt" >
				Cambios de Arte Reverso<br>
				<select id="txtcambiosartereverso2"></select>
				<!--input type="number" placeholder="Reverso" id="txtcambiosartereverso2" class="TT"/-->		
			</div>
		</div>
	</div>
	
	<section id="titulo">
			<center><h2>Ventana</h2>
			</center>
		</section>
		
	<div class="contenedor">
		<div class="myDiv" >
			<div class="txt" >
				Material<br>
				<select id="optionmaterialventana2" onChange="changevalue(13,this);"></select>	
			</div>
			<div class="txt" >
				Largo(mm)<br>
				<input type="number" placeholder="Ventana Largo (mm)" id="txtventanalargo2" class="TT"/>		
			</div>
			<div class="txt" >
				Alto(mm)<br>
				<input type="number" placeholder="Ventana Alto (mm)" id="txtventanaalto2" class="TT"/>		
			</div>
		</div>
	</div>
	<center>
		<!--div class="txt">
			<input type="button" name="insertar" Value="Sección +" onClick="savecotizacionsec32edit();" id="ok"/>
		</div-->
		<div class="txt">
			<input type="button" name="insertar" Value="Actualizar" onClick="savethirdsection();" id="ok"/>
		</div>
	</center>
</div>


			<section id="titulo">
				<center><h2 class="secciones" onclick="terminosycondiciones();">Terminos y Condiciones Comerciales</h2>
				</center>
			</section>	

<div id="sec4edit" style="display:none;">
	<hr>
			<!--section id="titulo">
				<center><h2>Terminos y Condiciones Comerciales</h2>
				</center>
			</section-->	
		</br>
	<div class="contenedor">
		<div class="myDiv">
			<div class="txt" >
				Herramental<br>
				<select id="optionherramental2"></select>	
			</div>
			<div class="txt" >
				Separar Cambios de Arte<br>
				<label class="fondotxt">
				<input type='radio' name='cambiosarte2' value='1' ">Si
				<input type='radio' name='cambiosarte2' value='0'  checked='checked'">No
				</label>	
			</div>
			<div class="txt" >
				Dummy<br>
				<select id="optiondummy2"></select>	
			</div>
			<div class="txt" >
				Agregar a Web to Print<br>
				<label class="fondotxt">
				<input type='radio' name='web2print2' value='1' ">Si
				<input type='radio' name='web2print2' value='0'  checked='checked'">No
				</label>
			</div>
		</div>


	</div>

		<section id="titulo">
			<center><h2>Escala de Volumen Solicitada</h2>
			</center>
		</section>
	<div class="contenedor">
		<datalist id="escalas2"></datalist>
		<div class="myDiv">
			<div class="txt" >
				Primera escala<br>
				<input type="number" placeholder="Escala uno" value=1 list="escalas2" id="txtprimerescala2" class="TT"/>	
					
			</div>
			<div class="txt" >
				Segunda escala<br>
				<input type="number" placeholder="Escala uno" value=1 list="escalas2" id="txtsegundaescala2" class="TT"/>		
			</div>
			<div class="txt" >
				Tercera escala<br>
				<input type="number" placeholder="Escala uno" value=1 list="escalas2" id="txttercerescala2" class="TT"/>		
			</div>
			<div class="txt" >
				Cuarta escala<br>
				<input type="number" placeholder="Escala uno" value=1 list="escalas2" id="txtcuartaescala2" class="TT"/>		
			</div>
			<div class="txt" >
				Quinta escala<br>
				<input type="number" placeholder="Escala uno" value=1 list="escalas2" id="txtquintaescala2" class="TT"/>		
			</div>
			<div class="txt" >
				Sexta escala<br>
				<input type="number" placeholder="Escala uno" value=1 list="escalas2" id="txtsextaescala2" class="TT"/>		
			</div>
			<div class="txt" >
				Costo Objetivo<br>
				<input type="number" placeholder="Costo Objetivo" value=1 id="txtcostoobjetivo2" class="TT"/>		
			</div>
			<div class="txt" >
				Margen de Ganancia<br>
				<select id="optionmargenganacia2"></select>	
			</div>
		</div>


	</div>

	<section id="titulo">
			<center><h2>Terminos Generales</h2>
		</center>
		</section>	
		</br>
	<div class="contenedor">
		<div class="myDiv">
			<div class="txt" >
				Vigencia de la cotización<br>
				<select id="optionvigenciacotizacion2"></select>	
			</div>
			<div class="txt" >
				Forma de pago<br>
				<select id="optionformapago2"></select>	
			</div>
			<div class="txt" >
				Flete<br>
				<select id="optionformaentrega2"></select>	
			</div>	
			
		</div>


	</div>
	<section id="titulo">
			<center><h2>Especificaciones del Embalaje</h2>
		</center>
		</section>	
		</br>
	<div class="contenedor">
		<div class="myDiv">
			<div class="txt" >
				Inner Pack<br>
				<select id="optioninner2" onChange="changevalue(16,this);"></select><br>
				Multiplo	
				<input type="number" placeholder="Inner Pack" value=0 id="txtmultiploinner2" class="TT"/>
			</div>
			<div class="txt" >
				Master Pack<br>
				<select id="optionmasterr2"></select><br>
				Multiplo
				<input type="number" placeholder="Master Pack" value=0 id="txtmultiplomaster2" class="TT"/>
			</div>	
			
		</div>


	</div>
	<center>
		<div class="txt">
			<input type="button" name="insertar" Value="Guardar" onClick="savefourthsectionupdate();" id="ok"/>
		</div>
	</center>
</div>



</div>

</div>
</div>
  
</div>

<!--/////////////////////////////////////////////////////view files of product//////////////////////////////////////////////////////////////////////////////-->


<div id="myModalfiles" class="modal">

  <!-- Modal content -->
  <div class="modal-content"> 
  <div class="modal-content2">	
    <span class="close" id="closefiles">&times;</span>
			<section id="titulo">
        			<center></br><h2>Archivos de la cotización</h2>
					     </br><h3 id="filescotizacion"></h3>
				</center>
			</section>
			</br>
	
	<center>
	</br>
	

		<div class="txt">

			<img src="../img/folder.png" onClick="addfile2();"/>
			
		</div>
		<div class="myDiv" id="docs22" style="display:none;">
	
			<div class="txt">
				Seleccione el archivo<br>
		    		<input type="file" id="file12" class="TT" onchange="enablebtn2();"/>
             	   		
				<center>
					<!--input type="button" name="insertar" Value="Guardar" onClick="" id="ok"/-->
					<textarea rows="6" cols="50" name="dir"  class="TT" id="txtfiledescription2" placeholder="Descripción del archivo"></textarea>
					<br>						
					<img src="../img/save2.png" onclick="uploadfiletoserver2();" id="imgsavefile2" style="display:none;"/>
				</center>
			</div>		
		</div>
		<div >
			<img src="../img/load.gif" class="imgload" id="imgloadfiles">
		</div>
	<div id="scro">

 		<div id="tablafiles">
   			  <table class="tbl-qa" id="resultadofiles2">
		 		 <thead>
					 <tr>
						<th class="table-header" >Descripcion</th>
						<th class="table-header" >Ver</th>
						<th class="table-header" >Editar</th>
						<th class="table-header" >Eliminar</th>
						
              				</tr>
 		  		</thead>
		  		<tbody >				
             	 		</tbody>
              		</table>
 		</div>
		</br></br>
		<div id="sinfiles" style="display:none;">
			<div class='myDiv'><div class='txt'>NO HAY ARCHIVOS REGISTRADOS EN EL SISTEMA </div></div>
		</div>
 	</div>
	</center>	



  </div>
  </div>
  
</div>



</main>
</body>
</html>
