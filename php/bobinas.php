﻿<?php
require_once("usersFunction.php");
$app = new usersFunction();
	if(!$app->islogged()){
		echo "<script>window.top.location.href = 'logout.php';</script>";	
	}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="initial-scale=1.0,user-scalable=yes"/>

<link rel="stylesheet" href="../css/estilo2.css">
<link rel="stylesheet" href="../css/jquery.dataTables.min.css">
 <script language="javascript" type="text/javascript" src="../js/gateway.js"></script>
<script language="javascript" type="text/javascript" src="../js/jquery-1.12.4.js"></script>
<script language="javascript" type="text/javascript" src="../js/jquery.dataTables.min.js"></script>
  <script language="javascript" type="text/javascript" src="../js/bobinas.js"></script>
<title></title>

</head>
<body onload="getbobinas();">
<main>
	
	<section id="titulo">
		<center><h2>Bobinas Registradas del Sistema</h2>
		</center>
	</section>
<div>
	<form name="f1" action="#">
		<center>
		<div id="sin2" style="display:none;">
			<div id="myDiv">
				<!--div class="txt">
					Ingresa el Nombre de la Bobina a Buscar<br>
					<input type="text" name="num" placeholder="Nombre de la bobina" id="txtbuscar" onkeyup="doSearch()" required="required" class="TT" />
				
				</div>
				<div class="txt">
						<input type="button" name="insertar" Value="Actualizar" onClick="" id="ok"/>
				</div-->
			<div >
				<img src="../img/load.gif" class="imgload" id="imgload">
			</div>
		</div>
		</center>
	</form>
</div>
<center>
</br>
	<div class="txt">
			<input type="button" name="insertar" Value="Agregar" onClick="agregar();" id="ok"/>
	</div>
	</br>
<div id="scro">
 <div id="tabla">
     <table class="tbl-qa display nowrap" style="width:100%" id="resultado">
		  <thead>
		 <tr>
		<th class="table-header" >Código SAE</th>
		<th class="table-header" >Nombre</th>
		<th class="table-header" >Largo (mts)</th>
                <th class="table-header" >Ancho (mm)</th>
	        <th class="table-header" >Puntos</th>
	        <th class="table-header" >Gramos</th>
	        <th class="table-header" >Peso aproximado</th>
	        <th class="table-header" >Activo</th>
		<th class="table-header" >Editar</th>
		<th class="table-header" >Eliminar</th>
		
              </tr>
 		  </thead>
		  <tbody style="height:250px;overflow:scroll">				
             	 </tbody>
              </table>
 </div>
	</br></br>
		<div id="sin" style="display:none;">
			<div class='myDiv'><div class='txt'>NO HAY BOBINAS REGISTRADOS EN EL SISTEMA </div></div>
		</div>
 </div>
</center>
</div>

<div id="myModal" class="modal">

  <!-- Modal content -->
  <div class="modal-content"> 
  <div class="modal-content2">	
    <span class="close">&times;</span>
			<section id="titulo">
        			<center></br><h2>Ingrese los datos de la nueva bobina</h2>
					     <h5>Asegurese de que los datos introducidos sean correctos</h5>
				</center>
			</section>
			</br>
	<div class="contenedor">
			
	<div class="myDiv" >
		<div class="txt">
			Código SAE<br>
			<input type="text" name="num" placeholder="Código SAE" id="txtcodigo" required="required" onblur='validateSAEbobina();' maxlength="8" class="TT" />	
		</div>
		<div class="txt">
			Nombre de la bobina<br>
			<input type="text" name="num" placeholder="Nombre" id="txtnombre" required="required" disabled class="TT" />	
		</div>
		<div class="txt">
			Largo (mts) <br>
			<input type="number" name="num" placeholder="Largo(mts)" id="txtlargo" required="required" disabled class="TT" />	
		</div>
		<div class="txt">
			Ancho (mm)<br>
			<input type="number" name="num" placeholder="Ancho(mm)" id="txtancho" required="required" disabled class="TT" />	
		</div>
		<div class="txt">
			Puntos<br>
			<input type="number" name="num" placeholder="Puntos" id="txtpuntos" required="required" disabled class="TT" />	
		</div>
		<div class="txt">
			Gramos<br>
			<input type="number" name="num" placeholder="Gramos" id="txtgramos" required="required" disabled class="TT" />	
		</div>
		<div class="txt">
			Peso neto aproximado(Kg)<br>
			<input type="number" name="num" placeholder="Peso neto aproximado(Kg)" id="txtpeso" required="required" disabled class="TT" />	
		</div>
		
	</div> 
			

	<center>
	<div class="txt">
	<input type="button" name="btnsave" Value="Guardar" onClick="saverecord();" id="ok"/>
	</div>
	</center>
	</div>   
  </div>
  </div>
  
</div>
</main>
</body>
</html>
