﻿<?php
require_once("usersFunction.php");
$app = new usersFunction();
	if(!$app->islogged()){
		echo "<script>window.top.location.href = 'logout.php';</script>";	
	}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="initial-scale=1.0,user-scalable=yes"/>

<link rel="stylesheet" href="../css/estilo2.css">
<link rel="stylesheet" href="../css/jquery.dataTables.min.css">
 <script language="javascript" type="text/javascript" src="../js/gateway.js"></script>
<script language="javascript" type="text/javascript" src="../js/jquery-1.12.4.js"></script>
<script language="javascript" type="text/javascript" src="../js/jquery.dataTables.min.js"></script>
  <script language="javascript" type="text/javascript" src="../js/componentes.js"></script>
<title></title>

</head>
<body onload="getcomponentes();">
<main>
	
	<section id="titulo">
		<center><h2>Componentes</h2>
		</center>
	</section>
<div>
	<form name="f1" action="#">
		<center>
		<div id="sin2" style="display:none;">
			<div id="myDiv">
				<!--div class="txt">
					Ingresa el Nombre del Usuario a Buscar<br>
					<input type="text" name="num" placeholder="Nombre del Usuario" id="txtbuscar" onkeyup="doSearch()" required="required" class="TT" />
				
				</div>
				<div class="txt">
						<input type="button" name="insertar" Value="Actualizar" onClick="getusuarios();" id="ok"/>
				</div-->
			<div >
				<img src="../img/load.gif" class="imgload" id="imgload">
			</div>
		</div>
		</center>
	</form>
</div>
<center>
</br>
	<div class="txt">
			<input type="button" name="insertar" Value="Agregar" onClick="agregar();" id="ok"/>
	</div>
	</br>
<div id="scro">
 <div id="tabla">
     <table class="tbl-qa display nowrap" style="width:100%" id="resultado">
		  <thead>
		 <tr>
		<th class="table-header" >Id</th>
		<th class="table-header" >Código SAE</th>
                <th class="table-header" >Descripción</th>
		<th class="table-header" >Línea</th>
		<th class="table-header" >Unidad de Entrada</th>
		<th class="table-header" >Ultimo Costo</th>
		<th class="table-header" >Activo</th>
		<th class="table-header" style="display:none;" >Editar</th>
		<th class="table-header" >Acciones</th>
		
              </tr>
 		  </thead>
		  
		<tbody style="height:250px;overflow:scroll">				
             	 </tbody>
              </table>
 </div>
	</br></br>
		<div id="sin" style="display:none;">
			<div class='myDiv'><div class='txt'>NO HAY COMPONENTES REGISTRADOS EN EL SISTEMA </div></div>
		</div>
 </div>
</center>
</div>

<div id="myModal" class="modal">

  <!-- Modal content -->
  <div class="modal-content"> 
  <div class="modal-content2">	
    <span class="close">&times;</span>
			<section id="titulo">
        			<center></br><h2>Ingrese los datos del nuevo componente</h2>
					     <h5>Asegurese de que los datos introducidos sean correctos</h5>
				</center>
			</section>
			</br>
	<div class="contenedor">
			
	<div class="myDiv" >
		<div class="txt">
			Clave SAE<br>
			<input type="number" name="num" placeholder="Clave SAE" id="txtclave" maxlength="8" required="required" class="TT" />	
		</div>
		<div class="txt">
			Descripción <br>
			<input type="text" name="num" placeholder="Descripción" id="txtdescripcion" required="required" class="TT" />	
		</div>
		<div class="txt">
			Línea<br>
			<input type="text" name="num" placeholder="Línea" id="txtlinea" required="required" class="TT" />	
		</div>
		<div class="txt">
			Unidad de Entrada<br>
			<input type="text" name="num" placeholder="Unidad de Entrada" id="txtume" required="required" class="TT" />	
		</div>
		<div class="txt">
			Ultimo Costo<br>
			<input type="number" name="num" placeholder="Ultimo Costo" id="txtultimocosto" value =0 required="required" class="TT" />	
		</div>
		<!--div class="txt">
			Confirma la contraseña<br>
			<input type="password" name="num" placeholder="Confirma la contraseña" id="txtpassword2" required="required" class="TT" />	
		</div>
		<div class="txt">
			El usuario es operador<br>
			<label class="fondotxt">
				<input type='radio' name='operador' value='1' ">Si
				<input type='radio' name='operador' value='0'  checked='checked'">No
			</label>
		</div>
		<div class="txt">
			El usuario es<br>
			<label class="fondotxt">
				<input type='radio' name='vendedor' value='4' ">Vendedor
				<input type='radio' name='vendedor' value='1' ">Cotizador
				<input type='radio' name='vendedor' value='2' ">Servicio a Ventas
				<input type='radio' name='vendedor' value='3' ">Supervisor
				<input type='radio' name='vendedor' value='0'  checked='checked'">N/A
			</label>
		</div-->
						
	</div> 
			<!--section id="titulo">
        			<center></br><h2>Seleccione los módulos de acceso del nuevo usuario</h2>
					 
				</center>
			</section>
			</br>
	<div class="myDiv" id="historia">
				

	</div-->
	<center>
	<div class="txt">
	<input type="button" name="insertar" Value="Guardar" onClick="savecomponente();" id="ok"/>
	</div>
	</center>
	</div>   
  </div>
  </div>
  
</div>
</main>
</body>
</html>
