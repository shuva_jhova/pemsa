var str2;
var editmodulos;
var modulosdata;
var modulosdata2;
var inedition=false;

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

function getclientes(){
	REQUEST('appControl', 'clientesandcodigos', [] , onClientes, onerror);
}

function onClientes(dataResult){

	var dataList = document.getElementById('clientes');
	dataList.innerHTML="";
	for (var i = 0; i<dataResult[0]['clientes'].length; i++)
	{
    		var opt = document.createElement('option');
    		opt.value = dataResult[0]['clientes'][i]['cliente'];
		dataList.appendChild(opt);
    	}

	

}

///////////////////////////////////////////////////////////GET PLANNING/////////////////////////////////////////////////////////////////////////////////////
function getdepartaments(){
	REQUEST('appControl', 'getdepartaments', [] , onDepartaments, onerror);
}
function onDepartaments(dataResult){
	var op=document.getElementById("optionarea");
	op.innerHTML="";
	var option = document.createElement("option");
	    		option.value='nada';
    			option.text ="---Seleccione---";
			
    			op.appendChild(option);
			var option = document.createElement("option");
	    		option.value='todas';
    			option.text ="Todas";
			
    			op.appendChild(option);
	if(dataResult.length>0){
		for(var a=0;a<dataResult.length;a++){
			option = document.createElement("option");
	    		option.value=dataResult[a]['nombre'];
    			option.text =dataResult[a]['nombre'];
			
    			op.appendChild(option);
			
		}
		
	}
	
}

function getincidents(){
		
	
	var fechainicio=document.getElementById("txtfechainicial").value;
	var fechafin=document.getElementById("txtfechafinal").value;
	var area=document.getElementById("optionarea").value;

	if(fechainicio.length==0 || fechafin.length==0){
		alert("Las fechas o pueden estar vacias");
	}else if(area.localeCompare("nada")==0){
		alert("Seleccione una \u00E1rea v\u00E1lida");
	}else{
		document.getElementById("imgload").style.display="block";
		REQUEST('appControl', 'getincidentsbydate', [fechainicio,fechafin,area] , onincidents, onerror);
	}
}

function onincidents(dataResult){
	inedition=false;
	document.getElementById("imgload").style.display="none";
	myDeleteFunction() ;
		if(dataResult.length>0){
			

		for (var i = 0; i<dataResult.length; i++)
		{		
			var tableRef = document.getElementById('resultado').getElementsByTagName('tbody')[0];
			// Insert a row in the table at the last row
			var newRow   = tableRef.insertRow(tableRef.rows.length);
			newRow.className = "table-row";
			newRow.id=dataResult[i]["id"];
			// Insert a cell in the row at index 0
			var newCell  = newRow.insertCell(0);
			newCell.id="omp"+dataResult[i]["id"];
			var newText  = document.createTextNode(dataResult[i]["idorden"]);
			newCell.appendChild(newText);

			 newCell  = newRow.insertCell(1);
			newCell.id="areadenuncia"+dataResult[i]["id"];
			//newCell.contentEditable = "true";	
			// Append a text node to the cell
			 newText  = document.createTextNode(dataResult[i]["areadenuncia"]);
			newCell.appendChild(newText);


 			newCell  = newRow.insertCell(2);
			newCell.id="areadenunciada"+dataResult[i]["id"];
			//newCell.contentEditable = "true";
			newText = document.createTextNode(dataResult[i]["areadenunciada"]);
			newCell.appendChild(newText);
			


 			newCell  = newRow.insertCell(3);
			newCell.id="responsable"+dataResult[i]["id"];
			//newCell.contentEditable = "true";
			newText = document.createTextNode(dataResult[i]["nombredenunciado"]+' '+dataResult[i]["apellidosdenunciado"]);
			newCell.appendChild(newText);
			


 			newCell  = newRow.insertCell(4);
			newCell.id="incidente"+dataResult[i]["id"];
			//newCell.contentEditable = "true";
			newText = document.createTextNode(dataResult[i]["incidente"]);
			newCell.appendChild(newText);
			


 			newCell  = newRow.insertCell(5);
			newCell.id="fecha"+dataResult[i]["id"];
			//newCell.contentEditable = "true";
			newText = document.createTextNode(dataResult[i]["fecha"]);
			newCell.appendChild(newText);
			
			

 			
			newCell  = newRow.insertCell(6);
			var img2 = document.createElement('img');
   			img2.src = "../img/print.png";
			img2.addEventListener('click', prinincident.bind(null,dataResult[i]["id"]));
			newCell.appendChild(img2);

			//newRow.addEventListener('click', navigateToController.bind(null, dataResult[i]["usuario"]));
		}
		
		
		document.getElementById("sin").style.display="none";
		document.getElementById("tabla").style.display="block";
		document.getElementById("sin2").style.display="block";
		imprimir("Reporte de Incidentes");
		//pagination('#resultado');

	}else{
		document.getElementById("sin").style.display="block";
		document.getElementById("tabla").style.display="none";
		document.getElementById("sin2").style.display="none";
	
	}	

}

function prinincident(str){
	window.open('incidentepdf.php?idi='+str, '_blank');
}
/////////////////////////////////////////////////////PRINT TABLE///////////////////////////////////////////////////////////////////////////////////////////////////////

function imprimir(reporte){
		
	// = TableExport(document.getElementById("resultado"));
	
	var table= TableExport(document.getElementById("resultado"), {
    		headers: true,                              // (Boolean), display table headers (th or td elements) in the <thead>, (default: true)
   		footers: true,                              // (Boolean), display table footers (th or td elements) in the <tfoot>, (default: false)
   		formats: ['xlsx'],            // (String[]), filetype(s) for the export, (default: ['xlsx', 'csv', 'txt'])
    		filename: reporte,                             // (id, String), filename for the downloaded file, (default: 'id')
    		bootstrap: true,                           // (Boolean), style buttons using bootstrap, (default: true)
    		exportButtons: true,                        // (Boolean), automatically generate the built-in export buttons for each of the specified formats (default: true)
    		position: 'bottom',                         // (top, bottom), position of the caption element relative to table, (default: 'bottom')
    		ignoreRows: null,                           // (Number, Number[]), row indices to exclude from the exported file(s) (default: null)
    		ignoreCols: null,                           // (Number, Number[]), column indices to exclude from the exported file(s) (default: null)
    		trimWhitespace: true                        // (Boolean), remove all leading/trailing newlines, spaces, and tabs from cell text in the exported file(s) (default: false)
	});
		table.reset();
}
///////////////////////////////////////////////////////////GENERAL FUNCTIONS/////////////////////////////////////////////////////////////////////////////////////
function showEdit(editableObj) {
			editableObj.style.backgroundColor="#e3bfc4";

		}
function edita(editableObj) {
			editableObj.style.backgroundColor="#FFCC99";
		}

function onerror(errObject)
{
		alert(errObject["Message"] + ":\n" + errObject["Detail"]);
}






function pagination(id){
		$(document).ready(function() {
   			table= $(id).DataTable( {
				"destroy":true,
      				 "scrollY": 500,
       				 "scrollX": true
    			} );
			
		} );
}




function myDeleteFunction() {
	var tableRef = document.getElementById('resultado');
	//var rowCount = tableRef.rows.length;
	
	var row = document.getElementsByTagName('tbody')[0];
	var rowCount = row.rows.length;

		for(var a=0;a<rowCount;a++)
		{

			//tableRef.deleteRow(1);
			row.deleteRow(0);
		}
    
}

function getval(grupo){
	var selec;

	for(var i = 0; i < grupo.length; i++) {
 	  if(grupo[i].checked)
    	   selec = grupo[i].value;
 	}
return selec;
}

function doSearch()
		{
			//var tableReg = document.getElementById('resultado');
			var tableReg = document.getElementsByTagName('tbody')[0];

			var searchText = document.getElementById('txtbuscar').value.toLowerCase();
			var cellsOfRow="";
			var found=false;
			var compareWith="";
 
			// Recorremos todas las filas con contenido de la tabla
			for (var i = 0; i < tableReg.rows.length; i++)
			{
				cellsOfRow = tableReg.rows[i].getElementsByTagName('td');
				found = false;
				// Recorremos todas las celdas
				for (var j = 0; j < cellsOfRow.length && !found; j++)
				{
					compareWith = cellsOfRow[j].innerHTML.toLowerCase();
					// Buscamos el texto en el contenido de la celda
					if (searchText.length == 0 || (compareWith.indexOf(searchText) > -1))
					{
						found = true;
					}
				}
				if(found)
				{
					tableReg.rows[i].style.display = '';
				} else {
					// si no ha encontrado ninguna coincidencia, esconde la
					// fila de la tabla
					tableReg.rows[i].style.display = 'none';
				}
			}
		}
function isNumber( input ) {
    return !isNaN( input );
}
