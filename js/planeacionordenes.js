////////////////////////////////////////////////////GLOBAL VARIABLES/////////////////////////////////////////////////////////////////////////////
var str2;
var perfil=localStorage.getItem("perfil");
var isoperator=localStorage.getItem("operador");
var iduser=localStorage.getItem("id");
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

function getstatus(){
	REQUEST('appControl', 'getallstatusfororders', [] , onStatusorder, onerror);
}

function onStatusorder(dataResult){

	var op= document.getElementById("optionstatus");
   		
	op.innerHTML="";
	var option = document.createElement("option");
	    		option.value=-1;
    			option.text ="---Seleccione---";
			
    			op.appendChild(option);
		//	option = document.createElement("option");
	    	//	option.value='0';
    		//	option.text ="Todas";
			
    		//	op.appendChild(option);
	if(dataResult.length>0){
		for(var a=0;a<dataResult.length;a++){
			if(dataResult[a]['status'].localeCompare("Proceso")==0){
				for(var b=0;b<dataResult.length;b++){
					if(dataResult[b]['status'].localeCompare("Programado(a)")==0){
						option = document.createElement("option");
	    					option.value=dataResult[a]['id']+"."+dataResult[b]['id'];
    						option.text =dataResult[a]['status']+"/"+dataResult[b]['status'];
						op.appendChild(option);
					}
				}
			}else if(dataResult[a]['status'].localeCompare("Programado(a)")==0 || dataResult[a]['status'].localeCompare("Sin Planear")==0){

			}
			else{
				option = document.createElement("option");
	    			option.value=dataResult[a]['id'];
    				option.text =dataResult[a]['status'];
				op.appendChild(option);
			}
		}
		
	}
	op.selectedIndex = "0";

}
function getOrdenes(status){
	REQUEST('appControl', 'getallordersbystatus', [status] , onOrders, onerror);
}

function onOrders(dataResult){

	var op= document.getElementById("optionorder");
   		
	op.innerHTML="";
	var option = document.createElement("option");
	    		option.value=-1;
    			option.text ="---Seleccione---";
			
    			op.appendChild(option);
		
			
	if(dataResult.length>0){
			option = document.createElement("option");
	    		option.value=-2;
    			option.text ="Todas";
			
    			//op.appendChild(option);
		for(var a=0;a<dataResult.length;a++){
			if(dataResult[a]['status'].localeCompare("Proceso")==0 || dataResult[a]['status'].localeCompare("Programado(a)")==0){

				option = document.createElement("option");
	    			option.value=dataResult[a]['id'];
    				option.text ="Orden No."+dataResult[a]['id']+" Pedido SAE:"+dataResult[a]['pedidosae'];
				op.appendChild(option);
			}else{
				option = document.createElement("option");
	    			option.value=dataResult[a]['id'];
    				option.text ="Orden No."+dataResult[a]['id']+" Pedido SAE:"+dataResult[a]['pedidosae'];
				op.appendChild(option);
			}
		}
			
	}
	//op.selectedIndex = 0;

}

///////////////////////////////////////////////////////////GET PLANNING/////////////////////////////////////////////////////////////////////////////////////

function getplaneacion(){
		
	
	var status=document.getElementById("optionstatus").value;
	
	var orden=document.getElementById("optionorder").value;

	if(status<0){
		alert("Seleccione un estado valido");
	}else if(orden<0 && orden!=-2){
		alert("Seleccione una orden valida");
	}else if(orden==-2){
	
		document.getElementById("imgload").style.display="block";
		REQUEST('appControl', 'getplaneacionallordersbystatus', [status] , onPlaneacion2, onerror);
	}
	else if(orden>0){	
		document.getElementById("imgload").style.display="block";
		REQUEST('appControl', 'getplaneacionbyorders', [status,orden] , onPlaneacion, onerror);
	}
}
function onPlaneacion2(dataResult){
	document.getElementById("imgload").style.display="none";
	myDeleteFunctionhead();
	myDeleteFunction();
	var data = [];
	if(dataResult.length>0){
			 
			var tableRef = document.getElementById('resultado').getElementsByTagName('thead')[0];
			// Insert a row in the table at the last row
			var newRow   = tableRef.insertRow(tableRef.rows.length);
			newRow.insertCell(0).outerHTML = "<th class='table-header'>Fecha/<br>Orden</th>";
			
			var min=dataResult[0]['min'].split("-");
			var max=dataResult[0]['max'].split("-");
			var dd,mm;
			var contcells=1;			
			for(var y=min[0];y<=max[0];y++){

				for(var m=parseInt(min[1]);m<=max[1];m++){
					if(min[1]<max[1]){
						if(min[1]==m){

							for(var d=min[2];d<=daysInMonth(min[1],min[0]);d++){
								if(parseInt(d)<10){
									dd='0'+parseInt(d);
								}else{
									dd=d;
								}
								if(parseInt(m)<10){
									mm='0'+parseInt(m);
								}else{
									mm=m;
								}							
								newRow.insertCell(contcells).outerHTML = "<th class='table-header'>"+dd+"-"+mm+"-"+y+"</th>";
								data.push(y+"-"+mm+"-"+dd);
								contcells++;
							}
						}else{
							for(var d=1;d<=max[2];d++){
								if(parseInt(d)<10){
									dd='0'+parseInt(d);
								}else{
									dd=d;
								}
								if(parseInt(m)<10){
									mm='0'+parseInt(m);
								}else{
									mm=m;
								}							
								newRow.insertCell(contcells).outerHTML = "<th class='table-header'>"+dd+"-"+mm+"-"+y+"</th>";
								data.push(y+"-"+mm+"-"+dd);
								contcells++;
							}
						}

					}else{					
						for(var d=min[2];d<=max[2];d++){
								if(parseInt(d)<10){
									dd='0'+parseInt(d);
								}else{
									dd=d;
								}
								if(parseInt(m)<10){
									mm='0'+parseInt(m);
								}else{
									mm=m;
								}
							newRow.insertCell(contcells).outerHTML = "<th class='table-header'>"+dd+"-"+mm+"-"+y+"</th>";
							data.push(y+"-"+mm+"-"+dd);
							contcells++;
						}
					}
				}

			}


			
		for(var i=0;i<dataResult.length;i++){
			
			var tableRef = document.getElementById('resultado').getElementsByTagName('tbody')[0];;
			// Insert a row in the table at the last row
			var newRow   = tableRef.insertRow(tableRef.rows.length);
			newRow.className = "table-row";
			newRow.id=dataResult[i]['id'];
			var newCell  = newRow.insertCell(0);
			linebreak = document.createElement("br");
			newText  = document.createTextNode("No Orden: "+dataResult[i]['id']);
			newCell.appendChild(newText);
			newCell.appendChild(linebreak);

			newText  = document.createTextNode("  Pedido SAE: "+dataResult[i]['pedidoSAE']);

			newCell.appendChild(newText);
			newCell.appendChild(linebreak);

			newText  = document.createTextNode("  Cantidad: "+dataResult[i]['cantidad']);
			newCell.appendChild(newText);
			newCell.appendChild(linebreak);


			newText  = document.createTextNode("  Fecha requerida: "+dataResult[i]['fecharequerida']);
			newCell.appendChild(newText);
			

			
			for(var ii=0;ii<data.length;ii++){
			
				newCell  = newRow.insertCell(ii+1);
				newCell.id=data[ii]+'-'+dataResult[i]['id'];
				// Append a text node to the cell
				newText  = document.createTextNode("");
				newCell.appendChild(newText);
			}
			

			
			
		}
		var color="";
		var bancolor=false;
		for(var t=0;t<dataResult.length;t++){
			if(bancolor){
				color="#d0902d";
			}else{
				color="#dfd492";
				//color="#d0902d";
			}
			bancolor=!bancolor;
			var min=dataResult[t]['fechainicio'].split("-");
			var max=dataResult[t]['fechaFin'].split("-");
			
						
			for(var y=min[0];y<=max[0];y++){

				for(var m=parseInt(min[1]);m<=max[1];m++){
					if(min[1]<max[1]){
						if(min[1]==m){

							for(var d=min[2];d<=daysInMonth(min[1],min[0]);d++){
								if(parseInt(d)<10){
									dd='0'+parseInt(d);
								}else{
									dd=d;
								}
								if(parseInt(m)<10){
									mm='0'+parseInt(m);
								}else{
									mm=m;
								}
								//if(d==min[2]){
										
									
									
									//document.getElementById(y+"-"+mm+"-"+dd+"-"+dataResult[t]['id']).innerText = "Hora de inicio: "+dataResult[t]['horainicio']+" Hora de termino: "+dataResult[t]['horaFin'];	
									document.getElementById(y+"-"+mm+"-"+dd+"-"+dataResult[t]['id']).style.backgroundColor=color;
										

								//}else{
								//		document.getElementById(y+"-"+mm+"-"+dd+hrr).innerText = "No de orden: "+dataResult[t]['orden']+" Proceso: "+dataResult[t]['proceso']+" Pedido SAE: "+dataResult[t]['pedidoSAE'];	
								//		document.getElementById(y+"-"+mm+"-"+dd+hrr).style.backgroundColor=color;
									

								//}		
														
								//document.getElementById(y+"-"+mm+"-"+dd).innerText = "No de orden: "+dataResult[t]['orden']+" Proceso: "+dataResult[t]['proceso']
								
							}
						}else{
							for(var d=1;d<=max[2];d++){
								if(parseInt(d)<10){
									dd='0'+parseInt(d);
								}else{
									dd=d;
								}
								if(parseInt(m)<10){
									mm='0'+parseInt(m);
								}else{
									mm=m;
								}
								//if(d==max[2]){
										
									
									//document.getElementById(y+"-"+mm+"-"+dd+"-"+dataResult[t]['id']).innerText = "Hora de inicio: "+dataResult[t]['horainicio']+" Hora de termino: "+dataResult[t]['horaFin'];
									document.getElementById(y+"-"+mm+"-"+dd+"-"+dataResult[t]['id']).style.backgroundColor=color;
										

								//}else{
									
								//		document.getElementById(y+"-"+mm+"-"+dd+hrr).innerText = "No de orden: "+dataResult[t]['orden']+" Proceso: "+dataResult[t]['proceso']+" Pedido SAE: "+dataResult[t]['pedidoSAE'];	
								//		document.getElementById(y+"-"+mm+"-"+dd+hrr).style.backgroundColor=color;
									

								//}		

															

							}
						}

					}else{					
						for(var d=min[2];d<=max[2];d++){
								if(parseInt(d)<10){
									dd='0'+parseInt(d);
								}else{
									dd=d;
								}
								if(parseInt(m)<10){
									mm='0'+parseInt(m);
								}else{
									mm=m;
								}
							
								//document.getElementById(y+"-"+mm+"-"+dd+"-"+dataResult[t]['id']).innerText = "Hora de inicio: "+dataResult[t]['horainicio']+" Hora de termino: "+dataResult[t]['horaFin'];
								document.getElementById(y+"-"+mm+"-"+dd+"-"+dataResult[t]['id']).style.backgroundColor=color;
							
							//document.getElementById(y+"-"+mm+"-"+dd).innerText = "No de orden: "+dataResult[t]['orden']+"Proceso: "+dataResult[t]['proceso']
						}
					}
				}

			}
		}

		imprimir("Reporte "+dataResult[0]['status']);
		document.getElementById("sin").style.display="none";
		document.getElementById("tabla").style.display="block";
		//pagination('#resultado');
	
	}else{
		document.getElementById("sin").style.display="block";
		document.getElementById("tabla").style.display="none";
	}			

}
function pagination(id){
		$(document).ready(function() {
   			 $(id).DataTable( {
				"destroy":true,
      				 "scrollY": 500,
       				 "scrollX": true
    			} );
		} );
}
function onPlaneacion(dataResult){
	document.getElementById("imgload").style.display="none";
	myDeleteFunctionhead();
	myDeleteFunction();
	var data = [];
	if(dataResult.length>0){
			 
			var tableRef = document.getElementById('resultado').getElementsByTagName('thead')[0];
			// Insert a row in the table at the last row
			var newRow   = tableRef.insertRow(tableRef.rows.length);
			newRow.insertCell(0).outerHTML = "<th class='table-header'>Fecha/<br>Proceso</th>";
			
			var min=dataResult[0]['min'].split("-");
			var max=dataResult[0]['max'].split("-");
			var dd,mm;
			var contcells=1;			
			for(var y=min[0];y<=max[0];y++){

				for(var m=parseInt(min[1]);m<=max[1];m++){
					if(min[1]<max[1]){
						if(min[1]==m){

							for(var d=min[2];d<=daysInMonth(min[1],min[0]);d++){
								if(parseInt(d)<10){
									dd='0'+parseInt(d);
								}else{
									dd=d;
								}
								if(parseInt(m)<10){
									mm='0'+parseInt(m);
								}else{
									mm=m;
								}							
								newRow.insertCell(contcells).outerHTML = "<th class='table-header'>"+dd+"-"+mm+"-"+y+"</th>";
								data.push(y+"-"+mm+"-"+dd);
								contcells++;
							}
						}else{
							for(var d=1;d<=max[2];d++){
								if(parseInt(d)<10){
									dd='0'+parseInt(d);
								}else{
									dd=d;
								}
								if(parseInt(m)<10){
									mm='0'+parseInt(m);
								}else{
									mm=m;
								}							
								newRow.insertCell(contcells).outerHTML = "<th class='table-header'>"+dd+"-"+mm+"-"+y+"</th>";
								data.push(y+"-"+mm+"-"+dd);
								contcells++;
							}
						}

					}else{					
						for(var d=min[2];d<=max[2];d++){
								if(parseInt(d)<10){
									dd='0'+parseInt(d);
								}else{
									dd=d;
								}
								if(parseInt(m)<10){
									mm='0'+parseInt(m);
								}else{
									mm=m;
								}
							newRow.insertCell(contcells).outerHTML = "<th class='table-header'>"+dd+"-"+mm+"-"+y+"</th>";
							data.push(y+"-"+mm+"-"+dd);
							contcells++;
						}
					}
				}

			}

		//for(var i=0;i<dataResult.length;i++){
			
		//	newRow.insertCell(i+1).outerHTML = "<th class='table-header'>"+dataResult[i]['fechacita']+"</th>";
		//}
			
		for(var i=0;i<dataResult.length;i++){
			
			var tableRef = document.getElementById('resultado').getElementsByTagName('tbody')[0];;
			// Insert a row in the table at the last row
			var newRow   = tableRef.insertRow(tableRef.rows.length);
			newRow.className = "table-row";
			newRow.id=dataResult[i]['id'];
			newText  = document.createTextNode(dataResult[i]['proceso']);
			var newCell  = newRow.insertCell(0);
			newCell.appendChild(newText);

			// Insert a cell in the row at index 0
			for(var ii=0;ii<data.length;ii++){
			
				newCell  = newRow.insertCell(ii+1);
				newCell.id=data[ii]+'-'+dataResult[i]['id'];
				// Append a text node to the cell
				newText  = document.createTextNode("");
				newCell.appendChild(newText);
			}
			

			
			
		}
		var color="";
		var bancolor=false;
		for(var t=0;t<dataResult.length;t++){
			if(bancolor){
				color="#d0902d";
			}else{
				//color="#dfd492";
				color="#d0902d";
			}
			bancolor=!bancolor;
			var min=dataResult[t]['fechacita'].split("-");
			var max=dataResult[t]['fechaFin'].split("-");
			var hrini=dataResult[t]['horainicio'].split(":");
			var hrfin=dataResult[t]['horaFin'].split(":");
						
			for(var y=min[0];y<=max[0];y++){

				for(var m=parseInt(min[1]);m<=max[1];m++){
					if(min[1]<max[1]){
						if(min[1]==m){

							for(var d=min[2];d<=daysInMonth(min[1],min[0]);d++){
								if(parseInt(d)<10){
									dd='0'+parseInt(d);
								}else{
									dd=d;
								}
								if(parseInt(m)<10){
									mm='0'+parseInt(m);
								}else{
									mm=m;
								}
								//if(d==min[2]){
										
									
									
									document.getElementById(y+"-"+mm+"-"+dd+"-"+dataResult[t]['id']).innerText = "Hora Inicio: "+dataResult[t]['horainicio']+" Hora Fin: "+dataResult[t]['horaFin'];	
									document.getElementById(y+"-"+mm+"-"+dd+"-"+dataResult[t]['id']).style.backgroundColor=color;
										

								//}else{
								//		document.getElementById(y+"-"+mm+"-"+dd+hrr).innerText = "No Orden: "+dataResult[t]['orden']+" Proceso: "+dataResult[t]['proceso']+" Pedido SAE: "+dataResult[t]['pedidoSAE'];	
								//		document.getElementById(y+"-"+mm+"-"+dd+hrr).style.backgroundColor=color;
									

								//}		
														
								//document.getElementById(y+"-"+mm+"-"+dd).innerText = "No Orden: "+dataResult[t]['orden']+" Proceso: "+dataResult[t]['proceso']
								
							}
						}else{
							for(var d=1;d<=max[2];d++){
								if(parseInt(d)<10){
									dd='0'+parseInt(d);
								}else{
									dd=d;
								}
								if(parseInt(m)<10){
									mm='0'+parseInt(m);
								}else{
									mm=m;
								}
								//if(d==max[2]){
										
									
									document.getElementById(y+"-"+mm+"-"+dd+"-"+dataResult[t]['id']).innerText = "Hora Inicio: "+dataResult[t]['horainicio']+" Hora Fin: "+dataResult[t]['horaFin'];
									document.getElementById(y+"-"+mm+"-"+dd+"-"+dataResult[t]['id']).style.backgroundColor=color;
										

								//}else{
									
								//		document.getElementById(y+"-"+mm+"-"+dd+hrr).innerText = "No de orden: "+dataResult[t]['orden']+" Proceso: "+dataResult[t]['proceso']+" Pedido SAE: "+dataResult[t]['pedidoSAE'];	
								//		document.getElementById(y+"-"+mm+"-"+dd+hrr).style.backgroundColor=color;
									

								//}		

															

							}
						}

					}else{					
						for(var d=min[2];d<=max[2];d++){
								if(parseInt(d)<10){
									dd='0'+parseInt(d);
								}else{
									dd=d;
								}
								if(parseInt(m)<10){
									mm='0'+parseInt(m);
								}else{
									mm=m;
								}
							
								document.getElementById(y+"-"+mm+"-"+dd+"-"+dataResult[t]['id']).innerText = "Hora Inicio: "+dataResult[t]['horainicio']+" Hora Fin: "+dataResult[t]['horaFin'];
								document.getElementById(y+"-"+mm+"-"+dd+"-"+dataResult[t]['id']).style.backgroundColor=color;
							
							//document.getElementById(y+"-"+mm+"-"+dd).innerText = "No de orden: "+dataResult[t]['orden']+"Proceso: "+dataResult[t]['proceso']
						}
					}
				}

			}
		}

		imprimir("Reporte orden No. "+dataResult[0]['id']);
		document.getElementById("sin").style.display="none";
		document.getElementById("tabla").style.display="block";
		//pagination('#resultado');
	
	}else{
		document.getElementById("sin").style.display="block";
		document.getElementById("tabla").style.display="none";
	}	
}
/////////////////////////////////////////////////////PRINT TABLE///////////////////////////////////////////////////////////////////////////////////////////////////////

function imprimir(reporte){
		
	// = TableExport(document.getElementById("resultado"));
	
	var table= TableExport(document.getElementById("resultado"), {
    		headers: true,                              // (Boolean), display table headers (th or td elements) in the <thead>, (default: true)
   		footers: true,                              // (Boolean), display table footers (th or td elements) in the <tfoot>, (default: false)
   		formats: ['xlsx'],            // (String[]), filetype(s) for the export, (default: ['xlsx', 'csv', 'txt'])
    		filename: reporte,                             // (id, String), filename for the downloaded file, (default: 'id')
    		bootstrap: true,                           // (Boolean), style buttons using bootstrap, (default: true)
    		exportButtons: true,                        // (Boolean), automatically generate the built-in export buttons for each of the specified formats (default: true)
    		position: 'bottom',                         // (top, bottom), position of the caption element relative to table, (default: 'bottom')
    		ignoreRows: null,                           // (Number, Number[]), row indices to exclude from the exported file(s) (default: null)
    		ignoreCols: null,                           // (Number, Number[]), column indices to exclude from the exported file(s) (default: null)
    		trimWhitespace: true                        // (Boolean), remove all leading/trailing newlines, spaces, and tabs from cell text in the exported file(s) (default: false)
	});
		table.reset();
}
////////////////////////////////////////////////////GLOBAL FUNCTIONS FOR DIFFERENT PROCESS/////////////////////////////////////////////////////////////////////////////
document.onkeydown = ShowKeyCode;
        function ShowKeyCode(evt) {
	//alert(localStorage.getItem("operador"));
            	if(evt.keyCode==8 ){
			
			cod=evt.keyCode;
		}else{
			cod=evt.keyCode;
		}
			
        }

function daysInMonth (month, year) {
    return new Date(year, month, 0).getDate();
}


function showEdit(editableObj) {
			editableObj.style.backgroundColor="#e3bfc4";

		}
function edita(editableObj) {
			editableObj.style.backgroundColor="#FFCC99";
		}

function onerror(errObject)
{
		alert(errObject["Message"] + ":\n" + errObject["Detail"]);
}


function getval(grupo){
	var selec;

	for(var i = 0; i < grupo.length; i++) {
 	  if(grupo[i].checked)
    	   selec = grupo[i].value;
 	}
return selec;
}



function myDeleteFunction() {
	var tableRef = document.getElementById('resultado');
	//var rowCount = tableRef.rows.length;
	
	var row = document.getElementsByTagName('tbody')[0];
	var rowCount = row.rows.length;

		for(var a=0;a<rowCount;a++)
		{

			//tableRef.deleteRow(1);
			row.deleteRow(0);
		}
    
}


function myDeleteFunctionhead() {
	var tableRef = document.getElementById('resultado');
	//var rowCount = tableRef.rows.length;
	
	var row = tableRef.getElementsByTagName('thead')[0];
	var rowCount = row.rows.length;

		for(var a=0;a<rowCount;a++)
		{

			//tableRef.deleteRow(1);
			row.deleteRow(0);
		}
    
}

function doSearch()
		{
			//var tableReg = document.getElementById('resultado');
			var tableReg =  document.getElementById('resultado').getElementsByTagName('tbody')[0];

			var searchText = document.getElementById('txtbuscar').value.toLowerCase();
			var cellsOfRow="";
			var found=false;
			var compareWith="";
 
			// Recorremos todas las filas con contenido de la tabla
			for (var i = 0; i < tableReg.rows.length; i++)
			{
				cellsOfRow = tableReg.rows[i].getElementsByTagName('td');
				found = false;
				// Recorremos todas las celdas
				for (var j = 0; j < cellsOfRow.length && !found; j++)
				{
					compareWith = cellsOfRow[j].innerHTML.toLowerCase();
					// Buscamos el texto en el contenido de la celda
					if (searchText.length == 0 || (compareWith.indexOf(searchText) > -1))
					{
						found = true;
					}
				}
				if(found)
				{
					tableReg.rows[i].style.display = '';
				} else {
					// si no ha encontrado ninguna coincidencia, esconde la
					// fila de la tabla
					tableReg.rows[i].style.display = 'none';
				}
			}
		}



function limpiar(field){
field.value="";
}
function validatenumber(val){
	if(val.value.length<=0){
		val.value=0;
	}
}

function isNumber( input ) {
    return !isNaN( input );
}


function uploadFile(file,doc){
    var code=document.getElementById("txtcodigo").value;
    var url = 'uploadfiles.php';
    var xhr = new XMLHttpRequest();
    var fd = new FormData();
    xhr.open("POST", url, true);
    xhr.onreadystatechange = function() {
        if (xhr.readyState == 4 && xhr.status == 200) {
            // Every thing ok, file uploaded
            console.log(xhr.responseText); // handle response.
        }
   };
    fd.append("upload_file", file);
    fd.append("doc", doc);	
    fd.append("code", code);
    xhr.send(fd);
}

function disableEnable(elems, isDisabled){
    for(var i = elems.length-1;i>=0;i--){
        elems[i].disabled = isDisabled;
    }
}

function viewfile(ruta){
 window.open(ruta, '_blank');
}
function completecvesae(cve){
	while(cve.toString().length<8){
			cve='0'+cve;	
		}
return cve;
}
function dopercentaje(str){
	var cantidad=str.value;
	var aux,last;
	if(cod!=8){
		if(cantidad.length>1){
			aux=cantidad.split("%");
			last = aux[1].substr(aux[1].length - 1);
		
			if(isNumber(last) || last.localeCompare(".")==0){
			
				cantidad=aux[0]+last;		
				if((cantidad>=0 && cantidad<=100)|| cantidad.indexOf(".")<=0){
					str.value=cantidad+"%";
				}else{
					str.value=str.value.substring(0, str.value.length - 1);;
				}
			}else{
				cantidad = aux[0].substring(0, aux[0].length - 1); 
				str.value=cantidad+"%";
			}
		}else{
			if(isNumber(cantidad)){
				if(cantidad>=0 && cantidad<=100){
					str.value=cantidad+"%";
				}else{
					str.value="";
				}
			}else{
				str.value="";
			}
		}
	}
}

function todaydate() {
	var date=new Date();
  var yyyy = date.getFullYear().toString();
  var mm = (date.getMonth()+1).toString();
  var dd  = date.getDate().toString();

  var mmChars = mm.split('');
  var ddChars = dd.split('');

  return yyyy + '-' + (mmChars[1]?mm:"0"+mmChars[0]) + '-' + (ddChars[1]?dd:"0"+ddChars[0]);
}

function getdate(date) {
	//var date=new Date();
  var yyyy = date.getFullYear().toString();
  var mm = (date.getMonth()+1).toString();
  var dd  = date.getDate().toString();

  var mmChars = mm.split('');
  var ddChars = dd.split('');

 var hours = date.getHours() < 10 ? "0" + date.getHours() : date.getHours();
        var minutes = date.getMinutes() < 10 ? "0" + date.getMinutes() : date.getMinutes();
        var seconds = date.getSeconds() < 10 ? "0" + date.getSeconds() : date.getSeconds();
       var time = hours + ":" + minutes + ":" + seconds;

  return yyyy + '-' + (mmChars[1]?mm:"0"+mmChars[0]) + '-' + (ddChars[1]?dd:"0"+ddChars[0])+' '+time;
}

function addMinutes(date, minutes) {
    return new Date(date.getTime() + minutes*60000);
}