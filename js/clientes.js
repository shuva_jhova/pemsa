var str2;
var editmodulos;
var modulosdata;
var modulosdata2;
var inedition=false;
var isvendedor=localStorage.getItem("vendedor");
var iduser=localStorage.getItem("id");
var idclienteedit;
var idcliente;
var inedition2=false;
function showEdit(editableObj) {
			editableObj.style.backgroundColor="#e3bfc4";

		}
function edita(editableObj) {
			editableObj.style.backgroundColor="#FFCC99";
		}

function onerror(errObject)
{
		MSG(errObject["Message"] + ":\n" + errObject["Detail"]);
}


////////////////////////////////////////////FUNCTIONS PERFIL LOGISTICO/////////////////////////////////////////////////

function agregarContacto(str){
idcliente=str;
// Get the modal
var modal = document.getElementById('myModalcontacto');


// Get the <span> element that closes the modal
var span = document.getElementById("closecontacto");
  modal.style.display = "block";

// When the user clicks on <span> (x), close the modal
span.onclick = function() {
    modal.style.display = "none";
	}
getcontactos();

}

function addcontacto(){
	document.getElementById("contactos").style.display='block';
	document.getElementById("tablacontatos").style.display='none';

}
function canceladdcontacto(){
	document.getElementById("contactos").style.display='none';
	document.getElementById("tablacontatos").style.display='block';
	document.getElementById('txtnombre').value="";
	document.getElementById('txtappat').value="";
	document.getElementById('txtapmat').value="";
	document.getElementById('txttelefono').value="";
	document.getElementById('txtextension').value="";
	document.getElementById('txtcelular').value="";
	document.getElementById('txtcorreo').value="";
	document.getElementById('txtarea').value="";
	document.getElementById('txtpuesto').value="";
	getcontactos();

}
function savecontacto(){
	var nombre=document.getElementById('txtnombre').value;
	var appat=document.getElementById('txtappat').value;
	var apmat=document.getElementById('txtapmat').value;
	var telefono=document.getElementById('txttelefono').value;
	var extension=document.getElementById('txtextension').value;
	var celular=document.getElementById('txtcelular').value;
	var correo=document.getElementById('txtcorreo').value;
	var area=document.getElementById('txtarea').value;
	var puesto=document.getElementById('txtpuesto').value;


	if(nombre.length==0){
		MSG("El nombre del contacto no es valido.");
	}else if(appat.length==0 || apmat.length==0){
		MSG("Alguno de los apellidos es incorrecto.");
	}else if(telefono.length==0 && celular.length<10){
		MSG("Debes ingresar almenos un telef\u00E9no");
	}else if(correo.length==0){
		MSG("El correo del contacto no es valido");
	}else if(area.length==0){
		MSG("Se debe especificar el \u00E1rea del contacto");
	}else if(puesto.length==0){
		MSG("Se debe especificar el puesto del contacto");
	}else{
		REQUEST('appControlcotizador', 'savecontacto', [idcliente,nombre,appat,apmat,telefono,extension,celular,correo,area,puesto] , onsavecontacto, onerror);
	}
}
function onsavecontacto(dataResult){
	if(dataResult[0]['exito'].localeCompare("si")==0)
	{
		MSG("\u00C9xito al Guardar Datos("+dataResult[0]['folio']+")");
		canceladdcontacto();
	}
	else if(dataResult[0]['exito'].localeCompare("ya")==0)
	{
		MSG("Almenos un n\u00FAmero ya ha sido dado de alta con otro contacto");
	}
	else if(dataResult[0]['exito'].localeCompare("no")==0)
	{	
		MSG("Error al Guardar Datos. Intente nuevamente");
	}

}
function getcontactos(){

	REQUEST('appControlcotizador', 'getallcontactos', [idcliente] , ongetcontactos, onerror);
}

function ongetcontactos(dataResult){
	document.getElementById("imgload").style.display="none";
	myDeleteFunctioncontacto();
		if(dataResult.length>0){
			

		for (var i = 0; i<dataResult.length; i++)
		{		
			
			var tableRef = document.getElementById('resultadocontactos').getElementsByTagName('tbody')[0];;
			// Insert a row in the table at the last row
			var newRow   = tableRef.insertRow(tableRef.rows.length);
			newRow.className = "table-row";
			newRow.id="profile"+dataResult[i]["id"];
			// Insert a cell in the row at index 0
			var newCell  = newRow.insertCell(0);
			newCell.id="nombre"+dataResult[i]["id"];
			//newCell.contentEditable = "true";
			//newCell.addEventListener('click', showEdit.bind(null,newCell));
			// Append a text node to the cell
			var newText  = document.createTextNode(dataResult[i]["nombre"]);
			newCell.appendChild(newText);

			 newCell  = newRow.insertCell(1);
			newCell.id="telefono"+dataResult[i]["id"];
			//newCell.contentEditable = "true";	
			// Append a text node to the cell
			 newText  = document.createTextNode(dataResult[i]["telefono"]);
			newCell.appendChild(newText);


 			newCell  = newRow.insertCell(2);
			newCell.id="extension"+dataResult[i]["id"];
			//newCell.contentEditable = "true";
			newText = document.createTextNode(dataResult[i]["extension"]);
			newCell.appendChild(newText);
			
			newCell  = newRow.insertCell(3);
			newCell.id="celular"+dataResult[i]["id"];
			//newCell.contentEditable = "true";
			// Append a text node to the cell
			newText  = document.createTextNode(dataResult[i]["celular"]);
			newCell.appendChild(newText);

			newCell  = newRow.insertCell(4);
			newCell.id="correo"+dataResult[i]["id"];
			//newCell.contentEditable = "true";
			// Append a text node to the cell
			newText  = document.createTextNode(dataResult[i]["correo"]);
			newCell.appendChild(newText);


			newCell  = newRow.insertCell(5);
			newCell.id="area"+dataResult[i]["id"];
			//newCell.contentEditable = "true";
			// Append a text node to the cell
			newText  = document.createTextNode(dataResult[i]["area"]);
			newCell.appendChild(newText);

			newCell  = newRow.insertCell(6);
			newCell.id="puesto"+dataResult[i]["id"];
			//newCell.contentEditable = "true";
			// Append a text node to the cell
			newText  = document.createTextNode(dataResult[i]["puesto"]);
			newCell.appendChild(newText);
			

			newCell  = newRow.insertCell(7);
			var img = document.createElement('img');
   			img.src = "../img/editar.png";
			img.id="imgeditarprofile"+dataResult[i]["id"];
			img.addEventListener('click', editprofile.bind(null,dataResult[i]["id"]));
			newCell.appendChild(img);
			var img3 = document.createElement('img');
   			 img3.src = "../img/save.png";
			img3.id="imgsaveprofile"+dataResult[i]["id"];
			img3.style.display="none";
			img3.addEventListener('click', saveupdateprofile.bind(null,dataResult[i]["id"]));
			newCell.appendChild(img3);
			var img4 = document.createElement('img');
   			img4.src = "../img/cancel.png";
			img4.id="imgcancelprofile"+dataResult[i]["id"];
			img4.style.display="none";
			img4.addEventListener('click', cancelaedirprofile.bind(null,dataResult[i]["id"]));
			newCell.appendChild(img4);

			newCell  = newRow.insertCell(8);
			var img2 = document.createElement('img');
   			img2.src = "../img/delete.png";
			img2.addEventListener('click', deleterecord.bind(null,dataResult[i]["id"]));
			newCell.appendChild(img2);

			//newRow.addEventListener('click', navigateToController.bind(null, dataResult[i]["usuario"]));
		}
		
		document.getElementById("sincontactos").style.display="none";
		document.getElementById("tablacontactos").style.display="block";
		document.getElementById("tablacontatos").style.display="block";
	
	}else{
		document.getElementById("sincontactos").style.display="block";
		document.getElementById("tablacontactos").style.display="none";
		document.getElementById("tablacontatos").style.display="block";
	
	}
}
function cancelaedirprofile(str){
	var a=confirm("No se guardar\u00E1 ning\u00FAn dato \u00BFSeguro que desea abandonar la modificaci\u00F3n?");
			if(a){
				agregarContacto(idcliente);	
			}
}

function editprofile(str){

	//idclienteedit=str;
	if(inedition2){
		return;
	}
	inedition2=true;

	var img1=document.getElementById('imgeditarprofile'+str);
	var img2=document.getElementById('imgsaveprofile'+str);
	var img3=document.getElementById('imgcancelprofile'+str);

	img1.style.display="none";
	img2.style.display="block";
	img3.style.display="block";

	//str2=str;
	var row=document.getElementById("profile"+str);
	row.className = "table-row2";
	var cells = row.getElementsByTagName('td')
	for(var a=0;a<cells.length-2;a++){
		cells[a].style.backgroundColor="#9adbcf";
		cells[a].contentEditable = "false";	
	}
	
	
	var nombre =cells[0].innerText.split(" ");
	cells[0].innerText="";
    		
  	var x = document.createElement("INPUT");
    	x.setAttribute("type", "text");
	x.setAttribute("placeholder", "Nombre");
	x.setAttribute("value", nombre[0]);
    	x.id="txtnombreedit"+str;
	cells[0].appendChild(x);


  	var x = document.createElement("INPUT");
    	x.setAttribute("type", "text");
	x.setAttribute("placeholder", "Apellido Paterno");
	x.setAttribute("value", nombre[1]);
    	x.id="txtappatedit"+str;
	cells[0].appendChild(x);

  	var x = document.createElement("INPUT");
    	x.setAttribute("type", "text");
	x.setAttribute("placeholder", "Apellido Materno");
	x.setAttribute("value", nombre[2]);
    	x.id="txtapmatedit"+str;
	cells[0].appendChild(x);
	

	var telefono =cells[1].innerText;
	cells[1].innerText="";

	var x = document.createElement("INPUT");
    	x.setAttribute("type", "number");
	//x.setAttribute("placeholder", "Tel&eacute;fono");
	x.setAttribute("value", telefono);
	//x.setAttribute("onKeyDown", "if(this.value.length==10) return false;");
	x.setAttribute("style"," width: 90px");
    	x.id="txttelefonoedit"+str;
	cells[1].appendChild(x);

	var extension =cells[2].innerText;
	cells[2].innerText="";

	var x = document.createElement("INPUT");
    	x.setAttribute("type", "number");
	//x.setAttribute("placeholder", "Tel&eacute;fono");
	x.setAttribute("value", extension);
	//x.setAttribute("onKeyDown", "if(this.value.length==10) return false;");
	x.setAttribute("style"," width: 90px");
    	x.id="txtextensionedit"+str;
	cells[2].appendChild(x);

	var celular =cells[3].innerText;
	cells[3].innerText="";

	var x = document.createElement("INPUT");
    	x.setAttribute("type", "number");
	//x.setAttribute("placeholder", "Tel&eacute;fono");
	x.setAttribute("value", celular);
	//x.setAttribute("onKeyDown", "if(this.value.length==10) return false;");
	x.setAttribute("style"," width: 90px");
    	x.id="txtcelularedit"+str;
	cells[3].appendChild(x);


	var correo =cells[4].innerText;
	cells[4].innerText="";

	var x = document.createElement("INPUT");
    	x.setAttribute("type", "text");
	//x.setAttribute("placeholder", "Tel&eacute;fono");
	x.setAttribute("value", correo);
	//x.setAttribute("onKeyDown", "if(this.value.length==10) return false;");
    	x.id="txtcorreoedit"+str;
	cells[4].appendChild(x);

	var area =cells[5].innerText;
	cells[5].innerText="";

	var x = document.createElement("INPUT");
    	x.setAttribute("type", "text");
	//x.setAttribute("placeholder", "&aacute;rea");
	x.setAttribute("value", area);
	//x.setAttribute("onKeyDown", "if(this.value.length==10) return false;");
    	x.id="txtareaedit"+str;
	cells[5].appendChild(x);

	var puesto =cells[6].innerText;
	cells[6].innerText="";

	var x = document.createElement("INPUT");
    	x.setAttribute("type", "text");
	//x.setAttribute("placeholder", "Puesto");
	x.setAttribute("value", puesto);
	//x.setAttribute("onKeyDown", "if(this.value.length==10) return false;");
    	x.id="txtpuestoedit"+str;
	cells[6].appendChild(x);
}
function saveupdateprofile(str){
	var nombre=document.getElementById('txtnombreedit'+str).value;
	var appat=document.getElementById('txtappatedit'+str).value;
	var apmat=document.getElementById('txtapmatedit'+str).value;
	var telefono=document.getElementById('txttelefonoedit'+str).value;
	var extension=document.getElementById('txtextensionedit'+str).value;
	var celular=document.getElementById('txtcelularedit'+str).value;
	var correo=document.getElementById('txtcorreoedit'+str).value;
	var area=document.getElementById('txtareaedit'+str).value;
	var puesto=document.getElementById('txtpuestoedit'+str).value;


	if(nombre.length==0){
		MSG("El nombre del contacto no es valido.");
	}else if(appat.length==0 || apmat.length==0){
		MSG("Alguno de los apellidos es incorrecto.");
	}else if(telefono.length==0 && celular.length<10){
		MSG("Debes ingresar almenos un telef\u00E9no");
	}else if(correo.length==0){
		MSG("El correo del contacto no es valido");
	}else if(area.length==0){
		MSG("Se debe especificar el \u00E1rea del contacto");
	}else if(puesto.length==0){
		MSG("Se debe especificar el puesto del contacto");
	}else{
		REQUEST('appControlcotizador', 'updatecontacto', [str,nombre,appat,apmat,telefono,extension,celular,correo,area,puesto] , onUpdateprofile, onerror);
	}

}
function onUpdateprofile(dataResult)
{
	if(dataResult.length>0){
		inedition2=false;
		if(dataResult[0]['exito'].localeCompare("si")==0)
		{		
			MSG("\u00C9xito al modificar los datos");
			agregarContacto(idcliente);	
		}
		else if(dataResult[0]['exito'].localeCompare("sin")==0)
		{
			MSG("El contacto no ha sido encontrado");
		}
	}else
	{
		MSG("Error al modificar los datos. Intente nuevamente");
	}
    	
	
}
///////////////////////////////////////////////////FUNCTIONS CLIENTE///////////////////////////////////////////////
function updateCliente()
{

	var razon=document.getElementById('txtrazon2').value;
	var giro=document.getElementById('txtgiro2').value;
	var domicilio="";//document.getElementById('txtdomicilio2').value;
	var nombre="";//document.getElementById('txtnombre2').value;
	var appat="";//document.getElementById('txtappat2').value;
	var apmat="";//document.getElementById('txtapmat2').value;
	var telefono="";//document.getElementById('txttelefono2').value;
	var area="";//document.getElementById('txtarea2').value;
	var puesto="";//document.getElementById('txtpuesto2').value;
	var etiqueta=getval(document.getElementsByName("etiqueta2"));
	var plegadiza=getval(document.getElementsByName("plegadiza2"));//carton
	var singleface=getval(document.getElementsByName("singleface2"));//corrugado	
	var cajaplastico=getval(document.getElementsByName("cajaplastico2"));//caja
	var display=getval(document.getElementsByName("display2"));
	var tiraje=getval(document.getElementsByName("tiraje2"));
	var variable=getval(document.getElementsByName("variable2"));
	var web=getval(document.getElementsByName("web2"));//metalizado
	var ingenieria=getval(document.getElementsByName("ingenieria2"));
	var xmpie=getval(document.getElementsByName("xmpie2"));//web
	var ventas=getval(document.getElementsByName("ventas2"));
	var costosevidentes=getval(document.getElementsByName("costosevidentes2"));//costosdirectos
	var costosnoevidentes=getval(document.getElementsByName("costosnoevidentes2"));
	var participacion="";//getval(document.getElementsByName("participacion2"));
	var satisfaccion=getval(document.getElementsByName("satisfaccion2"));
	var exporta=getval(document.getElementsByName("exporta2"));
	var productos="";//document.getElementById('txtproductos2').value;
	var consumidor="";//document.getElementById('txtconsumidor2').value;
	var canal="";//document.getElementById('txtcanal2').value;
	var competencia=getval(document.getElementsByName('competencia2'));
	var vision="";//document.getElementById('txtvision2').value;
	var estimacionventa=document.getElementById('txtestimacionventa2').value;
	var nececidades="";//document.getElementById('txtnececidades2').value;
	var estrategia=document.getElementById('txtestrategia2').value;//problematica
	var proveedores="";//document.getElementById('txtproveedores2').value;
	var espectativas="";//document.getElementById('txtespectativas2').value;
	var activo=document.getElementById('optionactivo2').value;
	


	if(razon.length==0){
		MSG("La raz\u00F3n social es inv\u00E1lida");
	}/*else if(domicilio.length==0){
		MSG("El domicilio fiscal es inv\u00E1lido");
	} */else if(giro.length==0){
		MSG("El giro es inv\u00E1lido");
	}else if(estrategia.length==0) {
		MSG("La estrategia es inv\u00E1lida");
	}/*else if(telefono.length==0){
		MSG("El tel\u00E9fono de contacto es inv\u00E1lido");
	}*/else{
		
		REQUEST('appControlcotizador', 'updateclient', [idclienteedit,razon,giro,domicilio,nombre,appat,apmat,telefono,area,puesto,etiqueta,plegadiza,singleface,cajaplastico,display,tiraje,variable,web,ingenieria,xmpie,ventas,costosevidentes,costosnoevidentes,participacion,satisfaccion,productos,consumidor,canal,competencia,vision,estimacionventa,nececidades,estrategia,proveedores,espectativas,exporta,activo] , onsavecliente, onerror);
		
	}
}

function onUpdatecliente(dataResult)
{
	if(dataResult.length>0){
		inedition=false;
		if(dataResult[0]['exito'].localeCompare("si")==0)
		{		
			MSG("\u00C9xito al modificar los datos");
			var iframe = window.parent.document.getElementById("iframe");
			iframe.contentWindow.location.reload(true);
		}
		else if(dataResult[0]['exito'].localeCompare("sin")==0)
		{
			MSG("El usuario no ha sido encontrado");
		}
	}else
	{
		MSG("Error al modificar los datos. Intente nuevamente");
	}
    	
	
}


function agregar(){
// Get the modal
var modal = document.getElementById('myModal');

function MSG(str){

}
// Get the <span> element that closes the modal
var span = document.getElementById("closeclientee");
  modal.style.display = "block";

// When the user clicks on <span> (x), close the modal
span.onclick = function() {
    modal.style.display = "none";
	}

getgiros();
}

function getgiros(){
	REQUEST('appControlcotizador', 'getgiros', [], onGiros, onerror);
}
		
function onGiros(dataResult){
	var dataList = document.getElementById('girosss');
	dataList.innerHTML="";
	for (var i = 0; i<dataResult.length; i++)
	{
    		var opt = document.createElement('option');
    		opt.value = dataResult[i]['nombre'];
		dataList.appendChild(opt);
    	}
	if(isvendedor>0){
		//agregar();	
		document.getElementById("vendedor").style.display="none";
	}else{
		getVendedores()
		document.getElementById("vendedor").style.display="block";
	}
	
}
function getgiros2(){
	REQUEST('appControlcotizador', 'getgiros', [], onGiros2, onerror);
}
		
function onGiros2(dataResult){
	var dataList = document.getElementById('girosss');
	dataList.innerHTML="";
	for (var i = 0; i<dataResult.length; i++)
	{
    		var opt = document.createElement('option');
    		opt.value = dataResult[i]['nombre'];
		dataList.appendChild(opt);
    	}
	if(isvendedor>0){
	//	agregar();	
		document.getElementById("vendedor").style.display="none";
	}else{
		getVendedores()
		document.getElementById("vendedor").style.display="block";
	}
	
}

function getVendedores(){
	REQUEST('appControlcotizador', 'getvendedores', [], onVendedores, onerror);
}

function onVendedores(dataResult){
	var op= document.getElementById("optionvendedor");
   		
	op.innerHTML="";
	var option = document.createElement("option");
	    		option.value=0;
    			option.text ="---Seleccione---";
			
    			op.appendChild(option);
			
	if(dataResult.length>0){
		for(var a=0;a<dataResult.length;a++){
			option = document.createElement("option");
	    		option.value=dataResult[a]['id'];
    			option.text =dataResult[a]['nombre']+" "+dataResult[a]['appat']+" "+dataResult[a]['apmat'];
			
    			op.appendChild(option);
		}
		
	}
	op.selectedIndex = "0";

}
function savecliente(){
		

	var razon=document.getElementById('txtrazon').value;
	var giro=document.getElementById('txtgiro').value;
	var domicilio="";//document.getElementById('txtdomicilio').value;
	var nombre="";//document.getElementById('txtnombre').value;
	var appat="";//document.getElementById('txtappat').value;
	var apmat="";//document.getElementById('txtapmat').value;
	var telefono="";//document.getElementById('txttelefono').value;
	var area="";//document.getElementById('txtarea').value;
	var puesto="";//document.getElementById('txtpuesto').value;
	var etiqueta=getval(document.getElementsByName("etiqueta"));
	var plegadiza=getval(document.getElementsByName("plegadiza"));
	var singleface=getval(document.getElementsByName("singleface"));	
	var cajaplastico=getval(document.getElementsByName("cajaplastico"));
	var display=getval(document.getElementsByName("display"));
	var tiraje=getval(document.getElementsByName("tiraje"));
	var variable=getval(document.getElementsByName("variable"));
	var web=getval(document.getElementsByName("web"));
	var ingenieria=getval(document.getElementsByName("ingenieria"));
	var xmpie=getval(document.getElementsByName("xmpie"));
	var ventas=getval(document.getElementsByName("ventas"));
	var costosevidentes=getval(document.getElementsByName("costosevidentes"));
	var costosnoevidentes=getval(document.getElementsByName("costosnoevidentes"));
	var participacion="";//getval(document.getElementsByName("participacion"));
	var satisfaccion=getval(document.getElementsByName("satisfaccion"));
	var exporta=getval(document.getElementsByName("exporta"));
	var productos="";//document.getElementById('txtproductos').value;
	var consumidor="";//document.getElementById('txtconsumidor').value;
	var canal="";//document.getElementById('txtcanal').value;
	var competencia=getval(document.getElementsByName('competencia'));
	var vision="";//document.getElementById('txtvision').value;
	var estimacionventa=document.getElementById('txtestimacionventa').value;
	var nececidades="";//document.getElementById('txtnececidades').value;
	var estrategia=document.getElementById('txtestrategia').value;
	var proveedores="";//document.getElementById('txtproveedores').value;
	var espectativas="";//document.getElementById('txtespectativas').value;
	var idvendedor=document.getElementById('optionvendedor').value;
	


	if(razon.length==0){
		MSG("La raz\u00F3n social es inv\u00E1lida");
	}else if(estimacionventa<=0){
		MSG("La Estimaci\u00F3n de Venta debe ser Mayor a Cero ");
	}else if(giro.length==0){
		MSG("El giro es inv\u00E1lido");
	}else if(estrategia.length==0 ){
		MSG("La estrategia de venta es inv\u00E1lida");
	}/*else if(telefono.length==0){
		MSG("El tel\u00E9fono de contacto es inv\u00E1lido");
	}*/else if(isvendedor==0 && idvendedor==0){
		MSG("Seleccione el vendedor asociado");
	}else{
		if(isvendedor>0){
			REQUEST('appControlcotizador', 'saveclient', [iduser,razon,giro,domicilio,nombre,appat,apmat,telefono,area,puesto,etiqueta,plegadiza,singleface,cajaplastico,display,tiraje,variable,web,ingenieria,xmpie,ventas,costosevidentes,costosnoevidentes,participacion,satisfaccion,productos,consumidor,canal,competencia,vision,estimacionventa,nececidades,estrategia,proveedores,espectativas,exporta] , onsavecliente, onerror);
		}else{
			REQUEST('appControlcotizador', 'saveclient', [idvendedor,razon,giro,domicilio,nombre,appat,apmat,telefono,area,puesto,etiqueta,plegadiza,singleface,cajaplastico,display,tiraje,variable,web,ingenieria,xmpie,ventas,costosevidentes,costosnoevidentes,participacion,satisfaccion,productos,consumidor,canal,competencia,vision,estimacionventa,nececidades,estrategia,proveedores,espectativas,exporta] , onsavecliente, onerror);
		}
	}

}
function onsavecliente(dataResult){
	if(dataResult[0]['exito'].localeCompare("si")==0)
	{
		MSG("\u00C9xito al Guardar Datos("+dataResult[0]['folio']+")");
		var iframe = window.parent.document.getElementById("iframe");
		iframe.contentWindow.location.reload(true);
	}
	else if(dataResult[0]['exito'].localeCompare("ya")==0)
	{
		MSG("El usuario ya ha sido dado de alta");
	}
	else if(dataResult[0]['exito'].localeCompare("no")==0)
	{	
		MSG("Error al Guardar Datos. Intente nuevamente");
	}

}

function getclientes(){
	if(isvendedor!=4){
		document.getElementById('addnew').style.display="none";
	}else if(isvendedor==4){
		document.getElementById('addnew').style.display="block";
	}
	document.getElementById("imgload").style.display="block";
	if(isvendedor==4){
		REQUEST('appControlcotizador', 'getclientesbyvendedor', [iduser] , onClientes, onerror);
	}else{
		REQUEST('appControlcotizador', 'getclientes', [] , onClientes, onerror);
	}
}

function onClientes(dataResult){
	inedition=false;
	document.getElementById("imgload").style.display="none";
	myDeleteFunction() ;
		if(dataResult.length>0){
			

		for (var i = 0; i<dataResult.length; i++)
		{		
			
			var tableRef = document.getElementById('resultado').getElementsByTagName('tbody')[0];;
			// Insert a row in the table at the last row
			var newRow   = tableRef.insertRow(tableRef.rows.length);
			newRow.className = "table-row";
			newRow.id=dataResult[i]["id"];
			// Insert a cell in the row at index 0

			var newCell  = newRow.insertCell(0);
			newCell.id="nombre"+dataResult[i]["id"];
			//newCell.contentEditable = "true";
			//newCell.addEventListener('click', showEdit.bind(null,newCell));
			// Append a text node to the cell
			var newText  = document.createTextNode(dataResult[i]["id"]);
			newCell.appendChild(newText);


			
			var newCell  = newRow.insertCell(1);
			newCell.id="nombre"+dataResult[i]["id"];
			//newCell.contentEditable = "true";
			//newCell.addEventListener('click', showEdit.bind(null,newCell));
			// Append a text node to the cell
			var newText  = document.createTextNode(dataResult[i]["razonsocial"]);
			newCell.appendChild(newText);


			/*newCell  = newRow.insertCell(2);
			newCell.id="domicilio"+dataResult[i]["id"];
			//newCell.contentEditable = "true";	
			// Append a text node to the cell
			 newText  = document.createTextNode(dataResult[i]["domicilio"]);
			newCell.appendChild(newText);*/


 			/*newCell  = newRow.insertCell(2);
			newCell.id="contacto"+dataResult[i]["id"];
			//newCell.contentEditable = "true";
			newText = document.createTextNode(dataResult[i]["contacto"]);
			newCell.appendChild(newText);
			
			newCell  = newRow.insertCell(3);
			newCell.id="telefono"+dataResult[i]["id"];
			//newCell.contentEditable = "true";
			// Append a text node to the cell
			newText  = document.createTextNode(dataResult[i]["telefono"]);
			newCell.appendChild(newText);*/

			newCell  = newRow.insertCell(2);
			newCell.id="vendedor"+dataResult[i]["id"];
			//newCell.contentEditable = "true";
			// Append a text node to the cell
			newText  = document.createTextNode(dataResult[i]["vendedor"]);
			newCell.appendChild(newText);



			newCell  = newRow.insertCell(3);
			newCell.id="activo"+dataResult[i]["id"];
			//newCell.contentEditable = "true";
			// Append a text node to the cell
			var activo="";
			if(dataResult[i]["activo"]>0){
				activo="Si";
			}else{
				activo="No";
			}
			newText  = document.createTextNode(activo);
			newCell.appendChild(newText);


			/*newCell  = newRow.insertCell(5);
			var img21 = document.createElement('img');
   			img21.src = "../img/group.png";
			img21.addEventListener('click', agregarContacto.bind(null,dataResult[i]["id"]));
			newCell.appendChild(img21);*/



			newCell  = newRow.insertCell(4);
			var img = document.createElement('img');
   			img.src = "../img/editar.png";
			img.id="imgeditar"+dataResult[i]["id"];
			img.addEventListener('click', edit.bind(null,dataResult[i]["id"]));
			newCell.appendChild(img);
			var img3 = document.createElement('img');
   			 img3.src = "../img/save.png";
			img3.id="imgsave"+dataResult[i]["id"];
			img3.style.display="none";
			//img3.addEventListener('click', saverecord.bind(null,dataResult[i]["id"]));
			newCell.appendChild(img3);
			var img4 = document.createElement('img');
   			img4.src = "../img/cancel.png";
			img4.id="imgcancel"+dataResult[i]["id"];
			img4.style.display="none";
			//img4.addEventListener('click', cancela.bind(null,dataResult[i]["id"]));
			newCell.appendChild(img4);

			newCell  = newRow.insertCell(5);
			var img2 = document.createElement('img');
   			img2.src = "../img/delete.png";
			img2.addEventListener('click', deleterecord.bind(null,dataResult[i]["id"]));
			newCell.appendChild(img2);

			//newRow.addEventListener('click', navigateToController.bind(null, dataResult[i]["usuario"]));
		}
		
		document.getElementById("sin").style.display="none";
		document.getElementById("tabla").style.display="block";
		document.getElementById("sin2").style.display="block";
		
		pagination('#resultado');
	}else{
		document.getElementById("sin").style.display="block";
		document.getElementById("tabla").style.display="none";
		document.getElementById("sin2").style.display="none";
	
	}	

}

function pagination(id){
		$(document).ready(function() {
   			 $(id).DataTable( {
				"destroy":true,
      				 "scrollY": 500,
       				 "scrollX": true
    			} );
		} );
}
function cancela(str){

	var si=confirm("Aunque los cambios hallan sido realizados no se guardaran\n \u00BFSeguro que desea cancelar la modificaci\u00F3n sin guardar?");
	if(si){ 
		/*var row=document.getElementById(str);
		row.className = "table-row";
		var cells = row.getElementsByTagName('td')
		for(var a=0;a<cells.length-2;a++){
				cells[a].contentEditable = "false";
				cells[a].style.backgroundColor="mediumseagreen";
				cells[a].addEventListener('focus', null);
		}

		var activo=document.getElementById('optionactivo'+str);
		//cells[3].innerHTML=sexo;
		cells[4].innerText=activo.options[activo.selectedIndex].text;;
	
		var img1=document.getElementById('imgeditar'+str);
		var img2=document.getElementById('imgsave'+str);
		var img3=document.getElementById('imgcancel'+str);
		img1.style.display="block";
		img2.style.display="none";
		img3.style.display="none";*/
		var iframe = window.parent.document.getElementById("iframe");
		iframe.contentWindow.location.reload(true);
	}
}

function update(){
// Get the modal
var modal = document.getElementById('editclient');


// Get the <span> element that closes the modal
var span = document.getElementById("closedit");
  modal.style.display = "block";

// When the user clicks on <span> (x), close the modal
span.onclick = function() {
    modal.style.display = "none";
	}

getgiros2();
}

function edit(str){
	idclienteedit=str;
	//if(inedition){
	//	return;
	//}
	//inedition=true;
	str2=str;
	var row=document.getElementById(str);
	//row.className = "table-row2";
	//var cells = row.getElementsByTagName('td')
	//for(var a=0;a<cells.length-3;a++){
	//	cells[a].style.backgroundColor="#9adbcf";
	//	cells[a].contentEditable = "false";	
	//}
	REQUEST('appControlcotizador', 'getspecificclient', [str] , onspecific, onerror);
	
}

function onspecific(dataResult){
	document.getElementById('txtrazon2').value=dataResult[0]['razonsocial'];
	document.getElementById('txtgiro2').value=dataResult[0]['giro'];
	document.getElementById('txtdomicilio2').value=dataResult[0]['domicilio'];
	//document.getElementById('txtnombre2').value=dataResult[0]['nombre'];
	//document.getElementById('txtappat2').value=dataResult[0]['appat'];
	//document.getElementById('txtapmat2').value=dataResult[0]['apmat'];
	//document.getElementById('txttelefono2').value=dataResult[0]['telefono'];
	//document.getElementById('txtarea2').value=dataResult[0]['area'];
	//document.getElementById('txtpuesto2').value=dataResult[0]['puesto'];
	setval(document.getElementsByName("etiqueta2"),dataResult[0]['etiqueta']);
	setval(document.getElementsByName("plegadiza2"),dataResult[0]['carton']);
	setval(document.getElementsByName("singleface2"),dataResult[0]['corrugado']);	
	setval(document.getElementsByName("cajaplastico2"),dataResult[0]['caja']);
	setval(document.getElementsByName("display2"),dataResult[0]['display']);
	setval(document.getElementsByName("tiraje2"),dataResult[0]['tiraje']);
	setval(document.getElementsByName("variable2"),dataResult[0]['datovariable']);
	setval(document.getElementsByName("web2"),dataResult[0]['metalizado']);
	setval(document.getElementsByName("ingenieria2"),dataResult[0]['diseno']);
	setval(document.getElementsByName("xmpie2"),dataResult[0]['web']);
	setval(document.getElementsByName("ventas2"),dataResult[0]['ventas']);
	setval(document.getElementsByName("competencia2"),dataResult[0]['competencia']);
	setval(document.getElementsByName("costosevidentes2"),dataResult[0]['costosdirectos']);
	setval(document.getElementsByName("costosnoevidentes2"),dataResult[0]['costosnoevidentes']);
	setval(document.getElementsByName("participacion2"),dataResult[0]['participacion']);
	setval(document.getElementsByName("satisfaccion2"),dataResult[0]['satisfaccioncliente']);
	setval(document.getElementsByName("exporta2"),dataResult[0]['exporta']);
	document.getElementById('txtproductos2').value=dataResult[0]['productos'];
	document.getElementById('txtconsumidor2').value=dataResult[0]['consumidor'];
	document.getElementById('txtcanal2').value=dataResult[0]['canal'];
	document.getElementById('txtcompetencia2').value=dataResult[0]['competencia'];
	document.getElementById('txtvision2').value=dataResult[0]['vision'];
	document.getElementById('txtestimacionventa2').value=dataResult[0]['ventasestimacion'];
	document.getElementById('txtnececidades2').value=dataResult[0]['necesidades'];
	document.getElementById('txtestrategia2').value=dataResult[0]['problematica'];
	document.getElementById('txtproveedores2').value=dataResult[0]['proveedores'];
	document.getElementById('txtespectativas2').value=dataResult[0]['expectativas'];
	var op= document.getElementById("optionactivo2");
	op.innerHTML="";
	if(dataResult[0]['activo']==1){
		var option = document.createElement("option");
	    		option.value=1;
    			option.text ="Si";
    			op.appendChild(option);
		var option = document.createElement("option");
	    		option.value=0;
    			option.text ="No";
    			op.appendChild(option);
	}else if(dataResult[0]['activo']==0){
		var option = document.createElement("option");
	    		option.value=0;
    			option.text ="No";
    			op.appendChild(option);
		var option = document.createElement("option");
	    		option.value=1;
    			option.text ="Si";
    			op.appendChild(option);
	}
	update();
		
}




function deleterecord(str){
	var res=confirm("\u00BFSeguro que deseas eliminar este registro?");

	if(res){
		REQUEST('appControlcotizador', 'deletecliente', [str] , onusuariodelete, onerror);
	}
}

function onusuariodelete(dataResult){
	if(dataResult[0]['exito'].localeCompare("si")==0)
	{	
		MSG("\u00C9xito al eliminar los datos");
		var iframe = window.parent.document.getElementById("iframe");
			iframe.contentWindow.location.reload(true);
			
	}
	else if(dataResult[0]['exito'].localeCompare("sin")==0)
	{
		MSG("El cliente no ha sido dado encontrado");
	}else
	{
		MSG("Error al eliminar los datos. Intente nuevamente");
	}	
}


function myDeleteFunction() {
	var tableRef = document.getElementById('resultado');
	//var rowCount = tableRef.rows.length;
	
	var row = tableRef.getElementsByTagName('tbody')[0];
	var rowCount = row.rows.length;

		for(var a=0;a<rowCount;a++)
		{

			//tableRef.deleteRow(1);
			row.deleteRow(0);
		}
    
}
function myDeleteFunctioncontacto() {
	var tableRef = document.getElementById('resultadocontactos');
	//var rowCount = tableRef.rows.length;
	
	var row = tableRef.getElementsByTagName('tbody')[0];
	var rowCount = row.rows.length;

		for(var a=0;a<rowCount;a++)
		{

			//tableRef.deleteRow(1);
			row.deleteRow(0);
		}
    
}
function getval(grupo){
	var selec;

	for(var i = 0; i < grupo.length; i++) {
 	  if(grupo[i].checked)
    	   selec = grupo[i].value;
 	}
return selec;
}
function setval(grupo,val){
	
	if(val==0){
		grupo[1].checked=true;
		grupo[0].checked=false;
	}else if(val==1){
		grupo[0].checked=true;
		grupo[1].checked=false;
	}

}
function doSearch()
		{
			//var tableReg = document.getElementById('resultado');
			var tableReg = document.getElementsByTagName('tbody')[0];

			var searchText = document.getElementById('txtbuscar').value.toLowerCase();
			var cellsOfRow="";
			var found=false;
			var compareWith="";
 
			// Recorremos todas las filas con contenido de la tabla
			for (var i = 0; i < tableReg.rows.length; i++)
			{
				cellsOfRow = tableReg.rows[i].getElementsByTagName('td');
				found = false;
				// Recorremos todas las celdas
				for (var j = 0; j < cellsOfRow.length && !found; j++)
				{
					compareWith = cellsOfRow[j].innerHTML.toLowerCase();
					// Buscamos el texto en el contenido de la celda
					if (searchText.length == 0 || (compareWith.indexOf(searchText) > -1))
					{
						found = true;
					}
				}
				if(found)
				{
					tableReg.rows[i].style.display = '';
				} else {
					// si no ha encontrado ninguna coincidencia, esconde la
					// fila de la tabla
					tableReg.rows[i].style.display = 'none';
				}
			}
		}
function isNumber( input ) {
    return !isNaN( input );
}

function MSG(str){
	alert(str);
}
