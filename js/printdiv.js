var str2;
var idpaciente="";
function showEdit(editableObj) {
			editableObj.style.backgroundColor="#e3bfc4";

		}
function edita(editableObj) {
			editableObj.style.backgroundColor="#FFCC99";
		}

function onerror(errObject)
{
		alert(errObject["Message"] + ":\n" + errObject["Detail"]);
}

function getpacientes()
{
REQUEST('appControl', 'getallpacientes', [], onPaciente, onerror);
}
function onPaciente(dataResult)
{
	var dataList = document.getElementById('pacientes');
	
	for (var i = 0; i<dataResult.length; i++)
	{
	
    		var opt = document.createElement('option');
		opt.setAttribute("idpaciente",dataResult[i]['id']);
    		opt.value = dataResult[i]['nombre']+" "+dataResult[i]['appat']+" "+dataResult[i]['apmat'];
		dataList.appendChild(opt);
     
    	}


}


function getidpaciente(name){
var ban=false;
	var dataList = document.getElementById('pacientes');
	for (var i=0; i<dataList.options.length; i++) {
   		 if (dataList.options[i].value.localeCompare(name)==0) {
       			  idpaciente = dataList.options[i].getAttribute('idpaciente');
				ban=true;
       			 break;
    			}
	}
	if(!ban){
		ban=false;
		idpaciente=-1;
		//alert("El paciente no ha sido dado de alta");
	}
}
function porfecha(str){
var d2=str.split("-");
var days = ['lu', 'ma', 'mi', 'ju', 'vi', 'sa','do'];
var d = new Date(str);
var dayName = days[d.getDay()];

	var inicio2 = new Date(str);
	var hoy=new Date();
    if(d2[0]>=2017){
	if(inicio2>=hoy){
		if(dayName.localeCompare("do")==0){
			var select = document.getElementById("optionproceso");
			deleteofselect(select);

			var doc=document.getElementById("optiondoctor");	
			deleteofselect(doc);
			var option = document.createElement("option");
    			option.value="sindocs";
    			option.text = "--SIN DOCTORES EN LA FECHA SELECCIONADA--";
    			doc.appendChild(option);
			document.getElementById('txtduracion').value="";
			alert("Por el momento no se pueden agendar citas en dia domingo");

		}else{
			getdoctors(dayName);
			//alert(dayName);
		
		}
	}else{
		alert("La fecha de la cita no puede ser menor a la de hoy");
	}
   }
}

function getdoctors(day){
REQUEST('appControl', 'getalldoctorsbydate', [day] , onDoctor, onerror);
}
function onDoctor(dataResult){
	var doc=document.getElementById("optiondoctor");
		deleteofselect(doc);
	if(dataResult.length>0){
		for(var a=0;a<dataResult.length;a++){
			var option = document.createElement("option");
    			option.value=dataResult[a]['id'];
    		 	option.text = dataResult[a]['nombre']+' '+dataResult[a]['appat']+' '+dataResult[a]['apmat'];
    		 	doc.appendChild(option);
		}	
	}else{
		deleteofselect(doc);
		var option = document.createElement("option");
    		option.value="sindocs";
    		option.text = "--SIN DOCTORES REGISTRADOS--";
    		doc.appendChild(option);		
	}
filltable();
}


function getprocess(id){
	if(id.localeCompare("no")!=0){
	REQUEST('appControl', 'getallprocessbydoctor', [id] , onProceso, onerror);
	}else{
		var select = document.getElementById("optionproceso");
		deleteofselect(select);
	}
}

function onProceso(dataResult){
	
	var doc=document.getElementById("optionproceso");
		deleteofselect(doc);
	if(dataResult.length>0){
		for(var a=0;a<dataResult.length;a++){
			var option = document.createElement("option");
    			option.value=dataResult[a]['id'];
    		 	option.text = dataResult[a]['proceso'];
    		 	doc.appendChild(option);
		}	
	}else{
		var option = document.createElement("option");
    		option.value="sindocs";
    		option.text = "--SIN PROCESOS REGISTRADOS--";
    		doc.appendChild(option);		
	}
filltable();
}
function filltable(){
myDeleteFunction() ;
//document.addEventListener("click",handler,true);
var min2;
	for (var hr = 10; hr<=19; hr++)
	{
		for (var min = 0; min<60; min+=10)
		{	
			if(min==0){
				min2="00";
			}else{
				min2=min;
			}
			
			var tableRef = document.getElementById('resultado').getElementsByTagName('tbody')[0];;
			// Insert a row in the table at the last row
			var newRow   = tableRef.insertRow(tableRef.rows.length);
			newRow.className = "table-row";
			newRow.id="f"+hr+min2;
			// Insert a cell in the row at index 0
			var newCell  = newRow.insertCell(0);
			newCell.id="hora"+hr+min2;
			//newCell.contentEditable = "true";
			//newCell.addEventListener('click', showEdit.bind(null,newCell));
			// Append a text node to the cell
			var newText  = document.createTextNode(hr+":"+min2);
			newCell.appendChild(newText);

			 newCell  = newRow.insertCell(1);
			newCell.id="cita"+hr+min2;
			newCell.setAttribute("status","libre");
			//newCell.contentEditable = "true";	
			// Append a text node to the cell
			newCell.addEventListener('click', cita.bind(null,hr,min));
			 newText  = document.createTextNode("");
			newCell.appendChild(newText);


 			newCell  = newRow.insertCell(2);
			newCell.id="doctor"+hr+min2;
			//newCell.contentEditable = "true";
			newText = document.createTextNode("");
			newCell.appendChild(newText);
			
			newCell  = newRow.insertCell(3);
			newCell.id="status"+hr+min2;
			//newCell.contentEditable = "true";
			newText = document.createTextNode("");
			newCell.appendChild(newText);

			//newRow.addEventListener('click', navigateToController.bind(null, dataResult[i]["usuario"]));
			if(hr==19 && min==30){
				break;
			}
		}
	}
getallappointments();
}
function getallappointments(){
	var iddoctor=document.getElementById("optiondoctor").value;
	var fecha=document.getElementById("txtfecha").value;
	REQUEST('appControl', 'getallappointments', [iddoctor,fecha] , onAppointments, onerror);	
}
function onAppointments(dataResult){
var colorr="coral";

	for(var a=0;a<dataResult.length;a++){
		if(colorr.localeCompare("coral")==0) {
			colorr="PeachPuff";
		}else if(colorr.localeCompare("PeachPuff")==0) {
			colorr="coral";
		}
		var hrinicio=dataResult[a]['horainicio'].split(":");
		var doctor=dataResult[a]['doctor'];
		var paciente=dataResult[a]['paciente'];
		var proceso=dataResult[a]['proceso'];
		var duracion=dataResult[a]['duracion'];
		var estado=dataResult[a]['status'];
		var hr=hrinicio[0];
		var min=hrinicio[1];
		var dur=duracion.split(":");

		var durhr=parseInt(dur[0])*60;
		var durmin=parseInt(dur[1]); 	
		var mintotales=parseInt(durhr)+parseInt(durmin) ;

			var contmin=parseInt(min);
			
			var hr2=parseInt(hr);
			document.getElementById("cita"+hr+min).style.backgroundColor=colorr;
			document.getElementById("hora"+hr+min).style.backgroundColor=colorr;
			document.getElementById("cita"+hr+min).setAttribute("status","ocupada");
			document.getElementById("cita"+hr+min).setAttribute("idcita",dataResult[a]['id']);
			
			
			document.getElementById("doctor"+hr+min).style.backgroundColor=colorr;
			document.getElementById("status"+hr+min).style.backgroundColor=colorr;
			document.getElementById("cita"+hr+min).innerText="PACIENTE: "+paciente.toUpperCase()+"\nPROCESO: "+proceso;
			document.getElementById("doctor"+hr+min).innerText=doctor.toUpperCase();
			document.getElementById("status"+hr+min).innerText=estado.toUpperCase();
			
			for(var b=0;b<mintotales;b+=10){
				contmin=parseInt(contmin)+10;
				if(hr2==19 && contmin>=30){
					break;
				}
				
				if(contmin==60){
					hr2+=1;
					contmin="00";
				}
				
				
			document.getElementById("cita"+hr2+contmin).style.backgroundColor=colorr;
			document.getElementById("hora"+hr2+contmin).style.backgroundColor=colorr;
			document.getElementById("doctor"+hr2+contmin).style.backgroundColor=colorr;
			document.getElementById("status"+hr2+contmin).style.backgroundColor=colorr;
			document.getElementById("cita"+hr2+contmin).setAttribute("idcita",dataResult[a]['id']);
			
			document.getElementById("cita"+hr2+contmin).innerText="PACIENTE: "+paciente.toUpperCase()+"\nPROCESO: "+proceso;
			document.getElementById("cita"+hr2+contmin).setAttribute("status","ocupada");
			document.getElementById("doctor"+hr2+contmin).innerText=doctor.toUpperCase();
			document.getElementById("status"+hr2+contmin).innerText=estado.toUpperCase();
								
			}
			

	}
}

function cita(hr,min){
	if(min==0)min="00";

	var estado=document.getElementById("cita"+hr+min).getAttribute("status");
	var fecha=document.getElementById("txtfecha").value;
	var doctor=document.getElementById("optiondoctor").value;
	var proceso=document.getElementById("optionproceso").value;
	var duracion=document.getElementById("txtduracion").value;
	var nocabe=false;
	if(estado.localeCompare("libre")==0){
		
		if(fecha.length==0){
			alert("La fecha de la cita no es valida");
		}else if(doctor.length==0 || doctor.localeCompare("no")==0 || doctor.localeCompare("sindocs")==0){
			alert("El doctor no es valido");
		}else if(proceso.length==0 || proceso.localeCompare("no")==0){
			alert("El proceso no es valido");
		}else if(idpaciente.length==0 || idpaciente<=0){
			alert("El paciente no es valido");
		}else 
		if(duracion.length==0 || duracion.localeCompare("00:00")==0){
			alert("La duracion del proceso no es valida");
		}else{
			var dur=duracion.split(":");
			var durhr=parseInt(dur[0])*60;
			var durmin=parseInt(dur[1]); 	
			var mintotales=parseInt(durhr)+parseInt(durmin) ;

			var contmin=parseInt(min);
			var hr2=parseInt(hr);
			//document.getElementById("cita"+hr+min).style.backgroundColor="coral";
				
			for(var a=0;a<=mintotales;a+=10){
				if(contmin==60){
					hr2+=1;
					contmin="00";
				}
				if(contmin==0){
					contmin="00";
				}
				var estado2=document.getElementById("cita"+hr2+contmin).getAttribute("status");
				
				if(!estado2.localeCompare("libre")==0){
					nocabe=true;
					break;
				}
				if(hr2==19 && contmin==30 && a<=mintotales){
					break;
				}
				contmin=parseInt(contmin)+10;

			}
			if(nocabe){
				nocabe=false;
				alert("La cita no puede ser agendada porque no hay espacio suficiente");	
			}else{
				nocabe=false;
				validatecita(hr,min,hr2,contmin);
			}
		}
	}else if(estado.localeCompare("ocupada")==0){
		var idcita=document.getElementById("cita"+hr+min).getAttribute("idcita");
		getappointment(idcita);		
		var modal = document.getElementById('myModal2');
		document.getElementById("txtfecharecita").value="";
		document.getElementById("tabla").style.display="none";	
		var span = document.getElementById("re");
 	 	modal.style.display = "block";
		span.onclick = function() {
   			 modal.style.display = "none";
		}

	}
	
}
function handler(e){
    if(e.target.className!=="tbl-qa")
    e.stopPropagation()
}
function validatecita(hora,minuto,hrfin,minfin){
	var minfin2=parseInt(minfin);
	if(!minfin2==0){
		minfin2=parseInt(minfin2)-10;
	}
	var iddoctor=document.getElementById("optiondoctor").value;
	var fecha=document.getElementById("txtfecha").value;
	var proceso=document.getElementById("optionproceso").value;
	var duracion=document.getElementById("txtduracion").value;
	
	REQUEST('appControl', 'validatecita', [iddoctor,idpaciente,fecha,parseInt(hora),parseInt(minuto),proceso,hrfin+":"+minfin2,duracion] , onValidate, onerror);	
	
}

function onValidate(dataResult){
	if(dataResult.length>0){
		if(dataResult[0]['exito'].localeCompare('no')==0){
			alert("La cita no puede ser agendada porque ya existe una cita en esta fecha");
		}else if(dataResult[0]['exito'].localeCompare('ausencia')==0){
			alert("La cita no puede ser agendada porque el doctor no estara en esta fecha");
		}else{
				confirmacita(dataResult[0]['id'],dataResult[0]['horainicio']);	
		}
		
	}else{
		alert("Se ha producido un error mientras se validaba la cita. Intenente nuevamente");	
	}
}
function agendar(){
//document.location.href = 'inicio.php';
//window.top.location.reload(true);
var iframe = window.parent.document.getElementById("iframe");
iframe.src = "https://edmx.mx/dentx/php/citas.php";

setTimeout(printDiv(),2000);
}

function cancelarcita(){
	var folio=document.getElementById("tab-folio").innerText;
	REQUEST('appControl', 'deletecita', [folio] , onDeletecita, onerror);	
	
}
function onDeletecita(dataResult){
	if(dataResult.length>0){
		var modal = document.getElementById('myModal');
		modal.style.display = "none";
	}else{
		cancelarcita();
	}
}
function printDiv() {

	var folio =document.getElementById("tab-folio").innerText;
	var doctor=document.getElementById("tab-doctor").innerText;
	var proceso=document.getElementById("tab-proceso").innerText;
	var duracion=document.getElementById("tab-duracion").innerText;
	var paciente=document.getElementById("tab-paciente").innerText;
	var fecha=document.getElementById("tab-fecha").innerText;
	var hora=document.getElementById("tab-hora").innerText;	

	    var printWindow = window.open("", "print", "'height=200,width=400'");
        //open the window
        //printWindow.document.open();
        //write the html to the new window, link to css file
        printWindow.document.write('<html><head><title>' + 'ticket' + '</title><link rel="stylesheet" type="text/css" href="../css/print.css"></head><body>');
         printWindow.document.write('<div class="principal">');
	printWindow.document.write('<input type="Button" id="btnimpri" value="imprimir" class="no-print" onclick="javascript:window.print();" />'+'<br/>'+'<center>CLINICA DENTX</center>'+'<br/>'+'<br/>');
	printWindow.document.write('<img src="../img/logo.jpg" align="middle" width="100" height="100">'+'<br/>'+'<br/>');
	printWindow.document.write('<center>DETALLE DE LA CITA AGENDADA</center>'+'<br/>'+'<br/>');
	printWindow.document.write('</br>FOLIO: </br>'+folio+'<br/>');
	printWindow.document.write('</br>PACIENTE: </br>'+paciente.toUpperCase()+'<br/>');
	printWindow.document.write('</br>FECHA DE LA CITA: </br>'+fecha+'<br/>');
	printWindow.document.write('</br>HORA DE LA CITA: </br>'+hora+'<br/>');
	printWindow.document.write('</br>MOTIVO DE LA CITA:</br> '+proceso.toUpperCase()+'<br/>');
	printWindow.document.write('</br>TIEMPO APROXIMADO DE LA CITA: </br>'+duracion+'<br/>');
	printWindow.document.write('</br>DOCTOR: </br>'+doctor.toUpperCase()+'<br/>');
	printWindow.document.write('</div>');
	printWindow.document.write();
	
	printWindow.document.write('</body></html>');
       	printWindow.document.close();
     setTimeout(function(){printWindow.print();console.log("print");}, 1000);
setTimeout(function(){printWindow.close();console.log("close");}, 6000);
   
printWindow.focus()
	//window.print();
//    printWindow.window.print();
   // printWindow.close();
    
   //window.location.href='inicio.php';
}

function confirmacita(folio,hrinicio){

	var doctor=document.getElementById("optiondoctor").options[document.getElementById('optiondoctor').selectedIndex].text;;
	var proceso=document.getElementById("optionproceso").options[document.getElementById('optionproceso').selectedIndex].text;;
	var duracion=document.getElementById("txtduracion").value;
	var paciente=document.getElementById("txtpaciente").value;
	var fecha=document.getElementById("txtfecha").value;
	document.getElementById("tab-folio").innerText=folio;
	document.getElementById("tab-doctor").innerText=doctor.toUpperCase();
	document.getElementById("tab-proceso").innerText=proceso.toUpperCase();
	document.getElementById("tab-duracion").innerText=duracion;
	document.getElementById("tab-paciente").innerText=paciente.toUpperCase();
	document.getElementById("tab-fecha").innerText=fecha;
	document.getElementById("tab-hora").innerText=hrinicio;

	// Get the modal
	var modal = document.getElementById('myModal');


	// Get the <span> element that closes the modal
	//var span = document.getElementsByClassName("close")[0];
 	 modal.style.display = "block";

	// When the user clicks on <span> (x), close the modal
//	span.onclick = function() {
  // 		 modal.style.display = "none";
//	}

}
function getprocess2(id){
	//document.removeEventListener("click",handler,true);

	if(id.localeCompare("no")!=0){
	REQUEST('appControl', 'getspecifficprocess', [id] , onProceso2, onerror);
	}else{
		document.getElementById('txtduracion').value="";
	}
}


function onProceso2(dataResult){
	
	var duracion=document.getElementById('txtduracion');
	if(dataResult.length>0){
		
    			duracion.value=dataResult[0]['tiempo'];    		 	
			
	}else{
			
	}

}
function myDeleteFunction() {
	var tableRef = document.getElementById('resultado');
	//var rowCount = tableRef.rows.length;
	
	var row = document.getElementsByTagName('tbody')[0];
	var rowCount = row.rows.length;

		for(var a=0;a<rowCount;a++)
		{

			//tableRef.deleteRow(1);
			row.deleteRow(0);
		}
    
}
function deleteofselect(select){
		var length = select.options.length;
		while(length>1){
		for (i = 1; i <=length; i++) {
 			 select.options[i] = null;
		}
		length = select.options.length;
		}
}
function isNumber( input ) {
    return !isNaN( input );
}
