////////////////////////////////////////////////////GLOBAL VARIABLES/////////////////////////////////////////////////////////////////////////////
var str2;
var perfil=localStorage.getItem("perfil");
var isoperator=localStorage.getItem("operador");
var iduser=localStorage.getItem("id");
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

function getMaquinas(){
	REQUEST('appControl', 'getallmaquinas', [] , onMaquinas, onerror);
}

function onMaquinas(dataResult){

	var op= document.getElementById("optionmaquinas");
   		
	op.innerHTML="";
	var option = document.createElement("option");
	    		option.value=0;
    			option.text ="---Seleccione---";
			
    			op.appendChild(option);
			option = document.createElement("option");
	    		option.value='todas';
    			option.text ="Todas";
			
    			op.appendChild(option);
	if(dataResult.length>0){
		for(var a=0;a<dataResult.length;a++){
			option = document.createElement("option");
	    		option.value=dataResult[a]['id'];
    			option.text =dataResult[a]['nombre'];
			
    			op.appendChild(option);
		}
		
	}
	op.selectedIndex = "0";

}

/////////////////////////////////////////////////////GET PLANNING//////////////////////////////////////////////

function getplaneacion(){
		
	
	var fechainicio=document.getElementById("txtfechainicial").value;
	var fechafin=document.getElementById("txtfechafinal").value;
	var maquina=document.getElementById("optionmaquinas").value;

	if(maquina<=0){
		alert("Seleccione una m\u00E1quina valida");
	}else if(fechainicio.length==0 || fechafin.length==0){
		alert("Las fechas no pueden estar vacias");
	}else if(maquina.localeCompare("todas")==0){
		document.getElementById("imgload").style.display="block";
		REQUEST('appControl', 'porcentofutilityallmachine', [fechainicio,fechafin] , onPlaneacion, onerror);
	}else{
		document.getElementById("imgload").style.display="block";
		REQUEST('appControl', 'porcentofutility', [maquina,fechainicio,fechafin] , onPlaneacion, onerror);
	}
}

function onPlaneacion(dataResult){
	document.getElementById("imgload").style.display="none";
	//myDeleteFunctionhead();
	myDeleteFunction();
	var data = [];
	var estandarturno;
	var porcentaje;
	var estandarperiodo;
	
	
		if(dataResult.length>0){
			for(var a=0;a<dataResult.length;a++){
					estandarturno=dataResult[a]['estandar']*8;
					estandarperiodo=(parseFloat(estandarturno)*3)*dataResult[a]['dias'];
					porcentaje=roudnumber((dataResult[a]['suma']/estandarperiodo)*100);
					var tableRef = document.getElementById('resultado').getElementsByTagName('tbody')[0];
					// Insert a row in the table at the last row
					var newRow   = tableRef.insertRow(tableRef.rows.length);
					newRow.className = "table-row";
					newRow.id=dataResult[a]['oroden'];
					var newText  = document.createTextNode(dataResult[a]['maquina']);
					var newCell  = newRow.insertCell(0);
					newCell.appendChild(newText);

					
					var newCell  = newRow.insertCell(1);
					var newText  = document.createTextNode(dataResult[a]['estandar']+' '+dataResult[a]['unidadmedida']);
					newCell.appendChild(newText);

					var newCell  = newRow.insertCell(2);
					var newText  = document.createTextNode(parseFloat(dataResult[a]['estandar'])*24+' '+dataResult[a]['unidadsalida']+'(s)');
					newCell.appendChild(newText);
					
					var newCell  = newRow.insertCell(3);
					var newText  = document.createTextNode(dataResult[a]['dias']);
					newCell.appendChild(newText);
					var suma=0;
					if(dataResult[a]['suma']==null){
						suma=0;
		
					}else{
						suma=dataResult[a]['suma'];					
					}

					var newCell  = newRow.insertCell(4);
					var newText  = document.createTextNode(estandarperiodo+' '+dataResult[a]['unidadsalida']+'(s)');
					newCell.appendChild(newText);

					var newCell  = newRow.insertCell(5);
					var newText  = document.createTextNode(suma+' '+dataResult[a]['unidadsalida']+'(s)');
					newCell.appendChild(newText);

					var newCell  = newRow.insertCell(6);
					var newText  = document.createTextNode(converttopercentaje(porcentaje,false));
					newCell.appendChild(newText);

					/*var newCell  = newRow.insertCell(4);
					var newText  = document.createTextNode(dataResult[a]['fechaelaboracion']);
					newCell.appendChild(newText);

					var newCell  = newRow.insertCell(5);
					var newText  = document.createTextNode(dataResult[a]['fechaentrega']);
					newCell.appendChild(newText);

					var newCell  = newRow.insertCell(6);
					var newText  = document.createTextNode(dataResult[a]['cantidadordenada']);
					newCell.appendChild(newText);

					var newCell  = newRow.insertCell(7);
					var newText  = document.createTextNode(dataResult[a]['maquina']);
					newCell.appendChild(newText);

					var newCell  = newRow.insertCell(8);
					var newText  = document.createTextNode(dataResult[a]['piezasformato']);
					newCell.appendChild(newText);

					var newCell  = newRow.insertCell(9);
					var newText  = document.createTextNode(dataResult[a]['multiploproceso']);
					newCell.appendChild(newText);

					var newCell  = newRow.insertCell(10);
					var newText  = document.createTextNode(dataResult[a]['pliegos']);
					newCell.appendChild(newText);

					var newCell  = newRow.insertCell(11);
					var newText  = document.createTextNode(dataResult[a]['horas']);
					newCell.appendChild(newText);

					var newCell  = newRow.insertCell(12);
					var newText  = document.createTextNode(dataResult[a]['fechainicio']);
					newCell.appendChild(newText);

					var newCell  = newRow.insertCell(13);
					var newText  = document.createTextNode(dataResult[a]['horainicio']);
					newCell.appendChild(newText);

					var newCell  = newRow.insertCell(14);
					var newText  = document.createTextNode(dataResult[a]['fechafin']);
					newCell.appendChild(newText);

					var newCell  = newRow.insertCell(15);
					var newText  = document.createTextNode(dataResult[a]['horafin']);
					newCell.appendChild(newText);*/
			}
			imprimir("Reporte de maquinas");
			document.getElementById("sin").style.display="none";
			document.getElementById("tabla").style.display="block";
			document.getElementById("prin").style.display="block";
			//pagination('#resultado');
	
		}else{
			document.getElementById("sin").style.display="block";
			document.getElementById("tabla").style.display="none";
			document.getElementById("prin").style.display="none";
		}			

}
function pagination(id){
		$(document).ready(function() {
   			 $(id).DataTable( {
				"destroy":true,
      				 "scrollY": 500,
       				 "scrollX": true
    			} );
		} );
}
/////////////////////////////////////////////////////PRINT TABLE/////////////////////////////////////////////////////

function imprimir(reporte){
		
	// = TableExport(document.getElementById("resultado"));
	
	var table= TableExport(document.getElementById("resultado"), {
    		headers: true,                              // (Boolean), display table headers (th or td elements) in the <thead>, (default: true)
   		footers: true,                              // (Boolean), display table footers (th or td elements) in the <tfoot>, (default: false)
   		formats: ['xlsx'],            // (String[]), filetype(s) for the export, (default: ['xlsx', 'csv', 'txt'])
    		filename: reporte,                             // (id, String), filename for the downloaded file, (default: 'id')
    		bootstrap: true,                           // (Boolean), style buttons using bootstrap, (default: true)
    		exportButtons: true,                        // (Boolean), automatically generate the built-in export buttons for each of the specified formats (default: true)
    		position: 'bottom',                         // (top, bottom), position of the caption element relative to table, (default: 'bottom')
    		ignoreRows: null,                           // (Number, Number[]), row indices to exclude from the exported file(s) (default: null)
    		ignoreCols: null,                           // (Number, Number[]), column indices to exclude from the exported file(s) (default: null)
    		trimWhitespace: true                        // (Boolean), remove all leading/trailing newlines, spaces, and tabs from cell text in the exported file(s) (default: false)
	});
		table.reset();
}


////////////////////////////////////////////////////GLOBAL FUNCTIONS FOR DIFFERENT PROCESS/////////////////////////////////////////////////////////////////////////////
document.onkeydown = ShowKeyCode;
        function ShowKeyCode(evt) {
	//alert(localStorage.getItem("operador"));
            	if(evt.keyCode==8 ){
			
			cod=evt.keyCode;
		}else{
			cod=evt.keyCode;
		}
			
        }

function daysInMonth (month, year) {
    return new Date(year, month, 0).getDate();
}


function showEdit(editableObj) {
			editableObj.style.backgroundColor="#e3bfc4";

		}
function edita(editableObj) {
			editableObj.style.backgroundColor="#FFCC99";
		}

function onerror(errObject)
{
		alert(errObject["Message"] + ":\n" + errObject["Detail"]);
}


function getval(grupo){
	var selec;

	for(var i = 0; i < grupo.length; i++) {
 	  if(grupo[i].checked)
    	   selec = grupo[i].value;
 	}
return selec;
}



function myDeleteFunction() {
	var tableRef = document.getElementById('resultado');
	//var rowCount = tableRef.rows.length;
	
	var row = document.getElementsByTagName('tbody')[0];
	var rowCount = row.rows.length;

		for(var a=0;a<rowCount;a++)
		{

			//tableRef.deleteRow(1);
			row.deleteRow(0);
		}
    
}


function myDeleteFunctionhead() {
	var tableRef = document.getElementById('resultado');
	//var rowCount = tableRef.rows.length;
	
	var row = tableRef.getElementsByTagName('thead')[0];
	var rowCount = row.rows.length;

		for(var a=0;a<rowCount;a++)
		{

			//tableRef.deleteRow(1);
			row.deleteRow(0);
		}
    
}

function doSearch()
		{
			//var tableReg = document.getElementById('resultado');
			var tableReg =  document.getElementById('resultado').getElementsByTagName('tbody')[0];

			var searchText = document.getElementById('txtbuscar').value.toLowerCase();
			var cellsOfRow="";
			var found=false;
			var compareWith="";
 
			// Recorremos todas las filas con contenido de la tabla
			for (var i = 0; i < tableReg.rows.length; i++)
			{
				cellsOfRow = tableReg.rows[i].getElementsByTagName('td');
				found = false;
				// Recorremos todas las celdas
				for (var j = 0; j < cellsOfRow.length && !found; j++)
				{
					compareWith = cellsOfRow[j].innerHTML.toLowerCase();
					// Buscamos el texto en el contenido de la celda
					if (searchText.length == 0 || (compareWith.indexOf(searchText) > -1))
					{
						found = true;
					}
				}
				if(found)
				{
					tableReg.rows[i].style.display = '';
				} else {
					// si no ha encontrado ninguna coincidencia, esconde la
					// fila de la tabla
					tableReg.rows[i].style.display = 'none';
				}
			}
		}



function limpiar(field){
field.value="";
}
function validatenumber(val){
	if(val.value.length<=0){
		val.value=0;
	}
}

function isNumber( input ) {
    return !isNaN( input );
}


function uploadFile(file,doc){
    var code=document.getElementById("txtcodigo").value;
    var url = 'uploadfiles.php';
    var xhr = new XMLHttpRequest();
    var fd = new FormData();
    xhr.open("POST", url, true);
    xhr.onreadystatechange = function() {
        if (xhr.readyState == 4 && xhr.status == 200) {
            // Every thing ok, file uploaded
            console.log(xhr.responseText); // handle response.
        }
   };
    fd.append("upload_file", file);
    fd.append("doc", doc);	
    fd.append("code", code);
    xhr.send(fd);
}

function disableEnable(elems, isDisabled){
    for(var i = elems.length-1;i>=0;i--){
        elems[i].disabled = isDisabled;
    }
}

function print(){
var idmaquina=document.getElementById("optionmaquinas").value;
var fechafin=document.getElementById("txtfechafinal").value;
var fechainicio=document.getElementById("txtfechainicial").value;
 window.open("cargatrabajopdf.php?idm="+idmaquina+"&fecfi="+fechafin+"&fecini="+fechainicio, '_blank');
}
function completecvesae(cve){
	while(cve.toString().length<8){
			cve='0'+cve;	
		}
return cve;
}
function dopercentaje(str){
	var cantidad=str.value;
	var aux,last;
	if(cod!=8){
		if(cantidad.length>1){
			aux=cantidad.split("%");
			last = aux[1].substr(aux[1].length - 1);
		
			if(isNumber(last) || last.localeCompare(".")==0){
			
				cantidad=aux[0]+last;		
				if((cantidad>=0 && cantidad<=100)|| cantidad.indexOf(".")<=0){
					str.value=cantidad+"%";
				}else{
					str.value=str.value.substring(0, str.value.length - 1);;
				}
			}else{
				cantidad = aux[0].substring(0, aux[0].length - 1); 
				str.value=cantidad+"%";
			}
		}else{
			if(isNumber(cantidad)){
				if(cantidad>=0 && cantidad<=100){
					str.value=cantidad+"%";
				}else{
					str.value="";
				}
			}else{
				str.value="";
			}
		}
	}
}

function todaydate() {
	var date=new Date();
  var yyyy = date.getFullYear().toString();
  var mm = (date.getMonth()+1).toString();
  var dd  = date.getDate().toString();

  var mmChars = mm.split('');
  var ddChars = dd.split('');

  return yyyy + '-' + (mmChars[1]?mm:"0"+mmChars[0]) + '-' + (ddChars[1]?dd:"0"+ddChars[0]);
}

function getdate(date) {
	//var date=new Date();
  var yyyy = date.getFullYear().toString();
  var mm = (date.getMonth()+1).toString();
  var dd  = date.getDate().toString();

  var mmChars = mm.split('');
  var ddChars = dd.split('');

 var hours = date.getHours() < 10 ? "0" + date.getHours() : date.getHours();
        var minutes = date.getMinutes() < 10 ? "0" + date.getMinutes() : date.getMinutes();
        var seconds = date.getSeconds() < 10 ? "0" + date.getSeconds() : date.getSeconds();
       var time = hours + ":" + minutes + ":" + seconds;

  return yyyy + '-' + (mmChars[1]?mm:"0"+mmChars[0]) + '-' + (ddChars[1]?dd:"0"+ddChars[0])+' '+time;
}

function addMinutes(date, minutes) {
    return new Date(date.getTime() + minutes*60000);
}
function roudnumber(num){
	return Math.round(num * 100) / 100;
}
function converttopercentaje(num,multi){
	if(multi){
		num=parseFloat(num)*100+" %";
	} else{
		num=num+" %";
	}
return num;
}