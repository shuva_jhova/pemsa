////////////////////////////////////////////////////GLOBAL VARIABLES/////////////////////////////////////////////////////////////////////////////
var cod=0;
var idproducto;
var idprocesoeditar;
var idcomponenteeditar;
var iddelcomponente;
var str2;
var perfil=localStorage.getItem("perfil");
var status;
var operador;
var idorden;
var isoperator=localStorage.getItem("operador");
var iduser=localStorage.getItem("id");
var inedition=false;
var departamento;
var idincidente;
var strr=0;
var idordenforincident=0;
var dptodenuncia="";
var deptodenunciado="";
var idincidentedit="";
var modificacion;
////////////////////////////////////////////////////FUNCTIONS FOR ORDERS/////////////////////////////////////////////////////////////////////////////


		//////////////////////////////////////UPDATE ORDER////////////////////////////////////////////////
function saverecord()
{
	
	var id=document.getElementById("txtnumeroorden").value;
	var fecharequerida=document.getElementById("txtfecharequerida2").value;
	var cantidad=document.getElementById("txtcantidad2").value;
	var status=document.getElementById("optionstatus").value;


 	if(fecharequerida.length == 0 )
	{
		alert("La fecha requerida es inv\u00E1lida");
	}
	else if(cantidad.length <=0)
	{
		alert("La cantidad es inv\u00E1lida, debe ser mayor a cero");
	}
	else
	{
		
		REQUEST('appControl', 'updateorder', [id,fecharequerida,cantidad,status], onUpdateorder, onerror);
		
	}
}

function onUpdateorder(dataResult)
{
	if(dataResult.length>0){
		if(dataResult[0]['exito'].localeCompare("si")==0)
		{		
			alert("\u00C9xito al modificar los datos");
			var iframe = window.parent.document.getElementById("iframe");
			iframe.contentWindow.location.reload(true);
		}
		else if(dataResult[0]['exito'].localeCompare("sin")==0)
		{
			alert("El producto no ha sido encontrado");
		}
	}else
	{
		alert("Error al modificar los datos. Intente nuevamente");
	}
    	
	
}

		


		//////////////////////////////////////GET ALL ORDERS////////////////////////////////////////////////


function gettickets(){
	if(perfil>0){
		REQUEST('appControl', 'getticketsbymachine', [perfil] , onTickets, onerror);
	}else{
		//REQUEST('appControl', 'getallorders', [] , onOrders, onerror);
	}
}
function onTickets(dataResult)
{	
	document.getElementById("imgload").style.display="none";
	myDeleteFunction() ;
	if(dataResult.length>0){
		for (var i = 0; i<dataResult.length; i++)
		{
				var tableRef = document.getElementById('resultado').getElementsByTagName('tbody')[0];;
				// Insert a row in the table at the last row
				var newRow   = tableRef.insertRow(tableRef.rows.length);
				newRow.className = "table-row";
				

			if(perfil>0){
				
				newRow.id="proc"+dataResult[i]["id"];
				// Insert a cell in the row at index 0
				var newCell  = newRow.insertCell(0);
				newCell.id="ticket"+dataResult[i]["id"];
				var newText  = document.createTextNode(dataResult[i]["idorden"]);
				newCell.appendChild(newText);


				var newCell  = newRow.insertCell(1);
				newCell.id="folio"+dataResult[i]["id"];
				var newText  = document.createTextNode(dataResult[i]["folio"]);
				newCell.appendChild(newText);

				var newCell  = newRow.insertCell(2);
				newCell.id="fecha"+dataResult[i]["id"];
				var newText  = document.createTextNode(dataResult[i]["fecha"]);
				newCell.appendChild(newText);

				var newCell  = newRow.insertCell(3);
				newCell.id="hora"+dataResult[i]["id"];
				var newText  = document.createTextNode(dataResult[i]["hora"]);
				newCell.appendChild(newText);

				var newCell  = newRow.insertCell(4);
				newCell.id="cantidad"+dataResult[i]["id"];
				var newText  = document.createTextNode(dataResult[i]["cantidad"]);
				newCell.appendChild(newText);

				
				var newCell  = newRow.insertCell(5);
				newCell.id="cantidadutil"+dataResult[i]["id"];
				var newText  = document.createTextNode(dataResult[i]["cantidadutil"]);
				newCell.appendChild(newText);

				var newCell  = newRow.insertCell(6);
				newCell.id="merma"+dataResult[i]["id"];
				var newText  = document.createTextNode(dataResult[i]["merma"]);
				newCell.appendChild(newText);
	
				var newCell  = newRow.insertCell(7);
				newCell.id="operador"+dataResult[i]["id"];
				var newText  = document.createTextNode(dataResult[i]["operador"]);
				newCell.appendChild(newText);
			
				newCell  = newRow.insertCell(8);
				var img = document.createElement('img');
   				img.src = "../img/print.png";
				img.id="imgprint"+dataResult[i]["id"];
				if(dataResult[i]["fechainicio"].localeCompare("0000-00-00")==0){
					img.addEventListener('click', printticketno.bind(null,dataResult[i]["id"]));
				}else{
					img.addEventListener('click', printticket.bind(null,dataResult[i]["id"]));
				}
				newCell.appendChild(img);
				

				

			
				var img5 = document.createElement('img');
   				img5.src = "../img/editar.png";
				img5.id="imgeditar"+dataResult[i]["id"];
				img5.addEventListener('click', edit.bind(null,dataResult[i]["id"]));
				newCell.appendChild(img5)
			
				var img3 = document.createElement('img');
	   			img3.src = "../img/save.png";
				img3.id="imgsave"+dataResult[i]["id"];
				img3.style.display="none";
				img3.addEventListener('click', savedit.bind(null,dataResult[i]["id"],dataResult[i]["proceso"]));
				newCell.appendChild(img3);
				var img4 = document.createElement('img');
   				img4.src = "../img/cancel.png";
				img4.id="imgcancel"+dataResult[i]["id"];
				img4.style.display="none";
				img4.addEventListener('click', canceledit);
				newCell.appendChild(img4);
			
				

			}else{
				
				
				
			}


			
		}

		document.getElementById("sin").style.display="none";
		document.getElementById("tabla").style.display="block";
		document.getElementById("sin2").style.display="block";
		pagination('#resultado');
	
	}else{
		document.getElementById("sin").style.display="block";
		document.getElementById("tabla").style.display="none";
		document.getElementById("sin2").style.display="none";
	
	}	
}
function pagination(id){
		$(document).ready(function() {
   			 $(id).DataTable( {
				"destroy":true,
      				 "scrollY": 500,
       				 "scrollX": true
    			} );
		} );
}

		//////////////////////////////////////PRINT ORDER TO PDF///////////////////////////////////////


function printticket(str){
	window.open('ticketpdf.php?idpro='+str, '_blank');
}

function printticketno(str){
	alert("Este ticket a\u00FAn no ha sido iniciado");
}
		//////////////////////////////////////USER DENIED///////////////////////////////////////////////

function userdenied(str){
	alert("El usuario no tiene permisos de acceso a estas acciones. Por favor contacte a su administrador");
}
function finalizado(){
	alert("Este registro no se puede modificar, El ticket ya ha sido finalizado");
}
function canceledit(){
	if(confirm("No se guardar\u00E1 ning\u00FAn cambio. \u00BFSeguro de continuar?")){
		inedition=false;
		var iframe = window.parent.document.getElementById("iframe");
			iframe.contentWindow.location.reload(true);	
	}
}
		
		////////////////////////////////////FUNCTION FOR EDIT PROCESS BY ORDER/////////////////////////////////////////

function edit(str){
	idprocesoeditar=str;
	if(inedition){
		return;
	}
	inedition=true;
	str2=str;
	var row=document.getElementById("proc"+str);
	row.className = "table-row2";
	var cells = row.getElementsByTagName('td');

	for(var a=0;a<cells.length-1;a++){
		cells[a].style.backgroundColor="#9adbcf";
		//if(a==3 ||a==4 ){
		//	cells[a].contentEditable = "true";
		//	cells[a].addEventListener('focus', showEdit.bind(null,cells[a]));
		//}
	}
	
	
	var img1=document.getElementById('imgeditar'+str);
	var img2=document.getElementById('imgsave'+str);
	var img3=document.getElementById('imgcancel'+str);
	var img4=document.getElementById('imgprint'+str);
	img1.style.display="none";
	img4.style.display="none";
	img2.style.display="block";
	img3.style.display="block";

	
		operador=cells[7].innerText;
		
		
		getspecificprocess();
	
}



		////////////////////////////////////GET OPERATORS OF ALL SYSTEM/////////////////////////////////////////////////////////////
function getoperators(){
	REQUEST('appControl', 'getoperators', [] , onOperators, onerror);
}

function onOperators(dataResult){
	var selectList=document.getElementById("optionoperator"+str2);
	for(var a=0;a<dataResult.length;a++){
		var option = document.createElement("option");
    		option.value=dataResult[a]['id'];
    		option.text =dataResult[a]['operador'];
		if(operador.trim().localeCompare(dataResult[a]['operador'].trim())==0){
			option.selected=true;
		}
    		selectList.appendChild(option);
	}
}

		////////////////////////////////////GET SPECIFIC PROCESS/////////////////////////////////////////////////////////////
function getspecificprocess(){
	REQUEST('appControl', 'getspecificticketbymachine', [idprocesoeditar] , onSpecificprocess, onerror);
}

function onSpecificprocess(dataResult){
	var row=document.getElementById("proc"+idprocesoeditar);
	row.className = "table-row2";
	var cells = row.getElementsByTagName('td');

	if(dataResult.length>0){
		if(dataResult[0]['fechainicio'].localeCompare("0000-00-00")==0 && dataResult[0]['horainicio'].localeCompare("00:00:00")==0){
			modificacion=1;
			cells[7].innerText="";
			cells[7].contentEditable="false";
	
			var selectList = document.createElement("select");	
			selectList.id="optionoperator"+idprocesoeditar;
 	
 		
			cells[7].innerHTML="";
			cells[7].innerText="";
			cells[7].appendChild(selectList);
			getoperators();
				
		}else if(dataResult[0]['fechainicio'].localeCompare("0000-00-00")!=0 && dataResult[0]['horainicio'].localeCompare("00:00:00")!=0 && dataResult[0]['fechatermino'].localeCompare("0000-00-00")==0 && dataResult[0]['horatermino'].localeCompare("00:00:00")==0){
			modificacion=2;
			var util=cells[5].innerText;
			cells[5].innerText="";
			cells[5].contentEditable="false";
    		
  			var x = document.createElement("INPUT");
    			x.setAttribute("type", "number");
    			x.setAttribute("value", util);
			x.setAttribute("style"," width: 80px");
			x.id="canutil"+idprocesoeditar;
   			cells[5].appendChild(x);

			var merma=cells[6].innerText;
			cells[6].innerText="";
			cells[6].contentEditable="false";
			x = document.createElement("INPUT");
    			x.setAttribute("type", "number");
    			x.setAttribute("value", merma);
			x.setAttribute("style"," width: 80px" );
			x.id="canmerma"+idprocesoeditar;
   			cells[6].appendChild(x);	
		}
	}	

	
}

		
function savedit(str,proceso){
	if(confirm("Esta apunto de modificar estos datos, esta acci\u00F3n es irreversible. \u00BFSeguro que desea proceder?")){
		if(modificacion==1){
			var operador=document.getElementById("optionoperator"+str).value;
			REQUEST('appControl', 'openticket', [str,operador,proceso] , onUpdateprocess, onerror);
		}else if(modificacion==2){
			var cantidadutil=document.getElementById("canutil"+str).value;
			var merma=document.getElementById("canmerma"+str).value;
			REQUEST('appControl', 'closeticket', [str,cantidadutil,merma] , onUpdateprocess, onerror);
		}
	}
}
function  onUpdateprocess(dataResult){

	if(dataResult.length>0){
		if(dataResult[0]['exito'].localeCompare("si")==0)
		{	inedition=false;	
			alert("\u00C9xito al modificar los datos");
			var iframe = window.parent.document.getElementById("iframe");
			iframe.contentWindow.location.reload(true);
		}else if(dataResult[0]['exito'].localeCompare("abierto")==0)
		{
			alert("Existe un ticket en ejecuci\u00F3n asociado al operador o a la m\u00E1quina");
		}
		else if(dataResult[0]['exito'].localeCompare("sin")==0)
		{
			alert("Los datos no han sido encontrados");
		}
	}else
	{
		alert("Error al modificar los datos. Intente nuevamente");
	}
	
}

////////////////////////////////////////////////////GLOBAL FUNCTIONS FOR DIFFERENT PROCESS/////////////////////////////////////////////////////////////////////////////
document.onkeydown = ShowKeyCode;
        function ShowKeyCode(evt) {
	//alert(localStorage.getItem("operador"));
            	if(evt.keyCode==8 ){
			
			cod=evt.keyCode;
		}else{
			cod=evt.keyCode;
		}
			
        }




function showEdit(editableObj) {
			editableObj.style.backgroundColor="#e3bfc4";

		}
function edita(editableObj) {
			editableObj.style.backgroundColor="#FFCC99";
		}

function onerror(errObject)
{
		alert(errObject["Message"] + ":\n" + errObject["Detail"]);
}


function getval(grupo){
	var selec;

	for(var i = 0; i < grupo.length; i++) {
 	  if(grupo[i].checked)
    	   selec = grupo[i].value;
 	}
return selec;
}



function myDeleteFunction() {
	var tableRef = document.getElementById('resultado');
	//var rowCount = tableRef.rows.length;
	
	var row = document.getElementsByTagName('tbody')[0];
	var rowCount = row.rows.length;

		for(var a=0;a<rowCount;a++)
		{

			//tableRef.deleteRow(1);
			row.deleteRow(0);
		}
    
}

function myDeleteFunctioncomponentes() {
	var tableRef = document.getElementById('componentesorden');
	//var rowCount = tableRef.rows.length;
	
	var row = tableRef.getElementsByTagName('tbody')[0];
	var rowCount = row.rows.length;

		for(var a=0;a<rowCount;a++)
		{

			//tableRef.deleteRow(1);
			row.deleteRow(0);
		}
    
}
function myDeleteFunctionincidents() {
	var tableRef = document.getElementById('resultadoincidents');
	//var rowCount = tableRef.rows.length;
	
	var row = tableRef.getElementsByTagName('tbody')[0];
	var rowCount = row.rows.length;

		for(var a=0;a<rowCount;a++)
		{

			//tableRef.deleteRow(1);
			row.deleteRow(0);
		}
    
}
function myDeleteFunctionprocesos() {
	var tableRef = document.getElementById('procesosorden');
	//var rowCount = tableRef.rows.length;
	
	var row = tableRef.getElementsByTagName('tbody')[0];
	var rowCount = row.rows.length;

		for(var a=0;a<rowCount;a++)
		{

			//tableRef.deleteRow(1);
			row.deleteRow(0);
		}
    
}

function doSearch()
		{
			//var tableReg = document.getElementById('resultado');
			var tableReg =  document.getElementById('resultado').getElementsByTagName('tbody')[0];

			var searchText = document.getElementById('txtbuscar').value.toLowerCase();
			var cellsOfRow="";
			var found=false;
			var compareWith="";
 
			// Recorremos todas las filas con contenido de la tabla
			for (var i = 0; i < tableReg.rows.length; i++)
			{
				cellsOfRow = tableReg.rows[i].getElementsByTagName('td');
				found = false;
				// Recorremos todas las celdas
				for (var j = 0; j < cellsOfRow.length && !found; j++)
				{
					compareWith = cellsOfRow[j].innerHTML.toLowerCase();
					// Buscamos el texto en el contenido de la celda
					if (searchText.length == 0 || (compareWith.indexOf(searchText) > -1))
					{
						found = true;
					}
				}
				if(found)
				{
					tableReg.rows[i].style.display = '';
				} else {
					// si no ha encontrado ninguna coincidencia, esconde la
					// fila de la tabla
					tableReg.rows[i].style.display = 'none';
				}
			}
		}



function limpiar(field){
field.value="";
}
function validatenumber(val){
	if(val.value.length<=0){
		val.value=0;
	}
}

function isNumber( input ) {
    return !isNaN( input );
}


function uploadFile(file,doc){
    var code=document.getElementById("txtcodigo").value;
    var url = 'uploadfiles.php';
    var xhr = new XMLHttpRequest();
    var fd = new FormData();
    xhr.open("POST", url, true);
    xhr.onreadystatechange = function() {
        if (xhr.readyState == 4 && xhr.status == 200) {
            // Every thing ok, file uploaded
            console.log(xhr.responseText); // handle response.
        }
   };
    fd.append("upload_file", file);
    fd.append("doc", doc);	
    fd.append("code", code);
    xhr.send(fd);
}

function disableEnable(elems, isDisabled){
    for(var i = elems.length-1;i>=0;i--){
        elems[i].disabled = isDisabled;
    }
}

function viewfile(ruta){
 window.open(ruta, '_blank');
}
function completecvesae(cve){
	while(cve.toString().length<8){
			cve='0'+cve;	
		}
return cve;
}
function dopercentaje(str){
	var cantidad=str.value;
	var aux,last;
	if(cod!=8){
		if(cantidad.length>1){
			aux=cantidad.split("%");
			last = aux[1].substr(aux[1].length - 1);
		
			if(isNumber(last) || last.localeCompare(".")==0){
			
				cantidad=aux[0]+last;		
				if((cantidad>=0 && cantidad<=100)|| cantidad.indexOf(".")<=0){
					str.value=cantidad+"%";
				}else{
					str.value=str.value.substring(0, str.value.length - 1);;
				}
			}else{
				cantidad = aux[0].substring(0, aux[0].length - 1); 
				str.value=cantidad+"%";
			}
		}else{
			if(isNumber(cantidad)){
				if(cantidad>=0 && cantidad<=100){
					str.value=cantidad+"%";
				}else{
					str.value="";
				}
			}else{
				str.value="";
			}
		}
	}
}

function todaydate() {
	var date=new Date();
  var yyyy = date.getFullYear().toString();
  var mm = (date.getMonth()+1).toString();
  var dd  = date.getDate().toString();

  var mmChars = mm.split('');
  var ddChars = dd.split('');

  return yyyy + '-' + (mmChars[1]?mm:"0"+mmChars[0]) + '-' + (ddChars[1]?dd:"0"+ddChars[0]);
}

function getdate(date) {
	//var date=new Date();
  var yyyy = date.getFullYear().toString();
  var mm = (date.getMonth()+1).toString();
  var dd  = date.getDate().toString();

  var mmChars = mm.split('');
  var ddChars = dd.split('');

 var hours = date.getHours() < 10 ? "0" + date.getHours() : date.getHours();
        var minutes = date.getMinutes() < 10 ? "0" + date.getMinutes() : date.getMinutes();
        var seconds = date.getSeconds() < 10 ? "0" + date.getSeconds() : date.getSeconds();
       var time = hours + ":" + minutes + ":" + seconds;

  return yyyy + '-' + (mmChars[1]?mm:"0"+mmChars[0]) + '-' + (ddChars[1]?dd:"0"+ddChars[0])+' '+time;
}

function addMinutes(date, minutes) {
	var m=parseFloat(minutes)*60000;
    return new Date(date.getTime() + m);
}