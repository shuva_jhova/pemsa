////////////////////////////////////////////////////GLOBAL VARIABLES/////////////////////////////////////////////////////////////////////////////


var cod=0;
var idproducto;
var idprocesoeditar;
var idcomponenteeditar;
var iddelcomponente;
var idespecificacionpt;
var str2;
var codesae='';
var perfil=localStorage.getItem("perfil");

////////////////////////////////////////////////////FUNCTIONS FOR PRODUCTS/////////////////////////////////////////////////////////////////////////////

		///////////////////////////////////////ADD FILE ////////////////////////////////////////////////////

function addfile(){
	document.getElementById("docs").style.display="block";
}
function enablebtn(){
	document.getElementById("imgsavefile").style.display="block";
}
function uploadfiletoserver(){	
	
	var files = document.getElementById("doc1").files;
	var desc=document.getElementById("txtfiledescription").value;

	if(desc.length<=0){
		alert("No ha ingresado una descripci\u00F3n valida.");
	}else{
        	uploadFile(files[0],desc); // call the function to upload the file
	}
}

function addfile2(){
	document.getElementById("docs22").style.display="block";
}
function enablebtn2(){
	document.getElementById("imgsavefile2").style.display="block";
}
function uploadfiletoserver2(){	
	
	var files = document.getElementById("file12").files;
	var desc=document.getElementById("txtfiledescription2").value;

	if(desc.length<=0){
		alert("No ha ingresado una descripci\u00F3n valida.");
	}else{
        	uploadFile2(files[0],desc,codesae) // call the function to upload the file
	}
}
		//////////////////////////////////////UPDATE PRODUCT////////////////////////////////////////////////


function edit(str){
// Get the modal
var modal = document.getElementById('myModaledit');


// Get the <span> element that closes the modal
var span = document.getElementById("closeedit");
  //modal.style.display = "block";
REQUEST('appControl', 'getspecificproduct', [str] , onSpecificProduct, onerror);
// When the user clicks on <span> (x), close the modal
span.onclick = function() {
		
	 	var codigo = document.getElementById('txtcodigo').value;
		
			var a=confirm("No se guardar\u00E1 ning\u00FAn dato \u00BFSeguro que desea abandonar la edici\u00F3n?");
			if(a){
				modal.style.display = "none";
				
			}
		
	}
}


function saverecord()
{
	var iduser=localStorage.getItem('id');
	var codigo = document.getElementById('txtcodigo2').value;
	var descripcion = document.getElementById('txtdescripcion2').value;
	var nombre = document.getElementById('txtnombre2').value;
	var presentacion = document.getElementById('txtpresentacionVenta2').value;
	var marca = document.getElementById('txtmarca2').value;
	var codigobarras = document.getElementById('txtcodigodebarras2').value;
	var codigobarrasempaque = document.getElementById('txtcodigodebarrasempaque2').value;
	var pais = document.getElementById('txtpais2').value;
	var fraccionarancelaria = document.getElementById('txtfraccion2').value;
	var exportacion=getval(document.getElementsByName("exportacion2"));
	var activo='0';
	if(perfil==1){
		activo=getval(document.getElementsByName("activo2"));	
	}
	var barniz = "0"//getval(document.getElementsByName("barniz2"));
	var foto = " "; //document.getElementById('txtfoto2').value;
	var fotodetalle = " "; //document.getElementById('txtfotodetalle2').value;
	var notasespeciales = document.getElementById('txtnotas2').value;
	
	var corrugadospallet= document.getElementById('txtcorrugadoporpallet2').value;
	
	var camaspallete = document.getElementById('txtcamasporpallet2').value;
	
	var piezaspallet = document.getElementById('txtpiezasporpallet2').value;
	
	var pesopresentacion = document.getElementById('txtpesopresentacionventa2').value;
	
	
	var anchopallet = document.getElementById('txtanchopallet2').value;
	var pesopallete = document.getElementById('txtpesoporpallet2').value;
	var largopallet =document.getElementById('txtlargopallet2').value;
	var anchoproducto = document.getElementById('txtanchoproducto2').value;
	var alturapallet = document.getElementById('txtalturapallet2').value;
	var largoproducto = document.getElementById('txtlargoproducto2').value;
	var corrugadosporcama=document.getElementById('txtcorrugadosporcama2').value;
	var altoproducto = document.getElementById('txtaltoproducto2').value;
	var esp1=document.getElementById('txtespec1e').value;
	var esp2=document.getElementById('txtespec2e').value;
	var esp3=document.getElementById('txtespec3e').value;
	var esp4=document.getElementById('txtespec4e').value;
	var esp5=document.getElementById('txtespec5e').value;

	var a=document.getElementById('txtaproducto2').value;
	var b=document.getElementById('txtbproducto2').value;
	var h=document.getElementById('txthproducto2').value;

	var corrugadoinner = document.getElementById('txtcorrugadoinner2').value;
	var piezascorrugadoinner = document.getElementById('txtpiezascorrugadoinner2').value;
	var largocorrugadoinner = document.getElementById('txtlargocorrugadoinner2').value;
	var anchocorrugadoinner = document.getElementById('txtanchocorrugadoinner2').value;
	var altrocorrugadoinner = document.getElementById('txtaltocorrugadoinner2').value;
	var pesocorrugadoinner = document.getElementById('txtpesoporcorrugadoinner2').value;


	var corrugado = document.getElementById('txtcorrugadomaster2').value;
	var piezascorrugado = document.getElementById('txtpiezascorrugadomaster2').value;
	var largocorrugado = document.getElementById('txtlargocorrugadomaster2').value;
	var anchocorrugado = document.getElementById('txtanchocorrugadomaster2').value;
	var altrocorrugado = document.getElementById('txtaltocorrugadomaster2').value;
	var pesocorrugado = document.getElementById('txtpesoporcorrugadomaster2').value;

	var pesoneto= document.getElementById('txtpesoneto2').value;
	

 	if(codigo.length == 0 || codigo.length<8 || !isNumber(codigo))
	{
		alert("El c\u00F3digo SAE es inv\u00E1lido");
	}
	else if(nombre.length == 0)
	{
		alert("El nombre es inv\u00E1lido");
	}
	else
	{
		if(perfil==1){
			REQUEST('appControl', 'updateproductoenable', [codigo,descripcion,nombre,presentacion,marca,codigobarras,codigobarrasempaque,pais,fraccionarancelaria,exportacion,barniz,foto,fotodetalle,notasespeciales,corrugado,corrugadospallet,piezascorrugado,camaspallete,anchocorrugado,piezaspallet,largocorrugado,pesopresentacion,altrocorrugado,pesocorrugado,anchopallet,pesopallete,largopallet,anchoproducto,alturapallet,largoproducto,corrugadosporcama,altoproducto,activo,esp1,esp2,esp3,esp4,esp5,corrugadoinner,piezascorrugadoinner,largocorrugadoinner,anchocorrugadoinner,altrocorrugadoinner,pesocorrugadoinner,pesoneto,a,b,h], onUpdateproducto, onerror);
		}else{
			REQUEST('appControl', 'updateproducto', [codigo,descripcion,nombre,presentacion,marca,codigobarras,codigobarrasempaque,pais,fraccionarancelaria,exportacion,barniz,foto,fotodetalle,notasespeciales,corrugado,corrugadospallet,piezascorrugado,camaspallete,anchocorrugado,piezaspallet,largocorrugado,pesopresentacion,altrocorrugado,pesocorrugado,anchopallet,pesopallete,largopallet,anchoproducto,alturapallet,largoproducto,corrugadosporcama,altoproducto,esp1,esp2,esp3,esp4,esp5,corrugadoinner,piezascorrugadoinner,largocorrugadoinner,anchocorrugadoinner,altrocorrugadoinner,pesocorrugadoinner,pesoneto,a,b,h], onUpdateproducto, onerror);
		}
	}
}

function onUpdateproducto(dataResult)
{
	if(dataResult.length>0){
		if(dataResult[0]['exito'].localeCompare("si")==0)
		{		
			alert("\u00C9xito al modificar los datos");
			var iframe = window.parent.document.getElementById("iframe");
			iframe.contentWindow.location.reload(true);
		}
		else if(dataResult[0]['exito'].localeCompare("sin")==0)
		{
			alert("El producto no ha sido encontrado");
		}
	}else
	{
		alert("Error al modificar los datos. Intente nuevamente");
	}
    	
	
}

		//////////////////////////////////////ADD PRODUCT////////////////////////////////////////////////

function agregar(){
			var div = document.getElementById("docs");
			disableEnable(div.getElementsByTagName("input"), true);
			disableEnable(div.getElementsByTagName("select"), true);
			disableEnable(div.getElementsByTagName("button"), true);
			disableEnable(div.getElementsByTagName("textarea"), true);
			var div2 = document.getElementById("mm");
			disableEnable(div2.getElementsByTagName("input"), true);
			disableEnable(div2.getElementsByTagName("select"), true);
			disableEnable(div2.getElementsByTagName("button"), true);
			disableEnable(div2.getElementsByTagName("textarea"), true);
			document.getElementsByName("save")[0].disabled=true;
// Get the modal
var modal = document.getElementById('myModal');


// Get the <span> element that closes the modal
var span = document.getElementsByClassName("close")[0];
  modal.style.display = "block";

// When the user clicks on <span> (x), close the modal
span.onclick = function() {
		
	 	var codigo = document.getElementById('txtcodigo').value;
		if(codigo.length>0){
			var a=confirm("No se guardar\u00E1 ning\u00FAn dato \u00BFSeguro que desea abandonar la captura?");
			if(a){
				modal.style.display = "none";
				 var iduser=localStorage.getItem('id');
				REQUEST('appControl', 'deletepaths', [iduser,codigo], onDeletepaths, onerror);
			}
		}else{
			modal.style.display = "none";
		}
	}
getallprodsaux();
}


function getallprodsaux(){
	REQUEST('appControl', 'getallproductsaux', [],onProductsaux, onerror);
}

function onProductsaux(dataResult){
	
	var dataList = document.getElementById('prodsaux');
	dataList.innerHTML="";
	for (var i = 0; i<dataResult.length; i++)
	{
    		var opt = document.createElement('option');
    		opt.value = completecvesae(dataResult[i]['sae']);
		dataList.appendChild(opt);
    	}
paises();
}

function onDeletepaths(dataResult){
var iframe = window.parent.document.getElementById("iframe");
		iframe.contentWindow.location.reload(true);	
}




function validateSAE(){
	var codigo = document.getElementById('txtcodigo').value;

	/*if(codigo.length==8 && isNumber(codigo)){
		REQUEST('appControl', 'validateSAE', [codigo], onValidateSae, onerror);
		
	}else if(codigo.length==10 && codigo.indexOf("-")>0 && (codigo.indexOf("A")>0||codigo.indexOf("B")>0 || codigo.indexOf("a")>0||codigo.indexOf("b")>0 )){
		REQUEST('appControl', 'validateSAE', [codigo], onValidateSae, onerror);

	}else{
		var div = document.getElementById("docs");
			disableEnable(div.getElementsByTagName("input"), true);
			disableEnable(div.getElementsByTagName("select"), true);
			disableEnable(div.getElementsByTagName("button"), true);
			disableEnable(div.getElementsByTagName("textarea"), true);
			var div2 = document.getElementById("mm");
			disableEnable(div2.getElementsByTagName("input"), true);
			disableEnable(div2.getElementsByTagName("select"), true);
			disableEnable(div2.getElementsByTagName("button"), true);
			disableEnable(div2.getElementsByTagName("textarea"), true);
			document.getElementsByName("save")[0].disabled=true;
		alert("El c\u00F3digo SAE es inv\u00E1lido");
		//document.getElementById('txtcodigo').focus();
	}*/

	if(codigo.length == 0){}
	else{
	if( codigo.length<8 || !isNumber(codigo))
	{		var div = document.getElementById("docs");
			disableEnable(div.getElementsByTagName("input"), true);
			disableEnable(div.getElementsByTagName("select"), true);
			disableEnable(div.getElementsByTagName("button"), true);
			disableEnable(div.getElementsByTagName("textarea"), true);
			var div2 = document.getElementById("mm");
			disableEnable(div2.getElementsByTagName("input"), true);
			disableEnable(div2.getElementsByTagName("select"), true);
			disableEnable(div2.getElementsByTagName("button"), true);
			disableEnable(div2.getElementsByTagName("textarea"), true);
			document.getElementsByName("save")[0].disabled=true;
		alert("El c\u00F3digo SAE es inv\u00E1lido");
		//document.getElementById('txtcodigo').focus();
	}
	else
	{
		REQUEST('appControl', 'validateSAE', [codigo], onValidateSae, onerror);
	}
	}
}


function onValidateSae(dataResult){
	var codigo = document.getElementById('txtcodigo').value;

	if(dataResult.length>0){
		if(dataResult[0]['valido'].localeCompare("Si")==0){
			var div = document.getElementById("docs");
			disableEnable(div.getElementsByTagName("input"), false);
			//disableEnable(div.getElementsByTagName("select"), false);
			disableEnable(div.getElementsByTagName("button"), false);
			disableEnable(div.getElementsByTagName("textarea"), false);
			var div2 = document.getElementById("mm");
			disableEnable(div2.getElementsByTagName("input"), false);
			//disableEnable(div2.getElementsByTagName("select"), false);
			disableEnable(div2.getElementsByTagName("button"), false);
			disableEnable(div2.getElementsByTagName("textarea"), false);
			document.getElementsByName("save")[0].disabled=false;

		REQUEST('appControl', 'getspecificproductsaux', [codigo], oncompletebysae, onerror);
			

		}else if(dataResult[0]['valido'].localeCompare("Existe")==0){
			var div = document.getElementById("docs");
			disableEnable(div.getElementsByTagName("input"), true);
			//disableEnable(div.getElementsByTagName("select"), true);
			disableEnable(div.getElementsByTagName("button"), true);
			disableEnable(div.getElementsByTagName("textarea"), true);
			var div2 = document.getElementById("mm");
			disableEnable(div2.getElementsByTagName("input"), true);
			//disableEnable(div2.getElementsByTagName("select"), true);
			disableEnable(div2.getElementsByTagName("button"), true);
			disableEnable(div2.getElementsByTagName("textarea"), true);
			document.getElementsByName("save")[0].disabled=true;
			alert("El c\u00F3digo SAE ya ha sido dado de alta. Por favor ingresa otro");
		}		
	}

}

function oncompletebysae(dataResult){
	if(dataResult.length>0){
	
	 document.getElementById('txtnombre').value=dataResult[0]['nombre'];
	}else{


	}
}

function saveproducto(){
	var iduser=localStorage.getItem('id');
	var codigo = document.getElementById('txtcodigo').value;
	var descripcion = document.getElementById('txtdescripcion').value;
	var nombre = document.getElementById('txtnombre').value;
	var presentacion = document.getElementById('txtpresentacionVenta').value;
	var marca = document.getElementById('txtmarca').value;
	var codigobarras = document.getElementById('txtcodigodebarras').value;
	var codigobarrasempaque = document.getElementById('txtcodigodebarrasempaque').value;
	var pais = document.getElementById('txtpais').value;
	var fraccionarancelaria = document.getElementById('txtfraccion').value;
	var exportacion=getval(document.getElementsByName("exportacion"));	
	var barniz = getval(document.getElementsByName("barniz"));
	var foto = ""; //document.getElementById('txtfoto').value;
	var fotodetalle = "";//document.getElementById('txtfotodetalle').value;
	var notasespeciales = document.getElementById('txtnotas').value;

	var corrugadoinner = document.getElementById('txtcorrugadoinner').value;
	var piezascorrugadoinner = document.getElementById('txtpiezascorrugadoinner').value;
	var largocorrugadoinner = document.getElementById('txtlargocorrugadoinner').value;
	var anchocorrugadoinner = document.getElementById('txtanchocorrugadoinner').value;
	var altrocorrugadoinner = document.getElementById('txtaltocorrugadoinner').value;
	var pesocorrugadoinner = document.getElementById('txtpesoporcorrugadoinner').value;


	var corrugado = document.getElementById('txtcorrugadomaster').value;
	var piezascorrugado = document.getElementById('txtpiezascorrugadomaster').value;
	var largocorrugado = document.getElementById('txtlargocorrugadomaster').value;
	var anchocorrugado = document.getElementById('txtanchocorrugadomaster').value;
	var altrocorrugado = document.getElementById('txtaltocorrugadomaster').value;
	var pesocorrugado = document.getElementById('txtpesoporcorrugadomaster').value;

	var pesoneto= document.getElementById('txtpesoneto').value;


	var corrugadospallet= document.getElementById('txtcorrugadoporpallet').value;
	var camaspallete = document.getElementById('txtcamasporpallet').value;
	var piezaspallet = document.getElementById('txtpiezasporpallet').value;
	var pesopresentacion = document.getElementById('txtpesopresentacionventa').value;
	var anchopallet = document.getElementById('txtanchopallet').value;
	var pesopallete = document.getElementById('txtpesoporpallet').value;
	var largopallet =document.getElementById('txtlargopallet').value;
	var anchoproducto = document.getElementById('txtanchoproducto').value;
	var alturapallet = document.getElementById('txtalturapallet').value;
	var largoproducto = document.getElementById('txtlargoproducto').value;
	var corrugadosporcama=document.getElementById('txtcorrugadosporcama').value;
	var altoproducto = document.getElementById('txtaltoproducto').value;
	var esp1=document.getElementById("txtespec1").value;
	var esp2=document.getElementById("txtespec2").value;
	var esp3=document.getElementById("txtespec3").value;
	var esp4=document.getElementById("txtespec4").value;
	var esp5=document.getElementById("txtespec5").value;
	var a=document.getElementById("txtaproducto").value;
	var b=document.getElementById("txtbproducto").value;
	var h=document.getElementById("txthproducto").value;
 	if(codigo.length == 0 || codigo.length<8 || !isNumber(codigo))
	{
		alert("El c\u00F3digo SAE es inv\u00E1lido");
	}
	else if(nombre.length == 0)
	{
		alert("El nombre es inv\u00E1lido");
	}
	else
	{
		REQUEST('appControl', 'saveproducto', [iduser,codigo,descripcion,nombre,presentacion,marca,codigobarras,codigobarrasempaque,pais,fraccionarancelaria,exportacion,barniz,foto,fotodetalle,notasespeciales,corrugado,corrugadospallet,piezascorrugado,camaspallete,anchocorrugado,piezaspallet,largocorrugado,pesopresentacion,altrocorrugado,pesocorrugado,anchopallet,pesopallete,largopallet,anchoproducto,alturapallet,largoproducto,corrugadosporcama,altoproducto,esp1,esp2,esp3,esp4,esp5,corrugadoinner,piezascorrugadoinner,largocorrugadoinner,anchocorrugadoinner,altrocorrugadoinner,pesocorrugadoinner,pesoneto,a,b,h], onsaveproducto, onerror);
	}
}
function onsaveproducto(dataResult){
	if(dataResult[0]['exito'].localeCompare("si")==0)
	{
		alert("\u00C9xito al Guardar Datos("+dataResult[0]['folio']+")");
		var iframe = window.parent.document.getElementById("iframe");
		iframe.contentWindow.location.reload(true);
	}
	else if(dataResult[0]['exito'].localeCompare("ya")==0)
	{
		alert("El producto ya ha sido dado de alta");
	}
	else if(dataResult[0]['exito'].localeCompare("no")==0)
	{	
		alert("Error al Guardar Datos. Intente nuevamente");
	}

}





		//////////////////////////////////////SEARCH PRODUCTS////////////////////////////////////////////////


function getproductos(){
	document.getElementById("imgload").style.display="block";
	if(perfil==1){
		REQUEST('appControl', 'getallproducts', [] , onProductos, onerror);
	}else{
		REQUEST('appControl', 'getallproductsenable', [] , onProductos, onerror);
	}

}

function onProductos(dataResult){
	document.getElementById("imgload").style.display="none";
	myDeleteFunction() ;
		if(dataResult.length>0){
			

		for (var i = 0; i<dataResult.length; i++)
		{
			var tableRef = document.getElementById('resultado').getElementsByTagName('tbody')[0];;
			// Insert a row in the table at the last row
			var newRow   = tableRef.insertRow(tableRef.rows.length);
			newRow.className = dataResult[i]["color"];//"table-row";
			//newRow.style.background=dataResult[i]["color"];
			newRow.id='pp'+dataResult[i]["id"];
			// Insert a cell in the row at index 0
			var newCell  = newRow.insertCell(0);
			newCell.id="codigo"+dataResult[i]["id"];
			var newText  = document.createTextNode(completecvesae(dataResult[i]["codigo"]));
			newCell.appendChild(newText);

			var newCell  = newRow.insertCell(1);
			newCell.id="nombre"+dataResult[i]["id"];
			var newText  = document.createTextNode(dataResult[i]["nombre"]);
			newCell.appendChild(newText);

			newCell  = newRow.insertCell(2);
			var img2 = document.createElement('img');
   			img2.src = "../img/view-files.png";
			img2.addEventListener('click', viewfiles.bind(null,dataResult[i]["id"]));
			newCell.appendChild(img2);

			var img2f = document.createElement('img');
   			img2f.src = "../img/fichatecnica.png";
			img2f.addEventListener('click', printficha.bind(null,dataResult[i]["id"]));
			newCell.appendChild(img2f);

			newCell  = newRow.insertCell(3);
			var img2 = document.createElement('img');
   			img2.src = "../img/see.png";
			img2.addEventListener('click', procesos.bind(null,dataResult[i]["id"]));
			newCell.appendChild(img2);			

			newCell  = newRow.insertCell(4);
			var img2 = document.createElement('img');
   			img2.src = "../img/see.png";
			img2.addEventListener('click',componentes.bind(null,dataResult[i]["id"]));
			newCell.appendChild(img2);

			newCell  = newRow.insertCell(5);
			var img2pt = document.createElement('img');
   			img2pt.src = "../img/see.png";
			img2pt.addEventListener('click',especificacionespt.bind(null,dataResult[i]["id"]));
			newCell.appendChild(img2pt);

			newCell  = newRow.insertCell(6);
			newCell.id="activo"+dataResult[i]["id"];
			//newCell.contentEditable = "true";
			// Append a text node to the cell
			var activo="";
			if(dataResult[i]["activo"]>0){
				activo="Si";
			}else{
				activo="No";
			}
			newText  = document.createTextNode(activo);
			newCell.appendChild(newText);


			newCell  = newRow.insertCell(7);
			var img = document.createElement('img');
   			img.src = "../img/editar.png";
			img.id="imgeditar"+dataResult[i]["id"];


			/*if(dataResult[i]["ordenes"]!=null){
				
				img.addEventListener('click', userdenied.bind(null,dataResult[i]["id"],true));
				
			}else{*/
				img.addEventListener('click', edit.bind(null,dataResult[i]["id"]));
			//}

			
			newCell.appendChild(img);
			

			newCell  = newRow.insertCell(8);
			var img2 = document.createElement('img');
   			img2.src = "../img/delete.png";

			if(dataResult[i]["ordenes"]!=null){
				
				img2.addEventListener('click', userdenied.bind(null,dataResult[i]["id"],true));
				
			}else{	
				img2.addEventListener('click', deleterecord.bind(null,dataResult[i]["id"]));
			}


		
			newCell.appendChild(img2);


			newCell  = newRow.insertCell(9);
			var img3 = document.createElement('img');
   			img3.src = "../img/duplicate.png";
			img3.addEventListener('click', duplicate.bind(null,dataResult[i]["id"]));
			newCell.appendChild(img3)
			
		}
		
		document.getElementById("sin").style.display="none";
		document.getElementById("tabla").style.display="block";
		document.getElementById("sin2").style.display="block";
		pagination('#resultado');
	
	}else{
		document.getElementById("sin").style.display="block";
		document.getElementById("tabla").style.display="none";
		document.getElementById("sin2").style.display="none";
	
	}	
}

function pagination(id){
		$(document).ready(function() {
   			 $(id).DataTable( {
				"destroy":true,
      				 "scrollY": 400,
       				 "scrollX": true
    			} );
		} );
}
function printficha(str){
	window.open('fichatecnicapdf.php?idp='+str, '_blank');
}
		//////////////////////////////////////SEARCH SPECIFIC PRODUCT////////////////////////////////////////////////


function onSpecificProduct(dataResult){
	var cad;
	if(dataResult.length>0){
			
		document.getElementById('txtcodigo2').value=completecvesae(dataResult[0]['codigo']);
		document.getElementById('txtdescripcion2').value=dataResult[0]['descripcion'];
		document.getElementById('txtnombre2').value=dataResult[0]['nombre'];
		document.getElementById('txtpresentacionVenta2').value="Pieza";//dataResult[0]['presentacion'];
		document.getElementById('txtmarca2').value=dataResult[0]['marca'];
		document.getElementById('txtcodigodebarras2').value=dataResult[0]['codigobarrasproducto'];
		document.getElementById('txtcodigodebarrasempaque2').value=dataResult[0]['codigobarrasempaque'];
		document.getElementById('txtpais2').value=dataResult[0]['pais'];
		document.getElementById('txtfraccion2').value=dataResult[0]['fraccionarancelaria'];
		if(dataResult[0]['exportacion']==1){
			cad="Exportacion<br><label class='fondotxt'><input type='radio' name='exportacion2' value='1' checked='checked'>Si<input type='radio' name='exportacion2' value='0'>No</label>";
		}else{
			cad="Exportacion<br><label class='fondotxt'><input type='radio' name='exportacion2' value='1'>Si<input type='radio' name='exportacion2' checked='checked' value='0'>No</label>";
		
		}
		document.getElementById('exportacion').innerHTML=cad;

		/*if(dataResult[0]['barniz'].localeCompare("Mate")==0){
			cad="Barniz<br><label class='fondotxt'><input type='radio' name='barniz2' value='Mate' checked='checked'>Mate<input type='radio' name='barniz2' value='Brillante'>Brillante</label>";
		}else{
			cad="Barniz<br><label class='fondotxt'><input type='radio' name='barniz2' value='Mate'>Mate<input type='radio' name='barniz2' checked='checked' value='Brillante'>Brillante</label>";
		
		}
		
		document.getElementById('barniz').innerHTML=cad;*/

		if(perfil==1){
			if(dataResult[0]['activo']==1){
				cad="Activo<br><label class='fondotxt'><input type='radio' name='activo2' value='1' checked='checked'>Si<input type='radio' name='activo2' value='0'>No</label>";
			}else{
				cad="Activo<br><label class='fondotxt'><input type='radio' name='activo2' value='1'>Si<input type='radio' name='activo2' checked='checked' value='0'>No</label>";
		
			}
		
			document.getElementById('activo').innerHTML=cad;
			document.getElementById('activo').style.display="block";
		}else{
			document.getElementById('activo').style.display="none";
		}

		/*if(dataResult[0]['foto'].length>0)
		{
			document.getElementById("imgfoto2").src=dataResult[0]['foto'];
			document.getElementById("imgfoto2").className ="thumb2" ;
			document.getElementById("fotodiv2").className ="imag2" ;
		}else{

			document.getElementById("imgfoto2").src="../img/null.png";
			document.getElementById("imgfoto2").className ="thumb2" ;
			document.getElementById("fotodiv2").className ="imag2" ;
		}
		if(dataResult[0]['fotodetalle'].length>0)
		{
			document.getElementById("imgfotodetalle2").src=dataResult[0]['fotodetalle'];
			document.getElementById("imgfotodetalle2").className ="thumb2" ;
			document.getElementById("fotodetallediv2").className ="imag2" ;
		}else{
			document.getElementById("imgfotodetalle2").src="../img/null.png";
			document.getElementById("imgfotodetalle2").className ="thumb2" ;
			document.getElementById("fotodetallediv2").className ="imag2" ;
		}*/

		document.getElementById('txtespec1e').value=dataResult[0]['doc1'];
		document.getElementById('txtespec2e').value=dataResult[0]['doc2'];
		document.getElementById('txtespec3e').value=dataResult[0]['doc3'];
		document.getElementById('txtespec4e').value=dataResult[0]['doc4'];
		document.getElementById('txtespec5e').value=dataResult[0]['doc5'];

		document.getElementById('txtaproducto2').value=dataResult[0]['a'];
		document.getElementById('txtbproducto2').value=dataResult[0]['b'];
		document.getElementById('txthproducto2').value=dataResult[0]['h'];


		document.getElementsByName("exportacion2");	
		document.getElementsByName("barniz2");
		//document.getElementById('txtfoto2').value=dataResult[0]['foto'];
		//document.getElementById('txtfotodetalle2').value=dataResult[0]['fotodetalle'];
		document.getElementById('txtnotas2').value=dataResult[0]['notas'];
		
		document.getElementById('txtcorrugadoporpallet2').value=dataResult[0]['corrugadospallet'];
		
		document.getElementById('txtcamasporpallet2').value=dataResult[0]['camaspallet'];
	
		document.getElementById('txtpiezasporpallet2').value=dataResult[0]['piezaspallet'];
		
		document.getElementById('txtpesopresentacionventa2').value=dataResult[0]['pesoprod'];
		
		document.getElementById('txtanchopallet2').value=dataResult[0]['anchopallet'];
		document.getElementById('txtpesoporpallet2').value=dataResult[0]['pesobrutopallet'];
		document.getElementById('txtlargopallet2').value=dataResult[0]['largopallet'];
		document.getElementById('txtanchoproducto2').value=dataResult[0]['anchoprod'];
		document.getElementById('txtalturapallet2').value=dataResult[0]['altopallet'];
		document.getElementById('txtlargoproducto2').value=dataResult[0]['largoprod'];
		document.getElementById('txtcorrugadosporcama2').value=dataResult[0]['corrugadoscamapallet'];
		document.getElementById('txtaltoproducto2').value=dataResult[0]['altoprod'];
			
		

		document.getElementById('txtcorrugadoinner2').value=dataResult[0]['empaqueinner'];
		document.getElementById('txtpiezascorrugadoinner2').value=dataResult[0]['multiploinner'];
		document.getElementById('txtlargocorrugadoinner2').value=dataResult[0]['largoinner'];
		document.getElementById('txtanchocorrugadoinner2').value=dataResult[0]['anchoinner'];
		document.getElementById('txtaltocorrugadoinner2').value=dataResult[0]['altoinner'];
		document.getElementById('txtpesoporcorrugadoinner2').value=dataResult[0]['pesoinner'];


		document.getElementById('txtcorrugadomaster2').value=dataResult[0]['empaquemaster'];
		document.getElementById('txtpiezascorrugadomaster2').value=dataResult[0]['multiplomaster'];
		document.getElementById('txtlargocorrugadomaster2').value=dataResult[0]['largomaster'];
		document.getElementById('txtanchocorrugadomaster2').value=dataResult[0]['anchomaster'];
		document.getElementById('txtaltocorrugadomaster2').value=dataResult[0]['altomaster'];
		document.getElementById('txtpesoporcorrugadomaster2').value=dataResult[0]['pesomaster'];

		document.getElementById('txtpesoneto2').value=dataResult[0]['pesonetopallet'];


		document.getElementById('myModaledit').style.display = "block";;
	}
paises();
}

		//////////////////////////////////////////////////GET LIST OF COUNTRIES /////////////////////////////////
function paises()
{
REQUEST('appControl', 'getpaises', [] , onPaises, onerror);

}
function onPaises(dataResult)
{
	var dataList = document.getElementById('paises');
	dataList.innerHTML="";
	for (var i = 0; i<dataResult.length; i++)
	{
    		var opt = document.createElement('option');
    		opt.value = dataResult[i]['pais'];
		dataList.appendChild(opt);
    	}
presentacionventa();
}

function presentacionventa()
{
REQUEST('appControl', 'getpresentacionventa', [] , onPresentacion, onerror);

}
function onPresentacion(dataResult)
{
	var dataList = document.getElementById('presentacion');
	dataList.innerHTML="";
	for (var i = 0; i<dataResult.length; i++)
	{
    		var opt = document.createElement('option');
    		opt.value = dataResult[i]['presentacion'];
		dataList.appendChild(opt);
    	}
}


		////////////////////////////////////FUNCTION FOR VIEW FILES/////////////////////////////////////////
function viewfiles(str){
	idproducto=str;
	var row=document.getElementById('pp'+str);
	var cells = row.getElementsByTagName('td');
	
	// Get the modal
	codesae=cells[0].innerText;
	document.getElementById('filespodructo').innerText=cells[0].innerText+"-"+cells[1].innerText;
	var modal = document.getElementById('myModalfiles');


	// Get the <span> element that closes the modal
	var span = document.getElementById("closefiles");
  	modal.style.display = "block";

	// When the user clicks on <span> (x), close the modal
	span.onclick = function() {
	 		modal.style.display = "none";
	}
	
	 getfilesbyproduct(cells[0].innerText);
}

function getfilesbyproduct(sae){
document.getElementById("imgloadfiles").style.display="block";
REQUEST('appControl', 'getfilesproducto', [idproducto,sae] , onFilesproducto, onerror);

}
function  onFilesproducto(dataResult){
	document.getElementById("imgloadfiles").style.display="none";
	myDeleteFunctionfiles() ;
	if(dataResult.length>0){
			

		for (var i = 0; i<dataResult.length; i++)
		{
			var tableRef = document.getElementById('resultadofiles').getElementsByTagName('tbody')[0];;
			// Insert a row in the table at the last row
			var newRow   = tableRef.insertRow(tableRef.rows.length);
			newRow.className = "table-row";
			newRow.id="file-"+dataResult[i]["id"];
			// Insert a cell in the row at index 0
			var newCell  = newRow.insertCell(0);
			newCell.id="descripcion"+dataResult[i]["id"];
			var newText  = document.createTextNode(dataResult[i]["descripcion"]);
			newCell.appendChild(newText);

			var newCell  = newRow.insertCell(1);
			newCell.id="ruta"+dataResult[i]["id"];
			var ext=dataResult[i]["ruta"].split(".");
			//newText  = document.createTextNode();
			newCell.appendChild(createelement(ext[1],dataResult[i]["ruta"]));

			newCell  = newRow.insertCell(2);
			var img = document.createElement('img');
   			img.src = "../img/editar.png";
			img.id="imgeditarfile"+dataResult[i]["id"];
			img.addEventListener('click', editfile.bind(null,dataResult[i]["id"]));
			newCell.appendChild(img);
				
				var img3 = document.createElement('img');
	   			img3.src = "../img/save.png";
				img3.id="imgsavefile"+dataResult[i]["id"];
				img3.style.display="none";
				img3.addEventListener('click', savefiledit.bind(null,dataResult[i]["id"]));
				newCell.appendChild(img3);
				var img4 = document.createElement('img');
   				img4.src = "../img/cancel.png";
				img4.id="imgcancelfile"+dataResult[i]["id"];
				img4.style.display="none";
				img4.addEventListener('click', canceleditfile);
				newCell.appendChild(img4);


			newCell  = newRow.insertCell(3);
			var img2 = document.createElement('img');
   			img2.src = "../img/delete.png";
			img2.addEventListener('click', deleterecordfile.bind(null,dataResult[i]["id"]));
			newCell.appendChild(img2);


			
			
		}
		
		document.getElementById("sinfiles").style.display="none";
		document.getElementById("tablafiles").style.display="block";
		//document.getElementById("sin2com").style.display="block";
	
	}else{
		document.getElementById("sinfiles").style.display="block";
		document.getElementById("tablafiles").style.display="none";
		//document.getElementById("sin2com").style.display="none";
	
	}
}

function editfile(str){
	var row=document.getElementById("file-"+str);
	//row.className = "table-row2";
	var cells = row.getElementsByTagName('td');
		
	for(var a=0;a<cells.length;a++){
		cells[a].style.backgroundColor="#9adbcf";
	}

	var img1=document.getElementById('imgeditarfile'+str);
	var img2=document.getElementById('imgsavefile'+str);
	var img3=document.getElementById('imgcancelfile'+str);

	img1.style.display="none";
	img2.style.display="block";
	img3.style.display="block";


		var desc=cells[0].innerText;
		cells[0].innerText="";
		cells[0].contentEditable="false";
    		  
	
		var x = document.createElement("textarea");
		x.value=desc;
    		x.cols="24";
		x.rows="2";
		x.id="txtdescripcionfile"+str;
   		cells[0].appendChild(x);


		cells[1].innerText="";
		cells[1].contentEditable="false";
    		  
		x = document.createElement("input");
		x.type="file";
		x.id="fileupdate"+str;
   		cells[1].appendChild(x);

}
function canceleditfile(){
	if(confirm("No se guradar\u00E1 ning\u00FAn cambio realizado. \u00BFSeguro que desea continuar?")){
		viewfiles(idproducto);	
	}
}

function savefiledit(str){
	var desc=document.getElementById("txtdescripcionfile"+str).value;
	var file=document.getElementById("fileupdate"+str).files;
	if(confirm("Esta a punto de modificar este archivo. \u00BFSeguro de continuar?")){
		uploadFile3(file[0],desc,codesae,str);
	}

}
function deleterecordfile(str){

	var res=confirm("\u00BFSeguro que desea eliminar este registro?");

	if(res){
		REQUEST('appControl', 'deletefile', [str] , onFiledelete, onerror);
	}
}

function onFiledelete(dataResult){
	if(dataResult[0]['exito'].localeCompare("si")==0)
	{	
		alert("\u00C9xito al eliminar el archivo");
		viewfiles(idproducto);	
			
	}
	else if(dataResult[0]['exito'].localeCompare("sin")==0)
	{
		alert("El archivo no ha sido encontrado");
	}else
	{
		alert("Error al eliminar los datos. Intente nuevamente");
	}	
}
		////////////////////////////////////FUNCTION FOR DELETE PRODUCT OR DISABLE IT/////////////////////////////////////////

function deleterecord(str){
	
	var res=confirm("\u00BFSeguro que desea descontinuar este registro?");

	if(res){
		REQUEST('appControl', 'deleteproducto', [str] , onProductodelete, onerror);
	}
}

function onProductodelete(dataResult){
	if(dataResult[0]['exito'].localeCompare("si")==0)
	{	
		alert("\u00C9xito al descontinuar el producto");
		var iframe = window.parent.document.getElementById("iframe");
			iframe.contentWindow.location.reload(true);
			
	}
	else if(dataResult[0]['exito'].localeCompare("sin")==0)
	{
		alert("El producto no ha sido encontrado");
	}else
	{
		alert("Error al eliminar los datos. Intente nuevamente");
	}	
}

function cancela(str){

	var si=confirm("Aunque los cambios hallan sido realizados no se guardar\u00E1n\n\u00BFSeguro que desea cancelar la modificaci\u00F3n sin guardar?");
	if(si){ 
		
		var iframe = window.parent.document.getElementById("iframe");
		iframe.contentWindow.location.reload(true);
	}
}


		////////////////////////////////////FUNCTION FOR DUPLICATE PRODUCT AND ALL RELATED OR ASOCIATED TO IT/////////////////////////////

function validateSAEduplicate(){
	var codigo = document.getElementById('txtcodigoduplicate').value;
	if(codigo.length==8 && isNumber(codigo)){
		REQUEST('appControl', 'validateSAE', [codigo], onValidateSaeduplicate, onerror);
		
	}else if(codigo.length==10 && codigo.indexOf("-")>0 && (codigo.indexOf("A")>0||codigo.indexOf("B")>0 || codigo.indexOf("a")>0||codigo.indexOf("b")>0 )){
		REQUEST('appControl', 'validateSAE', [codigo], onValidateSaeduplicate, onerror);

	}else{
		document.getElementsByName("saveduplicate")[0].disabled=true;
		alert("El c\u00F3digo SAE es inv\u00E1lido");
		//document.getElementById('txtcodigo').focus();
	}



	if(codigo.length == 0 || codigo.length<8 || !isNumber(codigo))
	{		document.getElementsByName("saveduplicate")[0].disabled=true;
		alert("El c\u00F3digo SAE es inv\u00E1lido");
	}
	else
	{
		REQUEST('appControl', 'validateSAE', [codigo], onValidateSaeduplicate, onerror);
	}
}


function onValidateSaeduplicate(dataResult){
	if(dataResult.length>0){
		if(dataResult[0]['valido'].localeCompare("Si")==0){
			document.getElementsByName("saveduplicate")[0].disabled=false;

		}else if(dataResult[0]['valido'].localeCompare("Existe")==0){
			document.getElementsByName("saveduplicate")[0].disabled=true;
			alert("El c\u00F3digo SAE ya ha sido dado de alta. Por favor ingresa otro");
		}else if(dataResult[0]['valido'].localeCompare("Sin A")==0){
			document.getElementsByName("saveduplicate")[0].disabled=true;
			alert("A\u00FAn no se ingresa el c\u00F3digo SAE del lado A");
		}		
	}

}


function duplicate(str){
	var row=document.getElementById("pp"+str);
	var cells = row.getElementsByTagName('td');
	str2=cells[0].innerText;
	// Get the modal
	var modal = document.getElementById('myModalduplicate');


	// Get the <span> element that closes the modal
	var span = document.getElementById("closeduplicate");
  	modal.style.display = "block";

	// When the user clicks on <span> (x), close the modal
	span.onclick = function() {
	 		modal.style.display = "none";
	}
}

function saveduplicate(){
	var idold=str2;
	var idnew=document.getElementById("txtcodigoduplicate").value;		
	var nombrenew=document.getElementById("txtnombreduplicate").value;		
	if(idold.length==0){

		alert("Hubo un error al duplicar el producto. Intente mas tarde");	
		var iframe = window.parent.document.getElementById("iframe");
			iframe.contentWindow.location.reload(true);	
	}else if(idnew.length==0 || idnew.length<8){
		alert("El c\u00F3digo SAE es incorrecto");
	}else if(nombrenew.length==0){
		alert("El nombre del producto es incorrecto");
	}else{
		REQUEST('appControl', 'duplicateproducto', [idold,idnew,nombrenew] , onProductoduplicate, onerror);
	}
}


function onProductoduplicate(dataResult){
	if(dataResult[0]['exito'].localeCompare("si")==0)
	{
		alert("\u00C9xito al Guardar Datos("+dataResult[0]['folio']+")");
		var iframe = window.parent.document.getElementById("iframe");
		iframe.contentWindow.location.reload(true);
	}
	else if(dataResult[0]['exito'].localeCompare("ya")==0)
	{
		alert("El producto ya ha sido dado de alta");
	}
	else if(dataResult[0]['exito'].localeCompare("no")==0)
	{	
		alert("Error al Guardar Datos. Intente nuevamente");
	}
}

 

////////////////////////////////////////////////////FUNCTIONS FOR PROCESS BY PRODUCT/////////////////////////////////////////////////////////////////////////////

		///////////////////////////////////FUNCTION FOR GET PROCESS BY PRODUCT SELECTED////////////////////////////////////////////

function procesos(str){
	idproducto=str;
	var row=document.getElementById('pp'+str);
	var cells = row.getElementsByTagName('td');
	//str2=cells[0].innerText;
	// Get the modal

	document.getElementById('propodructo').innerText=cells[0].innerText+"-"+cells[1].innerText;
	var modal = document.getElementById('myModalprocesos');


	// Get the <span> element that closes the modal
	var span = document.getElementById("closeprocesos");
  	modal.style.display = "block";

	// When the user clicks on <span> (x), close the modal
	span.onclick = function() {
	 		modal.style.display = "none";
	}
	document.getElementById("editaproceso").style.display="none";
	document.getElementById("altaproceso").style.display="none";
	document.getElementById("tablaproceso").style.display="block";
	document.getElementById("agregarr").style.display="block";
	document.getElementById("sin2pro").style.display="block";
	getprocessbyproduct();
}

function getprocessbyproduct(){
document.getElementById("imgloadpro").style.display="block";
REQUEST('appControl', 'getprocesosproducto', [idproducto] , onProcesosproducto, onerror);

}
function  onProcesosproducto(dataResult){
	document.getElementById("imgloadpro").style.display="none";
	myDeleteFunctionpro();
		if(dataResult.length>0){
			

		for (var i = 0; i<dataResult.length; i++)
		{
			var tableRef = document.getElementById('resultadopro').getElementsByTagName('tbody')[0];;
			// Insert a row in the table at the last row
			var newRow   = tableRef.insertRow(tableRef.rows.length);
			newRow.className = "table-row";
			newRow.id="proceso-"+dataResult[i]["id"];
			// Insert a cell in the row at index 0
			var newCell  = newRow.insertCell(0);
			newCell.id="codigo"+dataResult[i]["id"];
			var newText  = document.createTextNode(dataResult[i]["id"]);
			newCell.appendChild(newText);

			var newCell  = newRow.insertCell(1);
			newCell.id="maquina"+dataResult[i]["id"];
			var newText  = document.createTextNode(dataResult[i]["maquina"]);
			newCell.appendChild(newText);

			var newCell  = newRow.insertCell(2);
			newCell.id="proceso"+dataResult[i]["id"];
			var newText  = document.createTextNode(dataResult[i]["proceso"]);
			newCell.appendChild(newText);


			var newCell  = newRow.insertCell(3);
			newCell.id="piezasformato"+dataResult[i]["id"];
			var newText  = document.createTextNode(dataResult[i]["piezasformato"]);
			newCell.appendChild(newText);


			var newCell  = newRow.insertCell(4);
			newCell.id="multiploproceso"+dataResult[i]["id"];
			var newText  = document.createTextNode(dataResult[i]["multiploproceso"]);
			newCell.appendChild(newText);

			var newCell  = newRow.insertCell(5);
			newCell.id="descripcion"+dataResult[i]["id"];
			var newText  = document.createTextNode(dataResult[i]["descripcion"]);
			newCell.appendChild(newText);

			var newCell  = newRow.insertCell(6);
			newCell.id="estandarhora"+dataResult[i]["id"];
			var newText  = document.createTextNode(dataResult[i]["estandarhora"]);
			newCell.appendChild(newText);

			var newCell  = newRow.insertCell(7);
			newCell.id="medidaentrada"+dataResult[i]["id"];
			var newText  = document.createTextNode(dataResult[i]["medidaentrada"]);
			newCell.appendChild(newText);


			var newCell  = newRow.insertCell(8);
			newCell.id="mermaajuste"+dataResult[i]["id"];
			newCell.style.display='none';
			var newText  = document.createTextNode(dataResult[i]["mermaajuste"]);
			newCell.appendChild(newText);


			var newCell  = newRow.insertCell(9);
			newCell.id="estandarajuste"+dataResult[i]["id"];
			newCell.style.display='none';
			var newText  = document.createTextNode(dataResult[i]["estandarajuste"]);
			newCell.appendChild(newText);



			/*var newCell  = newRow.insertCell(3);
			newCell.id="categoria"+dataResult[i]["id"];
			var newText  = document.createTextNode(dataResult[i]["categoria"]);
			newCell.appendChild(newText);

			var newCell  = newRow.insertCell(4);
			newCell.id="subensamble"+dataResult[i]["id"];
			var newText  = document.createTextNode(dataResult[i]["subensamble"]);
			newCell.appendChild(newText);*/

			var newCell  = newRow.insertCell(10);
			newCell.id="esp1"+dataResult[i]["id"];
			var newText  = document.createTextNode(dataResult[i]["esp1"]);
			newCell.appendChild(newText);

			var newCell  = newRow.insertCell(11);
			newCell.id="esp2"+dataResult[i]["id"];
			var newText  = document.createTextNode(dataResult[i]["esp2"]);
			newCell.appendChild(newText);


			var newCell  = newRow.insertCell(12);
			newCell.id="esp3"+dataResult[i]["id"];
			var newText  = document.createTextNode(dataResult[i]["esp3"]);
			newCell.appendChild(newText);



			var newCell  = newRow.insertCell(13);
			newCell.id="horas"+dataResult[i]["id"];
			newCell.style.display='none';
			var newText  = document.createTextNode(dataResult[i]["horas"]);
			newCell.appendChild(newText);

			var newCell  = newRow.insertCell(14);
			newCell.id="estandarturno"+dataResult[i]["id"];
			newCell.style.display='none';
			var newText  = document.createTextNode(dataResult[i]["estandarturno"]);
			newCell.appendChild(newText);


			



			/*var newCell  = newRow.insertCell(11);
			newCell.id="estandarcalculado"+dataResult[i]["id"];
			var newText  = document.createTextNode(dataResult[i]["estandarcalculado"]);
			newCell.appendChild(newText);*/





			/*var newCell  = newRow.insertCell(15);
			newCell.id="esp4"+dataResult[i]["id"];
			var newText  = document.createTextNode(dataResult[i]["esp4"]);
			newCell.appendChild(newText);


			var newCell  = newRow.insertCell(16);
			newCell.id="esp5"+dataResult[i]["id"];
			var newText  = document.createTextNode(dataResult[i]["esp5"]);
			newCell.appendChild(newText);*/

			var newCell  = newRow.insertCell(15);
			newCell.id="activo"+dataResult[i]["id"];
			var activo;

			if(dataResult[i]["activo"]==0){
				activo="No";
			}else if(dataResult[i]["activo"]==1){
				activo="Si";
			}
			var newText  = document.createTextNode(activo);
			newCell.appendChild(newText);
			

			newCell  = newRow.insertCell(16);
			var img = document.createElement('img');
   			img.src = "../img/editar.png";
			img.id="imgeditar"+dataResult[i]["id"];
			if(dataResult[i]["ordenes"]!=null){
				document.getElementById('agregarr').style.display="none";
				img.addEventListener('click', editarproceso.bind(null,dataResult[i]["id"],true));
				
			}else{
				document.getElementById('agregarr').style.display="block";
				img.addEventListener('click', editarproceso.bind(null,dataResult[i]["id"],false));
			}
			newCell.appendChild(img);
			

			newCell  = newRow.insertCell(17);
			var img2 = document.createElement('img');
   			img2.src = "../img/delete.png";
			if(dataResult[i]["ordenes"]!=null){
				img2.addEventListener('click', userdenied.bind(null,dataResult[i]["id"]));
			}else{
				img2.addEventListener('click', deleterecordproceso.bind(null,dataResult[i]["id"]));
			}
			newCell.appendChild(img2);


			
			//newRow.addEventListener('click', navigateToController.bind(null, dataResult[i]["usuario"]));
		}
		
		document.getElementById("sinpro").style.display="none";
		document.getElementById("tablaproceso").style.display="block";
		document.getElementById("sin2pro").style.display="block";
	
	}else{
		document.getElementById("sinpro").style.display="block";
		document.getElementById("tablaproceso").style.display="none";
		document.getElementById("sin2pro").style.display="none";
	
	}

}

function userdenied(str){
	alert("Este producto no puede ser modificado, ya que existe una orden en ejecuci\u00F3n. Intente m\u00E1s tarde. ");
}
		///////////////////////////////////FUNCTION FOR DELETE PROCESS BY PRODUCT SELECTED////////////////////////////////////////////

function deleterecordproceso(str){
	if(confirm("Esta apunto de eliminar este proceso, esta acci\u00F3n es irreversible. \u00BFSeguro que desea proceder?")){
		REQUEST('appControl', 'deleterecordproceso', [str] , onDeleteproceso, onerror);
	
	}

}

function onDeleteproceso(dataResult){
	if(dataResult[0]['exito'].localeCompare("si")==0)
	{	
		alert("\u00C9xito al eliminar el proceso");
		
	}
	else if(dataResult[0]['exito'].localeCompare("sin")==0)
	{
		alert("El proceso no ha sido encontrado");
	}else
	{
		alert("Error al eliminar los datos. Intente nuevamente");
	}	
getprocessbyproduct();
}

		////////////////////////////////////FUNCTIONS FOR ADD NEW PROCESS TO PRODUCT////////////////////////////////////////////////

function agregarprocesos(){
	document.getElementById("altaproceso").style.display="block";
	document.getElementById("tablaproceso").style.display="none";
	document.getElementById("agregarr").style.display="none";
	document.getElementById("sin2pro").style.display="none";
	REQUEST('appControl', 'getallmaquinas', [] , onMaquinas, onerror);
	
}
function cancelaraltaproceso(){
	document.getElementById("altaproceso").style.display="none";
	document.getElementById("tablaproceso").style.display="block";
	document.getElementById("agregarr").style.display="block";
	document.getElementById("sin2pro").style.display="block";
		document.getElementById("optionhrsturno").selectedIndex = "0";
		document.getElementById("optionmaquina").selectedIndex = "0";;
		document.getElementById("txtproceso").value="";
		document.getElementById("txtcategoria").value="";
		document.getElementById("txtsubensamble").value="";
		document.getElementById("txtdescripcionproceso").value="";
		document.getElementById("txtmedidaentrada").value="";
		document.getElementById("txtpiezasforma").value="";
		document.getElementById("txtmermaajuste").value="";
		document.getElementById("txtestandarproceso").value="";
		document.getElementById("txtestandarajuste").value="";
		//document.getElementById("txtestandarcalculado").value="";
		document.getElementById("txtespec1").value="";
		document.getElementById("txtespec2").value="";
		document.getElementById("txtespec3").value="";
		document.getElementById("txtespec4").value="";
		document.getElementById("txtespec5").value="";
		document.getElementById("txtmultiploproceso").value="1";
		document.getElementById("txtestandarturno").value="";
getprocessbyproduct();
}
function onMaquinas(dataResult){

	var op= document.getElementById("optionmaquina");
   		
	//var length =op.options.length;
	//for (i = 1; i < length; i++) {
  		//op.options[i] = null;
	//}
	op.innerHTML="";
	var option = document.createElement("option");
	    		option.value=0;
    			option.text ="---Seleccione---";
			
    			op.appendChild(option);
	if(dataResult.length>0){
		for(var a=0;a<dataResult.length;a++){
			option = document.createElement("option");
	    		option.value=dataResult[a]['id'];
    			option.text =dataResult[a]['nombre'];
			
    			op.appendChild(option);
		}
		
	}
	op.selectedIndex = "0";
categorias();
}
function categorias()
{
REQUEST('appControl', 'getcategorias', [] , onCategorias, onerror);

}
function onCategorias(dataResult)
{
	var dataList = document.getElementById('categorias');
	dataList.innerHTML="";
	for (var i = 0; i<dataResult.length; i++)
	{
    		var opt = document.createElement('option');
    		opt.value = dataResult[i]['nombre'];
		dataList.appendChild(opt);
    	}
subensambles();
}

function subensambles()
{
REQUEST('appControl', 'getsubensambles', [] , onSubensambles, onerror);

}
function onSubensambles(dataResult)
{
	var dataList = document.getElementById('subensambles');
	dataList.innerHTML="";
	for (var i = 0; i<dataResult.length; i++)
	{
    		var opt = document.createElement('option');
    		opt.value = dataResult[i]['nombre'];
		dataList.appendChild(opt);
    	}
bobinas();
}

function bobinas()
{
REQUEST('appControl', 'getbobinasenable', [] , onBobinas, onerror);

}
function onBobinas(dataResult)
{
	var dataList = document.getElementById('bobinas');
	dataList.innerHTML="";
	for (var i = 0; i<dataResult.length; i++)
	{
    		var opt = document.createElement('option');
    		opt.value = completecvesae(dataResult[i]['sae'])+"-"+dataResult[i]['nombre']+" "+dataResult[i]['ancho']+" mm "+dataResult[i]['puntos']+" pts "+dataResult[i]['gramos']+" gsm";
		dataList.appendChild(opt);
    	}
horasturno();
}


function horasturno()
{
REQUEST('appControl', 'gethorasturno', [] , onHorasturno, onerror);

}
function onHorasturno(dataResult)
{
	var op= document.getElementById("optionhrsturno");
   	//var length =op.options.length;
	//for (i = 1; i < length; i++) {
  		//op.options[i] = null;
	//}
	op.innerHTML="";
	var option = document.createElement("option");
	    		option.value=0;
    			option.text ="---Seleccione---";
			
    			op.appendChild(option);
	if(dataResult.length>0){
		for(var a=0;a<dataResult.length;a++){
			option = document.createElement("option");
	    		option.value=dataResult[a]['horas'];
    			option.text =dataResult[a]['horas'];
			
    			op.appendChild(option);
		}
		
	}
	op.selectedIndex = "0";
}



function getmaquinadetail(str){
	if(str>0){
		REQUEST('appControl', 'getmaquinadetail', [str,idproducto] , onMaquinadetail, onerror);
	}else{
		document.getElementById("txtproceso").value ="";
    		document.getElementById("txtestandarproceso").value ="";
		document.getElementById("txtmedidaentrada").value ="";
	}
}
function onMaquinadetail(dataResult){
	if(dataResult.length>0){
    		document.getElementById("txtproceso").value =dataResult[0]['proceso'];
    		document.getElementById("txtestandarproceso").value =dataResult[0]['estandar'];
		document.getElementById("txtmedidaentrada").value =dataResult[0]['um'];
		document.getElementById("txtestandarajuste").value= parseFloat(dataResult[0]['mermaproceso'])*100+"%";
		document.getElementById("txtmermaajuste").value=dataResult[0]['mermafija'];
		//document.getElementById("txtestandarcalculado").value="";
		document.getElementById("txtestandarturno").value="";
		//document.getElementById("optionhrsturno").selectedIndex = "0";

		if(dataResult[0]['proceso'].localeCompare("Hojeado")==0 && dataResult[0]['numeroprocesos'][0]['procesos']>0){
			alert("Este producto ya tiene asignado un proceso. El proceso de hojeado debe ser el primero.");
			document.getElementsByName("saveprocesos")[0].disabled=true;
			
		}else if(dataResult[0]['proceso'].localeCompare("Hojeado")==0 && dataResult[0]['numeroprocesos'][0]['procesos']==0){
			document.getElementById("bobinatype").style.display='block';
		}else{
			document.getElementsByName("saveprocesos")[0].disabled=false;
			document.getElementById("bobinatype").style.display='none';
		}
		/*if(dataResult[0]['medida'].toUpperCase().localeCompare("PLIEGOS")==0){
			document.getElementById("txtpiezasforma").disabled=false;
		}else{
			document.getElementById("txtpiezasforma").disabled=true;
			document.getElementById("txtpiezasforma").value=1;
		}*/
	calcularestandarturno();
	}

}

function calculaestandar(){
	var estandarp=document.getElementById("txtestandarproceso").value;
	var estandara=document.getElementById("txtestandarajuste").value;
	var estandarc=document.getElementById("txtestandarcalculado");
	if(estandarp.length>0){
		if(estandara.length>0){
			var porcentaje1=estandara.split("%");
			var porcentaje=parseFloat(porcentaje1)/parseFloat(100);
			var res=estandarp;//parseFloat(estandarp)-(parseFloat(estandarp)*parseFloat(porcentaje));
			estandarc.value=Math.round(res);
		}else{
			alert("Debe ingresar el porcentaje en Merma del Proceso");
		}
	}else{
		alert("Debe seleccionar primero una m\u00E1quina para realizar este c\u00E1lculo");
	}

}

function calcularestandarturno(){
	var estandarc=document.getElementById("txtestandarproceso").value;
	var hrs=document.getElementById("optionhrsturno").value;
	var estandart=document.getElementById("txtestandarturno");
	
	if(estandarc.length>0){
		if(hrs>0){
			estandart.value=parseFloat(hrs)*parseFloat(estandarc);
		}else{
			alert("Debe seleccionar un horario");
		}
	}else{
		alert("Debe dar clic en estandar calculado primero");
	}


}


function saveproceso(){
	var maquina=document.getElementById("optionmaquina").value;
	var proceso=document.getElementById("txtproceso").value;
	var categoria=document.getElementById("txtcategoria").value;
	var subensamble=document.getElementById("txtsubensamble").value;
	var descripcion=document.getElementById("txtdescripcionproceso").value;
	var medidaentrada=document.getElementById("txtmedidaentrada").value;
	var piezasforma=document.getElementById("txtpiezasforma").value;
	var mermaajuste=document.getElementById("txtmermaajuste").value;
	var estandarproceso=document.getElementById("txtestandarproceso").value;
	var estandarajuste=document.getElementById("txtestandarajuste").value;
	var estandarcalculado=estandarproceso;//document.getElementById("txtestandarcalculado").value;
	var espec1=document.getElementById("txtespecc1").value;
	var espec2=document.getElementById("txtespecc2").value;
	var espec3=document.getElementById("txtespecc3").value;
	var espec4="";//document.getElementById("txtespec4").value;
	var espec5="";//document.getElementById("txtespec5").value;
	var multiploproceso=document.getElementById("txtmultiploproceso").value;
	var horas=document.getElementById("optionhrsturno").value;
	var estandarturno=document.getElementById("txtestandarturno").value;
	var bob=document.getElementById("txtbobina").value;
	var bobina;

	if(proceso.length<=0 || estandarproceso<=0 || medidaentrada.length<=0 || estandarajuste.length<=0 || horas<=0 || estandarturno<=0){
		alert("Alg\u00FAn dato asociado a la m\u00E1quina no se ha encontrado. El sistema tratar\u00E1 de buscarlo autom\u00E1ticamente. Presione nuevamente el boton de guardar; si el error persiste contacte a su administrador"); 
		REQUEST('appControl', 'getmaquinadetail', [maquina,idproducto] , onMaquinadetail, onerror);
	}else{
		if(bob.length>0){
			var aux=bob.split("-");
			bobina=aux[0];
		}


		if(maquina==0){
			alert("Debe seleccionar una m\u00E1quina"); 
		}else if(descripcion.length==0){
			alert("Debe ingresar una descripci\u00F3n del proceso");
		}/*else if(estandarajuste.length==0){
			alert("Debe ingresar la Merma del Proceso");
		}else if(horas==0){
			alert("Debe seleccionar las horas del turno");
		}else if(estandarcalculado.length==0){
			alert("Debe dar clic en estandar calculado");
		}*/
		else if(multiploproceso<=0){
			alert("El multiplo proceso debe ser mayor a cero");
		}else if(piezasforma<=0){
			alert("Los Formatos/Sustrato deben ser mayor a cero");
		}else{
			REQUEST('appControl', 'saveproceso', [idproducto,maquina,proceso,categoria,subensamble,descripcion,medidaentrada,piezasforma,mermaajuste,estandarproceso,estandarajuste,estandarcalculado,espec1,espec2,espec3,espec4,espec5,multiploproceso,horas,estandarturno,bobina] , onSaveproceso, onerror);
		}
	}

}

function onSaveproceso(dataResult){
	
	if(dataResult[0]['exito'].localeCompare("si")==0)
	{
		alert("\u00C9xito al Guardar Datos("+dataResult[0]['folio']+")");
		document.getElementById("altaproceso").style.display="none";
		document.getElementById("tablaproceso").style.display="block";
		document.getElementById("agregarr").style.display="block";
		document.getElementById("optionhrsturno").selectedIndex = "0";
		document.getElementById("optionmaquina").selectedIndex = "0";;
		document.getElementById("txtproceso").value="";
		document.getElementById("txtcategoria").value="EE";
		document.getElementById("txtsubensamble").value="Pliego";
		document.getElementById("txtdescripcionproceso").value="";
		document.getElementById("txtmedidaentrada").value="";
		document.getElementById("txtpiezasforma").value="";
		document.getElementById("txtmermaajuste").value="";
		document.getElementById("txtestandarproceso").value="";
		document.getElementById("txtestandarajuste").value="";
		//document.getElementById("txtestandarcalculado").value="";
		document.getElementById("txtespecc1").value="";
		document.getElementById("txtespecc2").value="";
		document.getElementById("txtespecc3").value="";
		//document.getElementById("txtespec4").value="";
		//document.getElementById("txtespec5").value="";
		document.getElementById("txtmultiploproceso").value="1";
		document.getElementById("txtestandarturno").value="";
	}
	else if(dataResult[0]['exito'].localeCompare("ya")==0)
	{
		alert("El proceso ya ha sido dado de alta");
	}
	else if(dataResult[0]['exito'].localeCompare("no")==0)
	{	
		alert("Error al Guardar Datos. Intente nuevamente");
	}
getprocessbyproduct();
}

		////////////////////////////////////FUNCTIONS FOR UPDATE PROCESS ////////////////////////////////////////////////////////

function editarproceso(str,calidad){
	idprocesoeditar=str;
	document.getElementById("editaproceso").style.display="block";
	document.getElementById("tablaproceso").style.display="none";
	document.getElementById("agregarr").style.display="none";
	document.getElementById("sin2pro").style.display="none";

	var row=document.getElementById("proceso-"+str);
	var cells = row.getElementsByTagName('td');
	//cells[0].innerText;


	if(calidad){
		//document.getElementById("optionmaquinaedita").value=cells[1].innerText;
		document.getElementById("txtprocesoedita").value=cells[2].innerText;
		document.getElementById("txtcategoriaedita").value="EE";
		document.getElementById("txtsubensambleedita").value="Pliego";
		document.getElementById("txtdescripcionprocesoedita").value=cells[5].innerText;
		document.getElementById("txtmedidaentradaedita").value=cells[7].innerText;
		document.getElementById("txtpiezasformaedita").value=cells[3].innerText;
		document.getElementById("txtmermaajusteedita").value=cells[8].innerText;
		document.getElementById("txtestandarprocesoedita").value=cells[6].innerText;
		document.getElementById("txtestandarajusteedita").value=cells[9].innerText;
		//document.getElementById("txtestandarcalculadoedita").value=cells[11].innerText;
		document.getElementById("txtespec1edita").value=cells[10].innerText;
		document.getElementById("txtespec2edita").value=cells[11].innerText;
		document.getElementById("txtespec3edita").value=cells[12].innerText;
		//document.getElementById("txtespec4edita").value=cells[15].innerText;
		//document.getElementById("txtespec5edita").value=cells[16].innerText;
		document.getElementById("txtmultiploprocesoedita").value=cells[4].innerText;
		document.getElementById("optionhrsturnoedita").value=cells[13].innerText;
		document.getElementById("txtestandarturnoedita").value=cells[14].innerText;



		document.getElementById("optionmaquinaedita").disabled=true;
		document.getElementById("txtprocesoedita").disabled=true;
		document.getElementById("txtcategoriaedita").disabled=true;
		document.getElementById("txtsubensambleedita").disabled=true;
		document.getElementById("txtdescripcionprocesoedita").disabled=true;
		document.getElementById("txtmedidaentradaedita").disabled=true;
		document.getElementById("txtpiezasformaedita").disabled=true;
		document.getElementById("txtmermaajusteedita").disabled=true;
		document.getElementById("txtestandarprocesoedita").disabled=true;
		document.getElementById("txtestandarajusteedita").disabled=true;
		//document.getElementById("txtestandarcalculadoedita").disabled=true;
		document.getElementById("txtespec1edita").disabled=false;
		document.getElementById("txtespec2edita").disabled=false;
		document.getElementById("txtespec3edita").disabled=false;
		//document.getElementById("txtespec4edita").disabled=true;
		//document.getElementById("txtespec5edita").disabled=true;
		document.getElementById("txtmultiploprocesoedita").disabled=true;
		document.getElementById("optionhrsturnoedita").disabled=true;
		document.getElementById("txtestandarturnoedita").disabled=true;
		document.getElementById("optionactivoprocess").disabled=true;
		if(cells[15].innerText.localeCompare("Si")==0){
			document.getElementById("optionactivoprocess").selectedIndex="0";
		}if(cells[15].innerText.localeCompare("No")==0)	{
			document.getElementById("optionactivoprocess").selectedIndex="1";
		}

	}else{
		//document.getElementById("optionmaquinaedita").value=cells[1].innerText;
		document.getElementById("txtprocesoedita").value=cells[2].innerText;
		document.getElementById("txtcategoriaedita").value="EE";
		document.getElementById("txtsubensambleedita").value="Pliego";
		document.getElementById("txtdescripcionprocesoedita").value=cells[5].innerText;
		document.getElementById("txtmedidaentradaedita").value=cells[7].innerText;
		document.getElementById("txtpiezasformaedita").value=cells[3].innerText;
		document.getElementById("txtmermaajusteedita").value=cells[8].innerText;
		document.getElementById("txtestandarprocesoedita").value=cells[6].innerText;
		document.getElementById("txtestandarajusteedita").value=cells[9].innerText;
		//document.getElementById("txtestandarcalculadoedita").value=cells[11].innerText;
		document.getElementById("txtespec1edita").value=cells[10].innerText;
		document.getElementById("txtespec2edita").value=cells[11].innerText;
		document.getElementById("txtespec3edita").value=cells[12].innerText;
		//document.getElementById("txtespec4edita").value=cells[15].innerText;
		//document.getElementById("txtespec5edita").value=cells[16].innerText;
		document.getElementById("txtmultiploprocesoedita").value=cells[4].innerText;
		document.getElementById("optionhrsturnoedita").value=cells[13].innerText;
		document.getElementById("txtestandarturnoedita").value=cells[14].innerText;



		document.getElementById("optionmaquinaedita").disabled=false;
		document.getElementById("txtprocesoedita").disabled=true;
		document.getElementById("txtcategoriaedita").disabled=false;
		document.getElementById("txtsubensambleedita").disabled=false;
		document.getElementById("txtdescripcionprocesoedita").disabled=false;
		document.getElementById("txtmedidaentradaedita").disabled=true;
		document.getElementById("txtpiezasformaedita").disabled=false;
		document.getElementById("txtmermaajusteedita").disabled=false;
		document.getElementById("txtestandarprocesoedita").disabled=true;
		document.getElementById("txtestandarajusteedita").disabled=false;
		//document.getElementById("txtestandarcalculadoedita").disabled=false;
		document.getElementById("txtespec1edita").disabled=false;
		document.getElementById("txtespec2edita").disabled=false;
		document.getElementById("txtespec3edita").disabled=false;
		//document.getElementById("txtespec4edita").disabled=false;
		//document.getElementById("txtespec5edita").disabled=false;
		document.getElementById("txtmultiploprocesoedita").disabled=false;
		document.getElementById("optionhrsturnoedita").disabled=false;
		document.getElementById("txtestandarturnoedita").disabled=false;
		document.getElementById("optionactivoprocess").disabled=false;


		if(cells[15].innerText.localeCompare("Si")==0){
			document.getElementById("optionactivoprocess").selectedIndex="0";
		}if(cells[15].innerText.localeCompare("No")==0)	{
			document.getElementById("optionactivoprocess").selectedIndex="1";
		}

	}
	
	
	REQUEST('appControl', 'getallmaquinas', [] , onMaquinas2, onerror);
}
function cancelareditaproceso(){
	document.getElementById("editaproceso").style.display="none";
	document.getElementById("tablaproceso").style.display="block";
	document.getElementById("agregarr").style.display="block";
	document.getElementById("sin2pro").style.display="block";
		document.getElementById("optionhrsturnoedita").selectedIndex = "0";
		document.getElementById("optionmaquinaedita").selectedIndex = "0";;
		document.getElementById("txtprocesoedita").value="";
		document.getElementById("txtcategoriaedita").value="";
		document.getElementById("txtsubensambleedita").value="";
		document.getElementById("txtdescripcionprocesoedita").value="";
		document.getElementById("txtmedidaentradaedita").value="";
		document.getElementById("txtpiezasformaedita").value="";
		document.getElementById("txtmermaajusteedita").value="";
		document.getElementById("txtestandarprocesoedita").value="";
		document.getElementById("txtestandarajusteedita").value="";
		document.getElementById("txtestandarcalculadoedita").value="";
		document.getElementById("txtespec1edita").value="";
		document.getElementById("txtespec2edita").value="";
		document.getElementById("txtespec3edita").value="";
		//document.getElementById("txtespec4edita").value="";
		//document.getElementById("txtespec5edita").value="";
		document.getElementById("txtmultiploprocesoedita").value="";
		document.getElementById("txtestandarturnoedita").value="";

}

function onMaquinas2(dataResult){
	
	var row=document.getElementById("proceso-"+idprocesoeditar);
	var cells = row.getElementsByTagName('td');
	
	

	var op= document.getElementById("optionmaquinaedita");
   		
	
	op.innerHTML="";
	var option = document.createElement("option");
	    		option.value=0;
    			option.text ="---Seleccione---";
			
    			op.appendChild(option);
	if(dataResult.length>0){
		for(var a=0;a<dataResult.length;a++){
			option = document.createElement("option");
	    		option.value=dataResult[a]['id'];
    			option.text =dataResult[a]['nombre'];
			if(dataResult[a]['nombre'].localeCompare(cells[1].innerText)==0){
				option.selected=true;
			}
			
    			op.appendChild(option);
		}
		
	}

categorias2();
}


function categorias2()
{
REQUEST('appControl', 'getcategorias', [] , onCategorias2, onerror);

}
function onCategorias2(dataResult)
{
	var dataList = document.getElementById('categorias');
	dataList.innerHTML="";
	for (var i = 0; i<dataResult.length; i++)
	{
    		var opt = document.createElement('option');
    		opt.value = dataResult[i]['nombre'];
		dataList.appendChild(opt);
    	}
subensambles2();
}

function subensambles2()
{
REQUEST('appControl', 'getsubensambles', [] , onSubensambles2, onerror);

}
function onSubensambles2(dataResult)
{
	var dataList = document.getElementById('subensambles');
	dataList.innerHTML="";
	for (var i = 0; i<dataResult.length; i++)
	{
    		var opt = document.createElement('option');
    		opt.value = dataResult[i]['nombre'];
		dataList.appendChild(opt);
    	}
horasturno2();
}

function horasturno2()
{
REQUEST('appControl', 'gethorasturno', [] , onHorasturno2, onerror);

}
function onHorasturno2(dataResult)
{
	
	var row=document.getElementById("proceso-"+idprocesoeditar);
	var cells = row.getElementsByTagName('td');
	
	var op= document.getElementById("optionhrsturnoedita");

	op.innerHTML="";
	var option = document.createElement("option");
	    		option.value=0;
    			option.text ="---Seleccione---";
			
    			op.appendChild(option);
	if(dataResult.length>0){
		for(var a=0;a<dataResult.length;a++){
			option = document.createElement("option");
	    		option.value=dataResult[a]['horas'];
    			option.text =dataResult[a]['horas'];
			if(dataResult[a]['horas']==cells[13].innerText){
				option.selected=true;
			}
    			op.appendChild(option);
		}
		
	}
bobinas();	
}



function getmaquinadetailedita(str){
	if(str>0){
		REQUEST('appControl', 'getmaquinadetail', [str,idproducto] , onMaquinadetail2, onerror);
	}else{
		document.getElementById("txtprocesoedita").value ="";
    		document.getElementById("txtestandarprocesoedita").value ="";
		document.getElementById("txtmedidaentradaedita").value ="";
	}
}
function onMaquinadetail2(dataResult){
	if(dataResult.length>0){
    		document.getElementById("txtprocesoedita").value =dataResult[0]['proceso'];
    		document.getElementById("txtestandarprocesoedita").value =dataResult[0]['estandar'];
		document.getElementById("txtmedidaentradaedita").value =dataResult[0]['um'];
		document.getElementById("txtmermaajusteedita").value=dataResult[0]['mermafija'];
		//document.getElementById("txtestandarcalculadoedita").value="";
		document.getElementById("txtestandarturnoedita").value="";
		document.getElementById("optionhrsturnoedita").selectedIndex = "0";
		document.getElementById("txtestandarajusteedita").value= parseFloat(dataResult[0]['mermaproceso'])*100+"%";
		if(dataResult[0]['proceso'].localeCompare("Hojeado")==0 && dataResult[0]['numeroprocesos'][0]['procesos']>0){
			alert("Este producto ya tiene asignado un proceso. El proceso de hojeado debe ser el primero.");
			document.getElementsByName("saveprocesos")[0].disabled=true;
			
		}else if(dataResult[0]['proceso'].localeCompare("Hojeado")==0 && dataResult[0]['numeroprocesos'][0]['procesos']==0){
			document.getElementById("bobinatype2").style.display='block';
		}else{
			document.getElementsByName("saveprocesos")[0].disabled=false;
			document.getElementById("bobinatype2").style.display='none';
		}
		/*if(dataResult[0]['medida'].toUpperCase().localeCompare("PLIEGOS")==0){
			document.getElementById("txtpiezasformaedita").disabled=false;
		}else{
			document.getElementById("txtpiezasformaedita").disabled=true;
			document.getElementById("txtpiezasformaedita").value=1;
		}*/
	}
}


function calculaestandaredita(){
	var estandarp=document.getElementById("txtestandarprocesoedita").value;
	var estandara=document.getElementById("txtestandarajusteedita").value;
	var estandarc=document.getElementById("txtestandarcalculadoedita");
	if(estandarp.length>0){
		if(estandara.length>0){
			var porcentaje1=estandara.split("%");
			var porcentaje=parseFloat(porcentaje1)/parseFloat(100);
			var res=estandarp;//parseFloat(estandarp)-(parseFloat(estandarp)*parseFloat(porcentaje));
			estandarc.value=Math.round(res);
		}else{
			alert("Debe ingresar el porcentaje en Merma del Proceso");
		}
	}else{
		alert("Debe seleccionar primero una m\u00E1quina para realizar este c\u00E1lculo");
	}

}

function calcularestandarturnoedita(){
	var estandarc=document.getElementById("txtestandarprocesoedita").value;
	var hrs=document.getElementById("optionhrsturnoedita").value;
	var estandart=document.getElementById("txtestandarturnoedita");
	
	if(estandarc.length>0){
		if(hrs>0){
			estandart.value=parseFloat(hrs)*parseFloat(estandarc);
		}else{
			alert("Debe seleccionar un horario");
		}
	}else{
		alert("Debe dar clic en estandar calculado primero");
	}


}

function saveprocesoedita(){
	var maquina=document.getElementById("optionmaquinaedita").value;
	var proceso=document.getElementById("txtprocesoedita").value;
	var categoria=document.getElementById("txtcategoriaedita").value;
	var subensamble=document.getElementById("txtsubensambleedita").value;
	var descripcion=document.getElementById("txtdescripcionprocesoedita").value;
	var medidaentrada=document.getElementById("txtmedidaentradaedita").value;
	var piezasforma=document.getElementById("txtpiezasformaedita").value;
	var mermaajuste=document.getElementById("txtmermaajusteedita").value;
	var estandarproceso=document.getElementById("txtestandarprocesoedita").value;
	var estandarajuste=document.getElementById("txtestandarajusteedita").value;
	var estandarcalculado=estandarproceso;//document.getElementById("txtestandarcalculadoedita").value;
	var espec1=document.getElementById("txtespec1edita").value;
	var espec2=document.getElementById("txtespec2edita").value;
	var espec3=document.getElementById("txtespec3edita").value;
	var espec4="";//document.getElementById("txtespec4edita").value;
	var espec5="";//document.getElementById("txtespec5edita").value;
	var multiploproceso=document.getElementById("txtmultiploprocesoedita").value;
	var horas=document.getElementById("optionhrsturnoedita").value;
	var estandarturno=document.getElementById("txtestandarturnoedita").value;
	var activo=document.getElementById("optionactivoprocess").value;
	var bob=document.getElementById("txtbobina").value;
	var bobina;
	if(bob.length>0){
		var aux=bob.split("-");
		bobina=aux[0];
	}
	if(maquina==0){
		alert("Debe seleccionar una m\u00E1quina"); 
	}else if(descripcion.length==0){
		alert("Debe ingresar una descripci\u00F3n para el proceso");
	}/*else if(estandarajuste.length==0){
		alert("Debe ingresar la Merma del Proceso");
	}else if(horas==0){
		alert("Debe seleccionar las horas del turno");
	}else if(estandarcalculado.length==0){
		alert("Debe dar clic en estandar calculado");
	}else if(piezasforma<=0){
		alert("Los Formatos/Sustrato deben ser mayor a cero");
	}*/else{
		REQUEST('appControl', 'saveprocesoedita', [idprocesoeditar,maquina,proceso,categoria,subensamble,descripcion,medidaentrada,piezasforma,mermaajuste,estandarproceso,estandarajuste,estandarcalculado,espec1,espec2,espec3,espec4,espec5,multiploproceso,horas,estandarturno,activo,bobina] , onSaveprocesoedita, onerror);
	}

}

function onSaveprocesoedita(dataResult){
	if(dataResult.length>0){
		if(dataResult[0]['exito'].localeCompare("si")==0)
		{		
			alert("\u00C9xito al modificar los datos");
			document.getElementById("editaproceso").style.display="none";
			document.getElementById("tablaproceso").style.display="block";
			document.getElementById("agregarr").style.display="block";
			document.getElementById("optionhrsturnoedita").selectedIndex = "0";
			document.getElementById("optionmaquinaedita").selectedIndex = "0";;
			document.getElementById("txtprocesoedita").value="";
			document.getElementById("txtcategoriaedita").value="";
			document.getElementById("txtsubensambleedita").value="";
			document.getElementById("txtdescripcionprocesoedita").value="";
			document.getElementById("txtmedidaentradaedita").value="";
			document.getElementById("txtpiezasformaedita").value="";
			document.getElementById("txtmermaajusteedita").value="";
			document.getElementById("txtestandarprocesoedita").value="";
			document.getElementById("txtestandarajusteedita").value="";
			//document.getElementById("txtestandarcalculadoedita").value="";
			document.getElementById("txtespec1edita").value="";
			document.getElementById("txtespec2edita").value="";
			document.getElementById("txtespec3edita").value="";
			//document.getElementById("txtespec4edita").value="";
			//document.getElementById("txtespec5edita").value="";
			document.getElementById("txtmultiploprocesoedita").value="";
			document.getElementById("txtestandarturnoedita").value="";
		}
		else if(dataResult[0]['exito'].localeCompare("sin")==0)
		{
			alert("El proceso no ha sido encontrado");
		}
	}else
	{
		alert("Error al modificar los datos. Intente nuevamente");
	}
	
	
getprocessbyproduct();
}


////////////////////////////////////////////////////FUNCTIONS FOR ESPECIFICATIONS P.T BY PRODUCT/////////////////////////////////////////////////////////////////////////////



function especificacionespt(str){
	idproducto=str;
	var row=document.getElementById('pp'+str);
	var cells = row.getElementsByTagName('td');

	// Get the modal

	document.getElementById('compodructopt').innerText=cells[0].innerText+"-"+cells[1].innerText;

	var modal = document.getElementById('myModalespecificacionespt');


	// Get the <span> element that closes the modal
	var span = document.getElementById("closeespecificacionespt");
  	modal.style.display = "block";

	// When the user clicks on <span> (x), close the modal
	span.onclick = function() {
	 		modal.style.display = "none";
	}
	document.getElementById("editaespecificacionpt").style.display="none";
	document.getElementById("altaespecificacionpt").style.display="none";
	document.getElementById("tablaespecificacionpt").style.display="block";
	document.getElementById("agregarespecificacionpt").style.display="block";
	document.getElementById("sin2pt").style.display="block";
	getspecificationsbyproduct();
	
}
////////////////////////////////FUNCTIONS FOR ADD NEW ESPECIFICATION////////////////////////////////////////////////////////////

function agregarespecificacionpt(){
	REQUEST('appControl', 'getnumberespecificaionpt', [idproducto] , onEspecificacionesptnumber, onerror);

}
function  onEspecificacionesptnumber(dataResult){
	if(dataResult.length>=0){
		if(dataResult[0]['total']<3){
			agregarespecificacionpt2();
		}else{
			alert("Se ha alcanzado el n\u00FAmero m\u00E1ximo de especificaciones");
		}
	}else{
		agregarespecificacionpt2();
	}
} 

function agregarespecificacionpt2(){
	document.getElementById("altaespecificacionpt").style.display="block";
	document.getElementById("tablaespecificacionpt").style.display="none";
	document.getElementById("agregarespecificacionpt").style.display="none";
	document.getElementById("sin2pt").style.display="none";
	document.getElementById("sinespecificacionpt").style.display="none";
	document.getElementById("txtespecificacionpt").value= "";
	
}



function cancelaraltaespecificacionpt(){
	document.getElementById("altaespecificacionpt").style.display="none";
	document.getElementById("tablaespecificacionpt").style.display="block";
	document.getElementById("agregarespecificacionpt").style.display="block";
	document.getElementById("sin2pt").style.display="block";
	document.getElementById("txtespecificacionpt").value= "";
	
getspecificationsbyproduct()
}


function getspecificationsbyproduct(){
document.getElementById("imgloadpt").style.display="block";
REQUEST('appControl', 'getespecificacionesptproducto', [idproducto] , onEspecificacionesptproducto, onerror);

}
function  onEspecificacionesptproducto(dataResult){
	document.getElementById("imgloadpt").style.display="none";
	myDeleteFunctionpt();
		if(dataResult.length>0){
			

		for (var i = 0; i<dataResult.length; i++)
		{
			var tableRef = document.getElementById('resultadoespecificacionpt').getElementsByTagName('tbody')[0];;
			// Insert a row in the table at the last row
			var newRow   = tableRef.insertRow(tableRef.rows.length);
			newRow.className = "table-row";
			newRow.id="especificaionpt-"+dataResult[i]["id"];
			// Insert a cell in the row at index 0

			var newCell  = newRow.insertCell(0);
			newCell.id="id"+dataResult[i]["id"];
			var newText  = document.createTextNode(dataResult[i]["id"]);
			newCell.appendChild(newText);

			var newCell  = newRow.insertCell(1);
			newCell.id="nombre"+dataResult[i]["id"];
			var newText  = document.createTextNode(dataResult[i]["nombre"]);
			newCell.appendChild(newText);

			

			var newCell  = newRow.insertCell(2);
			newCell.id="activo"+dataResult[i]["id"];
			var activo='';
			if(dataResult[i]["activo"]==0){
				activo='No';
			}else if(dataResult[i]["activo"]==1){
				activo='Si';
			}
			var newText  = document.createTextNode(activo);
			newCell.appendChild(newText);		

			newCell  = newRow.insertCell(3);
			var img = document.createElement('img');
   			img.src = "../img/editar.png";
			img.id="imgeditar"+dataResult[i]["id"];
			img.addEventListener('click', editarespecificacionpt.bind(null,dataResult[i]["id"]));
			newCell.appendChild(img);
			

			newCell  = newRow.insertCell(4);
			var img2 = document.createElement('img');
   			img2.src = "../img/delete.png";
			img2.addEventListener('click', deleterecordespecificacionpt.bind(null,dataResult[i]["id"]));
			newCell.appendChild(img2);


			
			//newRow.addEventListener('click', navigateToController.bind(null, dataResult[i]["usuario"]));
		}
		
		document.getElementById("sinespecificacionpt").style.display="none";
		document.getElementById("tablaespecificacionpt").style.display="block";
		document.getElementById("sin2pt").style.display="block";
	
	}else{
		document.getElementById("sinespecificacionpt").style.display="block";
		document.getElementById("tablaespecificacionpt").style.display="none";
		document.getElementById("sin2pt").style.display="none";
	
	}

}
function saveespecificacionpt(){
	var especificacion=document.getElementById("txtespecificacionpt").value;
	
	if(especificacion.length<=0){
		alert("La especificaci\u00F3n no puede estar vacia");
	}else{
		REQUEST('appControl', 'saveespecificaionpt', [idproducto,especificacion] , onSaveespecificacionpt, onerror);
	}

}

function onSaveespecificacionpt(dataResult){
	
	if(dataResult[0]['exito'].localeCompare("si")==0)
	{
		alert("\u00C9xito al Guardar Datos("+dataResult[0]['folio']+")");
		document.getElementById("altaespecificacionpt").style.display="none";
		document.getElementById("tablaespecificacionpt").style.display="block";
		document.getElementById("agregarespecificacionpt").style.display="block";
		document.getElementById("sin2pt").style.display="block";
		document.getElementById("txtespecificacionpt").value= "";
		
	}
	else if(dataResult[0]['exito'].localeCompare("ya")==0)
	{
		alert("Los datos ya ha sido dado de alta");
	}
	else if(dataResult[0]['exito'].localeCompare("no")==0)
	{	
		alert("Error al Guardar Datos. Intente nuevamente");
	}
getspecificationsbyproduct();
}

		/////////////////////////////////////FUNCTIONS FOR EDIT COMPONENTS/////////////////////////////////////////////////

function editarespecificacionpt(str){
	idespecificacionpt=str;
	document.getElementById("editaespecificacionpt").style.display="block";
	document.getElementById("tablaespecificacionpt").style.display="none";
	document.getElementById("agregarespecificacionpt").style.display="none";
	document.getElementById("sin2pt").style.display="none";

	var row=document.getElementById("especificaionpt-"+str);
	var cells = row.getElementsByTagName('td');
	//cells[0].innerText;
	if(cells[2].innerText.localeCompare("Si")==0){
		document.getElementById("optionactivoespecificacionpt").selectedIndex = "0";
	}else if(cells[2].innerText.localeCompare("No")==0){
		document.getElementById("optionactivoespecificacionpt").selectedIndex = "1";
	}
	document.getElementById("txtespecificacionptedita").value= cells[1].innerText;
	
	
}

function cancelareditaespecificacionpt(){
	document.getElementById("editaespecificacionpt").style.display="none";
	document.getElementById("tablaespecificacionpt").style.display="block";
	document.getElementById("agregarespecificacionpt").style.display="block";
	document.getElementById("sin2pt").style.display="block";
		
	document.getElementById("txtespecificacionptedita").value= "";
	
		
getspecificationsbyproduct();
}


function saveespecificacionptedita(){
	var esppt=document.getElementById("txtespecificacionptedita").value;
	var activo=document.getElementById("optionactivoespecificacionpt").value;
	

	if(esppt.length<=0){
		alert("La especificaci\u00F3n no puede estar vacia");
	}else{
		REQUEST('appControl', 'saveespecificacionptedita', [idespecificacionpt,esppt,activo] , onSaveespecificacionptedita, onerror);
	}
}
function onSaveespecificacionptedita(dataResult){
	if(dataResult.length>0){
		if(dataResult[0]['exito'].localeCompare("si")==0)
		{		
			alert("\u00C9xito al modificar los datos");
			document.getElementById("editaespecificacionpt").style.display="none";
			document.getElementById("tablaespecificacionpt").style.display="block";
			document.getElementById("agregarespecificacionpt").style.display="block";
			document.getElementById("txtespecificacionptedita").value= "";
			
		}else if(dataResult[0]['exito'].localeCompare("sin")==0)
		{
			alert("Los datos no ha sido encontrados");
		}
	}else
	{
		alert("Error al modificar los datos. Intente nuevamente");
	}
	
	
getspecificationsbyproduct();
}

		////////////////////////////////////FUNCTIONS FOR DELETE SPECIFICATION///////////////////////////////////////////////////////////////

function deleterecordespecificacionpt(str){
	if(confirm("Esta apunto de eliminar estos datos, esta acci\u00F3n es irreversible. \u00BFSeguro que desea proceder?")){
		REQUEST('appControl', 'deleterecordespecificacionpt', [str] , onDeleteespecificacionpt, onerror);
	
	}

}

function onDeleteespecificacionpt(dataResult){
	if(dataResult[0]['exito'].localeCompare("si")==0)
	{	
		alert("\u00C9xito al eliminar los datos");
		
	}
	else if(dataResult[0]['exito'].localeCompare("sin")==0)
	{
		alert("Los datos no han sido encontrados");
	}else
	{
		alert("Error al eliminar los datos. Intente nuevamente");
	}	
getspecificationsbyproduct();
}


////////////////////////////////////////////////////FUNCTIONS FOR COMPONENTS BY PRODUCT/////////////////////////////////////////////////////////////////////////////
		

		///////////////////////////////////FUNCTION FOR GET COMPONENTS BY PRODUCT SELECTED////////////////////////////////////////////

function componentes(str){
	idproducto=str;
	var row=document.getElementById('pp'+str);
	var cells = row.getElementsByTagName('td');

	// Get the modal

	document.getElementById('compodructo').innerText=cells[0].innerText+"-"+cells[1].innerText;

	var modal = document.getElementById('myModalcomponentes');


	// Get the <span> element that closes the modal
	var span = document.getElementById("closecomponentes");
  	modal.style.display = "block";

	// When the user clicks on <span> (x), close the modal
	span.onclick = function() {
	 		modal.style.display = "none";
	}
	document.getElementById("editacomponente").style.display="none";
	document.getElementById("altacomponente").style.display="none";
	document.getElementById("tablacomponente").style.display="block";
	document.getElementById("agregarcomponente").style.display="block";
	document.getElementById("sin2com").style.display="block";
	document.getElementById("txtcodigosae").value= "";
	document.getElementById("txtlinea").value= "";
	document.getElementById("txtidcomponente").value= "";
	document.getElementById("txtum").value= "";
	document.getElementById("txtultimocosto").value= "";
	document.getElementById("txtcantidad").value="";
	getcomponentsbyproduct();
}

function getcomponentsbyproduct(){
document.getElementById("imgloadcom").style.display="block";
REQUEST('appControl', 'getcomponentesproducto', [idproducto] , onComponentesproducto, onerror);

}
function  onComponentesproducto(dataResult){
	document.getElementById("imgloadcom").style.display="none";
	myDeleteFunctioncom() ;
		if(dataResult.length>0){
			

		for (var i = 0; i<dataResult.length; i++)
		{
			var tableRef = document.getElementById('resultadocom').getElementsByTagName('tbody')[0];;
			// Insert a row in the table at the last row
			var newRow   = tableRef.insertRow(tableRef.rows.length);
			newRow.className = "table-row";
			newRow.id="componente-"+dataResult[i]["id"];
			// Insert a cell in the row at index 0

			var newCell  = newRow.insertCell(0);
			newCell.id="clave"+dataResult[i]["id"];
			var newText  = document.createTextNode(completecvesae(dataResult[i]["clave"]));
			newCell.appendChild(newText);

			var newCell  = newRow.insertCell(1);
			newCell.id="codigo"+dataResult[i]["id"];
			var newText  = document.createTextNode(dataResult[i]["componente"]);
			newCell.appendChild(newText);

			

			var newCell  = newRow.insertCell(2);
			newCell.id="linea"+dataResult[i]["id"];
			var newText  = document.createTextNode(dataResult[i]["linea"]);
			newCell.appendChild(newText);

			var newCell  = newRow.insertCell(3);
			newCell.id="cantidad"+dataResult[i]["id"];
			var newText  = document.createTextNode(dataResult[i]["cantidad"]);
			newCell.appendChild(newText);

			var newCell  = newRow.insertCell(4);
			newCell.id="um"+dataResult[i]["id"];
			var newText  = document.createTextNode(dataResult[i]["unidadEntrada"]);
			newCell.appendChild(newText);

			/*var newCell  = newRow.insertCell(4);
			newCell.id="ultimocosto"+dataResult[i]["id"];
			var newText  = document.createTextNode(dataResult[i]["ultimoCosto"]);
			newCell.appendChild(newText);*/

			
		

			newCell  = newRow.insertCell(5);
			var img = document.createElement('img');
   			img.src = "../img/editar.png";
			img.id="imgeditar"+dataResult[i]["id"];
			img.addEventListener('click', editarcomponente.bind(null,dataResult[i]["id"],dataResult[i]["idcomponente"]));
			newCell.appendChild(img);
			

			newCell  = newRow.insertCell(6);
			var img2 = document.createElement('img');
   			img2.src = "../img/delete.png";
			img2.addEventListener('click', deleterecordcomponente.bind(null,dataResult[i]["id"]));
			newCell.appendChild(img2);


			
			//newRow.addEventListener('click', navigateToController.bind(null, dataResult[i]["usuario"]));
		}
		
		document.getElementById("sincom").style.display="none";
		document.getElementById("tablacomponente").style.display="block";
		document.getElementById("sin2com").style.display="block";
	
	}else{
		document.getElementById("sincom").style.display="block";
		document.getElementById("tablacomponente").style.display="none";
		document.getElementById("sin2com").style.display="none";
	
	}

}
		////////////////////////////////FUNCTIONS FOR ADD NEW COMPONENTS////////////////////////////////////////////////////////////

function agregarcomponentes(){
	document.getElementById("altacomponente").style.display="block";
	document.getElementById("tablacomponente").style.display="none";
	document.getElementById("agregarcomponente").style.display="none";
	document.getElementById("sin2com").style.display="none";
	document.getElementById("sincom").style.display="none";
	document.getElementById("txtcodigosae").value= "";
	document.getElementById("txtlinea").value= "";
	document.getElementById("txtidcomponente").value= "";
	document.getElementById("txtum").value= "";
	document.getElementById("txtultimocosto").value= "";
	document.getElementById("txtcantidad").value="";
	document.getElementById("txtcomponente").value =""; 
	componentess();
	
}
function cancelaraltacomponente(){
	document.getElementById("altacomponente").style.display="none";
	document.getElementById("tablacomponente").style.display="block";
	document.getElementById("agregarcomponente").style.display="block";
	document.getElementById("sin2com").style.display="block";
	document.getElementById("txtcodigosae").value= "";
	document.getElementById("txtlinea").value= "";
	document.getElementById("txtidcomponente").value= "";
	document.getElementById("txtum").value= "";
	document.getElementById("txtultimocosto").value= "";
	document.getElementById("txtcantidad").value="";
getcomponentsbyproduct();
}

		///////////////////////GET LIS OF ALL COMPONENTS IN DB/////////////////////

function componentess()
{
REQUEST('appControl', 'getcomponentes', [] , onComponentes, onerror);

}
function onComponentes(dataResult)
{
	var dataList = document.getElementById('componentes');
	dataList.innerHTML="";
	for (var i = 0; i<dataResult.length; i++)
	{
    		var opt = document.createElement('option');
		var cve=dataResult[i]['clave'].toString();
		
		
    		opt.value = completecvesae(cve)+"-"+dataResult[i]['componente'];
		dataList.appendChild(opt);
    	}

}



function getspecificcomponente(str)
{	var cod=document.getElementById("txtcodigosae").value.split("-") ;
	REQUEST('appControl', 'getspecificcomponente', [cod[0]] , onComponentespecific, onerror);

}
function onComponentespecific(dataResult)
{
	for (var i = 0; i<dataResult.length; i++)
	{
    		var cve=dataResult[i]['clave'].toString();
		
		while(cve.toString().length<8){
			cve='0'+cve;	
		}
    		document.getElementById("txtcodigosae").value = cve;
		document.getElementById("txtlinea").value = dataResult[i]['linea'];
		document.getElementById("txtidcomponente").value = dataResult[i]['id'];
		document.getElementById("txtcomponente").value = dataResult[i]['componente'];
		document.getElementById("txtum").value = dataResult[i]['unidadEntrada'];
		document.getElementById("txtultimocosto").value = dataResult[i]['ultimoCosto'];
		document.getElementById("txtcantidad").focus();
    	}

}

function savecomponente(){
	var cantidad=document.getElementById("txtcantidad").value;
	var componente=document.getElementById("txtidcomponente").value;
	if(cantidad<=0){
		alert("La cantidad debe ser mayor a cero");
	}else{
		REQUEST('appControl', 'savecomponente', [idproducto,cantidad,componente] , onSavecomponente, onerror);
	}

}

function onSavecomponente(dataResult){
	
	if(dataResult[0]['exito'].localeCompare("si")==0)
	{
		alert("\u00C9xito al Guardar Datos("+dataResult[0]['folio']+")");
		document.getElementById("altacomponente").style.display="none";
		document.getElementById("tablacomponente").style.display="block";
		document.getElementById("agregarcomponente").style.display="block";
		document.getElementById("sin2com").style.display="block";
		document.getElementById("txtcodigosae").value= "";
		document.getElementById("txtlinea").value= "";
		document.getElementById("txtidcomponente").value= "";
		document.getElementById("txtum").value= "";
		document.getElementById("txtultimocosto").value= "";
		document.getElementById("txtcantidad").value="";
	}
	else if(dataResult[0]['exito'].localeCompare("ya")==0)
	{
		alert("El proceso ya ha sido dado de alta");
	}
	else if(dataResult[0]['exito'].localeCompare("no")==0)
	{	
		alert("Error al Guardar Datos. Intente nuevamente");
	}
getcomponentsbyproduct();
}

		/////////////////////////////////////FUNCTIONS FOR EDIT COMPONENTS/////////////////////////////////////////////////

function editarcomponente(str,str2){
	idcomponenteeditar=str;
	iddelcomponente=str2;
	document.getElementById("editacomponente").style.display="block";
	document.getElementById("tablacomponente").style.display="none";
	document.getElementById("agregarcomponente").style.display="none";
	document.getElementById("sin2com").style.display="none";

	var row=document.getElementById("componente-"+str);
	var cells = row.getElementsByTagName('td');
	//cells[0].innerText;

	document.getElementById("txtcomponente2").value= cells[1].innerText;
	document.getElementById("txtcodigosae2").value= cells[0].innerText;
	document.getElementById("txtlinea2").value= cells[2].innerText;
	document.getElementById("txtum2").value= cells[4].innerText;
	document.getElementById("txtultimocosto2").value= cells[4].innerText;
	document.getElementById("txtcantidad2").value=cells[3].innerText;
	componentess();
	
}

function cancelareditacomponente(){
	document.getElementById("editacomponente").style.display="none";
	document.getElementById("tablacomponente").style.display="block";
	document.getElementById("agregarcomponente").style.display="block";
	document.getElementById("sin2com").style.display="block";
		
	document.getElementById("txtcomponente2").value= "";
	document.getElementById("txtcodigosae2").value=  "";
	document.getElementById("txtlinea2").value=  "";
	document.getElementById("txtidcomponente2").value= "";
	document.getElementById("txtum2").value=  "";
	document.getElementById("txtultimocosto2").value=  "";
	document.getElementById("txtcantidad2").value= "";
		
getcomponentsbyproduct();
}


function savecomponenteedita(){
	var cantidad=document.getElementById("txtcantidad2").value;
	var idcomponente=document.getElementById("txtidcomponente2").value;
	
	if(idcomponente.length==0){
		idcomponente=iddelcomponente;
	}

	if(cantidad.length==0 || cantidad<=0){
		alert("La cantidad debe ser mayor a cero");
	}else{
		REQUEST('appControl', 'savecomponenteedita', [idcomponenteeditar,cantidad,idcomponente] , onSavecomponenteedita, onerror);
	}
}
function onSavecomponenteedita(dataResult){
	if(dataResult.length>0){
		if(dataResult[0]['exito'].localeCompare("si")==0)
		{		
			alert("\u00C9xito al modificar los datos");
			document.getElementById("editacomponente").style.display="none";
			document.getElementById("tablacomponente").style.display="block";
			document.getElementById("agregarcomponente").style.display="block";
			document.getElementById("txtcomponente2").value= "";
			document.getElementById("txtcodigosae2").value=  "";
			document.getElementById("txtlinea2").value=  "";
			document.getElementById("txtidcomponente2").value= "";
			document.getElementById("txtum2").value=  "";
			document.getElementById("txtultimocosto2").value=  "";
			document.getElementById("txtcantidad2").value= "";
		}else if(dataResult[0]['exito'].localeCompare("sin")==0)
		{
			alert("El componente no ha sido encontrado");
		}
	}else
	{
		alert("Error al modificar los datos. Intente nuevamente");
	}
	
	
getcomponentsbyproduct();
}


function getspecificcomponente2(str)
{
	REQUEST('appControl', 'getspecificcomponente', [str] , onComponentespecific2, onerror);

}
function onComponentespecific2(dataResult)
{
	for (var i = 0; i<dataResult.length; i++)
	{
    		var cve=dataResult[i]['clave'].toString();
		
		while(cve.toString().length<8){
			cve='0'+cve;	
		}
    		document.getElementById("txtcodigosae2").value = cve;
		document.getElementById("txtlinea2").value = dataResult[i]['linea'];
		document.getElementById("txtidcomponente2").value = dataResult[i]['id'];
		document.getElementById("txtcomponente2").value = dataResult[i]['componente'];
		document.getElementById("txtum2").value = dataResult[i]['unidadEntrada'];
		document.getElementById("txtultimocosto2").value = dataResult[i]['ultimoCosto'];
		document.getElementById("txtcantidad2").value="";
		document.getElementById("txtcantidad2").focus();
    	}
	document.getElementsByName("savecomponentes").disabled = false;
}

		////////////////////////////////////FUNCTIONS FOR DELETE COMPONENTS///////////////////////////////////////////////////////////////

function deleterecordcomponente(str){
	if(confirm("Esta apunto de eliminar este componente, esta acci\u00F3n es irreversible. \u00BFSeguro que desea proceder?")){
		REQUEST('appControl', 'deleterecordcomponente', [str] , onDeletecomponente, onerror);
	
	}

}

function onDeletecomponente(dataResult){
	if(dataResult[0]['exito'].localeCompare("si")==0)
	{	
		alert("\u00C9xito al eliminar el componente");
		
	}
	else if(dataResult[0]['exito'].localeCompare("sin")==0)
	{
		alert("El componente no ha sido encontrado");
	}else
	{
		alert("Error al eliminar los datos. Intente nuevamente");
	}	
getcomponentsbyproduct();
}



////////////////////////////////////////////////////GLOBAL FUNCTIONS FOR DIFFERENT PROCESS/////////////////////////////////////////////////////////////////////////////
document.onkeydown = ShowKeyCode;
        function ShowKeyCode(evt) {
	//alert(evt.keyCode);
            	if(evt.keyCode==8 ){
			
			cod=evt.keyCode;
		}else{
			cod=evt.keyCode;
		}
			
        }




function showEdit(editableObj) {
			editableObj.style.backgroundColor="#e3bfc4";

		}
function edita(editableObj) {
			editableObj.style.backgroundColor="#FFCC99";
		}

function onerror(errObject)
{
		alert(errObject["Message"] + ":\n" + errObject["Detail"]);
}


function getval(grupo){
	var selec;

	for(var i = 0; i < grupo.length; i++) {
 	  if(grupo[i].checked)
    	   selec = grupo[i].value;
 	}
return selec;
}
function myDeleteFunction() {
	var tableRef = document.getElementById('resultado');
	//var rowCount = tableRef.rows.length;
	
	var row = document.getElementsByTagName('tbody')[0];
	var rowCount = row.rows.length;

		for(var a=0;a<rowCount;a++)
		{

			//tableRef.deleteRow(1);
			row.deleteRow(0);
		}
    
}

function myDeleteFunctionpro() {
	var tableRef = document.getElementById('resultadopro');
	//var rowCount = tableRef.rows.length;
	
	var row = tableRef.getElementsByTagName('tbody')[0];
	var rowCount = row.rows.length;

		for(var a=0;a<rowCount;a++)
		{

			//tableRef.deleteRow(1);
			row.deleteRow(0);
		}
    
}

function myDeleteFunctioncom() {
	var tableRef = document.getElementById('resultadocom');
	//var rowCount = tableRef.rows.length;
	
	var row = tableRef.getElementsByTagName('tbody')[0];
	var rowCount = row.rows.length;

		for(var a=0;a<rowCount;a++)
		{

			//tableRef.deleteRow(1);
			row.deleteRow(0);
		}
    
}
function myDeleteFunctionfiles() {
	var tableRef = document.getElementById('resultadofiles');
	//var rowCount = tableRef.rows.length;
	
	var row = tableRef.getElementsByTagName('tbody')[0];
	var rowCount = row.rows.length;

		for(var a=0;a<rowCount;a++)
		{

			//tableRef.deleteRow(1);
			row.deleteRow(0);
		}
    
}
function myDeleteFunctionpt() {
	var tableRef = document.getElementById('resultadoespecificacionpt');
	//var rowCount = tableRef.rows.length;
	
	var row = tableRef.getElementsByTagName('tbody')[0];
	var rowCount = row.rows.length;

		for(var a=0;a<rowCount;a++)
		{

			//tableRef.deleteRow(1);
			row.deleteRow(0);
		}
    
}
function doSearchpt()
		{
			//var tableReg = document.getElementById('resultado');
			var tableReg =  document.getElementById('resultadoespecificacionpt').getElementsByTagName('tbody')[0];

			var searchText = document.getElementById('txtbuscarpt').value.toLowerCase();
			var cellsOfRow="";
			var found=false;
			var compareWith="";
 
			// Recorremos todas las filas con contenido de la tabla
			for (var i = 0; i < tableReg.rows.length; i++)
			{
				cellsOfRow = tableReg.rows[i].getElementsByTagName('td');
				found = false;
				// Recorremos todas las celdas
				for (var j = 0; j < cellsOfRow.length && !found; j++)
				{
					compareWith = cellsOfRow[j].innerHTML.toLowerCase();
					// Buscamos el texto en el contenido de la celda
					if (searchText.length == 0 || (compareWith.indexOf(searchText) > -1))
					{
						found = true;
					}
				}
				if(found)
				{
					tableReg.rows[i].style.display = '';
				} else {
					// si no ha encontrado ninguna coincidencia, esconde la
					// fila de la tabla
					tableReg.rows[i].style.display = 'none';
				}
			}
		}

function doSearch()
		{
			//var tableReg = document.getElementById('resultado');
			var tableReg =  document.getElementById('resultado').getElementsByTagName('tbody')[0];

			var searchText = document.getElementById('txtbuscar').value.toLowerCase();
			var cellsOfRow="";
			var found=false;
			var compareWith="";
 
			// Recorremos todas las filas con contenido de la tabla
			for (var i = 0; i < tableReg.rows.length; i++)
			{
				cellsOfRow = tableReg.rows[i].getElementsByTagName('td');
				found = false;
				// Recorremos todas las celdas
				for (var j = 0; j < cellsOfRow.length && !found; j++)
				{
					compareWith = cellsOfRow[j].innerHTML.toLowerCase();
					// Buscamos el texto en el contenido de la celda
					if (searchText.length == 0 || (compareWith.indexOf(searchText) > -1))
					{
						found = true;
					}
				}
				if(found)
				{
					tableReg.rows[i].style.display = '';
				} else {
					// si no ha encontrado ninguna coincidencia, esconde la
					// fila de la tabla
					tableReg.rows[i].style.display = 'none';
				}
			}
		}


function doSearchpro()
		{
			//var tableReg = document.getElementById('resultadopro');
			var tableReg = document.getElementById('resultadopro').getElementsByTagName('tbody')[0];

			var searchText = document.getElementById('txtbuscarpro').value.toLowerCase();
			var cellsOfRow="";
			var found=false;
			var compareWith="";
 
			// Recorremos todas las filas con contenido de la tabla
			for (var i = 0; i < tableReg.rows.length; i++)
			{
				cellsOfRow = tableReg.rows[i].getElementsByTagName('td');
				found = false;
				// Recorremos todas las celdas
				for (var j = 0; j < cellsOfRow.length && !found; j++)
				{
					compareWith = cellsOfRow[j].innerHTML.toLowerCase();
					// Buscamos el texto en el contenido de la celda
					if (searchText.length == 0 || (compareWith.indexOf(searchText) > -1))
					{
						found = true;
					}
				}
				if(found)
				{
					tableReg.rows[i].style.display = '';
				} else {
					// si no ha encontrado ninguna coincidencia, esconde la
					// fila de la tabla
					tableReg.rows[i].style.display = 'none';
				}
			}
		}

function doSearchcom()
		{
			//var tableReg = document.getElementById('resultadopro');
			var tableReg = document.getElementById('resultadocom').getElementsByTagName('tbody')[0];

			var searchText = document.getElementById('txtbuscarcom').value.toLowerCase();
			var cellsOfRow="";
			var found=false;
			var compareWith="";
 
			// Recorremos todas las filas con contenido de la tabla
			for (var i = 0; i < tableReg.rows.length; i++)
			{
				cellsOfRow = tableReg.rows[i].getElementsByTagName('td');
				found = false;
				// Recorremos todas las celdas
				for (var j = 0; j < cellsOfRow.length && !found; j++)
				{
					compareWith = cellsOfRow[j].innerHTML.toLowerCase();
					// Buscamos el texto en el contenido de la celda
					if (searchText.length == 0 || (compareWith.indexOf(searchText) > -1))
					{
						found = true;
					}
				}
				if(found)
				{
					tableReg.rows[i].style.display = '';
				} else {
					// si no ha encontrado ninguna coincidencia, esconde la
					// fila de la tabla
					tableReg.rows[i].style.display = 'none';
				}
			}
		}



function limpiar(field){
field.value="";
}
function validatenumber(val){
	if(val.value.length<=0){
		val.value=0;
	}
}

function isNumber( input ) {
    return !isNaN( input );
}


function uploadFile(file,desc){
    var iduser=localStorage.getItem('id');
    var code=document.getElementById("txtcodigo").value;
    var url = 'uploadfiles.php';
    var xhr = new XMLHttpRequest();
    var fd = new FormData();
    xhr.open("POST", url, true);
    xhr.onreadystatechange = function() {
        if (xhr.readyState == 4 && xhr.status == 200) {
            // Every thing ok, file uploaded
		if(xhr.responseText.localeCompare("exito")!=0){
			alert("Error al subir el archivo. Intente nuevamente, sie el problema persiste contacte a su administrador");
		}else{
			document.getElementById("doc1").value="";
			document.getElementById("txtfiledescription").value="";
			document.getElementById("docs").style.display="none";	
		}
        }
   };
    fd.append("upload_file", file);
    fd.append("desc", desc);	
    fd.append("code", code);
    fd.append("iduser", iduser);
    xhr.send(fd);
}

function uploadFile2(file,desc,code){
  
  
    var url = 'uploadupdate.php';
    var xhr = new XMLHttpRequest();
    var fd = new FormData();
    xhr.open("POST", url, true);
    xhr.onreadystatechange = function() {
        if (xhr.readyState == 4 && xhr.status == 200) {
            // Every thing ok, file uploaded
		if(xhr.responseText.localeCompare("exito")!=0){
			alert("Error al subir el archivo. Intente nuevamente, sie el problema persiste contacte a su administrador");
		}else{
			document.getElementById("file12").value="";
			document.getElementById("txtfiledescription2").value="";
			document.getElementById("docs22").style.display="none";	
			viewfiles(idproducto);	
		}
        }
   };
    fd.append("upload_file", file);
    fd.append("desc", desc);	
    fd.append("code", code);
    fd.append("idprod", idproducto);
    xhr.send(fd);
}

function uploadFile3(file,desc,code,id){
  
  
    var url = 'uploadupdate2.php';
    var xhr = new XMLHttpRequest();
    var fd = new FormData();
    xhr.open("POST", url, true);
    xhr.onreadystatechange = function() {
        if (xhr.readyState == 4 && xhr.status == 200) {
            // Every thing ok, file uploaded
		if(xhr.responseText.localeCompare("exito")!=0){
			alert("Error al subir el archivo. Intente nuevamente, sie el problema persiste contacte a su administrador");
		}else{
			viewfiles(idproducto);	
		}
        }
   };
    fd.append("upload_file", file);
    fd.append("desc", desc);	
    fd.append("code", code);
    fd.append("id", id);
    xhr.send(fd);
}


function disableEnable(elems, isDisabled){
    for(var i = elems.length-1;i>=0;i--){
        elems[i].disabled = isDisabled;
    }
}

function viewfile(ruta){
 window.open(ruta, '_blank');
}
function completecvesae(cve){
	while(cve.toString().length<8){
			cve='0'+cve;	
		}
return cve;
}
function dopercentaje(str){
	var cantidad=str.value;
	var aux,last;
	if(cod!=8){
		if(cantidad.length>1){
			aux=cantidad.split("%");
			last = aux[1].substr(aux[1].length - 1);
		
			if(isNumber(last) || last.localeCompare(".")==0){
			
				cantidad=aux[0]+last;		
				if((cantidad>=0 && cantidad<=100)|| cantidad.indexOf(".")<=0){
					str.value=cantidad+"%";
				}else{
					str.value=str.value.substring(0, str.value.length - 1);;
				}
			}else{
				cantidad = aux[0].substring(0, aux[0].length - 1); 
				str.value=cantidad+"%";
			}
		}else{
			if(isNumber(cantidad)){
				if(cantidad>=0 && cantidad<=100){
					str.value=cantidad+"%";
				}else{
					str.value="";
				}
			}else{
				str.value="";
			}
		}
	}
}

function createelement(ext,ruta){
			var img3 = document.createElement('img');
   			 img3.className ="thumbfiles";
			switch(ext) 
			{
 			  	case "jpg":
		     			 img3.src = ".."+ruta;
					 img3.addEventListener('click', viewfile.bind(null,".."+ruta));
       					
					  
       			   	break;
				case "png":
       					 img3.src = ".."+ruta;
					 img3.addEventListener('click', viewfile.bind(null,".."+ruta));
       					
        			break;
   				case "jpeg":
       					 img3.src = ".."+ruta;
					 img3.addEventListener('click', viewfile.bind(null,".."+ruta));
       					 
        			break;
				case "txt":
					 img3.src = "../img/txt.png";
					 img3.addEventListener('click', viewfile.bind(null,".."+ruta));
       					
       					
        			break;


   				case "pdf":

					 img3.src = "../img/pdf.png";
					 img3.addEventListener('click', viewfile.bind(null,".."+ruta));
       					 
        			break;

   				case "doc":

					 img3.src = "../img/doc.png";
					 img3.addEventListener('click', viewfile.bind(null,".."+ruta));
       					 
        			break;

   				case "docx":

					 img3.src = "../img/doc.png";
					 img3.addEventListener('click', viewfile.bind(null,".."+ruta));
       					 
        			break;
    				default:

					 img3.src = "../img/otro.png";
					 img3.addEventListener('click', viewfile.bind(null,".."+ruta));
       					       				 
			}
return img3;
}

function calculatepesoneto(){
	var piezascorrugado=document.getElementById("txtpiezascorrugado").value;
	var corrugadosporcama=document.getElementById("txtcorrugadosporcama").value;
	var camaspallette=document.getElementById("txtcamasporpallet").value;
	var pesocorrugado=document.getElementById("txtpesoporcorrugado").value;
	var corrugadostarima=parseFloat(corrugadosporcama)*parseFloat(camaspallette);

	document.getElementById("txtcorrugadoporpallet").value=corrugadostarima;
	var piezastotales=parseFloat(corrugadostarima)*parseFloat(piezascorrugado);
	var pesoneto=parseFloat(corrugadostarima)*parseFloat(pesocorrugado);
	document.getElementById("txtpiezasporpallet").value=piezastotales;
	document.getElementById("txtpesoneto").value=pesoneto;
	
}
function calculatepesoneto2(){
	var piezascorrugado=document.getElementById("txtpiezascorrugado2").value;
	var corrugadosporcama=document.getElementById("txtcorrugadosporcama2").value;
	var camaspallette=document.getElementById("txtcamasporpallet2").value;
	var pesocorrugado=document.getElementById("txtpesoporcorrugado2").value;
	var corrugadostarima=parseFloat(corrugadosporcama)*parseFloat(camaspallette);

	document.getElementById("txtcorrugadoporpallet2").value=corrugadostarima;
	var piezastotales=parseFloat(corrugadostarima)*parseFloat(piezascorrugado);
	var pesoneto=parseFloat(corrugadostarima)*parseFloat(pesocorrugado);
	document.getElementById("txtpiezasporpallet2").value=piezastotales;
	document.getElementById("txtpesoneto2").value=pesoneto;
	
}

function disbleinner(str){
	if(str.localeCompare("No Aplica")==0){
		//document.getElementById("txtcorrugadoinner").disabled=true;
		document.getElementById("txtpiezascorrugadoinner").disabled=true;
		document.getElementById("txtlargocorrugadoinner").disabled=true;
		document.getElementById("txtanchocorrugadoinner").disabled=true;
		document.getElementById("txtaltocorrugadoinner").disabled=true;
		document.getElementById("txtpesoporcorrugadoinner").disabled=true;

		document.getElementById("txtpiezascorrugadoinner").value=0;
		document.getElementById("txtlargocorrugadoinner").value=0;
		document.getElementById("txtanchocorrugadoinner").value=0;
		document.getElementById("txtaltocorrugadoinner").value=0;
		document.getElementById("txtpesoporcorrugadoinner").value=0;
		
	}else{
		//document.getElementById("txtcorrugadoinner").disabled=false;
		document.getElementById("txtpiezascorrugadoinner").disabled=false;
		document.getElementById("txtlargocorrugadoinner").disabled=false;
		document.getElementById("txtanchocorrugadoinner").disabled=false;
		document.getElementById("txtaltocorrugadoinner").disabled=false;
		document.getElementById("txtpesoporcorrugadoinner").disabled=false;
	}

}
function disbleinner2(str){
	if(str.localeCompare("No Aplica")==0){
		//document.getElementById("txtcorrugadoinner2").disabled=true;
		document.getElementById("txtpiezascorrugadoinner2").disabled=true;
		document.getElementById("txtlargocorrugadoinner2").disabled=true;
		document.getElementById("txtanchocorrugadoinner2").disabled=true;
		document.getElementById("txtaltocorrugadoinner2").disabled=true;
		document.getElementById("txtpesoporcorrugadoinner2").disabled=true;

		document.getElementById("txtpiezascorrugadoinner2").value=0;
		document.getElementById("txtlargocorrugadoinner2").value=0;
		document.getElementById("txtanchocorrugadoinner2").value=0;
		document.getElementById("txtaltocorrugadoinner2").value=0;
		document.getElementById("txtpesoporcorrugadoinner2").value=0;
		
	}else{
		//document.getElementById("txtcorrugadoinner2").disabled=false;
		document.getElementById("txtpiezascorrugadoinner2").disabled=false;
		document.getElementById("txtlargocorrugadoinner2").disabled=false;
		document.getElementById("txtanchocorrugadoinner2").disabled=false;
		document.getElementById("txtaltocorrugadoinner2").disabled=false;
		document.getElementById("txtpesoporcorrugadoinner2").disabled=false;
	}

}