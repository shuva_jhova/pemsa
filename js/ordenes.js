////////////////////////////////////////////////////GLOBAL VARIABLES/////////////////////////////////////////////////////////////////////////////
var cod=0;
var idproducto;
var idprocesoeditar;
var idcomponenteeditar;
var iddelcomponente;
var str2;
var perfil=localStorage.getItem("perfil");
var status;
var operador;
var idorden;
var isoperator=localStorage.getItem("operador");
var iduser=localStorage.getItem("id");
var inedition=false;
var departamento;
var idincidente;
var strr=0;
var idordenforincident=0;
var dptodenuncia="";
var deptodenunciado="";
var idincidentedit="";
////////////////////////////////////////////////////FUNCTIONS FOR ORDERS/////////////////////////////////////////////////////////////////////////////


		//////////////////////////////////////UPDATE ORDER////////////////////////////////////////////////
function saverecord()
{
	
	var id=document.getElementById("txtnumeroorden").value;
	var fecharequerida=document.getElementById("txtfecharequerida2").value;
	var cantidad=document.getElementById("txtcantidad2").value;
	var status=document.getElementById("optionstatus").value;
	var fechaproduccion=document.getElementById("txtfechaconfirmada2").value;

 	if(fecharequerida.length == 0 )
	{
		alert("La fecha requerida es inv\u00E1lida");
	}
	else if(cantidad.length <=0)
	{
		alert("La cantidad es inv\u00E1lida, debe ser mayor a cero");
	}
	else
	{
		
		REQUEST('appControl', 'updateorder', [id,fecharequerida,fechaproduccion,cantidad,status], onUpdateorder, onerror);
		
	}
}

function onUpdateorder(dataResult)
{
	if(dataResult.length>0){
		if(dataResult[0]['exito'].localeCompare("si")==0)
		{		
			alert("\u00C9xito al modificar los datos");
			var iframe = window.parent.document.getElementById("iframe");
			iframe.contentWindow.location.reload(true);
		}
		else if(dataResult[0]['exito'].localeCompare("sin")==0)
		{
			alert("El producto no ha sido encontrado");
		}
	}else
	{
		alert("Error al modificar los datos. Intente nuevamente");
	}
    	
	
}

		//////////////////////////////////////ADD ORDER////////////////////////////////////////////////
function clientesandcodigos()
{
REQUEST('appControl', 'clientesandcodigos', [] , onClientesandcodigos, onerror);

}
function onClientesandcodigos(dataResult)
{
	var dataList = document.getElementById('clientess');
	dataList.innerHTML="";
	for (var i = 0; i<dataResult[0]['clientes'].length; i++)
	{
    		var opt = document.createElement('option');
    		opt.value = dataResult[0]['clientes'][i]['cliente'];
		dataList.appendChild(opt);
    	}

	var dataList = document.getElementById('codigoscliente');
	dataList.innerHTML="";
	for (var i = 0; i<dataResult[0]['codigos'].length; i++)
	{
    		var opt = document.createElement('option');
    		opt.value = dataResult[0]['codigos'][i]['codigo'];
		dataList.appendChild(opt);
    	}
agregar2();
}

function agregar(){
	clientesandcodigos();
}

function agregar2(){
			
	// Get the modal
	var modal = document.getElementById('myModal');


	// Get the <span> element that closes the modal
	var span = document.getElementsByClassName("close")[0];
  	modal.style.display = "block";

	// When the user clicks on <span> (x), close the modal
	span.onclick = function() {
		
	 	//var codigo = document.getElementById('txtcodigo').value;
		//if(codigo.length>0){
			var a=confirm("No se guardar\u00E1 ning\u00FAn dato \u00BFSeguro que desea abandonar la captura?");
			if(a){
				modal.style.display = "none";
			}
		//}else{
		//	modal.style.display = "none";
		//}
	}
	productos();
}


function saveorder(){
	
	var fechaemision = document.getElementById('txtfechaemision').value;
	var fecharequerida = document.getElementById('txtfecharequerida').value;
	var producto = document.getElementById('txtproducto').value;
	var cantidad = document.getElementById('txtcantidad').value;
	var descripcion = document.getElementById('txtdescripcion').value;
	var pedidosae = document.getElementById('txtpedidosae').value;
		
	var cliente=document.getElementById('txtcliente').value;
	var codigocliente=document.getElementById('txtcodigocliente').value;
	var ordencompracliente=document.getElementById('txtordencompra').value;

	var inicio2 = new Date(fecharequerida+" 23:59:00");
	var hoy=new Date();
 	/*if(fechaemision.length == 0)
	{
		alert("La fecha de emisi\u00F3n es inv\u00E1lida.");
	}
	else */
	if(cliente.length <= 0)
	{
		alert("El cliente es inv\u00E1lido.");
	}else if(codigocliente.length <= 0)
	{
		alert("El c\u00F3digo de producto del cliente es inv\u00E1lido.");
	} else if(ordencompracliente.length <= 0)
	{
		alert("La orden de compra del cliente es inv\u00E1lida.");
	}else if(fecharequerida.length == 0)
	{
		alert("La fecha requerida es inv\u00E1lida.");
	}else if(producto.length == 0)
	{
		alert("Debe seleccionar un producto.");
	}else if(cantidad.length == 0 || cantidad<=0 ||!isNumber(cantidad))
	{
		alert("La cantidad a producir es inv\u00E1lida");
	}else if(descripcion.length == 0)
	{
		alert("La descripci\u00F3n es inv\u00E1lida");
	}else if(inicio2<hoy){
		alert("La fecha requerida no puede ser menor o igual a hoy");
	}else if(pedidosae<=0){
		alert("El n\u00FAmero de pedido SAE es inv\u00E1lido");
	}
	else
	{
		REQUEST('appControl', 'saveorden', [pedidosae,fechaemision,fecharequerida,producto,cantidad,cliente,codigocliente,ordencompracliente], onsaveorden, onerror);
	}
}

function onsaveorden(dataResult){
	if(dataResult[0]['exito'].localeCompare("si")==0)
	{
		alert("\u00C9xito al Guardar Datos("+dataResult[0]['folio']+")");
		var iframe = window.parent.document.getElementById("iframe");
		iframe.contentWindow.location.reload(true);
	}
	else if(dataResult[0]['exito'].localeCompare("ya")==0)
	{
		alert("La orden ya ha sido dado de alta");
	}
	else if(dataResult[0]['exito'].localeCompare("no")==0)
	{	
		alert("Error al Guardar Datos. Intente nuevamente");
	}

}

function productos()
{
	REQUEST('appControl', 'getallproductsenable', [] , onProductoslist, onerror);
}
function onProductoslist(dataResult)
{
	var dataList = document.getElementById('productos');
	dataList.innerHTML="";
	for (var i = 0; i<dataResult.length; i++)
	{
    		var opt = document.createElement('option');
    		opt.value = completecvesae(dataResult[i]['codigo'])+"-"+dataResult[i]['nombre'];
		dataList.appendChild(opt);
    	}
}
function getproductodetail(){
	var cad=document.getElementById("txtproducto").value;
	REQUEST('appControl', 'getspecificproductbysae', [cad] , onProductosenable, onerror);
}
function onProductosenable(dataResult){
	for(var s=0;s<dataResult.length;s++){
		document.getElementById("txtdescripcion").value=dataResult[s]['descripcion'];	
		/*for (var i = 0; i<dataResult[s]['procesos'].length; i++)
		{
			var tableRef = document.getElementById('resultadotickets').getElementsByTagName('tbody')[0];;
			// Insert a row in the table at the last row
			var newRow   = tableRef.insertRow(tableRef.rows.length);
			newRow.className = "table-row";
			newRow.id=dataResult[s]['procesos'][i]["id"];

			// Insert a cell in the row at index 0
			var newCell  = newRow.insertCell(0);
			newCell.id="proceso"+dataResult[s]['procesos'][i]["id"];
			var newText  = document.createTextNode(dataResult[s]['procesos'][i]["proceso"]);
			newCell.appendChild(newText);

			var newCell  = newRow.insertCell(1);
			newCell.id="descripcion"+dataResult[s]['procesos'][i]["id"];
			var newText  = document.createTextNode(dataResult[s]['procesos'][i]["descripcion"]);
			newCell.appendChild(newText);

			var newCell  = newRow.insertCell(2);
			newCell.id="maquina"+dataResult[s]['procesos'][i]["id"];
			var newText  = document.createTextNode(dataResult[s]['procesos'][i]["maquina"]);
			newCell.appendChild(newText);

			var newCell  = newRow.insertCell(3);
			newCell.id="tickets"+dataResult[s]['procesos'][i]["id"];
			var estandar=dataResult[s]['procesos'][i]["estandarturno"];
			var cont=1;
			var resta=0;
			var cantidad=document.getElementById("txtcantidad").value;	
			do{
				var btn = document.createElement('input');
				btn.type = "button";
				btn.id = "ok";
				btn.value = "T No."+cont;	
			
				newCell.appendChild(btn);
				
				cantidad=parseFloat(cantidad)-parseFloat(estandar);
				cont++;
			}while(cantidad>0 && estandar>0);
		}*/
	}
}
		//////////////////////////////////////GET ALL ORDERS////////////////////////////////////////////////


function orders()
{
/*	    var filtersConfig = {
        base_path: '../recursos/tablefilter/',
        col_1: 'select',
        col_2: 'select',
        col_3: 'select',
        alternate_rows: true,
        rows_counter: false,
        btn_reset: false,
        loader: false,
        status_bar: false,
        mark_active_columns: true,
        highlight_keywords: true,
        col_types: [
            'number', 'string', 'date',
            'number', 'number', 'number',
            'date', 'string'
        ],
        custom_options: {
           
        },
        col_widths: [
            
        ],
        extensions:[{ name: 'sort' }]
    };

    var tf = new TableFilter('resultado', filtersConfig);
    tf.init();*/
	if(isoperator>0){
		document.getElementById("addnew").style.display="none";
	}
	document.getElementById("imgload").style.display="block";
	getorders();	
}
function getorders(){
	if(isoperator>0){
		REQUEST('appControl', 'getordersbyoperator', [iduser] , onOrders, onerror);
	}else{
		REQUEST('appControl', 'getallorders', [] , onOrders, onerror);
	}
}
function onOrders(dataResult)
{	
	document.getElementById("imgload").style.display="none";
	myDeleteFunction() ;
	if(dataResult.length>0){
		for (var i = 0; i<dataResult.length; i++)
		{
				var tableRef = document.getElementById('resultado').getElementsByTagName('tbody')[0];;
				// Insert a row in the table at the last row
				var newRow   = tableRef.insertRow(tableRef.rows.length);
				//newRow.className = "table-row";
				newRow.className = dataResult[i]["color"];

			if(isoperator>0){
				
				newRow.id=dataResult[i][0][0]["id"];
				// Insert a cell in the row at index 0
				var newCell  = newRow.insertCell(0);
				newCell.id="np"+dataResult[i][0][0]["id"];
				var newText  = document.createTextNode(dataResult[i][0][0]["id"]);
				newCell.appendChild(newText);


				var newCell  = newRow.insertCell(1);
				newCell.id="pedidosae"+dataResult[i][0][0]["id"];
				var newText  = document.createTextNode(dataResult[i][0][0]["pedidosae"]);
				newCell.appendChild(newText);

				var newCell  = newRow.insertCell(2);
				newCell.id="cliente"+dataResult[i][0][0]["id"];
				var newText  = document.createTextNode(dataResult[i][0][0]["cliente"]);
				newCell.appendChild(newText);


				var newCell  = newRow.insertCell(3);
				newCell.id="producto"+dataResult[i][0][0]["id"];
				var newText  = document.createTextNode(dataResult[i][0][0]["producto"]);
				newCell.appendChild(newText);

				var newCell  = newRow.insertCell(4);
				newCell.id="cantidad"+dataResult[i][0][0]["id"];
				var newText  = document.createTextNode(dataResult[i][0][0]["cantidad"]);
				newCell.appendChild(newText);

				var newCell  = newRow.insertCell(5);
				newCell.id="fechaemision"+dataResult[i][0][0]["id"];
				var newText  = document.createTextNode(dataResult[i][0][0]["fechaemision"]);
				newCell.appendChild(newText);

				/*var newCell  = newRow.insertCell(5);
				newCell.id="producido"+dataResult[i][0][0]["id"];
				var newText  = document.createTextNode(dataResult[i][0][0]["producido"]);
				newCell.appendChild(newText);

				var newCell  = newRow.insertCell(6);
				newCell.id="faltante"+dataResult[i][0][0]["id"];
				var newText  = document.createTextNode(dataResult[i][0][0]["faltante"]);
				newCell.appendChild(newText);*/

				var newCell  = newRow.insertCell(6);
				newCell.id="fecharequerida"+dataResult[i][0][0]["id"];
				var newText  = document.createTextNode(dataResult[i][0][0]["fecharequerida"]);
				newCell.appendChild(newText);

				var newCell  = newRow.insertCell(7);
				newCell.id="fecharequerida"+dataResult[i][0][0]["id"];
				var newText  = document.createTextNode(dataResult[i][0][0]["fechaproduccion"]);
				newCell.appendChild(newText);

				var newCell  = newRow.insertCell(8);
				newCell.id="status"+dataResult[i][0][0]["id"];
				var newText  = document.createTextNode(dataResult[i][0][0]["status"]);
				newCell.appendChild(newText);
	
			
				newCell  = newRow.insertCell(9);
				var img = document.createElement('img');
   				img.src = "../img/print.png";
				img.id="imgsearch"+dataResult[i][0][0]["id"];
				img.addEventListener('click', userdenied.bind(null,dataResult[i][0][0]["id"]));
				newCell.appendChild(img);
				img.style.display='none';

				var img3 = document.createElement('img');
   				img3.src = "../img/document.png";
				img3.id="imgadd"+dataResult[i][0][0]["id"];
				img3.addEventListener('click', detailorder.bind(null,dataResult[i][0][0]["id"]));
				newCell.appendChild(img3);
				var img4 = document.createElement('img');
   				img4.src = "../img/buy.png";
				img4.id="imgbuy"+dataResult[i][0][0]["id"];
				//img4.style.display="none";
				img4.addEventListener('click', userdenied.bind(null,dataResult[i][0][0]["id"]));
				newCell.appendChild(img4);
				img4.style.display='none';
				

				newCell  = newRow.insertCell(10);
				var img6 = document.createElement('img');
   				img6.src = "../img/route.png";

				img6.addEventListener('click', userdenied.bind(null,dataResult[i][0][0]["id"]));
				newCell.appendChild(img6);
				newCell.style.display='none';

				newCell  = newRow.insertCell(11);
				var img5 = document.createElement('img');
   				img5.src = "../img/editar.png";
				img5.addEventListener('click', userdenied.bind(null,dataResult[i][0][0]["id"]));
				newCell.appendChild(img5)
				newCell.style.display='none';
				
			
				newCell  = newRow.insertCell(12);
				var img2 = document.createElement('img');
   				img2.src = "../img/delete.png";
				img2.addEventListener('click', userdenied.bind(null,dataResult[i][0][0]["id"]));
				newCell.appendChild(img2);
				img2.style.display='none';

			}else{
				
				newRow.id=dataResult[i]["id"];
				// Insert a cell in the row at index 0
				var newCell  = newRow.insertCell(0);
				newCell.id="np"+dataResult[i]["id"];
				var newText  = document.createTextNode(dataResult[i]["id"]);
				newCell.appendChild(newText);


				var newCell  = newRow.insertCell(1);
				newCell.id="pedidosae"+dataResult[i]["id"];
				var newText  = document.createTextNode(dataResult[i]["pedidosae"]);
				newCell.appendChild(newText);

				var newCell  = newRow.insertCell(2);
				newCell.id="producto"+dataResult[i]["id"];
				var newText  = document.createTextNode(dataResult[i]["cliente"]);
				newCell.appendChild(newText);

				var newCell  = newRow.insertCell(3);
				newCell.id="producto"+dataResult[i]["id"];
				var newText  = document.createTextNode(dataResult[i]["producto"]);
				newCell.appendChild(newText);

				
				var newCell  = newRow.insertCell(4);
				newCell.id="cantidad"+dataResult[i]["id"];
				var newText  = document.createTextNode(dataResult[i]["cantidad"]);
				newCell.appendChild(newText);

				

				/*var newCell  = newRow.insertCell(5);
				newCell.id="producido"+dataResult[i]["id"];
				var newText  = document.createTextNode(dataResult[i]["producido"]);
				newCell.appendChild(newText);

				var newCell  = newRow.insertCell(6);
				newCell.id="faltante"+dataResult[i]["id"];
				var newText  = document.createTextNode(dataResult[i]["faltante"]);
				newCell.appendChild(newText);*/

				var newCell  = newRow.insertCell(5);
				newCell.id="fecharequerida"+dataResult[i]["id"];
				var newText  = document.createTextNode(dataResult[i]["fechaemision"]);
				newCell.appendChild(newText);

				var newCell  = newRow.insertCell(6);
				newCell.id="fecharequerida"+dataResult[i]["id"];
				var newText  = document.createTextNode(dataResult[i]["fecharequerida"]);
				newCell.appendChild(newText);
				
				var newCell  = newRow.insertCell(7);
				newCell.id="fecharequerida"+dataResult[i]["id"];
				var newText  = document.createTextNode(dataResult[i]["fechaproduccion"]);
				newCell.appendChild(newText);

				var newCell  = newRow.insertCell(8);
				newCell.id="status"+dataResult[i]["id"];
				var newText  = document.createTextNode(dataResult[i]["status"]);
				newCell.appendChild(newText);
	
			
				newCell  = newRow.insertCell(9);
				var img = document.createElement('img');
   				img.src = "../img/print.png";
				img.id="imgsearch"+dataResult[i]["id"];
				img.addEventListener('click', printorder.bind(null,dataResult[i]["id"]));
				newCell.appendChild(img);
				var img3 = document.createElement('img');
   				img3.src = "../img/document.png";
				img3.id="imgadd"+dataResult[i]["id"];
				img3.addEventListener('click', detailorder.bind(null,dataResult[i]["id"]));
				newCell.appendChild(img3);
				var img4 = document.createElement('img');
   				img4.src = "../img/buy.png";
				img4.id="imgbuy"+dataResult[i]["id"];
				//img4.style.display="none";
				img4.addEventListener('click', ordendecompra.bind(null,dataResult[i]["id"]));
				newCell.appendChild(img4);
				

				var img7 = document.createElement('img');
   				img7.src = "../img/award.png";
				img7.id="imgfile"+dataResult[i]["id"];
				//img7.style.display="none";
				img7.addEventListener('click', qualitycertificate.bind(null,dataResult[i]["id"]));
				newCell.appendChild(img7);

				var img81 = document.createElement('img');
   				img81.src = "../img/accident1.png";
				img81.id="imgaccident"+dataResult[i]["id"];
				//img81.style.display="none";
				
				img81.addEventListener('click', incident1.bind(null,dataResult[i]["id"]));
				newCell.appendChild(img81);

				var img5 = document.createElement('img');
   				img5.src = "../img/editar.png";
				img5.addEventListener('click', editar.bind(null,dataResult[i]["id"]));
				newCell.appendChild(img5);
				var img2 = document.createElement('img');
   				img2.src = "../img/delete.png";
				img2.addEventListener('click', deleterecord.bind(null,dataResult[i]["id"]));
				newCell.appendChild(img2);

				newCell  = newRow.insertCell(8);
				var img6 = document.createElement('img');
   				img6.src = "../img/route.png";
				img6.addEventListener('click', printroute.bind(null,dataResult[i]["id"]));
				newCell.appendChild(img6)
				newCell.style.display="none";

				newCell  = newRow.insertCell(9);
				var img5 = document.createElement('img');
   				img5.src = "../img/editar.png";
				img5.addEventListener('click', editar.bind(null,dataResult[i]["id"]));
				newCell.appendChild(img5);	
				newCell.style.display="none";
			
				newCell  = newRow.insertCell(10);
				var img2 = document.createElement('img');
   				img2.src = "../img/delete.png";
				img2.addEventListener('click', deleterecord.bind(null,dataResult[i]["id"]));
				newCell.appendChild(img2);
				newCell.style.display="none";
			}


			
		}

		document.getElementById("sin").style.display="none";
		document.getElementById("tabla").style.display="block";
		document.getElementById("sin2").style.display="block";
		pagination('#resultado');
	
	}else{
		document.getElementById("sin").style.display="block";
		document.getElementById("tabla").style.display="none";
		document.getElementById("sin2").style.display="none";
	
	}	
}
function pagination(id){
		$(document).ready(function() {
   			 $(id).DataTable( {
				"destroy":true,
      				 "scrollY": 500,
       				 "scrollX": true
    			} );
		} );
}

		//////////////////////////////////////PRINT ORDER TO PDF///////////////////////////////////////

function printorder(str){
	window.open('ordenpdf.php?ido='+str, '_blank');
}
function printroute(str){
	window.open('routepdf.php?ido='+str, '_blank');
}
function qualitycertificate(str){
	window.open('certificadocalidadpdf.php?omp='+str, '_blank');
}
function printticket(str){
	window.open('ticketpdf.php?idpro='+str, '_blank');
}
function ordendecompra(str){
	window.open('ordencomprapdf.php?ido='+str, '_blank');
}

		//////////////////////////////////////USER DENIED///////////////////////////////////////////////

function userdenied(str){
	alert("El usuario no tiene permisos de acceso a estas acciones. Por favor contacte a su administrador");
}


		//////////////////////////////////////EDIT ORDER////////////////////////////////////////////////

function editar(str){

	var row=document.getElementById(str);
	var cells = row.getElementsByTagName('td');
	status=cells[6].innerText;


	REQUEST('appControl', 'getspecificorder', [str] , onOrderspecific, onerror);

	// Get the modal
	var modal = document.getElementById('myModaledit');


	// Get the <span> element that closes the modal
	var span = document.getElementById("closeedit");
 	 modal.style.display = "block";

	// When the user clicks on <span> (x), close the modal
	span.onclick = function() {
		
	 	//var codigo = document.getElementById('txtcodigo').value;
		//if(codigo.length>0){
			var a=confirm("No se guardar\u00E1 ning\u00FAn dato \u00BFSeguro que desea abandonar la captura?");
			if(a){
				modal.style.display = "none";
			}
		//}else{
		//	modal.style.display = "none";
		//}
	}

}

function onOrderspecific(dataResult){
	if(dataResult.length>0){
		var fecemi=dataResult[0]['fechaemision'].split(" ");
		document.getElementById("txtnumeroorden").value=dataResult[0]['id'];
		document.getElementById("txtpedidosae2").value=dataResult[0]['pedidosae'];
		document.getElementById("txtfechaemision2").value=fecemi[0];
		document.getElementById("txtfecharequerida2").value=dataResult[0]['fecharequerida'];
		document.getElementById("txtcantidad2").value=dataResult[0]['cantidad'];
		document.getElementById("txtproducto2").value=dataResult[0]['producto'];
		document.getElementById("txtdescripcion2").value=dataResult[0]['descripcion'];
		document.getElementById("txtfechaconfirmada2").value=dataResult[0]['fechaproduccion'];
	}
	getstatus();
}

function getstatus(){
	REQUEST('appControl', 'getstatus', [] , onStatus, onerror);
}

function onStatus(dataResult){
	var op= document.getElementById("optionstatus");
   		op.innerHTML="";
	if(dataResult.length>0){
		for(var a=0;a<dataResult.length;a++){
			var option = document.createElement("option");
	    		option.value=dataResult[a]['id'];
    			option.text =dataResult[a]['status'];
			if(dataResult[a]['status'].localeCompare(status)==0){
				option.selected=true;
			}
    			op.appendChild(option);
		}
		
	}
}


		//////////////////////////////////////DELETE SPECIFIC ORDER////////////////////////////////////////////////


function deleterecord(str){
	if(confirm("Esta apunto de eliminar esta orden, esta acci\u00F3n es irreversible. \u00BFSeguro que desea proceder?")){
		REQUEST('appControl', 'deleterecordorder', [str] , onDeleteorder, onerror);	
	}

}

function onDeleteorder(dataResult){
	if(dataResult[0]['exito'].localeCompare("si")==0)
	{	
		alert("\u00C9xito al eliminar la orden");	
	}
	else if(dataResult[0]['exito'].localeCompare("sin")==0)
	{
		alert("La orden no ha sido encontrada");
	}else
	{
		alert("Error al eliminar los datos. Intente nuevamente");
	}	
//getorders();
	var iframe = window.parent.document.getElementById("iframe");
			iframe.contentWindow.location.reload(true);

}

	

		//////////////////////////////////////////////////GET DETAIL OF ORDER /////////////////////////////////

function detailorder(str){
	document.getElementById('numor').innerText='No. '+str;
	idorden=str;
	REQUEST('appControl', 'detailorder', [str,isoperator] , onDetailorder, onerror);

}

function onDetailorder(dataResult){



	if(dataResult.length>0){
inedition=false;
		//myDeleteFunctionprocesos();
		//myDeleteFunctioncomponentes();
		var div3=document.getElementById('scro3');
			div3.innerHTML="";
		for(var i=0;i<dataResult[0]['ruta'].length;i++){
			
	
			var divgral=document.createElement("div");
			var divproceso=document.createElement("div");
			divproceso.id=dataResult[0]['ruta'][i]["procesos"].replace(/ /g,"")+dataResult[0]['ruta'][i]['id'];
			divproceso.style.display='none';
			

			//divproceso.innerHTML="<table class='tbl-qa' id='tab-"+dataResult[0]['ruta'][i]['procesos'].replace(/ /g,"")+dataResult[0]['ruta'][i]['id']+"'><thead><tr>"+'<th class="table-header" >Proceso M&aacute;quina</th><!--th class="table-header" >M&aacute;quina</th--><th class="table-header" >Folio</th><th class="table-header" >Cantidad de Proceso</th><th class="table-header" >Merma Otorgada</th><th class="table-header" >Tiempo de Ajuste(min)</th><th class="table-header" >Estandar por Hora</th><th class="table-header" >Unidad de Medida</th><th class="table-header" >Fecha Inicio</th><th class="table-header" >Hora Inicio</th><th class="table-header" >Fecha y Hora de Fin Estimada</th><!--th class="table-header" >Hora de Fin Estimada</th--><!--th class="table-header" >Cantidad Final Calculada</th--><th class="table-header" >Cantidad \u00DAtil de Proceso</th><th class="table-header" >Merma</th><th class="table-header" >Fecha y Hora Inicio</th><!--th class="table-header" >Hora de Inicio</th--><th class="table-header" >Fecha y Hora de Fin</th><!--th class="table-header" >Operador</th--><th class="table-header" >Editar</th><th class="table-header" >Imprimir</th><th class="table-header" >Crear Ticket</th>'+"</tr></thead></table>";
			divproceso.innerHTML="<table class='tbl-qa' id='tab-"+dataResult[0]['ruta'][i]['procesos'].replace(/ /g,"")+dataResult[0]['ruta'][i]['id']+"'><thead><tr>"+'<th class="table-header" >Proceso M&aacute;quina</th><!--th class="table-header" >M&aacute;quina</th--><th class="table-header" >Folio</th><th class="table-header" >Cantidad de Proceso</th><th class="table-header" >Merma Otorgada</th><th class="table-header" >Tiempo de Ajuste(min)</th><th class="table-header" >Estandar por Hora</th><th class="table-header" >Unidad de Medida</th><th class="table-header" >Fecha Inicio</th><th class="table-header" >Hora Inicio</th><th class="table-header" >Fecha y Hora de Fin Estimada</th><!--th class="table-header" >Hora de Fin Estimada</th--><!--th class="table-header" >Cantidad Final Calculada</th--><th class="table-header" >Cantidad \u00DAtil de Proceso</th><th class="table-header" >Merma</th><th class="table-header" >Fecha y Hora Inicio</th><!--th class="table-header" >Hora de Inicio</th--><th class="table-header" >Fecha y Hora de Fin</th><th class="table-header" >Editar</th><th class="table-header" >Imprimir</th><th class="table-header" >Crear Ticket</th>'+"</tr></thead></table>";
			
			var divtitle=document.createElement("div");
			divtitle.className='txt';
			divtitle.style.fontSize = "medium";

			var spantit=document.createElement("h3");
			spantit.addEventListener('click', seee.bind(null,dataResult[0]['ruta'][i]["procesos"].replace(/ /g,"")+dataResult[0]['ruta'][i]['id']));


			var newText  = document.createTextNode('*'+dataResult[0]['ruta'][i]["procesos"]);
			spantit.appendChild(newText);

			divtitle.appendChild(spantit);

			var varbr=document.createElement("br");

			
			var add=document.createElement("div");
			add.className='txt';
			//add.style.fontSize = "medium";
			var imgg = document.createElement('img');
	   		imgg.src = "../img/folder.png";
			imgg.id="imgaddproc"+dataResult[0]['ruta'][i]['id1'];
			imgg.style.display="block";
			imgg.addEventListener('click', addticket.bind(null,"imgaddproc"+dataResult[0]['ruta'][i]['id1']));
			add.appendChild(imgg);
			
			divgral.appendChild(divtitle);
			divgral.appendChild(divproceso);
			//divgral.appendChild(add);
			div3.appendChild(divgral);
			div3.appendChild(varbr);
			
			document.getElementById('tab-'+dataResult[0]['ruta'][i]['procesos'].replace(/ /g,"")+dataResult[0]['ruta'][i]['id']).appendChild(document.createElement("tbody"));
		}



		for(var a=0;a<dataResult.length;a++){


			document.getElementById("noOrden").innerHTML="No. Orden: "+dataResult[a]['id'];
			document.getElementById("codigosae").innerHTML="Clave SAE: "+completecvesae(dataResult[a]['detalleproducto'][0]['codigo']);
			document.getElementById("pedidosae").innerHTML="Pedido SAE: "+dataResult[a]['pedidosae'];
			document.getElementById("fechaemision").innerHTML="Fecha Emisi&oacute;n: "+dataResult[a]['fechaemision'];
			document.getElementById("fecharequerida").innerHTML="Fecha Requeria: "+dataResult[a]['fecharequerida'];
			document.getElementById("status").innerHTML="Estado: "+dataResult[a]['status'][0]['status'];
			document.getElementById("cantidad").innerHTML="Cantidad de Producto: "+dataResult[a]['cantidad']+" Pzs";
			document.getElementById("producto").innerHTML="Producto:<br> "+dataResult[a]['detalleproducto'][0]['nombre'];
			document.getElementById("descripcion").innerHTML="Descripci&oacute;n:<br> "+dataResult[a]['detalleproducto'][0]['descripcion'];
		
			for(var i=0;i<dataResult[a]['procesos'].length;i++){
				if(dataResult[a]['procesos'][i]["idoperador"]==iduser || isoperator==0){

				

					var tableRef = document.getElementById('tab-'+dataResult[a]['procesos'][i]["maquina"].replace(/ /g,"")+dataResult[a]['procesos'][i]["idproceso"]).getElementsByTagName('tbody')[0];;
					// Insert a row in the table at the last row
					var newRow   = tableRef.insertRow(tableRef.rows.length);
					newRow.className = "table-row";
					newRow.id="proc"+dataResult[a]['procesos'][i]["id"];

					// Insert a cell in the row at index 0
					var newCell  = newRow.insertCell(0);
					newCell.id="proceso"+dataResult[a]['procesos'][i]["id"];
					var newText  = document.createTextNode(dataResult[a]['procesos'][i]["proceso"]);
					newCell.appendChild(newText);
					var linebreak = document.createElement('br');
					newCell.appendChild(linebreak);
					//var newCell  = newRow.insertCell(1);
					//newCell.id="maquina"+dataResult[a]['procesos'][i]["id"];
					var newText  = document.createTextNode(dataResult[a]['procesos'][i]["maquina"]);
					newCell.appendChild(newText);

					var newCell  = newRow.insertCell(1);
					newCell.id="maquina"+dataResult[a]['procesos'][i]["id"];
					var newText  = document.createTextNode(dataResult[a]['procesos'][i]["folio"]);
					newCell.appendChild(newText);

					var newCell  = newRow.insertCell(2);
					newCell.id="maquina"+dataResult[a]['procesos'][i]["id"];
					var newText  = document.createTextNode(dataResult[a]['procesos'][i]["cantidad"]);
					newCell.appendChild(newText);

					var newCell  = newRow.insertCell(3);
					newCell.id="merma"+dataResult[a]['procesos'][i]["id"]
					var newText  = document.createTextNode(dataResult[a]['procesos'][i]["mermaestimada"]);
					newCell.appendChild(newText);

					var newCell  = newRow.insertCell(4);
					newCell.id="ajuste"+dataResult[a]['procesos'][i]["id"];
					var newText  = document.createTextNode(dataResult[a]['procesos'][i]["tiempoajuste"]);
					newCell.appendChild(newText);

					var newCell  = newRow.insertCell(5);
					newCell.id="estandar"+dataResult[a]['procesos'][i]["id"];
					var newText  = document.createTextNode(dataResult[a]['procesos'][i]["estandarhora"]);
					newCell.appendChild(newText);

					var newCell  = newRow.insertCell(6);
					newCell.id="um"+dataResult[a]['procesos'][i]["id"];
					var newText  = document.createTextNode(dataResult[a]['procesos'][i]["um"]);
					newCell.appendChild(newText);


					var newCell  = newRow.insertCell(7);
					newCell.id="fecharequerida"+dataResult[a]['procesos'][i]["id"];
					//var newText=document.createTextNode("Fecha Requerida:");
					//newCell.appendChild(newText);
					//var linebreak = document.createElement('br');
					//newCell.appendChild(linebreak);
					var newText  = document.createTextNode(dataResult[a]['procesos'][i]["fecharequerida"]);
					newCell.appendChild(newText);
					var linebreak = document.createElement('br');
					newCell.appendChild(linebreak);

					//var newText=document.createTextNode("Fecha Emisi\u00F3n:");
					//newCell.appendChild(newText);
					
					//newText  = document.createTextNode("Fecha Emisi\u00F3n:"+dataResult[a]['procesos'][i]["fechaemision"]);
					//newCell.appendChild(newText);

					var newCell  = newRow.insertCell(8);
					newCell.id="horarequerida"+dataResult[a]['procesos'][i]["id"];
					var newText  = document.createTextNode(dataResult[a]['procesos'][i]["horarequerida"]);
					newCell.appendChild(newText);


					var newCell  = newRow.insertCell(9);
					newCell.id="fechafinestimada"+dataResult[a]['procesos'][i]["id"];
					var newText  = document.createTextNode(dataResult[a]['procesos'][i]["fechafinestimada"]);
					newCell.appendChild(newText);
					var linebreak = document.createElement('br');
					newCell.appendChild(linebreak);
					//var newCell  = newRow.insertCell(10);
					//newCell.id="hrfinestimada"+dataResult[a]['procesos'][i]["id"];
					var newText  = document.createTextNode(dataResult[a]['procesos'][i]["hrfinestimada"]);
					newCell.appendChild(newText);


					/*var newCell  = newRow.insertCell(10);
					newCell.id="cantidadfinalcalculada"+dataResult[a]['procesos'][i]["id"];
					var newText  = document.createTextNode(dataResult[a]['procesos'][i]["cantidadfinalcalculada"]);
					newCell.appendChild(newText);*/

					var newCell  = newRow.insertCell(10);
					newCell.id="cantidadutil"+dataResult[a]['procesos'][i]["id"];
					//var newText  = document.createTextNode("Total: "+dataResult[a]['procesos'][i]["cantidadtotal"]);
					//newCell.appendChild(newText);
					//var linebreak = document.createElement('br');
					//newCell.appendChild(linebreak);
					var newText  = document.createTextNode(dataResult[a]['procesos'][i]["cantidadutil"]);
					newCell.appendChild(newText);


					var newCell  = newRow.insertCell(11);
					newCell.id="merma"+dataResult[a]['procesos'][i]["id"];
					var newText  = document.createTextNode(dataResult[a]['procesos'][i]["merma"]);
					newCell.appendChild(newText);

					var newCell  = newRow.insertCell(12);
					newCell.id="fechainicio"+dataResult[a]['procesos'][i]["id"];
					var newText  = document.createTextNode(dataResult[a]['procesos'][i]["fechainicio"]);
					newCell.appendChild(newText);
					var linebreak = document.createElement('br');
					newCell.appendChild(linebreak);
					//var newCell  = newRow.insertCell(14);
					//newCell.id="horainicio"+dataResult[a]['procesos'][i]["id"];
					var newText  = document.createTextNode(dataResult[a]['procesos'][i]["horainicio"]);
					newCell.appendChild(newText);

					var newCell  = newRow.insertCell(13);
					newCell.id="fechatermino"+dataResult[a]['procesos'][i]["id"];
					var newText  = document.createTextNode(dataResult[a]['procesos'][i]["fechafin"]);
					newCell.appendChild(newText);
					var linebreak = document.createElement('br');
					newCell.appendChild(linebreak);
					//var newCell  = newRow.insertCell(16);
					//newCell.id="horatermino"+dataResult[a]['procesos'][i]["id"];
					var newText  = document.createTextNode(dataResult[a]['procesos'][i]["horafin"]);
					newCell.appendChild(newText);
				
					/*var newCell  = newRow.insertCell(14);
					newCell.id="operador"+dataResult[a]['procesos'][i]["id"];
					var op;
					if(dataResult[a]['procesos'][i]["operador"]==null){
						op="Sin asignar";
					}else{
						op=dataResult[a]['procesos'][i]["operador"];
					}
					var newText  = document.createTextNode(op);
					newCell.appendChild(newText);*/

			
					newCell  = newRow.insertCell(14);
					var img = document.createElement('img');
   					img.src = "../img/editar.png";
					img.id="imgeditar"+dataResult[a]['procesos'][i]["id"];
					if(dataResult[a]['procesos'][i]["status"]==6){
						img.addEventListener('click', finalizado);
					}else{
						img.addEventListener('click', edit.bind(null,dataResult[a]['procesos'][i]["id"],dataResult[a]['procesos'][i]["idmaquina"]));
					}
					newCell.appendChild(img);
					var img3 = document.createElement('img');
	   				img3.src = "../img/save.png";
					img3.id="imgsave"+dataResult[a]['procesos'][i]["id"];
					img3.style.display="none";
					img3.addEventListener('click', savedit.bind(null,dataResult[a]['procesos'][i]["id"]));
					newCell.appendChild(img3);
					var img4 = document.createElement('img');
   					img4.src = "../img/cancel.png";
					img4.id="imgcancel"+dataResult[a]['procesos'][i]["id"];
					img4.style.display="none";
					img4.addEventListener('click', canceledit);
					newCell.appendChild(img4);

					newCell  = newRow.insertCell(15);
					var img33 = document.createElement('img');
   					img33.src = "../img/printticket.png";
					img33.id="imgeditar"+dataResult[a]['procesos'][i]["id"];
					img33.addEventListener('click', printticket.bind(null,dataResult[a]['procesos'][i]["id"]));
					newCell.appendChild(img33);
					
					
						newCell  = newRow.insertCell(16);
						var imgg = document.createElement('img');
   						imgg.src = "../img/agregar.png";
						imgg.id="imgeditar"+dataResult[a]['procesos'][i]["id"];
						imgg.addEventListener('click', addticket.bind(null,dataResult[a]['procesos'][i]["id"],dataResult[a]['procesos'][i]["cantidad"],dataResult[a]['procesos'][i]["cantidadutil"]));
						newCell.appendChild(imgg);
					
					
				}
			}
			
			/*for(var i=0;i<dataResult[a]['componentes'].length;i++){

				var tableRef = document.getElementById('componentesorden').getElementsByTagName('tbody')[0];;
				// Insert a row in the table at the last row
				var newRow   = tableRef.insertRow(tableRef.rows.length);
				newRow.className = "table-row";
				newRow.id="comp"+dataResult[a]['componentes'][i]["id"];

				// Insert a cell in the row at index 0
				var newCell  = newRow.insertCell(0);
				newCell.id="codigo"+dataResult[a]['componentes'][i]["id"];
				var newText  = document.createTextNode(completecvesae(dataResult[a]['componentes'][i]["clave"]));
				newCell.appendChild(newText);

				var newCell  = newRow.insertCell(1);
				newCell.id="nombre"+dataResult[a]['componentes'][i]["id"];
				var newText  = document.createTextNode(dataResult[a]['componentes'][i]["descripcion"]);
				newCell.appendChild(newText);

				var newCell  = newRow.insertCell(2);
				newCell.id="linea"+dataResult[a]['componentes'][i]["id"];
				var newText  = document.createTextNode(dataResult[a]['componentes'][i]["linea"]);
				newCell.appendChild(newText);

				var newCell  = newRow.insertCell(3);
				newCell.id="um"+dataResult[a]['componentes'][i]["id"];
				var newText  = document.createTextNode(dataResult[a]['componentes'][i]["um"]);
				newCell.appendChild(newText);

				var newCell  = newRow.insertCell(4);
				newCell.id="cantidadunitaria"+dataResult[a]['componentes'][i]["id"];
				var newText  = document.createTextNode(dataResult[a]['componentes'][i]["cantidadunitaria"]);
				newCell.appendChild(newText);

				var newCell  = newRow.insertCell(5);
				newCell.id="costounitario"+dataResult[a]['componentes'][i]["id"];
				var newText  = document.createTextNode(dataResult[a]['componentes'][i]["ultimocosto"]);
				newCell.appendChild(newText);

				var newCell  = newRow.insertCell(6);
				newCell.id="cantidadorden"+dataResult[a]['componentes'][i]["id"];
				var newText  = document.createTextNode(dataResult[a]['componentes'][i]["cantidadorden"]);
				newCell.appendChild(newText);

				var newCell  = newRow.insertCell(7);
				newCell.id="total"+dataResult[a]['componentes'][i]["id"];
				var newText  = document.createTextNode(dataResult[a]['componentes'][i]["costototal"]);
				newCell.appendChild(newText);

				var newCell  = newRow.insertCell(8);
				newCell.id="total"+dataResult[a]['componentes'][i]["id"];
				var newText  = document.createTextNode(dataResult[a]['componentes'][i]["fecha"]);
				newCell.appendChild(newText);

				
				
				newCell  = newRow.insertCell(9);
				var img = document.createElement('img');
				img.src = "../img/editar.png";
				img.id="imgeditarcomponente"+dataResult[a]['componentes'][i]["id"];
				
				img.addEventListener('click', editcomponente.bind(null,dataResult[a]['componentes'][i]["id"]));
				
				newCell.appendChild(img);
				var img3 = document.createElement('img');
	   			img3.src = "../img/save.png";
				img3.id="imgsavecomponente"+dataResult[a]['componentes'][i]["id"];
				img3.style.display="none";
				img3.addEventListener('click', saveditcomponente.bind(null,dataResult[a]['componentes'][i]["id"]));
				newCell.appendChild(img3);
				var img4 = document.createElement('img');
   				img4.src = "../img/cancel.png";
				img4.id="imgcancelcomponente"+dataResult[a]['componentes'][i]["id"];
				img4.style.display="none";
				img4.addEventListener('click', canceleditcomponente);
				newCell.appendChild(img4);

			}*/
		}
		if(dataResult.length>0){
			// Get the modal
			var modal = document.getElementById('myModaldetail');

			// Get the <span> element that closes the modal
			var span = document.getElementById("closedetail");
 			 modal.style.display = "block";

			// When the user clicks on <span> (x), close the modal
			span.onclick = function() {
		
	 		//var codigo = document.getElementById('txtcodigo').value;
			//if(codigo.length>0){
				//var a=confirm("No se guardar\u00E1 ning\u00FAn dato \u00BFSeguro que desea abandonar la captura?");
				//if(a){
					modal.style.display = "none";
				//}
			//}else{
			//	modal.style.display = "none";
			//}
			}

		}
	}

}
function addticket(id,cantidad,cantidadutil){
	if(cantidad>cantidadutil && cantidadutil>0){
		REQUEST('appControl', 'generateticket', [id,cantidad,cantidadutil] , onGenerate, onerror);
	}else{
		alert("No se han cerrado este ticket. Por el momento no puede generar otro.");
	}
}

function onGenerate(dataResult){
	if(dataResult[0]['exito'].localeCompare("si")==0)
	{
		alert("\u00C9xito al Guardar Datos("+dataResult[0]['folio']+")");
		detailorder(idorden);
	}
	else if(dataResult[0]['exito'].localeCompare("ya")==0)
	{
		alert("La orden ya ha sido dado de alta");
	}
	else if(dataResult[0]['exito'].localeCompare("no")==0)
	{	
		alert("Error al Guardar Datos. Intente nuevamente");
	}	
}
function seee(id){
	var isVisible = document.getElementById(id).style.display == "block";
	if(isVisible){
		document.getElementById(id).style.display='none';
	}else{
		document.getElementById(id).style.display='block';
	}
}
function sedet(){
	// Get the modal
			var modal = document.getElementById('myModaldetail2');

			// Get the <span> element that closes the modal
			var span = document.getElementById("closedetail2");
 			 modal.style.display = "block";

			// When the user clicks on <span> (x), close the modal
			span.onclick = function() {
		
	 		//var codigo = document.getElementById('txtcodigo').value;
			//if(codigo.length>0){
				//var a=confirm("No se guardar\u00E1 ning\u00FAn dato \u00BFSeguro que desea abandonar la captura?");
				//if(a){
					modal.style.display = "none";
				//}
			//}else{
			//	modal.style.display = "none";
			//}
			}
}
		////////////////////////////////////FUNCTION FOR EDIT COMPONENT BY ORDER///////////////////////////////////////
function editcomponente(str){
	str2=str;
	var row=document.getElementById("comp"+str);
	row.className = "table-row2";
	var cells = row.getElementsByTagName('td')

	for(var a=0;a<cells.length-1;a++){
		cells[a].style.backgroundColor="#9adbcf";
	}
	
	
	var img1=document.getElementById('imgeditarcomponente'+str);
	var img2=document.getElementById('imgsavecomponente'+str);
	var img3=document.getElementById('imgcancelcomponente'+str);

	img1.style.display="none";
	img2.style.display="block";
	img3.style.display="block";

	

	var date =cells[8].innerText;
	if(date.localeCompare("0000-00-00")!=0){
			alert("La fecha de compra Ya ha sido ingresada");
	}else{
		cells[8].innerText="";
		cells[8].contentEditable="false";
    		
  		var x = document.createElement("INPUT");
    		x.setAttribute("type", "date");
    		x.id="fechacompra"+str;
   		cells[8].appendChild(x);
	}
}
function saveditcomponente(str){

		var fechacompra=document.getElementById("fechacompra"+str).value;
			

		var inicio = new Date(fechacompra+' 00:00:00');
		
		var hoy=new Date();

			if(inicio>hoy){
				alert("La fecha de compra no puede ser mayor a la de hoy");
			}
			else{
				if(confirm("Esta a punto de modificar este registro. \u00BFSeguro de continuar? ")){

					REQUEST('appControl', 'updatecomponent', [str,fechacompra] , onComponetupdate, onerror);
				}
			}
}
function onComponetupdate(dataResult){
	if(dataResult.length>0){
		if(dataResult[0]['exito'].localeCompare("si")==0)
		{		
			alert("\u00C9xito al modificar los datos");
			detailorder(idorden);
		}
		else if(dataResult[0]['exito'].localeCompare("sin")==0)
		{	detailorder(idorden);
			alert("El componente no ha sido encontrado");
		}
	}else
	{
		alert("Error al modificar los datos. Intente nuevamente");
	}
    	
}
function canceleditcomponente(){
	if(confirm("No se guardar\u00E1 ning\u00FAn cambio. \u00BFSeguro de continuar?")){
		detailorder(idorden);
	}
}

		////////////////////////////////////FUNCTION FOR EDIT PROCESS BY ORDER/////////////////////////////////////////

function edit(str,idmaquina){

	if(inedition){
		return;
	}
	inedition=true;
	str2=str;
	var row=document.getElementById("proc"+str);
	row.className = "table-row2";
	var cells = row.getElementsByTagName('td');

	for(var a=0;a<cells.length-1;a++){
		cells[a].style.backgroundColor="#9adbcf";
		//if(a==3 ||a==4 ){
		//	cells[a].contentEditable = "true";
		//	cells[a].addEventListener('focus', showEdit.bind(null,cells[a]));
		//}
	}
	
	
	var img1=document.getElementById('imgeditar'+str);
	var img2=document.getElementById('imgsave'+str);
	var img3=document.getElementById('imgcancel'+str);

	img1.style.display="none";
	img2.style.display="block";
	img3.style.display="block";

	if(isoperator>0){

		//var canti=cells[10].innerText.split("\n");
		var canti=cells[10].innerText;
		//var total=canti[0].split(":");
		//var util=canti[1].split(":");
		var util=canti;
		cells[10].innerText="";
		cells[10].contentEditable="false";
    		
  		var t = document.createTextNode("Cantidad Util");
     		  
		//cells[10].appendChild(t);
		//var x = document.createElement("br");
		//cells[10].appendChild(x);
		x = document.createElement("INPUT");
    		x.setAttribute("type", "number");
    		x.setAttribute("value", util);
		x.setAttribute("style"," width: 80px");
		x.id="canutil"+str;
   		cells[10].appendChild(x);
		
  		 /*t = document.createTextNode("Cantidad Producida");
		cells[10].appendChild(t);
		 x = document.createElement("br");
		cells[10].appendChild(x);
		 x = document.createElement("INPUT");
    		x.setAttribute("type", "number");
    		x.setAttribute("value", total[1].trim());
		x.setAttribute("style"," width: 80px");
		x.id="cantotal"+str;
   		cells[10].appendChild(x)*/

		var merma=cells[11].innerText;
		cells[11].innerText="";
		cells[11].contentEditable="false";
		x = document.createElement("INPUT");
    		x.setAttribute("type", "number");
    		x.setAttribute("value", merma);
		x.setAttribute("style"," width: 80px" );
		x.id="canmerma"+str;
   		cells[11].appendChild(x);
		



		var fec=cells[13].innerText.split("\n");
		var fecinicio=fec[0];
		
		cells[12].innerText="";
		cells[12].contentEditable="false";
		t = document.createTextNode("Fecha Inicio");
		cells[12].appendChild(t);		
		x = document.createElement("br");
		cells[12].appendChild(x);

		x = document.createElement("INPUT");
    		x.setAttribute("type", "date");
		if(fecinicio.localeCompare('0000-00-00')==0)
		{
			x.setAttribute("value", todaydate());
		}else{
    			x.setAttribute("value", fecinicio);
		}
		x.setAttribute("disabled", true);
		x.setAttribute("style"," width: 120px" );
		x.id="fecinicio"+str;
   		cells[12].appendChild(x);

		x = document.createElement("br");
		cells[12].appendChild(x);

		t = document.createTextNode("Hora Inicio");
		cells[12].appendChild(t);		
		x = document.createElement("br");
		cells[12].appendChild(x);

		var hrinicio=fec[1];
		//cells[12].innerText="";
		//cells[12].contentEditable="false";
		x = document.createElement("INPUT");
    		x.setAttribute("type", "time");
    		x.setAttribute("value", hrinicio);
		x.id="hrinicio"+str;
   		cells[12].appendChild(x);



		var fec2=cells[13].innerText.split("\n");
		var fectermino=fec2[0];
		cells[13].innerText="";
		cells[13].contentEditable="false";
		t = document.createTextNode("Fecha Fin");
		cells[13].appendChild(t);		
		x = document.createElement("br");
		cells[13].appendChild(x);

		x=document.createElement("INPUT");
    		x.setAttribute("type", "date");
    		x.setAttribute("value", fectermino);
		x.setAttribute("style"," width: 120px" );
		x.id="fectermino"+str;
   		cells[13].appendChild(x);

		x = document.createElement("br");
		cells[13].appendChild(x);

		t = document.createTextNode("Hora Fin");
		cells[13].appendChild(t);		
		x = document.createElement("br");
		cells[13].appendChild(x);

		var hrtermino=fec2[1];
		//cells[13].innerText="";
		//cells[13].contentEditable="false";
		x = document.createElement("INPUT");
    		x.setAttribute("type", "time");
    		x.setAttribute("value", hrtermino);
		x.id="hrtermino"+str;
   		cells[13].appendChild(x);
	}else{

		var merma=cells[3].innerText;
		cells[3].innerText="";
		cells[3].contentEditable="false";

		var x = document.createElement("INPUT");
    		x.setAttribute("type", "number");
    		x.setAttribute("value", merma);
		x.setAttribute("style"," width: 80px" );
		x.id="txtmerma"+str;
		x.disabled=true;
   		cells[3].appendChild(x);	


		var ajuste=cells[4].innerText;
		cells[4].innerText="";
		cells[4].contentEditable="false";

		var x = document.createElement("INPUT");
    		x.setAttribute("type", "number");
    		x.setAttribute("value", ajuste);
		x.setAttribute("style"," width: 80px" );
		x.id="txttiempoajuste"+str;
		x.disabled=true;
   		cells[4].appendChild(x);	

					
		var date=cells[7].innerText.split("\n");
		var requerida=date[0].split(":");
		var emision=date[1].split(":");
		cells[7].innerText="";
		cells[7].contentEditable="false";

  		var t = document.createTextNode("Fecha requerida");
		//cells[7].appendChild(t);		
		var x = document.createElement("br");
		//cells[7].appendChild(x);

		x = document.createElement("INPUT");
    		x.setAttribute("type", "date");
    		x.setAttribute("value", requerida[0]);
		x.id="fecrequerida"+str;
		x.addEventListener('blur', getenablehours.bind(null,idmaquina,str));
		//x.addEventListener('blur', calculafechafin);
   		cells[7].appendChild(x);
	
		/*t = document.createTextNode("Fecha de emisi\u00F3n");
		t.style.display="none";
		cells[7].appendChild(t);		
		var x = document.createElement("br");
		cells[7].appendChild(x);

		x = document.createElement("INPUT");
    		x.setAttribute("type", "date");
		x.setAttribute("style", "display:none");
    		x.setAttribute("value", emision[1]);
		x.id="fecemision"+str;
   		cells[7].appendChild(x);
		*/
		var time=cells[8].innerText;
		//cells[8].innerText="";
		cells[8].contentEditable="false";

		//var xx = document.createElement("INPUT");
    		//xx.setAttribute("type", "time");
    		//xx.setAttribute("value", time);
		//xx.addEventListener('blur', calculafechafin);
		
		//xx.id="hrequerida"+str;
   		//cells[8].appendChild(hrs(time,"hrequerida"+str,time));

		/*operador=cells[14].innerText;
		//if(){
		cells[14].innerText="";
		cells[14].contentEditable="false";
	
		var selectList = document.createElement("select");	
		selectList.id="optionoperator"+str;
 	
 		
		cells[14].innerHTML="";
		cells[14].innerText="";
		cells[14].appendChild(selectList);*/
		//}
		
		//getoperators();
	}
}

function getenablehours(idmaquina,str){
		strr=str;
	var fecharequerida=document.getElementById("fecrequerida"+str).value;

		var row=document.getElementById("proc"+str);
		row.className = "table-row2";
		var cells = row.getElementsByTagName('td')
		
		var ajuste=document.getElementById("txttiempoajuste"+str2).value;
   		var fecharequerida=document.getElementById("fecrequerida"+str2).value;
   		//var horarequerida=document.getElementById("hrequerida"+str2).value;
		//var cantidad=document.getElementById("cantidad").innerText.split(' ');
		var merma=document.getElementById("txtmerma"+str2).value;
		var cantidad=cells[2].innerText;

			var estandar=parseFloat(cells[5].innerText)*8;
			//var minutos=( ( ( (parseFloat(cantidad)+parseFloat(merma))/parseFloat(estandar))*480)+parseFloat(ajuste));	
			var minutos=( ( ( (parseFloat(cantidad))/parseFloat(estandar))*480)+parseFloat(ajuste));	
			
	if(fecharequerida.length>0){
		REQUEST('appControl', 'getdisponibilitybydate', [idmaquina,fecharequerida,minutos] , onDisponibility, onerror);
	}else{
		alert("La Fecha Requerida es Inv\u00E1lida");
	}
}
function  onDisponibility(dataResult){
	var row=document.getElementById("proc"+strr);
	//row.className = "table-row2";
	var cells = row.getElementsByTagName('td');
	cells[8].innerText='';
	var op= document.createElement("select");
   	op.id="hrequerida"+strr;
	op.addEventListener('change', calculafechafin);
	var aux;
		var option = document.createElement("option");

			option.value=0;
    			option.text ="---Seleccione---";
			
    			op.appendChild(option);

	
		for(var a=0;a<dataResult.length;a++){
			var option = document.createElement("option");
			
	    		option.value=dataResult[a]['hora'];
    			option.text =dataResult[a]['hora'];
			
    			op.appendChild(option);
		}
	cells[8].appendChild(op);
//console.log(dataResult);
}
function hrs(time,id,val){
	/*var op= document.createElement("select");
   	op.id=id;
	op.addEventListener('change', calculafechafin);
	var aux;
	
		for(var a=0;a<24;a++){
			var option = document.createElement("option");
			if(a<10){
				aux="0"+a;
			}else{
				aux=a;
			}
	    		option.value=aux+":01:00";
    			option.text =aux+":01:00";
			if(val.localeCompare(aux+":01:00")==0){
				option.selected=true;
			}
    			op.appendChild(option);
		}*/
	var x = document.createElement("INPUT");
    		x.setAttribute("type", "time");
    		x.setAttribute("value", val);
		x.addEventListener('blur', calculafechafin);
		x.id=id;
   			
	
return x;
}
function finalizado(){
	alert("Este registro no se puede modificar, El ticket ya ha sido finalizado");
}
function canceledit(){
	if(confirm("No se guardar\u00E1 ning\u00FAn cambio. \u00BFSeguro de continuar?")){
		detailorder(idorden);
	}
}
function savedit(str){
	if(isoperator>0){
		var cantidadutil=document.getElementById("canutil"+str).value;
		var cantotal=0;//document.getElementById("cantotal"+str).value;
		var merma=document.getElementById("canmerma"+str).value;
		var fechainicio=document.getElementById("fecinicio"+str).value;
		var hrinicio=document.getElementById("hrinicio"+str).value;
		var fechafin=document.getElementById("fectermino"+str).value;
		var hrfin=document.getElementById("hrtermino"+str).value;


		var inicio = new Date(fechainicio+' 23:59:00');
		var termino = new Date(fechafin+' '+hrfin);
		var iniciotodo = new Date(fechainicio+' '+hrinicio);
		var hoy=new Date();

			
			/*if(inicio2<hoy){
				alert("La fecha requerida no puede ser menor a la de hoy");
			}else if(cantidadutil<=0){
				alert("La cantidad \u00FAtil no es valida");
			}else if(cantotal<=0){
				alert("Debes ingresar la cantidad producida");
			}else if(fechainicio.length==0||fechafin.length==0){
				alert("La fecha y hora de fin asi como de inicio son obligatorias");
			}
			else if(inicio>hoy){
				alert("La fecha de inicio no puede ser mayor a la de hoy");
			}else if(termino<=inicio){
				alert("La fecha de fin no puede ser menor a la de inicio");
			}else if(iniciotodo>termino){
				alert("La fecha de fin no puede ser menor a la de inicio");
			}
			else{*/
				if(confirm("Esta a punto de modificar este registro. \u00BFSeguro de continuar? ")){

					REQUEST('appControl', 'updateprocessbyorderoperator', [str,cantidadutil,cantotal,merma,fechainicio,hrinicio,fechafin,hrfin] , onProcessupdate, onerror);
				}
			//}

	}else{

		var row=document.getElementById("proc"+str);
		row.className = "table-row2";
		var cells = row.getElementsByTagName('td')

		var fecharequerida=document.getElementById("fecrequerida"+str).value;
		var fechaemision="";//document.getElementById("fecemision"+str).value;							
		var hrrequerida=document.getElementById("hrequerida"+str).value;
		//var operador=document.getElementById("optionoperator"+str).value;
		var ajuste=document.getElementById("txttiempoajuste"+str).value;
		var merma=document.getElementById("txtmerma"+str).value;
		var fec=cells[9].innerText.split("\n");
		var fechaestimada=fec[0];
		var horaestimada=fec[1];
		var folio=cells[1].innerText;
		var inicio2 = new Date(fecharequerida+' '+hrrequerida);
		var hoy=new Date();
		
			/*if(inicio2<hoy){
				alert("La fecha requerida no puede ser menor a la de hoy");
			}else{*/
				if(confirm("Esta a punto de modificar este registro. \u00BFSeguro de continuar? ")){
					//REQUEST('appControl', 'updateprocessbyorder', [str,folio,merma,fecharequerida,hrrequerida,operador,fechaestimada,horaestimada,ajuste] , onProcessupdate, onerror);
					REQUEST('appControl', 'updateprocessbyorder', [str,folio,merma,fecharequerida,hrrequerida,fechaestimada,horaestimada,ajuste] , onProcessupdate, onerror);
				}
			//}
	}
}        
function onProcessupdate(dataResult){
	if(dataResult.length>0){
		inedition=false;
		if(dataResult[0]['exito'].localeCompare("si")==0)
		{		
			alert("\u00C9xito al modificar los datos");
			detailorder(idorden);
		}
		else if(dataResult[0]['exito'].localeCompare("sin")==0)
		{	detailorder(idorden);
			alert("El producto no ha sido encontrado");
		}else if(dataResult[0]['exito'].localeCompare("ya")==0)
		{	detailorder(idorden);
			alert("Ya existe un proceso con estos datos. Inicia el "+dataResult[0]['inicio']+" termina el "+dataResult[0]['termino']);
		}
	}else
	{
		alert("Error al modificar los datos. Intente nuevamente");
	}
    	
}


function calculafechafin(){
		var row=document.getElementById("proc"+str2);
		row.className = "table-row2";
		var cells = row.getElementsByTagName('td')
		
		var ajuste=document.getElementById("txttiempoajuste"+str2).value;
   		var fecharequerida=document.getElementById("fecrequerida"+str2).value;
   		var horarequerida=document.getElementById("hrequerida"+str2).value;
		//var cantidad=document.getElementById("cantidad").innerText.split(' ');
		var merma=document.getElementById("txtmerma"+str2).value;
		var cantidad=cells[2].innerText;

		if(ajuste<0 || !isNumber(ajuste) || fecharequerida.length==0){
			alert("Los minutos de ajuste o la fecha requerida son incorrectos");
			document.getElementById("hrequerida"+str2).value="";
		}else if(horarequerida.length==0 ||horarequerida==0){
			alert("Hora requerida incorrecta");
		}else{
			var estandar=parseFloat(cells[5].innerText)*8;
			//var minutos=( ( ( (parseFloat(cantidad)+parseFloat(merma))/parseFloat(estandar))*480)+parseFloat(ajuste));	
			var minutos=( ( ( (parseFloat(cantidad))/parseFloat(estandar))*480)+parseFloat(ajuste));	
			var dat=getdate(addMinutes(new Date(fecharequerida+' '+horarequerida), minutos)).split(' ');	
			cells[9].innerText=dat[0]+"\n"+dat[1];
			
			//cells[11].innerText=dat[1];
			//var min=dat[1].split(":");
			/*if(min[1]>=30){
				var su=parseFloat(min[0])+parseFloat(1);
				var aux= su< 10 ? "0" + su : su;
				cells[10].innerText=aux+":00:00"
			}else{
				cells[10].innerText=min[0]+":00:00";
			}*/
		}	
}

		////////////////////////////////////GET OPERATORS OF ALL SYSTEM/////////////////////////////////////////////////////////////
function getoperators(){
	REQUEST('appControl', 'getoperators', [] , onOperators, onerror);
}

function onOperators(dataResult){
	var selectList=document.getElementById("optionoperator"+str2);
	for(var a=0;a<dataResult.length;a++){
		var option = document.createElement("option");
    		option.value=dataResult[a]['id'];
    		option.text =dataResult[a]['operador'];
		if(operador.trim().localeCompare(dataResult[a]['operador'].trim())==0){
			option.selected=true;
		}
    		selectList.appendChild(option);
	}
}

		///////////////////////////////////FUNCTION FOR INCIDENTS////////////////////////////////////////////
function incident1(str){
	idordenforincident=str;
// Get the modal
	var modal = document.getElementById('myModalsaveincidente');


	// Get the <span> element that closes the modal
	var span = document.getElementById("closeincident");
  	modal.style.display = "block";

	// When the user clicks on <span> (x), close the modal
	span.onclick = function() {
		
			//var a=confirm("No se guardar\u00E1 ning\u00FAn dato \u00BFSeguro que desea abandonar la captura?");
			//if(a){
				document.getElementById('updateincidente').style.display='none';
				document.getElementById('incidente').style.display='none';
				document.getElementsByName('incidentcontenedor')[0].style.display='block';
				modal.style.display = "none";
			//}
		
	}

	getallincidentes(idordenforincident);

}
function getallincidentes(idorden){
	document.getElementById('updateincidente').style.display='none';
	document.getElementById('incidente').style.display='none';
	document.getElementsByName('incidentcontenedor')[0].style.display='block';
	document.getElementById("imgloadincidents").style.display="block";

	REQUEST('appControl', 'getallincidentsbyorder', [idorden] , onIncidents, onerror);
}
function onIncidents(dataResult){
	document.getElementById("imgloadincidents").style.display="none";
	myDeleteFunctionincidents();
	if(dataResult.length>0){
		document.getElementById('ordenid').innerText="No. "+dataResult[0]['idorden'];
		for (var i = 0; i<dataResult.length; i++)
		{
				
				var tableRef = document.getElementById('resultadoincidents').getElementsByTagName('tbody')[0];
				// Insert a row in the table at the last row
				var newRow   = tableRef.insertRow(tableRef.rows.length);
				newRow.className = "table-row";
				

			
				
				newRow.id=dataResult[i]["id"];
				// Insert a cell in the row at index 0

				var newCell  = newRow.insertCell(0);
				newCell.id="np"+dataResult[i]["id"];
				var newText  = document.createTextNode(dataResult[i]["id"]);
				newCell.appendChild(newText);

				var newCell  = newRow.insertCell(1);
				newCell.id="fecha"+dataResult[i]["id"];
				var newText  = document.createTextNode(dataResult[i]["fecha"]);
				newCell.appendChild(newText);
				
				
				var newCell  = newRow.insertCell(2);
				newCell.id="denuncia"+dataResult[i]["id"];
				var newText  = document.createTextNode(dataResult[i]["areadenuncia"]);
				newCell.appendChild(newText);
				
				
				var newCell  = newRow.insertCell(3);
				newCell.id="denunciada"+dataResult[i]["id"];
				var newText  = document.createTextNode(dataResult[i]["areadenunciada"]);
				newCell.appendChild(newText);

				
				var newCell  = newRow.insertCell(4);
				newCell.id="Nombre"+dataResult[i]["id"];
				var newText  = document.createTextNode(dataResult[i]["nombredenunciado"]+" "+dataResult[i]["apellidosdenunciado"]);
				newCell.appendChild(newText);

				newCell  = newRow.insertCell(5);
				var img0 = document.createElement('img');
				img0.src = "../img/print.png";
				img0.id="imgimpimirincidente"+dataResult[i]["id"];
				img0.addEventListener('click', prinincident.bind(null,dataResult[i]["id"]));			
				newCell.appendChild(img0);
	
				newCell  = newRow.insertCell(6);
				var img = document.createElement('img');
				img.src = "../img/editar.png";
				img.id="imgeditarincidente"+dataResult[i]["id"];
				img.addEventListener('click', editincident.bind(null,dataResult[i]["id"]));				
				newCell.appendChild(img);
				

				newCell  = newRow.insertCell(7);
				var img3 = document.createElement('img');
	   			img3.src = "../img/delete.png";
				img3.id="imgeliminarincidente"+dataResult[i]["id"];				
				img3.addEventListener('click',deleteincident.bind(null,dataResult[i]["id"]));
				newCell.appendChild(img3);
				


			
		}

		document.getElementById("sinincidents").style.display="none";
		document.getElementById("tablaincident").style.display="block";
		//document.getElementById("sin2").style.display="block";
		//pagination('#resultado');
	
	}else{
		document.getElementById("sinincidents").style.display="block";
		document.getElementById("tablaincident").style.display="none";
		//document.getElementById("sin2").style.display="none";
	
	}	
//getdepartaments();
}
function prinincident(str){
	window.open('incidentepdf.php?idi='+str, '_blank');
}
function deleteincident(id){
	if(confirm("Esta apunto de eliminar este incidente, esta acci\u00F3n es irreversible. \u00BFSeguro que desea proceder?")){
		REQUEST('appControl', 'deleteincident', [id] , onIncidentdelete, onerror);
	}
}
function onIncidentdelete(dataResult){
	if(dataResult[0]['exito'].localeCompare("si")==0)
	{	
		alert("\u00C9xito al eliminar los datos");
		getallincidentes(idordenforincident);	
	}
	else if(dataResult[0]['exito'].localeCompare("sin")==0)
	{
		alert("Los datos no ha sido encontrada");
	}else
	{
		alert("Error al eliminar los datos. Intente nuevamente");
	}	
}
function editincident(id){
	idincidentedit=id;
	REQUEST('appControl', 'getspecificincident', [id] , onIncidentspecific, onerror);
}
function  onIncidentspecific(dataResult){
	
	if(dataResult.length>0){
		dptodenuncia=dataResult[0]['areadenuncia'];
		deptodenunciado=dataResult[0]['areadenunciada'];
		document.getElementById("txtfecharegistro2").value=dataResult[0]['fecha'];
		document.getElementById("optionareadenuncia2").value=dataResult[0]['areadenuncia'];
		document.getElementById("optionareadenunciada2").value=dataResult[0]['areadenunciada'];
		document.getElementById("txtdenunciado2").value=dataResult[0]['nombredenunciado'];
		document.getElementById("txtapellidosdenunciado2").value=dataResult[0]['apellidosdenunciado'];
		document.getElementById("txtdescripcionincidente2").value=dataResult[0]['incidente'];
		document.getElementById("txtcontingencia2").value=dataResult[0]['contingencia'];
		document.getElementById("txtcausa2").value=dataResult[0]['causa'];
		document.getElementById("txterradicar2").value=dataResult[0]['erradicar'];
		document.getElementById("txtfechaconclusion2").value=dataResult[0]['fechaconclusion'];
		document.getElementById("txtdenunciante2").value=dataResult[0]['nombredenunciante'];
		document.getElementById("txtapellidosdenunciante2").value=dataResult[0]['apellidosdenunciante'];
	}
	document.getElementById("updateincidente").style.display="block";
	document.getElementsByName('incidentcontenedor')[0].style.display='none';
	
	getdepartaments2();
}

function formincident(){
	document.getElementById('incidente').style.display='block';
	document.getElementsByName('incidentcontenedor')[0].style.display='none';
getdepartaments();

}
function canceladdincident(){
	document.getElementById('incidente').style.display='none';
	document.getElementsByName('incidentcontenedor')[0].style.display='block';
}
function cancelupdateincident(){
	document.getElementById('updateincidente').style.display='none';
	document.getElementsByName('incidentcontenedor')[0].style.display='block';
}


function getdepartaments(){
	REQUEST('appControl', 'getdepartaments', [] , onDepartaments, onerror);
}
function onDepartaments(dataResult){
	var op=document.getElementById("optionareadenuncia");
	op.innerHTML="";
	var option = document.createElement("option");
	    		option.value=0;
    			option.text ="---Seleccione---";
			
    			op.appendChild(option);
			
	if(dataResult.length>0){
		for(var a=0;a<dataResult.length;a++){
			option = document.createElement("option");
	    		option.value=dataResult[a]['nombre'];
    			option.text =dataResult[a]['nombre'];
			
    			op.appendChild(option);
			
		}
		
	}
	var op=document.getElementById("optionareadenunciada");
	op.innerHTML="";
	var option = document.createElement("option");
	    		option.value=0;
    			option.text ="---Seleccione---";
			
    			op.appendChild(option);
			
	if(dataResult.length>0){
		for(var a=0;a<dataResult.length;a++){
			option = document.createElement("option");
	    		option.value=dataResult[a]['nombre'];
    			option.text =dataResult[a]['nombre'];
			
    			op.appendChild(option);
			
		}
		
	}
}
function saveincident(){
	var areadenuncia=document.getElementById("optionareadenuncia").value;
	var areadenunciada=document.getElementById("optionareadenunciada").value;
	var denunciado=document.getElementById("txtdenunciado").value;
	var apelliodosdenunciado=document.getElementById("txtapellidosdenunciado").value;
	var incidente=document.getElementById("txtdescripcionincidente").value;
	var contingencia=document.getElementById("txtcontingencia").value;
	var causa=document.getElementById("txtcausa").value;
	var erradicar=document.getElementById("txterradicar").value;
	var fechaconclusion=document.getElementById("txtfechaconclusion").value;
	var denunciante=document.getElementById("txtdenunciante").value;
	var apellidosdenunciante=document.getElementById("txtapellidosdenunciante").value;
	if(areadenuncia.length==0){
		alert("Debe seleccionar el \u00E1rea a denunciar");
	}else if(areadenunciada.length==0){
		alert("Debe seleccionar el \u00E1rea que denuncia");
	}else if(denunciado.length==0){
		alert("Ingrese el nombre del denunciado");
	}else if(apelliodosdenunciado.length==0){
		alert("Ingrese los apellidos del denunciado");
	}else if(incidente.length==0){
		alert("Describa el incidente");
	}else if(contingencia.length==0){
		alert("Describa la contingencia inmediata");
	}else if(denunciante.length==0){
		alert("Ingrese el nombre del denunciante");
	}else if(apellidosdenunciante.length==0){
		alert("Ingrese los apellidos del denunciante");
	}else {
			REQUEST('appControl', 'saveincident', [idordenforincident,areadenuncia,areadenunciada,denunciado,apelliodosdenunciado,incidente,contingencia,causa,erradicar,fechaconclusion,denunciante,apellidosdenunciante] , onSaveincident, onerror);
	}
}
function onSaveincident(dataResult){
	if(dataResult.length>0){
		inedition=false;
		if(dataResult[0]['exito'].localeCompare("si")==0)
		{		
			alert("\u00C9xito al guardar los datos");
			getallincidentes(idordenforincident);	
		}
	}else
	{
		alert("Error al modificar los datos. Intente nuevamente");
	}
    	
}
function getdepartaments2(){
	REQUEST('appControl', 'getdepartaments', [] , onDepartaments2, onerror);
}
function onDepartaments2(dataResult){
	var select=document.getElementById("optionareadenuncia2");
	select.innerHTML="";
	for(var a=0;a<dataResult.length;a++){
		var option = document.createElement("option");
    		option.value=dataResult[a]['nombre'];
    		option.text =dataResult[a]['nombre'];
		if(dptodenuncia.trim().localeCompare(dataResult[a]['nombre'].trim())==0){
			option.selected=true;
		}
    		select.appendChild(option);
	}
	var select2=document.getElementById("optionareadenunciada2");
		select2.innerHTML="";
	for(var a=0;a<dataResult.length;a++){
		var option = document.createElement("option");
    		option.value=dataResult[a]['nombre'];
    		option.text =dataResult[a]['nombre'];
		if(deptodenunciado.trim().localeCompare(dataResult[a]['nombre'].trim())==0){
			option.selected=true;
		}
    		select2.appendChild(option);
	}
}




function updateincident(){
	var areadenuncia=document.getElementById("optionareadenuncia2").value;
	var areadenunciada=document.getElementById("optionareadenunciada2").value;
	var denunciado=document.getElementById("txtdenunciado2").value;
	var apelliodosdenunciado=document.getElementById("txtapellidosdenunciado2").value;
	var incidente=document.getElementById("txtdescripcionincidente2").value;
	var contingencia=document.getElementById("txtcontingencia2").value;
	var causa=document.getElementById("txtcausa2").value;
	var erradicar=document.getElementById("txterradicar2").value;
	var fechaconclusion=document.getElementById("txtfechaconclusion2").value;
	var denunciante=document.getElementById("txtdenunciante2").value;
	var apellidosdenunciante=document.getElementById("txtapellidosdenunciante2").value;

	if(areadenuncia.length==0){
		alert("Debe seleccionar el \u00E1rea a denunciar");
	}else if(areadenunciada.length==0){
		alert("Debe seleccionar el \u00E1rea que denuncia");
	}else if(denunciado.length==0){
		alert("Ingrese el nombre del denunciado");
	}else if(apelliodosdenunciado.length==0){
		alert("Ingrese los apellidos del denunciado");
	}else if(incidente.length==0){
		alert("Describa el incidente");
	}else if(contingencia.length==0){
		alert("Describa la contingencia inmediata");
	}else if(denunciante.length==0){
		alert("Ingrese el nombre del denunciante");
	}else if(apellidosdenunciante.length==0){
		alert("Ingrese los apellidos del denunciante");
	}else {
		
		if(confirm("Esta a punto de modificar este registro. \u00BFSeguro de continuar? ")){
			REQUEST('appControl', 'updateincident', [idincidentedit,areadenuncia,areadenunciada,denunciado,apelliodosdenunciado,incidente,contingencia,causa,erradicar,fechaconclusion,denunciante,apellidosdenunciante] , onUpdateincident, onerror);
		}
	}
}
function onUpdateincident(dataResult){
	if(dataResult.length>0){
		inedition=false;
		if(dataResult[0]['exito'].localeCompare("si")==0)
		{		
			alert("\u00C9xito al modificar los datos");
			getallincidentes(idordenforincident);
		}else if(dataResult[0]['exito'].localeCompare("sin")==0){
			alert("Datos no encontrados");
		}
	}else
	{
		alert("Error al modificar los datos. Intente nuevamente");
	}
    	
}


////////////////////////////////////////////////////GLOBAL FUNCTIONS FOR DIFFERENT PROCESS/////////////////////////////////////////////////////////////////////////////
document.onkeydown = ShowKeyCode;
        function ShowKeyCode(evt) {
	//alert(localStorage.getItem("operador"));
            	if(evt.keyCode==8 ){
			
			cod=evt.keyCode;
		}else{
			cod=evt.keyCode;
		}
			
        }




function showEdit(editableObj) {
			editableObj.style.backgroundColor="#e3bfc4";

		}
function edita(editableObj) {
			editableObj.style.backgroundColor="#FFCC99";
		}

function onerror(errObject)
{
		alert(errObject["Message"] + ":\n" + errObject["Detail"]);
}


function getval(grupo){
	var selec;

	for(var i = 0; i < grupo.length; i++) {
 	  if(grupo[i].checked)
    	   selec = grupo[i].value;
 	}
return selec;
}



function myDeleteFunction() {
	var tableRef = document.getElementById('resultado');
	//var rowCount = tableRef.rows.length;
	
	var row = document.getElementsByTagName('tbody')[0];
	var rowCount = row.rows.length;

		for(var a=0;a<rowCount;a++)
		{

			//tableRef.deleteRow(1);
			row.deleteRow(0);
		}
    
}

function myDeleteFunctioncomponentes() {
	var tableRef = document.getElementById('componentesorden');
	//var rowCount = tableRef.rows.length;
	
	var row = tableRef.getElementsByTagName('tbody')[0];
	var rowCount = row.rows.length;

		for(var a=0;a<rowCount;a++)
		{

			//tableRef.deleteRow(1);
			row.deleteRow(0);
		}
    
}
function myDeleteFunctionincidents() {
	var tableRef = document.getElementById('resultadoincidents');
	//var rowCount = tableRef.rows.length;
	
	var row = tableRef.getElementsByTagName('tbody')[0];
	var rowCount = row.rows.length;

		for(var a=0;a<rowCount;a++)
		{

			//tableRef.deleteRow(1);
			row.deleteRow(0);
		}
    
}
function myDeleteFunctionprocesos() {
	var tableRef = document.getElementById('procesosorden');
	//var rowCount = tableRef.rows.length;
	
	var row = tableRef.getElementsByTagName('tbody')[0];
	var rowCount = row.rows.length;

		for(var a=0;a<rowCount;a++)
		{

			//tableRef.deleteRow(1);
			row.deleteRow(0);
		}
    
}

function doSearch()
		{
			//var tableReg = document.getElementById('resultado');
			var tableReg =  document.getElementById('resultado').getElementsByTagName('tbody')[0];

			var searchText = document.getElementById('txtbuscar').value.toLowerCase();
			var cellsOfRow="";
			var found=false;
			var compareWith="";
 
			// Recorremos todas las filas con contenido de la tabla
			for (var i = 0; i < tableReg.rows.length; i++)
			{
				cellsOfRow = tableReg.rows[i].getElementsByTagName('td');
				found = false;
				// Recorremos todas las celdas
				for (var j = 0; j < cellsOfRow.length && !found; j++)
				{
					compareWith = cellsOfRow[j].innerHTML.toLowerCase();
					// Buscamos el texto en el contenido de la celda
					if (searchText.length == 0 || (compareWith.indexOf(searchText) > -1))
					{
						found = true;
					}
				}
				if(found)
				{
					tableReg.rows[i].style.display = '';
				} else {
					// si no ha encontrado ninguna coincidencia, esconde la
					// fila de la tabla
					tableReg.rows[i].style.display = 'none';
				}
			}
		}



function limpiar(field){
field.value="";
}
function validatenumber(val){
	if(val.value.length<=0){
		val.value=0;
	}
}

function isNumber( input ) {
    return !isNaN( input );
}


function uploadFile(file,doc){
    var code=document.getElementById("txtcodigo").value;
    var url = 'uploadfiles.php';
    var xhr = new XMLHttpRequest();
    var fd = new FormData();
    xhr.open("POST", url, true);
    xhr.onreadystatechange = function() {
        if (xhr.readyState == 4 && xhr.status == 200) {
            // Every thing ok, file uploaded
            console.log(xhr.responseText); // handle response.
        }
   };
    fd.append("upload_file", file);
    fd.append("doc", doc);	
    fd.append("code", code);
    xhr.send(fd);
}

function disableEnable(elems, isDisabled){
    for(var i = elems.length-1;i>=0;i--){
        elems[i].disabled = isDisabled;
    }
}

function viewfile(ruta){
 window.open(ruta, '_blank');
}
function completecvesae(cve){
	while(cve.toString().length<8){
			cve='0'+cve;	
		}
return cve;
}
function dopercentaje(str){
	var cantidad=str.value;
	var aux,last;
	if(cod!=8){
		if(cantidad.length>1){
			aux=cantidad.split("%");
			last = aux[1].substr(aux[1].length - 1);
		
			if(isNumber(last) || last.localeCompare(".")==0){
			
				cantidad=aux[0]+last;		
				if((cantidad>=0 && cantidad<=100)|| cantidad.indexOf(".")<=0){
					str.value=cantidad+"%";
				}else{
					str.value=str.value.substring(0, str.value.length - 1);;
				}
			}else{
				cantidad = aux[0].substring(0, aux[0].length - 1); 
				str.value=cantidad+"%";
			}
		}else{
			if(isNumber(cantidad)){
				if(cantidad>=0 && cantidad<=100){
					str.value=cantidad+"%";
				}else{
					str.value="";
				}
			}else{
				str.value="";
			}
		}
	}
}

function todaydate() {
	var date=new Date();
  var yyyy = date.getFullYear().toString();
  var mm = (date.getMonth()+1).toString();
  var dd  = date.getDate().toString();

  var mmChars = mm.split('');
  var ddChars = dd.split('');

  return yyyy + '-' + (mmChars[1]?mm:"0"+mmChars[0]) + '-' + (ddChars[1]?dd:"0"+ddChars[0]);
}

function getdate(date) {
	//var date=new Date();
  var yyyy = date.getFullYear().toString();
  var mm = (date.getMonth()+1).toString();
  var dd  = date.getDate().toString();

  var mmChars = mm.split('');
  var ddChars = dd.split('');

 var hours = date.getHours() < 10 ? "0" + date.getHours() : date.getHours();
        var minutes = date.getMinutes() < 10 ? "0" + date.getMinutes() : date.getMinutes();
        var seconds = date.getSeconds() < 10 ? "0" + date.getSeconds() : date.getSeconds();
       var time = hours + ":" + minutes + ":" + seconds;

  return yyyy + '-' + (mmChars[1]?mm:"0"+mmChars[0]) + '-' + (ddChars[1]?dd:"0"+ddChars[0])+' '+time;
}

function addMinutes(date, minutes) {
	var m=parseFloat(minutes)*60000;
    return new Date(date.getTime() + m);
}