var str2;
var editmodulos;
var modulosdata;
var modulosdata2;
var cod=0;
var inedition=false;
var isvendedor=localStorage.getItem("vendedor");
var iduser=localStorage.getItem("id");
var dataResult2;
var idcotizacion=0;
var idco;


//////////////////////////////////////GENERAL FUNCTIONS FOR LIS TOF QUOTATIONS////////////////////////
function getquotations(){
	document.getElementById("imgload").style.display="block";
	var fecini=document.getElementById("txtfechainicial").value;
	var fecfin=document.getElementById("txtfechafinal").value;
	var vendedor=document.getElementById("optionagente").value;
	if(fecini.length==0 ||fecfin.length==0 ){
		showmessage("Alguna de las fechas es invalida!!.");
	}else if(vendedor==0){
		showmessage("El vendedor es invalido.");
	}else{
		REQUEST('appControlcotizador', 'getallquotationsbydateandseller', [fecini,fecfin,vendedor], onQuotations, onerror);
	}
}

function onQuotations(dataResult){
document.getElementById("imgload").style.display="none";
	myDeleteFunction();
      var totpedidos=0;
      var totactivas=0;
      var totvencidas=0;
	var totclientes=0;
	var totcotizaciones=0;
	if(dataResult.length>0){
		dataResult2=dataResult;	
		for (var i = 0; i<dataResult.length; i++)
		{		var modu="";
			
			var tableRef = document.getElementById('resultado').getElementsByTagName('tbody')[0];;
			// Insert a row in the table at the last row
			var newRow   = tableRef.insertRow(tableRef.rows.length);
			newRow.className = "table-row";
			newRow.id="cot"+dataResult[i]["id"];
			

						
			var newCell  = newRow.insertCell(0);
			newCell.id="cliente"+dataResult[i]["id"];
			var newText  = document.createTextNode(dataResult[i]["agente"]);
			newCell.appendChild(newText);

			
			var newCell  = newRow.insertCell(1);
			newCell.id="clave"+dataResult[i]["id"];
			var newText  = document.createTextNode(dataResult[i]["cliente"]);
			newCell.appendChild(newText);

			
						
			var newCell  = newRow.insertCell(2);
			newCell.id="clave"+dataResult[i]["id"];
			var newText  = document.createTextNode(dataResult[i]["registro"]);
			newCell.appendChild(newText);

			var newCell  = newRow.insertCell(3);
			newCell.id="clave"+dataResult[i]["id"];
			var newText  = document.createTextNode(dataResult[i]["ventas"]);
			newCell.appendChild(newText);

			var newCell  = newRow.insertCell(4);
			newCell.id="clave"+dataResult[i]["id"];
			var newText  = document.createTextNode(dataResult[i]["totalcotizaciones"]);
			newCell.appendChild(newText);

			var newCell  = newRow.insertCell(5);
			newCell.id="clave"+dataResult[i]["id"];
			var newText  = document.createTextNode(dataResult[i]["totalpedidos"]);
			newCell.appendChild(newText);
			var porc=0;
			if(dataResult[i]["totalcotizaciones"]==0){
				porc=0;
			}else{
			        porc=(parseFloat(dataResult[i]["totalpedidos"])/parseFloat(dataResult[i]["totalcotizaciones"]))*100 
			}
			var newCell  = newRow.insertCell(6);
			newCell.id="clave"+dataResult[i]["id"];
			var newText  = document.createTextNode( porc+" %");
			newCell.appendChild(newText);
 		

			
			totclientes++;
			totpedidos=parseFloat(totpedidos)+parseFloat(dataResult[i]['totalpedidos']);
			totactivas=parseFloat(totactivas)+parseFloat(dataResult[i]['totalactivas']);
			totvencidas=parseFloat(totvencidas)+parseFloat(dataResult[i]['totalvencidas']);
			totcotizaciones=parseFloat(totcotizaciones)+parseFloat(dataResult[i]['totalcotizaciones']);
		}
		document.getElementById("totclientes").innerText=totclientes;	
		document.getElementById("totpedidos").innerText=totpedidos;
	
		var conver=0;

	
			if(totcotizaciones==0){
				conver=0;
			}else{
			        conver=(parseFloat(totpedidos)/parseFloat(totcotizaciones))*100;
			}

		document.getElementById("conversion").innerText=conver.toFixed(2)+" %";
		document.getElementById("totvencidas").innerText=totvencidas;
		document.getElementById("totactivas").innerText=totactivas;
		
		document.getElementById("sin").style.display="none";
		document.getElementById("tabla").style.display="block";
		document.getElementById("sin2").style.display="block";
		document.getElementById("print").style.display="block";
		pagination('#resultado');
	}else{
		document.getElementById("sin").style.display="block";
		document.getElementById("tabla").style.display="none";
		//document.getElementById("sin2").style.display="none";
	
	}	
}

function pagination(id){
	
		$(document).ready(function() {
			var table;
		
			if ( $.fn.dataTable.isDataTable( id ) ) {
   				table = $(id).DataTable();
				table.destroy();
				getquotations();
			}
			else {
   				 table = $(id).DataTable( {
        				"scrollY": 500,
       					"scrollX": true
    				} );
			}
   			
		} );
}

function getvendedores(){
	
	REQUEST('appControlcotizador', 'getvendedores', [], onVendedores, onerror);
	
}

function onVendedores(dataResult){
	var op= document.getElementById("optionagente");
   		
	op.innerHTML="";
	var option = document.createElement("option");
	    		option.value=0;
    			option.text ="---Seleccione---";
			
    			op.appendChild(option);
	 option = document.createElement("option");
	    		option.value=-1;
    			option.text ="Todos";
			
    			op.appendChild(option);		
	if(dataResult.length>0){
		for(var a=0;a<dataResult.length;a++){
			option = document.createElement("option");
	    		option.value=dataResult[a]['id'];
    			option.text =dataResult[a]['nombre']+" "+dataResult[a]['appat']+" "+dataResult[a]['apmat'];
			
    			op.appendChild(option);
		}
		
	}
	op.selectedIndex = "0";

}

//////////////////////////////////////////////////////////GENERAL FUNCTIONS////////////////////////////////////////

function printreport(){
	var fecini=document.getElementById("txtfechainicial").value;
	var fecfin=document.getElementById("txtfechafinal").value;
	var vendedor=document.getElementById("optionagente").value;
	window.open('cotizacionesestimacionpdf.php?fecini='+fecini+"&fecfin="+fecfin+"&ven="+vendedor, '_blank');
}



document.onkeydown = ShowKeyCode;
        function ShowKeyCode(evt) {
	//alert(evt.keyCode);
            	if(evt.keyCode==8 ){
			
			cod=evt.keyCode;
		}else{
			cod=evt.keyCode;
		}
			
        }

function onerror(errObject)
{
		alert(errObject["Message"] + ":\n" + errObject["Detail"]);
}





function getval(grupo){
	var selec;

	for(var i = 0; i < grupo.length; i++) {
 	  if(grupo[i].checked)
    	   selec = grupo[i].value;
 	}
return selec;
}
function myDeleteFunction() {
	var tableRef = document.getElementById('resultado');
	//var rowCount = tableRef.rows.length;
	
	var row = document.getElementsByTagName('tbody')[0];
	var rowCount = row.rows.length;

		for(var a=0;a<rowCount;a++)
		{

			//tableRef.deleteRow(1);
			row.deleteRow(0);
		}
    
}

function doSearch()
		{
			//var tableReg = document.getElementById('resultado');
			var tableReg = document.getElementsByTagName('tbody')[0];

			var searchText = document.getElementById('txtbuscar').value.toLowerCase();
			var cellsOfRow="";
			var found=false;
			var compareWith="";
 
			// Recorremos todas las filas con contenido de la tabla
			for (var i = 0; i < tableReg.rows.length; i++)
			{
				cellsOfRow = tableReg.rows[i].getElementsByTagName('td');
				found = false;
				// Recorremos todas las celdas
				for (var j = 0; j < cellsOfRow.length && !found; j++)
				{
					compareWith = cellsOfRow[j].innerHTML.toLowerCase();
					// Buscamos el texto en el contenido de la celda
					if (searchText.length == 0 || (compareWith.indexOf(searchText) > -1))
					{
						found = true;
					}
				}
				if(found)
				{
					tableReg.rows[i].style.display = '';
				} else {
					// si no ha encontrado ninguna coincidencia, esconde la
					// fila de la tabla
					tableReg.rows[i].style.display = 'none';
				}
			}
		}
function isNumber( input ) {
    return !isNaN( input );
}
function showmessage(msg){
	alert(msg);
}
function clearInputFields(divElement) {
        var ele = document.getElementById(divElement);

       
        for (i = 0; i < ele.childNodes.length; i++) {
           
            var child = ele.childNodes[i].firstChild;
            //console.log(child);

            
            if (child) {
                switch (child.type) {
                    case 'button':
                    case 'text':
			child.value = ''
                    case 'submit':
                    case 'password':
                    case 'file':
                    case 'email':
                    case 'date':
                    case 'number':
                        child.value = '';
                    case 'checkbox':
                    case 'radio':
                        child.checked = false;
                }
            }
        }
    }
		//////////////////////////////////////USER DENIED///////////////////////////////////////////////

